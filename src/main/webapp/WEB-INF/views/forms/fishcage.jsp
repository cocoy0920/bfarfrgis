<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH CAGE INFORMATION</legend>
			<div class="form-row">
    			<div class="col-md-4 mb-3">
			 <label  for="nameOfOperator"><span>Name of Operator:</span></label>
			 <input type="text" name="nameOfOperator"  id="nameOfOperator"  class="fishcagenameOfOperator form-control" required/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-4 mb-3">
				<label  for="classificationofOperator"><span>Classification of Operator</span></label>				
				<select name="classificationofOperator"  id="classificationofOperator"  class="classificationofOperator form-control" required>
					
										<option value=""></option>
										<option value="Individual">Individual</option>
										<option value="Partnership">Partnership</option>
										<option value="Corporation">Corporation</option>
										<option value="Association">Association</option>
				</select>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-4 mb-3">
		<label  for="cageDimension"><span>Cage Dimension(LxWxH):</span></label>
					<input type="text" name="cageDimension"  id="cageDimension"  class="cageDimension form-control" required/>
		<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>	
		</div>
		</div>	
		<div class="form-row">
<!--     			<div class="col-md-4 mb-3">	
				<label  for="area"><span>Area</span></label>
				<input type="text" name="area"  id="area"  class="area form-control" required/>	
				<input type="number" name="area" min="0" value="0" step="any"  id="area"  class="area form-control" required/>																
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div> -->
				<div class="col-md-4 mb-3">	
					<label  for="cageNoOfCompartments"><span>No. of Compartments:</span></label>
					<input type="number" name="cageNoOfCompartments"  id="cageNoOfCompartments"  class="cageNoOfCompartments form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
		</div>
		<div class="form-row">
				<div class="col-md-4 mb-3">	
				<label  for="cageType"><span>Cage Type:</span></label>
					<select name="cageType"  id="cageType"  class="cageType form-control" required>		
							<option value=""></option>
							<option value="Round/Circular">Round/Circular</option>
							<option value="Square">Square</option>
							<option value="Long/Rectangle">Long/Rectangle</option>										
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
				<div class="col-md-4 mb-3">	
				<label  for="cageType"><span>Cage Design:</span></label>
					<select name="cageDesign"  id="cageDesign"  class="cageDesign form-control" required>		
							<option value=""></option>
							<option value="Bamboo">Bamboo</option>
							<option value="High-density Polyetylene/HDPE">High-density Polyetylene/HDPE</option>
							<option value="Galvanized">Galvanized</option>										
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
		</div>
		<div class="form-row">
    			<div class="col-md-12 mb-3">	
		<label  for="indicateSpecies"><span>Indicate Species:</span></label>
					<input type="text" name="indicateSpecies"  id="indicateSpecies"  class="fishcageindicateSpecies form-control" required/>
		<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div>
		</div>
		</fieldset>