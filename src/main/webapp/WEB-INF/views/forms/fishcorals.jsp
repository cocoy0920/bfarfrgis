
			<fieldset class="form-group border border-dark rounded p-4"><legend  class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH CORRAL INFORMATION</legend>
				<div class="form-row">
    				<div class="col-md-6 mb-3">
					<label  for="nameOfOperator"><span>Name of Operator:</span></label> 
					<input type="text"  name="nameOfOperator"  id="nameOfOperator"  class="fishcoralnameOfOperator form-control" required/>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>

					</div>
				<div class="form-row">
    				<div class="col-md-6 mb-3">
					<label  for="operatorClassification"><span>Operator Classification:</span></label>				
					<select name="operatorClassification"  id="operatorClassification"  class="fishcoraloperatorClassification form-control" required>
										<option value=""></option>
										<option value="Individual">Individual</option>
										<option value="Private Organization">Private Organization</option>
										<option value="Government">Government</option>
					</select>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
      				<div class="col-md-6 mb-3">
					<label  for="nameOfStationGearUse"><span>Gear Use:</span></label>	
					<input type="text"  name="nameOfStationGearUse"  id="nameOfStationGearUse"  class="nameOfStationGearUse form-control" required/>																
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
				</div>
				<div class="form-row">
    				<div class="col-md-6 mb-3">
					<label  for="noOfUnitUse"><span>Unit Use:</span></label>
					<input type="text"  name="noOfUnitUse"  id="noOfUnitUse"  class="noOfUnitUse form-control" required/>																
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
      				<div class="col-md-6 mb-3">
					<label  for="fishesCaught"><span>Fishes Caught:</span></label>	
					<input type="text"  name="fishesCaught"  id="fishesCaught"  class="fishesCaught form-control" required/>																
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
				</div>
			</fieldset>
