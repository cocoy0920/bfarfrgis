
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">MANGROVE INFORMATION</legend>
			<div class="form-row">
    			<div class="col-md-4 mb-3">
			 <label  for="indicateSpecies"><span>Indicate Species:</span></label>
			 <input type="text"  name="indicateSpecies"  id="indicateSpecies"  class="indicateSpecies form-control" required/>
			<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        							Required
      						</div>
			
			</div>
				<div class="col-md-4 mb-3">
				<label  for="type"><span>Type:</span></label>				
				<select name="type"  id="type"  class="type form-control" required>
										<option value=""></option>
										<option value="DENR">DENR</option>
										<option value="LGU">LGU</option>
										<option value="BFAR INITIATED">BFAR INITIATED</option>
										<option value="REFORESTATION">REFORESTATION</option>
				</select>
					<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        							Required
      						</div>
			
			</div>
<!-- 			<div class="col-md-4 mb-3">
				<label  for="area"><span>Area</span></label>
				<input type="text" name="area"  id="area"  class="area form-control" required/>	
					<input type="number" name="area"  min="0" value="0" step="any"   id="area"  class="area form-control" required/>																
					<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        							Required
      						</div>
			
			</div> -->
			</div>
		</fieldset>
		
	