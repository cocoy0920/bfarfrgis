				
		 <fieldset class="form-group border border-dark rounded p-4">
		 <legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISHERIES SCHOOL INFORMATION</legend>
		    <div class="form-row">
    				<div class="col-md-6 mb-3">
		    		<label   for="name"><span>Name of School:</span></label>
		    		<input type="text" name="name" id="name" class="schoolname form-control" required/>			
		   			<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
					</div>
		    
					<div class="col-md-6 mb-3">	
		    		<label   for="dateEstablished"><span>Date Established:</span></label>
               	 	<input  type="date" data-date-format="YYYY-MM-dd" name="dateEstablished" id="dateEstablished" class="dateEstablished form-control" required>	
              		<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
					</div>
		    </div>
		   
		    <div class="form-row">
    			<div class="col-md-6 mb-3">
		    		<label  for="numberStudentsEnrolled"><span>Number of Student Enrolled:</span></label>
		    	 	<input type="number" name="numberStudentsEnrolled" id="numberStudentsEnrolled" class="numberStudentsEnrolled form-control" required/>			
		    		<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
				</div>
		    
<!-- 				<div class="col-md-6 mb-3">	
		    		<label  for="area"><span>Area</span></label>
		    		<input type="text" name="area"  id="area"  class="area form-control" required/>	
		     		<input type="number" name="area"  min="0" value="0" step="any"  id="area"  class="schoolarea form-control" required/>				
		    			
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
				</div> -->
		    </div>
		    
		   </fieldset> 
