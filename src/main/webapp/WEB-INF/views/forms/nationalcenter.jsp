
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">NATIONAL CENTER INFORMATION</legend>
			<div class="form-row">
    			<div class="col-md-12 mb-3">
			 		<label  for="indicateGenus"><span>Name of BFAR National Center:</span></label>
			 		<input  type="text" name="name_of_center"  id="name_of_center"  class="name_of_center form-control" required/>
					<div class="valid-feedback">
        					ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
      				</div>	
      			
			

			</div>
			<div class="form-row">
    				
      			<div class="col-md-12 mb-3">	
					<label  for="description"><span>Center Description:</span></label>
					<textarea class="form-control description textarea" contenteditable="true" name="description" id="description" rows="10" style="height:100%;"></textarea>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>	
				</div>	
			

			</div>
			<div class="form-row">
    			<div class="col-md-6 mb-3">
			 		<label  for="indicateGenus"><span>Center Head:</span></label>
			 		<input  type="text" name="center_head"  id="center_head"  class="center_head form-control" required/>
					<div class="valid-feedback">
        					ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
      				</div>
      			<div class="col-md-6 mb-3">	
					<label  for="area"><span>Number of Personnel:</span></label>
					<input type="number" name="number_of_personnel" id="number_of_personnel"  class="number_of_personnel form-control" required/>																
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>	
				</div>	
				

			</div>
		
		</fieldset>
										
