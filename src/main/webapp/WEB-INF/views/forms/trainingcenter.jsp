
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISHERIES TRAINING CENTER INFORMATION</legend>
				<div class="form-row">
    			<div class="col-md-6 mb-3">
					<label  for="name"><span>Name of Training Center:</span></label> 
					<input type="text" name="name"  id="name"  class="name form-control" required/>
						<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Required
      							</div>
					</div>
					<div class="col-md-6 mb-3">
						<label  for="specialization"><span>Specialization:</span></label>	 
						<input type="text" name="specialization"  id="specialization"  class="specialization form-control" required/>
							<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        							Required
      						</div>
					</div>
					</div>
					<div class="form-row">
    				<div class="col-md-6 mb-3">
						<label  for="facilityWithinTheTrainingCenter"><span>Facility Within The Training Center</span></label>	
						<input type="text" name="facilityWithinTheTrainingCenter"  id="facilityWithinTheTrainingCenter"  class="facilityWithinTheTrainingCenter form-control" required/>																
							<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        							Required
      						</div>
      				</div>
      				<div class="col-md-6 mb-3">
					<label  for="type"><span>Type</span></label>		
					<select name="type"  id="type"  class="type form-control" required>
										<option value=""></option>
										<option value="National">National</option>
										<option value="Local">Local</option>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>
					</div>
			</fieldset>
			
