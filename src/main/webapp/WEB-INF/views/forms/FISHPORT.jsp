			
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH PORT INFORMATION</legend>
			<div class="form-row">
    				<div class="col-md-4 mb-3">	
			<label  for="nameOfFishport"><span>Name of Fish Port:</span></label> 
					<input type="text"  name="nameOfFishport"  id="nameOfFishport"  class="nameOfFishport form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
				<div class="col-md-4 mb-3">	
			<label  for="nameOfCaretaker"><span>Operated by:</span></label>	 
					<input type="text"  name="nameOfCaretaker"  id="nameOfCaretaker"  class="fishportnameOfCaretaker form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
				<div class="col-md-4 mb-3">	
			<label  for="type"><span>Type</span>	</label>			
				<select name="type"  id="type"  class="fishPortType form-control" required>
										<option value=""></option>
										<option value="PFDA">PFDA</option>
										<option value="Private">Private</option>
										<option value="Municipal">Municipal/City</option>
										<option value="National">National</option>
				</select>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			</div>
			<div class="form-row">
			<div class="col-md-4 mb-3">	
			<label  for="fishportclassification"><span>Classification</span>	</label>			
				<select name="classification"  id="classification"  class="classification form-control" required>
										<option value=""></option>
										<option value="Major">Major</option>
										<option value="Minor">Minor</option>
										<option value="Public">Public</option>
										<option value="Private">Private</option>
				</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>	
			</div>
<!-- 			<div class="col-md-4 mb-3">	
			<label  for="area"><span>Area</span></label>
			<input type="text" name="area"  id="area"  class="area form-control" required/>	
				<input type="number"  name="area" min="0" value="0" step="any" id="area"  class="fishportarea form-control" required/>																
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div> -->
			<div class="col-md-4� mb-3">	
			<label  for="volumeOfUnloading"><span>Volume of Unloading(MT)</span>	</label>
					<input type="text"  name="volumeOfUnloading"  id="volumeOfUnloading"  class="volumeOfUnloading form-control" required/>																
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			</div>
			</fieldset>
	