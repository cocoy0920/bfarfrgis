
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">PROVINCIAL FISHERIES OFFICE INFORMATION</legend>
			 <div class="form-row">
    			<div class="col-md-12 mb-3">
			 		<label  for="lguName"><span>PFO Name:</span></label>
			 		<input type="text" name="lguName"  id="lguName"  class="lguName form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
			</div>
			<div class="form-row">
    			<div class="col-md-6 mb-3">
					<label  for="noOfBarangayCoastal"><span>Number of Municipality Coastal:</span></label>
			 		<input type="number" name="noOfBarangayCoastal"  id="noOfBarangayCoastal"  class="noOfBarangayCoastal form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
				<div class="col-md-6 mb-3">
					<label  for="noOfBarangayInland"><span> Number of Municipality Inland:</span></label>
			 		<input type="number" name="noOfBarangayInland"  id="noOfBarangayInland"  class="noOfBarangayInland form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
			</div>
			<div class="form-row">
    			<div class="col-md-6 mb-3">
				<label  for="lguCoastalLength"><span>LGU Coastal Length(km.):</span></label>
			 	<input type="number" min="0" step="any"  name="lguCoastalLength"  id="lguCoastalLength"  class="lguCoastalLength form-control" required/>
				<div class="valid-feedback">
        				ok.
      			</div>
      			<div class="invalid-feedback">
        				Required
      			</div>
				</div>
			</div>
		</fieldset>
		
