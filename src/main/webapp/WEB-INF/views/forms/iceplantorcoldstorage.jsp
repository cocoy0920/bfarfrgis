
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ICE PLANT / COLD STORAGE INFORMATION</legend>
			<div class="form-row">
    			<div class="col-md-4 mb-3">
			 		<label  for="nameOfIcePlant"><span>Name of Ice Plant or Cold Storage:</span></label>
			 		<input type="text" name="nameOfIcePlant"  id="nameOfIcePlant"  class="nameOfIcePlant form-control" required/>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
				<div class="col-md-4 mb-3">
					<label  for="sanitaryPermit"><span>Valid Sanitary Permit:</span></label>
			 		<input type="text" name="sanitaryPermit"  id="sanitaryPermit"  class="sanitaryPermit form-control" required/>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
				<div class="col-md-4 mb-3">
					<label  for="operator"><span>Operator:</span></label>
			 		<select name="operator"  id="operator"  class="operator form-control" required>
										<option value="">- Please select - </option>
										<option value="BFAR">BFAR</option>
										<option value="COOPERATIVE">COOPERATIVE</option>
										<option value="PRIVATE">PRIVATE ORGANIZATION</option>
						</select>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
				</div>
				<div class="form-row">
    				<div class="col-md-4 mb-3">
						<label  for="typeOfFacility"><span>Type of Facility:</span>	</label>			
						<select name="typeOfFacility"  id="typeOfFacility"  class="typeOfFacility form-control" required>
										<option value="">- Please select -</option>
										<option value="IcePlant">Ice Plant</option>
										<option value="ColdStorage">Cold Storage</option>
										<option value="IcePlantColdStorage">Ice Plant and Cold Storage</option>
						</select>
						<div class="valid-feedback">
        						ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-4 mb-3">
						<label  for="structureType"><span>Structure Type:</span></label>				
						<select name="structureType"  id="structureType"  class="structureType form-control" required>
										<option value=""></option>
										<option value="Fixed Structure">Fixed Structure</option>
										<option value="Mobile">Mobile</option>
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-4 mb-3">
						<label  for="capacity"><span>Capacity:</span></label>
			 			<input type="text" name="capacity"  id="capacity"  class="capacity form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					</div>
					<div class="form-row">
    					<div class="col-md-2 mb-3">
							<label  for="withValidLicenseToOperate"><span>With Valid License to Operate:</span></label>	
							<select name="withValidLicenseToOperate"  id="withValidLicenseToOperate"  class="withValidLicenseToOperate form-control" required>
										<option value=""></option>
										<option value="Yes">Yes</option>
										<option value="No">No</option>
							</select>
							<div class="valid-feedback">
        						ok.
      						</div>
      						<div class="invalid-feedback">
        						Required
      						</div>
						</div>
						<div class="col-md-2 mb-3">
							<label  for="withValidSanitaryPermit"><span>With Valid Sanitary Permit</span></label>	
							<select name="withValidSanitaryPermit"  id="withValidSanitaryPermit"  class="withValidSanitaryPermit form-control" required>
										<option value=""></option>
										<option value="Yes">Yes</option>
										<option value="No">No</option>
							</select>
							<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        						Required
      						</div>
						</div>
						
							<div class="col-md-4 mb-3">	
							<div id="yesData" style="display:none;">						
								<label  for="withValidSanitaryPermitYes"><span>Type Sanitary Permit</span></label>
								<select name="withValidSanitaryPermitYes"  id="withValidSanitaryPermitYes"  class="withValidSanitaryPermitYes form-control">
										<option value=""></option>
										<option value="Marine">Marine</option>
										<option value="FishPond">Fish Pond</option>
										<option value="Others">Others</option>
								</select>
								<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Required
      							</div>
							</div>	
						</div>
						
								<div class="col-md-4 mb-3">
									<div id="othersData" style="display:none;">	
								<label  for="withValidSanitaryPermitOthers"><span>Others</span>	</label>					
								<input type="text" name="withValidSanitaryPermitOthers" id="withValidSanitaryPermitOthers" class="withValidSanitaryPermitOthers form-control"/>
								<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        						Required
      							</div>
							</div>	
						</div>	
						
						</div>
			<div class="form-row">
						<div class="col-md-4 mb-3">						
							<label  for="businessPermitsAndCertificateObtained"><span>Business Permits and Certificate Obtained:</span></label>
							 <input type="text" name="businessPermitsAndCertificateObtained"  id="businessPermitsAndCertificateObtained"  class="iceplantbusinessPermitsAndCertificateObtained form-control" required/>
								<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
						</div>
    			<div class="col-md-4 mb-3">
					<label  for="typeOfIceMaker"><span>Type of Ice Maker:</span></label>
			 		<input type="text" name="typeOfIceMaker"  id="typeOfIceMaker"  class="typeOfIceMaker form-control" required/>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
				</div>	
			<div class="form-row">	
				<div class="col-md-4 mb-3">				
					<label  for="foodGradule"><span>Food Gradule</span></label>	
					<select name="foodGradule"  id="foodGradule"  class="foodGradule form-control" required>
										<option value=""></option>
										<option value="Yes">Yes</option>
										<option value="No">No</option>
					</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
<!-- 				<div class="col-md-4 mb-3">	
					<label  for="area"><span>Area</span></label>
					<input type="text" name="area"  id="area"  class="area form-control" required/>	
					<input type="number" name="area"  min="0" value="0" step="any"   id="area"  class="icearea form-control" required/>																
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div> -->
		</div>
		</fieldset>
		
