
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH PROCESSING PLANT INFORMATION</legend>
			<div class="form-row">
			<div class="col-md-4 mb-3">
			<label  for="validationCustom04">Name of Processing Plant:</label>
					<input type="text" name="nameOfProcessingPlants"  id="validationCustom04"  class="nameOfProcessingPlants form-control" required/>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-4 mb-3">
				<label  for="validationCustom05">Name of Operator:</label>
					<input type="text" name="nameOfOperator"  id="validationCustom05"  class="nameOfOperator form-control " required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				
			</div>
<!-- 			<div class="col-md-4 mb-3">	
				<label  for="validationCustom06">Area</label>
				<input type="text" name="area"  id="area"  class="area form-control" required/>	
					<input type="number" name="area" min="0" value="0" step="any"  id="validationCustom06"  class="area form-control " required/>																
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div> -->
			</div>
			<div class="form-row">
			<div class="col-md-4 mb-3">
			<label  for="validationCustom07">Operator Classification</label>		
				<select name="operatorClassification"  id="validationCustom07"  class="operatorClassification form-control " required>
					<option value="">-- PLS SELECT --</option>
					<option value="BFAR_MANAGED">BFAR MANAGED</option>
					<option value="LGU_MANAGED">LGU MANAGED</option>
					<!-- <option value="PFDA">PFDA</option> -->
					<option value="PRIVATE_SECTOR">PRIVATE SECTOR</option>
					<option value="COOPERATIVE">COOPERATIVE</option>
					<!-- <option value="Business">Corporation</option>
					<option value="Government">Government</option>
					 -->
				</select>
				<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>	
			<div class="col-md-4 mb-3">
			<label  for="validationCustom08">Processing Technique</label>
					<input type="text" name="processingTechnique"  id="validationCustom08"  class="processingTechnique form-control " required/>																
				<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
			</div>
			<div class="col-md-4 mb-3">
			<label  for="processingEnvironmentClassification">Processing Environment Classification</label>
				<select name="processingEnvironmentClassification"  id="processingEnvironmentClassification"  class="processingEnvironmentClassification form-control " required>
										<option value=""></option>
										<option value="Backyard">Backyard</option>
										<option value="Plant">Plant</option>
				</select>
				<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>
		</div>
		
		<div class="form-row">
			<div class="col-md-4 mb-3">
			<div id="plantData" >
			<label  for="plantRegistered">BFAR Plant Registered</label>
				<select name="plantRegistered"  id="plantRegistered"  class="plantRegistered form-control ">
										<option value=""></option>
										<option value="YES">YES</option>
										<option value="NO">NO</option>
				</select>
			</div>	
			
		<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>
			<div class="col-md-4 mb-3">
			<label  for="bfarRegistered">BFAR Registered</label>		
				<select name="bfarRegistered"  id="bfarRegistered"  class="bfarRegistered form-control " required>
										<option value=""></option>
										<option value="YES">YES</option>
										<option value="NO">NO</option>
				</select>
				
			<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>	
			<div class="col-md-4 mb-3">	
			<label  for="packagingType">Packaging Type</label>	
				<select name="packagingType"  id="packagingType"  class="packagingType form-control " required>
					<option value=""></option>
					<option value="Vacuum Packed">Vacuum Packed</option>
					<option value="Sealed">Sealed</option>
					<option value="Canned">Canned</option>
					<option value="Bottling">Bottling</option>
					<option value="Others">Others</option>
				</select>			
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>
			</div>
			<div class="form-row">
			<div class="col-md-4 mb-3">
			<label  for="marketReach">Market Reach</label>		
				<select name="marketReach"  id="marketReach"  class="marketReach form-control " required>
											<option value=""></option>
											<option value="Local">Local</option>
											<option value="National">National</option>
											<option value="Export">Export</option>
											<option value="International">International</option>
											<option value="European Union">European Union</option>
				</select>
				<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>	
			<div class="col-md-4 mb-3">
			<label  for="indicateSpecies">Indicate Species:</label>
				<input type="text" name="indicateSpecies" id="indicateSpecies"  class="indicateSpecies form-control " required/>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>
			<div class="col-md-4 mb-3">
			<label  for="businessPermitsAndCertificateObtained">Business permits and Certificates Obtained</label>
				<input type="text" name="businessPermitsAndCertificateObtained" id="businessPermitsAndCertificateObtained"  class="businessPermitsAndCertificateObtained form-control " required/>
				<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>
			</div>	
					
			</fieldset>
