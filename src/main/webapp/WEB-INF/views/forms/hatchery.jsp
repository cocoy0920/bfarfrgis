
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">HATCHERY INFORMATION</legend>
			 <div class="form-row">
    				<div class="col-md-4 mb-3">
			 			<label  for="nameOfHatchery"><span>Name of Hatchery:</span></label>
			 			<input type="text" name="nameOfHatchery"  id="nameOfHatchery"  class="nameOfHatchery form-control"/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-4 mb-3">
						<label  for="nameOfOperator"><span>Name of Operator:</span></label>
			 			<input type="text" name="nameOfOperator"  id="nameOfOperator"  class="nameOfOperator form-control"/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
<!-- 					<div class="col-md-4 mb-3">	
						<label  for="area"><span>Area</span></label>
						<input type="text" name="area"  id="area"  class="area form-control" required/>	
						<input type="number" name="area"  id="area"  min="0" value="0" step="any"   class="hatcheryarea form-control"/>																
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div> -->
			</div>
			<div class="form-row">
    				<div class="col-md-12 mb-3">
						<label  for="addressOfOperator"><span>Address of Operator:</span></label>
			 			<input type="text" name="addressOfOperator"  id="addressOfOperator"  class="addressOfOperator form-control" />
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
			</div>
			<div class="form-row">
    				<div class="col-md-6 mb-3">			
						<label  for="operatorClassification"><span>Operator Classification:</span></label>				
						<select name="operatorClassification"  id="operatorClassification"  class="operatorClassification form-control">
										<option value="-- PLS SELECT --"></option>										
										<option value="BFAR">Bureau of Fisheries and Aquatic Resources</option>
										<option value="LGU">Local Government Unit (LGU)</option>
										<option value="Private">Private Sector</option>
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>	

			</div>
			<div class="form-row" id="bfar_id" style="display: none">
    				<div class="col-md-6 mb-3">
						<label  for="legislative"><span>Legislative</span></label>	
						<select name="legislated"  id="legislated"  class="legislated form-control">
										<option value=""></option>
										<option value="legislated">Legislated</option>
										<option value="nonLegislated">Non-legislated</option>
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-6 mb-3">
						<label  for="legislative"><span>Status</span></label>	
						<select name="status"  id="status"  class="status form-control">
										<option value=""></option>
										<option value="Completed">Completed</option>
										<option value="OngoingConstruction">Ongoing Construction</option>
										<option value="PendingImplementation">Pending Implementation</option>
										
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
		</div>
			<div class="form-row">
    				<div class="col-md-6 mb-3">			
						<label  for="indicateSpecies"><span>Indicate Species:</span></label>
			 			<input type="text" name="indicateSpecies"  id="indicateSpecies"  class="hatcheryindicateSpecies form-control"/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>

			</div>			
		<fieldset><legend>Production</legend>
		<div class="form-row">
    				<div class="col-md-4 mb-3">
						<label  for="prodStocking"><span>Stocking Density/Cycle:</span></label>
			 			<input type="text" name="prodStocking"  id="prodStocking"  class="prodStocking form-control"/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>	
					<div class="col-md-4 mb-3">
						<label  for="prodCycle"><span>Cycle/Year:</span></label>
			 			<input type="text" name="prodCycle"  id="prodCycle"  class="prodCycle form-control"/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-4 mb-3">	
						<label  for="actualProdForTheYear"><span>Actual Production for the year:</span></label>
			 			<input type="text" name="actualProdForTheYear"  id="actualProdForTheYear"  class="actualProdForTheYear form-control" />
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
		</div>			
		</fieldset>
	<div class="form-row">
    				<div class="col-md-6 mb-3">
						<label  for="legislative"><span>RA number</span></label>	
						<input type="text" name="ra"  id="ra"  class="ra form-control">
					
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-6 mb-3">
						<label  for="legislative"><span>Title</span></label>	
						<input type="text" name="title"  id="title"  class="title form-control">
						
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
		</div>
		</fieldset>
	