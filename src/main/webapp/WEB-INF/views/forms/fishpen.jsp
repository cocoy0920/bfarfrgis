
			<fieldset class="form-group border border-dark rounded p-4"><legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH PEN INFORMATION</legend>
			<div class="form-row">
    			<div class="col-md-6 mb-3">
			 <label  for="nameOfOperator"><span>Name of Operator:</span></label>
			 <input type="text" name="nameOfOperator"  id="nameOfOperator"  class="fishPenNameOfOperator form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

			</div>
			<div class="form-row">
    			<div class="col-md-6 mb-3">
			<label  for="noOfFishPen"><span>No. of Fish Pen:</span></label>
					<input type="text" name="noOfFishPen" id="noOfFishPen"  class="noOfFishPen form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-6 mb-3">
			<label  for="speciesCultured"><span>Species Cultured:</span></label>
					<input type="text" name="speciesCultured" id="speciesCultured"  class="speciesCultured form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			</div>

		</fieldset>
	