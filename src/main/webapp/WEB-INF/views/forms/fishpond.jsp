
			<fieldset  class="form-group border border-dark rounded p-4">
			<legend  class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH POND INFORMATION</legend>
			 <div class="form-row">
    			<div class="col-md-6 mb-3">	
			 		<label  for="nameoffishpondoperator">Name of Fishpond Operator:</label>
			 		<input type="text" name="nameoffishpondoperator"  id="nameoffishpondoperator"  class="form-control input-group-lg nameoffishpondoperator"/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>
				<div class="col-md-6 mb-3">	
			 		<label  for="nameofactualoccupant">Name of Actual Occupant:</label>
			 		<input type="text" name="nameofactualoccupant"  id="nameofactualoccupant"  class="form-control input-group-lg nameofactualoccupant"/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>
				<div class="col-md-6 mb-3">		
				<label  for="fishpondtype">Fish Pond Type</label>		
				<select name="fishpondtype"  id="fishpondtype"  class="fishpondtype form-control input-group-lg">
										<option value=""></option>
										<option value="FLA">FLA</option>
										<option value="Private">Private</option>
										<option value="Backyard">Backyard</option>
				</select>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>
			<div class="col-md-6 mb-3">	
			<label  for="status">FishPond Status</label>				
				<select name="status"  id="status"  class="status form-control input-group-lg">
					<option value=""></option>
					<option value="Active">Active</option>
					<option value="Non-Active">Non-Active</option>
				</select>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>
		</div>
		<div class="form-row">
		<div id="nonActiveData" style="display:none;">
		<div class="col-md-6 mb-12">	
		<label  for="nonactive">if Non-Active</label>	
			<select name="nonactive"  id="nonactive"  class="nonactive form-control input-group-lg">
										<option value=""></option>
										<!-- <option value="Developed">Developed</option> -->
										<option value="Underdeveloped">Under Developed</option>
										<option value="Abandoned">Abandoned</option>
				</select>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>		
		</div>
		</div>
		<div class="form-row">
		<div class="col-md-6 mb-6">
		<label   for="speciescultured">Species Cultured:</label>
			 <input type="text" name="speciescultured"  id="speciescultured"  class="speciescultured form-control input-group-lg"/>
		<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div>

		</div>
		<div class="form-row">
		
		<div class="col-md-6 mb-6">	
		<label   for="kind">Kind</label>				
				<select name="kind"  id="kind"  class="kind form-control input-group-lg">
										<option value=""></option>
										<option value="Natural">Natural</option>
										<option value="Commercial">Commercial</option>
				</select>
		<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div>		
		<div class="col-md-6 mb-6">		
		<label   for="hatchery">Hatchery</label>				
				<select name="hatchery"  id="hatchery"  class="hatchery form-control input-group-lg">
										<option value=""></option>
										<option value="Nursery">Nursery</option>
										<option value="Grow-Out">Grow-Out</option>
				</select>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div>
		</div>		

		</fieldset>
		
