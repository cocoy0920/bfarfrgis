
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">REGIONAL OFFICE INFORMATION</legend>
			
			<div class="form-row">
    			<div class="col-md-6 mb-3">
			 		<label  for="indicateGenus"><span>Director/Head:</span></label>
			 		<input  type="text" name="head"  id="head"  class="head form-control" required/>
					<div class="valid-feedback">
        					ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
      				</div>
      			<div class="col-md-6 mb-3">	
					<label  for="area"><span>Number of Personnel:</span></label>
					<input type="number" name="number_of_personnel" id="number_of_personnel"  class="number_of_personnel form-control" required/>																
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>	
				</div>	
				

			</div>
		
		</fieldset>
										
