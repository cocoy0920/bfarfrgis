
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">SEAWEEDS INFORMATION</legend>
			<div class="form-row">
			<div class="col-md-4 mb-3">
			 <label  for="type"><span>Type:</span></label>
			 <select name="type"  id="type"  class="type form-control " required>
					<option value="">-- PLS SELECT --</option>
					<option value="SEAWEEDLABORATORY">SEAWEED LABORATORY</option>
					<option value="SEAWEEDNURSERY">SEAWEED NURSERY</option>
					<option value="SEAWEEDWAREHOUSE">SEAWEED WAREHOUSE</option>
					<option value="SEAWEEDDRYER">SEAWEED DRYER</option>
					<!-- <option value="Business">Corporation</option>
					<option value="Government">Government</option>
					 -->
				</select>
					<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Required
      							</div>
					</div>
    			<div class="col-md-4 mb-3">
			 <label  for="indicateGenus"><span>Indicate Genus:</span></label>
			 <input type="text" name="indicateGenus"  id="indicateGenus"  class="indicateGenus form-control" required/>
					<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Required
      							</div>
					</div>
					<div class="col-md-4 mb-3">
				<label  for="culturedMethodUsed"><span>Cultured Method Used:</span></label>			
					<input type="text" name="culturedMethodUsed"  id="culturedMethodUsed"  class="culturedMethodUsed form-control" required/>
					<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Required
      							</div>
					</div>	
			</div>
			<div class="form-row">
    			<div class="col-md-6 mb-3">
					<label  for="culturedPeriod"><span>Cultured Period:</span></label>
					<input type="text" name="culturedPeriod" id="culturedPeriod" class="culturedPeriod form-control" required/>
					<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Required
      							</div>
					</div>
<!-- 					<div class="col-md-6 mb-3">
						<label  for="area"><span>Area</span></label>
						<input type="text" name="area"  id="area"  class="area form-control" required/>	
						<input type="number"  name="area"  min="0" value="0" step="any"  id="area"  class="seaweedsarea form-control" required/>																
						<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Required
      							</div>
					</div> -->
			</div>
			<div class="form-row">
    			<div class="col-md-6 mb-3">
					<label  for="productionKgPerCropping"><span>Volume per Cropping (Kgs.):</span></label>
			 		<input type="text" name="productionKgPerCropping"  id="productionKgPerCropping"  class="productionKgPerCropping form-control" required/>
					<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Required
      							</div>
					</div>
				<div class="col-md-6 mb-3">
					<label  for="productionNoOfCroppingPerYear"><span>No. of Cropping per Year:</span></label>
			 		<input type="text" name="productionNoOfCroppingPerYear"  id="productionNoOfCroppingPerYear"  class="productionNoOfCroppingPerYear form-control" required/>
						<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Required
      							</div>
					</div>
					</div>
		</fieldset>
										
