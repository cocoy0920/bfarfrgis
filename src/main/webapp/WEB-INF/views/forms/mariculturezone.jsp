
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">MARICULTURE ZONE / PARK INFORMATION</legend>
			<div class="form-row">
    				<div class="col-md-6 mb-3">
			 <label  for="nameOfMariculture"><span>Name of Mariculture:</span></label>
			 <input type="text" name="nameOfMariculture"  id="nameOfMariculture"  class="nameOfMariculture form-control" required/>
			<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
      				</div>
<!--       				<div class="col-md-6 mb-3">
			<label  for="area"><span>Area</span></label>
			<input type="text" name="area"  id="area"  class="area form-control" required/>	
			<input type="number" name="area"  id="area"  min="0" value="0" step="any"  class="mariculturearea form-control" required/>																
			<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      				</div> -->
			</div>
			<div class="form-row">
    			<div class="col-md-6 mb-3">
					<label  for="dateLaunched"><span>Date Launched:</span></label>
					<input type="date" name="dateLaunched" id="dateLaunched" class="dateLaunched form-control" required>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
      			</div>
      			<div class="col-md-6 mb-3">
					<label  for="dateInacted"><span>Date Enacted:</span></label>
						<input type="date" name="dateInacted" id="dateInacted" class="dateInacted form-control" required>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      			</div>
      		</div>
      		<div class="form-row">
    			<div class="col-md-6 mb-3">
						<label  for="eCcNumber"><span>eCcNumber:</span></label>
			 			<input type="text" name="eccNumber"  id="eccNumber"  class="eccNumber form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
      			</div>
      			<div class="col-md-6 mb-3">
				
						<label  for="maricultureType"><span>Mariculture Manage</span></label>				
						<select name="maricultureType"  id="maricultureType"  class="maricultureType form-control" required>
										<option value="-- PLS SELECT --"></option>
										<option value="BFAR">BFAR MANAGE</option>
										<option value="PRIVATE">PRIVATE SECTOR MANAGE</option>
										<option value="LGU">LGU MANAGE</option>
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      			</div>
      		</div>
			<div class="form-row">
    				<div class="col-md-6 mb-3">
			 			<label  for="municipalOrdinanceNo"><span>Municipal Ordinance Number:</span></label>
			 			<input type="text" name="municipalOrdinanceNo"  id="municipalOrdinanceNo"  class="municipalOrdinanceNo form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
      				</div>
      				<div class="col-md-6 mb-3">
						<label  for="dateApproved"><span>Date Approved:</span></label>
						<input type="date" name="dateApproved" id="dateApproved" class="dateApproved dateicon form-control" required>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      				</div>	
			</div>
		</fieldset>
