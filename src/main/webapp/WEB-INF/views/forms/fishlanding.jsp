			
			<fieldset class="form-group border border-dark rounded p-4"><legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH LANDING INFORMATION</legend>
			<div class="form-row">
    		<div class="col-md-4 mb-3">		
			<label  for="nameOfLanding"><span>Name of Fish Landing</span></label>
				<input type="text" name="nameOfLanding" id="nameOfLanding" class="nameOfLanding form-control" required/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
<!-- 			<div class="col-md-4 mb-3">
			<label  for="area"><span>Area</span></label>
			<input type="text" name="area"  id="area"  class="area form-control" required/>	
					<input type="number" name="area"  id="area"  class="area form-control" required/>																
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div> -->
			<div class="col-md-4 mb-3">
			<label  for="volumeOfUnloadingMT"><span>Volume of Unloading(MT)</span></label>
			 <input type="text" name="volumeOfUnloadingMT" id="volumeOfUnloadingMT" class="volumeOfUnloadingMT form-control" required/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			</div>
			<div class="form-row">
    		<div class="col-md-6 mb-3">	
			<label  for="classification"><span>Classification</span></label>
			<select name="classification"  id="classification"  class="classification form-control" required>
											<option value=""></option>
											<option value="Minor">Minor</option>
											<option value="Major">Major</option>
				</select>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-6 mb-3">	
			<label  for="type"><span>Type</span></label>
			<select name="type"  id="type"  class="fishLandingType form-control" required>
											<option value="">-- Please select --</option>											
											<option value="CFLC">CFLC</option>
											<option value="NonTraditional">Non-Traditional</option>
											<option value="Traditional">Traditional</option>
											
				</select>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
				</div>
				<div class="form-row">
				
				<div class="col-md-6 mb-3">	
			
				</div>
				<div class="col-md-6 mb-3"  id="cflc_status">	
				<label  for="type"><span>CFLC Status</span></label>
				<select name="cflc_status"  id="cflcStatus"  class="cflcStatus form-control" required>
											<option value="">- Pls select - </option>
											<option value="Operational">Operational</option>
											<option value="ForOperation">For Operation</option>
											<option value="ForCompletion">For Completion</option>
											<option value="ForTransfer">For Transfer</option>
											<option value="Damaged">Damaged</option>
											
				</select>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
				</div>
				</fieldset>
