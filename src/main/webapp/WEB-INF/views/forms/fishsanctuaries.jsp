<fieldset class="form-group border border-dark rounded p-4">
		<legend  class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH SANCTUARY INFORMATION</legend>	
		<div class="form-row">
    			<div class="col-md-6 mb-3">	
			 <label  for="validationCustom04">Name of Sanctuary/Protected Area</label>
			 <input type="text" name="nameOfFishSanctuary" id="validationCustom04"  class="form-control input-group-lg nameOfFishSanctuary" required/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>
					

    			<div class="col-md-6 mb-3">			
				<label  for="validationCustom05">Implementer</label>				
				<select name="bfardenr"  id="validationCustom05"  class="form-control bfardenr" required>
									<!-- 	<option value=""></option>
										<option value="BFAR">BFAR</option>
										<option value="DENR">DENR</option>
										<option value="LGU">LGU</option>
										<option value="NGO">NGO</option>
										<option value="SCHOOL">SCHOOL</option> -->
				</select>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>

						</div>	
						
			<div id="bfarData" style="display:none;">
				<div class="form-row">
    				<div class="col-md-6 mb-3">	
						<label  for="sheltered">Name of Sheltered Fisheries/Species:</label>
			 			<input type="text" name="sheltered"  id="sheltered"  class="sheltered form-control form-control-sm"/>				
					</div>
				</div>
			</div>
		
		<div id="denrData" style="display:none;">
		<div class="form-row">
    			<div class="col-md-6 mb-3">	
			<label  for="nameOfSensitiveHabitatMPA">Name of Sensitive Habitat with MPA:</label>	
			<input type="text" name="nameOfSensitiveHabitatMPA"  id="nameOfSensitiveHabitatMPA"  class="nameOfSensitiveHabitatMPA form-control"/>
			</div>
			<div class="col-md-6 mb-3">	
			<label  for="OrdinanceNo">Ordinance No.</label>					
			<input type="text" name="ordinanceNo" id="ordinanceNo" class="ordinanceNo form-control"/>
			</div>
			</div>
			<div class="form-row">
    			<div class="col-md-6 mb-3">	
			<label for="OrdinanceTitle">Ordinance Title</label>
			<input type="text" name="ordinanceTitle" id="ordinanceTitle"  class="ordinanceTitle form-control"/>
			</div>
			<div class="col-md-6 mb-3">	
			<label for="DateEstablished">Date Established</label>
			<input type="date" name="dateEstablished" id="dateEstablished" class="dateEstablished form-control "/>
			</div>			
		</div>
		</div>
		
		
		<div class="form-row">
<!--     			<div class="col-md-6 mb-3">	
					<label  for="validationCustom06">Area</label>
					<input type="text" name="area"  id="area"  class="area form-control" required/>	
					<input type="number" name="area"  id="validationCustom06"  class="form-control area" required/>																
					<div class="valid-feedback">
        				ok.<i class="fas fa-check-circle"></i>
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div> -->


    			<div class="col-md-6 mb-3">	
					<label  for="validationCustom07">Type</label>	
					<select name="type"  id="validationCustom07"  class="form-control type" required>
										<option value="">Select</option>
										<option value="National">National</option>
										<option value="Local">Local</option>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>

			</div>
</fieldset>