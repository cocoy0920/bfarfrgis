
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">MARKET INFORMATION</legend>
			
			<div class="form-row">
    			<div class="col-md-4 mb-3">
					<label  for="nameOfMarket"><span>Name of Market:</span></label>
					<input type="text" name="nameOfMarket" id="nameOfMarket" class="nameOfMarket form-control" required/>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
				<div class="col-md-4 mb-3">
					<label  for="publicprivate"><span>Type:</span></label>
					<select name="publicprivate"  id="publicprivate" class="publicprivate form-control" required>
						<option value=""></option>
						<option value="Public">Public</option>
						<option value="Private">Private</option>
					</select>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
				<div class="col-md-4 mb-3">
            	<label  for="majorMinorMarket"><span>Classification of Market:</span></label>
            	<select name="majorMinorMarket"  id="majorMinorMarket" class="majorMinorMarket form-control" required>
				<option value=""></option>
				<option value="Major">Major</option>
				<option value="Minor">Minor</option>
				</select>
					<div class="valid-feedback">
        					ok.
      				</div>
      				<div class="invalid-feedback">
        					Required
      				</div>
				</div>
			</div>
			<div class="form-row">
    			<div class="col-md-6 mb-3">
            		<label  for="volumeTraded"><span>Volume Traded:</span></label>
           			 <input type="text" name="volumeTraded" id="volumeTraded" class="volumeTraded form-control" required/>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
<!-- 				<div class="col-md-6 mb-3">
					<label  for="area"><span>Area</span></label>	
					<input type="text" name="area"  id="area"  class="area form-control" required/>	
					<input type="number" name="area"  min="0" value="0" step="any"  id="area"  class="marketarea form-control" required/>																
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>	    
		    	</div> -->
		    </div>			
			</fieldset>
	