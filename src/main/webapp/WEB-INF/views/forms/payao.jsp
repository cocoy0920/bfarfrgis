
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">PAYAO INFORMATION</legend>
			<div class="row">
    			<div class="col-md-4 mb-3">
			 <label  for="association"><span>Association:</span></label>
			 <input type="text" name="association"  id="association"  class="association form-control" required/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-4 mb-3">
				<label  for="beneficiary"><span>Beneficiary</span></label>				
				<input type="text" name="beneficiary"  id="beneficiary"  class="beneficiary form-control" required>
				
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-4 mb-3">
		<label  for="payao"><span>Payao:</span></label>
					<input type="number" name="payao"  id="payao"  class="payao form-control" required/>
		<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>	
		</div>
		</div>	

		</fieldset>
	