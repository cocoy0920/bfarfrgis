<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>BFAR-FRGIS</title>
<meta charset="utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />

<jsp:include page="../menu/css.jsp"></jsp:include>
	<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
	<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
  <script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
	<script src="/static/leaflet/bundle.js"></script>

</head>
<body>
 <jsp:include page="../home/header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">
  <!-- Sidebar Holder -->
<%-- <jsp:include page="../menu/sidebar2.jsp"></jsp:include> --%>

  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5 section-to-print">	 
              	<div id="map"></div>

    </div>

			</div>


	<div id="footer">
		<jsp:include page="../users/footer.jsp"></jsp:include>
	</div>

	<script  src="/static/js/resourcesOnMapByRegion.js" ></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
        $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
        
        $("#pageHatchery").click(function(){
        	 $('#sidebar').toggleClass('active');
        });
        
	 
/*    		 var resources = [];
   		  var pages="";
   		
        $(".btn").click(function(event) {
        	 document.getElementById("btn").disabled = true;
            event.preventDefault();
            event.stopPropagation();

      	  var markedCheckbox = document.getElementsByName('page');  
            for (var checkbox of markedCheckbox) {  
              if (checkbox.checked)  

          	   resources.push(checkbox.value);
              
            } 

           $.ajax({
                    type : "GET",
                   url : '/home/resources-get-by-pages-home',
                    data : {resources: resources},
                   success : function(data) {
                  	 		console.log("success --> home ");
                        if(data){
                            }else{
                        alert("failed to upload");

                       }
                    }
           });

   	  });
        
 */

}); 	    
        </script>
    </body>
</html>