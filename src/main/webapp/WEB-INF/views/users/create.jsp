<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="en">
<head>
<title>Create</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
<script src="/static/js/form/deleteMapList.js" type=" text/javascript"></script>
<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
<script src="/static/leaflet/leaflet.js"></script>
<script src="/static/leaflet/leaflet.ajax.min.js"></script>
<script src="//d3js.org/d3.v3.min.js"></script>
<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
<script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
<script src="/static/leaflet/bundle.js"></script>
<link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
<script src="/static/leaflet/L.Control.SlideMenu.js"></script>
<script src="/static/leaflet/leaflet-bootstrapmodal.js"></script>
<script src="https://cdn.jsdelivr.net/npm/exif-js"></script>
<style type="text/css">
.table2excel{
	padding: 5px;
}
#map{ 
	width:100%;
  	height: 600px;
   background: #009dc4;
}

</style>
</head>
<body>

  <jsp:include page="../home/header_users.jsp"></jsp:include>
  <nav id="sidebar">
<!-- <div class="custom-menu">
<button type="button" id="sidebarCollapse" class="btn btn-primary">
<i class="fa fa-bars"></i>
<span class="sr-only">Toggle Menu</span>
</button>
</div> -->
<!-- <div class="p-4"> -->
<!-- <div class="sidebar-header">
      <img class=" img img-fluid" alt="" src="/static/images/FRGIS-LOGO.png">
    </div> -->
<ul class="list-unstyled components mb-5">

           <li><a class="dropdown-item" href="#" onclick="GFG_Fun(); return false;"><img src="/static/images/iconCircle/fishsanctuary.png" style="width: 20px; height: 20px;"/>  Fish Sanctuary</a></li>
          <li><a class="dropdown-item" href="/admin/fishprocessingExcel"><img src="/static/images/iconCircle/fishprocessing.png" style="width: 20px; height: 20px;"/> Fish Processing</a></li>
          <li><a class="dropdown-item" href="/admin/fishlandingExcel"><img src="/static/images/iconCircle/fish-landing.png" style="width: 20px; height: 20px;"/> Fish Landing</a></li>
          <li><a class="dropdown-item" href="/admin/fishportExcel"><img src="/static/images/iconCircle/fishport.png" style="width: 20px; height: 20px;"/> Fish Port</a></li>
          <li><a class="dropdown-item" href="/admin/fishpenExcel"><img src="/static/images/iconCircle/fishpen.png" style="width: 20px; height: 20px;"/>Fish Pen</a></li>
          <li><a class="dropdown-item" href="/admin/fishcageExcel"><img src="/static/images/iconCircle/fishcage.png" style="width: 20px; height: 20px;"/> Fish Cage</a></li>
          <li><a class="dropdown-item" href="/admin/hatcheryExcel"><img src="/static/images/iconCircle/hatcheries.png" style="width: 20px; height: 20px;"/>Hatchery</a></li>
          <li><a class="dropdown-item" href="/admin/ipcsExcel"><img src="/static/images/iconCircle/iceplant.png" style="width: 20px; height: 20px;"/>Cold Storage</a></li>
          <li><a class="dropdown-item" href="/admin/marketExcel"><img src="/static/images/iconCircle/market.png" style="width: 20px; height: 20px;"/>Market</a></li>
          <li><a class="dropdown-item" href="/admin/schooloffisheriesExcel"><img src="/static/images/iconCircle/schoolof-fisheries.png" style="width: 20px; height: 20px;"/>School of Fisheries</a></li>
          <li><a class="dropdown-item" href="/admin/fishcoralExcel"><img src="/static/images/iconCircle/fishcorral.png" style="width: 20px; height: 20px;"/>Fish Corral(Baklad)</a></li>
          <li><a class="dropdown-item" href="/admin/seagrassExcel"><img src="/static/images/iconCircle/seagrass.png" style="width: 20px; height: 20px;"/>Seagrass</a></li>
          <li><a class="dropdown-item" href="/admin/seaweedsExcel"><img src="/static/images/iconCircle/seaweeds.png" style="width: 20px; height: 20px;"/>Seaweeds</a></li>
          <li><a class="dropdown-item" href="/admin/mangroveExcel"><img src="/static/images/iconCircle/mangrove.png" style="width: 20px; height: 20px;"/>Mangrove</a></li>
          <li><a class="dropdown-item" href="/admin/mariculturezoneExcel"> <img src="/static/images/iconCircle/mariculturezone.png" style="width: 20px; height: 20px;"/>Mariculture Zone</a></li>
          <li><a class="dropdown-item" href="/admin/lguExcel"><img src="/static/images/iconCircle/lgu.png" style="width: 20px; height: 20px;"/>PFO</a></li>
          <li><a class="dropdown-item" href="/admin/trainingcenterExcel"><img src="/static/images/iconCircle/fisheriestraining.png" style="width: 20px; height: 20px;"/>Training Center</a></li>
	
          <sec:authorize access="isAuthenticated()">
       <li><a href="/logout"><i class="fas fa-sign-out-alt"></i>LOGOUT</a></li>
      </sec:authorize>

</ul>
</nav>
<div class="wrapper d-flex align-items-stretch text-dark">

    <div id="content" class="p-4 p-md-5 pt-5 section-to-print" >
    <c:if test="${mapsData != '' }">
    <div id="mapview">
    <div id="map"></div>
    </div>
    </c:if>
    <c:if test="${mapsData == '' }">
    <div id="mapview"  style="display:none;">
    <div id="map"></div>
    </div>
    </c:if>
    	<div class="form_input" style="display: none">
				
				<div id="formOne" style="background-color: black;width: 400px;"></div>
	 			
	 	</div>		
</div>
<jsp:include page="../menu/fma_details.jsp"></jsp:include>

    </div>
   <div id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</div> 
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>		
<script src="/static/js/form/provinceMunBarangay.js"></script>
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script  src="/static/js/resource.js" ></script>
<script  src="/static/js/api/regionMap.js" ></script>
<script src="/static/js/map/philippineMap.js"></script>
<script  src="/static/js/resourcesOnMapByResources.js" ></script>
<script  src="/static/js/jpegmeta.js" ></script>
<script  src="/static/js/listform/forms.js" ></script>
    </body>
</html>