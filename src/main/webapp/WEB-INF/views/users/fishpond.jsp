<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="en">
<head>
<%-- <meta name="_csrf" content="${_csrf.token}"/>
<meta name="_csrf_header" content="${_csrf.headerName}"/> --%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">	
<link rel="icon" type="image/png" href="<c:url value='/static/images/favicon.png' />" />
      <title>FISH POND</title>
<jsp:include page="../menu/css.jsp"></jsp:include>
<script src="/static/js/form/deleteMapList.js"></script>
<script src="/static/js/home/fisheries.js"></script>
<script src="/static/leaflet/leaflet.js"></script>
<script src="/static/leaflet/leaflet.ajax.min.js"></script>
<script src="//d3js.org/d3.v3.min.js"></script>
<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
<script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
<script src="/static/leaflet/bundle.js"></script>
<link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
<script src="/static/leaflet/L.Control.SlideMenu.js"></script>
<script src="/static/leaflet/leaflet-bootstrapmodal.js"></script>
<script src="https://cdn.jsdelivr.net/npm/exif-js"></script>
<style type="text/css">
.table2excel{
	padding: 5px;
}
#map{ 
	width:100%;
  	height: 600px;
   background: #009dc4;
}
</style>
</head>
<body onload="getFisheriesData()">

  <jsp:include page="../home/header_users.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">

    <div id="content" class="p-4 p-md-5 pt-5 section-to-print" >
    <c:if test="${mapsData != '' }">
    <div id="mapview">
    <div id="map"></div>
    </div>
    </c:if>
    <c:if test="${mapsData == '' }">
    <div id="mapview"  style="display:none;">
    <div id="map"></div>
    </div>
    </c:if>
    <h1 align="center">FISH POND</h1>
	<div class="form_input" style="display: none">
				<form class="needs-validation" id="myform" novalidate>
	 			
				<input type="hidden" name="id" id="id" class="id"/>
				<input type="hidden" name="user" id="user" class="user" value="${username}"/>
				<input type="hidden" name="uniqueKey" id="uniqueKey" class="uniqueKey"/>
				<input type="hidden" name="dateencoded" id="dateencoded" class="dateencoded"/>
				<input type="hidden" name="dateedited" id="dateedited" class="dateedited" />
				<input type="hidden" name="encodedby" id="encodedby" class="encodedby"/>
				<input type="hidden" name="editedby" id="editedby" class="editedby" />
				<input type="hidden" name="region" id="region"  class="region" value="${region}"/>	
				<input type="hidden" name="region_id" id="region_id"  class="region_id" value="${region_id}"/>	
				<br>
		
			<fieldset class="form-group border border-dark rounded p-4">
			<legend  class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH POND LOCATION</legend>
				<div class="form-row">	
				<div class="col-md-4 mb-3">
				<label   for="province">Province</label>
				<select name="province"  id="province" class="province form-control" required></select>															
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid province.
      				</div>
					</div>	
					<div class="col-md-4 mb-3">	
						<label   for="municipality">Municipality</label>		
				  		<select name="municipality" id="municipality"  class="municipality form-control" required></select>	
				 			<div class="valid-feedback">
        						ok.
      						</div>
      						<div class="invalid-feedback">
        						Please select a valid Municipality/City 
      						</div>
					</div>
					<div class="col-md-4 mb-3">													
					<label   for="barangay">Barangay</label>
					<select name="barangay"  id="barangay"  class="barangay form-control" required>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Barangay
      				</div>
					</div>
				</div>
				<div class="form-row">
    			<div class="col-md-12 mb-3">
					<label   for="code">Code</label>
					<input type="text" name="code" id="code"  class="code form-control" placeholder="Code" readonly="readonly"/>															
				</div>
			</div>
			</fieldset>
			
			<fieldset  class="form-group border border-dark rounded p-4">
			<legend  class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH POND INFORMATION</legend>
			 <div class="form-row">
    			<div class="col-md-6 mb-3">	
			 		<label  for="nameoffishpondoperator">Name of Fishpond Operator:</label>
			 		<input type="text" name="nameoffishpondoperator"  id="nameoffishpondoperator"  class="form-control input-group-lg nameoffishpondoperator"/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>
				<div class="col-md-6 mb-3">	
			 		<label  for="nameofactualoccupant">Name of Actual Occupant:</label>
			 		<input type="text" name="nameofactualoccupant"  id="nameofactualoccupant"  class="form-control input-group-lg nameofactualoccupant"/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>
				<div class="col-md-6 mb-3">		
				<label  for="fishpondtype">Fish Pond Type</label>		
				<select name="fishpondtype"  id="fishpondtype"  class="fishpondtype form-control input-group-lg">
										<option value=""></option>
										<option value="FLA">FLA</option>
										<option value="Private">Private</option>
										<option value="Backyard">Backyard</option>
				</select>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>
<!-- 			<div class="col-md-6 mb-3">	
			<div id="FLAData" style="display:none;">
				<label for="yearissued"><span>Year Issued:</span>
					<input type="date" name="yearissued" id="yearissued" class="yearissued input-field">
					<i class="fas fa-check-circle"></i>
					<i class="fas fa-exclamation-circle"></i>
					<br>
					<small></small>
				</label>	
				<label for="yearexpired"><span>Year Expired:</span>
					<input type="date" name="yearexpired" id="yearexpired" class="yearexpired input-field">
					<i class="fas fa-check-circle"></i>
					<i class="fas fa-exclamation-circle"></i>
					<br>
					<small></small>
				</label>
			</div> -->
			<div class="col-md-6 mb-3">	
			<label  for="status">FishPond Status</label>				
				<select name="status"  id="status"  class="status form-control input-group-lg">
					<option value=""></option>
					<option value="Active">Active</option>
					<option value="Non-Active">Non-Active</option>
				</select>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>
		</div>
		<div class="form-row">
		<div id="nonActiveData" style="display:none;">
		<div class="col-md-6 mb-12">	
		<label  for="nonactive">if Non-Active</label>	
			<select name="nonactive"  id="nonactive"  class="nonactive form-control input-group-lg">
										<option value=""></option>
										<!-- <option value="Developed">Developed</option> -->
										<option value="Underdeveloped">Under Developed</option>
										<option value="Abandoned">Abandoned</option>
				</select>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>		
		</div>
		</div>
		<div class="form-row">
		<div class="col-md-6 mb-6">
			<label  for="area">Area</label>
			<input type="text" name="area"  id="area"  class="area form-control" required/>	
				<!-- <input type="number" name="area"  id="area"  class="area form-control input-group-lg"/>		 -->														
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div>
		<div class="col-md-6 mb-6">
		<label   for="speciescultured">Species Cultured:</label>
			 <input type="text" name="speciescultured"  id="speciescultured"  class="speciescultured form-control input-group-lg"/>
		<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div>
<!-- 		<div class="col-md-6 mb-4">
			<label  for="typeofwater">Type of Water:</label>
			 <input type="text" name="typeofwater"  id="typeofwater"  class="typeofwater form-control input-group-lg"/>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div> -->
<!-- 		<div class="col-md-6 mb-4">
			<label  for="status">Status:</label>
			 <input type="text" name="status"  id="status"  class="status form-control input-group-lg"/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div> -->
		</div>
		<div class="form-row">
		
		<div class="col-md-6 mb-6">	
		<label   for="kind">Kind</label>				
				<select name="kind"  id="kind"  class="kind form-control input-group-lg">
										<option value=""></option>
										<option value="Natural">Natural</option>
										<option value="Commercial">Commercial</option>
				</select>
		<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div>		
		<div class="col-md-6 mb-6">		
		<label   for="hatchery">Hatchery</label>				
				<select name="hatchery"  id="hatchery"  class="hatchery form-control input-group-lg">
										<option value=""></option>
										<option value="Nursery">Nursery</option>
										<option value="Grow-Out">Grow-Out</option>
				</select>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div>
		</div>		
<!-- 		<label for="productPer"><span>Product Per Cropping:</span>
			 <input type="text" name="productPer"  id="productPer"  class="productPer input-field "/>
				<i class="fas fa-check-circle"></i>
					<i class="fas fa-exclamation-circle"></i>
					<br>
					<small></small>
			</label>
		 -->
		</fieldset>
		
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ADDITIONAL INFORMATION</legend>
			<div class="form-row">
			<div class="col-md-6 mb-6">	
			<label  for="dateasof">Data as of:</label>
			<input type="date" name="dataasof" id="dataasof" class="dataasof  form-control input-group-lg">
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div>
			<div class="col-md-6 mb-6">	
			<label  for="datasource">Data Sources:</label>
					<input type="text" name="datasource"  id="datasource"  class="datasource form-control input-group-lg"/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div>
		</div>
		<div class="form-row">
			<div class="col-md-6 mb-12">		
			<label  for="remarks">Remarks:</label>
					<textarea name="remarks" id="remarks"  class="remarks  form-control input-group-lg"></textarea>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div>
		</div>
			
		</fieldset>
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">GEOGRAPHICAL LOCATION</legend>
			<div class="form-row">
			<div class="col-md-6 mb-12">	
			<label for="lat">Latitude:</label>			
				<input type="text" name="lat" readonly="readonly"  id="lat" class="lat form-control input-group-lg latValidation"/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
      		</div>	
      		<div class="col-md-6 mb-12">	
			<label for="lon">Longitude:</label>
			<input type="text" name="lon" readonly="readonly" id="lon" class="lon form-control input-group-lg lonValidation"/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			</div>	
			<div class="form-row">
			<div class="col-md-6 mb-12">		
			  <label for="photos">Captured Image</label>
    			
					<input type="file" id="photos" class="file" required accept="image/*" />
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			</div>
     			</fieldset>
     				<div id="img_preview" style="display: none">
     						<img id="preview" name="preview"  class="preview" style="width: 450px; height: 200px"></img>
     				</div>
     						<input type="hidden" name="image_src"  id="image_src" class="image_src" value="" /><br />
							<input type="hidden" name="image"  id="image" class="image"/>			
		  
		  		<div class="btn-group" role="group" aria-label="Basic example">
			 	<button type="submit" id="submitFishPond" class="submitFishPond btn btn-primary">SUBMIT</button>
				<button type="button" id="cancel" class="cancel btn btn-secondary">CANCEL</button>
		</div>
		 
		</form>
		<div id="pdf" style="display: none">
        	<button class="exportToPDF btn-info">Generate PDF</button>
		</div>
		
		</div>
		  
<br><br>
  <div id="table">				
<table id="pdfFISHPOND" style="display: none;">
  <thead>
    <tr>
      <th scope="col">FISH POND </th>
      <th scope="col">&nbsp;</th>
    </tr>
  </thead>
  <tbody id="tbodyID">
  </tbody>
</table>

</div>  
<br><br>
 <div class="container noToPrint"> 
 <button class="create btn-primary">Create</button>
  <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','FishPond')" />
 
 <div class="row">
       <div class="col-md-12  table-responsive">
			<table id="export-buttons-table" class="table table-hover">
               <thead>
                  <tr>
                    <th>Region</th><th>Province</th><th>Municipality</th><th>Barangay</th><th>Name of Fishpond Operator</th><th>Coordinates</th>
                  </tr>
                </thead>
                <tbody>  
               </tbody>
              </table>
              <div id="paging">
            
             </div>
 
      <table class="table2excel" id="exTable" style="display: none">
                <thead>
                
                <tr><th align="center" colspan="5">FISH POND</th></tr>
                
                <tr>               
					<th>${region}</th> 
                  </tr>
                  <tr>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of FishPond Operator</th>
                    <th>Name of ActualOccupant</th>
                    <th>Code</th>
                    <th>Area</th> 
                    <th>FishPond Type</th>
                    <th>FishPond Status</th>                     
                    <th>Species Cultured</th>                
                    <th>kind</th>
                    <th>Hatchery</th>
                    <th>Data Source</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="emp_body">
               </tbody>
              </table>       
      </div>
 
 </div>
  </div>
</div>
<jsp:include page="../menu/fma_details.jsp"></jsp:include>

</div>
   <div id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</div> 

<script src="/static/js/imageRPM.js"></script>
<script src="/static/js/form/denr_bfar.js"></script>	
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script src="/static/js/form/provinceMunBarangay.js"></script>	
<script src="/static/js/form/jspdf.debug.js"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/validationForm/fishPond.js"></script>
<script  src="/static/js/resource.js"></script>
<script  src="/static/js/api/regionMap.js" ></script>
<script src="/static/js/map/philippineMap.js"></script>
<script  src="/static/js/resourcesOnMapByResources.js" ></script>
	<script>
	
	 var map_location = ${mapsData}; 
	

		function getFisheriesData(){
			getFishPond(map_location);
		}

</script>

	</body>

</html>