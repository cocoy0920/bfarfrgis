<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="en">
<head>
<title>Fish Pen</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
<script src="/static/leaflet/leaflet.js"></script>
<script src="/static/leaflet/leaflet.ajax.min.js"></script>
<script src="//d3js.org/d3.v3.min.js"></script>
<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
<script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
<script src="/static/leaflet/bundle.js"></script>
<link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
<script src="/static/leaflet/L.Control.SlideMenu.js"></script>
<script src="/static/leaflet/leaflet-bootstrapmodal.js"></script>
<script src="https://cdn.jsdelivr.net/npm/exif-js"></script>
<style>
#map{ 
	width:100%;
  	height: 600px;
   background: #009dc4;
}
</style>
</head>
<body onload="getFisheriesData()">

  <jsp:include page="../home/header_users.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">
  <!-- Sidebar Holder -->
<%--   <jsp:include page="../menu/sidebar2.jsp"></jsp:include> --%>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
    <c:if test="${mapsData != '' }">
    <div id="mapview">
    <div id="map"></div>
    </div>
    </c:if>
    <c:if test="${mapsData == '' }">
    <div id="mapview"  style="display:none;">
    <div id="map"></div>
    </div>
    </c:if>
    <h1 align="center">FISH PEN</h1>
		<div class="form_input" style="display: none">
			<form class="needs-validation" id="myform" novalidate>
				<input type="hidden" name="id" id="id" class="id"/>
				<input type="hidden" name="user" id="user" class="user"/>
				<input type="hidden" name="uniqueKey" id="uniqueKey" class="uniqueKey"/>
				<input type="hidden" name="dateEncoded" id="dateEncoded" class="dateEncoded"/>
				<input type="hidden" name="dateEdited" id="dateEdited" class="dateEdited" />
				<input type="hidden" name="encodedBy" id="encodedBy" class="encodedBy"/>
				<input type="hidden" name="editedBy" id="editedBy" class="editedBy" />
				<input type="hidden" name="region" id="region" value="${region }" class="region"/>	
				<br>
		
			<fieldset class="form-group border border-dark rounded p-4"><legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH PEN LOCATION</legend>
					
				<div class="form-row">
    			<div class="col-md-4 mb-3">
					<label  for="validationCustom01">Province</label>
					<select name="province"  id="validationCustom01" class="form-control province" required></select>															
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid province.
      				</div>
					</div>


    			<div class="col-md-4 mb-3">	
				<label  for="validationCustom02">Municipality/City </label> 		
				  <select name="municipality" id="validationCustom02"  class="form-control municipality" required></select>	
				 <div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Municipality/City 
      				</div>
					</div>

    			<div class="col-md-4 mb-3">															
				<label  for="validationCustom03">Barangay</label>
					<select name="barangay"  id="validationCustom03"  class="form-control barangay" required>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Barangay
      				</div>
					</div>
			</div>
			
			<div class="form-row">
    			<div class="col-md-12 mb-3">			
					<label  for="code">Code:</label>
						<input type="text" name="code" id="code"  class="code" placeholder="Code" readonly="readonly"/>
				</div>
			</div>																
			</fieldset>
			<fieldset class="form-group border border-dark rounded p-4"><legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH PEN INFORMATION</legend>
			<div class="form-row">
    			<div class="col-md-6 mb-3">
			 <label  for="nameOfOperator"><span>Name of Operator:</span></label>
			 <input type="text" name="nameOfOperator"  id="nameOfOperator"  class="fishPenNameOfOperator form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-6 mb-3">
			<label  for="area"><span>Area</span></label>
			<input type="text" name="area"  id="area"  class="area form-control" required/>	
			<!-- <input type="number" name="area" min="0" value="0" step="any" id="area"  class="fishpenarea form-control" required/>	 -->															
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			</div>
			<div class="form-row">
    			<div class="col-md-6 mb-3">
			<label  for="noOfFishPen"><span>No. of Fish Pen:</span></label>
					<input type="text" name="noOfFishPen" id="noOfFishPen"  class="noOfFishPen form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-6 mb-3">
			<label  for="speciesCultured"><span>Species Cultured:</span></label>
					<input type="text" name="speciesCultured" id="speciesCultured"  class="speciesCultured form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			</div>
		<!-- <div class="form-row">
    			<div class="col-md-6 mb-3">
			<label  for="croppingStart"><span>Cropping Start:</span></label>
			 		
					<input type="text" name="croppingStart" id="croppingStart"  class="croppingStart form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-6 mb-3">
			<label  for="croppingEnd"><span>Cropping End:</span></label>
					<input type="text" name="croppingEnd" id="croppingEnd"  class="croppingEnd form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			
			</div>
			 </div> -->
		</fieldset>
			<fieldset class="form-group border border-dark rounded p-4"><legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ADDITIONAL INFORMATION</legend>
							<div class="form-row">

    			<div class="col-md-6 mb-3">	
					<label  for="dateAsOf">Data as of</label>
					<input type="date" name="dateAsOf" id="dateAsOf" class="form-control dateAsOf"  required>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
		<!-- 	<div class="col-md-6 mb-3">	
					<input type="text" name="startDate" id="startDate" class="date-picker"  data-date="" value="" />
			</div>-->

    			<div class="col-md-6 mb-3">	
				<label  for="validationCustom09">Data Source/s</label>
					<input type="text" name="sourceOfData"  id="validationCustom09"  class="form-control sourceOfData" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>

			</div>
		<div class="form-row">
    		<div class="col-md-12 mb-3">					
				<label  for="validationCustom10">Remarks</label>
					<textarea name="remarks" id="validationCustom10"  class="form-control remarks" required></textarea>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

		</div>
		</fieldset>
			<fieldset class="form-group border border-dark rounded p-4"><legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">GEOGRAPHICAL LOCATION</legend>
					<div class="form-row">

    		<div class="col-md-6 mb-3">	
				<label  for="lat">Latitude</label>			
					<input type="text" name="lat" readonly="readonly"  id="lat" class="form-control lat" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

    		<div class="col-md-6 mb-3">	
				<label  for="lon">Longitude</label>
				<input type="text" name="lon"  readonly="readonly"  id="lon" class="form-control lon" required/>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

		</div>	
			<div class="form-row" id="image_form">
    			<div class="col-md-12 mb-3">				
			  		<label  for="photos">Captured Image</label>		
					<input type="file" id="photos" class="form-control file" accept="image/*"/>
    				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
			</div>
     			</fieldset>
     				<div id="img_preview" style="display: none">
     						<img id="preview" name="preview"  class="preview" style="width: 450px; height: 200px"></img>
     				</div>
     						<input type="hidden" name="image_src" id="image_src" class="image_src" value="" /><br />
							<input type="hidden" name="image"  id="image" class="image"/>			
		  
		  <div class="btn-group" role="group" aria-label="Basic example">
		  		<button type="button" id="submitFishPen" class="submitFishPen btn btn-primary">SUBMIT</button>
				<button type="button" id="cancel" class="cancel btn btn-secondary">CANCEL</button>
		 </div>
		</form>
		 <div id="pdf" style="display: none">
        		<button class="exportToPDF btn-info">Generate PDF</button>
		</div>
</div>
		 
<br><br>
  <div id="table">				
<table id="pdfFISHPEN" style="display: none;">
  <thead>
    <tr>
      <th scope="col">FISH PEN </th>
      <th scope="col">&nbsp;</th>
    </tr>
  </thead>
  <tbody id="tbodyID">
  </tbody>
</table>

</div>  

 <div class="container"> 
 <button class="create btn-primary">Create</button>
  <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','FishPen')" />
    
 <div class="row">
       <div class="col-md-12 table-responsive">
<table id="export-buttons-table" class="table table-hover">
               <thead>
                  <tr>
                    <th>Region</th><th>Province</th><th>Municipality</th><th>Barangay</th><th>Name of Operator</th><th>Coordinates</th>
                  </tr>
                </thead>
                <tbody> 
               </tbody>
              </table>
              <div id="paging">
           
             </div>
 
      <table class="table2excel" id="exTable" style="display: none">
                <thead>
                          
                <tr><th align="center" colspan=5">FISH PEN</th></tr>
                <tr>            
					<th>${region}</th>                   
                  </tr>
                  <tr>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Operator</th>
                    <th>Code</th>
                    <th>Area</th>   
                    <th>No.of FishPen</th>                 
                    <th>Species Cultured</th>
                    <!-- <th>Cropping start</th>
                    <th>Cropping end</th> -->
                    <th>Source of Data</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="emp_body">
                <!--  ${listAll}--> 
               </tbody>
              </table>       
    <!-- <button class="exportToExcel btn-info">Export to XLS</button> -->
      
      </div>
 
 </div>
  </div>
<!-- end of div content -->
  </div>



  <!-- end wrapper div -->
  <jsp:include page="../menu/fma_details.jsp"></jsp:include>
  
    </div>
      	<div id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</div>

<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>
<script src="/static/js/form/provinceMunBarangay.js"></script>
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script src="/static/js/validationForm/fishPen.js" type=" text/javascript"></script>

<script  src="/static/js/resource.js" ></script>
<script  src="/static/js/api/regionMap.js" ></script>
<script src="/static/js/map/philippineMap.js"></script>
<script  src="/static/js/resourcesOnMapByResources.js" ></script>
<script>
	
	 var map_location = ${mapsData}; 

		function getFisheriesData(){
			getFishPen(map_location);
		}

</script>
    </body>
</html>