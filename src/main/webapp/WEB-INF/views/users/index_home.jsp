<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
<head>
<title>FRGIS</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />

<jsp:include page="../menu/css.jsp"></jsp:include>
	<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
	<script src="/static/leaflet/spin.min.js"></script>
	<script src="/static/leaflet/leaflet.spin.min.js"></script>
	<script src="//d3js.org/d3.v3.min.js"></script>
	<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
  	<script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
  	<link href='/static/leaflet/Leaflet.BigImage.min.css' rel='stylesheet' />
  	<script src='/static/leaflet/Leaflet.BigImage.min.js'></script>
  	 <link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
    <script src="/static/leaflet/L.Control.SlideMenu.js"></script>
	 <script  src="/static/leaflet/leaflet-bootstrapmodal.js"></script>
  	  <script src="/static/leaflet/Leaflet.Editable.js"></script>
  	 <script src="/static/leaflet/html2canvas.min.js"></script>
     <script src="/static/leaflet/Leaflet_measure.js"></script>
  	<script src="/static/leaflet/leaflet_export.js"></script>
 	<style type="text/css">
	.hiddenRow {
    padding: 0 !important;
}
.collapsible {
  background-color: #777;
  color: white;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
}

.active, .collapsible:hover {
  background-color: #555;
}

.content_collapse {
  padding: 0 18px;
  display: none;
  overflow: hidden;
  background-color: #f1f1f1;	
	
}	
	.my-div-icon{
	background: white url(../images/calendar_2.png) right no-repeat;
	}
	.countryLabel{
  background: rgba(255, 255, 255, 0);
  border:0;
  border-radius:0px;
  box-shadow: 0 0px 0px;
  
}
.my-div-icon {
				width: 300px;
				height:25px;
				background: white;
				border: 3px black;
				 
			}
.leaflet-div-icon {
   border: 1px solid #000;
  border-radius: 0px; 
  padding-bottom: 25px;
  text-align: center;
    background: transparent;
  border: 0px transparent; 
  display:inline-block;
  color: #000;
white-space: nowrap;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
pointer-events: none;
}
.leaflet-tooltip-own {
position: absolute;
/* background-color: rgba(0, 0, 0, 0.5); */
border: 0px solid #000;
border-radius: 0px;
color: #000;
white-space: nowrap;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
pointer-events: none;
/* box-shadow: 0 1px 3px rgba(0,0,0,0.4); */
}
/* .leaflet-tooltip {
    position: absolute;
    padding: 6px;
    background-color: #000;
    border: 1px solid #fff;
    border-radius: 3px;
    color: #222;
    white-space: nowrap;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    pointer-events: none;
    box-shadow: 0 1px 3px rgba(0,0,0,0.4);
} */

/*       .content {
        margin: 0.25rem;
        border-top: 1px solid #000;
        padding-top: 0.5rem;
        background: #002e7c; 
      } */

      .header {
        font-size: 1.8rem;
        color: #7f7f7f;
      }

      .title {
        font-size: 1.1rem;
        color: #7f7f7f;
        font-weight: bold;
      }

      .bottom {
        margin-top: 64px;
        font-size: 0.8rem;
        color: #7f7f7f;
      }
.myDivicon {
    background-clip: padding-box;
    border-radius: 20px;
    background-color: rgba(255, 255, 255, 0.6);
}
.myDivicon div {
    width: 30px;
    height: 30px;
    margin-left: 5px;
    margin-top: 5px;
    text-align: center;
    border-radius: 15px;
    font: 8px"Helvetica Neue", Arial, Helvetica, sans-serif;
    line-height: 10px;
}
</style> 		
</head>
<body>
<%--   <jsp:include page="../home/header_users_home.jsp"></jsp:include> --%>
<jsp:include page="../home/header_region.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">
  <!-- Page Content Holder -->
    <div id="content" class="section-to-print">	
<!--     <button id="exportWithCaptionBtn" onclick="downloadMap('My leaflet map');">download</button>
    <button id="exportBtn" onclick="downloadMap();">download</button>
    <button type="button" id="canvasButton">Draw Canvas</button> -->
					<div id="map">
					<!-- <div class="leaflet-control leaflet-top leaflet-center col-sm-2 col-sm-offset-1">
              			<img alt="" src="/static/images/FRGIS-LOGO.png" style="height: 50px; width: 50px">
              		</div> -->
					</div>
 
			</div>
<jsp:include page="../menu/fma_details.jsp"></jsp:include>
    </div>

	<div id="footer">
		<jsp:include page="../users/footer.jsp"></jsp:include>
	</div>
	<script src="/static/leaflet/bundle.js"></script>
	<script src="/static/js/api/regionMap.js"></script>
	<script  src="/static/js/api/FMA.js" ></script>	
	<script  src="/static/js/resource.js" ></script>

	<script src="/static/js/modal/users_menu.js"></script>
	<script src="/static/js/map/checkResourcesByUser.js"></script>
	<script src="/static/js/map/philippineMap.js"></script>
	<script  src="/static/js/resourcesOnMapByUser.js" ></script>
	<script  src="/static/js/listform/forms.js" ></script>
<script type="text/javascript">
        $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
        </script>    
        
   <script>
   
   function save_filename() {
   	var photos = document.getElementById('map');
 	  var image = new Image();
 	  image.src = photos;
 	 console.log(photos);
 	  image.onload = function() {
 		  var canvas = document.createElement("canvas");
 			
 	    	canvas.width = 800;
 	        canvas.height = 400;
 	        var ctx = canvas.getContext("2d");
 	    ctx.drawImage(image, 0,0,800,400);
 	    
   	  var link = document.createElement("a");
		link.download = 'fishlanding.png';
		link.href = canvas.toDataURL("image/png");
	  
		document.body.appendChild(link);
		link.click();

	  document.body.removeChild(link);
		delete link;
 	  };  
 	}

//	jQuery(document).ready(function($) {

//		 var resources = [];
//		  var pages="";
		
   // $(".btn").click(function(event) {
   //     event.preventDefault();
  //      event.stopPropagation();
    /*     $('input:checked').each(function() {
       	 resources.push($(this).val());
            }); */
//		 
 /*  	  var markedCheckbox = document.getElementsByName('page');  
        for (var checkbox of markedCheckbox) {  
         if (checkbox.checked)  
         //   console.log(checkbox.value + ' ');  
       	  resources.push(checkbox.value);
          pages += checkbox.value + ','
          
        } */
        //alert(pages);
//        console.log(resources);
       	//console.log(pages);
/*      $.ajax({
                type : "GET",
                url : '/create/resources-get-by-pages',
                data : {resources: resources},
                success : function(data) {
               	 		console.log("success --> ");
                    if(data){
                   	// console.log(data);
                        //  loadChannels();
                        //location.reload();
                        }else{
                    alert("failed to upload");

                    }
                }
        });  
      */   
	//  });
    
 
 //	});  

   </script>  
   <script>
/*     var centerPoint = [58.0060984592271, 56.2445307230809];

    // Create leaflet map.
    var baseExportOptions = {
      caption: {
        text: 'Карта',
        font: '30px Arial',
        fillStyle: 'black',
        position: [100, 200]
      }
    };
    var map = L.map('map', {
      editable: true,
      printable: true,
      downloadable: true,
    }).setView(centerPoint, 12);

    // Add OSM layer.
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    // Create custom measere tools instances.
    var measure = L.measureBase(map, {});

    var mimeTypes = map.supportedCanvasMimeTypes();
    var mimeArray = [];
    for (var type in mimeTypes) {
      mimeArray.push(mimeTypes[type]);
    }
    document.getElementById('suportedMimeTypes').innerHTML = mimeArray.join(',&nbsp;');
 */

 $("#canvasButton").click(function () {
	    var mapPane = $(".leaflet-map-pane")[0];
	    var mapTransform = mapPane.style.transform.replace("translate3d(", "").split(",");
	    var mapX = parseFloat(mapTransform[0].replace("px", ""));
	    var mapY = parseFloat(mapTransform[1].replace("px", ""));
	    mapPane.style.transform = "translate3d(0px,0px,0px)";

	    var myTiles = $("img.leaflet-tile");
	    var tilesLeft = [];
	    var tilesTop = [];
	    for (var i = 0; i < myTiles.length; i++) {
	        tilesLeft.push(parseFloat(myTiles[i].style.left.replace("px", "")));
	        tilesTop.push(parseFloat(myTiles[i].style.top.replace("px", "")));
	        myTiles[i].style.left = (tilesLeft[i] + mapX) + "px";
	        myTiles[i].style.top = (tilesTop[i] + mapY) + "px";
	    }

	    var myDivicons = $(".myDivicon");
	    var dx = [];
	    var dy = [];
	    var mLeft = [];
	    var mTop = [];
	    for (var i = 0; i < myDivicons.length; i++) {
	        mLeft.push(parseFloat(myDivicons[i].style.marginLeft.replace("px", "")));
	        mTop.push(parseFloat(myDivicons[i].style.marginTop.replace("px", "")));
	        var curTransform = myDivicons[i].style.transform;
	        var splitTransform = curTransform.replace("translate3d(", "").split(",");
	        dx.push(parseFloat(splitTransform[0].replace("px", "")));
	        dy.push(parseFloat(splitTransform[1].replace("px", "")));
	        myDivicons[i].style.transform = "translate3d(" + (dx[i] + mLeft[i] + mapX) + "px, " + (dy[i] + mTop[i] + mapY) + "px, 0px)";
	        myDivicons[i].style.marginLeft = "0px";
	        myDivicons[i].style.marginTop = "0px";
	    }

	    var linesLayer = $("svg.leaflet-zoom-animated")[0];
	    /* 	    var linesTransform = linesLayer.style.transform.replace("translate3d(", "").split(",");
	    var linesX = parseFloat(linesTransform[0].replace("px", ""));
	    var linesY = parseFloat(linesTransform[1].replace("px", ""));
	    linesLayer.style.transform = "translate3d(" + ((linesX + mapX) / 2) + "px," + ((linesY + mapY) / 2) + "px, 0px)";
 */
	    html2canvas(document.getElementById("map"), {
	        useCORS: true,
	        onrendered: function (canvas) {
	            document.body.appendChild(canvas);

	        }
	    });

	    for (var i = 0; i < myTiles.length; i++) {
	        myTiles[i].style.left = (tilesLeft[i]) + "px";
	        myTiles[i].style.top = (tilesTop[i]) + "px";
	    }
	    for (var i = 0; i < myDivicons.length; i++) {
	        myDivicons[i].style.transform = "translate3d(" + dx[i] + "px, " + dy[i] + "px, 0)";
	        myDivicons[i].style.marginLeft = mLeft[i] + "px";
	        myDivicons[i].style.marginTop = mTop[i] + "px";
	    }
	  //  linesLayer.style.transform = "translate3d(" + (linesX) + "px," + (linesY) + "px, 0px)";
	    mapPane.style.transform = "translate3d(" + (mapX) + "px," + (mapY) + "px, 0px)";
	});
  </script>      
    </body>
</html>