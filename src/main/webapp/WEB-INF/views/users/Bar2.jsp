  
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!doctype html>
<html lang="en">
<head>
<title>FRGIS - CHART</title>
<meta charset="utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />
  <jsp:include page="../menu/css.jsp"></jsp:include>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <style>
    .chart {
    display: block;
	margin: 0 auto;
    }
    </style>


</head>
<body>

 <jsp:include page="../home/header_bar.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">
  <!-- Sidebar Holder -->
<nav id="sidebar">
<!-- <div class="custom-menu">
<button type="button" id="sidebarCollapse" class="btn btn-primary">
<i class="fa fa-bars"></i>
<span class="sr-only">Toggle Menu</span>
</button>
</div> -->
<!-- <div class="p-4"> -->

<!--     <hr>
               		<hr> 
<ul class="list-unstyled components mb-5"> -->
	<!-- <li>      
      <a href="#provinceList" data-toggle="collapse" aria-expanded="false">
      <span class="text">Filter by Province<i class="fas fa-list"></i></span></a>
        <ul class="collapse list-unstyled" id="provinceList">  -->    
               		
       		<!-- 
       			<li>
    				<a href="/create/bar/all" >
    				<span class="text">All</span>
    				</a>
    				</li> -->
       	<%-- 	<c:forEach items="${provinces}" var="province" >
    					<input type="checkbox" class="checkprovince" value="${province.province_id}" name="province[1][]"> 
    					<span class="text">${province.province}</span><br>
    				<li>
    				<a href="/create/bar/${province.province_id},${province.province}" >
    				<span class="text">${province.province}</span>
    				</a>
    				</li>
    					
    				</c:forEach> --%>
<!--       	 	<li>AQUACULTURE</li>
	  		


</ul> -->
     <%--  <sec:authorize access="isAuthenticated()">
       <a href="/logout"><i class="fas fa-sign-out-alt"></i>LOGOUT</a>
      </sec:authorize> --%>
</nav>

  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5 section-to-print">	   

	<!-- <table class="columns">
		<tr>
			<td><div id="region_div" style="border: 1px solid #ccc"></div></td>
			<td><div id="chart_div" style="border: 1px solid #ccc"></div></td>
		</tr>
	</table> -->

	<table class="columns">
      <tr>
        <td>
        	<div id='png'></div>
			<div id="chart_div" style="border: 1px solid #ccc"></div>
        </td>
        <td>
        	<div id='pngForSeaWeeds'></div>
			<div id="chartSeaWeeds_div" style="border: 1px solid #ccc"></div>
        </td>
      </tr>
    </table>
    <table class="columns">
      <tr>
        <td>
        	<div id='pngForMariculture'></div>
			<div id="chartMariculture_div" style="border: 1px solid #ccc"></div>
        </td>
        <td>
        	<div id='pngForHatchery'></div>
			<div id="chartHatchery_div" style="border: 1px solid #ccc"></div>
        </td>
      </tr>
    </table>

			</div>



</div>

<!-- 	 <form id="form1" name="form1" onchange="drawChart()">

     <select id="thedropdown">
     <option value="Austin">Austin</option>
     <option value="Boulder">Boulder</option>
     </select>

  </form>

    <div id="div_chart"></div>
     -->

    <script type="text/javascript">
/* 		$.ajax({
			type : 'GET',
			headers : {
				Accept : "application/json; charset=utf-8",
				"Content-Type" : "application/json; charset=utf-8"
			},
			url : '${pageContext.request.contextPath}/region/data',
			success : function(result) {
				console.log("result: " + result);
				google.charts.load('current', {
					'packages' : [ 'corechart' ]
				});
				google.charts.setOnLoadCallback(function() {
					drawChart(result);
				});
			}
		}); */

		google.charts.load("current", {packages:['corechart']});
	    // Set a callback to run when the Google Visualization API is loaded.
		//  google.setOnLoadCallback(drawAllChart);


	    /*   google.setOnLoadCallback(drawChart1); 
	      google.setOnLoadCallback(drawChartAllResources); 
 	      google.setOnLoadCallback(drawChart);*/




/* 		function drawChart1() {


 		var data = google.visualization.arrayToDataTable([
     		['Resources', 'Total', { role: 'style'}, { role: 'annotation' }],
     			<c:forEach items="${provinceChart}" var="entry">
         			[ '${entry.resources}', ${entry.total},'${entry.color}' , ${entry.total}],
     			</c:forEach>
			]);

			var barchart_options = {
				title :   "${province_title}",
				legend :  { position: "none" },
			
				is3D : true,
		        
		            'width' : 900,
		            'height' : 500
			};

			var barchart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
			barchart.draw(data, barchart_options);
			

		} */
		/* 
		function drawChartAllResources() {


	 		var data = google.visualization.arrayToDataTable([
	     		['Resources', 'Total', { role: 'style'}, { role: 'annotation' }],
	     			<c:forEach items="${chart_by_region}" var="entry">
	         			[ '${entry.resources}', ${entry.total},'${entry.color}' , ${entry.total}],
	     			</c:forEach>
				]);

				var barchart_options = {
					title :   "${region_title}",
					legend :  { position: "none" },
					
					is3D : true,
			    
			            'width' : 900,
			            'height' : 500
				};

				var barchart = new google.visualization.ColumnChart(document.getElementById('all_resources_div'));
				barchart.draw(data, barchart_options);
				

			} */
			
		
/* 		
 		function drawAllChart() {

			
			 var region_data = google.visualization.arrayToDataTable([
			        ['Province', 
			        	'Fish Sanctuary', {role: 'style'}, { role: 'annotation' },
			        	'Fish Processing',{role: 'style'}, { role: 'annotation' },
			         	'Fish Landing',{role: 'style'}, { role: 'annotation' },
			         	'Fish Port',{role: 'style'}, { role: 'annotation' },
			         	'Fish Pen',{role: 'style'}, { role: 'annotation' },
			         	'Fish Cage',{role: 'style'}, { role: 'annotation' },
			         	'Cold Storage',{role: 'style'}, { role: 'annotation' },
			         	'Market',{role: 'style'}, { role: 'annotation' },
			         	'School of Fisheries',{role: 'style'}, { role: 'annotation' },
			         	'Seagrass',{role: 'style'}, { role: 'annotation' },
			         	'SeaWeeds',{role: 'style'}, { role: 'annotation' },
			         	'Mangrove',{role: 'style'}, { role: 'annotation' },
			         	'Fish Corals',{role: 'style'}, { role: 'annotation' },
			         	'Mariculture Zone',{role: 'style'}, { role: 'annotation' },
			         	'PFO',{role: 'style'}, { role: 'annotation' },
			         	'Training Center',{role: 'style'}, { role: 'annotation' },{ role: 'annotation' } ],
			         	<c:forEach items="${allChart}" var="entry">
	         			[ '${entry.province}',
	         				${entry.fishcount},"#FFD124", ${entry.fishcount},
	         				${entry.processingCount},"#EAEA7F",${entry.processingCount},
	         				${entry.fishlandingCount},"#5EE6EB",${entry.fishlandingCount},
	         				${entry.fishportCount},"#E2D784",${entry.fishportCount},
	         				${entry.fishpenCount},"#4D96FF",${entry.fishpenCount},
							${entry.fishcageCount},"#FF1700",${entry.fishcageCount},
							${entry.fishcoldstorageCount},"#97DBAE",${entry.fishcoldstorageCount},
							${entry.fishmarketCount},"#D49B54",${entry.fishmarketCount},
							${entry.fishschoolofFisheriesCount},"#C74B50",${entry.fishschoolofFisheriesCount},
							${entry.fishcoralCount},"#000957",${entry.fishcoralCount},
							${entry.seagrassCount},"#6BCB77",${entry.seagrassCount},
							${entry.seaweedsCount},"#019267",${entry.seaweedsCount},
							${entry.mangroveCount},"#05595B",${entry.mangroveCount},
							${entry.maricultureCount},"#F55353",${entry.maricultureCount},
							${entry.pfoCount},"#FF6464",${entry.pfoCount},
							${entry.trainingCount},"#0F2C67",${entry.trainingCount},''],
	     			</c:forEach>
	   
			    ]); 
			  
			      var options = {
			    	title: "${region_title}",		  
			        width: 900,
			        height: 700,
			        bar: { groupWidth: '75%' },
			       
			        isStacked: true,
			      series: {
			    	    0:{color:'#FFD124'},
			    	    1:{color:'#EAEA7F'},
			    	    2:{color:'#5EE6EB'},
			    	    3:{color:'#E2D784'},
			    	    4:{color:'#4D96FF'},
			    	    5:{color:'#FF1700'},
			    	    6:{color:'#97DBAE'},
			    	    7:{color:'#D49B54'},
			    	    8:{color:'#C74B50'},
			    	    9:{color:'#6BCB77'},
			    	    10:{color:'#019267'},
			    	    11:{color:'#05595B'},
			    	    12:{color:'#000957'},
			    	    13:{color:'#F55353'},
			    	    14:{color:'#FF6464'},
			    	    15:{color:'#0F2C67'}
			    	  }
			    	
			      };
			      
			var region_chart = new google.visualization.ColumnChart(document.getElementById('region_div'));
			region_chart.draw(region_data, options);  
			
		} */

 /* 	      function drawChart(){

 	      info = [{city: "Austin", gender: "male", work: 40, eat: 8, commute: 2, tv: 3, sleep: 49},
 	     {city: "Austin", gender: "female", work: 45, eat: 9, commute: 3, tv: 5, sleep: 56},
 	     {city: "Boulder", gender: "male", work: 50, eat: 7, commute: 5, tv: 8, sleep: 42},
 	     {city: "Boulder", gender: "female", work: 32, eat: 5, commute: 6, tv: 10, sleep: 42}
 	     ];

 	      mycity = document.getElementById("thedropdown").value;

 	     for(i=0; i<info.length; i++) {
 	    if(mycity == info[i].city && info[i].gender=="male") {
 	       workmale = info[i].work;
 	       eatmale = info[i].eat;
 	       commutemale = info[i].commute;
 	       tvmale = info[i].tv;
 	       sleepmale = info[i].sleep;

 	    }// end if

 	    if(mycity == info[i].city && info[i].gender=="female") {
 	       workfemale = info[i].work;
 	       eatfemale = info[i].eat;
 	       commutefemale = info[i].commute;
 	       tvfemale = info[i].tv;
 	       sleepfemale = info[i].sleep;

 	    }
 	    } */
/* 
 	var data = google.visualization.arrayToDataTable([
 	          ['Element', 'Male', 'Female'],
 	          ['work', workmale, workfemale],
 	          ['eat', eatmale, eatfemale],
 	          ['commute', commutemale, commutefemale],
 	          ['tv', tvmale, tvfemale],
 	          ['sleep', sleepmale, sleepfemale]
 	        ]); 

 	        var options = {
 	            title: 'Weekly Activities',
 	        };

 	        var chart = new google.visualization.ColumnChart(document.getElementById('div_chart'));

 	        chart.draw(data, options);
 	      }
 */
 	      // this function makes the chart responsive; include the name of the proper drawChart function; if more than one chart on page, include additional statements for each drawChart function
 	   /*  window.onresize = function() {
 	    	drawChart();
 	    } */
  	    var provinceSize = '${fn:length(provinces)}';
 	   google.charts.setOnLoadCallback(draw);

 	      function draw(){
 	    	 var data = google.visualization.arrayToDataTable([
 	            ['Resources',
 	            	<c:forEach items="${provinces}" var="province">	
 	            	'${province.province}',
 	            </c:forEach>
 	            	{ role: 'annotation' } ],
 	            	
 	            ['Fish Pen',
 	            	<c:forEach items="${provinceChart}" var="entry">
 	            	<c:if test = "${entry.resources == 'FPen'}">	            		
 	            			${entry.total},
 	            	</c:if>
 	            	
 			</c:forEach>
 					''
 	            	],
 	            ['Fish Cage',
 	            	
 	            	<c:forEach items="${provinceChart}" var="entry">
 	            	<c:if test = "${entry.resources == 'FCoral'}">
         			${entry.total},
         	</c:if>
					</c:forEach>
 	            	
 	            	'']
 	          
 	          ]);
 	    	 
 	    	var view = new google.visualization.DataView(data);

	    	var columns = [];
	    	for (var i = 0; i <= provinceSize ; i++) {
	    	    if (i > 0) {
	    	        columns.push(i);
	    	        columns.push({
	    	        	calc: function (dt, row) {
	 			 	    	  if(dt.getValue(row, i) == 0){
	 			 	    		  return ""; 
	 			 	    	  }else{ 
	 			 	        	return dt.getValue(row, i).toString();
	 			 	    	  }	
	 			 	      },
	 			 	      type: "string",
	 			 	      role: "annotation"
	    	        });

	    	    } else {
	    	        columns.push(i);
	    	    }
	    	}
	    	
	    	columns.push(
	    		{
		 	      calc: function (dt, row) {
		 	        return 0;
		 	      },
		 	      label: "Total",
		 	      type: "number"		  		 	   	
	        	},
	        );
	    	columns.push(
	    		{	  	    		
	    		 calc: function (dt, row) {
	    			if(provinceSize == 6){
	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5) + dt.getValue(row, 6);
	    			}
	    			if(provinceSize == 5){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5);
	  	    		}
	    			if(provinceSize == 4){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4);
	  	    		}
	    			},
		 	      type: "number",
		 	      role: "annotation"
	    	
	    	}
	    		);
	    	
	    	view.setColumns(columns);


 	          var options = {
 	            width: 500,
 	            height: 400,
 	           	title: 'AQUACULTURE(AQUAFARM) - ${region_title}' ,
 	            legend: { position: 'top', maxLines: 5 },
 	            bar: { groupWidth: '75%' },
 	            isStacked: true,
 	           vAxis: {
	  	            gridlines: {color: '#007bff;'},
	  	            minValue: 0
	  	          }
 	            
 	          };

 	         var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
 	      
 	         google.visualization.events.addListener(chart, 'ready', function () {
 	          // chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';

 	          document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '" target="_blank">PRINT AQUACULTURE CHARTS</a>';
 	         });
 	       
  	        chart.draw(view, options);
 	      }

 	      
 	 	   google.charts.setOnLoadCallback(drawSeaWeeds);

  	      function drawSeaWeeds(){
  	    	 var data = google.visualization.arrayToDataTable([
  	            ['Resources',
  	            	<c:forEach items="${provinces}" var="province">	
  	            	'${province.province}',
  	            </c:forEach>
  	            	{ role: 'annotation' } ],
  	            	
  	            ['NURSERY',
  	            	<c:forEach items="${provinceChart}" var="entry">
  	            	<c:if test = "${entry.resources == 'SNURSERY'}">	            		
  	            			${entry.total},
  	            	</c:if>
  	            	
  			</c:forEach>
  					''
  	            	],
  	            ['LABORATORY',
  	            	
  	            	<c:forEach items="${provinceChart}" var="entry">
  	            	<c:if test = "${entry.resources == 'SLABORATORY'}">
          			${entry.total},
          	</c:if>
 					</c:forEach>
  	            	
  	            	'']
  	          
  	          ]);
  	    	 
  	    	var view = new google.visualization.DataView(data);

	    	var columns = [];
	    	for (var i = 0; i <= provinceSize ; i++) {
	    	    if (i > 0) {
	    	        columns.push(i);
	    	        columns.push({
	    	        	calc: function (dt, row) {
	 			 	    	  if(dt.getValue(row, i) == 0){
	 			 	    		  return ""; 
	 			 	    	  }else{ 
	 			 	        	return dt.getValue(row, i).toString();
	 			 	    	  }	
	 			 	      },
	 			 	      type: "string",
	 			 	      role: "annotation"
	    	        });

	    	    } else {
	    	        columns.push(i);
	    	    }
	    	}
	    	
	    	columns.push(
	    		{
		 	      calc: function (dt, row) {
		 	        return 0;
		 	      },
		 	      label: "Total",
		 	      type: "number"		  		 	   	
	        	},
	        );
	    	columns.push(
	    		{	  	    		
	    		 calc: function (dt, row) {
	    			if(provinceSize == 6){
	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5) + dt.getValue(row, 6);
	    			}
	    			if(provinceSize == 5){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5);
	  	    		}
	    			if(provinceSize == 4){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4);
	  	    		}
	    			},
		 	      type: "number",
		 	      role: "annotation"
	    	
	    	}
	    		);
	    	
	    	view.setColumns(columns);


  	          var options = {
  	            width: 500,
  	            height: 400,
  	           	title: 'AQUACULTURE(SEAWEEDS) - ${region_title}' ,
  	            legend: { position: 'top', maxLines: 5 },
  	            bar: { groupWidth: '75%' },
  	            isStacked: true,
  	          vAxis: {
	  	            gridlines: {color: '#007bff;'},
	  	            minValue: 0
	  	          }
  	          };

  	         var chart = new google.visualization.ColumnChart(document.getElementById('chartSeaWeeds_div'));
  	      
  	         google.visualization.events.addListener(chart, 'ready', function () {
  	          // chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';

  	          document.getElementById('pngForSeaWeeds').outerHTML = '<a href="' + chart.getImageURI() + '" target="_blank">PRINT AQUACULTURE(SEAWEEDS) CHARTS</a>';
  	         });
  	       
   	        chart.draw(view, options);
  	      }

	 	   google.charts.setOnLoadCallback(drawMariculturePark);

	  	      function drawMariculturePark(){
	  	    	 var data = google.visualization.arrayToDataTable([
	  	            ['Resources',
	  	            	<c:forEach items="${provinces}" var="province">	
	  	            	'${province.province}',
	  	            </c:forEach>
	  	            	{ role: 'annotation' } ],
	  	            	
	  	            ['BFAR MANAGED',
	  	            	<c:forEach items="${provinceChart}" var="entry">
	  	            	<c:if test = "${entry.resources == 'BFAR_Mariculture'}">	            		
	  	            			${entry.total},
	  	            	</c:if>
	  	            	
	  			</c:forEach>
	  					''
	  	            	],
	  	            ['LGU MANAGED',
	  	            	
	  	            	<c:forEach items="${provinceChart}" var="entry">
	  	            		<c:if test = "${entry.resources == 'LGU_Mariculture'}">
	          					${entry.total},
	          				</c:if>
	 					</c:forEach>
	  	            	
	  	            	''],
	  	            ['PRIVATE SECTOR',
	  	            	
	  	            	<c:forEach items="${provinceChart}" var="entry">
	  	            		<c:if test = "${entry.resources == 'PRIVATE_Mariculture'}">
	          					${entry.total},
	          				</c:if>
	 					</c:forEach>
	  	            	
	  	            	'']
	  	          ]);
	  	    	 
	  	    	var view = new google.visualization.DataView(data);

	 		/*  view.setColumns([0,
	 	    1, {
	 	      calc: function (dt, row) {
	 	    	  if(dt.getValue(row, 1) == 0){
	 	    		  return ""; 
	 	    	  }else{ 
	 	        	return dt.getValue(row, 1).toString();
	 	    	  }	
	 	      },
	 	      type: "string",
	 	      role: "annotation"
	 	    },
	 	    2, {
	 	      calc: function (dt, row) {
	 	    	
	 	    	  if(dt.getValue(row, 2) == 0){
	 	    		  return ""; 
	 	    	  }else{
	 	        	return dt.getValue(row, 2).toString();
	 	    	  }
	 	      },
	 	      type: "string",
	 	      role: "annotation"
	 	    },
	 	    3, {
	 		      calc: function (dt, row) {
	 		    	  if(dt.getValue(row, 3) == 0){
	 		    		  return ""; 
	 		    	  }else{
	 		        return dt.getValue(row, 3).toString();
	 		    	  }
	 		      },
	 		      type: "string",
	 		      role: "annotation"
	 		    },
	 		 4, {
	 			      calc: function (dt, row) {
	 			    	  if(dt.getValue(row, 4) == 0){
	 			    		  return ""; 
	 			    	  }else{ 
	 			        	return dt.getValue(row, 4).toString();
	 			    	  }
	 			      },
	 			      type: "string",
	 			      role: "annotation"
	 			 },
	 			       
	 	     {
	 	      calc: function (dt, row) {
	 	        return 0;
	 	      },
	 	      label: "Total",
	 	      type: "number"
	 	    },
	 	    {
	 	      calc: function (dt, row) {
	 	        return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4);
	 	      },
	 	      type: "number",
	 	      role: "annotation"
	 	    } 
	 	  ]); */
	    	var columns = [];
	    	for (var i = 0; i <= provinceSize ; i++) {
	    	    if (i > 0) {
	    	        columns.push(i);
	    	        columns.push({
	    	        	calc: function (dt, row) {
	 			 	    	  if(dt.getValue(row, i) == 0){
	 			 	    		  return ""; 
	 			 	    	  }else{ 
	 			 	        	return dt.getValue(row, i).toString();
	 			 	    	  }	
	 			 	      },
	 			 	      type: "string",
	 			 	      role: "annotation"
	    	        });

	    	    } else {
	    	        columns.push(i);

	    	    }
	    	}

	    	columns.push(
	    		{
		 	      calc: function (dt, row) {
		 	        return 0;
		 	      },
		 	      label: "Total",
		 	      type: "number"		  		 	   	
	        	},
	        );
	    	columns.push(
	    		{	  	    		
	    		 calc: function (dt, row) {
	    			if(provinceSize == 6){
	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5) + dt.getValue(row, 6);
	    			}
	    			if(provinceSize == 5){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5);
	  	    		}
	    			if(provinceSize == 4){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4);
	  	    		}
	    			},
		 	      type: "number",
		 	      role: "annotation"
	    	
	    	}
	    		);
	    	$.each(columns , function(index, val) { 
	    		  console.log(val)
	    		});
	    	view.setColumns(columns);


	  	          var options = {
	  	            width: 500,
	  	            height: 400,
	  	           	title: 'AQUACULTURE(MARICULTURE) - ${region_title}' ,
	  	            legend: { position: 'top', maxLines: 5 },
	  	            bar: { groupWidth: '75%' },
	  	            isStacked: true,
	  	          vAxis: {
	  	            gridlines: {color: '#007bff;'},
	  	            minValue: 0
	  	          }
	  	            
	  	          };

	  	         var chart = new google.visualization.ColumnChart(document.getElementById('chartMariculture_div'));
	  	      
	  	         google.visualization.events.addListener(chart, 'ready', function () {
	  	          // chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';

	  	          document.getElementById('pngForMariculture').outerHTML = '<a href="' + chart.getImageURI() + '" target="_blank">PRINT AQUACULTURE(MARICULTURE) CHARTS</a>';
	  	         });
	  	       
	   	        chart.draw(view, options);
	  	      }
	  	   

		 	   google.charts.setOnLoadCallback(drawHatchery);

		  	      function drawHatchery(){
		  	    	
		  	    	 var data = google.visualization.arrayToDataTable([
		  	            ['Resources',
		  	            	<c:forEach items="${provinces}" var="province">	
		  	            	'${province.province}',
		  	            </c:forEach>
		  	            	{ role: 'annotation' } ],
		  	            	
		  	            ['BFAR - LEGISLATED',
		  	            	<c:forEach items="${provinceChart}" var="entry">
		  	            		<c:if test = "${entry.resources == 'BFAR_HATCHERY'}">	            		
		  	            			${entry.total},
		  	            		</c:if>		  	            	
		  					</c:forEach>
		  					''
		  	            	],
		  	            ['LGU',
		  	            	
		  	            	<c:forEach items="${provinceChart}" var="entry">
		  	            		<c:if test = "${entry.resources == 'LGU_HATCHERY'}">
		          					${entry.total},
		          				</c:if>
		 					</c:forEach>
		  	            	
		  	            	''],
		  	            ['PRIVATE',
		  	            	
		  	            	<c:forEach items="${provinceChart}" var="entry">
		  	            		<c:if test = "${entry.resources == 'PRIVATE_HATCHERY'}">
		          					${entry.total},
		          				</c:if>
		 					</c:forEach>
		  	            	
		  	            	'']
		  	          ]);
		  	    	 
		  	    	var view = new google.visualization.DataView(data);
		  	    	var columns = [];
		  	    	for (var i = 0; i <= provinceSize ; i++) {
		  	    	    if (i > 0) {
		  	    	        columns.push(i);
		  	    	        columns.push({
		  	    	        	calc: function (dt, row) {
			 			 	    	  if(dt.getValue(row, i) == 0){
			 			 	    		  return ""; 
			 			 	    	  }else{ 
			 			 	        	return dt.getValue(row, i).toString();
			 			 	    	  }	
			 			 	      },
			 			 	      type: "string",
			 			 	      role: "annotation"
		  	    	        });

		  	    	    } else {
		  	    	        columns.push(i);
		  	    	    }
		  	    	}
		  	    	
		  	    	columns.push(
		  	    		{
		  		 	      calc: function (dt, row) {
		  		 	        return 0;
		  		 	      },
		  		 	      label: "Total",
		  		 	      type: "number"		  		 	   	
  	    	        	},
  	    	        );
		  	    	columns.push(
		  	    		{	  	    		
		  	    		 calc: function (dt, row) {
		  	    			/* for (var i = 1; i <= provinceSize ; i++) {
		  	    				if(i == provinceSize){
		  	    				
		  	    					break;
		  	    				}else{
		  	    		
		  	    				total += dt.getValue(row,i);
		  	    				}
		  	    				}
		  	    		
		  	    			return total; */
		  	    			if(provinceSize == 6){
		  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5) + dt.getValue(row, 6);
		  	    			}
		  	    			if(provinceSize == 5){
			  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5);
			  	    		}
		  	    			if(provinceSize == 4){
			  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4);
			  	    		}
		  	    			},
		 		 	      type: "number",
		 		 	      role: "annotation"
		  	    	
		  	    	}
		  	    		);
		  	    	
		  	    	view.setColumns(columns);

		  	    	/* view.setColumns([0,
		 	   
		 		  		1, {
		 			 	      calc: function (dt, row) {
		 			 	    	  if(dt.getValue(row, i) == 0){
		 			 	    		  return ""; 
		 			 	    	  }else{ 
		 			 	        	return dt.getValue(row, i).toString();
		 			 	    	  }	
		 			 	      },
		 			 	      type: "string",
		 			 	      role: "annotation"
		 			 	    },
	
		 			 
		 	     2, {
		 	      calc: function (dt, row) {
		 	    	
		 	    	  if(dt.getValue(row, 2) == 0){
		 	    		  return ""; 
		 	    	  }else{
		 	        	return dt.getValue(row, 2).toString();
		 	    	  }
		 	      },
		 	      type: "string",
		 	      role: "annotation"
		 	    },
		 	    3, {
		 		      calc: function (dt, row) {
		 		    	  if(dt.getValue(row, 3) == 0){
		 		    		  return ""; 
		 		    	  }else{
		 		        return dt.getValue(row, 3).toString();
		 		    	  }
		 		      },
		 		      type: "string",
		 		      role: "annotation"
		 		    },
		 		 4, {
		 			      calc: function (dt, row) {
		 			    	  if(dt.getValue(row, 4) == 0){
		 			    		  return ""; 
		 			    	  }else{ 
		 			        	return dt.getValue(row, 4).toString();
		 			    	  }
		 			      },
		 			      type: "string",
		 			      role: "annotation"
		 			 },
		 			 5, {
		 			      calc: function (dt, row) {
		 			    	  if(dt.getValue(row, 5) == 0){
		 			    		  return ""; 
		 			    	  }else{ 
		 			        	return dt.getValue(row, 5).toString();
		 			    	  }
		 			      },
		 			      type: "string",
		 			      role: "annotation"
		 			 },	 
		 			6, {
		 			      calc: function (dt, row) {
		 			    	  if(dt.getValue(row, 6) == 0){
		 			    		  return ""; 
		 			    	  }else{ 
		 			        	return dt.getValue(row, 6).toString();
		 			    	  }
		 			      },
		 			      type: "string",
		 			      role: "annotation"
		 			 }, 
		 	     {
		 	      calc: function (dt, row) {
		 	        return 0;
		 	      },
		 	      label: "Total",
		 	      type: "number",
		 	    },
		 	    {
		 	      calc: function (dt, row) {
		 	        return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5) + dt.getValue(row, 6);
		 	      },
		 	      type: "number",
		 	      role: "annotation"
		 	    } 
		 	  ]);*/

		  	          var options = {
		  	            width: 500,
		  	            height: 400,
		  	           	title: 'AQUACULTURE(HATCHERY) - ${region_title}' ,
		  	            legend: { position: 'top', maxLines: 5 },
		  	            bar: { groupWidth: '75%' },
		  	            isStacked: true,
		  	          vAxis: {
			  	            gridlines: {color: '#007bff;'},
			  	            minValue: 0
			  	          }
		  	            
		  	          };

		  	         var chart = new google.visualization.ColumnChart(document.getElementById('chartHatchery_div'));
		  	      
		  	         google.visualization.events.addListener(chart, 'ready', function () {
		  	          // chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';

		  	          document.getElementById('pngForHatchery').outerHTML = '<a href="' + chart.getImageURI() + '" target="_blank">PRINT AQUACULTURE(Hatchery) CHARTS</a>';
		  	         });
		  	       
		   	        chart.draw(view, options);
		  	      }

		 	   	      	      
 	      
</script>
</body>

</html>