


<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html>
<head>
<link rel="icon" type="image/png" href="<c:url value='/static/images/favicon.png' />" />
<title></title>			 
<script  src="/static/js/jquery-1.8.3.min.js" type=" text/javascript"></script>
<script src="/static/js/bootstrap.min.js" type=" text/javascript"></script>
<script src="/static/js/navbar.js"></script>
<jsp:include page="../menu/css.jsp"></jsp:include>

</head>
<body>
    <script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
	
	<div class="se-pre-con"></div>	
<%-- 		<div class="navbar navbar-expand-md navbar-dark  bg-dark mb-4"
		role="navigation">
		<!--<a class="navbar-brand" href="#">Bootstrap NavBar</a>  -->
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarCollapse" aria-controls="navbarCollapse"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav mr-auto">
				<!-- <li class="nav-item active"><a class="nav-link" href="#">Home
						<span class="sr-only">(current)</span>
				</a></li> -->
				</ul>
				
			<ul class="navbar-nav navbar-right">
				<li><a class="nav-link" href="<c:url value="/logout" />"><span
						class="glyphicon glyphicon-log-out"></span> LOGOUT</a></li>
			</ul>
		</div>
	</div>
	 --%>

 <jsp:include page="../users/header.jsp"></jsp:include>
<jsp:include page="../menu/navigation.jsp"></jsp:include>
<br>	
	<div class="container-fluid text-center">
		<div class="row content">
			 <div class="col-md-2 panel panel-default">
				<!-- <nav class="nav"> -->
						<jsp:include page="../menu/leftForm.jsp"></jsp:include>
				<!-- </nav> -->
			</div>
			<div class="col-md-8 panel panel-default"
				style="box-shadow: 0 3px 3px 1px #000000;" id='section-to-print'>
					<div id="map"></div>
			</div>
			<div class="col-md-2 sidenav site-navigation">
				<%-- <nav class="nav">
					<jsp:include page="../menu/rightForm.jsp"></jsp:include>
				</nav> --%>
			</div>
		</div>
	</div>
  		
  			<div id="details" class="clock leaflet-control"></div>
    	<!-- 	
  			<div id="view_user" style="height: 500px; overflow: auto; background-color: white;"></div>
		 -->	
  		<!-- <div  id="details"></div> -->
		<!--</div>-->
	<!--<div class="col-md-4">			
		</div>  -->	
<!--  	</div>
</div>
-->
   <div id="footer">
   <jsp:include page="footer.jsp"></jsp:include>
   </div>

<script>
var url = '/static/geojson/'+ ${region_map} +'.geojson'; 
console.log("url: " + url);
/*  var region = ${region};
console.log("region: " + region); */ 
var arr = [];
var arr1 = [];
//var region = ${region_map};

 //var map_location = ${mapsData}; 
 // var map_location;
 /*
 function getJSONData(){
  $.getJSON("data", function(data) {
	console.log(data);
	map_location = data;								
	});
	}
	window.onload = getJSONData;
*/	
localStorage.setItem('regionmap', JSON.stringify(url));
const map_data = JSON.parse(localStorage.getItem('regionmap'));
  // Initialize autocomplete with empty source.
 // $( "#autocomplete" ).autocomplete(); zoomControl: false,
	// REGION 5 - 12.781953, 123.097564
	// REGION 1 - 16.913544, 120.879809
	// REGION 2 - 18.809179, 121.037159
	// REGION 3 - 15.542543, 121.078241
	// REGION 1VA - 14.191743, 121.786513
	// REGION IVB - 9.902934, 121.051223 .setView([9.902934, 121.051223],7);

var map =L.map('map',{scrollWheelZoom:false}).setView([${set_center}], 8); 
  var OpenTopoMap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
    opacity: 0.90
  });
  
  OpenTopoMap.addTo(map);

	map.zoomControl.setPosition('topright');
	var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
				maxZoom: 18,
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'});
	

	//osm.addTo(map);
	// https: also suppported.
	 var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
	});
	
	// https: also suppported.
	var Esri_WorldGrayCanvas = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'});
		
	var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {	
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
	});
	
	
	/* var wmsLayer = L.tileLayer.wms('http://geo.bfar.da.gov.ph/gwc/service/wms', {
    layers: 'region:Region_I'
	});
	 */
	
L.control.scale().addTo(map);
	 
		
		
/* 		
		function getColor(d) {
		    return d > 1000 ? '#800026' :
		           d > 500  ? '#BD0026' :
		           d > 200  ? '#E31A1C' :
		           d > 100  ? '#FC4E2A' :
		           d > 50   ? '#FD8D3C' :
		           d > 20   ? '#FEB24C' :
		           d > 10   ? '#FED976' :
		                      '#FFEDA0';
		}	
		
		var legend = L.control({position: 'bottomleft'});
		legend.onAdd = function (map) {

		var div = L.DomUtil.create('div', 'info legend');
		labels = ['<strong>Categories</strong>'],
		categories = ['Road Surface','Signage','Line Markings','Roadside Hazards','Other'];

		for (var i = 0; i < categories.length; i++) {

		        div.innerHTML += 
		        labels.push(
		            '<a href="/frgis/usermap/fishsanctuaries">' +
		        (categories[i] ? categories[i] : '+') +'</a> ');
					
		    }
		    div.innerHTML = labels.join('<br>');
		return div;
		};
		legend.addTo(map); */

function style(feature) {
    return {
        fillColor: 'green', 
        fillOpacity: 0.5,  
        weight: 2,
        opacity: 1,
        color: '#ffffff',
        dashArray: '3'
    };
}

	var highlight = {
		'fillColor': 'yellow',
		'weight': 2,
		'opacity': 1
	};

	
		function forEachFeature(feature, layer) {
            layer._leaflet_id = feature.properties.REGION;
            var popupContent = "<p><b>REGION: </b>" + feature.properties.REG_NM + "(" + feature.properties.REG_VAR_NM +")" + "</br><b>PROVINCE: </b>" + feature.properties.PRV_NM + "</br><b>MUNICIPALITY: </b>" + feature.properties.NAME +'</p>';

            layer.bindPopup(popupContent);
            
            layer.on("click", function (e) { 
                stateLayer.setStyle(style);
                layer.setStyle(highlight);
                
            }); 
            
		}

var stateLayer = L.geoJson(null, {onEachFeature: forEachFeature, style: style});

	$.getJSON(map_data, function(data) {
	
        stateLayer.addData(data);

    });

 stateLayer.addTo(map);

  		function polySelect(a){
  	//	console.log("polySelect(a): " + a);
			map._layers[a].fire('click'); 
			var layer = map._layers[a];
			map.fitBounds(layer.getBounds());
        }
 //       var geojsonLayer = new L.GeoJSON.AJAX(map_data);
// var geojsonLayer = new L.GeoJSON.AJAX("/frgis/static/geojson/Bicol.geojson");  
//  geojsonLayer.refilter(function(feature){
 //   return feature.properties.key === values;
//});
     
//geojsonLayer.addTo(map);       
        
var baseMaps = {
    "Open Street Map": osm,
   	"OSM B&W":OpenStreetMap_BlackAndWhite,
   	"Esri WorldI magery":Esri_WorldImagery,
   	"Esri World Gray Canvas":Esri_WorldGrayCanvas,
   //	"Geo map" :wmsLayer
   	
};

/* var overlayMaps = {
   "PHILIPPINES":geojsonLayer
};	 */
	
//L.control.layers(baseMaps, overlayMaps).addTo(map);
L.control.layers(baseMaps).addTo(map);
//L.control.layers(baseMaps).addTo(map);	
		var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});


 </script>

<script>
//paste this code under the head tag or in a separate js file.
	// Wait for window load
	 $(window).on('load', function () {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");
	});
	</script>
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<!-- <script src="/frgis/static/js/form/script.js" type=" text/javascript"></script>
 --><!--<script type="text/javascript" src="/frgis/static/js/output.min.js"></script>  -->
<!--<script type="text/javascript" src="/frgis/static/js/map/mapViewByResources.js"></script>  -->
<!--<script type="text/javascript" src="/frgis/static/js/form/viewListOnMap.js"></script>  -->

    <div id="myModal" class="modals"> 
  		<div class="close_id">
  			<a href="#"><span class="close">&times;</span></a></div>
  			<img class="modal-content" id="img01" >
  			<div class="modal-map"></div>
  		<div id="caption"></div>
	</div>
	<!-- 
	  <div class="modal fade" id="myModalForms" role="dialog">
     <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">DOWNLOADABLE FORMS</h4>
        </div>
       
        <div class="modal-body">
        <jsp:include page="../menu/form.jsp"></jsp:include> 
        </div>
       
        <div class="modal-footer">
          <button type="button" id="CLOSE" class="btn btn-default" data-dismiss="modal">CLOSE</button>
        </div>
      </div>
      
    </div>
  </div>
 -->
</body>
</html>