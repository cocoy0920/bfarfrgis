<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Seagrass</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />

<jsp:include page="../../menu/css.jsp"></jsp:include>
<script src="/static/js/form/deleteMapList.js" type=" text/javascript"></script>
<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
	<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
	<script src="//d3js.org/d3.v3.min.js"></script>
</head>
<body onload="getFisheriesData()">
<jsp:include page="../../home/header_users.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">
  <!-- Sidebar Holder -->
<%-- <jsp:include page="../../menu/sidebar2.jsp"></jsp:include> --%>

  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5 section-to-print">
					<div id="map">
						<div class="leaflet-control leaflet-top leaflet-center col-sm-2 col-sm-offset-1">
              			<img alt="" src="/static/images/FRGIS-LOGO.png" style="height: 50px; width: 50px">
              		</div>
					</div>
			</div>
			</div>
   <!--  <div id="details" class="clock leaflet-control"></div> -->
<div class="primary-color">
	<p>
	Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved Bureau of Fisheries and Aquatic Resources
	</p>
</div>
<script src="/static/leaflet/bundle.js"></script>
<script  src="/static/js/api/regionMap.js" ></script>
	<script>
	
	 var map_location = ${mapsData}; 
	 
	 
	 var map =L.map('map',{scrollWheelZoom:false}).setView([${lat}, ${lon}],6);

	 map.zoomControl.setPosition('topright');
	 var tiles = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}.png', {
	 		maxZoom: 18});

	 tiles.addTo(map);
	 getMapByRegion();
/* 	var wmsLayer = L.tileLayer.wms('http://geo.bfar.da.gov.ph/gwc/service/wms', {
    layers: 'region:Region_I'
	});
	 */
	
L.control.scale().addTo(map);

function style(feature) {
    return {
        fillColor: 'green', 
        fillOpacity: 0.5,  
        weight: 2,
        opacity: 1,
        color: '#ffffff',
        dashArray: '3'
    };
}

	var highlight = {
		'fillColor': 'yellow',
		'weight': 2,
		'opacity': 1
	};

	
		function forEachFeature(feature, layer) {
            layer._leaflet_id = feature.properties.REGION;
            var popupContent = "<p><b>REGION: </b>" + feature.properties.REG_NM + "(" + feature.properties.REG_VAR_NM +")" + "</br><b>PROVINCE: </b>" + feature.properties.PRV_NM + "</br><b>MUNICIPALITY: </b>" + feature.properties.NAME +'</p>';

            layer.bindPopup(popupContent);
            
            layer.on("click", function (e) { 
                stateLayer.setStyle(style);
                layer.setStyle(highlight);
                
            }); 
            
		}

  		function polySelect(a){
  		console.log("polySelect(a): " + a);
			map._layers[a].fire('click'); 
			var layer = map._layers[a];
			map.fitBounds(layer.getBounds());
        }
  //    var geojsonLayer = new L.GeoJSON.AJAX(map_data);

     
//geojsonLayer.addTo(map);       
        
/* var baseMaps = {
    "Open Street Map": osm,
   	"OSM B&W":OpenStreetMap_BlackAndWhite,
   	"Esri WorldI magery":Esri_WorldImagery,
   	"Esri World Gray Canvas":Esri_WorldGrayCanvas
   //	"Geo map" :wmsLayer
   	
}; */

/*  var overlayMaps = {
   "PHILIPPINES":geojsonLayer
};
	 */
//L.control.layers(baseMaps).addTo(map);
//L.control.layers(baseMaps).addTo(map);	
		var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});


		function getFisheriesData(){
			seaGrass(map_location);
		}
    	var printer = L.easyPrint({
      		tileLayer: tiles,
      		sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
      		filename: 'myMap',
      		exportOnly: true,
      		hideControlContainer: true
		}).addTo(map);

</script>
<script type="text/javascript">
$('#sidebarCollapse').on('click', function () {
    $('#sidebar').toggleClass('active');
});
</script>
    </body>
</html>