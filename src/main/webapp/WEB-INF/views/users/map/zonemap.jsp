<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Mariculture Zone</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />

<jsp:include page="../../menu/css.jsp"></jsp:include>
<script src="/static/js/form/deleteMapList.js" type=" text/javascript"></script>
<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
	<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
	<script src="//d3js.org/d3.v3.min.js"></script>
</head>
<body onload="getFisheriesData()">
<jsp:include page="../../home/header_users.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">
  <!-- Sidebar Holder -->
<%-- <jsp:include page="../../menu/sidebar2.jsp"></jsp:include> --%>

  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5 section-to-print">
					<div id="map">
						<div class="leaflet-control leaflet-top leaflet-center col-sm-2 col-sm-offset-1">
              			<img alt="" src="/static/images/FRGIS-LOGO.png" style="height: 50px; width: 50px">
              		</div>
					</div>
			</div>
  </div>
<div class="primary-color">
	<p>
	Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved Bureau of Fisheries and Aquatic Resources
	</p>
</div>
<script src="/static/leaflet/bundle.js"></script>
<script  src="/static/js/api/regionMap.js" ></script>
	<script>
	
	 var map_location = ${mapsData}; 
	 var map =L.map('map',{
		 scrollWheelZoom:false,
		fullscreenControl: {
	        pseudoFullscreen: false,
	        position:'topright'
	    }
		 });

map.zoomControl.setPosition('topright');

var tiles = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}.png', {
	maxZoom: 18});

var OpenTopoMap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
    opacity: 0.90
  });

var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
				maxZoom: 18,
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'});
	
var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {	
		 maxZoom: 10,
		attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
	});

var Esri_WorldGrayCanvas = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'});
		
var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {	
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
	});
getMapByRegion();	
/* 	var wmsLayer = L.tileLayer.wms('http://geo.bfar.da.gov.ph/gwc/service/wms', {
    layers: 'region:Region_I'
	});
	 */
	 var baseMaps = {
	  		    "Open Street Map": osm,
	  		   	"Esri WorldI magery":Esri_WorldImagery,
	  		   	"Esri World Gray Canvas":Esri_WorldGrayCanvas,
	  		   	"Topo Map":OpenTopoMap,
	  		   	"Word Street Map":tiles

};
	
L.control.scale().addTo(map);
L.control.layers(baseMaps).addTo(map);	



		function getFisheriesData(){
			getMaricultureZone(map_location);
		}
    	var printer = L.easyPrint({
      		tileLayer: tiles,
      		sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
      		filename: 'myMap',
      		exportOnly: true,
      		hideControlContainer: true
		}).addTo(map);

</script>

    </body>
</html>