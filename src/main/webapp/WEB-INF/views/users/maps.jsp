<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>FRGIS</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="icon" 
      type="image/png" 
      href="<c:url value='/static/images/favicon.png' />" />
        <style>
        #mapContainer{
            height: 480px;
        }
        #download {
            position:absolute;
            top:10px;
            right:10px;
            z-index:1000;
        }
        
        #cover {
            display: none;
            text-align: center;
            padding-top: 200px;
            background: #CCC;
            opacity: 0.5;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 10000;
        }
        
        #cover.active{
            display: block;
        }
        
        .info {  
  /*position: absolute;
  bottom: 5px;
  left: 5px;
  z-index: 400;
  padding: 5px;
  width: 20%;
  font-family: Verdana, Tahoma, Helvetica, Arial, sans-serif;
  font-size: 8pt;
   background-color: rgba(255, 255, 255, 0.5); */
  /* opacity: 0.5; */
  /* For IE8 and earlier */
  /* filter: alpha(opacity=50); */  
 /* box-shadow: 0 1px 5px rgba(0,0,0,0.65);
	border-radius: 4px;*/
}

table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}
caption {
  background-color: #aeaeae;
}
th{
  border: 1px solid #dddddd;
}
td{
  border: 1px solid #dddddd;
}

tr:nth-child(odd) {
  background-color: #dddddd;
}
        </style>

<!--
<script type="text/javascript">
    a = {
        show: function(elem) {
            document.getElementById(elem).style.visibility = 'visible';
        },
        hide: function(elem) {
            document.getElementById(elem).style.visibility = 'hidden';
        }
    }
    
    
</script>
  -->
  <!-- 
<script>
const myObjStr = JSON.stringify('${provincess}');
const objStr = '${provincess}';
const res = objStr.substring(1, objStr.length-1);
var optionValue="";
JSON.parse(res).forEach(function(element) {
	 					optionValue = optionValue + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
	           
	  			})
</script>
 -->
<jsp:include page="../menu/css.jsp"></jsp:include>


</head>
<body>
<!-- <div class="se-pre-con"></div> -->
<sec:authorize access="hasRole('USER')">

		<c:choose>
			<c:when test="${page == '404'}">
				<div class="inner-bg">
					<div class="container">
						
						<p>HTTP Status 404 - Page Not Found</p>
						<p>
							The page you requested is not available. You might try returning
							to the <a href="../logout">LOGOUT</a>.
						</p>


					</div>
				</div>

			</c:when>

			<c:otherwise>
			
			<br><br>
 <jsp:include page="header.jsp"></jsp:include>

  <nav class="navbar navbar-inverse navbar-fixed-top" id="modal_content">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <!--<a class="navbar-brand" href="#">Logo</a>  -->
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="<c:url value="resourceMaplist" />">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Contact</a></li>
        <!-- <li><a href="#" id="newpage">Add New Data</a></li> --> 
         <!--<li><a href="#" id="contentR" data-toggle="modal" data-target="" data-backdrop="static" data-keyboard="false">Add New Data</a></li>  -->
         <li><a href="fishsanctuaries">Add New Data</a></li>
         
          <!-- <li><a href="#" id="contentList" data-toggle="modal" data-target="#myModalList" data-backdrop="static" data-keyboard="false">List</a>
         </li>
          -->
           <!-- 
         <li><a href="resourceList">List of Resources</a>
         </li>
         -->
        <li><a href="#" id="printForms" data-toggle="modal" data-target="#myModalForms" data-backdrop="static" data-keyboard="false">Forms</a>
        </li>
        
		<li><a href="#" id="print">Print</a></li>
       <!--<c:if test="${listPage != null}">
        <li><a href="#"><input type="button" value="   List   " data-toggle="modal" data-target="#myModal"></a>
        </li>
      
      </c:if>-->
     	
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">
        <span><strong>${loggedinuser}</strong>, Welcome to FRGIS</span>
        </a>
        </li>
       
        <li><a href="<c:url value="/logout" />"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        
      </ul>
    </div>
  </div>
</nav>

  <!-- Begin page content -->
 <div class="container-fluid text-center">    
  <div class="row content">
      <div class="nav col-md-2 sidenav site-navigation">
     <nav class="nav">
      <jsp:include page="../menu/left.jsp"></jsp:include>
 </nav>
      </div>
      
      <div class="col-md-8 panel panel-default" style="box-shadow: 0 3px 3px 1px #000000;" id='section-to-print'>
		         <div id="cover">Generating pdf</div>   	
     	 <div id="mapContainer"></div>
     	 <div class="info" id="info">
  
		</div>
     	<!--  <button id="download">download</button>
		<script>L_PREFER_CANVAS = true</script>
		 -->
		
        <!-- <script src="<c:url value='/static/js/form/leaflet-image.js' />" type=" text/javascript"></script>
		<script src="<c:url value='/static/js/form/jspdf.debug.js' />" type=" text/javascript"></script>
 		-->
 		<script src="/frgis/static/leaflet/leaflet.js"></script>
    <script>
    
    
   // var data =  ${maps};

    	//var getMap = 'https://geo.bfar.da.gov.ph/gwc/service/wms';
    	//var mapLayer = ${region_map};
    	var getMap = 'http://localhost:8083/geoserver/wms?';
    	var mapLayer = "region:Region_I";
     	//var map = L.map('mapContainer').setView([${set_center}], 7),
     	var map = L.map('mapContainer').setView([16.86401,120.43213], 7),
		
    	vector = L.geoJson().addTo(map);
    	 var wmsLayer = L.tileLayer.wms(getMap, {
    		layers: mapLayer,
    		attribution: "FIMC",
    		transparent: true,
    		 format:'image/png',
    		 minZoom: 7,
    		maxZoom: 15
		}).addTo(map); 
         
    
          var legend = L.control({position: 'bottomright'});

         legend.onAdd = function (map) {

             var div = L.DomUtil.create('div', 'info legend'),
                 grades = [0, 10, 20, 50, 100, 200, 500, 1000],
                 labels = [];
                 div.innerHTML += " <img src='/frgis/static/images/nswe.jpeg' height='50' width='50'>";

             return div;
         };

         legend.addTo(map);
         
           var regionLegend = L.control({position: 'bottomleft'});

         regionLegend.onAdd = function (map) {

             var div = L.DomUtil.create('div', 'info legend'),
                 grades = [0, 10, 20, 50, 100, 200, 500, 1000],
                 labels = [];
                 div.innerHTML += ${region_name} ;

             return div;
         };

         regionLegend.addTo(map);
                  
                    	var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});
            
  
  ////PDF/////
  /*
    document.getElementById('download').addEventListener('click', function() {
                cover.className = 'active';
                leafletImage(map, downloadMap);
            });
            function downloadMap(err, canvas) {
                var imgData = canvas.toDataURL("image/svg+xml", 1.0);
                var dimensions = map.getSize();
                
                var pdf = new jsPDF('l', 'pt', 'letter');
                pdf.addImage(imgData, 'PNG', 10, 10, dimensions.x * 0.5, dimensions.y * 0.5);
                
                cover.className = '';
                pdf.save("download.pdf");
            };
            */
  ////PDF END////
  
  
  
  
          
          var fishSanctuaryLayerGroup = false; 
          
          var getAllMaps = new Array();
       	  var layerControl = false;
  
       	var role = ${role};		
  		var page;
  		var latestpage;
  		
 function refreshRecord(value,link,status)
    {
console.log("value: " + value);
console.log("link: " + link);
console.log("status: " + status);

       data = value;
		page = link;
/*
	if(status === 'new'){
	getRecordList(page);
	}
*/
			if(page == "fishsanctuaries"){	
			//viewListOnMap.js				
				fishsanctuariesGetListMap(data,role);  	
				
 	 		}
 	 		if(page == "fishprocessingplants"){
 	 			fishprocessingplantsGetListMap(data,role); 	
      				
 	 		}
		 	if(page == "fishlanding"){
				fishlandingGetListMap(data,role);         		
 	 		}   
 	 	 	if(page == "fishpen"){
 	 	 		fishpenGetListMap(data,role);
 	 		} 

			if(page == "fishcage"){
				fishcageGetListMap(data,role);
			} 
 	 	 	if(page == "fishpond"){
 				fishpondGetListMap(data,role);
 			} 	 	        		
        	if(page == "hatchery"){
 	 			hatcheryGetListMap(data,role);
 	 		}
 	 	 	if(page == "iceplantorcoldstorage"){
 	 			iceplantorcoldstorageGetListMap(data,role);
 	 		}
 	 		if(page == "fishcorals"){
 	 			fishcorralsGetListMap(data,role);
 	 		}  
 	 		if(page == "FISHPORT"){
 	 			FISHPORTGetListMap(data,role);
 	 		}   
 	 		if(page == "marineprotected"){
 	 			marineprotectedGetListMap(data,role);
 	 		} 
 	 		if(page == "market"){ 
 	 			marketGetListMap(data,role);
 	 		} 
 	 		if(page == "schooloffisheries"){ 
 	 			schooloffisheriesGetListMap(data,role);
 	 		} 
 	 		if(page == "seagrass"){ 	 	
 	 			seagrassGetListMap(data,role);
 	 		} 
 	 		if(page == "seaweeds"){
        			seaweedsGetListMap(data,role);
 	 		}
 	 		if(page == "mangrove"){      		
 	 			mangroveGetListMap(data,role);
 	 		}
 	 		if(page == "lgu"){        		
        		lguGetListMap(data,role);
 	 		}
 	 		if(page == "trainingcenter"){
        		trainingcenterGetListMap(data,role);
 	 		}
 	 		if(page == "mariculturezone"){
 	 			mariculturezoneGetListMap(data,role);
 	 		}
  
	//}

       	 
    }
/*
	 function getRecordList(link){

       if(link == "fishsanctuaries"){	
       		
				FishSanctuaryList(true);
 	 		}
 	 		if(link == "fishprocessingplants"){ 	 				
      			FisProcessingList(true); 	
 	 		}
		 	if(link == "fishlanding"){
				      FishLandingList(true); 		
 	 		}   
 	 	 	if(link == "fishpen"){
 	 	 		FishPenList(true);
 	 		} 

			if(link == "fishcage"){
				FishCageList(true);
			} 
 	 	 	if(link == "fishpond"){
 				FishPondList(true);
 			} 	 	        		
        	if(link == "hatchery"){
 	 			HatcheryList(true);
 	 		}
 	 	 	if(link == "iceplantorcoldstorage"){
 	 			IcePlantColdStorage(true);
 	 		}
 	 		if(link == "fishcorals"){
 	 			FishCorralList(true);
 	 		}  
 	 		if(link == "FISHPORT"){
 	 			FishPortList(true);
 	 		}   
 	 		if(link == "marineprotected"){
 	 			MarineProtectedAreaList(true);
 	 		} 
 	 		if(link == "market"){ 
 	 			MarketList(true);
 	 		} 
 	 		if(link == "schooloffisheries"){ 
 	 			 SchoolOfFisheriesList(true);
 	 		} 
 	 		if(link == "seagrass"){ 	 	
 	 			 SeaGrassList(true);
 	 		} 
 	 		if(link == "seaweeds"){
        			SeaweedsList(true);
 	 		}
 	 		if(link == "mangrove"){      		
 	 			MangroveList(true);
 	 		}
 	 		if(link == "lgu"){        		
        	 LGUList(true);
 	 		}
 	 		if(link == "trainingcenter"){
        		 TrainingCenterList(true);
 	 		}
 	 		if(link == "mariculturezone"){
 	 			 MaricultureZoneList(true);
 	 		}
 	 		
}
	*/	
        </script>
        
        <br><br><br>
		<div class="tableToPrint"></div>
	  </div>

      
      <div class="col-md-2 sidenav site-navigation">
      <nav class="nav">
      <jsp:include page="../menu/right.jsp"></jsp:include>
      </nav>
      </div>
    
    </div>
</div>



	<script type="text/javascript">	 
	 	document.querySelector("#print").addEventListener("click", function() {
			window.print();
		});

	</script>
	 
  <div class="modal fade" id="myModalForms" role="dialog">
     <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">DOWNLOADABLE FORMS</h4>
        </div>
       
        <div class="modal-body">
        <jsp:include page="../menu/form.jsp"></jsp:include> 
        </div>
       
        <div class="modal-footer">
          <button type="button" id="CLOSE" class="btn btn-default" data-dismiss="modal">CLOSE</button>
        </div>
      </div>
      
    </div>
  </div>
 <jsp:include page="../menu/script.jsp"></jsp:include>
 <script>

      $(document).ready(function(){   
      
    var container = $('#myModalList .modal-dialog .modal-content .modal-body');
	  var trigger = $('.nav ul li a'); 
      trigger.on('click', function(){
    	  
          var $this = $(this),
            target = $this.data('target');       
          	 	 
        		 
       		   $.get("map/" + target, function(content, status){
        	 			console.log(content + " THIS IS BULLSHIT!!!!");
         			refreshRecord(content,target,"new");
    	
    		  });
    	  
      	

          return false;
        });
        
     });

</script>

<script type="text/javascript">
     
///////////////////////////////////////////////////////////////////// 
    $(".fishsanctuaries_id a").live('click',function(e){  
     
     		id =  $(this).attr('id');    
     		console.log("ID: " + id);	
     	$.get("info/" + id, function(data, status){ 
     				//getdata.js      
     				
     				for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
					
						console.log("ID: " + obj.id);	
						 var markup = 
		        "<tr><td scope=\"col\">FISH SANCTUARY LOCATION</td><td scope=\"col\">&nbsp;</td></tr>"+
	            "<tr><td scope=\"row\">Region</td><td>" + obj.region + "</td></tr>"+ 
	            "<tr><td scope=\"row\">Province</td><td>" + obj.province + "</td></tr>"+ 
	            "<tr><td scope=\"row\">Municipality</td><td>" + obj.municipality + "</td></tr>"+
	            "<tr><td scope=\"row\">Barangay</td><td>" + obj.barangay + "</td></tr>"+
	            "<tr><td scope=\"col\">FISH SANCTUARY INFORMATION</td><td scope=\"col\">&nbsp;</td></tr>"+
	            "<tr><td scope=\"row\">Name of Sanctuary/Protected Area:</td><td>"+obj.nameOfFishSanctuary+"</td></tr>"+
	            "<tr><td scope=\"row\">Implementer:</td><td scope=\"col\">&nbsp;</tdd></tr>"+
	            "<tr><td scope=\"col\">BFAR</td><td scope=\"col\">&nbsp;</td></tr>"+
	           "<tr><td scope=\"row\">Name of Sheltered Fisheries/Species:</td><td>" + obj.sheltered + "</td></tr>"+
	           "<tr><td scope=\"col\">DENR</td><td scope=\"col\">&nbsp;</td></tr>"+
	           "<tr><td scope=\"row\">Name of Sensitive Habitat with MPA:</td><td>" + obj.nameOfSensitiveHabitatMPA + "</td></tr>"+
	           "<tr><td scope=\"row\">Ordinance No:</td><td>" + obj.OrdinanceNo + "</td></tr>"+
	          "<tr><td scope=\"row\">Ordinance Title:</td><td>" + obj.OrdinanceTitle + "</td></tr>"+
	          "<tr><td scope=\"row\">Data Established:</td><td>" + obj.DateEstablished + "</td></tr>"+
	          "<tr><td scope=\"row\">Area:</td><td>" + obj.area + "</td></tr>"+
	          "<tr><td scope=\"row\">Type:</td><td>" + obj.type + "</td></tr>"+
	           "<tr><td scope=\"col\">ADDITIONAL INFORMATION</td><td scope=\"col\">&nbsp;</td></tr>"+
	          "<tr><td scope=\"row\">Date as of:</td><td>" + obj.dateAsOf + "</td></tr>"+
	          "<tr><td scope=\"row\">Data Sources:</td><td>" + obj.fishSanctuarySource + "</td></tr>"+
	          "<tr><td scope=\"row\">Remarks:</td><td>" + obj.remarks + "</td></tr>"+
	          "<tr><td  scope=\"col\">GEOGRAPHICAL LOCATION</td><td scope=\"col\">&nbsp;</td></tr>"+
	          "<tr><td scope=\"row\">Latitude:</td><td>" + obj.lat + "</td></tr>"+
	          "<tr><td scope=\"row\">Longitude:</td><td>" + obj.lon + "</td></tr>"+
	          "<tr><td scope=\"row\" colspan=\"2\"><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td><td scope=\"col\">&nbsp;</td></tr>"; 
	         
	         console.log(markup);    
	        // document.getElementById("myList").appendChild(markup); 
	          var div = document.getElementById('info');
	            var tb = "<table>" + markup + "</table>"
	            div.innerHTML = tb;         
	            	}
     			
     	});
        	
    });
///////////////////////////////////////////////////////////////////// 
        
</script>


				</c:otherwise>
		</c:choose>


	</sec:authorize>
	

</body>
</html>