<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="en">
<head>
<title>Training Center</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
	<jsp:include page="../menu/css.jsp"></jsp:include>
<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
<script src="/static/leaflet/leaflet.js"></script>
<script src="/static/leaflet/leaflet.ajax.min.js"></script>
<script src="//d3js.org/d3.v3.min.js"></script>
<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
<script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
<script src="/static/leaflet/bundle.js"></script>
<link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
<script src="/static/leaflet/L.Control.SlideMenu.js"></script>
<script src="/static/leaflet/leaflet-bootstrapmodal.js"></script>
<script src="https://cdn.jsdelivr.net/npm/exif-js"></script>

<style type="text/css">
.table2excel{
	padding: 5px;
}
#map{ 
	width:100%;
  	height: 600px;
   background: #009dc4;
}
</style>
</head>
<body onload="getFisheriesData()">

  <jsp:include page="../home/header_users.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">

    <div id="content" class="p-4 p-md-5 pt-5 section-to-print" >
       <c:if test="${mapsData != '' }">
    <div id="mapview">
    <div id="map"></div>
    </div>
    </c:if>
    <c:if test="${mapsData == '' }">
    <div id="mapview"  style="display:none;">
    <div id="map"></div>
    </div>
    </c:if>
    
 <h1 align="center">TRAINING CENTER</h1>
<div class="form_input" style="display: none">
	
			<form class="needs-validation" id="myform" novalidate>
				<input type="hidden" name="id" id="id" class="id"/>
				<input type="hidden" name="user" id="user" class="user"/>
				<input type="hidden" name="uniqueKey" id="uniqueKey" class="uniqueKey"/>
				<input type="hidden" name="dateEncoded" id="dateEncoded" class="dateEncoded"/>
				<input type="hidden" name="dateEdited" id="dateEdited" class="dateEdited"/>
				<input type="hidden" name="encodedBy" id="encodedBy" class="encodedBy"/>
				<input type="hidden" name="editedBy" id="editedBy" class="editedBy"/>
				<input type="hidden"  name="region" id="region" value="${region }" class="region"/>	
				<br>
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISHERIES TRAINING CENTER LOCATION</legend>
				<div class="form-row">
    			<div class="col-md-4 mb-3">
					<label  for="province"><span>Province</span></label>					
						<select name="province"  id="province" class="province form-control" required>																																					
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Please select a valid province.
      					</div>
					</div>	
					<div class="col-md-4 mb-3">															
						<label  for="municipality"><span>Municipality</span></label>				
							<select name="municipality" id="municipality"  class="municipality form-control" required></select>															
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Please select a valid Municipality/City 
      					</div>
					</div>
					<div class="col-md-4 mb-3">	
						<label  for="barangay"><span>Barangay</span></label>	
							<select name="barangay"  id="barangay"  class="barangay form-control" required></select>															
								<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Please select a valid Barangay
      							</div>
					</div>
				</div>
			<div class="form-row">
    			<div class="col-md-12 mb-3">
					<label  for="code"><span>Code</span></label>			
					<input type="text" name="code" id="code"  class="code input-field " required placeholder="Code" readonly="readonly"/>
				</div>
			</div>
			</fieldset>
			
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISHERIES TRAINING CENTER INFORMATION</legend>
				<div class="form-row">
    			<div class="col-md-6 mb-3">
					<label  for="name"><span>Name of Training Center:</span></label> 
					<input type="text" name="name"  id="name"  class="name form-control" required/>
						<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Required
      							</div>
					</div>
					<div class="col-md-6 mb-3">
						<label  for="specialization"><span>Specialization:</span></label>	 
						<input type="text" name="specialization"  id="specialization"  class="specialization form-control" required/>
							<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        							Required
      						</div>
					</div>
					</div>
					<div class="form-row">
    				<div class="col-md-6 mb-3">
						<label  for="facilityWithinTheTrainingCenter"><span>Facility Within The Training Center</span></label>	
						<input type="text" name="facilityWithinTheTrainingCenter"  id="facilityWithinTheTrainingCenter"  class="facilityWithinTheTrainingCenter form-control" required/>																
							<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        							Required
      						</div>
      				</div>
      				<div class="col-md-6 mb-3">
					<label  for="type"><span>Type</span></label>		
					<select name="type"  id="type"  class="type form-control" required>
										<option value=""></option>
										<option value="National">National</option>
										<option value="Local">Local</option>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>
					</div>
			</fieldset>
			
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ADDITIONAL INFORMATION</legend>
				<div class="form-row">
    				<div class="col-md-6 mb-3">
					<label   for="dateAsOf"><span>Date as of:</span></label>
					<input type="date" name="dateAsOf" data-date-format="YYYY-MM-dd" id="dateAsOf" class="dateAsOf form-control" required>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>	
					<div class="col-md-6 mb-3">	
						<label   for="sourceOfData"><span>Data Sources:</span></label>	
						<input type="text" name="dataSource"  id="dataSource"  class="dataSource form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
					</div>
				</div>
				<div class="form-row">
    				<div class="col-md-12 mb-3">
					<label  for="remarks"><span>Remarks:</span></label>
						<textarea contenteditable="true" name="remarks" id="remarks"  class="remarks form-control" required></textarea>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      				</div>
      			</div>		
			</fieldset>
			
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">GEOGRAPHICAL INFORMATION</legend>
				
				<div class="form-row">
    				<div class="col-md-6 mb-3">
						<label  for="lat"><span>Latitude</span></label>
						<input type="text" name="lat" id="lat" readonly="readonly" class="lat form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
      				</div>
      				<div class="col-md-6 mb-3">
						<label  for="lon"><span>Longitude</span></label>	
						<input type="text" name="lon" id="lon" readonly="readonly" class="lon form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      				</div>	
				</div>
				<div class="form-row" id="image_form">
    				<div class="col-md-12 mb-3">
			 		<label  for="photos"><span>Captured Image</span></label>   			
					<input type="file" id="photos" class="form-control file" accept="image/*" />
     				<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      				</div>	
				</div>
     			</fieldset>
     			<div id="img_preview" style="display: none">
     						<img id="preview" name="preview"  class="preview" style="width: 450px; height: 200px"></img>
     			</div>
     						<input type="hidden" name="image_src"  id="image_src" class="image_src" value="" /><br />
							<input type="hidden" name="image"  id="image" class="image"/>
				<div class="btn-group" role="group" aria-label="Basic example">
				<button type="button" id="submitTrainingCenter" class="submitTrainingCenter btn btn-primary">SUBMIT</button>
        		<button type="button" id="cancel" class="cancel btn btn-secondary">CANCEL</button>
					  </div>
		</form> 
		<div id="pdf" style="display: none">
        	<button class="exportToPDF btn-info">Generate PDF</button>
		</div>
</div>

<br><br>
  <div id="table">				
<table id="pdfTRAINING" style="display: none;">
  <thead>
    <tr>
      <th scope="col">TRAINING CENTER</th>
      <th scope="col">&nbsp;</th>
    </tr>
  </thead>
  <tbody id="#tbodyID">
  </tbody>
</table>
</div>  

 <div class="container"> 
 <button class="create btn-primary">Create</button>
  <input class="btn-info exportToExcel" type="button" value="Export to XLS" onclick="exportToExcel('exTable','TrainingCenter')" />
     
 <div class="row">
       <div class="col-md-12 table-responsive">
			<table id="tableList" class="table table-hover">
                <thead>
                  <tr>
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Training Center</th>
                     <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody>  
               </tbody>
              </table>            
  			<div id="paging">
               
             </div>
     <!-- <table id="export-buttons-table" class="table" style="display: none"> --> 
       <table class="table2excel" id="exTable"  style="display: none">
                <thead>        
                <tr><th align="center" colspan="5">TRAINING CENTER</th></tr>
                
                <tr>
                
                    <th>${region}</th>
                   
                  </tr>
                  <tr>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Code</th>
                    <th>Name of Training Center</th>
                    <th>Specialization</th>
                    <th>Facility Within The Training Center</th>
                    <th>Type</th>
                    <th>Data Sources</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="emp_body">
                <!--  ${listAll}--> 
               </tbody>
              </table>       
    <!-- <button class="exportToExcel btn-info">Export to XLS</button> -->
     
      </div>
 </div>
 </div>
 </div>
 <jsp:include page="../menu/fma_details.jsp"></jsp:include>
 
 </div>
    <div id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</div> 
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>	
<script src="/static/js/form/provinceMunBarangay.js"></script>
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>

<script  src="/static/js/resource.js" ></script>
<script  src="/static/js/api/regionMap.js" ></script>
<script src="/static/js/map/philippineMap.js"></script>
<script  src="/static/js/resourcesOnMapByResources.js" ></script>
<script src="/static/js/validationForm/training.js" type=" text/javascript"></script>
<script>
	
	 var map_location = ${mapsData}; 

		function getFisheriesData(){
			getTrainingCenter(map_location);
		}

</script>

	</body>

</html>