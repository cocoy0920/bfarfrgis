<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="en">
<head>
 <c:set var = "page" scope = "session" value = "${page}"/>
      <c:if test = "${page == 'fishcage'}">
         <title>Fish Cage</title>
      </c:if>
	<c:if test = "${page == 'fishsanctuaries'}">
         <title>Fish Sanctuary</title>
      </c:if>
      	<c:if test = "${page == 'fishcorals'}">
         <title>Fish Corals</title>
      </c:if>
      <c:if test = "${page == 'fishlanding'}">
         <title>Fish Landing</title>
      </c:if>
      <c:if test = "${page == 'fishpen'}">
         <title>Fish Pen</title>
      </c:if>
      <c:if test = "${page == 'fishpond'}">
         <title>Fish Pond</title>
      </c:if>
      <c:if test = "${page == 'fishport'}">
         <title>Fish Port</title>
      </c:if>
      <c:if test = "${page == 'fishprocessingplants'}">
         <title>Fish Processing Plant</title>
      </c:if>
      <c:if test = "${page == 'hatchery'}">
         <title>Hatchery</title>
      </c:if>
      <c:if test = "${page == 'ipcs'}">
         <title>Cold Storage</title>
      </c:if>
      <c:if test = "${page == 'lambaklad'}">
         <title>Lambaklad</title>
      </c:if>
      <c:if test = "${page == 'lgu'}">
         <title>LGU</title>
      </c:if>
      <c:if test = "${page == 'mangrove'}">
         <title>Mangrove</title>
      </c:if>
      <c:if test = "${page == 'mariculture'}">
         <title>Mariculture</title>
      </c:if>
      <c:if test = "${page == 'market'}">
         <title>Market</title>
      </c:if>
      <c:if test = "${page == 'center'}">
         <title>National Center</title>
      </c:if>
      <c:if test = "${page == 'payao'}">
         <title>Payao</title>
      </c:if>
      <c:if test = "${page == 'regional'}">
         <title>Regional Office</title>
      </c:if>
      <c:if test = "${page == 'school'}">
         <title>School of Fisheries</title>
      </c:if>
      <c:if test = "${page == 'seagrass'}">
         <title>Seagrass</title>
      </c:if>
      <c:if test = "${page == 'seaweeds'}">
         <title>Seaweeds</title>
      </c:if>
      <c:if test = "${page == 'tos'}">
         <title>TOS</title>
      </c:if>
      <c:if test = "${page == 'trainingcenter'}">
         <title>Training Center</title>
      </c:if>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
	<script src="/static/js/form/deleteMapList.js" type=" text/javascript"></script>
	<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
	<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
	<script src="//d3js.org/d3.v3.min.js"></script>
	<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
    <script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
    <script src="/static/leaflet/bundle.js"></script>
    <link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
    <script src="/static/leaflet/L.Control.SlideMenu.js"></script>
    <script  src="/static/leaflet/leaflet-bootstrapmodal.js" type=" text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/exif-js"></script>
<style>
#map{ 
	width:100%;
  	height: 600px;
   background: #009dc4;
}
</style>
</head>
<body onload="getFisheriesData()">

<jsp:include page="../home/header_users.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">

  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
    <c:if test="${mapsData != '' }">
    <div id="mapview">
    <div id="map"></div>
    </div>
    </c:if>
    <c:if test="${mapsData == '' }">
    <div id="mapview"  style="display:none;">
    <div id="map"></div>
    </div>
    </c:if>
     <c:set var = "page" scope = "session" value = "${page}"/>
      <c:if test = "${page == 'fishcage'}">
         <h1 align="center">FISH CAGE</h1>
      </c:if>
      <c:if test = "${page == 'fishsanctuaries'}">
         		<h1 align="center">FISH SANCTUARY</h1>
      </c:if> 
      <c:if test = "${page == 'fishcorals'}">
         <h1 align="center">Fish Corals</h1>
      </c:if>
      <c:if test = "${page == 'fishlanding'}">
         <h1 align="center">Fish Landing</h1>
      </c:if>
      <c:if test = "${page == 'fishpen'}">
         <h1 align="center">Fish Pen</h1>
      </c:if>
      <c:if test = "${page == 'fishpond'}">
         <h1 align="center">Fish Pond</h1>
      </c:if>
      <c:if test = "${page == 'fishport'}">
         <h1 align="center">Fish Port</h1>
      </c:if>
      <c:if test = "${page == 'fishprocessingplants'}">
         <h1 align="center">Fish Processing Plant</h1>
      </c:if>
      <c:if test = "${page == 'hatchery'}">
         <h1 align="center">Hatchery</h1>
      </c:if>
      <c:if test = "${page == 'ipcs'}">
         <h1 align="center">Cold Storage</h1>
      </c:if>
      <c:if test = "${page == 'lambaklad'}">
         <h1 align="center">Lambaklad</h1>
      </c:if>
      <c:if test = "${page == 'lgu'}">
         <h1 align="center">LGU</h1>
      </c:if>
      <c:if test = "${page == 'mangrove'}">
         <h1 align="center">Mangrove</h1>
      </c:if>
      <c:if test = "${page == 'mariculture'}">
         <h1 align="center">Mariculture</h1>
      </c:if>
      <c:if test = "${page == 'market'}">
         <h1 align="center">Market</h1>
      </c:if>
      <c:if test = "${page == 'center'}">
         <h1 align="center">National Center</h1>
      </c:if>
      <c:if test = "${page == 'payao'}">
         <h1 align="center">Payao</h1>
      </c:if>
      <c:if test = "${page == 'regional'}">
         <h1 align="center">Regional Office</h1>
      </c:if>
      <c:if test = "${page == 'school'}">
         <h1 align="center">School of Fisheries</h1>
      </c:if>
      <c:if test = "${page == 'seagrass'}">
         <h1 align="center">Seagrass</h1>
      </c:if>
      <c:if test = "${page == 'seaweeds'}">
         <h1 align="center">Seaweeds</h1>
      </c:if>
      <c:if test = "${page == 'tos'}">
         <h1 align="center">TOS</h1>
      </c:if>
      <c:if test = "${page == 'trainingcenter'}">
         <h1 align="center">Training Center</h1>
      </c:if>   
	<div class="form_input" id="form_input" style="display: none">
		<form class="needs-validation" id="myform" novalidate>
				<input type="hidden" name="id" id="id" class="id">
				<input type="hidden" name="user" id="user" class="user"/>
				<input type="hidden" name="uniqueKey" id="uniqueKey" class="uniqueKey"/>
				<input type="hidden" name="dateEncoded" id="dateEncoded" class="dateEncoded"/>
				<input type="hidden" name="dateEdited" id="dateEdited" class="dateEdited" />
				<input type="hidden" name="encodedBy" id="encodedBy" class="encodedBy"/>
				<input type="hidden" name="editedBy" id="editedBy" class="editedBy" />
				<input type="hidden" name="region" id="region" class="region"/>	
				<input type="hidden" name="page" id="page" class="page"/>	
				
				<br>
		
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH CAGE LOCATION</legend>
				<div class="form-row">
    			<div class="col-md-4 mb-3">
					<label  for="validationCustom01">Province</label>
					<select name="province"  id="validationCustom01" class="form-control province" required></select>															
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid province.
      				</div>
					</div>


    			<div class="col-md-4 mb-3">	
				<label  for="validationCustom02">Municipality/City </label> 		
				  <select name="municipality" id="validationCustom02"  class="form-control municipality" required></select>	
				 <div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Municipality/City 
      				</div>
					</div>

    			<div class="col-md-4 mb-3">															
				<label  for="validationCustom03">Barangay</label>
					<select name="barangay"  id="validationCustom03"  class="form-control barangay" required>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Barangay
      				</div>
					</div>
			</div>
			
			<div class="form-row">
    			<div class="col-md-12 mb-3">			
					<label for="code">Code:</label>
						<input type="text" name="code" id="code"  class="code" placeholder="Code" readonly="readonly"/>
				</div>
			</div>															
			</fieldset>
			<c:set var = "page" scope = "session" value = "${page}"/>
      		<c:if test = "${page == 'fishcage'}">
         		<jsp:include page="../forms/fishcage.jsp"></jsp:include>
      		</c:if>
      		<c:if test = "${page == 'fishsanctuaries'}">
         		<jsp:include page="../forms/fishsanctuaries.jsp"></jsp:include>
      		</c:if>
      		<c:if test = "${page == 'fishcorals'}">
         		<script src="../forms/fishcorals.jsp"></script>
      		</c:if>
		      <c:if test = "${page == 'fishlanding'}">
		         <script src="../forms/fishlanding.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'fishpen'}">
		         <script src="../forms/fishpen.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'fishpond'}">
		         <script src="../forms/fishPond.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'fishport'}">
		         <script src="../forms/FISHPORT.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'fishprocessingplants'}">
		         <script src="../forms/fishprocessingplant.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'hatchery'}">
		         <script src="../forms/hatchery.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'ipcs'}">
		         <script src="../forms/iceplantorcoldstorage.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'lambaklad'}">
		         <script src="../forms/lambaklad.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'lgu'}">
		         <script src="../forms/lgu.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'mangrove'}">
		         <script src="../forms/mangrove.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'mariculture'}">
		         <script src="../forms/mariculturezone.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'market'}">
		         <script src="../forms/market.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'center'}">
		         <script src="../forms/nationalcenter.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'payao'}">
		         <script src="../forms/payao.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'regional'}">
		         <script src="../forms/regionaloffice.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'school'}">
		         <script src="../forms/schooloffisheries.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'seagrass'}">
		         <script src="../forms/seagrass.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'seaweeds'}">
		         <script src="../forms/seaweeds.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'tos'}">
		         <script src="../forms/tos.jsp"></script>
		      </c:if>
		      <c:if test = "${page == 'trainingcenter'}">
		         <script src="../forms/trainingcenter.jsp"></script>
		      </c:if> 
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ADDITIONAL INFORMATION</legend>
						<div class="form-row">

    			<div class="col-md-6 mb-3">	
					<label  for="dateAsOf">Data as of</label>
					<input type="date" name="dateAsOf" id="dateAsOf" class="form-control dateAsOf"  required>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>

    			<div class="col-md-6 mb-3">	
				<label  for="validationCustom09">Data Source/s</label>
					<input type="text" name="sourceOfData"  id="validationCustom09"  class="form-control sourceOfData" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>

			</div>
		<div class="form-row">
    		<div class="col-md-12 mb-3">					
				<label  for="validationCustom10">Remarks</label>
					<textarea name="remarks" id="validationCustom10"  class="form-control remarks" required></textarea>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

		</div>
		</fieldset>
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">GEOGRAPHICAL LOCATION</legend>
			<div class="form-row">
      				<div class="col-md-2 mb-3">
					<label  for="area"><span>Area</span></label>
					<input type="text" name="area"  id="area"  class="area form-control" required/>	
					<!-- <input type="number"  name="area" min="0" value="0" step="any"  id="area"  class="fishcoralarea form-control" required/> -->																
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-2 mb-3">	
					<label  for="unit"><span>Unit</span></label>
					<select name="unit"  id="unit"  class="unit
					 form-control" required>
													<option value=""></option>
													<option value="Minor">Minor</option>
													<option value="Major">Major</option>
						</select>
					<div class="valid-feedback">
		        				ok.
		      				</div>
		      				<div class="invalid-feedback">
		        				Required
		      				</div>
					</div>
    		<div class="col-md-2 mb-3">	
				<label  for="lat">Latitude</label>			
					<input type="text" name="lat"  readonly="readonly"   id="lat" class="form-control lat" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

    		<div class="col-md-2 mb-3">	
				<label  for="lon">Longitude</label>
				<input type="text" name="lon"  readonly="readonly"  id="lon" class="form-control lon" required/>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

		</div>	
			<div class="form-row" id="image_form">
    			<div class="col-md-12 mb-3">				
			  		<label  for="photos">Captured Image</label>		
					<i class="fas fa-upload"></i><input type="file" id="photos" class="form-control file" accept="image/*"/>
    				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
			</div>
     			</fieldset>
     				<div id="img_preview" style="display: none">
     						<img id="preview" name="preview" class="preview" style="width: 450px; height: 200px"></img>
     				</div>
     						<input type="hidden" name="image_src" id="image_src" class="image_src" value="" /><br />
							<input type="hidden" name="image"  id="image" class="image"/>			
		
						<div class="btn-group" role="group" aria-label="Basic example">
					  <c:set var = "page" scope = "session" value = "${page}"/>
					  	<c:if test = "${page == 'fishcage'}">			         	  	
					 			<button type="button" id="submitFishCage" class="submitFishCage btn btn-primary">SUBMIT</button>								
			      		</c:if>
					  <c:if test = "${page == 'fishsanctuaries'}">
			 				<button type="submit" id="submitFishSanctuaries" class="submitFishSanctuaries btn btn-primary">SUBMIT</button>							
			      	  </c:if>
					  <c:if test = "${page == 'fishcorals'}">
				         <button type="button" id="submitFishCoral" class="submitFishCoral btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'fishlanding'}">
				         <button type="button" id="submitFishLanding" class="submitFishLanding btn btn-primary" >SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'fishpen'}">
				         <button type="button" id="submitFishPen" class="submitFishPen btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'fishpond'}">
				         <button type="button" id="submitFishPond" class="submitFishPond btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'fishport'}">
				         <button type="button" id="submitFishport" class="submitFishport btn btn-primary" data-dismiss="modal">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'fishprocessingplants'}">
				          <button type="button" id="submitFishProcessingPlant" class="submitFishProcessingPlant btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'hatchery'}">
				         <button type="button" id="submitHatchery" class="submitHatchery btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'ipcs'}">
				         <button type="button" id="submitIcePlantColdStorage" class="submitIcePlantColdStorage btn btn-primary" data-dismiss="modal">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'lambaklad'}">
				         <button type="button" id="submitLambaklad" class="submitLambaklad btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'lgu'}">
				         <button type="button" id="submitLGU" class="submitLGU btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'mangrove'}">
				         <button type="button" id="submitMangrove" class="submitMangrove btn btn-primary" data-dismiss="modal">SUBMIT</button> 
				      </c:if>
				      <c:if test = "${page == 'mariculture'}">
				        <button type="button" id="submitmariculture" class="submitmariculture btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'market'}">
				         <button type="button" id="submitMarket" class="submitMarket btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'center'}">
				         <button type="button" id="submitNationalCenter" class="submitNationalCenter btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'payao'}">
				         <button type="button" id="submitPayao" class="submitPayao btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'regional'}">
				         <button type="button" id="submitRegionalOffice" class="submitRegionalOffice btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'school'}">
				         <button type="button" id="submitSchoolOfFisheries" class="submitSchoolOfFisheries btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'seagrass'}">
				         <button type="button" id="submitSeaGrass" class="submitSeaGrass btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'seaweeds'}">
				         <button type="button" id="submitSeaWeeds" class="submitSeaWeeds btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'tos'}">
				         <button type="button" id="submitTOS" class="submitTOS btn btn-primary">SUBMIT</button>
				      </c:if>
				      <c:if test = "${page == 'trainingcenter'}">
				         <button type="button" id="submitTrainingCenter" class="submitTrainingCenter btn btn-primary">SUBMIT</button>
				      </c:if>
			      		<button type="button" id="cancel" class="cancel btn btn-secondary">CANCEL</button>
					  	</div>
		</form>
		<div id="pdf" style="display: none">
        		<br><br>
        		<button class="exportToPDF btn btn-info">Generate PDF</button>
  		</div>
</div>

<!-- <a href="/create/fishcage/pdf">pdf</a> -->
		  
<br><br>
  <div id="table">				
<table id="pdfFISHCAGE" style="display: none;">
  <thead>
    <tr>
      <th scope="col">FISH CAGE </th>
      <th scope="col">&nbsp;</th>
    </tr>
  </thead>
  <tbody id="tbodyID">
  </tbody>
</table>

</div>  


 <div class="container"> 
 <button class="create btn-primary">Create</button>
  <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','FishCage')" />
 
 <div class="row">
       <div class="col-md-12 table-responsive">
	<table id="export-buttons-table" class="table table-hover">
                 <thead>
                  <tr>
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Operator</th>
                    <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody> 
               </tbody>
              </table>
              <div id="paging">
            
             </div>
 
          <table class="table2excel table table-hover" id="exTable" style="display: none">
                <thead>
<!--                 <tr><th align="center" colspan="17" style="height: 200px"> -->
                <!-- <img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png">
                 -->
                <!--  </th></tr> -->
                <tr><th align="center" colspan="5">FISH CAGE</th></tr>
                
                <tr>
               <%--  <c:set var="string" value="${region}"/>  
					<th>REGION</th> --%>
                    <th>${region}</th>
                   
                  </tr>
                  <tr>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Operator</th>
                    <th>Code</th>
                    <th>Area</th>
                    <th>Classification of Operator</th>
                    <th>Cage Dimension</th>
                  	<th>Cage Design</th>
                    <th>Cage Type</th>
                    <th>Cage no of Compartments</th>
                    <th>Indicate species</th>
                    <th>Source of data</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="emp_body">
               </tbody>
              </table>       
<!--     <button class="exportToExcel btn btn-info">Export to XLS</button> -->
       </div>
 <!--  <button id="print-button">
    Print
  </button> -->
 </div>
  </div>
 </div>
 <jsp:include page="../menu/fma_details.jsp"></jsp:include>
 
  </div>
  	<div id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>	
<script src="/static/js/form/provinceMunBarangay.js"></script>	
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
			<c:set var = "page" scope = "session" value = "${page}"/>
      		<c:if test = "${page == 'fishcage'}">
         		<script src="/static/js/validationForm/fishCage.js"></script>
      		</c:if>
      		<c:if test = "${page == 'fishsanctuaries'}">
         		<script src="/static/js/validationForm/fishSanctuary.js"></script>
      		</c:if>
			<c:if test = "${page == 'fishcorals'}">
         		<script src="/static/js/validationForm/fishCorral.js"></script>
      		</c:if>
		      <c:if test = "${page == 'fishlanding'}">
		         <script src="/static/js/validationForm/fishLanding.js"></script>
		      </c:if>
		      <c:if test = "${page == 'fishpen'}">
		         <script src="/static/js/validationForm/fishPen.js"></script>
		      </c:if>
		      <c:if test = "${page == 'fishpond'}">
		         <script src="/static/js/validationForm/fishPond.js"></script>
		      </c:if>
		      <c:if test = "${page == 'fishport'}">
		         <script src="/static/js/validationForm/fishPort.js"></script>
		      </c:if>
		      <c:if test = "${page == 'fishprocessingplants'}">
		         <script src="/static/js/validationForm/fishprocessingplant.js"></script>
		      </c:if>
		      <c:if test = "${page == 'hatchery'}">
		         <script src="/static/js/validationForm/hatchery.js"></script>
		      </c:if>
		      <c:if test = "${page == 'ipcs'}">
		         <script src="/static/js/validationForm/icePlant.js"></script>
		      </c:if>
		      <c:if test = "${page == 'lambaklad'}">
		         <script src="/static/js/validationForm/lambaklad.js"></script>
		      </c:if>
		      <c:if test = "${page == 'lgu'}">
		         <script src="/static/js/validationForm/pfo.js"></script>
		      </c:if>
		      <c:if test = "${page == 'mangrove'}">
		         <script src="/static/js/validationForm/mangrove.js"></script>
		      </c:if>
		      <c:if test = "${page == 'mariculture'}">
		         <script src="/static/js/validationForm/mariculture.js"></script>
		      </c:if>
		      <c:if test = "${page == 'market'}">
		         <script src="/static/js/validationForm/market.js"></script>
		      </c:if>
		      <c:if test = "${page == 'center'}">
		         <script src="/static/js/validationForm/nationalcenter.js"></script>
		      </c:if>
		      <c:if test = "${page == 'payao'}">
		         <script src="/static/js/validationForm/payao.js"></script>
		      </c:if>
		      <c:if test = "${page == 'regional'}">
		         <script src="/static/js/validationForm/regionaloffice.js"></script>
		      </c:if>
		      <c:if test = "${page == 'school'}">
		         <script src="/static/js/validationForm/school.js"></script>
		      </c:if>
		      <c:if test = "${page == 'seagrass'}">
		         <script src="/static/js/validationForm/seagrass.js"></script>
		      </c:if>
		      <c:if test = "${page == 'seaweeds'}">
		         <script src="/static/js/validationForm/seaweeds.js"></script>
		      </c:if>
		      <c:if test = "${page == 'tos'}">
		         <script src="/static/js/validationForm/tos.js"></script>
		      </c:if>
		      <c:if test = "${page == 'trainingcenter'}">
		         <script src="/static/js/validationForm/training.js"></script>
		      </c:if>   
<script  src="/static/js/resource.js" ></script>
<script  src="/static/js/api/regionMap.js" ></script>
<script src="/static/js/map/philippineMap.js"></script>
<script  src="/static/js/resourcesOnMapByResources.js" ></script>
<script>
		var map_location = ${mapsData}; 
		function getFisheriesData(){
		<c:set var = "page" scope = "session" value = "${page}"/>
      		<c:if test = "${page == 'fishcage'}">getFishCage(map_location);</c:if>
      		 <c:if test = "${page == 'fishsanctuaries'}">getFishSanctuaries(map_location);</c:if>
		      <c:if test = "${page == 'fishcorals'}">getFishCorals(map_location);</c:if>
		      <c:if test = "${page == 'fishlanding'}">getFishLanding(map_location);</c:if>
		      <c:if test = "${page == 'fishpen'}">getFishPen(map_location); </c:if>
		      <c:if test = "${page == 'fishpond'}">getFishPond(map_location);</c:if>
		      <c:if test = "${page == 'fishport'}">getFishPort(map_location);</c:if>
		      <c:if test = "${page == 'fishprocessingplants'}">getFishProcessingPlants(map_location);</c:if>
		      <c:if test = "${page == 'hatchery'}">getHatchery(map_location);</c:if>
		      <c:if test = "${page == 'ipcs'}">getIPCS(map_location);</c:if>
		      <c:if test = "${page == 'lambaklad'}">getLambaklad(map_location);</c:if>
		      <c:if test = "${page == 'lgu'}">getLGU(map_location);</c:if>
		      <c:if test = "${page == 'mangrove'}">getMangrove(map_location);</c:if>
		      <c:if test = "${page == 'mariculture'}">getMaricultureZone(map_location);</c:if>
		      <c:if test = "${page == 'market'}">getMarket(map_location);</c:if>
		      <c:if test = "${page == 'center'}">getNationalCenter(map_location);</c:if>
		      <c:if test = "${page == 'payao'}">getPayao(map_location); </c:if>
		      <c:if test = "${page == 'regional'}">getRegionalOffice(map_location);</c:if>
		      <c:if test = "${page == 'school'}">getSchoolOfFisheries(map_location);</c:if>
		      <c:if test = "${page == 'seagrass'}">seaGrass(map_location);</c:if>
		      <c:if test = "${page == 'seaweeds'}">getSeaweeds(map_location);</c:if>
		      <c:if test = "${page == 'tos'}">getTOS(map_location);</c:if>
		      <c:if test = "${page == 'trainingcenter'}">getTrainingCenter(map_location);</c:if>
		}
		</script>      
	</body>

</html>