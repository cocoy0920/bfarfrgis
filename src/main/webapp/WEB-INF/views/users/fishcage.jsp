<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="en">
<head>
<title>Fish Cage</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
	<script src="/static/js/form/deleteMapList.js" type=" text/javascript"></script>
	<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
	<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
	<script src="//d3js.org/d3.v3.min.js"></script>
	<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
    <script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
    <script src="/static/leaflet/bundle.js"></script>
    <link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
    <script src="/static/leaflet/L.Control.SlideMenu.js"></script>
    <script  src="/static/leaflet/leaflet-bootstrapmodal.js" type=" text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/exif-js"></script>
<style>
#map{ 
	width:100%;
  	height: 600px;
   background: #009dc4;
}
</style>
</head>
<body onload="getFisheriesData()">

<jsp:include page="../home/header_users.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">

  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
    <c:if test="${mapsData != '' }">
    <div id="mapview">
    <div id="map"></div>
    </div>
    </c:if>
    <c:if test="${mapsData == '' }">
    <div id="mapview"  style="display:none;">
    <div id="map"></div>
    </div>
    </c:if>
          <h1 align="center">FISH CAGE</h1>
	<div class="form_input" id="form_input" style="display: none">
		<form class="needs-validation" id="myform" novalidate>

		</form>
		<div id="pdf" style="display: none">
        		<br><br>
        		<button class="exportToPDF btn btn-info">Generate PDF</button>
  		</div>
</div>

<!-- <a href="/create/fishcage/pdf">pdf</a> -->
		  
<br><br>
  <div id="table">				
<table id="pdfFISHCAGE" style="display: none;">
  <thead>
    <tr>
      <th scope="col">FISH CAGE </th>
      <th scope="col">&nbsp;</th>
    </tr>
  </thead>
  <tbody id="tbodyID">
  </tbody>
</table>

</div>  


 <div class="container"> 
 <button class="create btn-primary">Create</button>
  <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','FishCage')" />
 
 <div class="row">
       <div class="col-md-12 table-responsive">
	<table id="export-buttons-table" class="table table-hover">
                 <thead>
                  <tr>
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Operator</th>
                    <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody> 
               </tbody>
              </table>
              <div id="paging">
            
             </div>
 
          <table class="table2excel table table-hover" id="exTable" style="display: none">
                <thead>
<!--                 <tr><th align="center" colspan="17" style="height: 200px"> -->
                <!-- <img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png">
                 -->
                <!--  </th></tr> -->
                <tr><th align="center" colspan="5">FISH CAGE</th></tr>
                
                <tr>
               <%--  <c:set var="string" value="${region}"/>  
					<th>REGION</th> --%>
                    <th>${region}</th>
                   
                  </tr>
                  <tr>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Operator</th>
                    <th>Code</th>
                    <th>Area</th>
                    <th>Classification of Operator</th>
                    <th>Cage Dimension</th>
                  	<th>Cage Design</th>
                    <th>Cage Type</th>
                    <th>Cage no of Compartments</th>
                    <th>Indicate species</th>
                    <th>Source of data</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="emp_body">
               </tbody>
              </table>       
<!--     <button class="exportToExcel btn btn-info">Export to XLS</button> -->
       </div>
 <!--  <button id="print-button">
    Print
  </button> -->
 </div>
  </div>
 </div>
 <jsp:include page="../menu/fma_details.jsp"></jsp:include>
 
  </div>
  	<div id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>	
<script src="/static/js/form/provinceMunBarangay.js"></script>	
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script src="/static/js/validationForm/fishCage.js"></script>
<script src="/static/js/form/dynamicForms.js"></script>	
<script  src="/static/js/resource.js" ></script>
<script  src="/static/js/api/regionMap.js" ></script>
<script src="/static/js/map/philippineMap.js"></script>
<script  src="/static/js/resourcesOnMapByResources.js" ></script>
	<script>
	getFormsByResources("fishcage");
	 var map_location = '${mapsData}'; 
	 
		function getFisheriesData(){
			
			getFishCage(map_location);
		}
</script>
	</body>

</html>