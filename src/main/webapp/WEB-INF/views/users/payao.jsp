<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="en">
<head>
<title>Payao</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
<script src="/static/js/form/deleteMapList.js" type=" text/javascript"></script>
<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
<script src="/static/leaflet/leaflet.js"></script>
<script src="/static/leaflet/leaflet.ajax.min.js"></script>
<script src="//d3js.org/d3.v3.min.js"></script>
<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
<script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
<script src="/static/leaflet/bundle.js"></script>
<link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
<script src="/static/leaflet/L.Control.SlideMenu.js"></script>
<script src="/static/leaflet/leaflet-bootstrapmodal.js"></script>
<script src="https://cdn.jsdelivr.net/npm/exif-js"></script>
<style type="text/css">
.table2excel{
	padding: 5px;
}
#map{ 
	width:100%;
  	height: 600px;
   background: #009dc4;
}
</style>
</head>
<body onload="getFisheriesData()">

  <jsp:include page="../home/header_users.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">

    <div id="content" class="p-4 p-md-5 pt-5 section-to-print" >
    <c:if test="${mapsData != '' }">
    <div id="mapview">
    <div id="map"></div>
    </div>
    </c:if>
    <c:if test="${mapsData == '' }">
    <div id="mapview"  style="display:none;">
    <div id="map"></div>
    </div>
    </c:if>
          <h1 align="center">PAYAO</h1>
	<div class="form_input" style="display: none">
		<form class="needs-validation" id="myform" novalidate>
				<input type="hidden" name="id" id="id" class="id">
				<input type="hidden" name="user" id="user" class="user"/>
				<input type="hidden" name="unique_key" id="unique_key" class="unique_key"/>
				<input type="hidden" name="dateEncoded" id="dateEncoded" class="dateEncoded"/>
				<input type="hidden" name="dateEdited" id="dateEdited" class="dateEdited" />
				<input type="hidden" name="encodedBy" id="encodedBy" class="encodedBy"/>
				<input type="hidden" name="editedBy" id="editedBy" class="editedBy" />
				<input type="hidden" name="region" id="region" class="region"/>	
				<input type="hidden" name="page" id="page" class="page"/>	
				
				<br>
		
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">PAYAO LOCATION</legend>
				<div class="row">
    			<div class="col-md-4 mb-3">
					<label  for="province">Province</label>
					<select name="province"  id="province" class="form-control province" required></select>															
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid province.
      				</div>
					</div>


    			<div class="col-md-4 mb-3">	
				<label  for="municipality">Municipality/City </label> 		
				  <select name="municipality" id="municipality"  class="form-control municipality" required></select>	
				 <div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Municipality/City 
      				</div>
					</div>

    			<div class="col-md-4 mb-3">															
				<label  for="barangay">Barangay</label>
					<select name="barangay"  id="barangay"  class="form-control barangay" required>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Barangay
      				</div>
					</div>
			</div>
			
			<div class="row">
    			<div class="col-md-12 mb-3">			
					<label for="code">Code:</label>
						<input type="text" name="code" id="code"  class="code" placeholder="Code" readonly="readonly"/>
				</div>
			</div>															
			</fieldset>
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">PAYAO INFORMATION</legend>
			<div class="row">
    			<div class="col-md-4 mb-3">
			 <label  for="association"><span>Association:</span></label>
			 <input type="text" name="association"  id="association"  class="association form-control" required/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-4 mb-3">
				<label  for="beneficiary"><span>Beneficiary</span></label>				
				<input type="text" name="beneficiary"  id="beneficiary"  class="beneficiary form-control" required>
				
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-4 mb-3">
		<label  for="payao"><span>Payao:</span></label>
					<input type="number" name="payao"  id="payao"  class="payao form-control" required/>
		<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>	
		</div>
		</div>	

		</fieldset>
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ADDITIONAL INFORMATION</legend>
						<div class="row">

    			<div class="col-md-6 mb-3">	
					<label  for="dateAsOf">Data as of</label>
					<input type="date" name="dateAsOf" data-date-format="YYYY-MM-dd" id="dateAsOf" class="form-control dateAsOf">
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
		<!-- 	<div class="col-md-6 mb-3">	
					<input type="text" name="startDate" id="startDate" class="date-picker"  data-date="" value="" />
			</div>-->

    			<div class="col-md-6 mb-3">	
				<label  for="sourceOfData">Data Source/s</label>
					<input type="text" name="sourceOfData"  id="sourceOfData"  class="form-control sourceOfData"/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>

			</div>
		<div class="row">
    		<div class="col-md-12">				
				<label  for="remarks">Remarks</label>
					<textarea contenteditable="true" name="remarks"  id="remarks"  class=" remarks form-control"></textarea>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div>

		</div> 
		</fieldset>
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">GEOGRAPHICAL LOCATION</legend>
			<div class="row">

    		<div class="col-md-6 mb-3">	
				<label  for="lat">Latitude:</label>			
					<input type="text" name="lat"  readonly="readonly"   id="lat" class="form-control lat" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

    		<div class="col-md-6 mb-3">	
				<label  for="lon">Longitude:</label>
				<input type="text" name="lon"  id="lon"  readonly="readonly" class="form-control lon" required/>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

		</div>	
			<div class="row" id="image_form">
    			<div class="col-md-12 mb-3">				
			  		<label  for="file">Captured Image</label>		
					<i class="fas fa-upload"></i><input type="file" id="file" class="form-control file" accept="image/*"/>
    				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
			</div>
     			</fieldset>
     				<div id="img_preview" style="display: none">
     						<img id="preview" name="preview" class="preview" style="width: 450px; height: 200px"></img>
     				</div>
     						<input type="hidden" name="image_src" id="image_src" class="image_src" value="" /><br />
							<input type="hidden" name="image"  id="image" class="image"/>			
		
		 			<div class="btn-group" role="group" aria-label="Basic example">
					 <button type="button" id="submitPayao" class="submitPayao btn btn-primary">SUBMIT</button>
 					<button type="button" id="cancel" class="cancel btn btn-secondary">CANCEL</button>
					  </div>
		</form>
		<div id="pdf" style="display: none">
        		<br><br>
        		<button class="exportToPDF btn btn-info">Generate PDF</button>
  		</div>
</div>

<!-- <a href="/create/fishcage/pdf">pdf</a> -->

<br><br>
  <div id="table">				
<table id="pdfFISHCAGE" style="display: none;">
  <thead>
    <tr>
      <th scope="col">PAYAO</th>
      <th scope="col">&nbsp;</th>
    </tr>
  </thead>
  <tbody id="tbodyID">
  </tbody>
</table>

</div>  


 <div class="container"> 
 <button class="create btn-primary">Create</button>
  <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','Payao')" />
 <div class="row">
       <div class="col-md-12 table-responsive">
	<table id="export-buttons-table" class="table table-hover">
                 <thead>
                  <tr>
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Association</th>
                    <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody> 
               </tbody>
              </table>
              <div id="paging">
            
             </div>
 
          <table class="table2excel table table-hover" id="exTable" style="display: none">
                <thead>
                <tr><th align="center" colspan="5">PAYAO</th></tr>
                
                <tr>
               <%--  <c:set var="string" value="${region}"/>  
					<th>REGION</th> --%>
                    <th>${region}</th>
                   
                  </tr>
                  <tr>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Association</th>
                    <th>Beneficiary</th>
                    <th>Payao</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="emp_body">
               </tbody>
              </table>       
<!--     <button class="exportToExcel btn btn-info">Export to XLS</button> -->
   
      </div>
 <!--  <button id="print-button">
    Print
  </button> -->
 </div>
  </div>
 </div>
 <jsp:include page="../menu/fma_details.jsp"></jsp:include>
 
  </div>
     <div id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</div> 
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>		
<script src="/static/js/form/provinceMunBarangay.js"></script>	
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script  src="/static/js/resource.js" ></script>
<script  src="/static/js/api/regionMap.js" ></script>
<script src="/static/js/map/philippineMap.js"></script>
<script  src="/static/js/resourcesOnMapByResources.js" ></script>
<script src="/static/js/validationForm/payao.js" type=" text/javascript"></script>
<script  src="/static/js/jpegmeta.js" ></script>
	<script>
	
	 var map_location = ${mapsData}; 

		function getFisheriesData(){
			getPayao(map_location);
		}

</script>
	</body>

</html>