<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="en">
<head>
<title>Fish Processing</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
<script src="/static/leaflet/leaflet.js"></script>
<script src="/static/leaflet/leaflet.ajax.min.js"></script>
<script src="//d3js.org/d3.v3.min.js"></script>
<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
<script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
<script src="/static/leaflet/bundle.js"></script>
<link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
<script src="/static/leaflet/L.Control.SlideMenu.js"></script>
<script src="/static/leaflet/leaflet-bootstrapmodal.js"></script>
<script src="https://cdn.jsdelivr.net/npm/exif-js"></script>
<style>
#map{ 
	width:100%;
  	height: 600px;
   background: #009dc4;
}
</style>
</head>
<body onload="getFisheriesData()">

  <jsp:include page="../home/header_users.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">
  <!-- Sidebar Holder -->
<%--  <jsp:include page="../menu/sidebar2.jsp"></jsp:include> --%>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
     <c:if test="${mapsData != '' }">
    <div id="mapview">
    <div id="map"></div>
    </div>
    </c:if>
    <c:if test="${mapsData == '' }">
    <div id="mapview"  style="display:none;">
    <div id="map"></div>
    </div>
    </c:if>
    
    
    <h1 align="center">FISH PROCESSING</h1>
	<div class="form_input" style="display: none">
		<form class="needs-validation" id="myform" novalidate>	
				<input type="hidden" name="id" id="id" class="id"/>
				<input type="hidden" name="user" id="user" class="user"/>
				<input type="hidden" name="uniqueKey" id="uniqueKey" class="uniqueKey"/>
				<input type="hidden" name="dateEncoded" id="dateEncoded" class="dateEncoded"/>
				<input type="hidden" name="dateEdited" id="dateEdited" class="dateEdited"/>
				<input type="hidden" name="encodedBy" id="encodedBy" class="encodedBy"/>
				<input type="hidden" name="editedBy" id="editedBy" class="editedBy"/>
				<input type="hidden"  name="region" id="region" value="${region }" class="region"/>	
				<input type="hidden"  name="page" id="page" value="fishprocessingplants" class="page"/>	
				<br>
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH PROCESSING PLANT LOCATION</legend>
			<div class="form-row">
    			<div class="col-md-4 mb-3">
					<label   for="Province">Province</label>
					<select name="province"  id="Province" class="form-control province" required></select>															
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid province.
      				</div>
					</div>


    			<div class="col-md-4 mb-3">	
				<label  for="municipality">Municipality/City </label> 		
				  <select name="municipality" id="municipality"  class="form-control municipality" required></select>	
				 <div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Municipality/City 
      				</div>
					</div>

    			<div class="col-md-4 mb-3">															
				<label  for="barangay">Barangay</label>
					<select name="barangay"  id="barangay"  class="form-control barangay" required>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Barangay
      				</div>
					</div>
			</div>
			
			<div class="form-row">
    			<div class="col-md-12 mb-3">			
					<label for="code">Code:</label>
						<input type="text" name="code" id="code"  class="code" placeholder="Code" readonly="readonly"/>
				</div>
			</div>	
			</fieldset>	
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH PROCESSING PLANT INFORMATION</legend>
			<div class="form-row">
			<div class="col-md-4 mb-3">
			<label  for="validationCustom04">Name of Processing Plant:</label>
					<input type="text" name="nameOfProcessingPlants"  id="validationCustom04"  class="nameOfProcessingPlants form-control" required/>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-4 mb-3">
				<label  for="validationCustom05">Name of Operator:</label>
					<input type="text" name="nameOfOperator"  id="validationCustom05"  class="nameOfOperator form-control " required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				
			</div>
			<div class="col-md-4 mb-3">	
				<label  for="validationCustom06">Area</label>
				<input type="text" name="area"  id="area"  class="area form-control" required/>	
				<!-- 	<input type="number" name="area" min="0" value="0" step="any"  id="validationCustom06"  class="area form-control " required/> -->																
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>
			</div>
			<div class="form-row">
			<div class="col-md-4 mb-3">
			<label  for="validationCustom07">Operator Classification</label>		
				<select name="operatorClassification"  id="validationCustom07"  class="operatorClassification form-control " required>
					<option value="">-- PLS SELECT --</option>
					<option value="BFAR_MANAGED">BFAR MANAGED</option>
					<option value="LGU_MANAGED">LGU MANAGED</option>
					<!-- <option value="PFDA">PFDA</option> -->
					<option value="PRIVATE_SECTOR">PRIVATE SECTOR</option>
					<option value="COOPERATIVE">COOPERATIVE</option>
					<!-- <option value="Business">Corporation</option>
					<option value="Government">Government</option>
					 -->
				</select>
				<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>	
			<div class="col-md-4 mb-3">
			<label  for="validationCustom08">Processing Technique</label>
					<input type="text" name="processingTechnique"  id="validationCustom08"  class="processingTechnique form-control " required/>																
				<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
			</div>
			<div class="col-md-4 mb-3">
			<label  for="processingEnvironmentClassification">Processing Environment Classification</label>
				<select name="processingEnvironmentClassification"  id="processingEnvironmentClassification"  class="processingEnvironmentClassification form-control " required>
										<option value=""></option>
										<option value="Backyard">Backyard</option>
										<option value="Plant">Plant</option>
				</select>
				<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>
		</div>
		
		<div class="form-row">
			<div class="col-md-4 mb-3">
			<div id="plantData" >
			<label  for="plantRegistered">BFAR Plant Registered</label>
				<select name="plantRegistered"  id="plantRegistered"  class="plantRegistered form-control ">
										<option value=""></option>
										<option value="YES">YES</option>
										<option value="NO">NO</option>
				</select>
			</div>	
			
		<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>
			<div class="col-md-4 mb-3">
			<label  for="bfarRegistered">BFAR Registered</label>		
				<select name="bfarRegistered"  id="bfarRegistered"  class="bfarRegistered form-control " required>
										<option value=""></option>
										<option value="YES">YES</option>
										<option value="NO">NO</option>
				</select>
				
			<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>	
			<div class="col-md-4 mb-3">	
			<label  for="packagingType">Packaging Type</label>	
				<select name="packagingType"  id="packagingType"  class="packagingType form-control " required>
					<option value=""></option>
					<option value="Vacuum Packed">Vacuum Packed</option>
					<option value="Sealed">Sealed</option>
					<option value="Canned">Canned</option>
					<option value="Bottling">Bottling</option>
					<option value="Others">Others</option>
				</select>			
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>
			</div>
			<div class="form-row">
			<div class="col-md-4 mb-3">
			<label  for="marketReach">Market Reach</label>		
				<select name="marketReach"  id="marketReach"  class="marketReach form-control " required>
											<option value=""></option>
											<option value="Local">Local</option>
											<option value="National">National</option>
											<option value="Export">Export</option>
											<option value="International">International</option>
											<option value="European Union">European Union</option>
				</select>
				<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>	
			<div class="col-md-4 mb-3">
			<label  for="indicateSpecies">Indicate Species:</label>
				<input type="text" name="indicateSpecies" id="indicateSpecies"  class="indicateSpecies form-control " required/>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>
			<div class="col-md-4 mb-3">
			<label  for="businessPermitsAndCertificateObtained">Business permits and Certificates Obtained</label>
				<input type="text" name="businessPermitsAndCertificateObtained" id="businessPermitsAndCertificateObtained"  class="businessPermitsAndCertificateObtained form-control " required/>
				<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
			</div>
			</div>	
					
			</fieldset>
			
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ADDITIONAL INFORMATION</legend>
				<div class="form-row">

    			<div class="col-md-6 mb-3">	
					<label  for="dateAsOf">Data as of</label>
					<input type="date" name="dateAsOf" id="dateAsOf" class="form-control dateAsOf"  required>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>		
			<div class="col-md-6 mb-3">	
				<label  for="sourceOfData">Data Source/s</label>	
					<input type="text" name="sourceOfData"  id="sourceOfData"  class="sourceOfData form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
				</div>
			<div class="form-row">
    		<div class="col-md-12 mb-3">					
				<label  for="remarks">Remarks</label>
					<textarea name="remarks" id="remarks"  class="form-control remarks" required></textarea>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

		</div>
			</fieldset>
			
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">GEOGRAPHICAL LOCATION</legend>
		<div class="form-row">

    		<div class="col-md-6 mb-3">	
				<label  for="lat">Latitude</label>			
					<input type="text" name="lat" readonly="readonly"  id="lat" class="form-control lat" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

    		<div class="col-md-6 mb-3">	
				<label  for="lon">Longitude</label>
				<input type="text" name="lon" readonly="readonly"  id="lon" class="form-control lon" required/>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

		</div>	
			<div class="form-row" id="image_form">
    			<div class="col-md-12 mb-3">				
			  		<label  for="photos">Captured Image</label>		
					<input type="file" id="photos" class="form-control file" accept="image/*"/>
    				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
			</div>
</fieldset>
     			<div id="img_preview" style="display: none">
     						<img id="preview" name="preview"  class="preview" style="width: 450px; height: 200px"></img>
     			</div>
     						<input type="hidden" name="image_src" id="image_src" class="image_src" value="" /><br />
							<input type="hidden" name="image"  id="image" class="image"/>
				       
				       <div class="btn-group" role="group" aria-label="Basic example">
				        <button type="submit" id="submitFishProcessingPlant" class="submitFishProcessingPlant btn btn-primary">SUBMIT</button>
				        <button type="button" id="cancel" class="cancel btn btn-secondary">CANCEL</button>
					  </div>
		</form>
				  <div id="pdf" style="display: none">
        		<button class="exportToPDF btn btn-info">Generate PDF</button>    	
    	</div>
		</div>

<br><br>
  <div id="table">				
<table id="pdfFISHPROCESSINGPLANT" style="display: none;">
  <thead>
    <tr>
      <th scope="col">FISH PROCESSING PLANT </th>
      <th scope="col">&nbsp;</th>
    </tr>
  </thead>
  <tbody id="tbodyID">
  </tbody>
</table> 

</div>  

 <div class="container"> 
 <button class="create btn-primary">Create</button>
 	<input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','FishProcessing')" />
 
 <div class="row">
       <div class="col-md-12 table-responsive">
<table id="export-buttons-table" class="table table-hover">
                <thead>
                  <tr>
                    <th>Region</th><th>Province</th><th>Municipality</th><th>Barangay</th><th>Name of Processing Plants</th><th>Coordinates</th>
                  </tr>
                </thead>
                <tbody>  
               </tbody>
              </table>
              <div id="paging">
             </div>
 
     <table class="table2excel" id="exTable" style="display: none">
                <thead>
                <tr><th align="center" colspan="5">FISH PROCESSING PLANT</th></tr>
                
                
                <tr>
               <th>${region}</th>
                   
                  </tr>
                  <tr>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Processing Plants</th>
                    <th>Code</th>
					 <th>Name Of Operator</th>
					 <th>Area</th>
					 <th>Operator Classification </th>
					 <th>Processing Technique</th>
					 <th>Processing Environment Classification</th>
					 <th>Plant Registered</th>
					 <th>BFAR Registered</th>
					 <th>Packaging Type </th>
					 <th>Market Reach</th>
					 <th>Business Permits and Certificate obtained</th>
					 <th>Indicate Species</th>
					 <th>Source of Data</th>
					 <th>Latitude</th>
					 <th>Longitude</th>
					 <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="emp_body">
               </tbody>
              </table>       
    <!-- <button class="exportToExcel btn btn-info">Export to XLS</button>
 -->     
      
  </div>
<!--   <button id="print-button">
    Print
  </button> -->
 </div>
  </div>
  <!-- end of div content -->
  </div>



  <!-- end wrapper div -->
  <jsp:include page="../menu/fma_details.jsp"></jsp:include>
  
    </div>

      	<div id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</div>  
 <script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>		
<script src="/static/js/form/provinceMunBarangay.js"></script>
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script src="/static/js/validationForm/fishprocessingplant.js"></script>
<script  src="/static/js/resource.js" ></script>
<script  src="/static/js/api/regionMap.js" ></script>
<script src="/static/js/map/philippineMap.js"></script>
<script  src="/static/js/resourcesOnMapByResources.js" ></script>
	<script>
	
	 var map_location = ${mapsData}; 

		function getFisheriesData(){
			getFishProcessingPlants(map_location);
		}

</script>
    </body>
</html>