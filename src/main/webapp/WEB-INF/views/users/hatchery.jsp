<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="en">
<head>
<title>Hatchery</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
<script src="/static/leaflet/leaflet.js"></script>
<script src="/static/leaflet/leaflet.ajax.min.js"></script>
<script src="//d3js.org/d3.v3.min.js"></script>
<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
<script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
<script src="/static/leaflet/bundle.js"></script>
<link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
<script src="/static/leaflet/L.Control.SlideMenu.js"></script>
<script src="/static/leaflet/leaflet-bootstrapmodal.js"></script>
<script src="https://cdn.jsdelivr.net/npm/exif-js"></script>
<style type="text/css">
.table2excel{
	padding: 5px;
}
.table2excel tr,td
{
    border:1px solid black;
}
#map{ 
	width:100%;
  	height: 600px;
   background: #009dc4;
}
</style>
</head>
<body onload="getFisheriesData()">

  <jsp:include page="../home/header_users.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">
  <!-- Sidebar Holder -->
<%--  <jsp:include page="../menu/sidebar2.jsp"></jsp:include> --%>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
   <c:if test="${mapsData != '' }">
    <div id="mapview">
    <div id="map"></div>
    </div>
    </c:if>
    <c:if test="${mapsData == '' }">
    <div id="mapview"  style="display:none;">
    <div id="map"></div>
    </div>
    </c:if>
    <h1 align="center">HATCHERY</h1>
<div class="form_input" style="display: none">
		<form class="needs-validation" id="myform" novalidate>
			<input type="hidden" name="id" id="id" class="id" />
			<input type="hidden" name="user" id="user" class="user" />
			<input type="hidden" name="uniqueKey" id="uniqueKey" class="uniqueKey" />		
			<input type="hidden" name="encodedBy" id="encodedBy" class="encodedBy" />
			<input type="hidden" name="editedBy" id="editedBy" class="editedBy" />
			<input type="hidden" name="dateEncoded" id="dateEncoded" class="dateEncoded" />
			<input type="hidden" name="dateEdited" id="dateEdited" class="dateEdited" />
			<input type="hidden"  name="region" id="region" value="${region }" class="region"/>		
			<input type="hidden"  name="page" id="page" value="${page }" class="page"/>			
				
			<br>
			
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">HATCHERY LOCATION</legend>
				<div class="form-row">
    			<div class="col-md-4 mb-3">
					<label  for="province"><span>Province</span></label>					
						<select name="province"  id="province" class="province form-control">																																					
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Please select a valid province.
      					</div>
					</div>	
					<div class="col-md-4 mb-3">															
						<label  for="municipality"><span>Municipality</span></label>				
							<select name="municipality" id="municipality"  class="municipality form-control"></select>															
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Please select a valid Municipality/City 
      					</div>
					</div>
					<div class="col-md-4 mb-3">	
						<label  for="barangay"><span>Barangay</span></label>	
							<select name="barangay"  id="barangay"  class="barangay form-control"></select>															
								<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Please select a valid Barangay
      							</div>
					</div>
				</div>
			<div class="form-row">
    			<div class="col-md-12 mb-3">
					<label  for="code"><span>Code</span></label>			
					<input type="text" name="code" id="code"  class="code input-field " required placeholder="Code" readonly="readonly"/>
				</div>
			</div>															
			</fieldset>
			
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">HATCHERY INFORMATION</legend>
			 <div class="form-row">
    				<div class="col-md-4 mb-3">
			 			<label  for="nameOfHatchery"><span>Name of Hatchery:</span></label>
			 			<input type="text" name="nameOfHatchery"  id="nameOfHatchery"  class="nameOfHatchery form-control"/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-4 mb-3">
						<label  for="nameOfOperator"><span>Name of Operator:</span></label>
			 			<input type="text" name="nameOfOperator"  id="nameOfOperator"  class="nameOfOperator form-control"/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-4 mb-3">	
						<label  for="area"><span>Area</span></label>
						<input type="text" name="area"  id="area"  class="area form-control" required/>	
						<!-- <input type="number" name="area"  id="area"  min="0" value="0" step="any"   class="hatcheryarea form-control"/>	 -->															
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
			</div>
			<div class="form-row">
    				<div class="col-md-12 mb-3">
						<label  for="addressOfOperator"><span>Address of Operator:</span></label>
			 			<input type="text" name="addressOfOperator"  id="addressOfOperator"  class="addressOfOperator form-control" />
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
			</div>
			<div class="form-row">
    				<div class="col-md-6 mb-3">			
						<label  for="operatorClassification"><span>Operator Classification:</span></label>				
						<select name="operatorClassification"  id="operatorClassification"  class="operatorClassification form-control">
										<option value="-- PLS SELECT --"></option>										
										<option value="BFAR">Bureau of Fisheries and Aquatic Resources</option>
										<option value="LGU">Local Government Unit (LGU)</option>
										<option value="Private">Private Sector</option>
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>	

			</div>
			<div class="form-row" id="bfar_id" style="display: none">
    				<div class="col-md-6 mb-3">
						<label  for="legislative"><span>Legislative</span></label>	
						<select name="legislated"  id="legislated"  class="legislated form-control">
										<option value=""></option>
										<option value="legislated">Legislated</option>
										<option value="nonLegislated">Non-legislated</option>
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-6 mb-3">
						<label  for="legislative"><span>Status</span></label>	
						<select name="status"  id="status"  class="status form-control">
										<option value=""></option>
										<option value="Completed">Completed</option>
										<option value="OngoingConstruction">Ongoing Construction</option>
										<option value="PendingImplementation">Pending Implementation</option>
										
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
		</div>
			<div class="form-row">
    				<div class="col-md-6 mb-3">			
						<label  for="indicateSpecies"><span>Indicate Species:</span></label>
			 			<input type="text" name="indicateSpecies"  id="indicateSpecies"  class="hatcheryindicateSpecies form-control"/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
<!-- 					<div class="col-md-6 mb-3">		
						<label  for="publicprivate"><span>Type</span></label>	
						<select name="publicprivate"  id="publicprivate"  class="hatcherypublicprivate form-control">
										<option value=""></option>
										<option value="LGU">LGU</option>
										<option value="Private">Private</option>
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div> -->
			</div>			
		<fieldset><legend>Production</legend>
		<div class="form-row">
    				<div class="col-md-4 mb-3">
						<label  for="prodStocking"><span>Stocking Density/Cycle:</span></label>
			 			<input type="text" name="prodStocking"  id="prodStocking"  class="prodStocking form-control"/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>	
					<div class="col-md-4 mb-3">
						<label  for="prodCycle"><span>Cycle/Year:</span></label>
			 			<input type="text" name="prodCycle"  id="prodCycle"  class="prodCycle form-control"/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-4 mb-3">	
						<label  for="actualProdForTheYear"><span>Actual Production for the year:</span></label>
			 			<input type="text" name="actualProdForTheYear"  id="actualProdForTheYear"  class="actualProdForTheYear form-control" />
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
		</div>			
		</fieldset>
	<div class="form-row">
    				<div class="col-md-6 mb-3">
						<label  for="legislative"><span>RA number</span></label>	
						<input type="text" name="ra"  id="ra"  class="ra form-control">
					
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-6 mb-3">
						<label  for="legislative"><span>Title</span></label>	
						<input type="text" name="title"  id="title"  class="title form-control">
						
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
		</div>
		</fieldset>
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ADDITIONAL INFORMATION</legend>
		<div class="form-row">
    				<div class="col-md-6 mb-3">
					<label  for="dateAsOf"><span>Data as of:</span></label>
					<input type="date" name="dateAsOf" data-date-format="YYYY-MM-dd" id="dateAsOf" class="dateAsOf form-control">
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>	
					<div class="col-md-6 mb-3">	
						<label  for="sourceOfData"><span>Data Source/s:</span></label>	
						<input type="text" name="dataSource"  id="dataSource"  class="dataSource form-control"/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
					</div>
				</div>
				<div class="form-row">
    				<div class="col-md-12 mb-3">
					<label  for="remarks"><span>Remarks:</span></label>
						<textarea contenteditable="true" name="remarks" id="remarks"  class="remarks form-control"></textarea>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      				</div>
      			</div>
		</fieldset>
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">GEOGRAPHICAL LOCATION</legend>
			
				<div class="form-row">
    				<div class="col-md-6 mb-3">
						<label  for="lat"><span>Latitude</span></label>
						<input type="text" name="lat" readonly="readonly"  id="lat" class="lat latValidation form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
      				</div>
      				<div class="col-md-6 mb-3">
						<label  for="lon"><span>Longitude</span></label>	
						<input type="text" name="lon" readonly="readonly"  id="lon" class="lon lonValidation form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      				</div>	
				</div>
				<div class="form-row" id="image_form">
    				<div class="col-md-12 mb-3">
			 		<label  for="photos"><span>Captured Image</span></label>   			
					<input type="file" id="photos" class="form-control file" accept="image/*" />
     				<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      				</div>	
				</div>
     			</fieldset>
     				<div id="img_preview" style="display: none">
     						<img id="preview" name="preview" class="preview" style="width: 450px; height: 200px"></img>
     				</div>
     						<input type="hidden" name="image_src" id="image_src" class="image_src" value="" /><br />
							<input type="hidden" name="image"  id="image" class="image"/>			
		 
		  <div class="btn-group" role="group" aria-label="Basic example">
		  		<button type="button" id="submitHatchery" class="submitHatchery btn btn-primary">SUBMIT</button>
				<button type="button" id="cancel" class="cancel btn btn-secondary">CANCEL</button>
			</div>
	</form>
	  <div id="pdf" style="display: none">
        				<button class="exportToPDF btn-info">Generate PDF</button>
  	
    			</div>
</div>
		
<br><br>
  <div id="table">				
<table id="pdfHATCHERY" style="display: none;">
  <thead>
    <tr>
      <th scope="col">HARCHERY</th>
      <th scope="col">&nbsp;</th>
    </tr>
  </thead>
  <tbody id="tbodyID">
  </tbody>
</table>
</div>  

 <div class="container"> 
 <button class="create btn-primary">Create</button>
 <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','Hatchery')" />
     
 <div class="row">
       <div class="col-md-12 table-responsive">
<table id="export-buttons-table" class="table table-hover">
                <thead>
                  <tr>
                    <th>Region</th><th>Province</th><th>Municipality</th><th>Barangay</th><th>Name of Hatchery</th><th>Coordinates</th>
                  </tr>
                </thead>
                <tbody> 
               </tbody>
              </table>
              <div id="paging">
              
             </div>
 
           <table class="table2excel table" id="exTable" style="display: none">
                <thead>
         		<!-- <tr><th align="center" colspan="21" style="height: 200px">-->
         		<!-- <img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"> 
         		</th></tr>              -->
                <tr><th align="center" colspan="5">HATCHERY</th></tr>
                
                <tr>
                  <th>${region}</th>
                  </tr>
                  <tr>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Legislated</th>
                     <th>Status</th>
                      <th>RA#</th>
                     <th>Title</th>
                     <th>Area</th>
                     <th>Name of Hatchery</th>
                     <th>Name of Operator</th>
                     <th>Address of Operator</th>
                     <th>Operator Classification</th>
                    <th>Type</th>
                     <th>Indicate Species</th>                     
                      <th>Stocking Density/Cycle</th>
                       <th>Cycle/Year</th>
                       <th>Actual Production for the year</th>
                      
                          <th>Data Sources</th>
                           <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Remarks</th>   
                  </tr>
                </thead>
                <tbody id="emp_body" style="border: 1px;">
               </tbody>
              </table>       
    <!-- <button class="exportToExcel btn-info">Export to XLS</button>
     -->
      
       </div>
 
 </div>
  </div>
 </div>
 <jsp:include page="../menu/fma_details.jsp"></jsp:include>
 
  </div>
<div id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</div> 
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>	
<script src="/static/js/form/provinceMunBarangay.js"></script>	
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script src="/static/js/validationForm/hatchery.js" type=" text/javascript"></script>

<script  src="/static/js/resource.js" ></script>
<script  src="/static/js/api/regionMap.js" ></script>
<script src="/static/js/map/philippineMap.js"></script>
<script  src="/static/js/resourcesOnMapByResources.js" ></script>
	<script>
	
	 var map_location = ${mapsData}; 

		function getFisheriesData(){
			getHatchery(map_location);
		}

</script>
	</body>

</html>