<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="en">
<head>
<title>Fish Sanctuary</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
<script src="/static/js/form/deleteMapList.js" type=" text/javascript"></script>
<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
<script src="/static/leaflet/leaflet.js"></script>
<script src="/static/leaflet/leaflet.ajax.min.js"></script>
<script src="//d3js.org/d3.v3.min.js"></script>
<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
<script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
<script src="/static/leaflet/bundle.js"></script>
<link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
<script src="/static/leaflet/L.Control.SlideMenu.js"></script>
<script src="/static/leaflet/leaflet-bootstrapmodal.js"></script>
<script src="https://cdn.jsdelivr.net/npm/exif-js"></script>
<style type="text/css">
.table2excel{
	padding: 5px;
}
#map{ 
	width:100%;
  	height: 600px;
   background: #009dc4;
}

</style>
</head>
<sec:authorize access="hasRole('ROLE_USER')">
<body onload="getFisheriesData()">
</sec:authorize>
<sec:authorize access="hasRole('ROLE_REGIONALADMIN')  or hasRole('ROLE_SUPERADMIN')">
<body>
</sec:authorize>
<%-- 	<sec:authorize access="hasRole('ROLE_USER')">
  		<jsp:include page="../home/header_users.jsp"></jsp:include>
  	</sec:authorize>
  	<sec:authorize access="hasRole('ROLE_REGIONALADMIN')">
   		<jsp:include page="../home/header_region.jsp"></jsp:include> 
   </sec:authorize>
     <sec:authorize access="hasRole('ROLE_SUPERADMIN')">
     <jsp:include page="../home/header.jsp"></jsp:include>
     </sec:authorize> --%>
<jsp:include page="../home/header_users.jsp"></jsp:include>     
<div class="wrapper d-flex align-items-stretch text-dark">

	<sec:authorize access="hasRole('ROLE_REGIONALADMIN')  or hasRole('ROLE_SUPERADMIN')">
		<jsp:include page="../report/sidenavbar.jsp"></jsp:include>
	</sec:authorize>


    <div id="content" class="p-4 p-md-5 pt-5 section-to-print" >
   <sec:authorize access="hasRole('ROLE_USER')">
    <c:if test="${mapsData != '' }">
    <div id="mapview">
    <div id="map"></div>
    </div>
    </c:if>
    <c:if test="${mapsData == '' }">
    <div id="mapview"  style="display:none;">
    <div id="map"></div>
    </div>
    </c:if>
     </sec:authorize>
    <h1 align="center">FISH SANCTUARY</h1>
    <sec:authorize access="hasRole('ROLE_USER')">
	<div class="form_input" style="display: none">
				<form class="needs-validation" id="myform" novalidate>
	 			
	 			<input type="number" name="id" id="id" class="id" hidden="true"/>
				<input type="hidden" name="user" id="user" class="user"/>
				<input type="hidden" name="uniqueKey" id="uniqueKey" class="uniqueKey"/>
				<input type="hidden" name="dateEncoded" id="dateEncoded" class="dateEncoded"/>
				<input type="hidden" name="dateEdited" id="dateEdited" class="dateEdited" />
				<input type="hidden" name="encodedBy" id="encodedBy" class="encodedBy"/>
				<input type="hidden" name="editedBy" id="editedBy" class="editedBy" />
				<input type="hidden" name="region" id="region" class="region"/>
				<input type="hidden"  name="page" id="page" class="page"/>			

<fieldset class="form-group border border-dark rounded p-4">
	<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH SANCTUARY LOCATION</legend>
			<div class="form-row">
    			<div class="col-md-4 mb-3">
					<label  for="validationCustom01">Province</label>
					<select name="province"  id="validationCustom01" class="form-control province" required></select>															
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid province.
      				</div>
					</div>


    			<div class="col-md-4 mb-3">	
				<label  for="validationCustom02">Municipality/City </label> 		
				  <select name="municipality" id="validationCustom02"  class="form-control municipality" required></select>	
				 <div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Municipality/City 
      				</div>
					</div>

    			<div class="col-md-4 mb-3">															
				<label  for="validationCustom03">Barangay</label>
					<select name="barangay"  id="validationCustom03"  class="form-control barangay" required>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Barangay
      				</div>
					</div>
			</div>
			
			<div class="form-row">
    			<div class="col-md-12 mb-3">			
					<label for="code">Code:</label>
						<input type="text" name="code" id="code"  class="code" placeholder="Code" readonly="readonly"/>
				</div>
			</div>	
			</fieldset>						
<!--end same as other pages -->
		
<fieldset class="form-group border border-dark rounded p-4">
		<legend  class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH SANCTUARY INFORMATION</legend>	
		<div class="form-row">
    			<div class="col-md-6 mb-3">	
			 <label  for="validationCustom04">Name of Sanctuary/Protected Area</label>
			 <input type="text" name="nameOfFishSanctuary" id="validationCustom04"  class="form-control input-group-lg nameOfFishSanctuary" required/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>
					

    			<div class="col-md-6 mb-3">			
				<label  for="validationCustom05">Implementer</label>				
				<select name="bfardenr"  id="validationCustom05"  class="form-control bfardenr" required>
									<!-- 	<option value=""></option>
										<option value="BFAR">BFAR</option>
										<option value="DENR">DENR</option>
										<option value="LGU">LGU</option>
										<option value="NGO">NGO</option>
										<option value="SCHOOL">SCHOOL</option> -->
				</select>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>

						</div>	
						
			<div id="bfarData" style="display:none;">
				<div class="form-row">
    				<div class="col-md-6 mb-3">	
						<label  for="sheltered">Name of Sheltered Fisheries/Species:</label>
			 			<input type="text" name="sheltered"  id="sheltered"  class="sheltered form-control form-control-sm"/>				
					</div>
				</div>
			</div>
		
		<div id="denrData" style="display:none;">
		<div class="form-row">
    			<div class="col-md-6 mb-3">	
			<label  for="nameOfSensitiveHabitatMPA">Name of Sensitive Habitat with MPA:</label>	
			<input type="text" name="nameOfSensitiveHabitatMPA"  id="nameOfSensitiveHabitatMPA"  class="nameOfSensitiveHabitatMPA form-control"/>
			</div>
			<div class="col-md-6 mb-3">	
			<label  for="OrdinanceNo">Ordinance No.</label>					
			<input type="text" name="ordinanceNo" id="ordinanceNo" class="ordinanceNo form-control"/>
			</div>
			</div>
			<div class="form-row">
    			<div class="col-md-6 mb-3">	
			<label for="OrdinanceTitle">Ordinance Title</label>
			<input type="text" name="ordinanceTitle" id="ordinanceTitle"  class="ordinanceTitle form-control"/>
			</div>
			<div class="col-md-6 mb-3">	
			<label for="DateEstablished">Date Established</label>
			<input type="date" name="dateEstablished" id="dateEstablished" class="dateEstablished form-control "/>
			</div>			
		</div>
		</div>
		
		
		<div class="form-row">
    			<div class="col-md-6 mb-3">	
					<label  for="validationCustom06">Area</label>
					<input type="text" name="area"  id="area"  class="area form-control" required/>	
					<!-- <input type="number" name="area"  id="validationCustom06"  class="form-control area" required/>	 -->															
					<div class="valid-feedback">
        				ok.<i class="fas fa-check-circle"></i>
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>


    			<div class="col-md-6 mb-3">	
					<label  for="validationCustom07">Type</label>	
					<select name="type"  id="validationCustom07"  class="form-control type" required>
										<option value="">Select</option>
										<option value="National">National</option>
										<option value="Local">Local</option>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>

			</div>
</fieldset>
<fieldset class="form-group border border-dark rounded p-4">
<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ADDITIONAL INFORMATION</legend>	
		
			<div class="form-row">

    			<div class="col-md-6 mb-3">	
					<label  for="dateAsOf">Data as of</label>
					<!-- <input type="text" name="dateAsOf" id="dateAsOf" class="form-control dateAsOf"  required> -->
					<input type="date" name="dateAsOf"  data-date-format="YYYY-MM-dd" id="dateAsOf" class="form-control form-control-sm dateAsOf" required>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
		<!-- 	<div class="col-md-6 mb-3">	
					<input type="text" name="startDate" id="startDate" class="date-picker"  data-date="" value="" />
			</div>-->

    			<div class="col-md-6 mb-3">	
				<label  for="validationCustom09">Data Source/s</label>
					<input type="text" name="fishSanctuarySource"  id="validationCustom09"  class="form-control fishSanctuarySource" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>

			</div>
		<div class="form-row">
    		<div class="col-md-12 mb-3">					
				<label  for="validationCustom10">Remarks</label>
					<textarea name="remarks" id="validationCustom10"  class="form-control remarks" required></textarea>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

		</div>
		</fieldset>
<fieldset  class="form-group border border-dark rounded p-4">
	<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">GEOGRAPHICAL LOCATION</legend>
		<div class="form-row">

    		<div class="col-md-6 mb-3">	
				<label  for="lat">Latitude</label>			
					<input type="text" name="lat" readonly="readonly" value="" id="lat" class="form-control lat" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

    		<div class="col-md-6 mb-3">	
				<label  for="lon">Longitude</label>
				<input type="text" name="lon"  readonly="readonly"  id="lon" class="form-control lon" required/>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

		</div>	
			<div class="form-row" id="image_form">
    			<div class="col-md-12 mb-3">				
			  		<label  for="photos">Captured Image</label>		
					<input type="file" id="photos" class="form-control file" accept="image/*"/>
    				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
			</div>
</fieldset>
     			
     			
     				<div id="img_preview" style="display: none">
     					 	<img id="preview" name="preview" class="preview" style="width: 450px; height: 200px"></img>
     				</div>
     				
<!--      					 	<img id="metadata" name="metadata" class="metadata" style="width: 450px; height: 200px"></img> -->
     				
     				<input type="hidden" name="image_src" id="image_src" class="image_src" value="" /><br />
					<input type="hidden" name="image"  id="image" class="image"/>	
				
		<div class="btn-group" role="group" aria-label="Basic example">
			 	<button type="submit" id="submitFishSanctuaries" class="submitFishSanctuaries btn btn-primary">SUBMIT</button>
				<button type="button" id="cancel" class="cancel btn btn-secondary">CANCEL</button>
		</div>
		
		</form>
			<div id="pdf" style="display: none">
        		<button class="exportToPDF btn-info">Generate PDF</button>    	
    		</div>
</div>
</sec:authorize>
<br><br>

<sec:authorize access="hasRole('ROLE_USER')">
  <div id="table">				
<table id="pdfFISHSANCTUARY" style="display: none;">
  <thead>              
   <tr>
      <th scope="col">FISH SANCTUARY </th>
      <th scope="col">&nbsp;</th>
    </tr>
  </thead>
  <tbody  id="tbodyID">
  </tbody>
</table> 

</div> 
</sec:authorize>
<br><br>


 
<div class="container noToPrint"> 
<sec:authorize access="hasRole('ROLE_USER')">
<button class="create btn-primary">Create</button>
</sec:authorize>
 <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','FishSanctuary')" />
     
 <div class="row">
       <div class="col-md-12 table-responsive">     
			<table id="export-buttons-table" class="table table-hover">
                <thead>
                  <tr>
        
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of FishSanctuary</th>
                    <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody> 
               </tbody>
              </table>
              <div id="paging">
             <!--${page}  --> 
             </div>
             
 		<table class="table2excel" id="exTable" style="display: none">
 				
                <thead>
                
                 <tr><th align="center" colspan="5">FISH SANCTUARY</th></tr>
                
                <tr>
                     <th>${region}</th>
                 </tr>  
                 
                  <tr>
                   <th>Province</th>
                   <th>Municipality</th>
                   <th>Barangay</th>
                   <th>Name of FishSanctuary</th>
                   <th>Code</th>
                   <th>Area</th>
                   <th>Type</th>
                   <th>Data Source</th>
                   <th>Implementer</th>
                   <th>Sheltered</th>
                   <th>Name Of Sensitive Habitat MPA</th>
                   <th>Ordinance No.</th>
                   <th>Ordinance Title</th>
                   <th>Date Established</th>
                   <th>Latitude</th>
                   <th>Longitude</th>
                   <th>Remarks</th>
                  </tr> 
                </thead>
                <tbody id="emp_body">
                
               </tbody>
              </table> 
      </div>
 
 	</div>
  	</div> 
  	 </div>
 <sec:authorize access="hasRole('ROLE_USER')">
<jsp:include page="../menu/fma_details.jsp"></jsp:include>
</sec:authorize>
    </div>
   <div id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</div> 
<sec:authorize access="hasRole('ROLE_USER')">	
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>		
<script src="/static/js/form/provinceMunBarangay.js"></script>
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script  src="/static/js/resource.js" ></script>
<script  src="/static/js/api/regionMap.js" ></script>
<script src="/static/js/map/philippineMap.js"></script>
<script  src="/static/js/resourcesOnMapByResources.js" ></script>
<script src="/static/js/validationForm/fishSanctuary.js" type=" text/javascript"></script>
<script  src="/static/js/jpegmeta.js" ></script>
	<script>

	 var map_location = '${mapsData}'; 

		function getFisheriesData(){
			getFishSanctuaries(map_location);
		}

</script>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_REGIONALADMIN')">
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script src="/static/js/api/create_update.js"></script>
<script src="/static/js/excel/user/fishSanctuaryExcelReport.js"></script>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_SUPERADMIN')">
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script src="/static/js/api/create_update.js"></script>
<script src="/static/js/excel/fishSanctuaryExcelReport.js"></script>
</sec:authorize>
    </body>
</html>