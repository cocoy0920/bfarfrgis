<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="en">
<head>
<title>IPCS</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
<script src="/static/leaflet/leaflet.js"></script>
<script src="/static/leaflet/leaflet.ajax.min.js"></script>
<script src="//d3js.org/d3.v3.min.js"></script>
<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
<script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
<script src="/static/leaflet/bundle.js"></script>
<link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
<script src="/static/leaflet/L.Control.SlideMenu.js"></script>
<script src="/static/leaflet/leaflet-bootstrapmodal.js"></script>
<script src="https://cdn.jsdelivr.net/npm/exif-js"></script>

<style type="text/css">
.table2excel{
	padding: 5px;
}
#map{ 
	width:100%;
  	height: 600px;
   background: #009dc4;
}
</style>
</head>
<body onload="getFisheriesData()">

  <jsp:include page="../home/header_users.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">

    <div id="content" class="p-4 p-md-5 pt-5">
		<c:if test="${mapsData != '' }">
    <div id="mapview">
    <div id="map"></div>
    </div>
    </c:if>
    <c:if test="${mapsData == '' }">
    <div id="mapview"  style="display:none;">
    <div id="map"></div>
    </div>
    </c:if>

 <h1 align="center">COLD STORAGE</h1>
 <div class="form_input" style="display: none">
		 <form class="needs-validation" id="myform" novalidate>
				<input type="hidden" name="id" id="id" class="id"/>
				<input type="hidden" name="user" id="user" class="user"/>
				<input type="hidden" name="uniqueKey" id="uniqueKey" class="uniqueKey"/>
				<input type="hidden" name="dateEncoded" id="dateEncoded" class="dateEncoded"/>
				<input type="hidden" name="dateEdited" id="dateEdited" class="dateEdited" />
				<input type="hidden" name="encodedBy" id="encodedBy" class="encodedBy"/>
				<input type="hidden" name="editedBy" id="editedBy" class="editedBy" />
				<input type="hidden" name="region" id="region" value="${region }" class="region"/>	
				<input type="hidden"  name="page" id="page" value="${page }" class="page"/>			
				
				<br>
		
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ICE PLANT / COLD STORAGE LOCATION</legend>
				<div class="form-row">
    			<div class="col-md-4 mb-3">
					<label  for="province"><span>Province</span></label>					
						<select name="province"  id="province" class="province form-control" required>																																					
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Please select a valid province.
      					</div>
					</div>	
					<div class="col-md-4 mb-3">															
						<label  for="municipality"><span>Municipality</span></label>				
							<select name="municipality" id="municipality"  class="municipality form-control" required></select>															
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Please select a valid Municipality/City 
      					</div>
					</div>
					<div class="col-md-4 mb-3">	
						<label  for="barangay"><span>Barangay</span></label>	
							<select name="barangay"  id="barangay"  class="barangay form-control" required></select>															
								<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Please select a valid Barangay
      							</div>
					</div>
				</div>
			<div class="form-row">
    			<div class="col-md-12 mb-3">
					<label  for="code"><span>Code</span></label>			
					<input type="text" name="code" id="code"  class="code input-field " required placeholder="Code" readonly="readonly"/>
				</div>
			</div>															
			</fieldset>
			
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ICE PLANT / COLD STORAGE INFORMATION</legend>
			<div class="form-row">
    			<div class="col-md-4 mb-3">
			 		<label  for="nameOfIcePlant"><span>Name of Ice Plant or Cold Storage:</span></label>
			 		<input type="text" name="nameOfIcePlant"  id="nameOfIcePlant"  class="nameOfIcePlant form-control" required/>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
				<div class="col-md-4 mb-3">
					<label  for="sanitaryPermit"><span>Valid Sanitary Permit:</span></label>
			 		<input type="text" name="sanitaryPermit"  id="sanitaryPermit"  class="sanitaryPermit form-control" required/>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
				<div class="col-md-4 mb-3">
					<label  for="operator"><span>Operator:</span></label>
			 		<select name="operator"  id="operator"  class="operator form-control" required>
										<option value="">- Please select - </option>
										<option value="BFAR">BFAR</option>
										<option value="COOPERATIVE">COOPERATIVE</option>
										<option value="PRIVATE">PRIVATE ORGANIZATION</option>
						</select>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
				</div>
				<div class="form-row">
    				<div class="col-md-4 mb-3">
						<label  for="typeOfFacility"><span>Type of Facility:</span>	</label>			
						<select name="typeOfFacility"  id="typeOfFacility"  class="typeOfFacility form-control" required>
										<option value="">- Please select -</option>
										<option value="IcePlant">Ice Plant</option>
										<option value="ColdStorage">Cold Storage</option>
										<option value="IcePlantColdStorage">Ice Plant and Cold Storage</option>
						</select>
						<div class="valid-feedback">
        						ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-4 mb-3">
						<label  for="structureType"><span>Structure Type:</span></label>				
						<select name="structureType"  id="structureType"  class="structureType form-control" required>
										<option value=""></option>
										<option value="Fixed Structure">Fixed Structure</option>
										<option value="Mobile">Mobile</option>
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-4 mb-3">
						<label  for="capacity"><span>Capacity:</span></label>
			 			<input type="text" name="capacity"  id="capacity"  class="capacity form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					</div>
					<div class="form-row">
    					<div class="col-md-2 mb-3">
							<label  for="withValidLicenseToOperate"><span>With Valid License to Operate:</span></label>	
							<select name="withValidLicenseToOperate"  id="withValidLicenseToOperate"  class="withValidLicenseToOperate form-control" required>
										<option value=""></option>
										<option value="Yes">Yes</option>
										<option value="No">No</option>
							</select>
							<div class="valid-feedback">
        						ok.
      						</div>
      						<div class="invalid-feedback">
        						Required
      						</div>
						</div>
						<div class="col-md-2 mb-3">
							<label  for="withValidSanitaryPermit"><span>With Valid Sanitary Permit</span></label>	
							<select name="withValidSanitaryPermit"  id="withValidSanitaryPermit"  class="withValidSanitaryPermit form-control" required>
										<option value=""></option>
										<option value="Yes">Yes</option>
										<option value="No">No</option>
							</select>
							<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        						Required
      						</div>
						</div>
						
							<div class="col-md-4 mb-3">	
							<div id="yesData" style="display:none;">						
								<label  for="withValidSanitaryPermitYes"><span>Type Sanitary Permit</span></label>
								<select name="withValidSanitaryPermitYes"  id="withValidSanitaryPermitYes"  class="withValidSanitaryPermitYes form-control">
										<option value=""></option>
										<option value="Marine">Marine</option>
										<option value="FishPond">Fish Pond</option>
										<option value="Others">Others</option>
								</select>
								<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Required
      							</div>
							</div>	
						</div>
						
								<div class="col-md-4 mb-3">
									<div id="othersData" style="display:none;">	
								<label  for="withValidSanitaryPermitOthers"><span>Others</span>	</label>					
								<input type="text" name="withValidSanitaryPermitOthers" id="withValidSanitaryPermitOthers" class="withValidSanitaryPermitOthers form-control"/>
								<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        						Required
      							</div>
							</div>	
						</div>	
						
						</div>
			<div class="form-row">
						<div class="col-md-4 mb-3">						
							<label  for="businessPermitsAndCertificateObtained"><span>Business Permits and Certificate Obtained:</span></label>
							 <input type="text" name="businessPermitsAndCertificateObtained"  id="businessPermitsAndCertificateObtained"  class="iceplantbusinessPermitsAndCertificateObtained form-control" required/>
								<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
						</div>
    			<div class="col-md-4 mb-3">
					<label  for="typeOfIceMaker"><span>Type of Ice Maker:</span></label>
			 		<input type="text" name="typeOfIceMaker"  id="typeOfIceMaker"  class="typeOfIceMaker form-control" required/>
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
				</div>	
			<div class="form-row">	
				<div class="col-md-4 mb-3">				
					<label  for="foodGradule"><span>Food Gradule</span></label>	
					<select name="foodGradule"  id="foodGradule"  class="foodGradule form-control" required>
										<option value=""></option>
										<option value="Yes">Yes</option>
										<option value="No">No</option>
					</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
				<div class="col-md-4 mb-3">	
					<label  for="area"><span>Area</span></label>
					<input type="text" name="area"  id="area"  class="area form-control" required/>	
					<!-- <input type="number" name="area"  min="0" value="0" step="any"   id="area"  class="icearea form-control" required/>	 -->															
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
				</div>
		</div>
		</fieldset>
		
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ADDITIONAL INFORMATION</legend>
		<div class="form-row">
    				<div class="col-md-6 mb-3">
					<label  for="dateAsOf"><span>Data as of:</span></label>
					<input type="date" name="dateAsOf" data-date-format="YYYY-MM-dd" id="dateAsOf" class="dateAsOf form-control" required>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>	
					<div class="col-md-6 mb-3">	
						<label  for="sourceOfData"><span>Data Sources:</span></label>	
						<input type="text" name="dataSource"  id="dataSource"  class="dataSource form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
					</div>
				</div>
				<div class="form-row">
    				<div class="col-md-12 mb-3">
					<label  for="remarks"><span>Remarks:</span></label>
						<textarea contenteditable="true" name="remarks" id="remarks"  class="remarks form-control" required></textarea>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      				</div>
      			</div>
		</fieldset>
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">GEOGRAPHICAL LOCATION</legend>
			<div class="form-row">
    				<div class="col-md-6 mb-3">
						<label  for="lat"><span>Latitude</span></label>
						<input type="text" name="lat"  readonly="readonly"  id="lat" class="lat latValidation form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
      				</div>
      				<div class="col-md-6 mb-3">
						<label  for="lon"><span>Longitude</span></label>	
						<input type="text" name="lon"   readonly="readonly"  id="lon" class="lon lonValidation form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      				</div>	
				</div>
				<div class="form-row" id="image_form">
    				<div class="col-md-12 mb-3">
			 		<label  for="photos"><span>Captured Image</span></label>   			
					<input type="file" id="photos" class="form-control file" accept="image/*" />
     				<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      				</div>	
				</div>
     			</fieldset>
     				<div id="img_preview" style="display: none">
     						<img id="preview" name="preview"  class="preview" style="width: 450px; height: 200px"></img>
     				</div>
     						<input type="hidden" name="image_src"  id="image_src" class="image_src" value="" /><br />
							<input type="hidden" name="image"  id="image" class="image"/>
		
		
		 <div class="btn-group" role="group" aria-label="Basic example">								
		 	<button type="button" id="submitIcePlantColdStorage" class="submitIcePlantColdStorage btn btn-primary" data-dismiss="modal">SUBMIT</button>
			<button type="button" id="cancel" class="cancel btn btn-secondary">CANCEL</button>
		</div>
		</form>
		<div id="pdf" style="display: none">
        		<button class="exportToPDF btn-info">Generate PDF</button>
		</div>
</div>
		  
<br><br>
  <div id="table">				
<table id="pdfICE" style="display: none;">
  <thead>
    <tr>
      <th scope="col">ICE PLANT/COLD STORAGE</th>
      <th scope="col">&nbsp;</th>
    </tr>
  </thead>
  <tbody id="tbodyID">
  </tbody>
</table> 

</div>  

 <div class="container"> 
 <button class="create btn-primary">Create</button>
  <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','ColdStorage')" />
    
 <div class="row">
       <div class="col-md-12 table-responsive">
<table id="export-buttons-table" class="table table-hover">
                <thead>
                  <tr>
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Ice Plant or Cold Storage</th>
                    <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody>  
               </tbody>
              </table>
              <div id="paging">
        
             </div>
 
     <table class="table2excel table table-bordered" id="exTable" style="display: none">
                <thead>
               
                <tr><th align="center" colspan="5">ICE PLANT/COLD STORAGE</th></tr>
                <tr>
                <th>${region}</th>
                  </tr>
                  <tr>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Ice Plant or Cold Storage</th>
                    <th>Code</th>
                   <th>Area</th>
                    <th>Valid Sanitary Permit</th>
                    <th>Operator</th>
                    <th>Type of Facility</th>
                    <th>Structure Type</th>
                    <th>Capacity</th>
                    <th>With Valid License to Operate</th>
                    <th>With Valid Sanitary Permit</th>
                    <th>Type Sanitary Permit</th>
                    <th>Others</th>
                    <th>Business Permits and Certificate Obtained</th>                    
                    <th>Type of Ice Maker</th>
                    <th>Food Gradule</th>
                    <th>Data Source</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Remarks</th>
                  
                  </tr>
                </thead>
                <tbody id="emp_body">
               </tbody>
              </table>       
    <!-- <button class="exportToExcel btn-info">Export to XLS</button>
     -->  
      
     </div>
 
 </div>
  </div>
</div>
<jsp:include page="../menu/fma_details.jsp"></jsp:include>

  </div>
         	<div id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</div> 
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>	
<script src="/static/js/form/provinceMunBarangay.js"></script>		
<script src="/static/js/form/jspdf.debug.js"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script src="/static/js/validationForm/icePlant.js"></script>
<script  src="/static/js/resource.js" ></script>
<script  src="/static/js/api/regionMap.js" ></script>
<script src="/static/js/map/philippineMap.js"></script>
<script  src="/static/js/resourcesOnMapByResources.js" ></script>
	<script>
	
 	 var map_location = ${mapsData}; 

		function getFisheriesData(){
			getIPCS(map_location);
		}

</script>
	</body>

</html>