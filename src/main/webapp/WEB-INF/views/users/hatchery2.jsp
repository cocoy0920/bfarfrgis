<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Hatchery</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
</head>
<body>

 <jsp:include page="../home/header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">
  <!-- Sidebar Holder -->
 <jsp:include page="../menu/sidebar2.jsp"></jsp:include>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
<div class="create" style="display: none">
		<form class="needs-validation" id="myform" novalidate>
			<input type="hidden" name="id" id="id" class="id" />
			<input type="hidden" name="user" id="user" class="user" />
			<input type="hidden" name="uniqueKey" id="uniqueKey" class="uniqueKey" />		
			<input type="hidden" name="encodedBy" id="encodedBy" class="encodedBy" />
			<input type="hidden" name="editedBy" id="editedBy" class="editedBy" />
			<input type="hidden" name="dateEncoded" id="dateEncoded" class="dateEncoded" />
			<input type="hidden" name="dateEdited" id="dateEdited" class="dateEdited" />
			<input type="hidden"  name="region" id="region" value="${region }" class="region"/>		
			<input type="hidden"  name="page" id="page" value="${page }" class="page"/>			
				
			<br>
			
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">HATCHERY LOCATION</legend>
				<div class="form-row">
    			<div class="col-md-4 mb-3">
					<label class="col-form-label-lg" for="province"><span>Province</span></label>					
						<select name="province"  id="province" class="province form-control" required>																																					
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Please select a valid province.
      					</div>
					</div>	
					<div class="col-md-4 mb-3">															
						<label class="col-form-label-lg" for="municipality"><span>Municipality</span></label>				
							<select name="municipality" id="municipality"  class="municipality form-control" required></select>															
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Please select a valid Municipality/City 
      					</div>
					</div>
					<div class="col-md-4 mb-3">	
						<label class="col-form-label-lg" for="barangay"><span>Barangay</span></label>	
							<select name="barangay"  id="barangay"  class="barangay form-control" required></select>															
								<div class="valid-feedback">
        							ok.
      							</div>
      							<div class="invalid-feedback">
        							Please select a valid Barangay
      							</div>
					</div>
				</div>
			<div class="form-row">
    			<div class="col-md-12 mb-3">
					<label class="col-form-label-lg" for="code"><span>Code</span></label>			
					<input type="text" name="code" id="code"  class="code input-field " required placeholder="Code" readonly="readonly"/>
				</div>
			</div>															
			</fieldset>
			
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">HATCHERY INFORMATION</legend>
			 <div class="form-row">
    				<div class="col-md-6 mb-3">
			 			<label class="col-form-label-lg" for="nameOfHatchery"><span>Name of Hatchery:</span></label>
			 			<input type="text" name="nameOfHatchery"  id="nameOfHatchery"  class="nameOfHatchery form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-6 mb-3">
						<label class="col-form-label-lg" for="nameOfOperator"><span>Name of Operator:</span></label>
			 			<input type="text" name="nameOfOperator"  id="nameOfOperator"  class="hatcherynameOfOperator form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
			</div>
			<div class="form-row">
    				<div class="col-md-12 mb-3">
						<label class="col-form-label-lg" for="addressOfOperator"><span>Address of Operator:</span></label>
			 			<input type="text" name="addressOfOperator"  id="addressOfOperator"  class="hatcheryaddressOfOperator form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
			</div>
			<div class="form-row">
    				<div class="col-md-6 mb-3">			
						<label class="col-form-label-lg" for="operatorClassification"><span>Operator Classification:</span></label>				
						<select name="operatorClassification"  id="operatorClassification"  class="hatcheryoperatorClassification form-control" required>
										<option value=""></option>
										<option value="LGU">Local Government Unit (LGU)</option>
										<option value="BFAR">Bureau of Fisheries and Aquatic Resources</option>
										<option value="State University">State University</option>
										<option value="Private Individual">Private Individual</option>
										<option value="Private Business">Private Business</option>
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>	
					<div class="col-md-6 mb-3">	
						<label class="col-form-label-lg" for="area"><span>Area(Hectare):</span></label>
						<input type="number" name="area"  id="area"  min="0" value="0" step="any"   class="hatcheryarea form-control" required/>																
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
			</div>
			<div class="form-row">
    				<div class="col-md-6 mb-3">			
						<label class="col-form-label-lg" for="indicateSpecies"><span>Indicate Species:</span></label>
			 			<input type="text" name="indicateSpecies"  id="indicateSpecies"  class="hatcheryindicateSpecies form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-6 mb-3">		
						<label class="col-form-label-lg" for="publicprivate"><span>Type</span></label>	
						<select name="publicprivate"  id="publicprivate"  class="hatcherypublicprivate form-control" required>
										<option value=""></option>
										<option value="Public">Public</option>
										<option value="Private">Private</option>
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
			</div>			
		<fieldset><legend>Production</legend>
		<div class="form-row">
    				<div class="col-md-4 mb-3">
						<label class="col-form-label-lg" for="prodStocking"><span>Stocking Density/Cycle:</span></label>
			 			<input type="text" name="prodStocking"  id="prodStocking"  class="prodStocking form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>	
					<div class="col-md-4 mb-3">
						<label class="col-form-label-lg" for="prodCycle"><span>Cycle/Year:</span></label>
			 			<input type="text" name="prodCycle"  id="prodCycle"  class="prodCycle form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-4 mb-3">	
						<label class="col-form-label-lg" for="actualProdForTheYear"><span>Actual Production for the year:</span></label>
			 			<input type="text" name="actualProdForTheYear"  id="actualProdForTheYear"  class="actualProdForTheYear form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
		</div>			
		</fieldset>
<!-- 		<fieldset><legend>Month range of production cycle</legend>
		<div class="form-row">
    				<div class="col-md-6 mb-3">
						<label class="col-form-label-lg" for="monthStart"><span>Month Start:</span></label>
						<input type="text" name="monthStart" id="monthStart" class="monthStart form-control" required>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-6 mb-3">		
						<label class="col-form-label-lg" for="monthEnd"><span>Month End:</span></label>
						<input type="text" name="monthEnd" id="monthEnd" class="monthEnd form-control" required>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
		</div>
						
		</fieldset> -->
		
		<div class="form-row">
    				<div class="col-md-6 mb-3">
						<label class="col-form-label-lg" for="legislative"><span>Legislative</span></label>	
						<select name="legislated"  id="legislated"  class="legislated form-control" required>
										<option value=""></option>
										<option value="legislated">Legislated</option>
										<option value="nonLegislated">Non-legislated</option>
						</select>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
		</div>
		</fieldset>
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ADDITIONAL INFORMATION</legend>
		<div class="form-row">
    				<div class="col-md-6 mb-3">
					<label class="col-form-label-lg" for="dateAsOf"><span>Data as of:</span></label>
					<input type="text" name="dateAsOf" id="dateAsOf" class="dateAsOf form-control" required>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>	
					<div class="col-md-6 mb-3">	
						<label class="col-form-label-lg" for="sourceOfData"><span>Data Source/s:</span></label>	
						<input type="text" name="dataSource"  id="dataSource"  class="dataSource form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
					</div>
				</div>
				<div class="form-row">
    				<div class="col-md-12 mb-3">
					<label class="col-form-label-lg" for="remarks"><span>Remarks:</span></label>
						<textarea contenteditable="true" name="remarks" id="remarks"  class="remarks form-control" required></textarea>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      				</div>
      			</div>
		</fieldset>
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">GEOGRAPHICAL LOCATION</legend>
			
				<div class="form-row">
    				<div class="col-md-6 mb-3">
						<label class="col-form-label-lg" for="lat"><span>Latitude:(limit up to 6 decimal places)</span></label>
						<input type="text" name="lat"   id="lat" class="lat latValidation form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
      				</div>
      				<div class="col-md-6 mb-3">
						<label class="col-form-label-lg" for="lon"><span>Longitude:(limit up to 6 decimal places)</span></label>	
						<input type="text" name="lon"  id="lon" class="lon lonValidation form-control" required/>
						<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      				</div>	
				</div>
				<div class="form-row" id="image_form">
    				<div class="col-md-12 mb-3">
			 		<label class="col-form-label-lg" for="file"><span>Captured Image</span></label>   			
					<input type="file" id="file" class="form-control file" accept="image/*" />
     				<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        				Required
      					</div>
      				</div>	
				</div>
     			</fieldset>
     				<div id="img_preview" style="display: none">
     						<img id="preview" name="preview" class="preview" style="width: 450px; height: 200px"></img>
     				</div>
     						<input type="hidden" name="image_src" id="image_src" class="image_src" value="" /><br />
							<input type="hidden" name="image"  id="image" class="image"/>			
		 
		  <div class="btn-group" role="group" aria-label="Basic example">
		  		<button type="button" id="submitHatchery" class="submitHatchery btn btn-primary">SUBMIT</button>
				<button type="button" id="cancel" class="cancel btn btn-secondary">CANCEL</button>
			</div>
	</form>
</div>
		  <div id="pdf" style="display: none">
        				<button class="exportToPDF btn-info">Generate PDF</button>
  	
    			</div>
<br><br>
  <div id="table">				
<table id="pdfHATCHERY" style="display: none;">
  <thead>
    <tr>
      <th scope="col">HARCHERY</th>
      <th scope="col">&nbsp;</th>
    </tr>
  </thead>
  <tbody id="tbodyID">
  </tbody>
</table>
</div>  

 <div class="container"> 
 <button class="create btn-primary">Create</button>
 <div class="row">
       <div class="col-md-12 table-responsive">
<table id="export-buttons-table" class="table table-hover">
                <thead>
                  <tr>
                    <th>Region</th><th>Province</th><th>Municipality</th><th>Barangay</th><th>Name of Hatchery</th><th>Coordinates</th>
                  </tr>
                </thead>
                <tbody> 
               </tbody>
              </table>
              <div id="paging">
              
             </div>
 
           <table class="table2excel" style="display: none">
                <thead>
                <tr><th align="center" colspan="21">HATCHERY</th></tr>
                
                <tr>
                  <th>${region}</th>
                  </tr>
                  <tr>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Hatchery</th>
                    <th>Legislated</th>
                     <th>Area (sqm.)</th>
                     <th>Name of Hatchery</th>
                     <th>Name of Operator</th>
                     <th>Address of Operator</th>
                     <th>Operator Classification</th>
                    <th>Type</th>
                     <th>Indicate Species</th>                     
                      <th>Stocking Density/Cycle</th>
                       <th>Cycle/Year</th>
                       <th>Actual Production for the year</th>
                        <th>Month start</th>
                         <th>Month end</th>
                          <th>Data Sources</th>
                           <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Remarks</th>   
                  </tr>
                </thead>
                <tbody id="emp_body">
               </tbody>
              </table>       
    <button class="exportToExcel btn-info">Export to XLS</button>
      </div>
 
 </div>
  </div>
 </div>
  </div>
  
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>	
<script src="/static/js/form/provinceMunBarangay.js"></script>	
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/validationForm/hatchery.js" type=" text/javascript"></script>

	</body>

</html>