<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<title>BFAR-FRGIS</title>
<meta charset="utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png" href="/static/images/favicon.png" />

<jsp:include page="../menu/css.jsp"></jsp:include>

</head>
<body>
<jsp:include page="../home/header_region.jsp"></jsp:include>
<jsp:include page="../report/sidenavbar.jsp"></jsp:include>
<%-- <nav id="sidebar">

<div class="sidebar-header">
      <img class=" img img-fluid" alt="" src="/static/images/FRGIS-LOGO.png">
    </div>
<ul class="list-unstyled components mb-5">

      <span class="text">Resources<i class="fas fa-list"></i></span>
      <sec:authorize access="hasRole('ROLE_SUPERADMIN')">
           <li><a class="dropdown-item" href="/admin/fishsanctuaryExcel"><img src="/static/images/iconCircle/fishsanctuary.png" style="width: 20px; height: 20px;"/>  Fish Sanctuary</a></li>
          <li><a class="dropdown-item" href="/admin/fishprocessingExcel"><img src="/static/images/iconCircle/fishprocessing.png" style="width: 20px; height: 20px;"/> Fish Processing</a></li>
          <li><a class="dropdown-item" href="/admin/fishlandingExcel"><img src="/static/images/iconCircle/fish-landing.png" style="width: 20px; height: 20px;"/> Fish Landing</a></li>
          <li><a class="dropdown-item" href="/admin/fishportExcel"><img src="/static/images/iconCircle/fishport.png" style="width: 20px; height: 20px;"/> Fish Port</a></li>
          <li><a class="dropdown-item" href="/admin/fishpenExcel"><img src="/static/images/iconCircle/fishpen.png" style="width: 20px; height: 20px;"/>Fish Pen</a></li>
          <li><a class="dropdown-item" href="/admin/fishcageExcel"><img src="/static/images/iconCircle/fishcage.png" style="width: 20px; height: 20px;"/> Fish Cage</a></li>
          <li><a class="dropdown-item" href="/admin/hatcheryExcel"><img src="/static/images/iconCircle/hatcheries.png" style="width: 20px; height: 20px;"/>Hatchery</a></li>
          <li><a class="dropdown-item" href="/admin/ipcsExcel"><img src="/static/images/iconCircle/iceplant.png" style="width: 20px; height: 20px;"/>Cold Storage</a></li>
          <li><a class="dropdown-item" href="/admin/marketExcel"><img src="/static/images/iconCircle/market.png" style="width: 20px; height: 20px;"/>Market</a></li>
          <li><a class="dropdown-item" href="/admin/schooloffisheriesExcel"><img src="/static/images/iconCircle/schoolof-fisheries.png" style="width: 20px; height: 20px;"/>School of Fisheries</a></li>
          <li><a class="dropdown-item" href="/admin/fishcoralExcel"><img src="/static/images/iconCircle/fishcorral.png" style="width: 20px; height: 20px;"/>Fish Corral(Baklad)</a></li>
          <li><a class="dropdown-item" href="/admin/seagrassExcel"><img src="/static/images/iconCircle/seagrass.png" style="width: 20px; height: 20px;"/>Seagrass</a></li>
          <li><a class="dropdown-item" href="/admin/seaweedsExcel"><img src="/static/images/iconCircle/seaweeds.png" style="width: 20px; height: 20px;"/>Seaweeds</a></li>
          <li><a class="dropdown-item" href="/admin/mangroveExcel"><img src="/static/images/iconCircle/mangrove.png" style="width: 20px; height: 20px;"/>Mangrove</a></li>
          <li><a class="dropdown-item" href="/admin/mariculturezoneExcel"> <img src="/static/images/iconCircle/mariculturezone.png" style="width: 20px; height: 20px;"/>Mariculture Zone</a></li>
          <li><a class="dropdown-item" href="/admin/lguExcel"><img src="/static/images/iconCircle/lgu.png" style="width: 20px; height: 20px;"/>PFO</a></li>
          <li><a class="dropdown-item" href="/admin/trainingcenterExcel"><img src="/static/images/iconCircle/fisheriestraining.png" style="width: 20px; height: 20px;"/>Training Center</a></li>
	
      <li><a href="${pageContext.request.contextPath}/admin/bar/all">Chart Report</a></li>
 </sec:authorize>
       <sec:authorize access="hasRole('ROLE_USER') or hasRole('ROLE_REGIONALADMIN')">
           <li><a class="dropdown-item" href="/excel/fishsanctuaryExcel"><img src="/static/images/iconCircle/fishsanctuary.png" style="width: 20px; height: 20px;"/>  Fish Sanctuary</a></li>
          <li><a class="dropdown-item" href="/excel/fishprocessingExcel"><img src="/static/images/iconCircle/fishprocessing.png" style="width: 20px; height: 20px;"/> Fish Processing</a></li>
          <li><a class="dropdown-item" href="/excel/fishlandingExcel"><img src="/static/images/iconCircle/fish-landing.png" style="width: 20px; height: 20px;"/> Fish Landing</a></li>
          <li><a class="dropdown-item" href="/excel/fishportExcel"><img src="/static/images/iconCircle/fishport.png" style="width: 20px; height: 20px;"/> Fish Port</a></li>
          <li><a class="dropdown-item" href="/excel/fishpenExcel"><img src="/static/images/iconCircle/fishpen.png" style="width: 20px; height: 20px;"/>Fish Pen</a></li>
          <li><a class="dropdown-item" href="/excel/fishcageExcel"><img src="/static/images/iconCircle/fishcage.png" style="width: 20px; height: 20px;"/> Fish Cage</a></li>
          <li><a class="dropdown-item" href="/excel/hatcheryExcel"><img src="/static/images/iconCircle/hatcheries.png" style="width: 20px; height: 20px;"/>Hatchery</a></li>
          <li><a class="dropdown-item" href="/excel/ipcsExcel"><img src="/static/images/iconCircle/iceplant.png" style="width: 20px; height: 20px;"/>Cold Storage</a></li>
          <li><a class="dropdown-item" href="/excel/marketExcel"><img src="/static/images/iconCircle/market.png" style="width: 20px; height: 20px;"/>Market</a></li>
          <li><a class="dropdown-item" href="/excel/schooloffisheriesExcel"><img src="/static/images/iconCircle/schoolof-fisheries.png" style="width: 20px; height: 20px;"/>School of Fisheries</a></li>
          <li><a class="dropdown-item" href="/excel/fishcoralExcel"><img src="/static/images/iconCircle/fishcorral.png" style="width: 20px; height: 20px;"/>Fish Corral(Baklad)</a></li>
          <li><a class="dropdown-item" href="/excel/seagrassExcel"><img src="/static/images/iconCircle/seagrass.png" style="width: 20px; height: 20px;"/>Seagrass</a></li>
          <li><a class="dropdown-item" href="/excel/seaweedsExcel"><img src="/static/images/iconCircle/seaweeds.png" style="width: 20px; height: 20px;"/>Seaweeds</a></li>
          <li><a class="dropdown-item" href="/excel/mangroveExcel"><img src="/static/images/iconCircle/mangrove.png" style="width: 20px; height: 20px;"/>Mangrove</a></li>
          <li><a class="dropdown-item" href="/excel/mariculturezoneExcel"> <img src="/static/images/iconCircle/mariculturezone.png" style="width: 20px; height: 20px;"/>Mariculture Zone</a></li>
          <li><a class="dropdown-item" href="/excel/lguExcel"><img src="/static/images/iconCircle/lgu.png" style="width: 20px; height: 20px;"/>PFO</a></li>
          <li><a class="dropdown-item" href="/excel/trainingcenterExcel"><img src="/static/images/iconCircle/fisheriestraining.png" style="width: 20px; height: 20px;"/>Training Center</a></li>
	
      <li><a href="/create/bar">Chart Report</a></li>
 </sec:authorize>

      <sec:authorize access="isAuthenticated()">
       <li><a href="/logout"><i class="fas fa-sign-out-alt"></i>LOGOUT</a></li>
      </sec:authorize>

</ul>
</nav> --%>
</body>
</html>