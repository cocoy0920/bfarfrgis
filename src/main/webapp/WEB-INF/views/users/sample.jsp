
     <!--  <div class="container-fluid"> -->
    <!-- <div class="col-md-12 panel panel-default" style="box-shadow: 0 3px 3px 1px #000000;" id='section-to-print'>
 -->
				<form class="needs-validation" novalidate>
	 			<input type="number" name="id" id="id" class="id" hidden="true"/>
				<input type="hidden" name="user" id="user" class="user"/>
				<input type="hidden" name="uniqueKey" id="uniqueKey" class="uniqueKey"/>
				<input type="hidden" name="dateEncoded" id="dateEncoded" class="dateEncoded"/>
				<input type="hidden" name="dateEdited" id="dateEdited" class="dateEdited" />
				<input type="hidden" name="encodedBy" id="encodedBy" class="encodedBy"/>
				<input type="hidden" name="editedBy" id="editedBy" class="editedBy" />
				<input type="hidden" name="region" id="region" class="region"/>
				<input type="hidden"  name="page" id="page" class="page"/>			
				<br>


			<div class="form-row">
    			<div class="col-md-4 mb-3">
					<label for="validationCustom01">Province</label>
					<select name="province"  id="validationCustom01" class="form-control province" required></select>															
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid province.
      				</div>
					</div>


    			<div class="col-md-4 mb-3">	
				<label for="validationCustom02">Municipality/City </label> 		
				  <select name="municipality" id="validationCustom02"  class="form-control municipality" required></select>	
				 <div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Municipality/City 
      				</div>
					</div>

    			<div class="col-md-4 mb-3">															
				<label for="validationCustom03">Barangay</label>
					<select name="barangay"  id="validationCustom03"  class="form-control barangay" required>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Barangay
      				</div>
					</div>
			</div>
			<div class="form-row">
    			<div class="col-md-12 mb-3">			
					<label for="code">Code:</label>
						<input type="text" name="code" id="code"  class="code" placeholder="Code" readonly="readonly"/>
				</div>
			</div>							

			
<div class="form-row">
    			<div class="col-md-6 mb-3">	
			 <label for="validationCustom04">Name of Sanctuary/Protected Area</label>
			 <input type="text" name="nameOfFishSanctuary"  id="validationCustom04"  class="form-control nameOfFishSanctuary" required/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please input a valid Name of Sanctuary/Protected Area
      				</div>
					</div>
					

    			<div class="col-md-6 mb-3">			
				<label for="validationCustom05">Implementer</label>				
				<select name="bfardenr"  id="validationCustom05"  class="form-control bfardenr" required>
										<option value=""></option>
										<option value="BFAR">BFAR</option>
										<option value="DENR">DENR</option>
				</select>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please input Implementer
      				</div>
					</div>

			
			<div id="bfarData" style="display:none;">
				<label for="sheltered"><span>Name of Sheltered Fisheries/Species:</span>
			 	<input type="text" name="sheltered"  id="sheltered"  class="sheltered input-field "/>
					
			</label>	
			</div>
		
		<div id="denrData" style="display:none;">
			<label for="nameOfSensitiveHabitatMPA"><span>Name of Sensitive Habitat with MPA:</span>		
			<input type="text" name="nameOfSensitiveHabitatMPA"  id="nameOfSensitiveHabitatMPA"  class="nameOfSensitiveHabitatMPA input-field "/>
			</label>
			<label for="OrdinanceNo"><span>Ordinance No.</span>					
			<input type="text" name="OrdinanceNo" id="OrdinanceNo" class="OrdinanceNo input-field "/>
			</label>
			<label for="OrdinanceTitle"><span>Ordinance Title</span>
			<input type="text" name="OrdinanceTitle" id="OrdinanceTitle"  class="OrdinanceTitle input-field "/>
			</label>
			<label for="DateEstablished"><span>Date Established</span>
			<input type="text" name="DateEstablished" id="DateEstablished" class="DateEstablished input-field "/>
			</label>
			
		</div>
		

    	<div class="col-md-6 mb-3">	
			<label for="validationCustom06">Area (sqm.)</label>
				<input type="number" name="area"  id="validationCustom06"  class="form-control area" required/>																
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please input valid Area
      				</div>
					</div>


    	<div class="col-md-6 mb-3">	
		<label for="validationCustom07">Type</label>	
				<select name="type"  id="validationCustom07"  class="form-control type" required>
										<option value="">Select</option>
										<option value="National">National</option>
										<option value="Local">Local</option>
				</select>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select Type.
      				</div>
					</div>

				</div>

			<div class="form-row">

    		<div class="col-md-6 mb-3">	
				<label for="validationCustom08">Date as of</label>
					<input type="date" name="dateAsOf" id="validationCustom08" class="form-control dateAsOf" required>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select Date
      				</div>
					</div>

		

    		<div class="col-md-6 mb-3">	
		<label for="validationCustom09">Data Sources</label>
					<input type="text" name="fishSanctuarySource"  id="validationCustom09"  class="form-control fishSanctuarySource" required/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please input Data Source
      				</div>
					</div>

		</div>
<div class="form-row">
    		<div class="col-md-12 mb-3">					
			<label for="validationCustom10">Remarks</label>
					<textarea name="remarks" id="validationCustom10"  class="form-control remarks" required></textarea>
		<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please input Remarks
      				</div>
					</div>
</div>
		

			<div class="form-row">

    		<div class="col-md-6 mb-3">	
			<label for="validationCustom11">Latitude</label>			
				<input type="text" name="lat"   id="validationCustom11" class="form-control lat" required/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please input valid Latitude
      				</div>
					</div>
	

    		<div class="col-md-6 mb-3">	
			<label for="validationCustom12">Longitude</label>
			<input type="text" name="lon"  id="validationCustom12" class="form-control lon" required/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please input valid Longitude
      				</div>
					</div>

			</div>	
<div class="form-row">
    		<div class="col-md-12 mb-3">				
			  <label for="validationCustom13">Captured Image</label>		
					<input type="file" id="validationCustom13" class="form-control file" accept="image/*" required/>
    			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select Image
      				</div>
					</div>
</div>

     			
     			
     				<div id="img_preview" style="display: none">
     					 	<img id="preview" name="preview" class="preview" style="width: 450px; height: 200px"></img>
     				</div>
     				<input type="hidden" name="image_src" id="image_src" class="image_src" value="" /><br />
							<input type="hidden" name="image"  id="image" class="image"/>	
				

		    <button type="submit" class="btn btn-primary">SUBMIT</button>
<!-- <button type="button" class="btn btn-primary">SUBMIT</button> -->
		<%--   <input type="submit" class="btn btn-primary" value="SUBMIT"/><br><br> 
       </form:form> --%>
</form>

		  <div id="pdf" style="display: none">
		  <!-- <div id="Progress_Status"> 
			<div id="myprogressBar"></div> 
			</div>
			 -->
        				<button onclick="generatepdfFISHSANCTUARY()">Generate PDF</button>     	
    			</div>
<br><br>
  <div id="table">				
<table id="pdfFISHSANCTUARY">
  <thead>
    <tr>
      <th scope="col">FISH SANCTUARY </th>
      <th scope="col">&nbsp;</th>
    </tr>
  </thead>
  <tbody  id="tbodyID">
  </tbody>
</table> 

</div> 

<!-- 			</div>
			 -->
			
			
			
<!-- 		          </div>	
			 -->
<br><br>
<div class="container"> 
 <div class="row">
       <div class="col-md-12 table-responsive">     
			<table id="export-buttons-table" class="table table-hover">
                <thead>
                  <tr>
        
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of FishSanctuary</th>
                    <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody> 
               </tbody>
              </table>
              <div id="paging">
             <!--${page}  --> 
             </div>
             
 <table class="table2excel" style="display: none">
                <thead>
                <tr><th align="center" colspan="17">FISH SANCTUARY</th></tr>
                
                <tr>
                    <th>${region}</th>
                 </tr>  
                 
                  <tr>
                   <th>Province</th>
                   <th>Municipality</th>
                   <th>Barangay</th>
                   <th>Name of FishSanctuary</th>
                   <th>Code</th>
                   <th>Area</th>
                   <th>Type</th>
                   <th>Data Source</th>
                   <th>BFAR/DENR</th>
                   <th>Sheltered</th>
                   <th>Name Of Sensitive Habitat MPA</th>
                   <th>Ordinance No.</th>
                   <th>Ordinance Title</th>
                   <th>Date Established</th>
                   <th>Latitude</th>
                   <th>Longitude</th>
                   <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="emp_body">
                
               </tbody>
              </table> 
      <button class="exportToExcel">Export to XLS</button>
      </div>
 
 </div>
  </div>  

    