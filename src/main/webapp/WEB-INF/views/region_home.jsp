<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>BFAR-FRGIS</title>
<meta charset="utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png" href="/static/images/favicon.png" />

<jsp:include page="menu/css.jsp"></jsp:include>
<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
		<script src="/static/leaflet/spin.min.js"></script>
	<script src="/static/leaflet/leaflet.spin.min.js"></script>
	<script src="//d3js.org/d3.v3.min.js"></script>
	<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
  <script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
 <link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
    <script src="/static/leaflet/L.Control.SlideMenu.js"></script>
    <script  src="/static/leaflet/leaflet-bootstrapmodal.js" type=" text/javascript"></script>
 <link rel="stylesheet" href="/static/leaflet/MarkerCluster.css" />
	<link rel="stylesheet" href="/static/leaflet/MarkerCluster.Default.css" />
	<script src="/static/leaflet/leaflet.markercluster-src.js"></script> 	 
   

	<!-- <script src="/static/leaflet/leaflet-panel-layers.js"></script> -->
	<style type="text/css">
/* 	.hiddenRow { */
/*     padding: 0 !important; */
/* } */
/* .collapsible { */
/*   background-color: #777; */
/*   color: white; */
/*   cursor: pointer; */
/*   padding: 18px; */
/*   width: 100%; */
/*   border: none; */
/*   text-align: left; */
/*   outline: none; */
/*   font-size: 15px; */
/* } */

/* .active, .collapsible:hover { */
/*   background-color: #555; */
/* } */

/* .content_collapse { */
/*   padding: 0 18px; */
/*   display: none; */
/*   overflow: hidden; */
/*   background-color: #f1f1f1;	 */
	
/* }	 */
	.my-div-icon{
	background: white url(../images/calendar_2.png) right no-repeat;
	}
	.countryLabel{
  background: rgba(255, 255, 255, 0);
  border:0;
  border-radius:0px;
  box-shadow: 0 0px 0px;
  
}
.my-div-icon {
				width: 300px;
				height:25px;
				background: white;
				border: 3px black;
				 
			}
.leaflet-div-icon {
   border: 1px solid #000;
  border-radius: 0px; 
  padding-bottom: 25px;
  text-align: center;
    background: transparent;
  border: 0px transparent; 
  display:inline-block;
  color: #000;
white-space: nowrap;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
pointer-events: none;
}
.leaflet-tooltip-own {
position: absolute;
/* background-color: rgba(0, 0, 0, 0.5); */
border: 0px solid #000;
border-radius: 0px;
color: #000;
white-space: nowrap;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
pointer-events: none;
/* box-shadow: 0 1px 3px rgba(0,0,0,0.4); */
}
/* .leaflet-tooltip {
    position: absolute;
    padding: 6px;
    background-color: #000;
    border: 1px solid #fff;
    border-radius: 3px;
    color: #222;
    white-space: nowrap;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    pointer-events: none;
    box-shadow: 0 1px 3px rgba(0,0,0,0.4);
} */

/*       .content {
        margin: 0.25rem;
        border-top: 1px solid #000;
        padding-top: 0.5rem;
        background: #002e7c; 
      } */

      .header {
        font-size: 1.8rem;
        color: #7f7f7f;
      }

      .title {
        font-size: 1.1rem;
        color: #7f7f7f;
        font-weight: bold;
      }

      .bottom {
        margin-top: 64px;
        font-size: 0.8rem;
        color: #7f7f7f;
      }

.table_body {
    width: 100%;
    display:block;
}
.thead_body {
    display: inline-block;
    width: 100%;
    height: 20px;
}
.tbody_body {
    height: 200px;
    display: inline-block;
    width: 100%;
    overflow: auto;
}

.fixed_header{
    width: 100%;
   display:block;
}

.fixed_header tbody{
  height: 200px;
    display: inline-block;
    width: 100%;
  overflow: auto;
}

/* .fixed_header thead {
  background: black;
  color:#fff;
}
 */
.fixed_header th, .fixed_header td {

  text-align: left;
  width: 200px;
}
.modal {
/*    position: absolute; */
    left: 400px;
/*    bottom: 0; */
/*    left: 0; */
/*    z-index: 10040; */
/*    overflow: auto; */
/*    overflow-y: auto; */
} 
</style>
</head>
<body>
 <jsp:include page="users/header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">

	   <div id="content">	
              	<div id="map" class="section-to-print"></div>

 </div>

   <!--  <button class="manualButton" onclick="manualPrint()">Manual print</button> -->
</div>


<%-- 	<div id="footer">
		<jsp:include page="users/footer.jsp"></jsp:include>
	</div> --%>
<!-- a Bootstrap modal, which we'll bind to the control via the modalId parameter -->
<jsp:include page="menu/fma_details.jsp"></jsp:include>
<jsp:include page="fma/fma2.jsp"></jsp:include>
<jsp:include page="fma/fma1.jsp"></jsp:include>
<jsp:include page="fma/fma3.jsp"></jsp:include>
<jsp:include page="fma/fma4.jsp"></jsp:include>
<jsp:include page="fma/fma5.jsp"></jsp:include>
<jsp:include page="fma/fma6.jsp"></jsp:include>
<jsp:include page="fma/fma7.jsp"></jsp:include>
<jsp:include page="fma/fma8.jsp"></jsp:include>
<jsp:include page="fma/fma9.jsp"></jsp:include>
<jsp:include page="fma/fma10.jsp"></jsp:include>
<jsp:include page="fma/fma11.jsp"></jsp:include>
<jsp:include page="fma/fma12.jsp"></jsp:include>
<!-- a Bootstrap modal, which we'll bind to the control via the modalId parameter -->
 <div class="modal" role="dialog" id="modal_help">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Help</h4>
            </div>
            <div class="modal-body">

                <p><a>sample</a>.</p>

            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
	<script src="/static/leaflet/bundle.js"></script>
	<script src="/static/js/api/regionMap.js"></script>
	<script  src="/static/js/api/FMA.js" ></script>	
	<script  src="/static/js/resource.js" ></script>
	<script src="/static/js/modal/users_menu.js"></script>
	<script src="/static/js/map/checkResourcesByUser.js"></script>
	<script src="/static/js/map/philippineMap.js"></script>
	<script  src="/static/js/resourcesOnMapByUser.js" ></script>	 
	<script type="text/javascript">
	var regionID = '${regionID}';
	var region = '${regionName}';
	getRegionName(region);
	</script>
<!-- <script  src="/static/js/api/sample.js" ></script> -->
	
    </body>
</html>