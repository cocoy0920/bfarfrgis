
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>BFAR-FRGIS</title>
<meta charset="utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png" href="/static/images/favicon.png" />

<jsp:include page="menu/css.jsp"></jsp:include>
<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
		<script src="/static/leaflet/spin.min.js"></script>
	<script src="/static/leaflet/leaflet.spin.min.js"></script>
	<script src="//d3js.org/d3.v3.min.js"></script>
	<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
  <script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
 <link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
    <script src="/static/leaflet/L.Control.SlideMenu.js"></script>
    <script  src="/static/leaflet/leaflet-bootstrapmodal.js" type=" text/javascript"></script>
 <link rel="stylesheet" href="/static/leaflet/MarkerCluster.css" />
	<link rel="stylesheet" href="/static/leaflet/MarkerCluster.Default.css" />
	<script src="/static/leaflet/leaflet.markercluster-src.js"></script> 	
	 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
   <!--  <link rel="stylesheet" href="/static/leaflet-sidebar.css" /> -->

	<!-- <script src="/static/leaflet/leaflet-panel-layers.js"></script> -->
</head>
<body>
 <jsp:include page="users/header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">
<%-- <jsp:include page="menu/lsidebar.jsp"></jsp:include> --%>
	   <div id="content">
	   
              	<div id="map" class="section-to-print">
              		<!-- <div class="leaflet-control leaflet-bottom leaflet-right col-sm-2 col-sm-offset-1">
              			<img alt="" src="/static/images/FRGIS-LOGO.png" style="height: 50px; width: 50px">
              		</div>  -->
              		<!-- <div class="leaflet-control laeflet-bottom leaflet-right" style="background-color: green;">
              			<p  id="legendTitle">Legend Title</p>
              			<div id="legend"></div>
              		</div> -->

              	</div>

 </div>

   <!--  <button class="manualButton" onclick="manualPrint()">Manual print</button> -->
</div>


<div id="footer">
		<jsp:include page="users/footer.jsp"></jsp:include>
	</div> 
<!-- a Bootstrap modal, which we'll bind to the control via the modalId parameter -->
<jsp:include page="menu/fma_details.jsp"></jsp:include>
<jsp:include page="fma/fma2.jsp"></jsp:include>
<jsp:include page="fma/fma1.jsp"></jsp:include>
<jsp:include page="fma/fma3.jsp"></jsp:include>
<jsp:include page="fma/fma4.jsp"></jsp:include>
<jsp:include page="fma/fma5.jsp"></jsp:include>
<jsp:include page="fma/fma6.jsp"></jsp:include>
<jsp:include page="fma/fma7.jsp"></jsp:include>
<jsp:include page="fma/fma8.jsp"></jsp:include>
<jsp:include page="fma/fma9.jsp"></jsp:include>
<jsp:include page="fma/fma10.jsp"></jsp:include>
<jsp:include page="fma/fma11.jsp"></jsp:include>
<jsp:include page="fma/fma12.jsp"></jsp:include>
<!-- a Bootstrap modal, which we'll bind to the control via the modalId parameter -->
 <div class="modal" role="dialog" id="modal_help">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Help</h4>
            </div>
            <div class="modal-body">

                <p><a>sample</a>.</p>

            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
	<script src="/static/leaflet/bundle.js"></script>	
	<script src="/static/js/leaflet-sidebar.js"></script> 
	<script src="/static/js/modal/home_menu.js"></script>
	<script src="/static/js/map/philippineMap.js"></script>
	<script src="/static/js/api/majorFishingGround.js"></script>
	<script  src="/static/js/api/FMA.js" ></script>
	<script  src="/static/js/resource.js"></script>
	<script src="/static/js/map/checkResources.js"></script>
	<script  src="/static/js/resourcesOnMap.js"></script>	 
	
<!-- <script  src="/static/js/api/sample.js" ></script> -->
	
    </body>
</html>