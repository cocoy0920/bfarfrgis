<div class="modal" role="dialog" id="modal_about">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fisheries Management Areas</h4>
            </div>
            <div class="modal-body">

               		<table class="table" style="width:100%">
					  	<thead>
					  	<tr>
					  							  
					  		<th>FMA ID</th>
					  		<th>SQUARE KILOMETERS</th>
					  		<th>HECTARES</th>
					  		<th>LEAD REGION</th>
					  		<th>NO. of LGUs</th>
					  		<th>MAJOR COMMERCIAL SPECIES</th>
					  	</tr>
					  </thead>

					  <tbody>
					  <tr data-toggle="collapse" data-target="#FMA01" class="accordion-toggle">
					  	
					  	<td><button class="btn btn-default btn-xs">FMA-01</button></td>
					  	<td>505,345</td>
					  	<td>50,343,500</td>
					  	<td>Region 2</td>
					  	<td>103</td>
					  	<td>Tuna<br>Mackerel<br>Scads<br></td>
					  </tr>
			
					  <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA01"> 
					  			<table class="table table-striped">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 2</td>
										<td>Batanes,<br>Cagayan<br> Isabela</td>
										<td>1. Babuyan Channel<br>
					  						2. Isabela Waters<br>
					  						3. Batanes Waters<br>
					  						4. Philippine Rise<br>
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 3</td>
										<td>Aurora</td>
										<td>1. Baler Bay<br>
					  						2. Casiguran Sound<br>
					  						3. Dingalan Bay<br>
					  						4. Philippine Rise<br>
					  					</td>					
									</tr>	
									<tr>
										<td>4A (CALABARZON)</td>
										<td>Quezon</td>
										<td>1. Lamon Bay<br>
					  						2. Philippine Rise<br>
					  						3. Polilio Strait<br>
					  						4. Burdeos Bay<br>
					  						5. Calauag Bay<br>
					  						6. Lopez Bay<br>
					  						7. Basian Bay<br>
					  						8. Anibawan Bay
					  					</td>					
									</tr>
									<tr>
										<td>5</td>
										<td>Albay,<br> 
					  						Camarines Norte,<br> 
					  						Camarines Sur,<br> 
					  						Catanduanes,<br> 
					  						Sorsogon (Incl Norther Samar)</td>
										<td>1. Albay Gulf<br>
					  						2. Lagonoy Gulf<br>
					  						3. Lamon Bay<br>
					  						4. San Miguel Bay<br>
					  						5. Philippine Rise<br>
					  						6. San Bernardino Strait<br>
					  						7. Maqueda Channel
					  					</td>					
									</tr>						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
        </tr>
		 <tr data-toggle="collapse" data-target="#FMA02" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-02</button></td>
				    <td>300,098</td>
				    <td>30,009,800</td>
				    <td>Region 11</td>
				    <td>83</td>
					<td>Tuna<br>Mackerel<br>Scads<br></td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA02"> 
					  			<table class="table table-striped">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 8</td>
										<td>Eastern Samar,<br>Northern Samar</td>
										<td>1. Oras Bay<br>
											2. Philippine Sea<br>
											3. Matarinao BaySea<br>
											4. Bantayan BaySea<br>
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 11</td>
										<td>Davao De Oro,<br>
											Davao del Norte,<br>
											Davao del Sur,<br>
											Davao Occidental<br>
											Davao Oriental<br>
											</td>
										<td>
										1. Davao Gulf<br>
										2. Mayo Bay<br>
										3. Pujada Bay<br>
										4. Caraga Bay<br>
										5. Baculin Bay<br>
										6. Cateel Bay<br>
										7. Baganga Bay<br>
										8. Sarangani Strait<br>
										9. Manay Bay<br>
										10. Philippine Sea (Pacific)<br>
										11. Boston Bay<br>
										</td>
									</tr>	
									<tr>
										<td>13</td>
										<td>Sarangani (Region 12),<br>
											Surigao Del Norte,<br>
											Surigao del Sur
										</td>
										<td>
											1. Celebes Sea<br>
											2. East Siargao Waters<br>
											3. Hinatuan Bay<br>
											4. Surigao Sea<br>
											5. Bislig Bay<br>
											6. Lianga Bay<br>
					  					</td>					
									</tr>
						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>	
				 <tr data-toggle="collapse" data-target="#FMA03" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-03</button></td>
				    <td>166,659</td>
				    <td>16,665,900</td>
				    <td>Region 12</td>
				    <td>64</td>
					<td>Tuna<td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA03"> 
					  			<table class="table table-striped">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 9</td>
										<td>
										Zamboanga Sibugay,<br>
										Zamboanga del Sur
										</td>
										<td>
											1. Moro Gulf<br>
											2. Iliana Bay<br>
											3. Dumanquillas Bay<br>

					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 12</td>
										<td>
											Sarangani,<br>
											South Cotabato,<br>
											Sultan Kudarat,<br>
											(incl. Davao Del Sur)										
										</td>
										<td>
											1. Sarangani Bay<br>
											2. Moro Gulf<br>
											3. Celebes Sea
					  					</td>					
									</tr>	
									<tr>
										<td>BARMM</td>
										<td>
											Basilan, <br>
											Lanao del Sur,<br>
											Maguindanao, <br>
											Sulu, <br>
											Tawi-Tawi
										</td>
										<td>&nbsp;
					  					</td>					
									</tr>
						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>	
				  		 <tr data-toggle="collapse" data-target="#FMA04" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-04</button></td>
				   <td>152,076</td>
				    <td>15,207,600</td>
				    <td>Region 9</td>
				    <td>86</td>
					<td>Sardines, <br>Scads</td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA04"> 
					  			<table class="table table-striped">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 6</td>
										<td>
											Antique,<br>
											Guimaras,<br> 
											Iloilo,<br>
											Negros Occidental										
										</td>
										<td>1. Panay Gulf(East Sulu Sea)
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 7</td>
										<td>Negros Oriental </td>
										<td>1.East Sulu Sea<br>
												(Negros Oriental)
					  					</td>					
									</tr>	
									<tr>
										<td>9</td>
										<td>
											Zamboanga del Norte,<br>
											Zamboanga Del Sur,<br>
											Zamboanga Sibugay<br>
											HUC: Zamboanga City<br> 
											and Isabela City
										</td>
										<td>
											1. Sibugay Bay<br>
											2. Basilan Strait<br>
											3. Dipolog Bay<br>
											4. Sindangan Bay<br>
											5. Tawi-Tawi Bay<br>
											6. Coronado Bay<br>
											7. Dapitan Bay<br>
											8. Sibuco Bay<br>
											9. Siocon Bay<br>
											10. Northern Sulu Sea<br>
											11.South East Sulu

					  					</td>					
									</tr>
									<tr>
										<td>BARMM</td>
										<td>
											Basilan,<br>
											Sulu,<br>
											Tawi Tawi<br>
										</td>
										<td>
											1. Sulu Sea<br>
											2. Basilan Strait<br>
											3. Tawi-Tawi Bay<br>
					  					</td>					
									</tr>						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>	
				 <tr data-toggle="collapse" data-target="#FMA05" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-05</button></td>
				     <td>485,417</td>
				    <td>48,541,700</td>
				    <td>MIMAROPA</td>
				    <td>50</td>
					<td>
						Roundscads<br>
						Mackerel<br>
						Tuna
					</td>
				</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA05"> 
					  			<table class="table table-striped">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>4B(MIMAROPA)</td>
										<td>Palawan,<br>Occidental Mindoro</td>
										<td>
											1. Bacuit Bay<br>
											2. Sulu Sea-Brooke’s Point<br>
											3. Coron Bay<br>
											4. Green Island Bay<br>
											5. Honda Bay<br>
											6. Imuran Bay<br>
											7. Malanut Bay<br>
											8. Pagdanan Bay<br>
											9. Sulu Sea-Narra<br>
											10. Taytay Bay<br>
											11. Ulugan Bay<br>
											12. Mindoro Strait<br>
											13. Balabac Strait<br>
											14. Malampaya Sound<br>
											15. San Antonio Bay<br>
											17. Sulu Sea — PPC<br>
											18. Sulu Sea — Aborlan<br>
											19. West Philippine Sea —Rizal<br>
											20. Dumaran Channel<br>
											21. Cuyo Pass<br>
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 6</td>
										<td>Aklan,<br> Antique</td>
										<td>1. Cuyo East Pass
					  					</td>					
									</tr>	
									<tr>
										<td>BARMM</td>
										<td>Tawi-Tawi</td>
										<td>1. Sulu Sea
					  					</td>					
									</tr>
						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>	
				 <tr data-toggle="collapse" data-target="#FMA06" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-06</button></td>
				    <td>293,930</td>
				    <td>29,393,000</td>
				    <td>Region 3</td>
				    <td>120</td>
					<td>
						YF Tuna<br>
						Skipjack<br>
						Small-pelagics<br>
					<td>
				</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA06"> 
					  			<table class="table table-striped">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
								 <tr>
								 	<td>Region 1</td>
								 	<td>
										Ilocos Norte,<br>
										Ilocos Sur,<br>
										La Union,<br>
										Pangasinan								 	
								 	</td>
								 	<td>
										1. Pasaleng Bay<br>
										2. Bangui Bay<br>
										3. Ilocos Coast / NWPS<br>
										4. Lingayen Gulf<br>
										5. Zambales Coast<br>
										6. Scarborough<br>							 	
								 	</td>
								 </tr>
									<tr>
										<td>Region 3</td>
										<td>
											Bataan,<br>
											Bulacan,<br>
											Pampanga, <br>
											Zambales
										</td>
										<td>
											1. Manila Bay<br>
											2. Subic Bay<br>
											3. Zambales Coast
					  					</td>
					  										
									</tr>
									<tr>
										<td>4A (CALABARZON)</td>
										<td>Batangas,<br>Cavite</td>
										<td>1. Nasugbu Bay</td>
									</tr>
									<tr>
										<td>4B (MIMAROPA)</td>
										<td>Occidental Mindoro</td>
										<td>1. West Philippine Sea</td>
									</tr>
									<tr>
										<td>CAR</td>
										<td>Abra</td>
										<td></td>
									</tr>
									<tr>
										<td>NCR</td>
										<td></td>
										<td>1. Manila Bay</td>
									</tr>
									
						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>		
		 <tr data-toggle="collapse" data-target="#FMA07" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-07</button></td>
				    <td>16,699</td>
				    <td>1,669,900</td>
				    <td>Region 5</td>
				    <td>85</td>
					<td>Small-pelagics (i.e. sardines)</td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA07"> 
					  			<table class="table table-striped">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 4A</td>
										<td>Quezon</td>
										<td>1. Ragay Gulf<br>
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 5</td>
										<td>
											Albay,<br>
											Camarines Sur,<br>
											Masbate,<br>
											Sorsogon
										</td>
										<td>
											1. Ragay Gulf<br>
											2. Sorsogon Bay<br>
											3. Burias Pass<br>
											4. Ticao Pass<br>
					  					</td>					
									</tr>	
									<tr>
										<td>8</td>
										<td>
											Biliran,<br>
											Leyte,<br> 
											Northern Samar,<br> 
											Wester Samar
										</td>
										<td>1. Samar Sea
					  					</td>					
									</tr>
						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>	
			 <tr data-toggle="collapse" data-target="#FMA08" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-08</button></td>
				    <td>14,090</td>
				    <td>1,409,000</td>
				    <td>Region 8</td>
				    <td>51</td>
					<td>
						Anchovies<br>
						Sardines<br>
						Scads<br>
						Tuna	
					<td>
				</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA08"> 
					  			<table class="table table-striped">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 8</td>
										<td>
											Eastern Samar,<br> 
											Leyte,<br>
											Samar, <br>
											Southern Leyte
										<td>
										<td>
											1. Leyte Gulf<br>
											2. San Pedro Bay<br>
											3. Cabalian Bay
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 13</td>
										<td>Dinagat Island</td>
										<td>1. Dinagat Sound
					  					</td>					
									</tr>							              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>	
		 <tr data-toggle="collapse" data-target="#FMA09" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-09</button></td>
				    <td>28,228</td>
				    <td>2,822,800</td>
				    <td>Region 10</td>
				    <td>106</td>
					<td>
						Scads<br>
						Sardines<br>
						Bullet tuna<br>
					<td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA09"> 
					  			<table class="table table-striped">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 7</td>
										<td>Bohol, <br>Siquijor</td>
										<td>1. Bohol Sea
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 8</td>
										<td>Southern Leyte </td>
										<td>1. Sogod Bay
					  					</td>					
									</tr>	
									<tr>
										<td>9</td>
										<td>
											Zamboanga del Norte,<br>
											Zamboanga del Sur
										</td>
										<td>1. Murcielagos Bay
					  					</td>					
									</tr>
									<tr>
										<td>10</td>
										<td>
											Camiguin,<br>
											Lanao del Norte,<br>
											Misamis Occidental,<br>
											Misamis Oriental
										</td>
										<td>
											1. Camiguin Waters<br>
											2. Iligan Bay<br>
											3. Macajalar Bay<br>
											4. Panguil Bay<br>
											5. Gingoog Bay<br>
					  					</td>					
									</tr>
									<tr>
										<td>13</td>
										<td>Agusan del Norte,<br>
											Surigao del Norte
										</td>
										<td>
											1. Butuan Bay<br>
											2. Bohol Sea
										</td>
									</tr>						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>		
		 <tr data-toggle="collapse" data-target="#FMA10" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-10</button></td>
				     <td>15,265</td>
				    <td>1,526,500</td>
				    <td>Region 7</td>
				    <td>83</td>
					<td>Mackerel<br> Scads</td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA10"> 
					  			<table class="table table-striped">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 7</td>
										<td>

											Bohol,<br> 
											Cebu,<br>
											Negros Oriental, <br>
											Siquijor<br>
											HUC: Cebu City
										</td>
										<td>
											1. Cebu Strait<br>
											2. Bohol Strait<br> 
											(also known as Cebu Strait)<br>
											3. Camotes Sea<br>
											4. Danajon Bank<br>
											5. Maribojoc Bay
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 8</td>
										<td>
												Biliran,<br> 
												Leyte,<br>
												Southern Leyte
										</td>
										<td>
											1. Camotes Sea<br>
											2. Biliran Strait<br>
											3. Ormoc Strait
					  					</td>					
									</tr>	
														              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>	
		 <tr data-toggle="collapse" data-target="#FMA11" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-11</button></td>
				    <td>20,386</td>
				    <td>2,038,600</td>
				    <td>Region 6</td>
				    <td>90</td>
					<td>
						Sardines<br>
						Scads<br>
						Bullet tuna<br>
						Blue-swimming crabs
					</td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA11"> 
					  			<table class="table table-striped">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 5</td>
										<td>Masbate</td>
										<td>1. Asid Gulf</td>
					  										
									</tr>
									<tr>
										<td>Region 6</td>
										<td>
											Capiz,<br>
											Guimaras,<br> 
											Iloilo,<br>
											Negros Occidental<br>
											HUC: Bacolod City,<br> 
											Iloilo City
										</td>
										<td>
											1. Visayan Sea<br>
											2. Guimaras Strait
					  					</td>					
									</tr>	
									<tr>
										<td>7</td>
										<td>
											Cebu,<br>
											Negros Oriental
										</td>
										<td>
											1. Bais Bay<br>
											2. Tañon Strait<br>
											3. Visayan Sea
					  					</td>					
									</tr>						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>		
		 <tr data-toggle="collapse" data-target="#FMA12" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-12</button></td>
				    <td>36,674</td>
				    <td>3,667,400</td>
				    <td>Region 4A-CALABARZON</td>
				    <td>85</td>
					<td>
						Sardines<br>
						Scads<br>
						Tuna
					</td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA12"> 
					  			<table class="table table-striped">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>4A (CALABARZON)</td>
										<td>Batangas,<br> Quezon</td>
										<td>
											1. Balayan Bay<br>
											2. Calatagan Bay (Pagapas)<br>
											3. Batangas Bay <br>
											4. Tayabas Bay
					  					</td>
					  										
									</tr>
									<tr>
										<td>4B (MIMAROPA)</td>
										<td>
											Marinduque,<br>
											Occidental Mindoro,<br>
											Oriental Mindoro,<br>
											Romblon
										</td>
										<td>
											1. Tablas Bay<br>
											2. Mogpog Pass<br>
											3. Tayabas Bay
					  					</td>					
									</tr>	
									<tr>
										<td>5</td>
										<td>Masbate</td>
										<td>1. Sibuyan Sea
					  					</td>					
									</tr>
									<tr>
										<td>6</td>
										<td>
											Aklan,<br>Antique,<br> Capiz
					  					</td>
										<td>1. Southern Sibuyan Bay
					  					</td>					
									</tr>						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>				  		  			  		  		  			    	    		  
    </tbody>
</table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->