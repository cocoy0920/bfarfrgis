<div class="modal" role="dialog" id="modal_fm6">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fisheries Management Areas</h4>
            </div>
            <div class="modal-body">

               		<table class="table" style="width:100%">
					  	<thead>
					  	<tr>
					  							  
					  		<th>FMA ID</th>
					  		<th>SQUARE KILOMETERS</th>
					  		<th>HECTARES</th>
					  		<th>LEAD REGION</th>
					  		<th>NO. of LGUs</th>
					  		<th>MAJOR COMMERCIAL SPECIES</th>
					  	</tr>
					  </thead>

					  <tbody>

				 <tr data-toggle="collapse" data-target="#FMA06" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-06</button></td>
				    <td>293,930</td>
				    <td>29,393,000</td>
				    <td>Region 3</td>
				    <td>120</td>
					<td>
						YF Tuna<br>
						Skipjack<br>
						Small-pelagics<br>
					<td>
				</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA06"> 
					  			<table class="table table-striped fixed_header">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
								 <tr>
								 	<td>Region 1</td>
								 	<td>
										Ilocos Norte,<br>
										Ilocos Sur,<br>
										La Union,<br>
										Pangasinan								 	
								 	</td>
								 	<td>
										1. Pasaleng Bay<br>
										2. Bangui Bay<br>
										3. Ilocos Coast / NWPS<br>
										4. Lingayen Gulf<br>
										5. Zambales Coast<br>
										6. Scarborough<br>							 	
								 	</td>
								 </tr>
									<tr>
										<td>Region 3</td>
										<td>
											Bataan,<br>
											Bulacan,<br>
											Pampanga, <br>
											Zambales
										</td>
										<td>
											1. Manila Bay<br>
											2. Subic Bay<br>
											3. Zambales Coast
					  					</td>
					  										
									</tr>
									<tr>
										<td>4A (CALABARZON)</td>
										<td>Batangas,<br>Cavite</td>
										<td>1. Nasugbu Bay</td>
									</tr>
									<tr>
										<td>4B (MIMAROPA)</td>
										<td>Occidental Mindoro</td>
										<td>1. West Philippine Sea</td>
									</tr>
									<tr>
										<td>CAR</td>
										<td>Abra</td>
										<td></td>
									</tr>
									<tr>
										<td>NCR</td>
										<td></td>
										<td>1. Manila Bay</td>
									</tr>
									
						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>		
				  		  			  		  		  			    	    		  
    </tbody>
</table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->