<div class="modal" role="dialog" id="modal_fm8">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fisheries Management Areas</h4>
            </div>
            <div class="modal-body">

               		<table class="table" style="width:100%">
					  	<thead>
					  	<tr>
					  							  
					  		<th>FMA ID</th>
					  		<th>SQUARE KILOMETERS</th>
					  		<th>HECTARES</th>
					  		<th>LEAD REGION</th>
					  		<th>NO. of LGUs</th>
					  		<th>MAJOR COMMERCIAL SPECIES</th>
					  	</tr>
					  </thead>

					  <tbody>

			 <tr data-toggle="collapse" data-target="#FMA08" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-08</button></td>
				    <td>14,090</td>
				    <td>1,409,000</td>
				    <td>Region 8</td>
				    <td>51</td>
					<td>
						Anchovies<br>
						Sardines<br>
						Scads<br>
						Tuna	
					<td>
				</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA08"> 
					  			<table class="table table-striped fixed_header">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 8</td>
										<td>
											Eastern Samar,<br> 
											Leyte,<br>
											Samar, <br>
											Southern Leyte
										<td>
										<td>
											1. Leyte Gulf<br>
											2. San Pedro Bay<br>
											3. Cabalian Bay
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 13</td>
										<td>Dinagat Island</td>
										<td>1. Dinagat Sound
					  					</td>					
									</tr>							              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>	
    </tbody>
</table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->