<div class="modal" role="dialog" id="modal_fm11">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fisheries Management Areas</h4>
            </div>
            <div class="modal-body">

               		<table class="table" style="width:100%">
					  	<thead>
					  	<tr>
					  							  
					  		<th>FMA ID</th>
					  		<th>SQUARE KILOMETERS</th>
					  		<th>HECTARES</th>
					  		<th>LEAD REGION</th>
					  		<th>NO. of LGUs</th>
					  		<th>MAJOR COMMERCIAL SPECIES</th>
					  	</tr>
					  </thead>

					  <tbody>
			 <tr data-toggle="collapse" data-target="#FMA11" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-11</button></td>
				    <td>20,386</td>
				    <td>2,038,600</td>
				    <td>Region 6</td>
				    <td>90</td>
					<td>
						Sardines<br>
						Scads<br>
						Bullet tuna<br>
						Blue-swimming crabs
					</td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA11"> 
					  			<table class="table table-striped fixed_header">
                      			<thead>
                        		<tr>
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 5</td>
										<td>Masbate</td>
										<td>1. Asid Gulf</td>
					  										
									</tr>
									<tr>
										<td>Region 6</td>
										<td>
											Capiz,<br>
											Guimaras,<br> 
											Iloilo,<br>
											Negros Occidental<br>
											HUC: Bacolod City,<br> 
											Iloilo City
										</td>
										<td>
											1. Visayan Sea<br>
											2. Guimaras Strait
					  					</td>					
									</tr>	
									<tr>
										<td>7</td>
										<td>
											Cebu,<br>
											Negros Oriental
										</td>
										<td>
											1. Bais Bay<br>
											2. Tañon Strait<br>
											3. Visayan Sea
					  					</td>					
									</tr>						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>		
    </tbody>
</table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->