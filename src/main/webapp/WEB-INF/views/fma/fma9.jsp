<div class="modal" role="dialog" id="modal_fm9">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fisheries Management Areas</h4>
            </div>
            <div class="modal-body">

               		<table class="table" style="width:100%">
					  	<thead>
					  	<tr>
					  							  
					  		<th>FMA ID</th>
					  		<th>SQUARE KILOMETERS</th>
					  		<th>HECTARES</th>
					  		<th>LEAD REGION</th>
					  		<th>NO. of LGUs</th>
					  		<th>MAJOR COMMERCIAL SPECIES</th>
					  	</tr>
					  </thead>

					  <tbody>

		 <tr data-toggle="collapse" data-target="#FMA09" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-09</button></td>
				    <td>28,228</td>
				    <td>2,822,800</td>
				    <td>Region 10</td>
				    <td>106</td>
					<td>
						Scads<br>
						Sardines<br>
						Bullet tuna<br>
					<td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA09"> 
					  			<table class="table table-striped fixed_header">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 7</td>
										<td>Bohol, <br>Siquijor</td>
										<td>1. Bohol Sea
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 8</td>
										<td>Southern Leyte </td>
										<td>1. Sogod Bay
					  					</td>					
									</tr>	
									<tr>
										<td>9</td>
										<td>
											Zamboanga del Norte,<br>
											Zamboanga del Sur
										</td>
										<td>1. Murcielagos Bay
					  					</td>					
									</tr>
									<tr>
										<td>10</td>
										<td>
											Camiguin,<br>
											Lanao del Norte,<br>
											Misamis Occidental,<br>
											Misamis Oriental
										</td>
										<td>
											1. Camiguin Waters<br>
											2. Iligan Bay<br>
											3. Macajalar Bay<br>
											4. Panguil Bay<br>
											5. Gingoog Bay<br>
					  					</td>					
									</tr>
									<tr>
										<td>13</td>
										<td>Agusan del Norte,<br>
											Surigao del Norte
										</td>
										<td>
											1. Butuan Bay<br>
											2. Bohol Sea
										</td>
									</tr>						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>		
    </tbody>
</table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->