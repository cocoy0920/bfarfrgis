<div class="modal" role="dialog" id="modal_fm4">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fisheries Management Areas</h4>
            </div>
            <div class="modal-body">

               		<table class="table" style="width:100%">
					  	<thead>
					  	<tr>
					  							  
					  		<th>FMA ID</th>
					  		<th>SQUARE KILOMETERS</th>
					  		<th>HECTARES</th>
					  		<th>LEAD REGION</th>
					  		<th>NO. of LGUs</th>
					  		<th>MAJOR COMMERCIAL SPECIES</th>
					  	</tr>
					  </thead>

					  <tbody>
					  		 <tr data-toggle="collapse" data-target="#FMA04" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-04</button></td>
				   <td>152,076</td>
				    <td>15,207,600</td>
				    <td>Region 9</td>
				    <td>86</td>
					<td>Sardines, <br>Scads</td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA04"> 
					  			<table class="table table-striped fixed_header">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 6</td>
										<td>
											Antique,<br>
											Guimaras,<br> 
											Iloilo,<br>
											Negros Occidental										
										</td>
										<td>1. Panay Gulf(East Sulu Sea)
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 7</td>
										<td>Negros Oriental </td>
										<td>1.East Sulu Sea<br>
												(Negros Oriental)
					  					</td>					
									</tr>	
									<tr>
										<td>9</td>
										<td>
											Zamboanga del Norte,<br>
											Zamboanga Del Sur,<br>
											Zamboanga Sibugay<br>
											HUC: Zamboanga City<br> 
											and Isabela City
										</td>
										<td>
											1. Sibugay Bay<br>
											2. Basilan Strait<br>
											3. Dipolog Bay<br>
											4. Sindangan Bay<br>
											5. Tawi-Tawi Bay<br>
											6. Coronado Bay<br>
											7. Dapitan Bay<br>
											8. Sibuco Bay<br>
											9. Siocon Bay<br>
											10. Northern Sulu Sea<br>
											11.South East Sulu

					  					</td>					
									</tr>
									<tr>
										<td>BARMM</td>
										<td>
											Basilan,<br>
											Sulu,<br>
											Tawi Tawi<br>
										</td>
										<td>
											1. Sulu Sea<br>
											2. Basilan Strait<br>
											3. Tawi-Tawi Bay<br>
					  					</td>					
									</tr>						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>	
			  			  		  			  		  		  			    	    		  
    </tbody>
</table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->