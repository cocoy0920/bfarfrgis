<div class="modal" role="dialog" id="modal_fm10">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fisheries Management Areas</h4>
            </div>
            <div class="modal-body">

               		<table class="table" style="width:100%">
					  	<thead>
					  	<tr>
					  							  
					  		<th>FMA ID</th>
					  		<th>SQUARE KILOMETERS</th>
					  		<th>HECTARES</th>
					  		<th>LEAD REGION</th>
					  		<th>NO. of LGUs</th>
					  		<th>MAJOR COMMERCIAL SPECIES</th>
					  	</tr>
					  </thead>

					  <tbody>
			 <tr data-toggle="collapse" data-target="#FMA10" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-10</button></td>
				     <td>15,265</td>
				    <td>1,526,500</td>
				    <td>Region 7</td>
				    <td>83</td>
					<td>Mackerel<br> Scads</td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA10"> 
					  			<table class="table table-striped fixed_header">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 7</td>
										<td>

											Bohol,<br> 
											Cebu,<br>
											Negros Oriental, <br>
											Siquijor<br>
											HUC: Cebu City
										</td>
										<td>
											1. Cebu Strait<br>
											2. Bohol Strait<br> 
											(also known as Cebu Strait)<br>
											3. Camotes Sea<br>
											4. Danajon Bank<br>
											5. Maribojoc Bay
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 8</td>
										<td>
												Biliran,<br> 
												Leyte,<br>
												Southern Leyte
										</td>
										<td>
											1. Camotes Sea<br>
											2. Biliran Strait<br>
											3. Ormoc Strait
					  					</td>					
									</tr>	
														              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>	
	</tbody>
</table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->