<div class="modal" role="dialog" id="modal_fm2">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fisheries Management Areas</h4>
            </div>
            <div class="modal-body">

               		<table class="table" style="width:100%">
					  	<thead>
					  	<tr>
					  							  
					  		<th>FMA ID</th>
					  		<th>SQUARE KILOMETERS</th>
					  		<th>HECTARES</th>
					  		<th>LEAD REGION</th>
					  		<th>NO. of LGUs</th>
					  		<th>MAJOR COMMERCIAL SPECIES</th>
					  	</tr>
					  </thead>

					  <tbody>

		 <tr data-toggle="collapse" data-target="#FMA02" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-02</button></td>
				    <td>300,098</td>
				    <td>30,009,800</td>
				    <td>Region 11</td>
				    <td>83</td>
					<td>Tuna<br>Mackerel<br>Scads<br></td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA02"> 
					  			<table class="table table-striped fixed_header">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 8</td>
										<td>Eastern Samar,<br>Northern Samar</td>
										<td>1. Oras Bay<br>
											2. Philippine Sea<br>
											3. Matarinao BaySea<br>
											4. Bantayan BaySea<br>
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 11</td>
										<td>Davao De Oro,<br>
											Davao del Norte,<br>
											Davao del Sur,<br>
											Davao Occidental<br>
											Davao Oriental<br>
											</td>
										<td>
										1. Davao Gulf<br>
										2. Mayo Bay<br>
										3. Pujada Bay<br>
										4. Caraga Bay<br>
										5. Baculin Bay<br>
										6. Cateel Bay<br>
										7. Baganga Bay<br>
										8. Sarangani Strait<br>
										9. Manay Bay<br>
										10. Philippine Sea (Pacific)<br>
										11. Boston Bay<br>
										</td>
									</tr>	
									<tr>
										<td>13</td>
										<td>Sarangani (Region 12),<br>
											Surigao Del Norte,<br>
											Surigao del Sur
										</td>
										<td>
											1. Celebes Sea<br>
											2. East Siargao Waters<br>
											3. Hinatuan Bay<br>
											4. Surigao Sea<br>
											5. Bislig Bay<br>
											6. Lianga Bay<br>
					  					</td>					
									</tr>
						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>	
					              
                      </tbody>
		  		  			  		  		  			    	    		  
    </tbody>
</table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->