<div class="modal" role="dialog" id="modal_fm12">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fisheries Management Areas</h4>
            </div>
            <div class="modal-body">

               		<table class="table" style="width:100%">
					  	<thead>
					  	<tr>
					  							  
					  		<th>FMA ID</th>
					  		<th>SQUARE KILOMETERS</th>
					  		<th>HECTARES</th>
					  		<th>LEAD REGION</th>
					  		<th>NO. of LGUs</th>
					  		<th>MAJOR COMMERCIAL SPECIES</th>
					  	</tr>
					  </thead>

					  <tbody>
		 <tr data-toggle="collapse" data-target="#FMA12" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-12</button></td>
				    <td>36,674</td>
				    <td>3,667,400</td>
				    <td>Region 4A-CALABARZON</td>
				    <td>85</td>
					<td>
						Sardines<br>
						Scads<br>
						Tuna
					</td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA12"> 
					  			<table class="table table-striped fixed_header">
                      			<thead>
                        		<tr>
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>4A (CALABARZON)</td>
										<td>Batangas,<br> Quezon</td>
										<td>
											1. Balayan Bay<br>
											2. Calatagan Bay (Pagapas)<br>
											3. Batangas Bay <br>
											4. Tayabas Bay
					  					</td>
					  										
									</tr>
									<tr>
										<td>4B (MIMAROPA)</td>
										<td>
											Marinduque,<br>
											Occidental Mindoro,<br>
											Oriental Mindoro,<br>
											Romblon
										</td>
										<td>
											1. Tablas Bay<br>
											2. Mogpog Pass<br>
											3. Tayabas Bay
					  					</td>					
									</tr>	
									<tr>
										<td>5</td>
										<td>Masbate</td>
										<td>1. Sibuyan Sea
					  					</td>					
									</tr>
									<tr>
										<td>6</td>
										<td>
											Aklan,<br>Antique,<br> Capiz
					  					</td>
										<td>1. Southern Sibuyan Bay
					  					</td>					
									</tr>						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>				  		  			  		  		  			    	    		  
    </tbody>
</table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->