<div class="modal" role="dialog" id="modal_fm3">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fisheries Management Areas</h4>
            </div>
            <div class="modal-body">

               		<table class="table" style="width:100%">
					  	<thead>
					  	<tr>
					  							  
					  		<th>FMA ID</th>
					  		<th>SQUARE KILOMETERS</th>
					  		<th>HECTARES</th>
					  		<th>LEAD REGION</th>
					  		<th>NO. of LGUs</th>
					  		<th>MAJOR COMMERCIAL SPECIES</th>
					  	</tr>
					  </thead>

					  <tbody>
				 <tr data-toggle="collapse" data-target="#FMA03" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-03</button></td>
				    <td>166,659</td>
				    <td>16,665,900</td>
				    <td>Region 12</td>
				    <td>64</td>
					<td>Tuna<td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA03"> 
					  			<table class="table table-striped fixed_header">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 9</td>
										<td>
										Zamboanga Sibugay,<br>
										Zamboanga del Sur
										</td>
										<td>
											1. Moro Gulf<br>
											2. Iliana Bay<br>
											3. Dumanquillas Bay<br>

					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 12</td>
										<td>
											Sarangani,<br>
											South Cotabato,<br>
											Sultan Kudarat,<br>
											(incl. Davao Del Sur)										
										</td>
										<td>
											1. Sarangani Bay<br>
											2. Moro Gulf<br>
											3. Celebes Sea
					  					</td>					
									</tr>	
									<tr>
										<td>BARMM</td>
										<td>
											Basilan, <br>
											Lanao del Sur,<br>
											Maguindanao, <br>
											Sulu, <br>
											Tawi-Tawi
										</td>
										<td>&nbsp;
					  					</td>					
									</tr>
						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>	
    </tbody>
</table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->