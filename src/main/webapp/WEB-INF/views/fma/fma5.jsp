<div class="modal" role="dialog" id="modal_fm5">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fisheries Management Areas</h4>
            </div>
            <div class="modal-body">

               		<table class="table" style="width:100%">
					  	<thead>
					  	<tr>
					  							  
					  		<th>FMA ID</th>
					  		<th>SQUARE KILOMETERS</th>
					  		<th>HECTARES</th>
					  		<th>LEAD REGION</th>
					  		<th>NO. of LGUs</th>
					  		<th>MAJOR COMMERCIAL SPECIES</th>
					  	</tr>
					  </thead>

					  <tbody>

				 <tr data-toggle="collapse" data-target="#FMA05" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-05</button></td>
				     <td>485,417</td>
				    <td>48,541,700</td>
				    <td>MIMAROPA</td>
				    <td>50</td>
					<td>
						Roundscads<br>
						Mackerel<br>
						Tuna
					</td>
				</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA05"> 
					  			<table class="table table-striped fixed_header">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>4B(MIMAROPA)</td>
										<td>Palawan,<br>Occidental Mindoro</td>
										<td>
											1. Bacuit Bay<br>
											2. Sulu Sea-Brooke’s Point<br>
											3. Coron Bay<br>
											4. Green Island Bay<br>
											5. Honda Bay<br>
											6. Imuran Bay<br>
											7. Malanut Bay<br>
											8. Pagdanan Bay<br>
											9. Sulu Sea-Narra<br>
											10. Taytay Bay<br>
											11. Ulugan Bay<br>
											12. Mindoro Strait<br>
											13. Balabac Strait<br>
											14. Malampaya Sound<br>
											15. San Antonio Bay<br>
											17. Sulu Sea — PPC<br>
											18. Sulu Sea — Aborlan<br>
											19. West Philippine Sea —Rizal<br>
											20. Dumaran Channel<br>
											21. Cuyo Pass<br>
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 6</td>
										<td>Aklan,<br> Antique</td>
										<td>1. Cuyo East Pass
					  					</td>					
									</tr>	
									<tr>
										<td>BARMM</td>
										<td>Tawi-Tawi</td>
										<td>1. Sulu Sea
					  					</td>					
									</tr>
						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>	
				  		  			  		  		  			    	    		  
    </tbody>
</table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->