<div class="modal" role="dialog" id="modal_fm1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fisheries Management Areas</h4>
            </div>
            <div class="modal-body">

               		<table class="table" style="width:100%">
					  	<thead>
					  	<tr>
					  							  
					  		<th>FMA ID</th>
					  		<th>SQUARE KILOMETERS</th>
					  		<th>HECTARES</th>
					  		<th>LEAD REGION</th>
					  		<th>NO. of LGUs</th>
					  		<th>MAJOR COMMERCIAL SPECIES</th>
					  	</tr>
					  </thead>

					  <tbody>
					   <tr data-toggle="collapse" data-target="#FMA01" class="accordion-toggle">
					  	
					  	<td><button class="btn btn-default btn-xs">FMA-01</button></td>
					  	<td>505,345</td>
					  	<td>50,343,500</td>
					  	<td>Region 2</td>
					  	<td>103</td>
					  	<td>Tuna<br>Mackerel<br>Scads<br></td>
					  </tr>
			
					  <tr>
					  	<td colspan="12" class="hiddenRow">
							<div  class="accordian-body collapse" id="FMA01"> 
					  			<table class="table table-striped fixed_header">
                      			<thead>
                        		<tr>
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 2</td>
										<td>Batanes,<br>Cagayan<br> Isabela</td>
										<td>1. Babuyan Channel<br>
					  						2. Isabela Waters<br>
					  						3. Batanes Waters<br>
					  						4. Philippine Rise<br>
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 3</td>
										<td>Aurora</td>
										<td>1. Baler Bay<br>
					  						2. Casiguran Sound<br>
					  						3. Dingalan Bay<br>
					  						4. Philippine Rise<br>
					  					</td>					
									</tr>	
									<tr>
										<td>4A (CALABARZON)</td>
										<td>Quezon</td>
										<td>1. Lamon Bay<br>
					  						2. Philippine Rise<br>
					  						3. Polilio Strait<br>
					  						4. Burdeos Bay<br>
					  						5. Calauag Bay<br>
					  						6. Lopez Bay<br>
					  						7. Basian Bay<br>
					  						8. Anibawan Bay
					  					</td>					
									</tr>
									<tr>
										<td>5</td>
										<td>Albay,<br> 
					  						Camarines Norte,<br> 
					  						Camarines Sur,<br> 
					  						Catanduanes,<br> 
					  						Sorsogon (Incl Norther Samar)</td>
										<td>1. Albay Gulf<br>
					  						2. Lagonoy Gulf<br>
					  						3. Lamon Bay<br>
					  						4. San Miguel Bay<br>
					  						5. Philippine Rise<br>
					  						6. San Bernardino Strait<br>
					  						7. Maqueda Channel
					  					</td>					
									</tr>						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
        </tr>
				  		  			  		  		  			    	    		  
    </tbody>
</table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->