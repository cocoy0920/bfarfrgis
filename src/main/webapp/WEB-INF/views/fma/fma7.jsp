<div class="modal" role="dialog" id="modal_fm7">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Fisheries Management Areas</h4>
            </div>
            <div class="modal-body">

               		<table class="table" style="width:100%">
					  	<thead>
					  	<tr>
					  							  
					  		<th>FMA ID</th>
					  		<th>SQUARE KILOMETERS</th>
					  		<th>HECTARES</th>
					  		<th>LEAD REGION</th>
					  		<th>NO. of LGUs</th>
					  		<th>MAJOR COMMERCIAL SPECIES</th>
					  	</tr>
					  </thead>

					  <tbody>
	
		 <tr data-toggle="collapse" data-target="#FMA07" class="accordion-toggle">
					  	
					 <td><button class="btn btn-default btn-xs">FMA-07</button></td>
				    <td>16,699</td>
				    <td>1,669,900</td>
				    <td>Region 5</td>
				    <td>85</td>
					<td>Small-pelagics (i.e. sardines)</td>
		</tr>			
				     <tr>
					  	<td colspan="12" class="hiddenRow">
							<div class="accordian-body collapse" id="FMA07"> 
					  			<table class="table table-striped fixed_header">
                      			<thead>
                        		<tr class="info">
									<th>REGIONS</th>
									<th>PROVINCES</th>
									<th>FISHING GROUNDS</th>										
								</tr>
								</thead>	
								 <tbody>
									<tr>
										<td>Region 4A</td>
										<td>Quezon</td>
										<td>1. Ragay Gulf<br>
					  					</td>
					  										
									</tr>
									<tr>
										<td>Region 5</td>
										<td>
											Albay,<br>
											Camarines Sur,<br>
											Masbate,<br>
											Sorsogon
										</td>
										<td>
											1. Ragay Gulf<br>
											2. Sorsogon Bay<br>
											3. Burias Pass<br>
											4. Ticao Pass<br>
					  					</td>					
									</tr>	
									<tr>
										<td>8</td>
										<td>
											Biliran,<br>
											Leyte,<br> 
											Northern Samar,<br> 
											Wester Samar
										</td>
										<td>1. Samar Sea
					  					</td>					
									</tr>
						              
                      </tbody>
               	</table>
              
              </div> 
          </td>
				  </tr>	
    </tbody>
</table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->