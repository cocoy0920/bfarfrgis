<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Hatchery</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />

<jsp:include page="../menu/css.jsp"></jsp:include>
	<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
	<script src="/static/js/form/deleteMapList.js"></script>
<script src="/static/js/getDataById/viewImageById.js"></script>
<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
	
</head>
<!-- <body onload="getFisheriesData()"> -->
<body>
 <jsp:include page="../users/header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">
  <!-- Sidebar Holder -->
<jsp:include page="../menu/home_sidebar.jsp"></jsp:include>

  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">	
					<div id="map"></div>

			</div>

    </div>

	<script>



//var url = '/static/geojson/'+ ${region_map} +'.geojson'; 
var arr = [];
var arr1 = [];
//var region = ${region_map};
//console.log(region);
 var map_location = ${mapsData}; 
var lll = ${hl};

console.log(lll);

 // var map_location;
 /*
 function getJSONData(){
  $.getJSON("data", function(data) {
	console.log(data);
	map_location = data;								
	});
	}
	window.onload = getJSONData;
*/	
//localStorage.setItem('regionmap', JSON.stringify(url));
//const map_data = JSON.parse(localStorage.getItem('regionmap'));
  // Initialize autocomplete with empty source.
 // $( "#autocomplete" ).autocomplete(); zoomControl: false,
	
var map =L.map('map',{scrollWheelZoom:false}).setView([8.678300, 125.800369],6);

  var OpenTopoMap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
    opacity: 0.90
  });
  
 // OpenTopoMap.addTo(map);

	map.zoomControl.setPosition('topright');
	var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
				maxZoom: 18,
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'});
	
	
	

	//osm.addTo(map);
	// https: also suppported.
	 var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
	});
	
	 Esri_WorldImagery.addTo(map);
	
	// https: also suppported.
	var Esri_WorldGrayCanvas = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'});
		
	var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {	
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
	});
	
	
/* 	var wmsLayer = L.tileLayer.wms('http://geo.bfar.da.gov.ph/gwc/service/wms', {
    layers: 'region:Region_I'
	});
	 */
	
L.control.scale().addTo(map);

function style(feature) {
    return {
        fillColor: 'green', 
        fillOpacity: 0.5,  
        weight: 2,
        opacity: 1,
        color: '#ffffff',
        dashArray: '3'
    };
}

	var highlight = {
		'fillColor': 'yellow',
		'weight': 2,
		'opacity': 1
	};
	 
	var LeafIconMarker = L.Icon.extend({
			 options: {
		iconSize:     [30, 48]
	}
	});

	
	
 	var coordList = [];
	var iconMarker = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/hatcheries.png"});

 	function forEachFeature(feature, layer,latlng) {
           var popupContent = "<p>Location"  + feature.properties.Location + "</br><b>RaNo: </b>" + feature.properties.RANo +"" + "</br><b>Status: </b>" + feature.properties.Status + '</p>';
           
          //var popupContent = new  L.marker(latlng,{icon: iconMarker}).bindPopup(feature.properties.Location + "<br>" + feature.properties.RANo + "<br>" + feature.properties.Status);
            
           
          // coordList.push(popupContent);
           
           
            layer.bindPopup(popupContent);
            
            layer.on("click", function (e) { 
                stateLayer.setStyle(style);
                layer.setStyle(highlight);
                
            });  
            
		}


		var stateLayer = L.geoJson(null, {onEachFeature: forEachFeature, style: style});
					
			$.getJSON("/static/geojson/StatusofLegislatedHatcheries.geojson", function(data) {
				
		        stateLayer.addData(data);

		    });

	var HatchMarker;
	var HatchMarkerList;

		if( lll == "legislated"){

	 	$.getJSON("/static/geojson/StatusofLegislatedHatcheries.geojson",function(data){
	 		L.geoJson(data  ,{
		    pointToLayer: function(feature,latlng){
		    	HatchMarker = new L.marker(latlng,{icon: iconMarker}).addTo(map).bindPopup(feature.properties.Location + "<br>" + feature.properties.RANo + "<br>" + feature.properties.Status).on('click', clickZoom);

		    	HatchMarker.getPopup().on('remove', function() {

					 window.location.reload();

				});
		    }
	 		
		  })
		  function clickZoom(e) {
            map.setView(e.target.getLatLng(),17);
        }
		});  
	 	
		}
		
		
		
  		function polySelect(a){
  		console.log("polySelect(a): " + a);
			map._layers[a].fire('click'); 
			var layer = map._layers[a];
			map.fitBounds(layer.getBounds());
        }
  //    var geojsonLayer = new L.GeoJSON.AJAX(map_data);

     
//geojsonLayer.addTo(map);       
 var kiko = L.layerGroup(coordList);      
var baseMaps = {
    /*  "Open Street Map": osm,
   	"OSM B&W":OpenStreetMap_BlackAndWhite,
   	"Esri WorldI magery":Esri_WorldImagery,
   	"Esri World Gray Canvas":Esri_WorldGrayCanvas, */
   //	"Legislated Hatchery" :stateLayer

   	
};

/*  var overlayMaps = {
   "PHILIPPINES":geojsonLayer
};
	 */
//L.control.layers(baseMaps).addTo(map);
L.control.layers(null,baseMaps,{collapsed:false}).addTo(map);	 
//L.control.layers(baseMaps).addTo(map);	
		var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});


		//function getFisheriesData(){
			getHatchery(map_location);
		//}

</script>


<script type="text/javascript">
        $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
        </script> 
</body>
</html>