<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
					
								<ul class="nav navbar-default">
								
								<li><a href="/home/Hfishsanctuaries" class=" animated-button victoria-four">
								<img class="img-thumbnail" height="15" width="50" src="/static/images/iconCircle/fishsanctuary.png">
								<br>FISH SANCTUARY
								</a>
								</li>								
								<li><a href="/home/Hfishprocessingplants" class=" animated-button victoria-four">
								<img class="img-thumbnail" height="15" width="50" src="/static/images/iconCircle/fishprocessing.png">
								<br>FISH PROCESSING
								</a>
								</li>
								<c:if test = "${regionName != 'CAR'}">								
								<li><a href="/home/HfishLanding" class=" animated-button victoria-four">
								<img class="img-thumbnail" height="15" width="50" src="/static/images/iconCircle/fish-landing.png">
								<br>FISH LANDING
								</a>
								</li>
								<li><a href="/home/HFishPort" class=" animated-button victoria-four">
								<img class="img-thumbnail" height="15" width="50" src="/static/images/iconCircle/fishport.png">
								<br>FISH PORT
								</a>
								</li>
								</c:if>								
								<li><a href="/home/HfishPen" class=" animated-button victoria-four">
								<img class="img-thumbnail" height="15" width="50" src="/static/images/iconCircle/fishpen.png">
								<br>FISH PEN
								</a>
								</li>
								
								<li><a href="/home/HfishCage" class=" animated-button victoria-four">
								<img class="img-thumbnail" height="15" width="50" src="/static/images/iconCircle/fishcage.png">
								<br>FISH CAGE
								</a>
								</li>
										
								<!-- <li><a href="/home/HfishPond" class=" animated-button victoria-four">
								<img class="img-thumbnail" height="15" width="50" src="/static/images/iconCircle/fish-pond.png">
								<br>FISH POND
								</a>
								</li> -->
								
								<li><a href="/home/HHatchery" class=" animated-button victoria-four">
								<img class="img-thumbnail" height="15" width="50" src="/static/images/iconCircle/hatcheries.png">
								<br>HATCHERY
								</a>
								</li>
								<li><a href="/home/HIPCS" class=" animated-button victoria-four">
								<img class="img-thumbnail" height="15" width="50" src="/static/images/iconCircle/iceplant.png">
								<br>ICE PLANT/COLD STORAGE
								</a>
								</li>
								
								<c:if test = "${regionName != 'CAR'}">	
								
								</c:if>
								
							</ul>
<!-- 						<ul class="list-group">
  <li class="list-group-item active"><a href="/home/Hfishsanctuaries" class=" animated-button victoria-four">
								<img class="img-thumbnail" height="15" width="50" src="/static/images/iconCircle/fishsanctuary.png">
								<br>FISH SANCTUARY
								</a></li>
  <li class="list-group-item"><a href="/home/Hfishprocessingplants" class=" animated-button victoria-four">
								<img class="img-thumbnail" height="15" width="50" src="/static/images/iconCircle/fishprocessing.png">
								<br>FISH PROCESSING
								</a></li>
  <li class="list-group-item"><a href="/home/HfishLanding" class=" animated-button victoria-four">
								<img class="img-thumbnail" height="15" width="50" src="/static/images/iconCircle/fish-landing.png">
								<br>FISH LANDING
								</a></li>
  <li class="list-group-item"><a href="/home/HFishPort" class=" animated-button victoria-four">
								<img class="img-thumbnail" height="15" width="50" src="/static/images/iconCircle/fishport.png">
								<br>FISH PORT
								</a></li>
  <li class="list-group-item">Vestibulum at eros</li>
</ul> -->
