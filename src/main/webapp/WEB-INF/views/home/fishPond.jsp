


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html>
<head>
<link rel="icon" type="image/png"
	href="<c:url value='/static/images/favicon.png' />" />
<title></title>
<link href="<c:url value='/static/leaflet/leaflet.css' />"
	rel="stylesheet"></link>
<link href="<c:url value='/static/css/geo.css' />" rel="stylesheet"></link>
<link href="/static/css/navbar.css" rel="stylesheet"></link>
<script src="/static/js/navbar.js"></script>
<script src="/static/js/form/deleteMapList.js"
	type=" text/javascript"></script>
<script src="/static/js/getDataById/viewImageById.js"
	type=" text/javascript"></script>
<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>

<style>
li:hover {
	cursor: pointer;
}
</style>
<jsp:include page="../menu/css.jsp"></jsp:include>
</head>
<body onload="getFisheriesData()">
	<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>

	<div class="se-pre-con"></div>

 <jsp:include page="../users/header.jsp"></jsp:include>
<jsp:include page="navigation.jsp"></jsp:include>
<br>

	<!-- end of header -->
	<!-- <div id="mapContent" class="container-fluid">
	<div class="row content"> -->
	<!-- <div class="col-md-12"> -->
	<div class="container-fluid text-center">
		<div class="row content">
			<div class="nav col-md-2 sidenav site-navigation">
				<nav class="nav">
					<div class="leftForm">
						<jsp:include page="leftForm.jsp"></jsp:include>
					</div>
				</nav>
			</div>
			<div class="col-md-8 panel panel-default"
				style="box-shadow: 0 3px 3px 1px #000000;" id='section-to-print'>
				<div class="row" id="reloadMap">

					<div class="col-sm-12" id="map"></div>

				</div>

			</div>
			<div class="col-md-2 sidenav site-navigation">
				<nav class="nav">
					<jsp:include page="rightForm.jsp"></jsp:include>
				</nav>
			</div>
		</div>
	</div>

	<div id="details" class="clock leaflet-control"></div>

	<div id="footer">
		<jsp:include page="../users/footer.jsp"></jsp:include>
	</div>

	<script>



//var url = '/static/geojson/'+ ${region_map} +'.geojson'; 
var arr = [];
var arr1 = [];
//var region = ${region_map};
//console.log(region);
 var map_location = ${mapsData}; 
 // var map_location;
 /*
 function getJSONData(){
  $.getJSON("data", function(data) {
	console.log(data);
	map_location = data;								
	});
	}
	window.onload = getJSONData;
*/	
//localStorage.setItem('regionmap', JSON.stringify(url));
//const map_data = JSON.parse(localStorage.getItem('regionmap'));
  // Initialize autocomplete with empty source.
 // $( "#autocomplete" ).autocomplete(); zoomControl: false,
	
var map =L.map('map',{scrollWheelZoom:false}).setView([8.678300, 125.800369],6);

  var OpenTopoMap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
    opacity: 0.90
  });
  
  OpenTopoMap.addTo(map);

	map.zoomControl.setPosition('topright');
	var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
				maxZoom: 18,
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'});
	
	
	

	//osm.addTo(map);
	// https: also suppported.
	 var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
	});
	
	// https: also suppported.
	var Esri_WorldGrayCanvas = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'});
		
	var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {	
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
	});
	
	
/* 	var wmsLayer = L.tileLayer.wms('http://geo.bfar.da.gov.ph/gwc/service/wms', {
    layers: 'region:Region_I'
	});
	 */
	
L.control.scale().addTo(map);

function style(feature) {
    return {
        fillColor: 'green', 
        fillOpacity: 0.5,  
        weight: 2,
        opacity: 1,
        color: '#ffffff',
        dashArray: '3'
    };
}

	var highlight = {
		'fillColor': 'yellow',
		'weight': 2,
		'opacity': 1
	};

	
		function forEachFeature(feature, layer) {
            layer._leaflet_id = feature.properties.REGION;
            var popupContent = "<p><b>REGION: </b>" + feature.properties.REG_NM + "(" + feature.properties.REG_VAR_NM +")" + "</br><b>PROVINCE: </b>" + feature.properties.PRV_NM + "</br><b>MUNICIPALITY: </b>" + feature.properties.NAME +'</p>';

            layer.bindPopup(popupContent);
            
            layer.on("click", function (e) { 
                stateLayer.setStyle(style);
                layer.setStyle(highlight);
                
            }); 
            
		}

  		function polySelect(a){
  		console.log("polySelect(a): " + a);
			map._layers[a].fire('click'); 
			var layer = map._layers[a];
			map.fitBounds(layer.getBounds());
        }
  //    var geojsonLayer = new L.GeoJSON.AJAX(map_data);

     
//geojsonLayer.addTo(map);       
        
var baseMaps = {
    "Open Street Map": osm,
   	"OSM B&W":OpenStreetMap_BlackAndWhite,
   	"Esri WorldI magery":Esri_WorldImagery,
   	"Esri World Gray Canvas":Esri_WorldGrayCanvas,
   	/* "Geo map" :wmsLayer */
   	
};

/*  var overlayMaps = {
   "PHILIPPINES":geojsonLayer
};
	 */
L.control.layers(baseMaps).addTo(map);
//L.control.layers(baseMaps).addTo(map);	
		var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});


		function getFisheriesData(){
			getFishPond(map_location);
		}

</script>

	<script>
//paste this code under the head tag or in a separate js file.
	// Wait for window load
	 $(window).on('load', function () {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");
	});
	</script>
	<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>
	<script src="/static/js/form/script.js" type=" text/javascript"></script>
<script  src="<c:url value='/static/js/jquery-1.8.3.min.js' />" type=" text/javascript"></script>
<script src="<c:url value='/static/js/bootstrap.min.js' />" type=" text/javascript"></script>

	<div id="myModal" class="modals">
		<div class="close_id">
			<a href="#"><span class="close">&times;</span></a>
		</div>
		<img class="modal-content" id="img01">
		<div class="modal-map"></div>
		<div id="caption"></div>
	</div>

</body>
</html>