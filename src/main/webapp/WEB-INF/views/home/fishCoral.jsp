<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Fish Coral</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />

<jsp:include page="../menu/css.jsp"></jsp:include>
	<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
	<script src="/static/js/form/deleteMapList.js"></script>
<script src="/static/js/getDataById/viewImageById.js"></script>
<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
	
</head>
<body onload="getFisheriesData()">
 <jsp:include page="../users/header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">
  <!-- Sidebar Holder -->
<jsp:include page="../menu/home_sidebar.jsp"></jsp:include>

  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">	
					<div id="map"></div>

			</div>

    </div>

	<script>



//var url = '/static/geojson/'+ ${region_map} +'.geojson'; 
var arr = [];
var arr1 = [];
//var region = ${region_map};
//console.log(region);
 var map_location = ${mapsData}; 
 // var map_location;
 /*
 function getJSONData(){
  $.getJSON("data", function(data) {
	console.log(data);
	map_location = data;								
	});
	}
	window.onload = getJSONData;
*/	
//localStorage.setItem('regionmap', JSON.stringify(url));
//const map_data = JSON.parse(localStorage.getItem('regionmap'));
  // Initialize autocomplete with empty source.
 // $( "#autocomplete" ).autocomplete(); zoomControl: false,
	
var map =L.map('map',{scrollWheelZoom:false}).setView([8.678300, 125.800369],6);

  var OpenTopoMap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
    opacity: 0.90
  });
  
  OpenTopoMap.addTo(map);

	map.zoomControl.setPosition('topright');
	var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
				maxZoom: 18,
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'});
	
	
	

	//osm.addTo(map);
	// https: also suppported.
	 var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
	});
	
	// https: also suppported.
	var Esri_WorldGrayCanvas = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'});
		
	var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {	
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
	});
	
	
/* 	var wmsLayer = L.tileLayer.wms('http://geo.bfar.da.gov.ph/gwc/service/wms', {
    layers: 'region:Region_I'
	});
	 */
	
L.control.scale().addTo(map);

function style(feature) {
    return {
        fillColor: 'green', 
        fillOpacity: 0.5,  
        weight: 2,
        opacity: 1,
        color: '#ffffff',
        dashArray: '3'
    };
}

	var highlight = {
		'fillColor': 'yellow',
		'weight': 2,
		'opacity': 1
	};

	
		function forEachFeature(feature, layer) {
            layer._leaflet_id = feature.properties.REGION;
            var popupContent = "<p><b>REGION: </b>" + feature.properties.REG_NM + "(" + feature.properties.REG_VAR_NM +")" + "</br><b>PROVINCE: </b>" + feature.properties.PRV_NM + "</br><b>MUNICIPALITY: </b>" + feature.properties.NAME +'</p>';

            layer.bindPopup(popupContent);
            
            layer.on("click", function (e) { 
                stateLayer.setStyle(style);
                layer.setStyle(highlight);
                
            }); 
            
		}

  		function polySelect(a){
  		console.log("polySelect(a): " + a);
			map._layers[a].fire('click'); 
			var layer = map._layers[a];
			map.fitBounds(layer.getBounds());
        }
  //    var geojsonLayer = new L.GeoJSON.AJAX(map_data);

     
//geojsonLayer.addTo(map);       
        
var baseMaps = {
    "Open Street Map": osm,
   	"OSM B&W":OpenStreetMap_BlackAndWhite,
   	"Esri WorldI magery":Esri_WorldImagery,
   	"Esri World Gray Canvas":Esri_WorldGrayCanvas,
   	/* "Geo map" :wmsLayer */
   	
};

/*  var overlayMaps = {
   "PHILIPPINES":geojsonLayer
};
	 */
L.control.layers(baseMaps).addTo(map);
//L.control.layers(baseMaps).addTo(map);	
		var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});


		function getFisheriesData(){
			getFishCorals(map_location);
		}

</script>


<script type="text/javascript">
        $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
        </script> 
</body>
</html>