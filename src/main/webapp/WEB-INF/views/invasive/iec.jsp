<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="Programmer" content="Francisco P. Vejerano - BFAR">
  <meta name="Web Designer" content="Mary Alaine B. Albaladejo - BFAR">
  <meta name="Description" content="BFAR Invasive Fishes On-line Information System">
  <meta name="author" content="">

  <title>INVASIVE FISHES</title>

  <!-- Bootstrap core CSS -->
  <link href="/static/invasive/css/bootstrap.css" rel="stylesheet">
  <link href="/static/invasive/css/styles.css" rel="stylesheet">



  <!-- Custom styles for this template -->
  <link href="/static/invasive/css/modern-business.css" rel="stylesheet">

</head>

<body>
  <img class="img-responsive img-center" src="/static/invasive/img/sampleInvasive.png" height="120" width="100%">
  <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="/invasive">INVASIVE FISHES</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Reference
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
              
              <a class="dropdown-item" href="/invasive/iec">IEC Materials</a>
              <a class="dropdown-item" href="/invasive/proceedings">Proceedings</a>
        
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="/invasive/activities">Activities</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Announcement</a>
          </li>
         
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Gallery
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
              
              <a class="dropdown-item" href="/invasive/photogallery">Photo Gallery</a>
              <a class="dropdown-item" href="/invasive/videogallery">Video Gallery</a>
        
            </div>
          </li>
       
          
          

          <li class="nav-item">
            <a class="nav-link" href="/invasive/map">Map</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="/invasive/events">Events</a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              LOGIN
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
              
              <a class="dropdown-item" href="/invasive/login">LOGIN</a>
            </div>
          </li>


        </ul>

      </div>
    </div>
  </nav>



  <div class="container">
    <div style="overflow:auto">
      <div class="main">
       
          <h1 class="my-4">IEC Materials</h1>
              <h6>PRODUCTION AND DISSEMINATION OF INFORMATION ON INVASIVE SPECIES</h6>
          
             
                <div class="row">
                  <div class="col-sm-3">
                    <img src="/static/invasive/img/IECMaterials/BOOKMARK.png" class="img-responsive" style="width:100%" alt="Image">
                    <p>BOOKMARK</p>
                  </div>
                  <div class="col-sm-3"> 
                    <img src="/static/invasive/img/IECMaterials/CALENDAR.png" class="img-responsive" style="width:100%" alt="Image">
                    <p>CALENDAR</p>    
                  </div>
                  <div class="col-sm-3"> 
                    <img src="/static/invasive/img/IECMaterials/DVD.png" class="img-responsive" style="width:100%" alt="Image">
                    <p>DVD</p>
                  </div>
                  <div class="col-sm-3"> 
                    <img src="/static/invasive/img/IECMaterials/FAN1.png" class="img-responsive" style="width:100%" alt="Image">
                    <p>FAN</p>
                  </div> 
                  
                </div>
                <br>
                <div class="row">
                  <div class="col-sm-3">
                    <img src="/static/invasive/img/IECMaterials/FAN3.png" class="img-responsive" style="width:100%" alt="Image">
                    <p>BOOKMARK</p>
                  </div>
                  <div class="col-sm-3"> 
                    <img src="img/IECMaterials/Fliers.png" class="img-responsive" style="width:100%" alt="Image">
                    <p>CALENDAR</p>    
                  </div>
                  <div class="col-sm-3"> 
                    <img src="/static/invasive/img/IECMaterials/POSTER.png" class="img-responsive" style="width:100%" alt="Image">
                    <p>DVD</p>
                  </div>
                  <div class="col-sm-3"> 
                    <img src="/static/invasive/img/IECMaterials/STICKER1.png" class="img-responsive" style="width:100%" alt="Image">
                    <p>FAN</p>
                  </div> 
                  
                </div>
              <br>
      </div>
    
      <div class="right">
        <div class="sidebar">
          <h2>About</h2> 
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
       </div>
        <br>

        <div class="sidebar">
          <h2>About</h2> 
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
       </div> 
      </div>
    </div>
</div>
  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy;2022 All rights reserved- Bureau of Fisheries and Aquatic
        Resources</p>

    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="/static/invasive/jquery/jquery.min.js"></script>
  <script src="/static/invasive/javas/bootstrap.bundle.min.js"></script>


</body>

</html>