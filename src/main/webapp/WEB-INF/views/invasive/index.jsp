<!doctype html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="Programmer" content="Francisco P. Vejerano - BFAR">
  <meta name="Web Designer" content="Mary Alaine B. Albaladejo - BFAR">
  <meta name="Description" content="BFAR Invasive Fishes On-line Information System">
  <meta name="author" content="">

  <title>INVASIVE FISHES</title>

  <!-- Bootstrap core CSS -->
  <link href="/static/invasive/css/bootstrap.css" rel="stylesheet">
  <link href="/static/invasive/css/styles.css" rel="stylesheet">
  <link href="/static/invasive/css/flip.css" rel="stylesheet">
  

  <!-- Custom styles for this template -->
  <link href="/static/invasive/css/modern-business.css" rel="stylesheet">
  <link rel="shortcut icon" type="image/png" href="/static/invasive/img/favicon.png" />
  

</head>

<body>
<img class="img-responsive img-center" src="/static/invasive/img/sampleInvasive.png"  height="120" width="100%">
  <!-- Navigation -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="/invasive">INVASIVE FISHES</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Reference
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
              
              <a class="dropdown-item" href="/invasive/iec">IEC Materials</a>
              <a class="dropdown-item" href="/invasive/proceedings">Proceedings</a>
        
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="/invasive/activities">Activities</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Announcement</a>
          </li>
         
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Gallery
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
              
              <a class="dropdown-item" href="/invasive/photogallery">Photo Gallery</a>
              <a class="dropdown-item" href="/invasive/videogallery">Video Gallery</a>
        
            </div>
          </li>
       
          
          

          <li class="nav-item">
            <a class="nav-link" href="/invasive/map">Map</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="/invasive/events">Events</a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              LOGIN
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
              
              <a class="dropdown-item" href="/invasive/login">LOGIN</a>
            </div>
          </li>


        </ul>

      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">

    <h1 class="my-4">BFAR INVASIVE FISHES</h1>

    

    <div class="boxesContainer">
    <div class="row mb-12">
      
      <div class="col-md-4">
        <div class="card " style="border: 1px solid rgba(0, 0, 0, 0.125);">
          <h5 class="card-header">KNIFEFISH </h5>
          <div class="card-body">
            
            <div class="cardBox">
              <div class="card">
                <div class="flip-card-front">
                  <h3><img class="img-responsive img-center" src="/static/invasive/img/knife_fish.png"  height="120" width="100%"></h3>
                  <ul>
                    <li><strong>PERCEIVED THREAT:</strong></li>
                    <li>Damage to prey population, Disruption of ecosystem, Substantial economic loss.  </li>
                    <br>
                    <li><strong> SIGHTINGS:</strong></li>
                    <li>Laguna de Bay  </li>
                  </ul>
                </div>
                <div class="back">
                  
                  <img src="/static/invasive/img/knife_fish.png"  style="width:150px;height:90px;"> 
                  <h6>KNIFEFISH <br>
                    (Chitala Ornata)</h6> 
                  <button onclick="document.getElementById('id01').style.display='block'" class="btn btn-primary">Read more</button>
                </div>
              </div>
            </div>
            
          </div>
          </div>
      </div>
      <div class="col-md-4">
        <div class="card " style="border: 1px solid rgba(0, 0, 0, 0.125);">
          <h5 class="card-header">
            GIANT THAI SNAKEHEAD  </h5>
          <div class="card-body">
            
            <div class="cardBox">
              <div class="card">
                <div class="flip-card-front">
                  <h3><img class="img-responsive img-center" src="/static/invasive/img/Giant_snakehead.png"  height="120" width="100%"></h3>
                  <ul>
                    <li><strong>PERCEIVED THREAT:</strong></li>
                    <li>Damage to prey population, Disruption of ecosystem.  </li>
                    <br>
                    <li><strong> SIGHTINGS:</strong></li>
                    <li>Pantabangan Dam   </li>
                  </ul>
                </div>
                <div class="back">
                  
                  <img src="/static/invasive/img/Giant_snakehead.png"  style="width:150px;height:90px;"> 
                  <h6>GIANT THAI SNAKEHEAD<br>
                    Channa micropeltes</h6> 
                  <button onclick="document.getElementById('id04').style.display='block'" class="btn btn-primary">Read more</button>
                </div>
              </div>
            </div>

        </div>
          </div>
      </div>
      <div class="col-md-4">
        <div class="card " style="border: 1px solid rgba(0, 0, 0, 0.125);">
          <h5 class="card-header">JAGUAR GUAPOTE</h5>
          <div class="card-body">
            <div class="cardBox">
              <div class="card">
                <div class="flip-card-front">
                  <h3><img class="img-responsive img-center" src="/static/invasive/img/jaguar.png"  height="120" width="100%"></h3>
                  <ul>
                    <li><strong>PERCEIVED THREAT:</strong></li>
                    <li>Damage to prey population, Disruption of ecosystem.  </li>
                    <br>
                    <li><strong> SIGHTINGS:</strong></li>
                    <li>Taal Lake </li>
                  </ul>
                </div>
                <div class="back">
                  
                  <img src="/static/invasive/img/jaguar.png"  style="width:150px;height:90px;"> 
                  <h6>JAGUAR GUAPOTE<br>
                    Parachromis manguensis</h6> 
                  <button onclick="document.getElementById('id04').style.display='block'" class="btn btn-primary">Read more</button>
                </div>
              </div>
            </div>
          </div>
          </div>
      </div>
    </div>
  </div>


    <br>
    <br>


    <div class="row mb-12">
      
      <div class="col-md-4">
        <div class="card " style="border: 1px solid rgba(0, 0, 0, 0.125);">
          <h5 class="card-header">GLORIA </h5>
          <div class="card-body">
            
            <div class="cardBox">
              <div class="card">
                <div class="flip-card-front">
                  <h3><img class="img-responsive img-center" src="/static/invasive/img/gloria.png"  height="120" width="100%"></h3>
                  <ul>
                    <li><strong>PERCEIVED THREAT:</strong></li>
                    <li>Genetic contamination, Erode Biodiversity   </li>
                    <br>
                    <li><strong> SIGHTINGS:</strong></li>
                    <li>Manila Bay  </li>
                  </ul>
                </div>
                <div class="back">
                  
                  <img src="/static/invasive/img/gloria.png"  style="width:150px;height:90px;"> 
                  <h6>GLORIA <br>
                    Sarotherodon melanotheron</h6> 
                  <button onclick="document.getElementById('id04').style.display='block'" class="btn btn-primary">Read more</button>
                </div>
              </div>
            </div>
            
          </div>
          </div>
      </div>
      <div class="col-md-4">
        <div class="card " style="border: 1px solid rgba(0, 0, 0, 0.125);">
          <h5 class="card-header">
            WALKING CATFISH   </h5>
          <div class="card-body">
            
            <div class="cardBox">
              <div class="card">
                <div class="flip-card-front">
                  <h3><img class="img-responsive img-center" src="/static/invasive/img/walking_catfish.png"  height="120" width="100%"></h3>
                  <ul>
                    <li><strong>PERCEIVED THREAT:</strong></li>
                    <li>Genetic contamination, Erode Biodiversity   </li>
                    <br>
                    <!-- <li><strong> SIGHTINGS:</strong></li>
                    <li>Manila Bay  </li> -->
                  </ul>
                </div>
                <div class="back">
                  
                  <img src="/static/invasive/img/walking_catfish.png"  style="width:150px;height:90px;"> 
                  <h6>WALKING CATFISH <br>
                    (Clarias batrachus)</h6> 
                  <button onclick="document.getElementById('id04').style.display='block'" class="btn btn-primary">Read more</button>
                </div>
              </div>
            </div>

        </div>
          </div>
      </div>
      <div class="col-md-4">
        <div class="card " style="border: 1px solid rgba(0, 0, 0, 0.125);">
          <h5 class="card-header">JANITOR FISH</h5>
          <div class="card-body">
            <div class="cardBox">
              <div class="card">
                <div class="flip-card-front">
                  <h3><img class="img-responsive img-center" src="/static/invasive/img/Janitor_fish.png"  height="120" width="100%"></h3>
                  <ul>
                    <li><strong>PERCEIVED THREAT:</strong></li>
                    <li>Destruction or tearing of nets due to the hard body plate.  </li>
                    <br>
                    <li><strong> SIGHTINGS:</strong></li>
                    <li>Laguna de Bay, Agusan Marsh, Pulangui Reservoir  </li>
                  </ul>
                </div>
                <div class="back">
                  
                  <img src="/static/invasive/img/Janitor_fish.png"  style="width:150px;height:90px;"> 
                  <h6>JANITOR FISH <br>
                    Pterygoplichthys disjunctivus</h6> 
                  <button onclick="document.getElementById('id04').style.display='block'" class="btn btn-primary">Read more</button>
                </div>
              </div>
            </div>
          </div>
          </div>
      </div>
    </div>
<br>
<br>

  

   
   
    <div class="row">
      <div class="col-lg-12">
        <p>Invasive alien species are non-native species transported to the new environment where they result to mass colonization resulting to damage to prey population, erosion of biodiversity, disruption of the ecosystem, genetic contamination, disease introduction and substantial socio-economic cost.  Invasive  alien species has cause 50% of global animal extinction and 48% of fish extinction (Ricciardi, 2013).</p>
<p> In the Philippines, Guerrero (2014) reported that of the 60 introduced freshwater fishes, 8 are invasive and 4 are potentially invasive.  </p>
<p>The invasive species website was created to serve as repository of information pertaining to the  biology of some reported invasive fishes; their  perceived threats, reported sightings; strategies and  interventions of concerned government agencies; and information-education campaign  materials.  It provides updates of the activities of the inter-agency technical working group on the containment of knifefish in Laguna de Bay as well as special events of public interest.</p>
<p>While initially focus on knifefish,  it intends to include information on  other potentially invasive  species.   </p>
<p>The website is interactive providing hotline for citizen involvement and invites  contribution from the public. </p>
    </div>
      
    </div>
</div>
    <br>
    <br>
    <center>
    <div class="mainlogo">
      
      <a href="http://www.pcarrd.dost.gov.ph/" target="_blank"><img class="img-responsive img-center" src="/static/invasive/img/logo/PCAARRD.png" width="100" height="100"  /></a>
      <a href="http://www.dti.gov.ph/" target="_blank"><img class="img-responsive img-center" src="/static/invasive/img/logo/DTI.png" width="100" height="100" /></a>
      <a href="http://www.dilg.gov.ph/" target="_blank"><img class="img-responsive img-center" src="/static/invasive/img/logo/DILG.png" width="100" height="100" /></a>
      <a href="http://www.llda.gov.ph/" target="_blank"><img class="img-responsive img-center" src="/static/invasive/img/logo/LLDA.png" width="100" height="100" /></a>
      <a href="http://www.dswd.gov.ph/" target="_blank"><img class="img-responsive img-center" src="/static/invasive/img/logo/DSWD.jpg" width="100" height="100" /></a>
      <a href="http://www.tesda.gov.ph/" target="_blank"><img class="img-responsive img-center" src="/static/invasive/img/logo/TESDA.png" width="100" height="100" /></a>
      <a href="http://www.denr.gov.ph/" target="_blank"><img class="img-responsive img-center" src="/static/invasive/img/logo/DENR.png" width="100" height="100" /></a>
      </div>	
  </center>
    <br>

  
    
    
<!-- /.row -->

    <br>

    
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy;2022 All rights reserved- Bureau of Fisheries and Aquatic Resources</p>
      
    </div>
    <!-- /.container -->
  </footer>
  <div id="id01" class="modal">

    <div class="modal-content">

      <header class="modal-header"> 
        <span onclick="document.getElementById('id01').style.display='none'"class="button">&times;</span>
        
      </header>
      
      <div class="modal-body">
        <div class="row mb-12">
        <div class="col-md-4">
          <div class="imagepostion">
          <img src="/static/invasive/img/knife_fish.png"  height="120" width="70%">   
        </div>
        </div>
        <div class="col-md-4"><p>Chitala Ornata also known as Knifefish occurs naturally in swamps, lakes and rivers of Southeast Asia and South America. It has a flat silvery body with long anal fin and 5-10black spots ringed with below the lateral line. Iit was introduced in the Philippines through the ornamental fish trade. 

          The fish is nocturnal, bentho-pelagic and often referred to as big bully in the aquarium because of its aggressiveness and highly carnivorous nature. Its mouth is equipped with razor sharp teeth and has bony tongue (Osteoglossiformes) and equally sharp gill rakers. 
          
          Analysis of the gut content showed mainly remnants of heads/tails of smal fishes (35.29%), scales and eyes of other fishes particularly Arius manilensis (29.41%), Shrimps (17.65%) and shells (17.65%)</p> </div>
        <div class="col-md-4">sightnings </div>
        </div>
      </div>

      
      <footer class="modal-footer">
        
      </footer>
    </div>
  </div>

  <div id="id02" class="modal">

    <div class="modal-content">

      <header class="modal-header"> 
        <span onclick="document.getElementById('id02').style.display='none'"class="button">&times;</span>
        
      </header>
      
      <div class="modal-body">
        <div class="popup__left">
        <img src="/static/invasive/img/Giant_snakehead.png"/>
        </div>
        <div class="popup_right">
        <p class="popup__text">
          <strong>Channa micropeltes</strong> also known as Giant Snakehead or Toman is among the largest species and the most aggressive of the 35 snakehead species. It is native to the freshwater of Southeast Asia but has been introduced to other countries, including Philippines through the ornamental fish trade. It has the ability to crawl into land and breathe air using a primitive lung which allows it to survive in stagnant water. The young of the giant snakehead is red in color with orange and black lateral stripes. The juveniles are sold in the ornamental fish trade as red/redline snakeheads. Mature snakehead loses its stripes and redness and develop a bluish black and white pattern on the upper body. 

          The giant snakehead is a voracious predator that will chase and eat anything that fits in the mouth. (Wikipedia) 
          <br>
          <strong>PERCEIVED THREAT:</strong> 
          
          Pantabangan Dam 
          
          <br>
          <strong> SIGHTINGS:</strong> 
          
          Damage to prey population, Disruption of ecosystem.  
        </p>
        </div>
      </div>

      
      <footer class="modal-footer">
        
      </footer>
    </div>
  </div>

  <div id="id03" class="modal">

    <div class="modal-content">

      <header class="modal-header"> 
        <span onclick="document.getElementById('id03').style.display='none'"class="button">&times;</span>
        
      </header>
      
      <div class="modal-body">
        <div class="popup__left">
        <img src="/static/invasive/img/jaguar.png"/>
        </div>
        <div class="popup_right">
        <p class="popup__text">
          <strong>Parachromis manguensis</strong> also known as Jaguar guapote is a native of Central America and has been introduced in the Philippines in 1990 for ornamental fish trade (Agassen, et. al, 2006). It is a large, robust cichlid, displaying a shade of yellow/bronze scattered throughout the body with vivid black spots which are seen all along the flanks and gill plates. A series of several large black dots then run horizontally along the lateral line area. The fins are often dark to black coloration, especially when in spawning. 

         Jaguar guapote was described as the most predatory of all guapotes because it is highly piscivorous and aggressive; and may cause serious damage to native fish communities through direct predation. Its diet consists mainly of small fishes and macro-invertebrates. 

         It prefers turbid, eutrophic lakes with mud bottom substrate but can also be found in areas with sandy bottom covered with plant debris. It has become abundant in Taal lake because of the luxuriant growth of aquatic vegetation which serves as spawning ground; abundant food and favorable environment. (Wikipedia) 
          <br>
          <strong>PERCEIVED THREAT:</strong> 
          
          Damage to prey population, Disruption of ecosystem. 
          
          <br>
          <strong> SIGHTINGS:</strong> 
          
          Taal Lake   
        </p>
        </div>
      </div>

      
      
      <footer class="modal-footer">
        
      </footer>
    </div>
  </div>
  <div id="id04" class="modal">

    <div class="modal-content">

      <header class="modal-header"> 
        <span onclick="document.getElementById('id04').style.display='none'"class="button">&times;</span>
        
      </header>
      
      <div class="modal-body">
        <div class="popup__left">
        <img src="/static/invasive/img/walking_catfish.png"/>
        </div>
        <div class="popup_right">
        <p class="popup__text">
          <strong> Clarias batrachus </strong> also known as Walking Catfish is a species of freshwater air breathing catfish native to Southeast Asia, but also introduced outside its native range where it is considered an invasive species. It has the ability to use its pectoral fins to keep it upright to “walk” across dry land, to find food or sustainable environment. 
        The walking catfish has an elongated body shape, and reaches almost 0.5 m (1.6 ft) in length and 1.2 kg (2.6 lbs) in weight. Often covered laterally in small white spots, the body is mainly colored gray or grayish brown has a long-based dorsal and anal fins as well as several pairs of sensory barbels. The skin is scaleless, but covered with mucus, which protects the fish when it is out of water. 
        Walking Catfish possesses a large accessory breathing organ which enable them to breath atmospheric oxygen.
         <br>
          <strong>PERCEIVED THREAT:</strong> 
          
          Genetic contamination, Erode Biodiversity
          
          <br>
          <strong> SIGHTINGS:</strong> 
          
        </p>
        </div>
      </div>

      
      
      <footer class="modal-footer">
        
      </footer>
    </div>
  </div>

  <div id="id05" class="modal">
    <div class="modal-content">
      <header class="modal-header"> 
        <span onclick="document.getElementById('id05').style.display='none'"class="button">&times;</span>
        
      </header>
      
      <div class="modal-body">
        <div class="popup__left">
        <img src="/static/invasive/img/Janitor_fish.png"/>
        </div>
        <div class="popup_right">
        <p class="popup__text">
          The Pterygoplichthys disjunctivus also known as Janitor fish is famous among tropical fish hobbyist because of its ability to keep the aquarium clean by sucking the algae adhering to the aquarium glass. It is a native of the freshwater, brackishwater and marine water of South America. 
          The body is elongated, dark brown covered with bony plates. The head is large. has a bushy face decorated with horns. 
          It is herbivorous, feeding on the naturally growing algae detritus and small crustaceans. 
          The natural spawning grounds is characterized by moving water and the presence of narrow caves and hiding places. The fish is a nocturnal spawner, depositing an estimated 150 large orange-yellow eggs usually attached to the upper side of the crevice. 
          In the tropical fish trade, it is famous for its ability to keep the aquarium free of algae.
         <br>
          <strong>PERCEIVED THREAT:</strong> 
          
          Destruction or tearing of nets due to the hard body plate, reduce the income from fishing due to its predominance practically no market value in the catch of the fish corral, destruction of the ecosystem due to benthic foraging; and the destruction of the physical habitat brought about by the breeding behavior of the fish. Numerous Tunnels have been exposed in the river bed of Siniloan, Laguna and similar tunnels are even found inland towards the irrigation canals. 
          
          <br>
          <strong> SIGHTINGS:</strong> 
          Laguna de Bay, Agusan Marsh, Pulangui Reservoir
        </p>
        </div>
      </div>

      <footer class="modal-footer">
        
      </footer>
    </div>
  </div>

  <div id="id05" class="modal">
    <div class="modal-content">
      <header class="modal-header"> 
        <span onclick="document.getElementById('id05').style.display='none'"class="button">&times;</span>
        
      </header>
      
      <div class="modal-body">
        <div class="popup__left">
        <img src="/static/invasive/img/Janitor_fish.png">
        </div>
        <div class="popup_right">
        <p class="popup__text">
         <strong> Pterygoplichthys disjunctivus</strong> also known as Janitor fish is famous among tropical fish hobbyist because of its ability to keep the aquarium clean by sucking the algae adhering to the aquarium glass. It is a native of the freshwater, brackishwater and marine water of South America. 
          The body is elongated, dark brown covered with bony plates. The head is large. has a bushy face decorated with horns. 
          It is herbivorous, feeding on the naturally growing algae detritus and small crustaceans. 
          The natural spawning grounds is characterized by moving water and the presence of narrow caves and hiding places. The fish is a nocturnal spawner, depositing an estimated 150 large orange-yellow eggs usually attached to the upper side of the crevice. 
          In the tropical fish trade, it is famous for its ability to keep the aquarium free of algae.
         <br>
          <strong>PERCEIVED THREAT:</strong> 
          
          Destruction or tearing of nets due to the hard body plate, reduce the income from fishing due to its predominance practically no market value in the catch of the fish corral, destruction of the ecosystem due to benthic foraging; and the destruction of the physical habitat brought about by the breeding behavior of the fish. Numerous Tunnels have been exposed in the river bed of Siniloan, Laguna and similar tunnels are even found inland towards the irrigation canals. 
          
          <br>
          <strong> SIGHTINGS:</strong> 
          Laguna de Bay, Agusan Marsh, Pulangui Reservoir
        </p>
        </div>
      </div>

      <footer class="modal-footer">
        
      </footer>
    </div>
  </div>

  <div id="id06" class="modal">
    <div class="modal-content">
      <header class="modal-header"> 
        <span onclick="document.getElementById('id06').style.display='none'"class="button">&times;</span>
        
      </header>
      
      <div class="modal-body">
        <div class="popup__left">
        <img src="/static/invasive/img/gloria.png"/>
        </div>
        <div class="popup_right">
        <p class="popup__text">
          The Sarotherodon melanotheron also known as Black chin tilapia thrives in brackishwater lagoons and estuaries surrounded by dense mangrove trees where freshwater rivers meet the ocean. It is native to the freshwater lagoons and brackish or slightly salty waters of West Africa from Senegal to Southern Cameroon. 
          Blackchin tilapia is named for the patches of black melanic areas on the neck and throat. The lower body is usually pale blue and the back is metallic golden yellow or orange. The operculum is gold in males; and transparent and appears a deep mauve in females due to the blood that flows through the gills. 
          It feeds primarily on filamentous algae, microorganisms and organic matter from dead and decomposing plants and animals. The juveniles are more carnivorous than adults and includes small crustaceans and zooplankton in their diet. 
          Blackchin Tilapia can tolerate salinity from 0 to 45 ppt and has proliferated in the coastal waters of Manila Bay. It is potentially invasive and could disrupt the Manila Bay ecosystem.
         <br>
          <strong>PERCEIVED THREAT:</strong> 
          
          Genetic contamination, Erode Biodiversity 
          <br>
          <strong> SIGHTINGS:</strong> 
          Manila Bay  
        </p>
        </div>
      </div>

      <footer class="modal-footer">
        
      </footer>
    </div>
  </div>
  
  <!-- Bootstrap core JavaScript -->
  <script src="/static/invasive/jquery/jquery.min.js"></script>
  <script src="/static/invasive/javas/bootstrap.bundle.min.js"></script> 

 <script>
  window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
 </script>
  
</body>

</html>
