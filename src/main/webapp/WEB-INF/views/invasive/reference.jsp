<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="Programmer" content="Francisco P. Vejerano - BFAR">
    <meta name="Web Designer" content="Mary Alaine B. Albaladejo - BFAR">
    <meta name="Description" content="BFAR Invasive Fishes On-line Information System">
    <meta name="author" content="">
  
    <title>INVASIVE FISHES</title>
  
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
    
  
    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">
    
  
  </head>

<body>
<img class="img-responsive img-center" src="img/sampleInvasive.png"  height="120" width="100%">
  <!-- Navigation -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="index.html">INVASIVE FISHES</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="About.html">References</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="services.html">Activities</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact.html">Announcement</a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Gallery
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
              
              <a class="dropdown-item" href="#">Photo Gallery</a>
              <a class="dropdown-item" href="#">Video Gallery</a>
        
            </div>
          </li>
       
          
          <li class="nav-item">
            <a class="nav-link" href="#">IEC Materials</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="#">Map</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="#">Events</a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              LOGIN
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
              
              <a class="dropdown-item" href="login.html">LOGIN</a>
            </div>
          </li>


        </ul>

      </div>
    </div>
  </nav>

  

  <!-- Page Content -->
 
<!-- /.row -->

    <hr>

    <!-- Call to Action Section -->
    <div class="row mb-4">
      <!-- <div class="col-md-8">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
      </div> -->
      <div class="col-md-4">
        <a class="btn btn-lg btn-secondary btn-block" href="video.html">VIDEO</a>
      </div>
    </div>

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy;2022 All rights reserved- Bureau of Fisheries and Aquatic Resources</p>
      
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
