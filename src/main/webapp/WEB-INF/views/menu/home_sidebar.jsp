 
 <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<nav id="sidebar">
<div class="custom-menu">
<button type="button" id="sidebarCollapse" class="btn btn-primary">
<i class="fa fa-bars"></i>
<span class="sr-only">Toggle Menu</span>
</button>
</div>

<ul class="list-unstyled components mb-5">
       <li>
        <label class="checkbox-inline">
     	 <input type="checkbox" name="page" id="fishsanctuaries" value="fishsanctuaries">
		<img src="/static/images/iconCircle/fishsanctuary.png" style="width: 40px; height: 40px;"/>
     	<span class="text">Fish Sanctuary</span>
    </label> 
        </li>
      <li>   
       <label class="checkbox-inline">
      	<input type="checkbox" name="page" id="fishprocessingplants" value="fishprocessingplants">
		<img src="/static/images/iconCircle/fishprocessing.png" style="width: 40px; height: 40px;"/>
     	<span class="text">Fish Processing</span>
    </label> 
      </li>
      <li>
       <label class="checkbox-inline">
      	<input type="checkbox" name="page" id="fishlanding" value="fishlanding">
		<img src="/static/images/iconCircle/fish-landing.png" style="width: 40px; height: 40px;"/>
      	<span class="text">Fish Landing</span>
    </label>
      </li>
      <li>
      <label class="checkbox-inline">
      	<input type="checkbox" name="page" id="FISHPORT" value="FISHPORT">
		<img src="/static/images/iconCircle/fishport.png" style="width: 40px; height: 40px;"/>
     	<span class="text"> Fish Port</span>
    </label>
      </li>
      <li>
      <label class="checkbox-inline">
      	<input type="checkbox" name="page" id="fishpen" value="fishpen">
		<img src="/static/images/iconCircle/fishpen.png" style="width: 40px; height: 40px;"/>
      	<span class="text">Fish Pen</span>
    </label>
      </li>
      <!-- <li><a href="/create/fishcages"> -->
      <li>
      <label class="checkbox-inline">
      	<input type="checkbox" name="page" id="fishcage" value="fishcage">
		<img src="/static/images/iconCircle/fishcage.png" style="width: 40px; height: 40px;"/>
      	<span class="text">Fish Cage</span>
    </label>
      </li>
      <li>
      
       <a href="#pageHatchery" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/hatcheries.png" style="width: 40px; height: 40px;"/>
     <span class="text"> Hatchery<i class="fas fa-list"></i></span></a>
        <ul class="collapse list-unstyled" id="pageHatchery">

          <li>
          <label class="checkbox-inline">
      			<input type="checkbox" name="page" id="hatchery" value="hatchery">Map Legislated
    	  </label>
         <!--  <a href="/home/hLegislated/legislated">Map Legislated</a> -->
          </li>
          <li>
          <label class="checkbox-inline">
      			<input type="checkbox" name="page" id="hatchery" value="hatchery">Map Non-Legislated
    	  </label>
          <!-- <a href="/home/hLegislated/nonLegislated">Map Non-Legislated</a> -->
          </li>
         </ul>
      </li>
      <li>
       <label class="checkbox-inline">
      	<input type="checkbox" name="page" id="iceplantorcoldstorage" value="iceplantorcoldstorage">
		<img src="/static/images/iconCircle/iceplant.png" style="width: 40px; height: 40px;"/>
      	<span class="text">Cold Storage</span>
    </label>
      </li>
      <li>
       <label class="checkbox-inline">
      	<input type="checkbox" name="page" id="market" value="market">
		<img src="/static/images/iconCircle/market.png" style="width: 40px; height: 40px;"/>
      	<span class="text">Market</span>
    </label>
      </li>
      <li>
      <label class="checkbox-inline">
      	<input type="checkbox" name="page" id="schooloffisheries" value="schooloffisheries">
		<img src="/static/images/iconCircle/schoolof-fisheries.png" style="width: 40px; height: 40px;"/>
      	<span class="text">School of Fisheries</span>
    </label>
      </li>
      <li>
       <label class="checkbox-inline">
      	<input type="checkbox" name="page" id="fishcorals" value="fishcorals">
		<img src="/static/images/iconCircle/fishcorral.png" style="width: 40px; height: 40px;"/>
      	<span class="text">Fish Corals</span>
    </label>
      </li>
      <li>
      <label class="checkbox-inline">
      	<input type="checkbox" name="page" value="seagrass">
		<img src="/static/images/iconCircle/seagrass.png" style="width: 40px; height: 40px;"/>
      	<span class="text">Sea Grass</span>
    </label>
      </li>
      <li>
      <label class="checkbox-inline">
      	<input type="checkbox" name="page" id="seaweeds" value="seaweeds">
		<img src="/static/images/iconCircle/seaweeds.png" style="width: 40px; height: 40px;"/>
      	<span class="text">Sea Weeds</span>
    </label>
      </li>
      <li>
      <label class="checkbox-inline">
      	<input type="checkbox" id="mangrove" value="mangrove">
		<img src="/static/images/iconCircle/mangrove.png" style="width: 40px; height: 40px;"/>
      	<span class="text">Mangrove</span>
    </label>
      </li>
      <li>
      <label class="checkbox-inline">
      	<input type="checkbox" name="page" id="mariculturezone" value="mariculturezone">
		<img src="/static/images/iconCircle/mariculturezone.png" style="width: 40px; height: 40px;"/>
      	<span class="text">Mariculture Park</span>
    </label>
      </li>
      <li>
      <label class="checkbox-inline">
      	<input type="checkbox" name="page" id="lgu" value="lgu">
		<img src="/static/images/iconCircle/lgu.png" style="width: 40px; height: 40px;"/>
      	<span class="text">LGU</span>
    </label>
      </li>
      <li>
       <label class="checkbox-inline">
      	<input type="checkbox" name="page" id="trainingcenter" value="trainingcenter">
		<img src="/static/images/iconCircle/fisheriestraining.png" style="width: 40px; height: 40px;"/>
     	 <span class="text">Training Center</span>
    </label>
      </li>

       <li><a href="/login"><i class="fa fa-fw fa-user"></i>LOGIN</a></li>
       


</ul>

</nav>
