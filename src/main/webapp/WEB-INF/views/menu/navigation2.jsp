  <nav class="navbar navbar-inverse navbar-fixed-top" id="modal_content">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <!--<a class="navbar-brand" href="#">Logo</a>  -->
    </div>
   
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="/frgis/users/resourceMaplist">Home</a></li>
      <!--  <li><a href="#">About</a></li>
        <li><a href="#">Contact</a></li>
        <li><a href="#" id="printForms" data-toggle="modal" data-target="#myModalForms" data-backdrop="static" data-keyboard="false">Forms</a>
        </li> -->
       <li>
    
       </li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Resources<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="fishsanctuaries">Fish Sanctuary</a></li>
          <li><a href="fishprocessingplants">Fish Processing Plants</a></li>
          <li><a href="fishlanding">Fish Landing</a></li>
          <li><a href="FISHPORT">FishPort</a></li>
          <li><a href="fishpen">FishPen</a></li>
          <li><a href="fishcage">Fish Cage</a></li>
          <li><a href="fishpond">Fish Pond</a></li>
          <li><a href="hatchery">Hatchery</a></li>
          <li><a href="iceplantorcoldstorage">Ice Plant/Cold Storage</a></li>
          <li><a href="market">Market</a></li>
          <li><a href="schooloffisheries">School of Fisheries</a></li>
          <li><a href="fishcorals">Fish Corals</a></li>
          <li><a href="seagrass">Seagrass</a></li>
          <li><a href="seaweeds">Seaweeds</a></li>
          <li><a href="mangrove">Mangrove</a></li>
          <li><a href="mariculturezone">Mariculturezone</a></li>
          <li><a href="lgu">PFO</a></li>
          <li><a href="trainingcenter">Training Center</a></li>
        </ul>
      </li>
   
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">
        <span><strong>${loggedinuser}</strong>, Welcome to FRGIS</span>
        </a>
        </li>
       
        <li><a href="/frgis/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
        
      </ul>
    </div>
  </div>
</nav>