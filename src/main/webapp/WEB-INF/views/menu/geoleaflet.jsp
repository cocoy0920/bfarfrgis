<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
<head>
	<title>Checkbox/filter Example</title>
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css" />
	<script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	
<style>
#wrapper {
  margin: 0 auto;
  width: 100%;
}

#panel{
 float: left;
  width: 100%;
  height:50px;
background-color: #175B81;
color:white;
}

#map {
  float: left;
  width: 75%;
  height:700px;
}

.sidebar {
  float: left;
  width: 20%;
  height:600px;
   background-color: white;
   color: #175B81;
}

#footer {
float: left;
background-color: #175B81;
color: white;
  width: 100%;
  height:25px
}

.text-labels     {
	font-weight: bold;
    color:#006400;
	font-family: "Arial Black"
	font-size: 20px ;
    line-height:25px;
    text-align:center;
   }
   
table, th, td 
	{
		margin:0px 5px;
		/*border:solid 1px #333; */
		padding:2px 4px;
		font:10px Verdana;
	}
th {
		font-size:14px;
		font-weight:bold
	}
td {
		font-size:14px;
		font-weight:normal
	}
tr {
		background: white;
	}
	
tbody  tr:hover {
		background: yellow;
	}
table {
		border-collapse: collapse;
	}  
.table a
{
    display:block;
    text-decoration:none;
}
#f1{
	font-weight:bold
}
.p2 {
    font-size: 24px;
	font-weight:bold;
    position: relative;
    top: 11px;
	left: 20px;
	}
	
#ckboxes {	/* pushes checkbox a little from sidebar left edge */
	padding-left: 10px;
}


img {  /* Images are all different sizes, this makes them the same */
  width:100px;
  height:100px;
  object-fit:scale-down;
}

	.box{
		width:15px;
		height:15px;
		border-radius:25px; /* makes a circle from a box */
    	 display: inline-block;
		}
	 
	.red{
		background:red;
	} 
	.green{
		background:green;
	}
	.blue{
		background:blue;
	} 
	.orange{
		background:orange;
	}
	#ball{	/* pushes the circle right 80 px. */
		padding-left: 80px;
	}
</style>
</head>
<body>

<div id="wrapper">
	<div id="panel"><span class="p2">Demo Leaflet Sidebar, Checkbox, and filter</span> </div>
  <div id="map">  </div>
  
  <div class="sidebar">
<p style="text-align:center; font-size: 24px; color:#175B81; font-weight:bold; "><span id="sport">Baseball</span></p>	 
			<table  style="width:90%; ">
			<tr>
			<td id='pict' ALIGN='center'; >	</td>
			<td id='pict2' ALIGN='center'; ></td>
			</tr>
			</table></br> 
			
			<table  style="width:90%">
			  <tr>
              <th align="left">League</th>
              <td id='f1'></td>
              </tr> <tr>
              <th align="left">Team</th>
              <td id='f2'></td></tr>
              <tr>
			 <th align="left">Address</th>
              <td id='f3'></td>
              </tr> <tr>
			  <th align="left">Stadium</th>
              <td id='f4'></td>
              </tr> <tr>
              <th align="left">Capacity</th>
              <td id='f5'></td></tr>
              <tr>
			  <th align="left">Website</th>
              <td id='f6'><a href="#"></a></td>
               </table>
			   
			   <br><hr>
				<div id="ckboxes">
				 
							
				<label class="container"> <!-- need a name property for check/uncheck funtion to work -->
				
						<input type="checkbox"  id="fishs" class="fishs" name="fishs" value="fishs"><b>Fish Sanctuaries</b><img alt="" src="/frgis/static/images/iconCircle/fishsanctuary.png" style="width: 20px;height: 20px"><br><br>						
				</label>	
				<label class="container"> <!-- need a name property for check/uncheck funtion to work -->
						<input type="checkbox"  id="fishpen" class="fishpen" name="fishpen" value="fishpen""><b>Fish Pen</b><img alt="" src="/frgis/static/images/iconCircle/fishsanctuary.png" style="width: 20px;height: 20px"><br><br>						
					</label>	
				</div>
  </div>
  <div id="footer">  </div>
  
</div>
<script>
 // Here is the url path to my GeoJSON file, 

var getMap = 'http://localhost:8083/geoserver/wms?';
    	var mapLayer = "barangay:Barangays";
     	//var map = L.map('mapContainer').setView([${set_center}], 7),
     	var map = L.map('map').setView([16.86401,120.43213], 5),
		
    	vector = L.geoJson().addTo(map);
    	 var wmsLayer = L.tileLayer.wms(getMap, {
    		layers: mapLayer,
    		attribution: "FIMC",
    		transparent: true,
    		 format:'image/png',
    		 minZoom: 5,
    		maxZoom: 15
		}).addTo(map); 
		
		var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});
		
	//var map = L.map('map').setView([39.0, -98.26], 4); 

	//var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
	//			attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'});
	
	// https: also suppported.
	//var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
	//	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
	//});
	
	// https: also suppported.
	//var Esri_WorldGrayCanvas = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
	//	attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ',
	//	maxZoom: 16
	//}).addTo(map);;
	
	//var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
	//maxZoom: 18,
	//attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
	//});


	// Set function for color ramp, used for both layers
	function getColor(league){
		return league == 'fishsanctuaries' ? '/frgis/static/images/pin/20x34/fishsanctuary.png' :
			   league == 'AL' ? 'red' :
			   league == 'AFC' ? 'orange' :
			   league == 'NFC' ? 'green' :
								'white';
	       }	
	
	// Set style function that sets fill color property
	function style(feature) {
		return {
			fillColor: setColor(feature.page),
			fillOpacity: 0.5,
			weight: 2,
			opacity: 1,
			color: '#ffffff',
			dashArray: '3'
		};
	}

/*	var fishsanctuaries;	
 $.getJSON("data", function(data) {		
for ( var i = 0; i < data.length; i++) {
		var obj = data[i];
		var role = 	'ROLE_USER';	
			lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			image = obj.image;
			var latlon = lat + ',' + lon;
			uniqueKey =  obj.uniqueKey;     
			var imageview = "../../../../frgis_captured_images/" + image;
		    if (role == 'ROLE_USER') {
						var edit = '<div class="fishsanctuaries_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" ><img src="'+imageview+'"></img></a></div>';
			} else {
						var edit = '';
			}

				var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});
				var fishSancturiesGroup = new Array();
				  var blueLayer = new L.LayerGroup();	
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    var LamMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    					console.log(this.getLatLng());
    					onMapClick(this.getLatLng());
    					});
        		   
        	fishsanctuaries	 =   blueLayer.addLayer(LamMarker);
	}
});
*/

     function refreshRecord(value,link,status)
    {
console.log("value: " + value);
console.log("link: " + link);
console.log("status: " + status);
var role = 	'ROLE_USER';
       data = value;
		page = link;

			if(page == "fishsanctuaries"){	
			//viewListOnMap.js				
			fishsanctuariesGetListMap(data,role);  	
				
 	 		}
 	 		if(page == "fishprocessingplants"){
 	 			fishprocessingplantsGetListMap(data,role); 	
      				
 	 		}
		 	if(page == "fishlanding"){
				fishlandingGetListMap(data,role);         		
 	 		}   
 	 	 	if(page == "fishpen"){
 	 	 		fishpenGetListMap(data,role);
 	 		} 

			if(page == "fishcage"){
				fishcageGetListMap(data,role);
			} 
 	 	 	if(page == "fishpond"){
 				fishpondGetListMap(data,role);
 			} 	 	        		
        	if(page == "hatchery"){
 	 			hatcheryGetListMap(data,role);
 	 		}
 	 	 	if(page == "iceplantorcoldstorage"){
 	 			iceplantorcoldstorageGetListMap(data,role);
 	 		}
 	 		if(page == "fishcorals"){
 	 			fishcorralsGetListMap(data,role);
 	 		}  
 	 		if(page == "FISHPORT"){
 	 			FISHPORTGetListMap(data,role);
 	 		}   
 	 		if(page == "marineprotected"){
 	 			marineprotectedGetListMap(data,role);
 	 		} 
 	 		if(page == "market"){ 
 	 			marketGetListMap(data,role);
 	 		} 
 	 		if(page == "schooloffisheries"){ 
 	 			schooloffisheriesGetListMap(data,role);
 	 		} 
 	 		if(page == "seagrass"){ 	 	
 	 			seagrassGetListMap(data,role);
 	 		} 
 	 		if(page == "seaweeds"){
        			seaweedsGetListMap(data,role);
 	 		}
 	 		if(page == "mangrove"){      		
 	 			mangroveGetListMap(data,role);
 	 		}
 	 		if(page == "lgu"){        		
        		lguGetListMap(data,role);
 	 		}
 	 		if(page == "trainingcenter"){
        		trainingcenterGetListMap(data,role);
 	 		}
 	 		if(page == "mariculturezone"){
 	 			mariculturezoneGetListMap(data,role);
 	 		}
  
	//}

       	 
    }


// click marker
  var clickmark;

  // When you click on a circle, it calls the onMapClick function and passes the layers coordinates.
  // I grab the coords which are X,Y, and I need to flip them to latLng for a marker,  
  function onMapClick(coords) {
		console.log(coords + " " + coords.lat);
		//if prior marker exists, remove it.
		if (clickmark != undefined) {
		  map.removeLayer(clickmark);
		};
		 $.getJSON("data", function(data) {	
			 for ( var i = 0; i < data.length; i++) {
				var obj = data[i];
				if(coords.lat == obj.lat && coords.lng == obj.lon){
					console.log("true");
					$('#f5').html(obj.html);
				}
				}	
		 });
		
  
		 clickmark = L.circleMarker([coords.lat,coords.lng],{
			radius: 20,
			color: "yellow",
			fillColor:  "yellow",
			fillOpacity: 0.8}
		 ).addTo(map);
	}

// end of code for click marker.



//Add layer control
//var baseMaps = {
//    "Open Street Map": osm,
//    "Imagery":Esri_WorldImagery,
//	"Gray":Esri_WorldGrayCanvas,
//	"OSM B&W":OpenStreetMap_BlackAndWhite
//};

//var overlayMaps = {};
//L.control.layers(baseMaps, overlayMaps).addTo(map);

 </script>
<script>

      $(document).ready(function(){   
     
	  var trigger = $('.nav ul li input'); 
      trigger.on('click', function(){
    	  
          var $this = $(this),
            target = $this.data('target');       
          	 	 
        		 
       		   $.get("map/" + target, function(content, status){
        	 			console.log(content + " THIS IS BULLSHIT!!!!");
         			refreshRecord(content,target,"new");
    	
    		  });
    	  
      	

          return false;
        });
        
     });

</script>
<script>
 $(document).ready(function(){  
 
 function swapper(target) {

  $.get("map/" + target, function(content, status){
        	 			console.log(content + " THIS IS BULLSHIT!!!!");
         			refreshRecord(content,target,"new");
    	
    		  });
}
$('#fishs').click(function() {

 	if ($('#fishs').is(":checked")){
   		//var el = document.getElementById('fishs');
		//	if(el){
  		//		el.addEventListener('click', swapper("fishsanctuaries"), false);
		//	}
		swapper("fishsanctuaries");
	}else{
	markerFishSancturiesDel();
		}
});
$('#fishpen').click(function() {

 	if ($('#fishpen').is(":checked")){
   		//var el = document.getElementById('fishpen');
		//	if(el){
  		//		el.addEventListener('click', swapper("fishpen"), false);
		//	}
		swapper("fishpen")
	}else{
	markerFishPenDel();
	}
});

/* var el = document.getElementById('fishs');
if(el){
  el.addEventListener('click', swapper("fishsanctuaries"), false);
}
if(!el){
	markerFishSancturiesDel();
}
*/


// Handles the check boxes being turned on/off
/*document.querySelector("input[name=fishsanct]").addEventListener('change', function() {
                if(this.checked) map.addLayer(fishsanctuaries)
                  else map.removeLayer(fishsanctuaries)
				  	if (clickmark != undefined) {  //i.e. if it exists...
					//function all to remove the yellow select circle, could call function to clear table from here.
						map.removeLayer(clickmark);
					};
                })
*/				
/*document.querySelector("input[name=fishspen]").addEventListener('change', function() {
                if(this.checked) map.addLayer(fishspen)
                  else map.removeLayer(fishspen)
				  	if (clickmark != undefined) {
						map.removeLayer(clickmark);
					};
                })
*/
	 });
</script>
<script type="text/javascript" src="/frgis/static/js/form/viewListOnMap.js"></script>
<script src="/frgis/static/js/form/deleteMapList.js" type=" text/javascript"></script>

</body>
</html>