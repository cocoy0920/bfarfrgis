 
 <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<nav id="sidebar">
<div class="custom-menu">
<button type="button" id="sidebarCollapse" class="btn btn-primary">
<i class="fa fa-bars"></i>
<span class="sr-only">Menu</span>
</button>
</div>
<div class="p-4">
<div class="sidebar-header">
      <img class=" img img-fluid" alt="" src="/static/images/FRGIS-LOGO.png" style="height: 100px; width: 100px">
    </div>
 </div>  
    <hr> 
<ul class="list-unstyled components mb-5">
    <li>
       <a href="#pageCreate" data-toggle="collapse"  aria-expanded="false">
     	 <span class="text-white"><i class="fas fa-list"></i>&nbsp;FMA</span>
       </a>
        <ul class="collapse list-unstyled" id="pageCreate">
        <li><input id="FMA" type="checkbox">Select all</li>
      	<li class="dropdown-item">
      		<input type="checkbox" id="FMA-1" value="FMA-1" name="FMA" class="FMA">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">FMA 1</span>
       	</li>
       <li class="dropdown-item">
      		<input type="checkbox" id="FMA-2" value="FMA-2" name="FMA" class="FMA">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">FMA 2</span>
       	</li>

      <li class="dropdown-item">
      		<input type="checkbox" id="FMA-3" value="FMA-3" name="FMA" class="FMA">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">FMA 3</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="FMA-4" value="FMA-4" name="FMA" class="FMA">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">FMA 4</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="FMA-5" value="FMA-5" name="FMA" class="FMA">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">FMA 5</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="FMA-6" value="FMA-6" name="FMA" class="FMA">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">FMA 6</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="FMA-7" value="FMA-7" name="FMA" class="FMA">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">FMA 7</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="FMA-8" value="FMA-8" name="FMA" class="FMA">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">FMA 8</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="FMA-9" value="FMA-9" name="FMA" class="FMA">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">FMA 9</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="FMA-10" value="FMA-10" name="FMA" class="FMA">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">FMA 10</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="FMA-11" value="FMA-11" name="FMA" class="FMA">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">FMA 11</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="FMA-12" value="FMA-12" name="FMA" class="FMA">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">FMA 12</span>
       	</li>
       	
       	
      </ul>
      </li>
      <li>
       <a href="#major_fg" data-toggle="collapse"  aria-expanded="false">
     	 <span class="text-white"><i class="fas fa-list"></i>&nbsp;24 Major Fishing Grounds</span>
       </a>
        <ul class="collapse list-unstyled" id="major_fg">
        <li><input id="major24_fg" type="checkbox">Select all</li>
      	<li class="dropdown-item">
      		<input type="checkbox" id="checked_lingayen_gulf" value="LINGAYEN GULF" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">1 - LINGAYEN GULF</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_manila_bay" value="Manila Bay" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">2 - Manila Bay</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_batangas_coast" value="Batangas Coast" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">3 - Batangas Coast</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_tayabas_bay" value="Tayabas Bay" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">4 - Tayabas Bay</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_wpalawan_waters" value="West Palawan waters" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">5 - West Palawan waters</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_cuyo_pass" value="Cuyo Pass" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">6 - Cuyo Pass</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_wsulu_sea" value="West Sulu Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">7 - West Sulu Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_ssulu_sea" value="South Sulu Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">8- South Sulu Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_esulu_sea" value="East Sulu Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">9 - East Sulu Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_moro_gulf" value="Moro Gulf" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">10 - Moro Gulf</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_davao_gulf" value="Davao Gulf" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">11 - Davao Gulf</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_samar_gulf" value="Samar Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">12 - Samar Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_sibuyan_gulf" value="Sibuyan Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">13 - Sibuyan Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_camotes_gulf" value="Camotes Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">14 - Camotes Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_visayan_gulf" value="Visayan Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">15 - Visayan Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_guimaras_gulf" value="Guimaras Strait" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">16 - Guimaras Strait</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_bohol_gulf" value="Bohol Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">17 - Bohol Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_ragay_gulf" value="Ragay Gulf" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">18 - Ragay Gulf</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_leyte_gulf" value="Leyte Gulf" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">19 - Leyte Gulf</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_lagonoy_gulf" value="Lagonoy Gulf" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">20 - Lagonoy Gulf</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_lamon_bay" value="Lamon Bay" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">21 - Lamon Bay</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_casiguran_sound" value="Casiguran Sound" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">22 - Casiguran Sound</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_palanan_bay" value="Palanan Bay" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">23 - Palanan Bay</span>
       	</li>
       <li class="dropdown-item">
      		<input type="checkbox" id="checked_babuyan_channel" value="Babuyan Channel" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">24 - Babuyan Channel</span>
       	</li>
       	</ul>
       	</li>
       <li class="dropdown-item">
      	 		<input type="checkbox" id="checked_municipal_waters" value="Municipal Water ">
      	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Sanctuary"/>
     	 		<span class="text-white">Municipal Waters</span>
	  </li>
	  <!-- <li class="dropdown-item">
      	 		<input type="checkbox" id="checked_fishing_grounds" value="Municipal Water ">
      	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Sanctuary"/>
     	 		<span class="text-white">24 Major Fishing Grounds</span>
	  </li> -->
<!--        <li>      
      <a href="#provinceList" data-toggle="collapse" aria-expanded="false">
      <span class="text">Filter by Province<i class="fas fa-list"></i></span></a>       
			<select name="provincemap"  id="provincemap" class="form-control provincemap"></select>															
      </li>
       -->
       
       <li>
       <a href="#bfarinfra" data-toggle="collapse"  aria-expanded="false">
     	 <span class="text-white"><i class="fas fa-list"></i>&nbsp;BFAR INFRASTRUCTURE</span>
       </a>
        <ul class="collapse list-unstyled" id="bfarinfra">
        <li><input id="infrastructure" type="checkbox">Select all</li>
      	<li class="dropdown-item">
      		<input type="checkbox" id="check_nationalcenter" value="nationalcenter" name="infrastructure" class="infrastructure">
       			<span class="text-white">National Center</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="check_regionaloffices" value="regionaloffices" name="infrastructure" class="infrastructure">
       			<span class="text-white">Regional Offices</span>
       	</li>
       	<li  class="dropdown-item">
       		<input type="checkbox" id="checked_lgu" value="lgu" name="infrastructure" class="infrastructure">
        	<!-- <img src="/static/images/iconCircle/lgu.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="PFO"/>
       		 --><span class="text-white">Provincial Fisheries Office</span>
      	</li>
      	<li class="dropdown-item">
      		<input type="checkbox" id="checked_training" value="trainingcenter" name="infrastructure" class="infrastructure">
       		<!-- <img src="/static/images/iconCircle/fisheriestraining.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Training Center"/>
       		 --><span class="text-white">Training Center</span>
      	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="check_tos" value="technology_outreach_station" name="infrastructure" class="infrastructure">
       			<span class="text-white">Technology Outreach Station</span>
       	</li>
       	<li class="dropdown-item">
      <input type="checkbox" id="checked_schoolOfFisheries" value="schoolOfFisheries" name="infrastructure" class="infrastructure">
       <img src="/static/images/iconCircle/schoolof-fisheries.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="School of Fisheries"/>
       <span class="text-white">Fisheries School</span>
      </li>
       	</ul>
       	</li>
       	
       	<li>
       <a href="#aquafarms" data-toggle="collapse"  aria-expanded="false">
     	 <span class="text-white"><i class="fas fa-list"></i>&nbsp;AQUAFARMS</span>
       </a>
        <ul class="collapse list-unstyled" id="aquafarms">
        
        <li><input id="aquafarm" type="checkbox">Select all</li>
        
        <li class="dropdown-item">
      		<input type="checkbox" id="checked_fispond" value="fishpen" class="aquafarm">
      		<img src="/static/images/iconCircle/fishpen.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Pen"/>
       		<span class="text-white">FISH POND</span>
      </li>
      	<li class="dropdown-item">
      <input type="checkbox" id="checked_fishpen" value="fishpen" class="aquafarm">
      <img src="/static/images/iconCircle/fishpen.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Pen"/>
       <span class="text-white">FISH PEN</span>
      </li>
       <li class="dropdown-item">
       <input type="checkbox" id="checked_fishcage" value="fishcage" class="aquafarm">
       <img src="/static/images/iconCircle/fishcage.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Cage"/>
       <span class="text-white">FISH CAGE</span>
      </li>	
      <li class="dropdown-item">
      				<input type="checkbox" id="checked_fishcoral" value="fishcoral" class="aquafarm">
      				<img src="/static/images/iconCircle/fishcorral.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Corals"/>
       				<span class="text-white">FISH CORRAL</span>
      			</li>
      </ul>
      </li>
      
      <li>
       <a href="#mariculture" data-toggle="collapse"  aria-expanded="false">
     	 <span class="text-white"><i class="fas fa-list"></i>&nbsp;MARICULTURE PARK</span>
       </a>
        <ul class="collapse list-unstyled" id="mariculture">
         <li><input id="cb_mariculture" type="checkbox">Select all</li>
        <li class="dropdown-item">
      		<input type="checkbox" id="checked_mariculture_bfar" value="fishpen" class="mariculture">
      		<img src="/static/images/iconCircle/mariculturezone.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Pen"/>
       		<span class="text-white">BFAR MANAGE</span>
      </li>
      <li class="dropdown-item">
      		<input type="checkbox" id="checked_mariculture_private" value="fishpen" class="mariculture">
      		<img src="/static/images/iconCircle/mariculturezone.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Pen"/>
       		<span class="text-white">PRIVATE SECTOR MANAGE</span>
      </li>
      <li class="dropdown-item">
      		<input type="checkbox" id="checked_mariculture_lgu" value="fishpen" class="mariculture">
      		<img src="/static/images/iconCircle/mariculturezone.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Pen"/>
       		<span class="text-white">LGU MANAGE</span>
      </li>
      </ul>
      </li>
      
      <li>
       <a href="#capture" data-toggle="collapse"  aria-expanded="false">
     	 <span class="text-white"><i class="fas fa-list"></i>&nbsp;CAPTURE FISHERIES</span>
       </a>
        <ul class="collapse list-unstyled" id="capture">
        <li><input id="cb_capture" type="checkbox">Select all</li>
      	<li class="dropdown-item">
      		<input type="checkbox" id="check_payao" value="payao" name="CAPTURE" class="capture">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">PAYAO</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="check_lambaklad" value="lambaklad" name="CAPTURE" class="capture">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">LAMBAKLAD</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="check_frp" value="frp" name="CAPTURE" class="capture">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       			<span class="text-white">FRP BOATS</span>
       	</li>
       	</ul>
       	</li>
       	
       	<li>
       <a href="#postharvest" data-toggle="collapse"  aria-expanded="false">
     	 <span class="text-white"><i class="fas fa-list"></i>&nbsp;POST HARVEST FACILITIES</span>
       </a>
        <ul class="collapse list-unstyled" id="postharvest">
        
      	<li>
       		<a href="#fishlanding" data-toggle="collapse"  aria-expanded="false">
     	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;FISH LANDING</span>
       		</a>
        		<ul class="collapse list-unstyled" id="fishlanding">
        		<li><input id="cb_fishlanding" type="checkbox">Select all</li>
      			
      			<li>
       				<a href="#cflc" data-toggle="collapse"  aria-expanded="false">
     	 			<span class="text-white"><i class="fas fa-list"></i>&nbsp;CFLC</span>
       				</a>
        				<ul class="collapse list-unstyled" id="cflc">
        					<li class="dropdown-item">
      							<input type="checkbox" id="check_cflc_operational" value="cflc" name="FISH_LANDING" class="fishlanding">
       							<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       							<span class="text-white">OPERATIONAL</span>
       						</li>
       						<li class="dropdown-item">
      							<input type="checkbox" id="check_cflc_for_operation" value="cflc" name="FISH_LANDING" class="fishlanding">
       							<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       							<span class="text-white">FOR OPERATION</span>
       						</li>
       						<li class="dropdown-item">
      							<input type="checkbox" id="check_cflc_for_complition" value="cflc" name="FISH_LANDING" class="fishlanding">
       							<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       							<span class="text-white">FOR COMPLETION</span>
       						</li>
       						<li class="dropdown-item">
      							<input type="checkbox" id="check_cflc_for_transfer" value="cflc" name="FISH_LANDING" class="fishlanding">
       							<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       							<span class="text-white">FOR TRANSFER</span>
       						</li>
       						<li class="dropdown-item">
      							<input type="checkbox" id="check_cflc_damaged" value="cflc" name="FISH_LANDING" class="fishlanding">
       							<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       							<span class="text-white">DAMAGED</span>
       						</li>
      					</ul>
      			</li>		
      			
       			<li class="dropdown-item">
      				<input type="checkbox" id="check_noncflc" value="cflc" name="FISH_LANDING" class="fishlanding">
       				<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       				<span class="text-white">NON-CFLC</span>
       			</li>
       			<li class="dropdown-item">
      				<input type="checkbox" id="check_traditional" value="cflc" name="FISH_LANDING" class="fishlanding">
       				<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       				<span class="text-white">TRADITIONAL</span>
       			</li>
       			</ul>
       	</li>
      	<li>
       		<a href="#coldstorage" data-toggle="collapse"  aria-expanded="false">
     	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;Cold Storage/IPCS</span>
       		</a>
        		<ul class="collapse list-unstyled" id="coldstorage">
        		<li><input id="cb_coldstorage" type="checkbox">Select all</li>
        		<li class="dropdown-item">
      				<input type="checkbox" id="check_pfda" value="BFAR" name="coldstorage" class="coldstorage">
       				<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       				<span class="text-white">PFDA</span>
       			</li>
       			<li class="dropdown-item">
      				<input type="checkbox" id="check_cooperative" value="BFAR" name="coldstorage" class="coldstorage">
       				<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       				<span class="text-white">COOPERATIVE</span>
       			</li>
       			<li class="dropdown-item">
      				<input type="checkbox" id="check_po" value="BFAR" name="coldstorage" class="coldstorage">
       				<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       				<span class="text-white">PRIVATE ORGANIZATION</span>
       			</li>
        		</ul>
        </li>			
       <li>
       		<a href="#processing" data-toggle="collapse"  aria-expanded="false">
     	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;FISH PROCESSING PLANT</span>
       		</a>
        		<ul class="collapse list-unstyled" id="processing">
       			<li><input id="cb_processing" type="checkbox">Select all</li>
      			<li class="dropdown-item">
      				<input type="checkbox" id="check_fp_bfar" value="BFAR" name="FISH_PROCESSING_PLANT" class="processing">
       				<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       				<span class="text-white">BFAR MANAGED</span>
       			</li>
       			<li class="dropdown-item">
      				<input type="checkbox" id="check_fp_lgu" value="LGU" name="FISH_PROCESSING_PLANT" class="processing">
       				<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       				<span class="text-white">LGU MANAGED</span>
       			</li>
       			<li class="dropdown-item">
      				<input type="checkbox" id="check_fp_pfda" value="PFDA" name="FISH_PROCESSING_PLANT" class="processing">
       				<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       				<span class="text-white">PFDA</span>
       			</li>
       			<li class="dropdown-item">
      				<input type="checkbox" id="check_fp_private" value="PRIVATE" name="FISH_PROCESSING_PLANT" class="processing">
       				<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       				<span class="text-white">PRIVATE SECTOR</span>
       			</li>
       			</ul>
       	</li>
       	<li>
       		<a href="#seaweed" data-toggle="collapse"  aria-expanded="false">
     	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;SEAWEED</span>
       		</a>
        		<ul class="collapse list-unstyled" id="seaweed">
        		<li><input id="cb_seaweed" type="checkbox">Select all</li>
      			
      			<li class="dropdown-item">
      				<input type="checkbox" id="check_seaweed_laboratory" value="LABORATORY" name="SEAWEED" class="seaweed">
       				<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       				<span class="text-white">SEAWEED LABORATORY</span>
       			</li>
       			<li class="dropdown-item">
      				<input type="checkbox" id="check_seaweed_nursery" value="NURSERY" name="SEAWEED" class="seaweed">
       				<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       				<span class="text-white">SEAWEED NURSERY</span>
       			</li>
       			<li class="dropdown-item">
      				<input type="checkbox" id="check_seaweed_warehouse" value="WAREHOUSE" name="SEAWEED" class="seaweed">
       				<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       				<span class="text-white">SEAWEED WAREHOUSE</span>
       			</li>
       			<li class="dropdown-item">
      				<input type="checkbox" id="check_seaweed_dryer" value="DRYER" name="SEAWEED" class="seaweed">
       				<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       				<span class="text-white">SEAWEED DRYER</span>
       			</li>
       			</ul>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_market" value="market">
       		<img src="/static/images/iconCircle/market.png" style="width: 20px; height: 20px;"   data-toggle="tooltip" title="Market"/>
       		<span class="text-white">Market</span>
       </li>
       	<li class="dropdown-item">
       		<input type="checkbox" id="checked_fishport" value="FISHPORT">
      		<img src="/static/images/iconCircle/fishport.png" style="width: 20px; height: 20px;"   data-toggle="tooltip" title="Fish Port"/>
       		<span class="text-white">Fish Port</span>
      	</li>
       	</ul>
       	</li>
       	
       	<li>
       		<a href="#habitat" data-toggle="collapse"  aria-expanded="false">
     	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;FISH HABITATS</span>
       		</a>
        		<ul class="collapse list-unstyled" id="habitat">
        		<li><input id="cb_habitat" type="checkbox">Select all</li>
      			<li class="dropdown-item">
      				<input type="checkbox" id="checked_seagrass" value="seagrass" class="habitat">
       				<img src="/static/images/iconCircle/seagrass.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Sea Grass"/>
       				<span class="text-white">Sea Grass</span>
      			</li>
      			<li class="dropdown-item">
      				<input type="checkbox" id="checked_mangrove" value="mangrove" class="habitat">
        			<img src="/static/images/iconCircle/mangrove.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Mangrove"/>
       				<span class="text-white">Mangrove</span>
      			</li>
      			<li class="dropdown-item">
      				<input type="checkbox" id="checked_coralreefs" value="fishcoral" class="habitat">
      				<img src="/static/images/iconCircle/fishcorral.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Corals"/>
       				<span class="text-white">Coral Reefs</span>
      			</li>
      			 <li class="dropdown-item">
      	 		<input type="checkbox" id="checked_fishsanctuaries" value="fishsanctuaries" class="habitat">
      	 		 <img src="/static/images/iconCircle/fishsanctuary.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Sanctuary"/>
     	 		<span class="text-white">Fish Sanctuary</span>
	  		</li>
      			</ul>
      			</li>
     	
     <li>
       <a href="#hatchery" data-toggle="collapse"  aria-expanded="false">
     	 <span class="text-white"><i class="fas fa-list"></i>&nbsp;Hatchery</span>
       </a>
        <ul class="collapse list-unstyled" id="hatchery">
        <li><input id="cb_hatchery" type="checkbox">Select all</li>
      <li class="dropdown-item">
       <input type="checkbox" id="checked_hatcheries_complete" value="hatcheries" class="hatchery">
       <img src="/static/images/iconCircle/hatcheries.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       <span class="text-white">Legislated Hatchery</span>
      </li>
      <li class="dropdown-item">
      <input type="checkbox" id="checked_hatcheries_incomplete" value="hatcheries" class="hatchery">
       <img src="/static/images/iconCircle/mpa.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       <span class="text-white">Non Legislated Hatchery</span>
      </li>
      </ul>
      </li>
    
   <%--    <li class="dropdown-item"><a href="${pageContext.request.contextPath}/admin/bar/all">Chart Report</a></li>
 	<li class="dropdown-item"><a href="${pageContext.request.contextPath}/admin/report/all">Excel Report</a></li>
 --%>
      <sec:authorize access="isAuthenticated()">
       <li class="dropdown-item"><a href="/logout">
       <span class="text-white"><i class="fas fa-sign-out-alt"></i>LOGOUT</span>
       </a>
       </li>
      </sec:authorize>

</ul>
</nav>