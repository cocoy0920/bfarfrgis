<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<nav id="sidebar">
<sec:authorize access="hasRole('ROLE_SUPERADMIN')">
<ul class="list-unstyled components mb-5">

       
             <li> 
    		<a href="#aqualist" data-toggle="collapse"  aria-expanded="false"> 
	  			<span class="text-white"><i class="fas fa-list"></i>&nbsp;AQUACULTURE</span></a> 
    		<ul class="collapse list-unstyled" id="aqualist"> 

   			<li class="dropdown-subitem"> 
   				<a href="#farmlist" data-toggle="collapse"  aria-expanded="false"> 
  	 			<span class="text-white">&nbsp;I. Aquafarms</span></a> 
    				<ul class="collapse list-unstyled" id="farmlist"> 

   					<li class="dropdown-item"> 
   					
   								<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../create/fishpondAllChart" >
	    							<img src="/static/images/pin2022/FishPond.png" style="width: 20px; height: 20px;"/>
    							 	<span class="text-white">Fish Pond</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="/create/fishpondAllChart" >
    								<img src="/static/images/pin2022/FishPond.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Pond</span>
    								</a>
    							</c:if>
    					</li>
    					<li class="dropdown-item">
    					
    					   					
   								<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../create/fishpenAllChart" >
	    							<img src="/static/images/pin2022/FishPen.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Pen</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="/create/fishpenAllChart" >
    								<img src="/static/images/pin2022/FishPen.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Pen</span>
    								</a>
    							</c:if>
 
   					</li>
   					<li class="dropdown-item">
   					
   					   		<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../create/fishcageAllChart" >
	    							<img src="/static/images/pin2022/FishCage.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Cage</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="/create/fishcageAllChart" >
    								<img src="/static/images/pin2022/FishCage.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Cage</span>
    								</a>
    							</c:if>
    						
   					</li>
   					<li class="dropdown-item">
   						<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../create/fishcorralAllChart" >
	    							<img src="/static/images/pin2022/FishCorral.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Corral(BAKLAD)</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="/create/fishcorralAllChart" >
    								<img src="/static/images/pin2022/FishCorral.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Corral(BAKLAD)</span>
    								</a>
    							</c:if>
   						
   					</li>
    				  </ul> 
      		  </li> 
      		<li class="dropdown-subitem"> 
      		<c:if test="${page == true }">	
   				<a href="../create/seaweedsNLAllChart"> 
  	 			<span class="text-white">&nbsp;II. Seaweed</span></a> 
  	 		</c:if>	
      		<c:if test="${page == false }">	
   				<a href="/create/seaweedsNLAllChart"> 
  	 			<span class="text-white">&nbsp;II. Seaweed</span></a> 
  	 		</c:if>	

      		</li>
      		<li class="dropdown-subitem"> 
      			<c:if test="${page == true }">	
      				<a href="../create/maricultureAllChart"> 
  	 				<span class="text-white">&nbsp;III. Mariculture Park</span></a> 
  	 			</c:if>
  	 			<c:if test="${page == false }">	
      				<a href="/create/maricultureAllChart"> 
  	 				<span class="text-white">&nbsp;III. Mariculture Park</span></a> 
  	 			</c:if>

      		</li>
      		<li class="dropdown-subitem"> 
      		<c:if test="${page == true }">	
      			<a href="../create/hatcheryAllChart"> 
  	 			<span class="text-white">&nbsp;IV. Hatchery</span></a> 
  	 		</c:if>	
  	 		<c:if test="${page == false }">	
      			<a href="/create/hatcheryAllChart"> 
  	 			<span class="text-white">&nbsp;IV. Hatchery</span></a> 
  	 		</c:if>

      		</li>
    			</ul>
    		</li>
    		<li> 
    			<a href="#capturedList" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;CAPTURE FISHERIES</span></a> 
    				<ul class="collapse list-unstyled" id="capturedList"> 
     				<!-- <li><input id="capture" type="checkbox"><span class="text-white">Select all</span></li> -->
   					<li class="dropdown-item">
   						<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../create/payaoAllChart" >
	    							<img src="/static/images/pin2022/Payao.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Payao</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="/create/payaoAllChart" >
    								<img src="/static/images/pin2022/Payao.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Payao</span>
    								</a>
    							</c:if>
						</li>
    					<li class="dropdown-item">
    							<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../create/lambakladAllChart" >
	    							<img src="/static/images/pin2022/Lambaklad.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Lambaklad</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="/create/lambakladAllChart" >
    								<img src="/static/images/pin2022/Lambaklad.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Lambaklad</span>
    								</a>
    							</c:if>
    					</li>

   					<li class="dropdown-item">
   					    		<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../create/frpAllChart" >
	    							<img src="/static/images/pin2022/FRP_Boats.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">FRP Boats</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="/create/frpAllChart" >
    								<img src="/static/images/pin2022/FRP_Boats.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">FRP Boats</span>
    								</a>
    							</c:if>
    					</li>	
    				</ul>
    		</li>
    		   <li> 
    			<a href="#postharvestList" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;POST HARVEST</span></a> 
    				<ul class="collapse list-unstyled" id="postharvestList"> 
     				<li class="dropdown-subitem">
     				<c:if test="${page == true }">
    						<a href="../create/fishlandingAllChart">
  	 						<span class="text-white">&nbsp;I. Fish Landing</span></a>
  	 				</c:if>		
  	 				<c:if test="${page == false }">
    						<a href="/create/fishlandingAllChart">
  	 						<span class="text-white">&nbsp;I. Fish Landing</span></a>
  	 				</c:if>	

    					</li>
    					<li class="dropdown-subitem">
    						<c:if test="${page == true }">		
    							<a href="../create/coldAllChart">
  	 							<span class="text-white">&nbsp;II. Cold Storage/IPCS</span></a>
  	 						</c:if>
  	 						<c:if test="${page == false }">		
    							<a href="/create/coldAllChart">
  	 							<span class="text-white">&nbsp;II. Cold Storage/IPCS</span></a>
  	 						</c:if>

     				</li>
     				<li class="dropdown-subitem">
     					<c:if test="${page == true }">	
     						<a href="../create/fishprocessingAllChart">
  	 						<span class="text-white">&nbsp;III. Fish Processing Facility</span></a>
  	 					</c:if>	
  	 					<c:if test="${page == false }">	
     						<a href="/create/fishprocessingAllChart">
  	 						<span class="text-white">&nbsp;III. Fish Processing Facility</span></a>
  	 					</c:if>	

     				</li>
     				<li class="dropdown-subitem">
     				<c:if test="${page == true }">	
     					<a href="../create/seaweedsWDAllChart"><span class="text-white">&nbsp;IV. Seaweed - W/D</span></a>
     				</c:if>
     				<c:if test="${page == false }">	
     					<a href="/create/seaweedsWDAllChart"><span class="text-white">&nbsp;IV. Seaweed - W/D</span></a>
     				</c:if>

     				</li>
     				
     				<li class="dropdown-item">
     					
   						<!-- <input type="checkbox" id="checked_market" value="market"> -->
   						<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../create/marketAllChart" >
    						<!-- 	<img src="/static/images/pin2022/Market.png" style="width: 20px; height: 20px;"/> -->
    						<span class="text-white">V. Market</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="/create/marketAllChart" >
    							<!-- <img src="/static/images/pin2022/Market.png" style="width: 20px; height: 20px;"/> -->
    						<span class="text-white">V. Market</span>
    							</a>
    							</c:if>
    					
    					</li>	
    					<li class="dropdown-item">
    					<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../create/fishportAllChart" >   							
    								<span class="text-white">VI. Fish Port</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="/create/fishportAllChart" >
    								<!-- <img src="/static/images/pin2022/FishPort.png" style="width: 20px; height: 20px;"/> -->
    									<span class="text-white">VI. Fish Port</span>
    							</a>
    							</c:if>
    						
   						
   					</li>
    			</ul>
    		</li>
    			  	<li>         
    		<a href="#pageOther" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;OTHER RESOURCES</span></a> 
    	<ul class="collapse list-unstyled" id="pageOther"> 
     
	  <li class="dropdown-subitem">         
    		<a href="#pagefishhabitat" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white">&nbsp;II. Fish Habitat</span></a> 
    			<ul class="collapse list-unstyled" id="pagefishhabitat"> 
   					<li class="dropdown-item">
   						<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../create/seagrassAllChart" >
    							<img src="/static/images/pin2022/Seagrass.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Sea Grass</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="/create/seagrassAllChart" >
    							<img src="/static/images/pin2022/Seagrass.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Sea Grass</span>
    							</a>
    							</c:if>
    						
   					</li>
   					<li class="dropdown-item">
   						<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../create/mangroveAllChart" >
    								<img src="/static/images/pin2022/Mangrove.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Mangrove</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    								<a  class=" dropdown-item" tabindex="-1" href="/create/mangroveAllChart" >
    									<img src="/static/images/pin2022/Mangrove.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Mangrove</span>
    								</a>
    							</c:if>
     					
   					</li>
   					<li class="dropdown-item">
   						<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../create/coralreefAllChart" >
    								<img src="/static/images/pin2022/Coral-Reefs.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Coral Reefs</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    								<a  class=" dropdown-item" tabindex="-1" href="/create/coralreefAllChart" >
    								<img src="/static/images/pin2022/Coral-Reefs.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Coral Reefs</span>
    							</a>
    							</c:if>
   					</li>
    			</ul>
    </li>

    <li class="dropdown-subitem">         
    		<a href="#pageoma" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white">&nbsp;IV. Other Management Area</span></a> 
    			<ul class="collapse list-unstyled" id="pageoma"> 
    				<li class="dropdown-item">
   	 					<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../create/fishSanctuaryAllChart" >
    								<img src="/static/images/pin2022/FishSanctuary.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Sanctuary"/>
  	 								<span class="text-white">Fish Sanctuary</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    								<a  class=" dropdown-item" tabindex="-1" href="/create/fishSanctuaryAllChart" >
    								<img src="/static/images/pin2022/FishSanctuary.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Sanctuary"/>
  	 								<span class="text-white">Fish Sanctuary</span>
    							</a>
    							</c:if>
   	 		 			
	  				</li>
    			</ul>
    </li>
   
    </ul>
    </li>

      <sec:authorize access="isAuthenticated()">
       <li><a href="/logout"><i class="fas fa-sign-out-alt"></i>LOGOUT</a></li>
      </sec:authorize>

</ul>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_REGIONALADMIN') or hasRole('ROLE_USER')">
<ul class="list-unstyled components mb-5">

       
             <li> 
    		<a href="#aqualist" data-toggle="collapse"  aria-expanded="false"> 
	  			<span class="text-white"><i class="fas fa-list"></i>&nbsp;AQUACULTURE</span></a> 
    		<ul class="collapse list-unstyled" id="aqualist"> 

   			<li class="dropdown-subitem"> 
   				<a href="#farmlist" data-toggle="collapse"  aria-expanded="false"> 
  	 			<span class="text-white">&nbsp;I. Aquafarms</span></a> 
    				<ul class="collapse list-unstyled" id="farmlist"> 

   					<li class="dropdown-item"> 
   					
   								<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../create/fishpondChart" >
	    							<img src="/static/images/pin2022/FishPond.png" style="width: 20px; height: 20px;"/>
    							 	<span class="text-white">Fish Pond</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="/create/fishpondChart" >
    								<img src="/static/images/pin2022/FishPond.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Pond</span>
    								</a>
    							</c:if>
    					</li>
    					<li class="dropdown-item">
    					
    					   					
   								<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../create/fishpenChart" >
	    							<img src="/static/images/pin2022/FishPen.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Pen</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="/create/fishpenChart" >
    								<img src="/static/images/pin2022/FishPen.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Pen</span>
    								</a>
    							</c:if>
 
   					</li>
   					<li class="dropdown-item">
   					
   					   		<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../create/fishcageChart" >
	    							<img src="/static/images/pin2022/FishCage.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Cage</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="/create/fishcageChart" >
    								<img src="/static/images/pin2022/FishCage.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Cage</span>
    								</a>
    							</c:if>
    						
   					</li>
   					<li class="dropdown-item">
   						<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../create/fishcorralChart" >
	    							<img src="/static/images/pin2022/FishCorral.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Corral(BAKLAD)</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="/create/fishcorralChart" >
    								<img src="/static/images/pin2022/FishCorral.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Corral(BAKLAD)</span>
    								</a>
    							</c:if>
   						
   					</li>
    				  </ul> 
      		  </li> 
      		<li class="dropdown-subitem"> 
      		<c:if test="${page == true }">	
   				<a href="../create/seaweedsNLChart"> 
  	 			<span class="text-white">&nbsp;II. Seaweed</span></a> 
  	 		</c:if>	
      		<c:if test="${page == false }">	
   				<a href="/create/seaweedsNLChart"> 
  	 			<span class="text-white">&nbsp;II. Seaweed</span></a> 
  	 		</c:if>	

      		</li>
      		<li class="dropdown-subitem"> 
      			<c:if test="${page == true }">	
      				<a href="../create/maricultureChart"> 
  	 				<span class="text-white">&nbsp;III. Mariculture Park</span></a> 
  	 			</c:if>
  	 			<c:if test="${page == false }">	
      				<a href="/create/maricultureChart"> 
  	 				<span class="text-white">&nbsp;III. Mariculture Park</span></a> 
  	 			</c:if>

      		</li>
      		<li class="dropdown-subitem"> 
      		<c:if test="${page == true }">	
      			<a href="../create/hatcheryChart"> 
  	 			<span class="text-white">&nbsp;IV. Hatchery</span></a> 
  	 		</c:if>	
  	 		<c:if test="${page == false }">	
      			<a href="/create/hatcheryChart"> 
  	 			<span class="text-white">&nbsp;IV. Hatchery</span></a> 
  	 		</c:if>

      		</li>
    			</ul>
    		</li>
    		<li> 
    			<a href="#capturedList" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;CAPTURE FISHERIES</span></a> 
    				<ul class="collapse list-unstyled" id="capturedList"> 
     				<!-- <li><input id="capture" type="checkbox"><span class="text-white">Select all</span></li> -->
   					<li class="dropdown-item">
   						<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../create/payaoChart" >
	    							<img src="/static/images/pin2022/Payao.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Payao</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="/create/payaoChart" >
    								<img src="/static/images/pin2022/Payao.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Payao</span>
    								</a>
    							</c:if>
						</li>
    					<li class="dropdown-item">
    							<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../create/lambakladChart" >
	    							<img src="/static/images/pin2022/Lambaklad.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Lambaklad</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="/create/lambakladChart" >
    								<img src="/static/images/pin2022/Lambaklad.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Lambaklad</span>
    								</a>
    							</c:if>
    					</li>

   					<li class="dropdown-item">
   					    		<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../create/frpChart" >
	    							<img src="/static/images/pin2022/FRP_Boats.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">FRP Boats</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="/create/frpChart" >
    								<img src="/static/images/pin2022/FRP_Boats.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">FRP Boats</span>
    								</a>
    							</c:if>
    					</li>	
    				</ul>
    		</li>
    		   <li> 
    			<a href="#postharvestList" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;POST HARVEST</span></a> 
    				<ul class="collapse list-unstyled" id="postharvestList"> 
     				<li class="dropdown-subitem">
     				<c:if test="${page == true }">
    						<a href="../create/fishlandingChart">
  	 						<span class="text-white">&nbsp;I. Fish Landing</span></a>
  	 				</c:if>		
  	 				<c:if test="${page == false }">
    						<a href="/create/fishlandingChart">
  	 						<span class="text-white">&nbsp;I. Fish Landing</span></a>
  	 				</c:if>	

    					</li>
    					<li class="dropdown-subitem">
    						<c:if test="${page == true }">		
    							<a href="../create/coldChart">
  	 							<span class="text-white">&nbsp;II. Cold Storage/IPCS</span></a>
  	 						</c:if>
  	 						<c:if test="${page == false }">		
    							<a href="/create/coldChart">
  	 							<span class="text-white">&nbsp;II. Cold Storage/IPCS</span></a>
  	 						</c:if>

     				</li>
     				<li class="dropdown-subitem">
     					<c:if test="${page == true }">	
     						<a href="../create/fishprocessingChart">
  	 						<span class="text-white">&nbsp;III. Fish Processing Facility</span></a>
  	 					</c:if>	
  	 					<c:if test="${page == false }">	
     						<a href="/create/fishprocessingChart">
  	 						<span class="text-white">&nbsp;III. Fish Processing Facility</span></a>
  	 					</c:if>	

     				</li>
     				<li class="dropdown-subitem">
     				<c:if test="${page == true }">	
     					<a href="../create/seaweedsWDChart"><span class="text-white">&nbsp;IV. Seaweed - W/D</span></a>
     				</c:if>
     				<c:if test="${page == false }">	
     					<a href="/create/seaweedsWDChart"><span class="text-white">&nbsp;IV. Seaweed - W/D</span></a>
     				</c:if>

     				</li>
     				
     				<li class="dropdown-item">
     					
   						<!-- <input type="checkbox" id="checked_market" value="market"> -->
   						<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../create/marketChart" >
    						<!-- 	<img src="/static/images/pin2022/Market.png" style="width: 20px; height: 20px;"/> -->
    						<span class="text-white">V. Market</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="/create/marketChart" >
    							<!-- <img src="/static/images/pin2022/Market.png" style="width: 20px; height: 20px;"/> -->
    						<span class="text-white">V. Market</span>
    							</a>
    							</c:if>
    					
    					</li>	
    					<li class="dropdown-item">
    					<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../create/fishportChart" >   							
    								<span class="text-white">VI. Fish Port</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="/create/fishportChart" >
    								<!-- <img src="/static/images/pin2022/FishPort.png" style="width: 20px; height: 20px;"/> -->
    									<span class="text-white">VI. Fish Port</span>
    							</a>
    							</c:if>
    						
   						
   					</li>
    			</ul>
    		</li>
    			  	<li>         
    		<a href="#pageOther" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;OTHER RESOURCES</span></a> 
    	<ul class="collapse list-unstyled" id="pageOther"> 
     
	  <li class="dropdown-subitem">         
    		<a href="#pagefishhabitat" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white">&nbsp;II. Fish Habitat</span></a> 
    			<ul class="collapse list-unstyled" id="pagefishhabitat"> 
   					<li class="dropdown-item">
   						<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../create/seagrassChart" >
    							<img src="/static/images/pin2022/Seagrass.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Sea Grass</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="/create/seagrassChart" >
    							<img src="/static/images/pin2022/Seagrass.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Sea Grass</span>
    							</a>
    							</c:if>
    						
   					</li>
   					<li class="dropdown-item">
   						<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../create/mangroveChart" >
    								<img src="/static/images/pin2022/Mangrove.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Mangrove</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    								<a  class=" dropdown-item" tabindex="-1" href="/create/mangroveChart" >
    									<img src="/static/images/pin2022/Mangrove.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Mangrove</span>
    								</a>
    							</c:if>
     					
   					</li>
   					<li class="dropdown-item">
   						<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../create/coralreefChart" >
    								<img src="/static/images/pin2022/Coral-Reefs.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Coral Reefs</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    								<a  class=" dropdown-item" tabindex="-1" href="/create/coralreefChart" >
    								<img src="/static/images/pin2022/Coral-Reefs.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Coral Reefs</span>
    							</a>
    							</c:if>
   					</li>
    			</ul>
    </li>

    <li class="dropdown-subitem">         
    		<a href="#pageoma" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white">&nbsp;IV. Other Management Area</span></a> 
    			<ul class="collapse list-unstyled" id="pageoma"> 
    				<li class="dropdown-item">
   	 					<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../create/fishSanctuaryChart" >
    								<img src="/static/images/pin2022/FishSanctuary.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Sanctuary"/>
  	 								<span class="text-white">Fish Sanctuary</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    								<a  class=" dropdown-item" tabindex="-1" href="/create/fishSanctuaryChart" >
    								<img src="/static/images/pin2022/FishSanctuary.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Sanctuary"/>
  	 								<span class="text-white">Fish Sanctuary</span>
    							</a>
    							</c:if>
   	 		 			
	  				</li>
    			</ul>
    </li>
   
    </ul>
    </li>

      <sec:authorize access="isAuthenticated()">
       <li><a href="/logout"><i class="fas fa-sign-out-alt"></i>LOGOUT</a></li>
      </sec:authorize>

</ul>
</sec:authorize>
</nav>