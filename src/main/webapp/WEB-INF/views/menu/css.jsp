
	<link rel="stylesheet" href="/static/leaflet/leaflet.css"/> 
	<link rel="stylesheet" href="/static/leaflet/leaflet-bootstrapmodal.css"/> 
     <link rel="stylesheet" href="/static/js/bootstrap4/bootstrap-4.6.1.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/static/sidebar/sidebarstyle.css">
	<link href="/static/css/modal/print.css" rel="stylesheet"  type="text/css" media="print" />
 	<link href="/static/js/excel/tableexport.min.css" rel="stylesheet"></link>   
 	<link href="/static/css/table.css" rel="stylesheet"></link>
 	 <link href="/static/css/geo.css" rel="stylesheet"></link>
	<link href="/static/js/jquery-ui.min.css" rel="stylesheet">
	 <link rel="stylesheet" href="/static/leaflet/leaflet.legend.css" />  

	<script  src="/static/js/jquery-3.3.1.min.js" type=" text/javascript"></script>
	<script src="/static/js/jquery-ui.min.js" ></script>
	 <script src="/static/js/bootstrap4/bootstrap-4.6.1.min.js" ></script>

	<script src="/static/js/fontawesome.js" ></script>
	<script src='/static/sidebar/main.js'></script>

