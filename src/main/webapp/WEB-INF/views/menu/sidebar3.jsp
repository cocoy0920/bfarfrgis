 
 <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<nav id="sidebar">
<div class="custom-menu">
<button type="button" id="sidebarCollapse" class="btn btn-primary">
<i class="fa fa-bars"></i>
<span class="sr-only">Menu</span>
</button>
</div>
<!-- <div class="p-4"> -->
	<div class="sidebar-header">
      	<img class=" img img-fluid" alt="" src="/static/images/FRGIS-LOGO.png">
    </div>
<ul class="list-unstyled components mb-5">
	<li><a href="/create/user"><i class="fas fa-home"></i><span class="text">Home</span></a></li>
    <li>
       <a href="#pageCreate" data-toggle="collapse"  aria-expanded="false">
     	 <span class="text"><i class="fas fa-plus"></i>  CREATE</span>
       </a>
        <ul class="collapse list-unstyled" id="pageCreate">
          <li><a href="/create/fishsanctuary"><img src="/static/images/iconCircle/fishsanctuary.png" style="width: 20px; height: 20px;"/>  Fish Sanctuary</a></li>
          <li><a href="/create/fishprocessingplants"><img src="/static/images/iconCircle/fishprocessing.png" style="width: 20px; height: 20px;"/> Fish Processing</a></li>
          <li><a href="/create/fishlanding"><img src="/static/images/iconCircle/fish-landing.png" style="width: 20px; height: 20px;"/> Fish Landing</a></li>
          <li><a href="/create/fishport"><img src="/static/images/iconCircle/fishport.png" style="width: 20px; height: 20px;"/> Fish Port</a></li>
          <li><a href="/create/fishpen"><img src="/static/images/iconCircle/fishpen.png" style="width: 20px; height: 20px;"/>Fish Pen</a></li>
          <li><a href="/create/fishcages"><img src="/static/images/iconCircle/fishcage.png" style="width: 20px; height: 20px;"/> Fish Cage</a></li>
          <li><a href="/create/hatchery"><img src="/static/images/iconCircle/hatcheries.png" style="width: 20px; height: 20px;"/>Hatchery</a></li>
          <li><a href="/create/iceplantorcoldstorage"><img src="/static/images/iconCircle/iceplant.png" style="width: 20px; height: 20px;"/>Cold Storage</a></li>
          <li><a href="/create/market"><img src="/static/images/iconCircle/market.png" style="width: 20px; height: 20px;"/>Market</a></li>
          <li><a href="/create/schooloffisheries"><img src="/static/images/iconCircle/schoolof-fisheries.png" style="width: 20px; height: 20px;"/>School of Fisheries</a></li>
          <li><a href="/create/fishcorals"><img src="/static/images/iconCircle/fishcorral.png" style="width: 20px; height: 20px;"/>Fish Corals</a></li>
          <li><a href="/create/seagrass"><img src="/static/images/iconCircle/seagrass.png" style="width: 20px; height: 20px;"/>Sea Grass</a></li>
          <li><a href="/create/seaweeds"><img src="/static/images/iconCircle/seaweeds.png" style="width: 20px; height: 20px;"/>Sea Weeds</a></li>
          <li><a href="/create/mangrove"><img src="/static/images/iconCircle/mangrove.png" style="width: 20px; height: 20px;"/>Mangrove</a></li>
          <li><a href="/create/mariculturezone"> <img src="/static/images/iconCircle/mariculturezone.png" style="width: 20px; height: 20px;"/>Mariculture Zone</a></li>
          <li><a href="/create/lgu"><img src="/static/images/iconCircle/lgu.png" style="width: 20px; height: 20px;"/>PFO</a></li>
          <li><a href="/create/trainingcenter"><img src="/static/images/iconCircle/fisheriestraining.png" style="width: 20px; height: 20px;"/>Training Center</a></li>
        </ul>       
    </li>
        <li>
       <a href="#pageMap" data-toggle="collapse"  aria-expanded="false">
     	 <span class="text"><i class="fas fa-map"></i>  MAP</span>
       </a>
        <ul class="collapse list-unstyled" id="pageMap">
                   <li>
      	 		<input type="checkbox" id="checked_fishsanctuaries" value="fishsanctuaries">
      	 		 <img src="/static/images/iconCircle/fishsanctuary.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Sanctuary"/>
     	 		<span class="text">Fish Sanctuary</span>
	  		</li>
      <li>
      <input type="checkbox" id="checked_fishprocessing" value="fishprocessingplants">
      <img src="/static/images/iconCircle/fishprocessing.png" style="width: 20px; height: 20px;"  data-toggle="tooltip" title="Fish Processing"/>
       <span class="text">Fish Processing</span>
      </li>
      <li>
      <input type="checkbox" id="checked_fishslanding" value="fishslanding">
      <img src="/static/images/iconCircle/fish-landing.png" style="width: 20px; height: 20px;"  data-toggle="tooltip" title="Fish Landing"/>
       <span class="text">Fish Landing</span>
      </li>
      <li>
       <input type="checkbox" id="checked_fishport" value="FISHPORT">
      <img src="/static/images/iconCircle/fishport.png" style="width: 20px; height: 20px;"   data-toggle="tooltip" title="Fish Port"/>
       <span class="text">Fish Port</span>
      </li>
      <li>
      <input type="checkbox" id="checked_fishpen" value="fishpen">
      <img src="/static/images/iconCircle/fishpen.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Pen"/>
       <span class="text">Fish Pen</span>
      </li>
       <li>
       <input type="checkbox" id="checked_fishcage" value="fishcage">
       <img src="/static/images/iconCircle/fishcage.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Cage"/>
       <span class="text">Fish Cage</span>
      </li>
      <li>
      <input type="checkbox" id="checked_hatcheries" value="hatcheries">
       <img src="/static/images/iconCircle/hatcheries.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Hatchery"/>
       <span class="text">Hatchery</span>
      </li>
      <li>
      <input type="checkbox" id="checked_coldstorage" value="coldstorage">
     	<img src="/static/images/iconCircle/iceplant.png" style="width: 20px; height: 20px;"  data-toggle="tooltip" title="Cold Storage"/>
       <span class="text">Cold Storage</span>
      </li>
      <li>
      <input type="checkbox" id="checked_market" value="market">
       <img src="/static/images/iconCircle/market.png" style="width: 20px; height: 20px;"   data-toggle="tooltip" title="Market"/>
       <span class="text">Market</span>
      </li>
      <li>
      <input type="checkbox" id="checked_schoolOfFisheries" value="schoolOfFisheries">
       <img src="/static/images/iconCircle/schoolof-fisheries.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="School of Fisheries"/>
       <span class="text">School of Fisheries</span>
      </li>
      <li>
      <input type="checkbox" id="checked_fishcoral" value="fishcoral">
      <img src="/static/images/iconCircle/fishcorral.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Corals"/>
       <span class="text">Fish Corals</span>
      </li>
      <li>
      <input type="checkbox" id="checked_seagrass" value="seagrass">
       <img src="/static/images/iconCircle/seagrass.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Sea Grass"/>
       <span class="text">Sea Grass</span>
      </li>
      <li>
      <input type="checkbox" id="checked_seaweeds" value="seaweeds">
        <img src="/static/images/iconCircle/seaweeds.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Sea Weeds"/>
       <span class="text">Sea Weeds</span>
      </li>
      <li>
      <input type="checkbox" id="checked_mangrove" value="mangrove">
        <img src="/static/images/iconCircle/mangrove.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Mangrove"/>
       <span class="text">Mangrove</span>
      </li>
      <li>
      <input type="checkbox" id="checked_mariculturezone" value="mariculturezone">
        <img src="/static/images/iconCircle/mariculturezone.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Mariculture Zone"/>
       <span class="text">Mariculture Zone</span>
      <li>
      <input type="checkbox" id="checked_lgu" value="lgu">
        <img src="/static/images/iconCircle/lgu.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="PFO"/>
       <span class="text">PFO</span>
      </li>
      <li>
      <input type="checkbox" id="checked_training" value="trainingcenter">
       <img src="/static/images/iconCircle/fisheriestraining.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Training Center"/>
       <span class="text">Training Center</span>
      </li>
        </ul>       
    </li>
   
      <sec:authorize access="isAuthenticated()">
       <li><a href="/logout"><i class="fas fa-sign-out-alt"></i>LOGOUT</a></li>
      </sec:authorize>
</ul>

</nav>

<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>