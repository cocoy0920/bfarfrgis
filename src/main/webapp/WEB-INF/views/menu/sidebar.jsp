 
 <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
  <!-- Sidebar Holder -->
  <nav id="sidebar">
    <div class="sidebar-header">
      <img alt="" src="/static/images/FRGIS-LOGO.png" style="width: 200px; height: 150px">
    </div>
    

    <ul  class="list-unstyled components">
      <p>Resources</p>
      <!-- <li class="active">
        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">Home</a>
        <ul class="collapse list-unstyled" id="homeSubmenu">
          <li><a href="#">Home 1</a></li>
          <li><a href="#">Home 2</a></li>
          <li><a href="#">Home 3</a></li>
        </ul>
      </li>
      <li>
        <a href="#">About</a>
        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">Pages</a>
        <ul class="collapse list-unstyled" id="pageSubmenu">
          <li><a href="#">Page 1</a></li>
          <li><a href="#">Page 2</a></li>
          <li><a href="#">Page 3</a></li>
        </ul>
      </li> -->
     
      
       <li>
       <a href="#pageSanctuary" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/fishsanctuary.png" style="width: 40px; height: 40px;"/>
     Fish Sanctuary   <i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageSanctuary">
          <li><a href="/create/fishSanctuaryMap">Map</a></li>
          <li><a href="/create/fishsanctuary">Add</a></li>
        
        </ul>
       
       <!-- <a href="/create/fishsanctuary" class="menuFishsanctuary">
       <img src="/static/images/iconCircle/fishsanctuary.png" style="width: 40px; height: 40px;"/>
        Fish Sanctuary</a> -->
        
        </li>
      <li>
      
      <a href="#pageFishProcessing" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/fishprocessing.png" style="width: 40px; height: 40px;"/>
     Fish Processing   <i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageFishProcessing">
          <li><a href="/create/fishProcessingMap">Map</a></li>
          <li><a href="/create/fishprocessingplants">Add</a></li>
        
        </ul>
      
     <!--  <a href="/create/fishprocessingplants">
      <img src="/static/images/iconCircle/fishprocessing.png" style="width: 40px; height: 40px;"/>
      Fish Processing</a> -->
      </li>
      <li>
      <a href="#pageFishLanding" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/fish-landing.png" style="width: 40px; height: 40px;"/>
      Fish Landing   <i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageFishLanding">
          <li><a href="/create/fishLandingMap">Map</a></li>
          <li><a href="/create/fishlanding">Add</a></li>
        
        </ul>
     <!--  <a href="/create/fishlanding">
      <img src="/static/images/iconCircle/fish-landing.png" style="width: 40px; height: 40px;"/>
      Fish Landing</a> -->
      </li>
      <li>
      <a href="#pageFishPort" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/fishport.png" style="width: 40px; height: 40px;"/>
      Fish Port   <i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageFishPort">
          <li><a href="/create/fishPortMap">Map</a></li>
          <li><a href="/create/fishport">Add</a></li>
        
        </ul>
     <!--  <a href="/create/fishport">
      <img src="/static/images/iconCircle/fishport.png" style="width: 40px; height: 40px;"/>
      Fish Port</a> -->
      </li>
      <li>
      <a href="#pageFishPen" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/fishpen.png" style="width: 40px; height: 40px;"/>
      Fish Pen   <i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageFishPen">
          <li><a href="/create/fishPenMap">Map</a></li>
          <li><a href="/create/fishpen">Add</a></li>
        
        </ul>
        
     <!--  <a href="/create/fishpen">
      <img src="/static/images/iconCircle/fishpen.png" style="width: 40px; height: 40px;"/>
      Fish Pen</a> -->
      </li>
      <!-- <li><a href="/create/fishcages"> -->
      <li>
      
       <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/fishcage.png" style="width: 40px; height: 40px;"/>
      Fish Cage   <i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageSubmenu">
          <li><a href="/create/fishCageMap">Map</a></li>
          <li><a href="/create/fishcages">Add</a></li>
        
        </ul>
      
      <!-- <a href="/create/fishCageMap">
      <img src="/static/images/iconCircle/fishcage.png" style="width: 40px; height: 40px;"/>
      Fish Cage</a> -->
      </li>
      <li>
       <a href="#pageHatchery" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/hatcheries.png" style="width: 40px; height: 40px;"/>
      Hatchery   <i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageHatchery">
          <li><a href="/create/hatcheryMap">Map</a></li>
          <li><a href="/create/hatchery">Add</a></li>
        
        </ul>
      
      <!-- <a href="/create/hatchery">
      <img src="/static/images/iconCircle/hatcheries.png" style="width: 40px; height: 40px;"/>
      Hatchery</a> -->
      </li>
      <li>
      <a href="#pageIpcs" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/iceplant.png" style="width: 40px; height: 40px;"/>
      Cold Storage   <i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageIpcs">
          <li><a href="/create/ipcsMap">Map</a></li>
          <li><a href="/create/iceplantorcoldstorage">Add</a></li>
        
        </ul>
        
     <!--  <a href="/create/iceplantorcoldstorage" >
      <img src="/static/images/iconCircle/iceplant.png" style="width: 40px; height: 40px;"/>
      Cold Storage</a> -->
      </li>
      <li>
       <a href="#pageMarket" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/market.png" style="width: 40px; height: 40px;"/>
      Market   <i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageMarket">
          <li><a href="/create/marketMap">Map</a></li>
          <li><a href="/create/market">Add</a></li>
        
        </ul>
      
<!--       <a href="/create/market">
      <img src="/static/images/iconCircle/market.png" style="width: 40px; height: 40px;"/>
      Market</a> -->
      </li>
      <li>
       <a href="#pageSchool" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/schoolof-fisheries.png" style="width: 40px; height: 40px;"/>
      School of Fisheries   <i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageSchool">
          <li><a href="/create/schoolMap">Map</a></li>
          <li><a href="/create/schooloffisheries">Add</a></li>
        
        </ul>
      <!-- <a href="/create/schooloffisheries">
      <img src="/static/images/iconCircle/schoolof-fisheries.png" style="width: 40px; height: 40px;"/>
      School of Fisheries</a> -->
      </li>
      <li>
      <a href="#pageFishCoral" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/fishcorral.png" style="width: 40px; height: 40px;"/>
      Fish Corals<i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageFishCoral">
          <li><a href="/create/fishcoralMap">Map</a></li>
          <li><a href="/create/fishcorals">Add</a></li>
          </ul>
     <!--  <a href="/create/fishcorals">
      <img src="/static/images/iconCircle/fishcorral.png" style="width: 40px; height: 40px;"/>
      Fish Corals</a>
       -->
      </li>
      <li>
      <a href="#pageSeagrass" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/seagrass.png" style="width: 40px; height: 40px;"/>
      Sea Grass<i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageSeagrass">
          <li><a href="/create/seagrassMap">Map</a></li>
          <li><a href="/create/seagrass">Add</a></li>
          </ul>
          
     <!--  <a href="/create/seagrass">
      <img src="/static/images/iconCircle/seagrass.png" style="width: 40px; height: 40px;"/>
      Sea Grass</a> -->
      
      </li>
      <li>
        <a href="#pageSeaweeds" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/seaweeds.png" style="width: 40px; height: 40px;"/>
      Sea Weeds<i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageSeaweeds">
          <li><a href="/create/seaweedsMap">Map</a></li>
          <li><a href="/create/seaweeds">Add</a></li>
          </ul>
      
     <!--  <a href="/create/seaweeds">
      <img src="/static/images/iconCircle/seaweeds.png" style="width: 40px; height: 40px;"/>
      Sea Weeds</a> -->
      
      </li>
      <li>
        <a href="#pageMangrove" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/mangrove.png" style="width: 40px; height: 40px;"/>
      Mangrove<i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageMangrove">
          <li><a href="/create/mangroveMap">Map</a></li>
          <li><a href="/create/mangrove">Add</a></li>
          </ul>
      
      <!-- <a href="/create/mangrove">
      <img src="/static/images/iconCircle/mangrove.png" style="width: 40px; height: 40px;"/>
      Mangrove</a> -->
      </li>
      <li>
        <a href="#pageZone" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/mariculturezone.png" style="width: 40px; height: 40px;"/>
      Mariculture Zone<i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageZone">
          <li><a href="/create/zoneMap">Map</a></li>
          <li><a href="/create/mariculturezone">Add</a></li>
          </ul>
     <!--  <a href="/create/mariculturezone">
      <img src="/static/images/iconCircle/mariculturezone.png" style="width: 40px; height: 40px;"/>
      Mariculture Zone</a> -->
      </li>
      <li>
        <a href="#pageLGU" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/lgu.png" style="width: 40px; height: 40px;"/>
      LGU<i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageLGU">
          <li><a href="/create/lguMap">Map</a></li>
          <li><a href="/create/lgu">Add</a></li>
          </ul>
      <!-- <a href="/create/lgu">
      <img src="/static/images/iconCircle/lgu.png" style="width: 40px; height: 40px;"/>
      LGU</a>
       -->
      </li>
      <li>
       <a href="#pageTraining" data-toggle="collapse" aria-expanded="false"><img src="/static/images/iconCircle/fisheriestraining.png" style="width: 40px; height: 40px;"/>
      Training Center<i class="fas fa-list"></i></a>
        <ul class="collapse list-unstyled" id="pageTraining">
          <li><a href="/create/trainingMap">Map</a></li>
          <li><a href="/create/trainingcenter">Add</a></li>
          </ul>
      
      <!-- <a href="/create/trainingcenter">
      <img src="/static/images/iconCircle/fisheriestraining.png" style="width: 40px; height: 40px;"/>
      Training Center</a> -->
      </li>
     
      <sec:authorize access="isAuthenticated()">
       <li><a href="/logout"><i class="fas fa-sign-out-alt"></i>LOGOUT</a></li>
       </sec:authorize>
        <sec:authorize access="!isAuthenticated()">
       <li><a href="/login"><i class="fa fa-fw fa-user"></i>LOGIN</a></li>
       </sec:authorize>
    </ul>

  </nav>
<script>
jQuery(document).ready(function($) {
	

});
</script>