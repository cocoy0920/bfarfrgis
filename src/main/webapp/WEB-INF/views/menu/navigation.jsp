<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<style>
#navheight {
height: 50px;
}
</style>
<nav id="navheight" class="navbar navbar-inverse p-3 mb-2 bg-primary text-white">
  <div class="container-fluid">
   <sec:authorize access="isAuthenticated()">
      <span>welcome back  <sec:authentication property="principal.username" /> </span>
      <ul class="list-unstyled">
       <li><a href="/logout"><i class="fas fa-sign-out-alt"></i>LOGOUT</a></li>
       </ul>
      </sec:authorize>
        <sec:authorize access="!isAuthenticated()">
         <ul>
       <li><a href="/login"><i class="fa fa-fw fa-user"></i>LOGIN</a></li>
       </ul>
       </sec:authorize>
  </div>
</nav>
