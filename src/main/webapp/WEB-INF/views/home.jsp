
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
<head>
<title>LOGIN</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="<c:url value='/static/images/favicon.png' />" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

 <body style="background-color:#ededed;">
	<div style="background-color:#337ab7;height:50px;"></div>
	<div class="container-fluid">
		<div class="row col-lg-4 col-lg-offset-4" style="margin-top: 80px;background-color:#fff;padding:20px;border:solid 1px #ddd;">
<!-- 			<img th:src="@{/images/login.jpg}" class="img-responsive center-block" width="300" height="300" alt="Logo" /> -->
<!-- 			<form action="/login" method="POST" class="form-signin"> -->
				<h3 class="form-signin-heading" >Login</h3>
				<br /> 
				
				<input type="text" id="username" name="username" placeholder="username" class="form-control" /> <br /> 
				<input type="password" placeholder="Password" id="password" name="password" class="form-control" /> <br />

				<button class="btn btn-lg btn-primary" name="Submit" value="Login" type="Submit"  style="margin-right:10px;">Login</button>
				<a href="/">Home</a>
			</form>
		</div>
	</div> 
<!-- 	<my-counter page="fishSanctuary"></my-counter>
	<script src="/static/component/home.js"></script> -->
	
</body>
</html>