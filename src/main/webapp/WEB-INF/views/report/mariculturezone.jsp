<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Mariculture Zone</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
</head>
<body>

 <jsp:include page="../home/header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">
<jsp:include page="sidenavbar.jsp"></jsp:include>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
    <h1 align="center">MARICULTURE ZONE</h1>

 <div class="container">
 <!-- <button class="create btn-primary">Create</button> 
 <div class="row"> -->
       <div class="col-md-12 table-responsive">
<table id="mariculture-table" class="table table-hover">
                <thead>
                  <tr>
                   <th>Region</th><th>Province</th><th>Municipality</th><th>Barangay</th><th>Name of Mariculture</th><th>Coordiantes</th>
                  </tr>
                </thead>
                <tbody>  
               </tbody>
              </table>
              <div id="mariculture_paging">
             
             </div>
 
      <table class="table2excel" id="exTable" style="display: none">
                <thead>
                <tr><th align="center" colspan="17" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr>             
                <tr><th align="center" colspan="14">MARICULTURE ZONE</th></tr>
                
                  <tr>
                  <th>Region</th>
                   <th>Province</th>
                   <th>Municipality</th>
                   <th>Barangay</th>
                    <th>Name of Mariculture</th>
                   <th>Code</th>
                   <th>Area(sqm)</th>
                  
                   <th>Date Launched</th>
                   <th>Date Inacted</th>
                   <th>eCcNumber</th>
                   <th>Mariculture Type</th>
                   <th>Municipal Ordinance Number</th>
                   <th>Date Approved</th>
                   <!--<th>Individual</th>
                   <th>Partnership</th>
                   <th>Cooperative Association<th>
                     -->
                     <!--
                   <th>Type</th>
                   <th>Size<th>
                   <th>Quality</th>
                   <th>Species Cultured</th>
                   <th>Quantity (mt.)</th>
                   <th>Production per Cropping (M.t)</th>
                   <th>No. of Cropping per year</th>
                   <th>Species</th>
                   <th>Farm Produce/Kg</th>
                   <th>Product destination for marketing monitoring</th>
                   <th>Aquaculture production MT for the last 5 year</th>
                   -->
                   <th>Data Source</th>
                   <th>Latitude</th>
                   <th>Longitude</th>
                   <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="mariculture_body">
                
               </tbody>
              </table> 
      <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','MaricultureZone')" />
      
      </div>
 
 </div>
  </div>
   </div>


<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>
<script src="/static/js/form/provinceMunBarangay.js"></script>
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script>

var region_id = "all";

getMaricultureDefaultPage();
getMariculturePerPage();
	getAllMaricultureList();
	
$('#Regions').change(function(){

  	var ddlOption = $("#Regions option:selected").text();
    var ddlValue = $("#Regions option:selected").val();
     region_id = ddlValue;
    
     
     getMaricultureDefaultPage();
     getMariculturePerPage();
   	getAllMaricultureList();
   
});
function getMaricultureDefaultPage(){
	
	 $.get("/admin/mariculturezone_admin/"+ region_id +"/" + 0, function(content, status){
  	          	      	
 	 var markup = "";
  			for ( var x = 0; x < content.maricultureZone.length; x++) {
  			  // console.log(content.maricultureZone[x].id);
  			   markup +="<tr>" +
  			   				"<td><span>" + content.maricultureZone[x].region + "</span></td>" +
  			   				"<td>"+content.maricultureZone[x].province+"</td><td>"+content.maricultureZone[x].municipality+"</td><td>"+content.maricultureZone[x].barangay+"</td><td>"+content.maricultureZone[x].nameOfMariculture+"</td><td>"+content.maricultureZone[x].lat+","+content.maricultureZone[x].lat+"</td>" +
  			   				//"<td class='map menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">MAP</td>" +
  							//"<td class='view menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">View</td>" +
  			   				"</tr>";
				}
				$("#mariculture-table").find('tbody').empty();
				$("#mariculture-table").find('tbody').append(markup);
			$("#mariculture_paging").empty();		
			$("#mariculture_paging").append(content.pageNumber);	
		  });
}
function getAllMaricultureList(){			
	$.get("/admin/getAllmariculturezoneList/" + region_id, function(content, status){
	        	
	        	 var markup = "";
	        	 var tr;
	        	 $('#mariculture_body').empty();
	         			for ( var x = 0; x < content.length; x++) {
	         			region = content[x].region;
						
	         			 if(content.length - 1 === x) {
	 				        $(".exportToExcel").show();
	 				    }
	 				
	         			
	         			tr = $('<tr/>');
	         			tr.append("<td>" + content[x].region + "</td>");
						tr.append("<td>" + content[x].province + "</td>");
						tr.append("<td>" + content[x].municipality + "</td>");
						tr.append("<td>" + content[x].barangay + "</td>");
						tr.append("<td>" + content[x].nameOfMariculture + "</td>");
						tr.append("<td>" + content[x].code + "</td>");
						tr.append("<td>" + content[x].area + "</td>");
						tr.append("<td>" + content[x].dateLaunched + "</td>");
						tr.append("<td>" + content[x].dateInacted + "</td>");
						tr.append("<td>" + content[x].eccNumber + "</td>");
						tr.append("<td>" + content[x].maricultureType + "</td>");
						tr.append("<td>" + content[x].municipalOrdinanceNo + "</td>");
						tr.append("<td>" + content[x].dateApproved + "</td>");
						/*
						tr.append("<td>" + content.maricultureZone[x].individual + "</td>");
						tr.append("<td>" + content.maricultureZone[x].partnership + "</td>");
						tr.append("<td>" + content.maricultureZone[x].cooperativeAssociation + "</td>");
						tr.append("<td>" + content.maricultureZone[x].cageType + "</td>");
						tr.append("<td>" + content.maricultureZone[x].cageSize + "</td>");
						tr.append("<td>" + content.maricultureZone[x].cageQuantity + "</td>");
						tr.append("<td>" + content.maricultureZone[x].speciesCultured + "</td>");
						tr.append("<td>" + content.maricultureZone[x].quantityMt + "</td>");
						tr.append("<td>" + content.maricultureZone[x].kind + "</td>");
						tr.append("<td>" + content.maricultureZone[x].productionPerCropping + "</td>");
						tr.append("<td>" + content.maricultureZone[x].numberofCropping + "</td>");
						tr.append("<td>" + content.maricultureZone[x].species + "</td>");
						tr.append("<td>" + content.maricultureZone[x].farmProduce + "</td>");
						tr.append("<td>" + content.maricultureZone[x].productDestination + "</td>");
						tr.append("<td>" + content.maricultureZone[x].aquacultureProduction + "</td>");
						*/
						tr.append("<td>" + content[x].dataSource + "</td>");
						tr.append("<td>" + content[x].lat + "</td>");
						tr.append("<td>" + content[x].lon + "</td>");
						tr.append("<td>" + content[x].remarks + "</td>");
						
						$('#mariculture_body').append(tr);	
						//ExportTable();
						}    		
	    		  });
	}
function getMariculturePerPage(){   

		  $('#mariculture_paging').delegate('a', 'click' , function(){
			  var $this = $(this),
	           target = $this.data('target');    
	          
			  $.get("/admin/mariculturezone_admin/"+ region_id +"/" + target, function(content, status){
	    	       	
			       	 var markup = "";
			        			for ( var x = 0; x < content.maricultureZone.length; x++) {
			        			 //  console.log(content.maricultureZone[x].id);
			        			   markup +="<tr>" +
			        			   "<td><span>" + content.maricultureZone[x].region + "</span></td>" +
			        			   				"<td>"+content.maricultureZone[x].province+"</td><td>"+content.maricultureZone[x].municipality+"</td><td>"+content.maricultureZone[x].barangay+"</td><td>"+content.maricultureZone[x].nameOfMariculture+"</td><td>"+content.maricultureZone[x].lat+","+content.maricultureZone[x].lat+"</td>" +
			        			   				//"<td class='map menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">MAP</td>" +
			        							//"<td class='view menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">View</td>" +
			        			   				"</tr>";
								}
			   				$("#mariculture-table").find('tbody').empty();
			   				$("#mariculture-table").find('tbody').append(markup);
			   			$("#mariculture_paging").empty();		
			   			$("#mariculture_paging").append(content.pageNumber);	
			   		  });
	      });
	      

	   }

</script>
	</body>

</html>