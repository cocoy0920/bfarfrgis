<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>FISH PORT</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
</head>
<body>

 <jsp:include page="../home/header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">
<jsp:include page="sidenavbar.jsp"></jsp:include>	
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
<h1 align="center">FISH PORT</h1>


 <div class="container"> 
 <div class="row">
       <div class="col-md-12 table-responsive">
<table id="fishport-table" class="table table-hover">
                <thead>
                  <tr>
                    <th>Region</th><th>Province</th><th>Municipality</th><th>Barangay</th><th>Name of Fish Port</th><th>Coordinates</th>
                  </tr>
                </thead>
                <tbody>  
               </tbody>
              </table>
              <div id="fishport_paging">
             
             </div>
   <table class="table2excel" id="exTable" style="display: none">
                <thead>
                <tr><th align="center" colspan="17" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr>               
                <tr><th align="center" colspan="16">FISH PORT</th></tr>
                
                  <tr>
                  <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Fish Port</th>
                    <th>Code</th>
                    <th>Area (sqm.)</th>
                    <th>Name of Caretaker</th>
                    <th>Type</th>
                    <th>Classification</th>
                    <th>Volume of Unloading</th>
                    <th>Data Sources</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Remarks</th>
                  
                  </tr>
                </thead>
                <tbody id="fishport_body">
               </tbody>
              </table>       
    <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','FishPort')" />
      
    </div>
 
 </div>
  </div>
<!-- end of div content -->
  </div>



  <!-- end wrapper div -->
    </div>
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>		
<script src="/static/js/form/provinceMunBarangay.js"></script>
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script>
var region_id = "all";

getFishPortDefaultPage();
getFishPortPerPage();
getFishPortList();

$('#Regions').change(function(){

  	var ddlOption = $("#Regions option:selected").text();
    var ddlValue = $("#Regions option:selected").val();
     region_id = ddlValue;
    
     getFishPortDefaultPage();
     getFishPortPerPage();
     getFishPortList();
   
});

function getFishPortDefaultPage(){
	  
	$.get("/admin/fishport_admin/" + region_id +"/" + 0,function(content, status) {
	
	 var markup = "";
			for ( var x = 0; x < content.fishPort.length; x++) {
			  // console.log(content.fishPort[x].id);
			   markup +="<tr>" +
			   			"<td><span>" + content.fishPort[x].region + "</span></td>" +
			   			"<td>"+content.fishPort[x].province+"</td>" +
			   			"<td>"+content.fishPort[x].municipality+"</td>" +
			   			"<td>"+content.fishPort[x].barangay+"</td>" +
			   			"<td>"+content.fishPort[x].nameOfFishport+"</td>" +
			   			"<td>"+content.fishPort[x].lat+","+content.fishPort[x].lon+"</td>" +
			   			//"<td class='map menu_links btn-primary' data-href="+ content.fishPort[x].uniqueKey + ">MAP</td>" +
						//"<td class='view menu_links btn-primary' data-href="+ content.fishPort[x].uniqueKey + ">View</td>" +
			   			"</tr>";
			}
			$("#fishport-table").find('tbody').empty();
			$("#fishport-table").find('tbody').append(markup);
		$("#fishport_paging").empty();		
		$("#fishport_paging").append(content.pageNumber);	
	  });
}
function getFishPortList(){
	
	$.get("/admin/getAllfishportList/" + region_id, function(content, status){
	        	 
	        	 var markup = "";
	        	 var tr;
	        	 $('#fishport_body').empty();
	         			for ( var x = 0; x < content.length; x++) {
	         			region = content[x].region;
	         			
	         			 if(content.length - 1 === x) {
	 				       // console.log('loop ends');
	 				        $(".exportToExcel").show();
	 				    }
	 				
	         			
						tr = $('<tr/>');
						tr.append("<td>" + content[x].region + "</td>");
						tr.append("<td>" + content[x].province + "</td>");
						tr.append("<td>" + content[x].municipality + "</td>");
						tr.append("<td>" + content[x].barangay + "</td>");
						tr.append("<td>" + content[x].nameOfCaretaker + "</td>");
						tr.append("<td>" + content[x].code + "</td>");
						tr.append("<td>" + content[x].area + "</td>");
						tr.append("<td>" + content[x].nameOfCaretaker + "</td>");
						tr.append("<td>" + content[x].type + "</td>");
						tr.append("<td>" + content[x].classification + "</td>");
						tr.append("<td>" + content[x].volumeOfUnloading + "</td>");
						tr.append("<td>" + content[x].dataSource + "</td>");
						tr.append("<td>" + content[x].lat + "</td>");
						tr.append("<td>" + content[x].lon + "</td>");
						tr.append("<td>" + content[x].remarks + "</td>");
						
						
						$('#fishport_body').append(tr);	
						}    		
	    		  });
	}
function getFishPortPerPage(){
		
		$('#fishport_paging').delegate('a','click',function() {
					var $this = $(this),
					target = $this.data('target');

					$.get("/admin/fishport_admin/" + region_id +"/" + target,function(content, status) {
					   	
					   	 var markup = "";
					    			for ( var x = 0; x < content.fishPort.length; x++) {
					    		
					    			   markup +="<tr>" +
					    			   			"<td><span>" + content.fishPort[x].region + "</span></td>" +
					    			   			"<td>"+content.fishPort[x].province+"</td>" +
					    			   			"<td>"+content.fishPort[x].municipality+"</td>" +
					    			   			"<td>"+content.fishPort[x].barangay+"</td>" +
					    			   			"<td>"+content.fishPort[x].nameOfFishport+"</td>" +
					    			   			"<td>"+content.fishPort[x].lat+","+content.fishPort[x].lon+"</td>" +
					    			   			//"<td class='map menu_links btn-primary' data-href="+ content.fishPort[x].uniqueKey + ">MAP</td>" +
					    						//"<td class='view menu_links btn-primary' data-href="+ content.fishPort[x].uniqueKey + ">View</td>" +
					    			   			"</tr>";
									}
									$("#fishport-table").find('tbody').empty();
									$("#fishport-table").find('tbody').append(markup);
								$("#fishport_paging").empty();		
								$("#fishport_paging").append(content.pageNumber);	
							  });
		});

	}

</script>
    </body>
</html>