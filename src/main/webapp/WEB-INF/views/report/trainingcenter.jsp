<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Training Center</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
	<jsp:include page="../menu/css.jsp"></jsp:include>
</head>
<body>

 <jsp:include page="../home/header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">
<jsp:include page="sidenavbar.jsp"></jsp:include>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
    <h1 align="center">TRAINING CENTER</h1>

 <div class="container"> 
<!--  <button class="create btn-primary">Create</button> -->
 <div class="row">
       <div class="col-md-12 table-responsive">
			<table id="tableList" class="table table-hover">
                <thead>
                  <tr>
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Training Center</th>
                     <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody>  
               </tbody>
              </table>            
  			<div id="training_paging">
               
             </div>
     <!-- <table id="export-buttons-table" class="table" style="display: none"> --> 
       <table class="table2excel" id="exTable"  style="display: none">
                <thead>
                <tr><th align="center" colspan="17" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr>              
                <tr><th align="center" colspan="14">TRAINING CENTER</th></tr>
               
                  <tr>
                  	<th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Code</th>
                    <th>Name of Training Center</th>
                    <th>Specialization</th>
                    <th>Facility Within The Training Center</th>
                    <th>Type</th>
                    <th>Data Sources</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="training_body">
                <!--  ${listAll}--> 
               </tbody>
              </table>       
    <!-- <button class="exportToExcel btn-info">Export to XLS</button> -->
     <input class="btn-info exportToExcel" type="button" value="Export to XLS" onclick="exportToExcel('exTable','TrainingCenter')" />
      
      </div>
 </div>
 </div>
 </div>
 </div>
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>	
<script src="/static/js/form/provinceMunBarangay.js"></script>
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script>
var region_id = "all";

getTrainingDefaultPage();
getTrainingCenterPerPage();
getTrainingList();

$('#Regions').change(function(){

  	var ddlOption = $("#Regions option:selected").text();
    var ddlValue = $("#Regions option:selected").val();
     region_id = ddlValue;
     
     getTrainingDefaultPage();
     getTrainingCenterPerPage();
     getTrainingList();
   
});
function getTrainingDefaultPage(){

	 $.get("/admin/trainings_admin/"+ region_id +"/" + 0, function(content, status){
 	  
var markup = "";
		for ( var x = 0; x < content.trainingCenter.length; x++) {
			//console.log("/admin/trainings_admin/" + content.trainingCenter[x].region);
		   markup +="<tr>" +
		   		"<td><span>" + content.trainingCenter[x].region + "</span></td>" +
			   		"<td>"+content.trainingCenter[x].province+"</td>" +
			 		"<td>"+content.trainingCenter[x].municipality+"</td>" +
			 		"<td>"+content.trainingCenter[x].barangay+"</td>" +
			 		"<td>"+content.trainingCenter[x].name+"</td>" +
			 		"<td>"+content.trainingCenter[x].lat+","+content.trainingCenter[x].lon+"</td>" +
			 		//"<td class='map menu_links btn-primary' data-href="+ content.trainingCenter[x].uniqueKey + ">MAP</td>" +
			 		//"<td class='view menu_links btn-primary' data-href="+ content.trainingCenter[x].uniqueKey + ">View</td>" +
			 		"</tr>";
		}
		$("#tableList").find('tbody').empty();
		$("#tableList").find('tbody').append(markup);
	$("#training_paging").empty();		
	$("#training_paging").append(content.pageNumber);	
});
}
function getTrainingList(){
	$.get("/admin/getAllTrainingcenterList/" + region_id, function(content, status){
		    	 
	        	 var markup = "";
	        	 var tr;
	        	 $('#training_body').empty();	
	         			for ( var x = 0; x < content.length; x++) {
	         			region = content[x].region;
	         			
	         			 if(content.length - 1 === x) {
	 				        $(".exportToExcel").show();
	 				    }
	 				
	         			
						tr = $('<tr/>');
						tr.append("<td>" + content[x].region + "</td>");
						tr.append("<td>" + content[x].province + "</td>");
						tr.append("<td>" + content[x].municipality + "</td>");
						tr.append("<td>" + content[x].barangay + "</td>");
						tr.append("<td>" + content[x].code + "</td>");
						tr.append("<td>" + content[x].name + "</td>");
						tr.append("<td>" + content[x].specialization + "</td>");
						tr.append("<td>" + content[x].facilityWithinTheTrainingCenter + "</td>");
						tr.append("<td>" + content[x].type + "</td>");
						tr.append("<td>" + content[x].dataSource + "</td>");
						tr.append("<td>" + content[x].lat + "</td>");
						tr.append("<td>" + content[x].lon + "</td>");
						tr.append("<td>" + content[x].remarks + "</td>");
						
						$('#training_body').append(tr);	
						//ExportTable();
						}    		
	    		  });
	}
function getTrainingCenterPerPage(){   
	    
		  $('#training_paging').delegate('a', 'click' , function(){
	  	  
	        var $this = $(this),
	           target = $this.data('target');    
	        	
	        $.get("/admin/trainings_admin/"+ region_id +"/" + target, function(content, status){
	         	  
	       	 var markup = "";
	       			for ( var x = 0; x < content.trainingCenter.length; x++) {
	       			   markup +="<tr>" +
	       			   		"<td><span>" + content.trainingCenter[x].region + "</span></td>" +
	       			   		"<td>"+content.trainingCenter[x].province+"</td>" +
	       			 		"<td>"+content.trainingCenter[x].municipality+"</td>" +
	       			 		"<td>"+content.trainingCenter[x].barangay+"</td>" +
	       			 		"<td>"+content.trainingCenter[x].name+"</td><td>"+content.trainingCenter[x].lat+","+content.trainingCenter[x].lon+"</td>" +
	       			 		//"<td class='map menu_links btn-primary' data-href="+ content.trainingCenter[x].uniqueKey + ">MAP</td>" +
	       			 		//"<td class='view menu_links btn-primary' data-href="+ content.trainingCenter[x].uniqueKey + ">View</td>" +
	       			 		"</tr>";
	       			}
	       			$("#tableList").find('tbody').empty();
	       			$("#tableList").find('tbody').append(markup);
	       		$("#training_paging").empty();		
	       		$("#training_paging").append(content.pageNumber);	
	       	  });
	      });
	   }


</script>

	</body>

</html>