<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>IPCS</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
</head>
<body>

 <jsp:include page="../home/header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">
<jsp:include page="sidenavbar.jsp"></jsp:include>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">

 <h1 align="center">COLD STORAGE</h1>
 <div class="container"> 
 <div class="row">
       <div class="col-md-12 table-responsive">
<table id="cold-table" class="table table-hover">
                <thead>
                  <tr>
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Ice Plant or Cold Storage</th>
                    <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody>  
               </tbody>
              </table>
              <div id="cold_paging">
        
             </div>
 
     <table class="table2excel table table-hover" id="exTable" style="display: none">
                <thead>
                <tr><th align="center" colspan="17" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr>              
                <tr><th align="center" colspan="16">ICE PLANT/COLD STORAGE</th></tr>
                
                  <tr>
                  <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Ice Plant or Cold Storage</th>
                    <th>Code</th>
                   <th>Area</th>
                    <th>Valid Sanitary Permit</th>
                    <th>Operator</th>
                    <th>Type of Facility</th>
                    <th>Structure Type</th>
                    <th>Capacity</th>
                    <th>With Valid License to Operate</th>
                    <th>With Valid Sanitary Permit</th>
                    <th>Type Sanitary Permit</th>
                    <th>Others</th>
                    <th>Business Permits and Certificate Obtained</th>                    
                    <th>Type of Ice Maker</th>
                    <th>Food Gradule</th>
                    <th>Data Source</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Remarks</th>
                  
                  </tr>
                </thead>
                <tbody id="cold_body">
               </tbody>
              </table>       
 
     <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','ColdStorage')" />
      
     </div>
 
 </div>
  </div>
</div>
  </div>
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>	
<script src="/static/js/form/provinceMunBarangay.js"></script>		
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script>
var region_id = "all";

getIPCSDefaultPage();
getICPSPerPage();
getIPCSList();

$('#Regions').change(function(){

  	var ddlOption = $("#Regions option:selected").text();
    var ddlValue = $("#Regions option:selected").val();
     region_id = ddlValue;
     
     getIPCSDefaultPage();
     getICPSPerPage();
     getIPCSList();
   
});
function getIPCSDefaultPage(){
	
	 $.get("/admin/ipcs_admin/"+ region_id +"/" + 0, function(content, status){

	  
	 var markup = "";
			for ( var x = 0; x < content.icePlantColdStorage.length; x++) {
			  // console.log(content.icePlantColdStorage[x].id);
			   markup +="<tr>" +
			  "<td><span>" + content.icePlantColdStorage[x].region + "</span></td>" +
			   				"<td>"+content.icePlantColdStorage[x].province+"</td><td>"+content.icePlantColdStorage[x].municipality+"</td><td>"+content.icePlantColdStorage[x].barangay+"</td><td>"+content.icePlantColdStorage[x].nameOfIcePlant+"</td><td>"+content.icePlantColdStorage[x].lat+","+content.icePlantColdStorage[x].lon+"</td>" +
			   			//"<td class='map menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].uniqueKey + ">MAP</td>" +
						//"<td class='view menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].uniqueKey + ">View</td>" +
						
			   				"</tr>";
			}
			$("#cold-table").find('tbody').empty();
			$("#cold-table").find('tbody').append(markup);
		$("#cold_paging").empty();		
		$("#cold_paging").append(content.pageNumber);	
	  });
}
function getIPCSList(){
	$.get("/admin/getAllIPCSList/" + region_id, function(content, status){
	        	
	        	 var markup = "";
	        	 var tr;
	        	 $('#cold_body').empty();	
	         			for ( var x = 0; x < content.length; x++) {
	         			region = content[x].region;
	         			
	         			 if(content.length - 1 === x) {

	 				        $(".exportToExcel").show();
	 				    }
	 				
	         			
						tr = $('<tr/>');
						tr.append("<td>" + content[x].region + "</td>");
						tr.append("<td>" + content[x].province + "</td>");
						tr.append("<td>" + content[x].municipality + "</td>");
						tr.append("<td>" + content[x].barangay + "</td>");
						tr.append("<td>" + content[x].nameOfIcePlant + "</td>");
						tr.append("<td>" + content[x].code + "</td>");
						tr.append("<td>" + content[x].area + "</td>");
						tr.append("<td>" + content[x].sanitaryPermit + "</td>");
						tr.append("<td>" + content[x].operator + "</td>");
						tr.append("<td>" + content[x].typeOfFacility + "</td>");
						tr.append("<td>" + content[x].structureType + "</td>");
						tr.append("<td>" + content[x].capacity + "</td>");
						tr.append("<td>" + content[x].withValidLicenseToOperate + "</td>");
						tr.append("<td>" + content[x].withValidSanitaryPermit + "</td>");	
						tr.append("<td>" + content[x].withValidSanitaryPermitYes + "</td>");
						tr.append("<td>" + content[x].withValidSanitaryPermitOthers + "</td>");								
						tr.append("<td>" + content[x].businessPermitsAndCertificateObtained + "</td>");
						tr.append("<td>" + content[x].typeOfIceMaker + "</td>");
						tr.append("<td>" + content[x].foodGradule + "</td>");
						tr.append("<td>" + content[x].dataSource + "</td>");
						tr.append("<td>" + content[x].lat + "</td>");
						tr.append("<td>" + content[x].lon + "</td>");
						tr.append("<td>" + content[x].remarks + "</td>");
						$('#cold_body').append(tr);	
						}    		
	    		  });
	}
function getICPSPerPage(){   

		  $('#cold_paging').delegate('a', 'click' , function(){

	        var $this = $(this),
	           target = $this.data('target');    
	           
	        $.get("/admin/ipcs_admin/"+ region_id +"/" + target, function(content, status){

				  
	       	 var markup = "";
	        			for ( var x = 0; x < content.icePlantColdStorage.length; x++) {
	        			  
	        			   markup +="<tr>" +
	        			   "<td><span>" + content.icePlantColdStorage[x].region + "</span></td>" +
	        			   				"<td>"+content.icePlantColdStorage[x].province+"</td><td>"+content.icePlantColdStorage[x].municipality+"</td><td>"+content.icePlantColdStorage[x].barangay+"</td><td>"+content.icePlantColdStorage[x].nameOfIcePlant+"</td><td>"+content.icePlantColdStorage[x].lat+","+content.icePlantColdStorage[x].lon+"</td>" +
	        			   				//"<td class='map menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].uniqueKey + ">MAP</td>" +
	            						//"<td class='view menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].uniqueKey + ">View</td>" +		
	        			   				"</tr>";
						}
	   				$("#cold-table").find('tbody').empty();
	   				$("#cold-table").find('tbody').append(markup);
	   			$("#cold_paging").empty();		
	   			$("#cold_paging").append(content.pageNumber);	
	   		  });
	      });
	      

	   } 
</script>

	</body>

</html>