<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Fish Landing</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
<style>
table,thead,tbody th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>
</head>
<body>

 <jsp:include page="../home/header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">
<jsp:include page="sidenavbar.jsp"></jsp:include>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
    <h1 align="center">FISH LANDING</h1>

 <div class="container"> 
<!--  <button class="create btn-primary">Create</button> -->
 <div class="row">
       <div class="col-md-12 table-responsive">
<table id="fishlanding-table" class="table table-hover">
                <thead>
                  <tr>
                   <th>Region</th>
                   <th>Province</th>
                   <th>Municipality</th>
                   <th>Barangay</th>
                   <th>Name of Landing</th>
                   <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody>  
               </tbody>
              </table>
              <div id="fishlanding_paging">
             </div>
 
    <table class="table table2excel" id="exTable" style="display: none">
                <thead>
                <!--  <tr><th align="center" colspan="17" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr>
                --> <tr><th align="center" colspan="13">FISH LANDING</th></tr>
                
                <tr>

                  <tr>
                  <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Landing</th>
                    <th>Code</th>
                    <th>Area</th>
                    <th>Classification</th>
                    <th>Volume of Unloading MT</th>
                    <th>Type</th>
                    <th>CFlC Status</th>
                    <th>Data Source</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="fishlanding_body">
               </tbody>
              </table>       
    <!-- <button class="exportToExcel btn btn-info">Export to XLS</button>
     -->
     <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','FishLanding')" />
      
      </div>
 
 </div>
  </div>
<!-- end of div content -->
  </div>



  <!-- end wrapper div -->
    </div>

 <script src="/static/js/imageRPM.js" type=" text/javascript"></script>
	<script src="/static/js/form/denr_bfar.js"></script>	
	<script src="/static/js/form/provinceMunBarangay.js"></script>	
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script>
var region_id = "all";

getFishlandingDefaultPage();
getFishLandingPerPage();
getFishLandingList();

$('#Regions').change(function(){
	
	  	var ddlOption = $("#Regions option:selected").text();
	    var ddlValue = $("#Regions option:selected").val();
	    
	    region_id = ddlValue;
	    
	   getFishlandingDefaultPage();
	    getFishLandingPerPage();
	    getFishLandingList();
});

function getFishlandingDefaultPage(){

	$.get("/admin/fishlanding_admin/" + region_id +"/"+ 0, function(content, status) {
		
		var markup = "";
		for (var x = 0; x < content.fishLanding.length; x++) {
			markup += "<tr>" +
					"<td><span>" + content.fishLanding[x].region + "</span><td>"
					+ content.fishLanding[x].province + "</td><td>"
					+ content.fishLanding[x].municipality + "</td><td>"
					+ content.fishLanding[x].barangay + "</td><td>"
					+ content.fishLanding[x].nameOfLanding
					+ "</td><td>" + content.fishLanding[x].lat + ","
					+ content.fishLanding[x].lon + "</td>" +
					//"<td class='map menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">MAP</td>" +
					//"<td class='view menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">View</td>" +
					"</tr>";		}
		$("#fishlanding-table").find('tbody').empty();
		$("#fishlanding-table").find('tbody').append(markup);
		$("#fishlanding_paging").empty();
		$("#fishlanding_paging").append(content.pageNumber);
	});
}
function getFishLandingList(){
	$.get("/admin/getAllfishlandingList/" + region_id, function(content, status) {
	
		var markup = "";
		var tr;
		$('#fishlanding_body').empty();
		
		for (var x = 0; x < content.length; x++) {
			region = content[x].region;
			
			 if(content.length - 1 === x) {
			        $(".exportToExcel").show();
			    }
			
			
			tr = $('<tr/>');
			tr.append("<td>" + content[x].region + "</td>");			
			tr.append("<td>" + content[x].province + "</td>");
			tr.append("<td>" + content[x].municipality + "</td>");
			tr.append("<td>" + content[x].barangay + "</td>");
			tr.append("<td>" + content[x].nameOfLanding + "</td>");
			tr.append("<td>" + content[x].code + "</td>");
			tr.append("<td>" + content[x].area + "</td>");
			tr.append("<td>" + content[x].classification + "</td>");
			tr.append("<td>" + content[x].volumeOfUnloadingMT + "</td>");
			tr.append("<td>" + content[x].type + "</td>");
			tr.append("<td>" + content[x].cflc_status + "</td>");
			tr.append("<td>" + content[x].dataSource + "</td>");
			tr.append("<td>" + content[x].lat + "</td>");
			tr.append("<td>" + content[x].lon + "</td>");
			tr.append("<td>" + content[x].remarks + "</td>");
			$('#fishlanding_body').append(tr);
		}
	});
	}
function getFishLandingPerPage() {
		 $('#fishlanding_paging').delegate('a','click',function() {
				var $this = $(this), 
				target = $this.data('target');

				$.get("/admin/fishlanding_admin/" + region_id +"/" + target, function(content, status) {

					var markup = "";
					for (var x = 0; x < content.fishLanding.length; x++) {
							//console.log(content.fishLanding[x].lat + ","+ content.fishLanding[x].lon);
							markup += "<tr>" +
							"<td><span>" + content.fishLanding[x].region + "</span><td>"
							+ content.fishLanding[x].province + "</td><td>"
							+ content.fishLanding[x].municipality + "</td><td>"
							+ content.fishLanding[x].barangay + "</td><td>"
							+ content.fishLanding[x].nameOfLanding
							+ "</td><td>" + content.fishLanding[x].lat + ","
							+ content.fishLanding[x].lon + "</td>" +
							//"<td class='map menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">MAP</td>" +
							//"<td class='view menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">View</td>" +
							"</tr>";
					}
					$("#fishlanding-table").find('tbody').empty();
					$("#fishlanding-table").find('tbody').append(markup);
					$("#fishlanding_paging").empty();
					$("#fishlanding_paging").append(content.pageNumber);
				});
		 });
	}


</script>
    </body>
</html>