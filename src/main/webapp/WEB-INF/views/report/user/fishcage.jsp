<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Fish Cage</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../../menu/css.jsp"></jsp:include>
</head>
<body>

 <jsp:include page="../../home/header_region.jsp"></jsp:include> 
<div class="wrapper d-flex align-items-stretch text-dark">
<jsp:include page="../sidenavbar.jsp"></jsp:include>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
          <h1 align="center">FISH CAGE</h1>

 <div class="container"> 
 <div class="row">
       <div class="col-md-12 table-responsive">
	<table id="fishcage-table" class="table table-hover">
                 <thead>
                  <tr>
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Operator</th>
                    <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody> 
               </tbody>
              </table>
              <div id="fishcage_paging">
            
             </div>
 
          <table class="table2excel table table-hover" id="exTable" style="display: none">
                <thead>
                <tr><th align="center" colspan="17" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr>
                <tr><th align="center" colspan="16">FISH CAGE</th></tr>
                
                  <tr>
                  	<th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Operator</th>
                    <th>Code</th>
                    <th>Area</th>
                    <th>Classification of Operator</th>
                    <th>Cage Dimension</th>
                  	<th>Cage Design</th>
                    <th>Cage Type</th>
                    <th>Cage no of Compartments</th>
                    <th>Indicate species</th>
                    <th>Source of data</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="fishcage_body">
               </tbody>
              </table>       
    <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','FishCage')" />
      </div>

 </div>
  </div>
 </div>
  </div>
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>		
<script src="/static/js/form/provinceMunBarangay.js"></script>	
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script>
var region_id = "all";
getAllFishCageFirstPage();
getFishCagePerPage();
getFishCageList();


function getAllFishCageFirstPage(){
	$.get("/excel/fishcage_user/" + 0, function(content, status){
  
		var markup = "";
		var ShowHide ="";
		for ( var x = 0; x < content.fishCagePage.length; x++) {
			
			var active = content.fishCagePage[x].enabled;
			if(active){
				ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
			}else{
				ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
			}
		   markup += "<tr>" +
		   "<td><span>"+ content.fishCagePage[x].region+ "</span></td>" +
		   "<td>"+content.fishCagePage[x].province+"</td>" +
		   "<td>"+content.fishCagePage[x].municipality+"</td>" +
		   "<td>"+content.fishCagePage[x].barangay+"</td>" +
		   "<td>"+content.fishCagePage[x].nameOfOperator+"</td>" +
		    "<td>"+content.fishCagePage[x].lat+","+content.fishCagePage[x].lon+"</td>" +
		    ShowHide +
		   "</tr>";
		}
		$("#fishcage-table").find('tbody').empty();
		$("#fishcage-table").find('tbody').append(markup);
	$("#fishcage_paging").empty();		
	$("#fishcage_paging").append(content.pageNumber);	
  });

}
function getFishCagePerPage() {   
    
	  $('#paging').delegate('a', 'click' , function(){    	  
      var $this = $(this),
         target = $this.data('target');    
      	
		 $.get("/excel/fishcage_user/" + target, function(content, status){
			  
			 var markup = "";
			 var ShowHide ="";
					for ( var x = 0; x < content.fishCage.length; x++) {
						
						var active = content.fishCagePage[x].enabled;
						if(active){
							ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
						}else{
							ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
						}
					   markup +="<tr>" +
					   "<td><span>"+ content.fishCage[x].region+ "</span></td>" +
					   "<td>"+content.fishCage[x].province+"</td>" +
					   "<td>"+content.fishCage[x].municipality+"</td>" +
					   "<td>"+content.fishCage[x].barangay+"</td>" +
					   "<td>"+content.fishCage[x].nameOfOperator+"</td>" +
					    "<td>"+content.fishCage[x].lat+","+content.fishCage[x].lon+"</td>" +
					    ShowHide +
					   "</tr>";
					}
					$("#fishcage-table").find('tbody').empty();
					$("#fishcage-table").find('tbody').append(markup);
				$("#fishcage_paging").empty();		
				$("#fishcage_paging").append(content.pageNumber);	
			  });

    });       
 }
function getFishCageList(){
	$.get("/excel/getAllfishcageList", function(content, status){
	        	
	        	 var markup = "";
	        	 var tr;
	        	 $('#fishcage_body').empty();	
	         			for ( var x = 0; x < content.length; x++) {
	         				
	         				 if(content.length - 1 === x) {
	     				        $(".exportToExcel").show();
	     				    }
	     				
						tr = $('<tr/>');
						tr.append("<td>" + content[x].region + "</td>");				
						tr.append("<td>" + content[x].province + "</td>");
						tr.append("<td>" + content[x].municipality + "</td>");
						tr.append("<td>" + content[x].barangay + "</td>");
						tr.append("<td>" + content[x].nameOfOperator + "</td>");
						tr.append("<td>" + content[x].code + "</td>");
						tr.append("<td>" + content[x].area + "</td>");
						tr.append("<td>" + content[x].classificationofOperator + "</td>");
						tr.append("<td>" + content[x].cageDimension + "</td>");
						tr.append("<td>" + content[x].cageDesign + "</td>");
						tr.append("<td>" + content[x].cageType + "</td>");
						tr.append("<td>" + content[x].cageNoOfCompartments + "</td>");
						tr.append("<td>" + content[x].indicateSpecies + "</td>");
						tr.append("<td>" + content[x].sourceOfData + "</td>");
						tr.append("<td>" + content[x].lat + "</td>");
						tr.append("<td>" + content[x].lon + "</td>");
						tr.append("<td>" + content[x].remarks + "</td>");
						$('#fishcage_body').append(tr);	
						}    		
	    		  });
}

</script>
	</body>

</html>