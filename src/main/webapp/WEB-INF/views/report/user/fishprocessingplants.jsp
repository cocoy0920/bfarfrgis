<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Fish Processing</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../../menu/css.jsp"></jsp:include>
</head>
<body>

 <jsp:include page="../../home/header_region.jsp"></jsp:include> 
<div class="wrapper d-flex align-items-stretch text-dark">
<jsp:include page="../sidenavbar.jsp"></jsp:include>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
    <h1 align="center">FISH PROCESSING</h1>
	

 <div class="container"> 
 
 <div class="row">
       <div class="col-md-12 table-responsive">
<table id="processing-table" class="table table-hover">
                <thead>
                  <tr>
                    <th>Region</th><th>Province</th><th>Municipality</th><th>Barangay</th><th>Name of Processing Plants</th><th>Coordinates</th>
                  </tr>
                </thead>
                <tbody>  
               </tbody>
              </table>
              <div id="processing_paging">
             </div>
 
     <table class="table2excel" id="exTable" style="display: none">
                <thead>
                 <tr><th align="center" colspan="20" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr>
                <tr><th align="center" colspan="20">FISH PROCESSING PLANT</th></tr>
                
                  <tr>
                  	<th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Processing Plants</th>
                    <th>Code</th>
					 <th>Name Of Operator</th>
					 <th>Area</th>
					 <th>Operator Classification </th>
					 <th>Processing Technique</th>
					 <th>Processing Environment Classification</th>
					 <th>Plant Registered</th>
					 <th>BFAR Registered</th>
					 <th>Packaging Type </th>
					 <th>Market Reach</th>
					 <th>Business Permits and Certificate obtained</th>
					 <th>Indicate Species</th>
					 <th>Source of Data</th>
					 <th>Latitude</th>
					 <th>Longitude</th>
					 <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="processing_body">
               </tbody>
              </table>       
    
 	<input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','FishProcessing')" />
      
  </div>
<!--   <button id="print-button">
    Print
  </button> -->
 </div>
  </div>
  <!-- end of div content -->
  </div>



  <!-- end wrapper div -->
    </div>

  
 <script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>		
<script src="/static/js/form/provinceMunBarangay.js"></script>
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script>
var region_id = "all";

getFishProcessingPlantDefaultPage();
getFishProcessingPlantList();
getFishProcessingPerPage();

function getFishProcessingPlantDefaultPage(){
	
	$.get("/excel/fishprocessing_user/" + 0,function(content, status) {

		
				var markup = "";
				var ShowHide ="";
				for (var x = 0; x < content.fishProcessingPlantPage.length; x++) {
					
					var active = content.fishProcessingPlantPage[x].enabled;
					
					if(active){
						ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
					}else{
						ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
					}
					markup += "<tr>"+
							"<td><span>"+ content.fishProcessingPlantPage[x].region+ "</span></td>"+
							"<td>"+ content.fishProcessingPlantPage[x].province+ "</td>" +
							"<td>"+ content.fishProcessingPlantPage[x].municipality+"</td>" +
							"<td>"+ content.fishProcessingPlantPage[x].barangay+ "</td>" +
							"<td>"+ content.fishProcessingPlantPage[x].nameOfProcessingPlants+"</td>" +
							"<td>"+ content.fishProcessingPlantPage[x].lat+ ","+ content.fishProcessingPlantPage[x].lon + "</td>"+
							ShowHide + 
							"</tr>";
				}
				$("#processing-table").find('tbody').empty();
				$("#processing-table").find('tbody').append(markup);
				$("#processing_paging").empty();
				$("#processing_paging").append(content.pageNumber);
			});
}
function getFishProcessingPlantList(){
	$.get("/excel/getAllfishprocessingList",function(content, status) {
						
						var markup = "";
						var tr;
						$('#processing_body').empty();
						for (var x = 0; x < content.length; x++) {
							
							if(content.length - 1 === x) {
						        $(".exportToExcel").show();
						    }
							
							tr = $('<tr/>');
							tr.append("<td>"+ content[x].region+ "</td>");
							tr.append("<td>"+ content[x].province+ "</td>");
							tr.append("<td>"+ content[x].municipality+ "</td>");
							tr.append("<td>"+ content[x].barangay+ "</td>");
							tr.append("<td>"+ content[x].nameOfProcessingPlants+ "</td>");
							tr.append("<td>" + content[x].code+ "</td>");
							tr.append("<td>"+ content[x].nameOfOperator+ "</td>");
							tr.append("<td>" + content[x].area+ "</td>");
							tr.append("<td>"+ content[x].operatorClassification+ "</td>");
							tr.append("<td>"+ content[x].processingTechnique+ "</td>");
							tr.append("<td>"+ content[x].processingEnvironmentClassification+ "</td>");
							tr.append("<td>"+ content[x].plantRegistered+ "</td>");
							tr.append("<td>"+ content[x].bfarRegistered+ "</td>");
							tr.append("<td>"+ content[x].packagingType+ "</td>");
							tr.append("<td>"+ content[x].marketReach+ "</td>");
							tr.append("<td>"+ content[x].businessPermitsAndCertificateObtained+ "</td>");
							tr.append("<td>"+ content[x].indicateSpecies+ "</td>");
							tr.append("<td>"+ content[x].sourceOfData+ "</td>");
							tr.append("<td>" + content[x].lat+ "</td>");
							tr.append("<td>" + content[x].lon+ "</td>");
							tr.append("<td>"+ content[x].remarks+ "</td>");
							$('#processing_body').append(tr);
							//ExportTable();
						}
					});

}
function getFishProcessingPerPage(){
	// var trigger = $('ul li a'); 
	$('#processing_paging').delegate('a','click',function() {
				var $this = $(this), 
				target = $this.data('target');

				$.get("/excel/fishprocessing_user/" + target,function(content, status) {

							var markup = "";
							var ShowHide ="";
							for (var x = 0; x < content.fishProcessingPlantPage.length; x++) {
								
								var active = content.fishProcessingPlantPage[x].enabled;
								
								if(active){
									ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
								}else{
									ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
								}
								markup += "<tr>"+
								"<td><span>"+ content.fishProcessingPlantPage[x].region+ "</span></td>"+
								"<td>"+ content.fishProcessingPlantPage[x].province+ "</td>" +
								"<td>"+ content.fishProcessingPlantPage[x].municipality+"</td>" +
								"<td>"+ content.fishProcessingPlantPage[x].barangay+ "</td>" +
								"<td>"+ content.fishProcessingPlantPage[x].nameOfProcessingPlants+"</td>" +
								"<td>"+ content.fishProcessingPlantPage[x].lat+ ","+ content.fishProcessingPlantPage[x].lon + "</td>"+
								ShowHide + 
								//"<td class='map menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].uniqueKey + ">MAP</td>" +
								//"<td class='view menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].uniqueKey + ">View</td>"+ 
								"</tr>";
							}
							$("#processing-table")
									.find('tbody').empty();
							$("#processing-table")
									.find('tbody').append(markup);
							$("#processing_paging").empty();
							$("#processing_paging").append(content.pageNumber);
						});
					});

					
}

</script>
    </body>
</html>