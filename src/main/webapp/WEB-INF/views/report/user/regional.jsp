<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Regional Office</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../../menu/css.jsp"></jsp:include>
</head>
<body>

 <jsp:include page="../../home/header_region.jsp"></jsp:include> 
<div class="wrapper d-flex align-items-stretch text-dark">
<jsp:include page="../sidenavbar.jsp"></jsp:include>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
    <h1 align="center">Regional Office</h1>

 <div class="container"> 
 <div class="row">
       <div class="col-md-12 table-responsive">
<table id="market-table" class="table table-hover">
                <thead>
                  <tr>
                   <th>Region</th>
                   <th>Province</th>
                   <th>Municipality</th>
                   <th>Barangay</th>          
                   <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody>
                 
               </tbody>
              </table>
              <div id="market_paging">
            
             </div>
 
      <table class="table2excel" id="exTable" style="display: none">
                <thead>
                <tr><th align="center" colspan="21" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr>
                <tr><th align="center" colspan="21">Regional Office</th></tr>
                
                  <tr>
                  <th>Region</th>
                   <th>Province</th>
                   <th>Municipality</th>
                   <th>Barangay</th>
                   <th>Head</th>
                   <th>Number of Personnel</th>
                    <th>Data Source</th>
                   <th>Latitude</th>
                   <th>Longitude</th>
                   <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="market_body">
                
               </tbody>
              </table> 
      <!-- <button class="exportToExcel btn-info">Export to XLS</button> -->
      <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','RegionalOffice')" />
      
      </div>
 
 </div>
  </div>
 </div>
  </div>
 <script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>		
<script src="/static/js/form/provinceMunBarangay.js"></script>
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script>

var region_id = "all";

getMarketDefaultPage();
getMarketPerPage();
getMarketList();

function getMarketDefaultPage(){
	$.get("/excel/regional_user/" + 0, function(content, status) {

		var markup = "";
		var ShowHide = "";
		for (var x = 0; x < content.regionalOffice.length; x++) {
			
			var active = content.regionalOffice[x].enabled;
			
			if(active){
				ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
			}else{
				ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
			}
			markup += "<tr>" +
			"<td><span>" + content.regionalOffice[x].region + "</span></td>" +
			"<td>"+ content.regionalOffice[x].province + "</td>" +
			"<td>" + content.regionalOffice[x].municipality + "</td>" +
			"<td>"+ content.regionalOffice[x].barangay + "</td>"+
			"<td>"+ content.regionalOffice[x].lat + "," + content.regionalOffice[x].lon + "</td>" +
			ShowHide +
			"</tr>";
		}
		$("#market-table").find('tbody').empty();
		$("#market-table").find('tbody').append(markup);
		$("#market_paging").empty();
		$("#market_paging").append(content.pageNumber);
	});
}
function getMarketPerPage(){

	$('#market_paging').delegate('a','click',function() {
			var $this = $(this), 
			target = $this.data('target');

			$.get("/excel/regional_user/" + target, function(content, status) {

				var markup = "";
				var ShowHide = "";
				for (var x = 0; x < content.regionalOffice.length; x++) {

					var active = content.regionalOffice[x].enabled;
					
					if(active){
						ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
					}else{
						ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
					}
					markup += "<tr>" +
					"<td><span>" + content.regionalOffice[x].region + "</span></td>" +
					"<td>"+ content.regionalOffice[x].province + "</td>" +
					"<td>" + content.regionalOffice[x].municipality + "</td>" +
					"<td>"+ content.regionalOffice[x].barangay + "</td>"+
					"<td>"+ content.regionalOffice[x].lat + "," + content.regionalOffice[x].lon + "</td>" +
					ShowHide + 
					//"<td class='map menu_links btn-primary' data-href="+ content.market[x].uniqueKey + ">MAP</td>" +
					//"<td class='view menu_links btn-primary' data-href="+ content.market[x].uniqueKey + ">View</td>" +
					"</tr>";
				}
				$("#market-table").find('tbody').empty();
				$("#market-table").find('tbody').append(markup);
				$("#market_paging").empty();
				$("#market_paging").append(content.pageNumber);
			});
			//return target;
	});

}
function getMarketList(){
	$.get("/excel/getAllRegionalList", function(content, status) {

		var markup = "";
		var tr;
		$('#market_body').empty();
		for (var x = 0; x < content.length; x++) {
			region = content[x].region;
			
			 if(content.length - 1 === x) {

			        $(".exportToExcel").show();
			    }
			
			
			tr = $('<tr/>');
			tr.append("<td>" + content[x].region + "</td>");
			tr.append("<td>" + content[x].province + "</td>");
			tr.append("<td>" + content[x].municipality + "</td>");
			tr.append("<td>" + content[x].barangay + "</td>");
			tr.append("<td>" + content[x].head + "</td>");
			tr.append("<td>" + content[x].number_of_personnel + "</td>");
			tr.append("<td>" + content[x].source_of_data + "</td>");
			tr.append("<td>" + content[x].lat + "</td>");
			tr.append("<td>" + content[x].lon + "</td>");
			tr.append("<td>" + content[x].remarks + "</td>");

			$('#market_body').append(tr);
			//ExportTable();
		}
	});
	}

</script>
	</body>

</html>