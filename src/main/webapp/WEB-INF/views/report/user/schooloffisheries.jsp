<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>School of Fisheries</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../../menu/css.jsp"></jsp:include>
</head>
<body>

 <jsp:include page="../../home/header_region.jsp"></jsp:include> 
<div class="wrapper d-flex align-items-stretch text-dark">
<jsp:include page="../sidenavbar.jsp"></jsp:include>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
    <h1 align="center">SCHOOL OF FISHERIES</h1>

 <div class="container"> 
<!--  <button class="create btn-primary">Create</button> -->
 <div class="row">
       <div class="col-md-12 table-responsive">
<table id="school-table" class="table table-hover">
                <thead>
                  <tr>
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of School</th>
                    <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody>  
               </tbody>
              </table>
              <div id="school_paging">
              
             </div>
 
      <table class="table2excel" id="exTable" style="display: none">
                <thead>
                <tr><th align="center" colspan="21" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr>             
                <tr><th align="center" colspan="21">SCHOOL OF FISHERIES</th></tr>
                
                  <tr>
                  <th>Region</th>
                   <th>Province</th>
                   <th>Municipality</th>
                   <th>Barangay</th>
                   <th>Code</th>
                   <th>Area(sqm)</th>
                   <th>Name of School</th>
                   <th>Date Established</th>
                   <th>Number of Student Enrolled</th>
                   <th>Data Source</th>
                   <th>Latitude</th>
                   <th>Longitude</th>
                   <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="school_body">
                
               </tbody>
              </table> 
     
      <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','SchoolofFisheries')" />
      
      </div>
 
 </div>
  </div>
 </div>
  </div>
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>	
<script src="/static/js/form/provinceMunBarangay.js"></script>

<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script>
var region_id = "all";

getSchoolDefaultPage();
getSchoolPerPage();
getSchoolList();

function getSchoolDefaultPage(){

	$.get("/excel/schooloffisheries_user/" + 0, function(content, status) {

		var markup = "";
		var ShowHide = "";
		for (var x = 0; x < content.schoolOfFisheries.length; x++) {
			
			var active = content.schoolOfFisheries[x].enabled;
			
			if(active){
				ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
			}else{
				ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
			}
			markup += "<tr>" +
					"<td><span>" + content.schoolOfFisheries[x].region + "</span></td>" +
							
					"<td>"+ content.schoolOfFisheries[x].province
					+ "</td><td>"
					+ content.schoolOfFisheries[x].municipality
					+ "</td><td>"
					+ content.schoolOfFisheries[x].barangay
					+ "</td><td>" + content.schoolOfFisheries[x].name
					+ "</td><td>" + content.schoolOfFisheries[x].lat
					+ "," + cont + ent.schoolOfFisheries[x].lon
					+ "</td>" +
					ShowHide +
					//"<td class='map menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].uniqueKey + ">MAP</td>" +
					//"<td class='view menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].uniqueKey + ">View</td>" +
					
							"</tr>";
		}
		$("#school-table").find('tbody').empty();
		$("#school-table").find('tbody').append(markup);
		$("#school_paging").empty();
		$("#school_paging").append(content.pageNumber);
	});
}
function getSchoolList(){
	$.get("/excel/getAllSchooloffisheriesList",function(content, status) {

						var markup = "";
						var tr;
						$('#school_body').empty();
						for (var x = 0; x < content.length; x++) {
							
							 if(content.length - 1 === x) {
							       // console.log('loop ends');
							        $(".exportToExcel").show();
							    }
							
							
							tr = $('<tr/>');

							tr.append("<td>"
									+ content[x].region
									+ "</td>");
							tr.append("<td>"
									+ content[x].province
									+ "</td>");
							tr.append("<td>"
									+ content[x].municipality
									+ "</td>");
							tr.append("<td>"
									+ content[x].barangay
									+ "</td>");
							tr.append("<td>" + content[x].code
									+ "</td>");
							tr.append("<td>" + content[x].area
									+ "</td>");
							tr.append("<td>" + content[x].name
									+ "</td>");
							tr.append("<td>"
									+ content[x].dateEstablished
									+ "</td>");
							tr.append("<td>"
											+ content[x].numberStudentsEnrolled
											+ "</td>");
							tr.append("<td>"
									+ content[x].dataSource
									+ "</td>");
							tr.append("<td>" + content[x].lat
									+ "</td>");
							tr.append("<td>" + content[x].lon
									+ "</td>");
							tr.append("<td>" + content[x].remarks
									+ "</td>");

							$('#school_body').append(tr);
							// ExportTable();
						}
					});
	}
function getSchoolPerPage(){
		$('#school_paging').delegate('a','click',function() {
				var $this = $(this), 
				target = $this.data('target');

				$.get("/excel/schooloffisheries_user/" + target, function(content, status) {

					var markup = "";
					var ShowHide ="";
					for (var x = 0; x < content.schoolOfFisheries.length; x++) {
						
						var active = content.schoolOfFisheries[x].enabled;
						
						if(active){
							ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
						}else{
							ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
						}
						markup += "<tr>" +
								"<td><span>" + content.schoolOfFisheries[x].region + "</span></td><td>" +
								+ content.schoolOfFisheries[x].province
								+ "</td><td>"
								+ content.schoolOfFisheries[x].municipality
								+ "</td><td>"
								+ content.schoolOfFisheries[x].barangay
								+ "</td><td>" + content.schoolOfFisheries[x].name
								+ "</td><td>" + content.schoolOfFisheries[x].lat
								+ "," + content.schoolOfFisheries[x].lon
								+ "</td>" +
								ShowHide +
								//"<td class='map menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].uniqueKey + ">MAP</td>" +
								//"<td class='view menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].uniqueKey + ">View</td>" +
										
								"</tr>";
					}
					$("#school-table").find('tbody').empty();
					$("#school-table").find('tbody').append(markup);
					$("#school_paging").empty();
					$("#school_paging").append(content.pageNumber);
				});
		});
	}

</script>
	</body>

</html>