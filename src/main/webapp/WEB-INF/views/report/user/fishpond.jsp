<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Fish Pen</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../../menu/css.jsp"></jsp:include>
</head>
<body>

 <jsp:include page="../../home/header_region.jsp"></jsp:include> 
<div class="wrapper d-flex align-items-stretch text-dark">
<jsp:include page="../sidenavbar.jsp"></jsp:include>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
    <h1 align="center">FISH POND</h1>

 <div class="container"> 
<!--  <button class="create btn-primary">Create</button> -->
 <div class="row">
       <div class="col-md-12 table-responsive">
<table id="fishpen-table" class="table table-hover">
               <thead>
                  <tr>
                    <th>Region</th><th>Province</th><th>Municipality</th><th>Barangay</th><th>Name of Operator</th><th>Coordinates</th>
                  </tr>
                </thead>
                <tbody> 
               </tbody>
              </table>
              <div id="fishpen_paging">
           
             </div>
 
      <table class="table2excel" id="exTable" style="display: none">
                <thead>
                <tr><th align="center" colspan="17" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr>              
                <tr><th align="center" colspan="14">FISH POND</th></tr>
                
                  <tr>
                  <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Operator</th>
                    <th>Name of Occupant</th>
                    <th>Code</th>
                    <th>Area</th>   
                    <th>Source of Data</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="fishpen_body">
                <!--  ${listAll}--> 
               </tbody>
              </table>       
    <!-- <button class="exportToExcel btn-info">Export to XLS</button> -->
     <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','FishPond')" />
      
      </div>
 
 </div>
  </div>
<!-- end of div content -->
  </div>



  <!-- end wrapper div -->
    </div>


<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>
<script src="/static/js/form/provinceMunBarangay.js"></script>
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script>

var region_id = "all";

getFishPenDefaultPage();
getFishPenPerPage();
getFishPenList();

function getFishPenDefaultPage(){

	 $.get("/excel/fishpond_user/" + 0, function(content, status){

	var markup = "";
	var ShowHide ="";
		for ( var x = 0; x < content.fishPond.length; x++) {
			
			var active = content.fishPond[x].enabled;
			if(active){
				ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
			}else{
				ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
			}
			
		   markup +="<tr>" +
		   	"<td><span>" + content.fishPond[x].region + "</span></td>" +
		   	"<td>"+content.fishPond[x].province+"</td>" +
		   	"<td>"+content.fishPond[x].municipality+"</td>" +
		   	"<td>"+content.fishPond[x].barangay+"</td>" +
		   	"<td>"+content.fishPond[x].nameofactualoccupant+"</td>" +
		   	"<td>"+content.fishPond[x].lat+","+content.fishPond[x].lon+"</td>" +
		   	ShowHide+
		  // "<td class='map menu_links btn-primary' data-href="+ content.fishPen[x].uniqueKey + ">MAP</td>" +
			//"<td class='view menu_links btn-primary' data-href="+ content.fishPen[x].uniqueKey + ">View</td>" +
		   	"</tr>";
		}
		$("#fishpen-table").find('tbody').empty();
		$("#fishpen-table").find('tbody').append(markup);
	$("#fishpen_paging").empty();		
	$("#fishpen_paging").append(content.pageNumber);	
});
}
function getFishPenList(){			
	$.get("/excel/getAllfishpondList", function(content, status){
	        	 var markup = "";
	        	 var tr;
	        	 $('#fishpen_body').empty();	
	       
	         			for ( var x = 0; x < content.length; x++) {
	         			region = content[x].region;
	         			
	         			 if(content.length - 1 === x) {
	 				        $(".exportToExcel").show();
	 				    }
	 				
	         			
						tr = $('<tr/>');
						tr.append("<td>" + content[x].region + "</td>");
						tr.append("<td>" + content[x].province + "</td>");
						tr.append("<td>" + content[x].municipality + "</td>");
						tr.append("<td>" + content[x].barangay + "</td>");
						tr.append("<td>" + content[x].nameoffishpondoperator + "</td>");
						tr.append("<td>" + content[x].nameofactualoccupant + "</td>");
						tr.append("<td>" + content[x].code + "</td>");
						tr.append("<td>" + content[x].area + "</td>");
						tr.append("<td>" + content[x].datasource + "</td>");
						tr.append("<td>" + content[x].lat + "</td>");
						tr.append("<td>" + content[x].lon + "</td>");
						tr.append("<td>" + content[x].remarks + "</td>");
						$('#fishpen_body').append(tr);	
						}    		
	    		  });
	}
function getFishPenPerPage(){  
		
		  $('#fishpen_paging').delegate('a', 'click' , function(){
			  var $this = $(this),
	           target = $this.data('target');    

		     		 $.get("/excel/fishpond_user/" + target, function(content, status){
	        	 
	        	 var markup = "";
	        	 var ShowHide ="";
	         			for ( var x = 0; x < content.fishPond.length; x++) {
	         				var active = content.fishPond[x].enabled;
	         				if(active){
	         					ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
	         				}else{
	         					ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
	         				}
	         				
	         			   markup +="<tr>" +
	         			   			"<td><span>" + content.fishPond[x].region + "</span></td>" +
	         			   			"<td>"+content.fishPond[x].province+"</td>" +
	         			   			"<td>"+content.fishPond[x].municipality+"</td>" +
	         			   			"<td>"+content.fishPond[x].barangay+"</td>" +
	         			   			"<td>"+content.fishPond[x].nameofactualoccupant+"</td>" +
	         			   			"<td>"+content.fishPond[x].lat+","+content.fishPond[x].lon+"</td>" +
	         			   			ShowHide+
	         			   			//"<td class='map menu_links btn-primary' data-href="+ content.fishPen[x].uniqueKey + ">MAP</td>" +
	    						//"<td class='view menu_links btn-primary' data-href="+ content.fishPen[x].uniqueKey + ">View</td>" +
	         			   			"</tr>";
						}
	    				$("#fishpen-table").find('tbody').empty();
	    				$("#fishpen-table").find('tbody').append(markup);
	    			$("#fishpen_paging").empty();		
	    			$("#fishpen_paging").append(content.pageNumber);	
	    		  });
	      });        
	   }

</script>
    </body>
</html>