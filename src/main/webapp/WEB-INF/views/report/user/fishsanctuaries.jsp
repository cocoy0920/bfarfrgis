<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Fish Sanctuary</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../../menu/css.jsp"></jsp:include>
<style type="text/css">
.table2excel{
	padding: 5px;
}
</style>
</head>
<body>

 <jsp:include page="../../home/header_region.jsp"></jsp:include> 
<div class="wrapper d-flex align-items-stretch text-dark">
<jsp:include page="../sidenavbar.jsp"></jsp:include>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5 section-to-print" >
    <h1 align="center">FISH SANCTUARY</h1>

<div class="container noToPrint"> 
<!-- <button class="create btn-primary">Create</button> -->
 <div class="row">
       <div class="col-md-12 table-responsive">     
			<table id="export-buttons-table" class="table table-hover">
                <thead>
                  <tr>
        
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of FishSanctuary</th>
                    <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody> 
               </tbody>
              </table>
              <div id="paging">
             <!--${page}  --> 
             </div>

 		<table class="table2excel" id="exTable" style="display: none">
 				
                <thead id="thead_body">
                <!-- <tr><th align="center" colspan="17">Republic of the Philippines</th></tr>
                <tr><th align="center" colspan="17">Department of Agriculture</th></tr>
                <tr><th align="center" colspan="17">Bureau of Fisheries and Aquatic Resources</th></tr>
                <tr><th align="center" colspan="17">Fisheries Resources Geopatial Information System</th></tr> -->
               <%--  <a href="${pageContext.request.contextPath}/static/images/HEAD.png">kiko</a> --%>
               <!-- <tr><th align="center" colspan="17" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr> -->
               <tr><th align="center" colspan="17" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr>
               <%--  <tr><th align="center" colspan="17">FISH SANCTUARY</th></tr>
                
                <tr>
                    <th>${region}</th>
                 </tr>  
                 
                  <tr>
                   <th>Province</th>
                   <th>Municipality</th>
                   <th>Barangay</th>
                   <th>Name of FishSanctuary</th>
                   <th>Code</th>
                   <th>Area</th>
                   <th>Type</th>
                   <th>Data Source</th>
                   <th>Implementer</th>
                   <th>Sheltered</th>
                   <th>Name Of Sensitive Habitat MPA</th>
                   <th>Ordinance No.</th>
                   <th>Ordinance Title</th>
                   <th>Date Established</th>
                   <th>Latitude</th>
                   <th>Longitude</th>
                   <th>Remarks</th>
                  </tr> --%>
                </thead>
                <tbody id="emp_body">
                
               </tbody>
              </table> 
      <!-- <button class="exportToExcel btn-info">Export to XLS</button> -->
      <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','FishSanctuary')" />
      </div>
 
 	</div>
  	</div> 
  	

  </div>
  

<!-- 	<div id="footer">
	<p class="text-white align-center">
	Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved Bureau of Fisheries and Aquatic Resources
	</p>
</div>  -->
    </div>
  
<!-- <script src="/static/js/imageRPM.js"></script>
<script src="/static/js/form/denr_bfar.js"></script>		
<script src="/static/js/excel/jquery.table2excel.js"></script>
 -->
 <script src="/static/js/excel/exportToExcel.js" defer></script>
<!-- <script src="/static/js/form/provinceMunBarangay.js"></script>
<script src="/static/js/form/jspdf.debug.js"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js"></script> -->
<script src="/static/js/api/create_update.js"></script>
<script src="/static/js/excel/user/fishSanctuaryExcelReport.js"></script>

    </body>
</html>