<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Fish Corral(Baklad)</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../../menu/css.jsp"></jsp:include>
</head>
<body>

 <jsp:include page="../../home/header_region.jsp"></jsp:include> 
<div class="wrapper d-flex align-items-stretch text-dark">
<jsp:include page="../sidenavbar.jsp"></jsp:include>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
    <h1 align="center">Fish Corral(Baklad)</h1>

 <!-- <div class="container-fluid text-center">  -->
<div class="container"> 
<!-- <button class="create btn-primary">Create</button> -->
 <div class="row">
       <div class="col-md-12 table-responsive">
		<table id="fishcoral-table" class="table table-hover">
                <thead>
                  <tr>
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Operator</th>
                    <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody> 
               </tbody>
              </table>
              <div id="fishcoral_paging">
              
             </div>
 
    <table class="table2excel" id="exTable" style="display: none">
                <thead>
                 <tr><th align="center" colspan="17" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr>
                <tr><th align="center" colspan="14">FISH CORRALS(BAKLAD)</th></tr>
                
                  <tr>
                   <th>Region</th>
                   <th>Province</th>
                   <th>Municipality</th>
                   <th>Barangay</th>
                   <th>Name of Operator</th>
                   <th>Code</th>
                   <th>Area (sqm.)</th>
                   <th>Operator Classification</th>
                   <th>Name of Station Gear Use</th>
                   <th>Name of Unit Use</th>
                   <th>Fishes Caught</th>
                   <th>Data Source</th>
                   <th>Latitude</th>
                   <th>Longitude</th>
                   <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="fishcoral_body">
                
               </tbody>
              </table> 
     <!--  <button class="exportToExcel btn btn-info">Export to XLS</button> -->
  <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','FishCorals')" />
      
      </div>
 
 </div>
  </div>
 </div>
  </div>
 <script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>	
<script src="/static/js/form/provinceMunBarangay.js"></script>	
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script>
var region_id = "all";

geFishCoralPage();
getFishCoralPerPage();
getFishCoralList();

function getFishCoralPerPage(){

	 $.get("/excel/fishcoral_user/" + 0, function(content, status){


	var markup = "";
	var ShowHide ="";
	for ( var x = 0; x < content.fishCoral.length; x++) {
	  
		var active = content.fishCoral[x].enabled;
		if(active){
			ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
		}else{
			ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
		}
		markup +="<tr>" +
	  "<td><span>" + content.fishCoral[x].region + "</span></td>" +
	   		"<td>"+content.fishCoral[x].province+"</td>" +
	   		"<td>"+content.fishCoral[x].municipality+"</td>" +
	   		"<td>"+content.fishCoral[x].barangay+"</td>" +
	   		"<td>"+content.fishCoral[x].nameOfOperator+"</td>" +
	   		"<td>"+content.fishCoral[x].lat+ "," +content.fishCoral[x].lon+ "</td>" +
	   		ShowHide + 
	   		//	"<td class='map menu_links btn-primary' data-href="+ content.fishCoral[x].uniqueKey + ">MAP</td>" +
	   	//	"<td class='view menu_links btn-primary' data-href="+ content.fishCoral[x].uniqueKey + ">View</td>" +
	   		"</tr>";
	}
	$("#fishcoral-table").find('tbody').empty();
	$("#fishcoral-table").find('tbody').append(markup);
	$("#fishcoral_paging").empty();		
	$("#fishcoral_paging").append(content.pageNumber);	
});
}
function geFishCoralPage(){   

 $('#paging').delegate('a', 'click' , function(){
 
 var $this = $(this),
    target = $this.data('target');    
 	
	 $.get("/excel/fishcoral_user/" + target, function(content, status){
 	 

   	 var markup = "";
   	var ShowHide ="";
    			for ( var x = 0; x < content.fishCoral.length; x++) {
    				
    				 var active = content.fishCoral[x].enabled;
    					if(active){
    						ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
    					}else{
    						ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
    					}
    			   markup +="<tr>" +
    			   			"<td><span>" + content.fishCoral[x].region + "</span></td>" +
    			   			"<td>"+content.fishCoral[x].province+"</td>" +
    			   			"<td>"+content.fishCoral[x].municipality+"</td>" +
    			   			"<td>"+content.fishCoral[x].barangay+"</td>" +
    			   			"<td>"+content.fishCoral[x].nameOfOperator+"</td>" +
    			   			"<td>"+content.fishCoral[x].lat+ "," +content.fishCoral[x].lon+ "</td>" +
    				   		ShowHide + 
    			   		//"<td class='map menu_links btn-primary' data-href="+ content.fishCoral[x].uniqueKey + ">MAP</td>" +
    			   		//"<td class='view menu_links btn-primary' data-href="+ content.fishCoral[x].uniqueKey + ">View</td>" +
    			   			"</tr>";
				}
    			$("#fishcoral-table").find('tbody').empty();
    			$("#fishcoral-table").find('tbody').append(markup);
    			$("#fishcoral_paging").empty();		
    			$("#fishcoral_paging").append(content.pageNumber);	
		  });;
});


}
function getFishCoralList(){	

$.get("/excel/getAllfishcoralList", function(content, status){
       	
       	 var markup = "";
       	 var tr;
       	$('#fishcoral_body').empty();
        			for ( var x = 0; x < content.length; x++) {
        			region = content[x].region;
        			
        			 if(content.length - 1 === x) {
				       // console.log('loop ends');
				        $(".exportToExcel").show();
				    }
				
        			
					tr = $('<tr/>');
					tr.append("<td>" + content[x].region + "</td>");
					tr.append("<td>" + content[x].province + "</td>");
					tr.append("<td>" + content[x].municipality + "</td>");
					tr.append("<td>" + content[x].barangay + "</td>");
					tr.append("<td>" + content[x].nameOfOperator + "</td>");
					tr.append("<td>" + content[x].code + "</td>");
					tr.append("<td>" + content[x].area + "</td>");
					tr.append("<td>" + content[x].operatorClassification + "</td>");
					tr.append("<td>" + content[x].nameOfStationGearUse + "</td>");
					tr.append("<td>" + content[x].noOfUnitUse + "</td>");
					tr.append("<td>" + content[x].fishesCaught + "</td>");
					tr.append("<td>" + content[x].dataSource + "</td>");
					tr.append("<td>" + content[x].lat + "</td>");
					tr.append("<td>" + content[x].lon + "</td>");
					tr.append("<td>" + content[x].remarks + "</td>");
					
					$('#fishcoral_body').append(tr);	
					//ExportTable();
					}    		
   		  });
}



</script>
	</body>

</html>