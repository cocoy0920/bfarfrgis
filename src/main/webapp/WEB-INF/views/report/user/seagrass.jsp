<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Sea Grass</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
	<jsp:include page="../../menu/css.jsp"></jsp:include>
</head>
<body>

 <jsp:include page="../../home/header_region.jsp"></jsp:include> 
<div class="wrapper d-flex align-items-stretch text-dark">
<jsp:include page="../sidenavbar.jsp"></jsp:include>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
    <h1 align="center">SEAGRASS</h1>

 <div class="container"> 
 <div class="row">
       <div class="col-md-12 table-responsive">
<table id="seagrass-table" class="table table-hover">
                <thead>
                  <tr>
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                   <!--  <th>Cultured Method Used</th> -->
                    <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody>  
               </tbody>
              </table>
              <div id="seagrass_paging">
             
             </div>
 
      <table class="table2excel" id="exTable" style="display: none">
                <thead>
                <tr><th align="center" colspan="21" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr>                         
                <tr><th align="center" colspan="14">SEAGRASS</th></tr>
                
                  <tr>
                  <th>Region</th>
                   <th>Province</th>
                   <th>Municipality</th>
                   <th>Barangay</th>
                   <th>Code</th>
                   <th>Area(sqm.)</th>
                   <th>Indicate Genus</th>
                   <!-- <th>Cultured Method Used</th>
                   <th>Cultured Period</th>
                   <th>Volume per Cropping (Kgs.)</th>
                   <th>No. of Cropping per Year</th> -->
                   <th>Data Source</th>
                   <th>Latitude</th>
                   <th>Longitude</th>
                   <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="seagrass_body">
                
               </tbody>
              </table> 
     
      <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','Seagrass')" />
      
      </div>
 
 </div>
  </div>
 </div>
  </div>


<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>

<script src="/static/js/form/provinceMunBarangay.js"></script>
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script>

var region_id = "all";

getSeagrassDefaultPage();
getSeagrassPerPage();
getSeaGrassList();

function getSeagrassDefaultPage(){

	 $.get("/excel/seagrass_user/" + 0, function(content, status){
		 
		 var markup = "";
		 var ShowHide = "";
	 			for ( var x = 0; x < content.seaGrass.length; x++) {
	 				
	 				var active = content.seaGrass[x].enabled;
	 				
	 				if(active){
	 					ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
	 				}else{
	 					ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
	 				}
	 			   markup +="<tr>" +
	 			  "<td><span>" + content.seaGrass[x].region + "</span></td>" +
	 			   	"<td>"+content.seaGrass[x].province+"</td>" +
	 			   	"<td>"+content.seaGrass[x].municipality+"</td>" +
	 			   	"<td>"+content.seaGrass[x].barangay+"</td>" +
//	 			   	"<td>"+content.seaGrass[x].culturedMethodUsed+"</td>" +
	 			   	"<td>"+content.seaGrass[x].lat+","+content.seaGrass[x].lon+"</td>" +
	 			   ShowHide + 
	 			   "</tr>";
				}
				$("#seagrass-table").find('tbody').empty();
				$("#seagrass-table").find('tbody').append(markup);
			$("#seagrass_paging").empty();		
			$("#seagrass_paging").append(content.pageNumber);	
		  });
}
function getSeagrassPerPage(){   

	  $('#seagrass_paging').delegate('a', 'click' , function(){
		  var $this = $(this),
      target = $this.data('target');    
       
			 $.get("/excel/seagrass_user/" + target, function(content, status){
				 
				 var markup = "";
				 var ShowHide = "";
			 			for ( var x = 0; x < content.seaGrass.length; x++) {
			 				var active = content.seaGrass[x].enabled;
			 				
			 				if(active){
			 					ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
			 				}else{
			 					ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
			 				}
			 			   markup +="<tr>" +
			 			  "<td><span>" + content.seaGrass[x].region + "</span>" +
							"</td>" +
			 			   	"<td>"+content.seaGrass[x].province+"</td><td>"+content.seaGrass[x].municipality+"</td><td>"+content.seaGrass[x].barangay+"</td><td>"+content.seaGrass[x].culturedMethodUsed+"</td><td>"+content.seaGrass[x].lat+","+content.seaGrass[x].lon+"</td>" +
			 			   ShowHide + 
			 			   	"</tr>";
						}
						$("#seagrass-table").find('tbody').empty();
						$("#seagrass-table").find('tbody').append(markup);
					$("#seagrass_paging").empty();		
					$("#seagrass_paging").append(content.pageNumber);	
				  });
    });       
 }
function getSeaGrassList(){			
	$.get("/excel/getAllSeagrassList", function(content, status){

	        	 var markup = "";
	        	 var tr;
	        	 $('#seagrass_body').empty();	
	         			for ( var x = 0; x < content.length; x++) {
	         			region = content[x].region;
	         			
	         			 if(content.length - 1 === x) {
	 				        $(".exportToExcel").show();
	 				    }
	 				
						tr = $('<tr/>');
						
						tr.append("<td>" + content[x].province + "</td>");
						tr.append("<td>" + content[x].municipality + "</td>");
						tr.append("<td>" + content[x].barangay + "</td>");
						tr.append("<td>" + content[x].code + "</td>");
						tr.append("<td>" + content[x].area + "</td>");
						tr.append("<td>" + content[x].indicateGenus + "</td>");
//						tr.append("<td>" + content[x].culturedMethodUsed + "</td>");
//						tr.append("<td>" + content[x].culturedPeriod + "</td>");
//						tr.append("<td>" + content[x].volumePerCropping + "</td>");
//						tr.append("<td>" + content[x].noOfCroppingPerYear + "</td>");
						tr.append("<td>" + content[x].dataSource + "</td>");
						tr.append("<td>" + content[x].lat + "</td>");
						tr.append("<td>" + content[x].lon + "</td>");
						tr.append("<td>" + content[x].remarks + "</td>");
						
						$('#seagrass_body').append(tr);	
						//ExportTable();
						}    		
	    		  });
}


</script>
	</body>

</html>