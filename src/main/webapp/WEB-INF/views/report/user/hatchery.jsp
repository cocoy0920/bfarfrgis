<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>Hatchery</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../../menu/css.jsp"></jsp:include>
</head>
<body>

 <jsp:include page="../../home/header_region.jsp"></jsp:include> 
<div class="wrapper d-flex align-items-stretch text-dark">
<jsp:include page="../sidenavbar.jsp"></jsp:include>
  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5">
    <h1 align="center">HATCHERY</h1>

 <div class="container"> 
 <div class="row">
       <div class="col-md-12 table-responsive">
<table id="hatchery-table" class="table table-hover">
                <thead>
                  <tr>
                    <th>Region</th><th>Province</th><th>Municipality</th><th>Barangay</th><th>Name of Hatchery</th><th>Coordinates</th>
                  </tr>
                </thead>
                <tbody> 
               </tbody>
              </table>
              <div id="hatchery_paging">
              
             </div>
 
           <table class="table2excel" id="exTable" style="display: none">
                <thead>
         		<tr><th align="center" colspan="21" style="height: 200px"><img src="https://frgis.bfar.da.gov.ph/static/images/HEAD.png"></th></tr>              
                <tr><th align="center" colspan="21">HATCHERY</th></tr>
                
                  <tr>
                  	<th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Hatchery</th>
                    <th>Legislated</th>
                     <th>Status</th>
                      <th>RA#</th>
                     <th>Title</th>
                     <th>Name of Hatchery</th>
                     <th>Name of Operator</th>
                     <th>Address of Operator</th>
                     <th>Operator Classification</th>
                    <th>Type</th>
                     <th>Indicate Species</th>                     
                      <th>Stocking Density/Cycle</th>
                       <th>Cycle/Year</th>
                       <th>Actual Production for the year</th>
                      
                          <th>Data Sources</th>
                           <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Remarks</th>   
                  </tr>
                </thead>
                <tbody id="hatchery_body">
               </tbody>
              </table>       
    <!-- <button class="exportToExcel btn-info">Export to XLS</button>
     -->
     <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','Hatchery')" />
      
       </div>
 
 </div>
  </div>
 </div>
  </div>
  
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>
<script src="/static/js/form/denr_bfar.js"></script>	
<script src="/static/js/form/provinceMunBarangay.js"></script>	
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>
<script>
var region_id = "all";

getHacheryDefaultPerPage();
getHatcheryPerPage();
getHatcheryList();


function getHacheryDefaultPerPage(){
	
	 $.get("/excel/hatchery_user/" + 0, function(content, status){
   	 
    	
   	 var markup = "";
   	var ShowHide ="";
    			for ( var x = 0; x < content.hatcheries.length; x++) {
    				
    				var active = content.hatcheries[x].enabled;
    				
    				if(active){
    					ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
    				}else{
    					ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
    				}
    			   markup +="<tr>" +
    			   			"<td><span>" + content.hatcheries[x].region + "</span></td>" +
    			   			"<td>"+content.hatcheries[x].province+"</td>" +
    			   			"<td>"+content.hatcheries[x].municipality+"</td>" +
    			   			"<td>"+content.hatcheries[x].barangay+"</td>" +
    			   			"<td>"+content.hatcheries[x].nameOfHatchery+"</td>" +
    			   			"<td>"+content.hatcheries[x].lat+","+content.hatcheries[x].lon+"</td>" +
    			   			ShowHide + 
    			   			//"<td class='map menu_links btn-primary' data-href="+ content.hatcheries[x].uniqueKey + ">MAP</td>" +
    			   			//"<td class='view menu_links btn-primary' data-href="+ content.hatcheries[x].uniqueKey + ">View</td>" +			
    			   			"</tr>";
				}
				$("#hatchery-table").find('tbody').empty();
				$("#hatchery-table").find('tbody').append(markup);
			$("#hatchery_paging").empty();		
			$("#hatchery_paging").append(content.pageNumber);	
		  });
}
function getHatcheryList(){
	$.get("/excel/getAllhatcheryList", function(content, status){
	        	
	        	 var markup = "";
	        	 var tr;
	        	 $('#hatchery_body').empty();	
	         			for ( var x = 0; x < content.length; x++) {
	         			region = content[x].region;
	         			
	         			 if(content.length - 1 === x) {
	 				        $(".exportToExcel").show();
	 				    }
	 				
	         			
						tr = $('<tr/>');
						tr.append("<td>" + content[x].region + "</td>");
						tr.append("<td>" + content[x].province + "</td>");
						tr.append("<td>" + content[x].municipality + "</td>");
						tr.append("<td>" + content[x].barangay + "</td>");
						tr.append("<td>" + content[x].nameOfHatchery + "</td>");
						tr.append("<td>" + content[x].legislated + "</td>");
						tr.append("<td>" + content[x].status + "</td>");
						tr.append("<td>" + content[x].ra + "</td>");
						tr.append("<td>" + content[x].title + "</td>");
						tr.append("<td>" + content[x].area + "</td>");
						tr.append("<td>" + content[x].nameOfHatchery + "</td>");
						tr.append("<td>" + content[x].nameOfOperator + "</td>");
						tr.append("<td>" + content[x].addressOfOperator + "</td>");
						tr.append("<td>" + content[x].operatorClassification + "</td>");					
						tr.append("<td>" + content[x].publicprivate + "</td>");
						tr.append("<td>" + content[x].indicateSpecies + "</td>");
						tr.append("<td>" + content[x].prodStocking + "</td>");
						tr.append("<td>" + content[x].prodCycle + "</td>");
						tr.append("<td>" + content[x].actualProdForTheYear + "</td>");
						tr.append("<td>" + content[x].dataSource + "</td>");
						tr.append("<td>" + content[x].lat + "</td>");
						tr.append("<td>" + content[x].lon + "</td>");
						tr.append("<td>" + content[x].remarks + "</td>");
						
						$('#hatchery_body').append(tr);	
						}    		
	    		  });
				}
function getHatcheryPerPage(){   

		  $('#hatchery_paging').delegate('a', 'click' , function(){
			  var $this = $(this),
	           target = $this.data('target');    

			  $.get("/excel/hatchery_user/" + target, function(content, status){
		        	 
		         	
		        	 var markup = "";
		        	 var ShowHide ="";
		         			for ( var x = 0; x < content.hatcheries.length; x++) {
		         				var active = content.hatcheries[x].enabled;
		        				
		        				if(active){
		        					ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
		        				}else{
		        					ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
		        				}
		         			   markup +="<tr>" +
		         			  "<td><span>" + content.hatcheries[x].region + "</span></td>" +
		         			   				"<td>"+content.hatcheries[x].province+"</td>" +
		         			   				"<td>"+content.hatcheries[x].municipality+"</td>" +
		         			   				"<td>"+content.hatcheries[x].barangay+"</td>" +
		         			   				"<td>"+content.hatcheries[x].nameOfHatchery+"</td>" +
		         			   				"<td>"+content.hatcheries[x].lat+","+content.hatcheries[x].lon+"</td>" +
		         			   			ShowHide +
		         			   				//"<td class='map menu_links btn-primary' data-href="+ content.hatcheries[x].uniqueKey + ">MAP</td>" +
		         			   			//"<td class='view menu_links btn-primary' data-href="+ content.hatcheries[x].uniqueKey + ">View</td>" +		
		         			   				"</tr>";
							}
		    				$("#hatchery-table").find('tbody').empty();
		    				$("#hatchery-table").find('tbody').append(markup);
		    			$("#hatchery_paging").empty();		
		    			$("#hatchery_paging").append(content.pageNumber);	
		    		  });
	      });
	      

	   }

</script>

	</body>

</html>