
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<nav id="sidebar">
<!-- <div class="custom-menu">
<button type="button" id="sidebarCollapse" class="btn btn-primary">
<i class="fa fa-bars"></i>
<span class="sr-only">Toggle Menu</span>
</button>
</div> -->
<!-- <div class="p-4"> -->
<div class="sidebar-header">
      <img class=" img img-fluid" alt="" src="/static/images/FRGIS-LOGO.png" height="90px;" width="90px;">
    </div>
<ul class="list-unstyled components mb-5">

<!--        <li>      
      <a href="#provinceList" data-toggle="collapse" aria-expanded="false">
      <span class="text">Filter by Province<i class="fas fa-list"></i></span></a>       
			<select name="provincemap"  id="provincemap" class="form-control provincemap"></select>															
      </li> -->
      
      	  		<hr>
 
            
           <!--  <li>
            <a href="/create/fishcage/pdf">pdf</a>
            </li>
            <li>
            <a href="/create/fishcage/excel">excel</a>
            </li> -->
 <%--         <li><a class="dropdown-item" href="/admin/fishsanctuaryExcel"><img src="/static/images/iconCircle/fishsanctuary.png" style="width: 20px; height: 20px;"/>  Fish Sanctuary</a></li>
          <li><a class="dropdown-item" href="/admin/fishprocessingExcel"><img src="/static/images/iconCircle/fishprocessing.png" style="width: 20px; height: 20px;"/> Fish Processing</a></li>
          <li><a class="dropdown-item" href="/admin/fishlandingExcel"><img src="/static/images/iconCircle/fish-landing.png" style="width: 20px; height: 20px;"/> Fish Landing</a></li>
          <li><a class="dropdown-item" href="/admin/fishportExcel"><img src="/static/images/iconCircle/fishport.png" style="width: 20px; height: 20px;"/> Fish Port</a></li>
          <li><a class="dropdown-item" href="/admin/fishpenExcel"><img src="/static/images/iconCircle/fishpen.png" style="width: 20px; height: 20px;"/>Fish Pen</a></li>
          <li><a class="dropdown-item" href="/admin/fishcageExcel"><img src="/static/images/iconCircle/fishcage.png" style="width: 20px; height: 20px;"/> Fish Cage</a></li>
          <li><a class="dropdown-item" href="/admin/hatcheryExcel"><img src="/static/images/iconCircle/hatcheries.png" style="width: 20px; height: 20px;"/>Hatchery</a></li>
          <li><a class="dropdown-item" href="/admin/ipcsExcel"><img src="/static/images/iconCircle/iceplant.png" style="width: 20px; height: 20px;"/>Cold Storage</a></li>
          <li><a class="dropdown-item" href="/admin/marketExcel"><img src="/static/images/iconCircle/market.png" style="width: 20px; height: 20px;"/>Market</a></li>
          <li><a class="dropdown-item" href="/admin/schooloffisheriesExcel"><img src="/static/images/iconCircle/schoolof-fisheries.png" style="width: 20px; height: 20px;"/>School of Fisheries</a></li>
          <li><a class="dropdown-item" href="/admin/fishcoralExcel"><img src="/static/images/iconCircle/fishcorral.png" style="width: 20px; height: 20px;"/>Fish Corral(Baklad)</a></li>
          <li><a class="dropdown-item" href="/admin/seagrassExcel"><img src="/static/images/iconCircle/seagrass.png" style="width: 20px; height: 20px;"/>Seagrass</a></li>
          <li><a class="dropdown-item" href="/admin/seaweedsExcel"><img src="/static/images/iconCircle/seaweeds.png" style="width: 20px; height: 20px;"/>Seaweeds</a></li>
          <li><a class="dropdown-item" href="/admin/mangroveExcel"><img src="/static/images/iconCircle/mangrove.png" style="width: 20px; height: 20px;"/>Mangrove</a></li>
          <li><a class="dropdown-item" href="/admin/mariculturezoneExcel"> <img src="/static/images/iconCircle/mariculturezone.png" style="width: 20px; height: 20px;"/>Mariculture Zone</a></li>
          <li><a class="dropdown-item" href="/admin/lguExcel"><img src="/static/images/iconCircle/lgu.png" style="width: 20px; height: 20px;"/>PFO</a></li>
          <li><a class="dropdown-item" href="/admin/trainingcenterExcel"><img src="/static/images/iconCircle/fisheriestraining.png" style="width: 20px; height: 20px;"/>Training Center</a></li>
	
      <li><a href="${pageContext.request.contextPath}/admin/bar/all">Chart Report</a></li>

 --%>
      <sec:authorize access="hasRole('ROLE_SUPERADMIN')">
      	 <li>      
      <a href="#regionList" data-toggle="collapse" aria-expanded="false">
      <span class="text">Filter by Region<i class="fas fa-list"></i></span></a>       
			<!-- <select name="region"  id="region" class="form-control region"></select> -->
			<select id="Regions" class="form-control">
						  				<option value = "all">select Region</option>
						  				<option value = "010000000">Region 1</option>
						  				<option value = "020000000">Region 2</option>
						  				<option value = "030000000">Region 3</option>
						  				<option value = "040000000">Region 4-A</option>
						  				<option value = "050000000">Region 5</option>
						  				<option value = "060000000">Region 6</option>
						  				<option value = "070000000">Region 7</option>
						  				<option value = "080000000">Region 8</option>
						  				<option value = "090000000">Region 9</option>
						  				<option value = "100000000">Region 10</option>
						  				<option value = "110000000">Region 11</option>
						  				<option value = "120000000">Region 12</option>
						  				<option value = "130000000">NCR</option>
						  				<option value = "140000000">CAR</option>
						  				<option value = "150000000">ARMM</option>
						  				<option value = "160000000">CARAGA</option>
						  				<option value = "170000000">Region 4-B</option>
						  				</select>															
      </li>
           <span class="text">Resources<i class="fas fa-list"></i></span>
         <!--   <li><a class="dropdown-item" href="/admin/fishsanctuaryExcel"><img src="/static/images/iconCircle/fishsanctuary.png" style="width: 20px; height: 20px;"/>  Fish Sanctuary</a></li> -->
          <li><a class="dropdown-item" href="/create/fishsanctuary"><img src="/static/images/iconCircle/fishsanctuary.png" style="width: 20px; height: 20px;"/>  Fish Sanctuary</a></li>
          <li><a class="dropdown-item" href="/admin/fishprocessingExcel"><img src="/static/images/iconCircle/fishprocessing.png" style="width: 20px; height: 20px;"/> Fish Processing</a></li>
          <li><a class="dropdown-item" href="/admin/fishlandingExcel"><img src="/static/images/iconCircle/fish-landing.png" style="width: 20px; height: 20px;"/> Fish Landing</a></li>
          <li><a class="dropdown-item" href="/admin/fishportExcel"><img src="/static/images/iconCircle/fishport.png" style="width: 20px; height: 20px;"/> Fish Port</a></li>
          <li><a class="dropdown-item" href="/admin/fishpenExcel"><img src="/static/images/iconCircle/fishpen.png" style="width: 20px; height: 20px;"/>Fish Pen</a></li>
          <li><a class="dropdown-item" href="/admin/fishcageExcel"><img src="/static/images/iconCircle/fishcage.png" style="width: 20px; height: 20px;"/> Fish Cage</a></li>
          <li><a class="dropdown-item" href="/admin/hatcheryExcel"><img src="/static/images/iconCircle/hatcheries.png" style="width: 20px; height: 20px;"/>Hatchery</a></li>
          <li><a class="dropdown-item" href="/admin/ipcsExcel"><img src="/static/images/iconCircle/iceplant.png" style="width: 20px; height: 20px;"/>Cold Storage</a></li>
          <li><a class="dropdown-item" href="/admin/marketExcel"><img src="/static/images/iconCircle/market.png" style="width: 20px; height: 20px;"/>Market</a></li>
          <li><a class="dropdown-item" href="/admin/schooloffisheriesExcel"><img src="/static/images/iconCircle/schoolof-fisheries.png" style="width: 20px; height: 20px;"/>School of Fisheries</a></li>
          <li><a class="dropdown-item" href="/admin/fishcoralExcel"><img src="/static/images/iconCircle/fishcorral.png" style="width: 20px; height: 20px;"/>Fish Corral(Baklad)</a></li>
          <li><a class="dropdown-item" href="/admin/seagrassExcel"><img src="/static/images/iconCircle/seagrass.png" style="width: 20px; height: 20px;"/>Seagrass</a></li>
          <li><a class="dropdown-item" href="/admin/seaweedsExcel"><img src="/static/images/iconCircle/seaweeds.png" style="width: 20px; height: 20px;"/>Seaweeds</a></li>
          <li><a class="dropdown-item" href="/admin/mangroveExcel"><img src="/static/images/iconCircle/mangrove.png" style="width: 20px; height: 20px;"/>Mangrove</a></li>
          <li><a class="dropdown-item" href="/admin/mariculturezoneExcel"> <img src="/static/images/iconCircle/mariculturezone.png" style="width: 20px; height: 20px;"/>Mariculture Zone</a></li>
          <li><a class="dropdown-item" href="/admin/lguExcel"><img src="/static/images/iconCircle/lgu.png" style="width: 20px; height: 20px;"/>PFO</a></li>
          <li><a class="dropdown-item" href="/admin/trainingcenterExcel"><img src="/static/images/iconCircle/fisheriestraining.png" style="width: 20px; height: 20px;"/>Training Center</a></li>
	
      <li><a href="${pageContext.request.contextPath}/admin/bar/all">Chart Report</a></li>
 </sec:authorize>
       <sec:authorize access="hasRole('ROLE_USER')or hasRole('ROLE_REGIONALADMIN') ">
           <li><a class="dropdown-item" href="/excel/fishsanctuaryExcel"><img src="/static/images/iconCircle/fishsanctuary.png" style="width: 20px; height: 20px;"/>  Fish Sanctuary</a></li>
             
         <!--   <li><a class="dropdown-item" href="/create/fishsanctuary"><img src="/static/images/iconCircle/fishsanctuary.png" style="width: 20px; height: 20px;"/>  Fish Sanctuary</a></li>
          -->
          <li><a class="dropdown-item" href="/excel/fishprocessingExcel"><img src="/static/images/iconCircle/fishprocessing.png" style="width: 20px; height: 20px;"/> Fish Processing</a></li>
          <li><a class="dropdown-item" href="/excel/fishlandingExcel"><img src="/static/images/iconCircle/fish-landing.png" style="width: 20px; height: 20px;"/> Fish Landing</a></li>
          <li><a class="dropdown-item" href="/excel/fishportExcel"><img src="/static/images/iconCircle/fishport.png" style="width: 20px; height: 20px;"/> Fish Port</a></li>
          <li><a class="dropdown-item" href="/excel/fishpenExcel"><img src="/static/images/iconCircle/fishpen.png" style="width: 20px; height: 20px;"/>Fish Pen</a></li>
          <li><a class="dropdown-item" href="/excel/fishpondExcel"><img src="/static/images/iconCircle/fish-pond.png" style="width: 20px; height: 20px;"/>Fish Pond</a></li>
          <li><a class="dropdown-item" href="/excel/fishcageExcel"><img src="/static/images/iconCircle/fishcage.png" style="width: 20px; height: 20px;"/> Fish Cage</a></li>
          <li><a class="dropdown-item" href="/excel/hatcheryExcel"><img src="/static/images/iconCircle/hatcheries.png" style="width: 20px; height: 20px;"/>Hatchery</a></li>
          <li><a class="dropdown-item" href="/excel/ipcsExcel"><img src="/static/images/iconCircle/iceplant.png" style="width: 20px; height: 20px;"/>Cold Storage</a></li>
          <li><a class="dropdown-item" href="/excel/marketExcel"><img src="/static/images/iconCircle/market.png" style="width: 20px; height: 20px;"/>Market</a></li>
          <li><a class="dropdown-item" href="/excel/payaoExcel"><img src="/static/images/iconCircle/market.png" style="width: 20px; height: 20px;"/>Payao</a></li>
          <li><a class="dropdown-item" href="/excel/nationalExcel"><img src="/static/images/iconCircle/market.png" style="width: 20px; height: 20px;"/>National Center</a></li>   
         <li><a class="dropdown-item" href="/excel/regionalExcel"><img src="/static/images/iconCircle/market.png" style="width: 20px; height: 20px;"/>Regional Office</a></li>   
          
          <li><a class="dropdown-item" href="/excel/TOSExcel"><img src="/static/images/iconCircle/schoolof-fisheries.png" style="width: 20px; height: 20px;"/>TOS</a></li>
          <li><a class="dropdown-item" href="/excel/schooloffisheriesExcel"><img src="/static/images/iconCircle/schoolof-fisheries.png" style="width: 20px; height: 20px;"/>School of Fisheries</a></li>
          
          <li><a class="dropdown-item" href="/excel/fishcoralExcel"><img src="/static/images/iconCircle/fishcorral.png" style="width: 20px; height: 20px;"/>Fish Corral(Baklad)</a></li>
          <li><a class="dropdown-item" href="/excel/seagrassExcel"><img src="/static/images/iconCircle/seagrass.png" style="width: 20px; height: 20px;"/>Seagrass</a></li>
          <li><a class="dropdown-item" href="/excel/seaweedsExcel"><img src="/static/images/iconCircle/seaweeds.png" style="width: 20px; height: 20px;"/>Seaweeds</a></li>
          <li><a class="dropdown-item" href="/excel/mangroveExcel"><img src="/static/images/iconCircle/mangrove.png" style="width: 20px; height: 20px;"/>Mangrove</a></li>
          <li><a class="dropdown-item" href="/excel/mariculturezoneExcel"> <img src="/static/images/iconCircle/mariculturezone.png" style="width: 20px; height: 20px;"/>Mariculture Zone</a></li>
          <li><a class="dropdown-item" href="/excel/lguExcel"><img src="/static/images/iconCircle/lgu.png" style="width: 20px; height: 20px;"/>PFO</a></li>
          <li><a class="dropdown-item" href="/excel/trainingcenterExcel"><img src="/static/images/iconCircle/fisheriestraining.png" style="width: 20px; height: 20px;"/>Training Center</a></li>
	
      <li><a href="/create/bar">Chart Report</a></li>
 </sec:authorize>


      <sec:authorize access="isAuthenticated()">
       <li><a href="/logout"><i class="fas fa-sign-out-alt"></i>LOGOUT</a></li>
      </sec:authorize>

</ul>
</nav>