  
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
<head>
<title>FRGIS - CHART</title>
<meta charset="utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />
  <jsp:include page="../menu/css.jsp"></jsp:include>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- <script type="text/javascript" src="https://www.google.com/jsapi"></script>
 -->
<script type="text/javascript">
	$(document).ready(function() {
		getRregions();
		var chartData="";
		var chartContent="";
		var provinceChartContent="";
		
		var FS = 0;
		var FP = 0;
		var FL = 0;
		var FPT = 0;
		var FPEN = 0;
		var FCage = 0;
		var IPCS = 0;
		var Market = 0;
		var School = 0;
		var FishCorral = 0;
		var Seagrass = 0;
		var Seaweeds = 0;
		var Mangrove = 0;
		var LGU = 0;
		var Mariculture = 0;
		var Trainingcenter = 0;
		var Hatchery = 0;
		
		var region="";
		var province="";
		var province_id = "";
		
/* 		 var selected = new Array();
		
		  $('button').on('click', function() {
	            var array = [];
	            $("input:checkbox[name=check_province]:checked").each(function() {
	            	selected.push($(this).val());
	            });
	            console.log(selected);
	        });
		 */
		  $(".region").change(function(){
			  $(".region_chart").show();
			  $(".province_chart").hide();
			  region = $("#region option:selected").text();
			  region_id = $("#region option:selected").val();
		
			//  console.log(region + region_id + province_id);
			  var url = "/admin/bar_per_region/" + region_id;
			  /*  if (url) {
	              window.location = url;
	          } */
			  $.get("/admin/barPerRegion/" + region_id, function(content, status) {
				  
				  chartContent = content;
				  //		console.log("chartContent.length: " + chartContent.length);
				 
				  		
				  for (var i = 0; i < chartContent.length; i++) {
					  
					 if("Fish Sanctuary" === chartContent[i].resources){
						 FS = chartContent[i].total;
					 }
					 if("Fish Processing" === chartContent[i].resources){
						 FP = chartContent[i].total;
					 }
					 if("Fish Landing" === chartContent[i].resources){
						 FL = chartContent[i].total;
					 }
					 if("Fish Port" === chartContent[i].resources){
						 FPT = chartContent[i].total;
					 }
					 if("Fish Pen" === chartContent[i].resources){
						 FPEN = chartContent[i].total;
					 }
					 if("Fish Cage" === chartContent[i].resources){
						 FCage = chartContent[i].total;
					 }
					 if("Cold Storage" === chartContent[i].resources){
						 IPCS = chartContent[i].total;
					 }
					 if("Market" === chartContent[i].resources){
						 Market = chartContent[i].total;
					 }
					 if("School of Fisheries" === chartContent[i].resources){
						 School = chartContent[i].total;
					 }
					 if("Seagrass" === chartContent[i].resources){
						 Seagrass = chartContent[i].total;
					 }
					 if("Seaweeds" === chartContent[i].resources){
						 Seaweeds = chartContent[i].total;
					 }
					 if("Mangrove" === chartContent[i].resources){
						 Mangrove = chartContent[i].total;
					 }
					 if("Fish Corral" === chartContent[i].resources){
						 FishCorral = chartContent[i].total;
					 }
					 if("Mariculture Zone" === chartContent[i].resources){
						 Mariculture = chartContent[i].total;
					 }
					 if("PFO" === chartContent[i].resources){
						 LGU = chartContent[i].total;
					 }
					 if("Training Center" === chartContent[i].resources){
						 Trainingcenter = chartContent[i].total;
					 }
					 if("Hatchery" === chartContent[i].resources){
						 Hatchery = chartContent[i].total;
					 }
					 /*  if(chartContent.length - i == 1){
						  console.log(true);
						  chartData += '[' + "\'" + chartContent[i].resources + "\'" + ',' + chartContent[i].total + ',' + "\'" + chartContent[i].color + "\'" + ',' + chartContent[i].total + ']'
					  }else{
						  console.log(false);
	     				chartData += '['  + "\'" + chartContent[i].resources + "\'"  +',' + chartContent[i].total + ',' + "\'" + chartContent[i].color + "\'" + ',' + chartContent[i].total + ']' + ','
					  } */
					  }
				  
				  drawChartAllResources();
				
			  });
	          	          
	          return false;
	        
	          
		  });
		 
		  $(".provincemap").change(function(){
			  
			  $(".province_chart").show();
			   province = $("#provincemap option:selected").text();
			  var province_id = $("#provincemap option:selected").val();
			   //console.log("Regions: " + region_id + " : " + "Province: " + province_id + " : " + ddlOption);
			  
			   $.get("/admin/barPerProvince/" + province_id, function(content, status) {
					  
				   provinceChartContent = content;
					  		
					  for (var i = 0; i < provinceChartContent.length; i++) {
						  
						 if("Fish Sanctuary" === provinceChartContent[i].resources){
							 FS = provinceChartContent[i].total;
						 }
						 if("Fish Processing" === provinceChartContent[i].resources){
							 FP = provinceChartContent[i].total;
						 }
						 if("Fish Landing" === provinceChartContent[i].resources){
							 FL = provinceChartContent[i].total;
						 }
						 if("Fish Port" === provinceChartContent[i].resources){
							 FPT = provinceChartContent[i].total;
						 }
						 if("Fish Pen" === provinceChartContent[i].resources){
							 FPEN = provinceChartContent[i].total;
						 }
						 if("Fish Cage" === provinceChartContent[i].resources){
							 FCage = provinceChartContent[i].total;
						 }
						 if("Cold Storage" === provinceChartContent[i].resources){
							 IPCS = provinceChartContent[i].total;
						 }
						 if("Market" === provinceChartContent[i].resources){
							 Market = provinceChartContent[i].total;
						 }
						 if("School of Fisheries" === provinceChartContent[i].resources){
							 School = provinceChartContent[i].total;
						 }
						 if("Seagrass" === provinceChartContent[i].resources){
							 Seagrass = provinceChartContent[i].total;
						 }
						 if("Seaweeds" === provinceChartContent[i].resources){
							 Seaweeds = provinceChartContent[i].total;
						 }
						 if("Mangrove" === provinceChartContent[i].resources){
							 Mangrove = provinceChartContent[i].total;
						 }
						 if("Fish Corral" === provinceChartContent[i].resources){
							 FishCorral = provinceChartContent[i].total;
						 }
						 if("Mariculture Zone" === provinceChartContent[i].resources){
							 Mariculture = provinceChartContent[i].total;
						 }
						 if("PFO" === provinceChartContent[i].resources){
							 LGU = provinceChartContent[i].total;
						 }
						 if("Training Center" === provinceChartContent[i].resources){
							 Trainingcenter = provinceChartContent[i].total;
						 }
						 if("Hatchery" === provinceChartContent[i].resources){
							 Hatchery = provinceChartContent[i].total;
						 }

						  }
					
					  drawChart();
					 
				  });
			  
		  });

		google.charts.load("current", {packages:['corechart']});
	    // Set a callback to run when the Google Visualization API is loaded.
		//  google.setOnLoadCallback(drawAllChart); 
	      google.setOnLoadCallback(drawChart); 
	      google.setOnLoadCallback(drawChartAllResources); 
	      google.setOnLoadCallback(drawChartAllRegionResources); 
	      
	

		function drawChart() {

		var barchart = new google.visualization.ColumnChart(document.getElementById('chart_div'));			
 		var data = google.visualization.arrayToDataTable([
     		['Resources', 'Total', { role: 'style'}, { role: 'annotation' }],
     			/* <c:forEach items="${provinceChart}" var="entry">
         			[ '${entry.resources}', ${entry.total},'${entry.color}' , ${entry.total}],
     			</c:forEach> */
     			
     			['Fish Sanctuary',FS,'#FFD124',FS],
     			['Fish Processing',FP,'#EAEA7F',FP],
     			['Fish Landing',FL,'#5EE6EB',FL],
     			['Fish Port',FPT,'#E2D784',FPT],
     			['Fish Pen',FPEN,'#4D96FF',FPEN],
     			['Fish Cage',FCage,'#FF1700',FCage],
     			['Cold Storage',IPCS,'#97DBAE',IPCS],
     			['Market',Market,'#D49B54',Market],
     			['School of Fisheries',School,'#C74B50',School],
     			['Seagrass',Seagrass,'#6BCB77',Seagrass],
     			['Seaweeds',Seaweeds,'#019267',Seaweeds],
     			['Mangrove',Mangrove,'#05595B',Mangrove],
     			['Fish Corral',FishCorral,'#000957',FishCorral],
     			['Mariculture Zone',Mariculture,'#F55353',Mariculture],
     			['PFO',LGU,'#FF6464',LGU],
     			['Training Center',Trainingcenter,'#0F2C67',Trainingcenter],
     			['Hatchery',Hatchery,'#0F2C67',Hatchery]
         			
			]);

			var barchart_options = {
				title :   province,
				legend :  { position: "none" },
				/*  hAxis: {title: 'Region 1', titleTextStyle: {color: 'black'}},
			      colors: ['red','green','black'], */
				is3D : true,
		        //tooltip :  {showColorCode: true},
		            'width' : 900,
		            'height' : 500
			};
			/* $(".region").change(function(){
		        alert("Awd");
		        console.log("rows: "+data.getNumberOfRows() + "column: " + data.getColumnRange(0));
		        region_trigger = true;
		        for(i=0;i>=data.getNumberOfRows(); i++){
		        	data.removeRow(i);
		        }
		        barchart.draw(data, barchart_options); // redraw the chart
		    });
			if(!region_trigger){
				console.log(region_trigger); */
			barchart.draw(data, barchart_options);
			

		}
		
		function drawChartAllResources() {
				/* ...data.map(d =>[d.resources,d.total,d.color,d.total])
				console.log(data); */
				
	 		var data = google.visualization.arrayToDataTable([
	     		['Resources', 'Total', { role: 'style'}, { role: 'annotation' }],
	     			 /*  <c:forEach items="${chart_by_region}" var="entry">
	         			[ '${entry.resources}', ${entry.total},'${entry.color}' , ${entry.total}],
	     			</c:forEach> */ 
	     			['Fish Sanctuary',FS,'#FFD124',FS],
	     			['Fish Processing',FP,'#EAEA7F',FP],
	     			['Fish Landing',FL,'#5EE6EB',FL],
	     			['Fish Port',FPT,'#E2D784',FPT],
	     			['Fish Pen',FPEN,'#4D96FF',FPEN],
	     			['Fish Cage',FCage,'#FF1700',FCage],
	     			['Cold Storage',IPCS,'#97DBAE',IPCS],
	     			['Market',Market,'#D49B54',Market],
	     			['School of Fisheries',School,'#C74B50',School],
	     			['Seagrass',Seagrass,'#6BCB77',Seagrass],
	     			['Seaweeds',Seaweeds,'#019267',Seaweeds],
	     			['Mangrove',Mangrove,'#05595B',Mangrove],
	     			['Fish Corral',FishCorral,'#000957',FishCorral],
	     			['Mariculture Zone',Mariculture,'#F55353',Mariculture],
	     			['PFO',LGU,'#FF6464',LGU],
	     			['Training Center',Trainingcenter,'#0F2C67',Trainingcenter],
	     			['Hatchery',Hatchery,'#0F2C67',Hatchery]
				]);
	     		 
				var barchart_options = {
					title :   region,
					legend :  { position: "none" },
					/*  hAxis: {title: 'Region 1', titleTextStyle: {color: 'black'}},
				      colors: ['red','green','black'], */
					is3D : true,
			        //tooltip :  {showColorCode: true},
			            'width' : 900,
			            'height' : 500
				};

				var barchart = new google.visualization.ColumnChart(document.getElementById('all_resources_div'));
				barchart.draw(data, barchart_options);
				
			
			}
		
		function drawChartAllRegionResources() {


	 		var data = google.visualization.arrayToDataTable([
	     		['Resources', 'Total', { role: 'style'}, { role: 'annotation' }],
	     			<c:forEach items="${chart_all_region}" var="entry">
	         			[ '${entry.resources}', ${entry.total},'${entry.color}' , ${entry.total}],
	     			</c:forEach>
				]);

				var barchart_options = {
					title :   "${all_region}",
					legend :  { position: "none" },
					is3D : true,
			            'width' : 900,
			            'height' : 500
				};
				//console.log("data.cache.length: " + data.cache.length);
				var barchart = new google.visualization.ColumnChart(document.getElementById('all_region_div'));
				barchart.draw(data, barchart_options);
				

			}

	});
</script>

</head>
<body>

 <jsp:include page="../home/header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">
  <!-- Sidebar Holder -->
<nav id="sidebar">
<!-- <div class="custom-menu">
<button type="button" id="sidebarCollapse" class="btn btn-primary">
<i class="fa fa-bars"></i>
<span class="sr-only">Toggle Menu</span>
</button>
</div> -->
<!-- <div class="p-4"> -->
<div class="sidebar-header">
      <img class=" img img-fluid" alt="" src="/static/images/FRGIS-LOGO.png" style="height: 100px; width: 100px">
    </div>
    <hr>
<ul class="list-unstyled components mb-5">
	<!-- <li>      
      <a href="#provinceList" data-toggle="collapse" aria-expanded="false">
      <span class="text">Filter by Province<i class="fas fa-list"></i></span></a>
        <ul class="collapse list-unstyled" id="provinceList">  -->    
               	 <li>      
      <a href="#regionList" data-toggle="collapse" aria-expanded="false">
      <span class="text">Filter by Region<i class="fas fa-list"></i></span></a>       
			<select name="region"  id="region" class="form-control region"></select>															
      </li>
       <li>      
      <a href="#provinceList" data-toggle="collapse" aria-expanded="false">
      <span class="text">Filter by Province<i class="fas fa-list"></i></span></a>       
			<select name="provincemap"  id="provincemap" class="form-control provincemap"></select>															
      </li>
      <li>
      		<!-- <ul class="provinceCheck"></ul> -->
      </li>
      	
      <sec:authorize access="isAuthenticated()">
       <li><a href="/logout"><i class="fas fa-sign-out-alt"></i>LOGOUT</a></li>
      </sec:authorize>

</ul>
</nav>

  <!-- Page Content Holder -->
    <div id="content" class="p-4 p-md-5 pt-5 section-to-print">	   

	<!-- <table class="columns">
		<tr>
			<td><div id="region_div" style="border: 1px solid #ccc"></div></td>
			<td><div id="chart_div" style="border: 1px solid #ccc"></div></td>
		</tr>
	</table> -->
	<div class="container">
	<!-- <div class="row" >
		<div id="region_div" style="border: 1px solid #ccc"></div>
	</div> -->
	<div class="row section-to-print">
	<div id="all_region_div" style="border: 1px solid #ccc"></div>
	</div>
	<hr>
	<div class="region_chart row section-to-print" style="display: none">
	<div id="all_resources_div"  style="border: 1px solid #ccc"></div>
	</div>
	<hr>
	<div class="province_chart row section-to-print" style="display: none">
	<div id="chart_div" style="border: 1px solid #ccc"></div>
	</div>
	</div>
			</div>



</div>

<script src="/static/js/form/provinceMunBarangay.js"></script>	
</body>
</html>