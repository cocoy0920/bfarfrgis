<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<title>BFAR-FRGIS</title>
<meta charset="utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png" href="/static/images/favicon.png" />

<jsp:include page="../menu/css.jsp"></jsp:include>

</head>
<body>
 <jsp:include page="../home/header_users.jsp"></jsp:include>  
<div class="wrapper d-flex align-items-stretch text-dark">
  <jsp:include page="../report/sidenavbar.jsp"></jsp:include>
</div>
</body>
</html>