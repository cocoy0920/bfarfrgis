<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
<head>
<title>BFAR-FRGIS</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />

<jsp:include page="../menu/css.jsp"></jsp:include>
	<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
	<script src="/static/leaflet/spin.min.js"></script>
	<script src="/static/leaflet/leaflet.spin.min.js"></script>
	<script src="//d3js.org/d3.v3.min.js"></script>
	<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
  <script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
<link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
   <script src="/static/leaflet/L.Control.SlideMenu.js"></script>
 <script  src="/static/leaflet/leaflet-bootstrapmodal.js" type=" text/javascript"></script>
  	
 <style>
 	.hiddenRow {
    padding: 0 !important;
}
.collapsible {
  background-color: #777;
  color: white;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
}

.active, .collapsible:hover {
  background-color: #555;
}

.content_collapse {
  padding: 0 18px;
  display: none;
  overflow: hidden;
  background-color: #f1f1f1;	
	
}
 	.my-div-icon{
	background: white url(../images/calendar_2.png) right no-repeat;
	}
	.countryLabel{
  background: rgba(255, 255, 255, 0);
  border:0;
  border-radius:0px;
  box-shadow: 0 0px 0px;
  
}
.leaflet-div-icon {
   border: 1px solid #000;
  border-radius: 0px; 
  padding-bottom: 25px;
  text-align: center;
    background: transparent;
  border: 0px transparent; 
  display:inline-block;
  color: #000;
white-space: nowrap;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
pointer-events: none;
}
.leaflet-tooltip-own {
position: absolute;
background-color: rgba(0, 0, 0, 0.5);
border: 0px solid #000;
border-radius: 0px;
color: #000;
white-space: nowrap;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
pointer-events: none;
/* box-shadow: 0 1px 3px rgba(0,0,0,0.4); */
}
/* .leaflet-tooltip {
    position: absolute;
    padding: 6px;
    background-color: #000;
    border: 1px solid #fff;
    border-radius: 3px;
    color: #222;
    white-space: nowrap;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    pointer-events: none;
    box-shadow: 0 1px 3px rgba(0,0,0,0.4);
} */

/*       .content {
        margin: 0.25rem;
        border-top: 1px solid #000;
        padding-top: 0.5rem;
        background: #002e7c; 
      }
 */
      .header {
        font-size: 1.8rem;
        color: #7f7f7f;
      }

      .title {
        font-size: 1.1rem;
        color: #7f7f7f;
        font-weight: bold;
      }

      .bottom {
        margin-top: 64px;
        font-size: 0.8rem;
        color: #7f7f7f;
      }

 table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  text-align: center;
  background: white;
  font-size: 10px;
}

.info {
padding: 6px 8px;
font: 14px/16px Arial, Helvetica, sans-serif;
background: white;
background: rgba(255,255,255,0.8);
box-shadow: 0 0 15px rgba(0,0,0,0.2);
border-radius: 5px;
}
.legend {
background-color: “black”;
line-height: 25px;
color: #555;
width: auto;
}
.legend i {
width: 18px;
height: 18px;
float: left;
margin-right: 8px;
opacity: 0.7;
}
 </style>
</head>
<body>
<jsp:include page="../home/header_users.jsp"></jsp:include>
 <div class="wrapper d-flex align-items-stretch">

  <!-- Page Content Holder -->
<div id="content" class="section-to-print">
    <div id="map">
    	<!-- <div class="leaflet-control leaflet-top leaflet-center col-sm-2 col-sm-offset-1 custom-popup">
              			<img alt="" src="/static/images/FRGIS-LOGO.png" style="height: 50px; width: 50px">
         -->	<!-- <div class="card-body">
        	 <span class="card-title">PAYAO REGION 4A</span><br/>
  								<p id="fs" class="card-text text-center btn btn-primary">100</p><br/>
    			</div> -->		
        </div>
    </div>

</div> 

<jsp:include page="../menu/fma_details.jsp"></jsp:include>
	<div id="footer">
		<jsp:include page="../users/footer.jsp"></jsp:include>
	</div>


<script src="/static/leaflet/bundle.js"></script>
<script  src="/static/js/api/regionMap.js" ></script>
<script  src="/static/js/api/FMA.js" ></script>		
<script  src="/static/js/resource.js" ></script>
<script src="/static/js/modal/sadmin_menu.js"></script>
<script src="/static/js/map/checkResourcesByAdmin.js"></script>
<script src="/static/js/map/philippineMap.js"></script>
<script  src="/static/js/resourcesOnMapSuperAdmin.js" ></script>



<script type="text/javascript">

jQuery(document).ready(function($) {
    $('#sidebarCollapse').on('click', function () {
      $('#sidebar').toggleClass('active');
  });

}); 	    
        </script>
    </body>
</html>