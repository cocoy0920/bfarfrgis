<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
<head>
<title>BFAR-FRGIS</title>
<meta charset="utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />

<jsp:include page="../menu/css.jsp"></jsp:include>
	<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
	    <script src="//d3js.org/d3.v3.min.js"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
	<script src="/static/leaflet/bundle.js"></script>
</head>
<body>
 <jsp:include page="../home/header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">


 	<div id="container" class="p-4 p-md-5 pt-5 section-to-print"> 
 		<canvas id="bar-chart" width="800" height="450"></canvas>
 	</div>

	</div> 
    	
 	 	<div id="footer">
		<jsp:include page="../users/footer.jsp"></jsp:include>
    </div>

	<script  src="/static/js/resourcesOnMapByRegion.js" ></script>
	<script src="/static/js/validationForm/headerFilter.js" type=" text/javascript"></script>
	
<script type="text/javascript">
jQuery(document).ready(function($) {
        $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
        
        $("#pageHatchery").click(function(){
        	 $('#sidebar').toggleClass('active');
        });
        
        new Chart(document.getElementById("bar-chart"), {
            type: 'bar',
            data: {
              labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
              datasets: [
                {
                  label: "Population (millions)",
                  backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                  data: [2478,5267,734,784,433]
                }
              ]
            },
            options: {
              legend: { display: false },
              title: {
                display: true,
                text: 'Predicted world population (millions) in 2050'
              }
            }
        });
        
	 
/*    		 var resources = [];
   		  var pages="";
   		
        $(".btn").click(function(event) {
        	 document.getElementById("btn").disabled = true;
            event.preventDefault();
            event.stopPropagation();

      	  var markedCheckbox = document.getElementsByName('page');  
            for (var checkbox of markedCheckbox) {  
              if (checkbox.checked)  

          	   resources.push(checkbox.value);
              
            } 

           $.ajax({
                    type : "GET",
                   url : '/home/resources-get-by-pages-home',
                    data : {resources: resources},
                   success : function(data) {
                  	 		console.log("success --> home ");
                        if(data){
                            }else{
                        alert("failed to upload");

                       }
                    }
           });

   	  });
        
 */

}); 	    
        </script>
    </body>
</html>