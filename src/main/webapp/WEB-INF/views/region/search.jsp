<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!doctype html>
<html lang="en">
<head>
<title>User Registration</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>

 <style>
      .error {
         color: #ff0000;
      }

      .errorblock {
         color: #000;
         background-color: #ffEEEE;
         border: 3px solid #ff0000;
         padding: 8px;
         margin: 16px;
      }
   </style>
</head>
<body>

<jsp:include page="../users/header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">
<div id="content" class="p-4 p-md-5 pt-5">		
<%-- <form:form method="POST" action="register" modelAttribute="userForm"> --%>
<form class="needs-validation" id="myform" novalidate>
	<fieldset class="form-group border border-dark rounded p-4">
	<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">Felter</legend>
	<%--  <form:errors path = "*" cssClass = "errorblock" element = "div" /> --%>

		<div class="form-row">
    			<div class="col-md-4 mb-3">
					<label class="col-form-label-lg" for="userProfiles">User Role:</label>
					<select name="userProfiles"  id="userProfiles" class="form-control userProfiles" required>							
    							<option label = "${role.type}" value="${role.id},${role.type}"/>
    							
					</select>															
							<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        						Required
      						</div> 			
    			</div>
    			<%-- 
    			<div class="col-md-4 mb-3 regionList" style="display: none;">
					<label class="col-form-label-lg" for="region">Region</label>
					<select name="region"  id="region" class="form-control region" required>															
					<option label = "--- PLS SELECT ---" value=""/>
					<c:forEach items="${regions}" var="region">
    					<option label = "${region.region}" value="${region.region_id},${region.region}"/>
    				</c:forEach>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid province.
      				</div>			
    			</div> --%>
    			
    			<div class="col-md-4 mb-3">
					<label class="col-form-label-lg" for="province">Province</label>
					<select name="province"  id="province" class="form-control province" required>
					<option label = "--- PLS SELECT ---" value=""/>
					<c:forEach items="${provinces}" var="province">
    					<option label = "${province.province}" value="${province.province_id},${province.province}"/>
    				</c:forEach>
					</select>															
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid province.
      				</div>			
    			</div>
		</div>
		<%-- <div class="form-group">
			<div class="">
			 <form:errors path="userProfiles" cssClass = "error"  /><br>
    			Role: <form:select path="userProfiles">
    						<c:forEach items="${roles}" var="role">
    					<form:option label = "${role.type}" value="${role.id},${role.type}"/>
    				</c:forEach>
    			</form:select>
    		</div>
		</div> --%>

	<%-- 	<div class="form-group">
			<div class="">
			 <form:errors path="region" cssClass = "error"  /><br>
    			Region: <form:select path="region">
    						<c:forEach items="${regions}" var="region">
    					<form:option label = "${region.region}" value="${region.region_id},${region.region}"/>
    				</c:forEach>
    			</form:select>
    		</div>
		</div> --%>
		</fieldset>
   		<div class="form-group">
			<div class="">
				<!-- <input type="submit" class="btn btn-primary" value="SUBMIT"/> -->
				 <button type="button" id="submitUseInfo" class="submitUseInfo btn btn-primary">SUBMIT</button>
			</div>
		</div>
		
		</form>
		<%-- </form:form> --%>
<%--    <span>
    <c:out value="${successMessage}"></c:out></span> --%>
    <%-- <c:if test="${bindingResult != null && bindingResult.getAllErrors() != null}">
    <div class="alert alert-success alert-dismissable col-sm-12">
					
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					
					<ul>
					<c:forEach var="data" items="${bindingResult.getAllErrors()}">
						<li>
						<c:out value="${data.getObjectName() + '::' + data.getDefaultMessage()}"></c:out> 
						</li>
					</c:forEach>
					</ul>
				</div>
		</c:if>		 --%> 
</div>
</div>


<script src="/static/js/validationForm/provinceRegistration.js" type=" text/javascript"></script>

</body>
</html>