  
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!doctype html>
<html lang="en">
<head>
<title>FRGIS - CHART</title>
<meta charset="utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />
  <jsp:include page="../menu/css.jsp"></jsp:include>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <style>
    .chart {
    display: block;
	margin: 0 auto;
    }
    </style>


</head>
<body>

 <jsp:include page="../home/header_bar.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">

    <div id="content" class="p-4 p-md-5 pt-5 section-to-print">	   

	<table class="columns">
      <tr>
        <td>
        	<div id='png'></div>
			<div id="chart_div" style="border: 1px solid #ccc"></div>
        </td>
        <td>
        	<div id='pngForSeaWeeds'></div>
			<div id="chartSeaWeeds_div" style="border: 1px solid #ccc"></div>
        </td>
      </tr>
    </table>
    <table class="columns">
      <tr>
        <td>
        	<div id='pngForMariculture'></div>
			<div id="chartMariculture_div" style="border: 1px solid #ccc"></div>
        </td>
        <td>
        	<div id='pngForHatchery'></div>
			<div id="chartHatchery_div" style="border: 1px solid #ccc"></div>
        </td>
      </tr>
<!--       <tr>
      <td>
      <input class="btn" type="button" value="Draw Chart" />
      <input type="checkbox" class="checkbox"  id="kolom1"> one<br>
		<input type="checkbox" class="checkbox"  id="kolom2"> two<br>
		<input type="checkbox" class="checkbox"  id="kolom3"> three<br>
       <div id="table_div"></div>
      </td>
      </tr> -->
    </table>

			</div>



</div>

    <script type="text/javascript">

		google.charts.load("current", {packages:['corechart']});
	
  	    var provinceSize = '${fn:length(provinces)}';
  	    
 	   google.charts.setOnLoadCallback(draw);

 	      function draw(){
 	    	 var data = google.visualization.arrayToDataTable([
 	            ['Resources',
 	            	<c:forEach items="${provinces}" var="province">	
 	            	'${province.province}',
 	            </c:forEach>
 	            	{ role: 'annotation' } ],
 	            	
 	            ['Fish Pen',
 	            	<c:forEach items="${provinceChart}" var="entry">
 	            	<c:if test = "${entry.resources == 'FPen'}">	            		
 	            			${entry.total},
 	            	</c:if>
 	            	
 			</c:forEach>
 					''
 	            	],
 	            ['Fish Cage',
 	            	
 	            	<c:forEach items="${provinceChart}" var="entry">
 	            	<c:if test = "${entry.resources == 'FCoral'}">
         			${entry.total},
         	</c:if>
					</c:forEach>
 	            	
 	            	'']
 	          
 	          ]);
 	    	 
 	    	var view = new google.visualization.DataView(data);

	    	var columns = [];
	    	for (var i = 0; i <= provinceSize ; i++) {
	    	    if (i > 0) {
	    	        columns.push(i);
	    	        columns.push({
	    	        	calc: function (dt, row) {
	 			 	    	  if(dt.getValue(row, i) == 0){
	 			 	    		  return ""; 
	 			 	    	  }else{ 
	 			 	        	return dt.getValue(row, i).toString();
	 			 	    	  }	
	 			 	      },
	 			 	      type: "string",
	 			 	      role: "annotation"
	    	        });

	    	    } else {
	    	        columns.push(i);
	    	    }
	    	}
	    	
	    	columns.push(
	    		{
		 	      calc: function (dt, row) {
		 	        return 0;
		 	      },
		 	      label: "Total",
		 	      type: "number"		  		 	   	
	        	},
	        );
	    	columns.push(
	    		{	  	    		
	    		 calc: function (dt, row) {
	    			if(provinceSize == 6){
	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5) + dt.getValue(row, 6);
	    			}
	    			if(provinceSize == 5){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5);
	  	    		}
	    			if(provinceSize == 4){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4);
	  	    		}
	    			},
		 	      type: "number",
		 	      role: "annotation"
	    	
	    	}
	    		);
	    	
	    	view.setColumns(columns);


 	          var options = {
 	            width: 500,
 	            height: 400,
 	           	title: 'AQUACULTURE(AQUAFARM) - ${region_title}' ,
 	            legend: { position: 'top', maxLines: 5 },
 	            bar: { groupWidth: '75%' },
 	            isStacked: true,
 	           vAxis: {
	  	            gridlines: {color: '#007bff;'},
	  	            minValue: 0
	  	          }
 	            
 	          };

 	         var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
 	      
 	         google.visualization.events.addListener(chart, 'ready', function () {
 	          // chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';

 	          document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '" target="_blank">PRINT AQUACULTURE CHARTS</a>';
 	         });
 	       
  	        chart.draw(view, options);
 	      }

 	      
 	 	   google.charts.setOnLoadCallback(drawSeaWeeds);

  	      function drawSeaWeeds(){
  	    	 var data = google.visualization.arrayToDataTable([
  	            ['Resources',
  	            	<c:forEach items="${provinces}" var="province">	
  	            	'${province.province}',
  	            </c:forEach>
  	            	{ role: 'annotation' } ],
  	            	
  	            ['NURSERY',
  	            	<c:forEach items="${provinceChart}" var="entry">
  	            	<c:if test = "${entry.resources == 'SNURSERY'}">	            		
  	            			${entry.total},
  	            	</c:if>
  	            	
  			</c:forEach>
  					''
  	            	],
  	            ['LABORATORY',
  	            	
  	            	<c:forEach items="${provinceChart}" var="entry">
  	            	<c:if test = "${entry.resources == 'SLABORATORY'}">
          			${entry.total},
          	</c:if>
 					</c:forEach>
  	            	
  	            	'']
  	          
  	          ]);
  	    	 
  	    	var view = new google.visualization.DataView(data);

	    	var columns = [];
	    	for (var i = 0; i <= provinceSize ; i++) {
	    	    if (i > 0) {
	    	        columns.push(i);
	    	        columns.push({
	    	        	calc: function (dt, row) {
	 			 	    	  if(dt.getValue(row, i) == 0){
	 			 	    		  return ""; 
	 			 	    	  }else{ 
	 			 	        	return dt.getValue(row, i).toString();
	 			 	    	  }	
	 			 	      },
	 			 	      type: "string",
	 			 	      role: "annotation"
	    	        });

	    	    } else {
	    	        columns.push(i);
	    	    }
	    	}
	    	
	    	columns.push(
	    		{
		 	      calc: function (dt, row) {
		 	        return 0;
		 	      },
		 	      label: "Total",
		 	      type: "number"		  		 	   	
	        	},
	        );
	    	columns.push(
	    		{	  	    		
	    		 calc: function (dt, row) {
	    			if(provinceSize == 6){
	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5) + dt.getValue(row, 6);
	    			}
	    			if(provinceSize == 5){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5);
	  	    		}
	    			if(provinceSize == 4){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4);
	  	    		}
	    			},
		 	      type: "number",
		 	      role: "annotation"
	    	
	    	}
	    		);
	    	
	    	view.setColumns(columns);


  	          var options = {
  	            width: 500,
  	            height: 400,
  	           	title: 'AQUACULTURE(SEAWEEDS) - ${region_title}' ,
  	            legend: { position: 'top', maxLines: 5 },
  	            bar: { groupWidth: '75%' },
  	            isStacked: true,
  	          vAxis: {
	  	            gridlines: {color: '#007bff;'},
	  	            minValue: 0
	  	          }
  	          };

  	         var chart = new google.visualization.ColumnChart(document.getElementById('chartSeaWeeds_div'));
  	      
  	         google.visualization.events.addListener(chart, 'ready', function () {
  	          // chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';

  	          document.getElementById('pngForSeaWeeds').outerHTML = '<a href="' + chart.getImageURI() + '" target="_blank">PRINT AQUACULTURE(SEAWEEDS) CHARTS</a>';
  	         });
  	       
   	        chart.draw(view, options);
  	      }

	 	   google.charts.setOnLoadCallback(drawMariculturePark);

	  	      function drawMariculturePark(){
	  	    	 var data = google.visualization.arrayToDataTable([
	  	            ['Resources',
	  	            	<c:forEach items="${provinces}" var="province">	
	  	            	'${province.province}',
	  	            </c:forEach>
	  	            	{ role: 'annotation' } ],
	  	            	
	  	            ['BFAR MANAGED',
	  	            	<c:forEach items="${provinceChart}" var="entry">
	  	            	<c:if test = "${entry.resources == 'BFAR_Mariculture'}">	            		
	  	            			${entry.total},
	  	            	</c:if>
	  	            	
	  			</c:forEach>
	  					''
	  	            	],
	  	            ['LGU MANAGED',
	  	            	
	  	            	<c:forEach items="${provinceChart}" var="entry">
	  	            		<c:if test = "${entry.resources == 'LGU_Mariculture'}">
	          					${entry.total},
	          				</c:if>
	 					</c:forEach>
	  	            	
	  	            	''],
	  	            ['PRIVATE SECTOR',
	  	            	
	  	            	<c:forEach items="${provinceChart}" var="entry">
	  	            		<c:if test = "${entry.resources == 'PRIVATE_Mariculture'}">
	          					${entry.total},
	          				</c:if>
	 					</c:forEach>
	  	            	
	  	            	'']
	  	          ]);
	  	    	 
	  	    	var view = new google.visualization.DataView(data);

	    	var columns = [];
	    	for (var i = 0; i <= provinceSize ; i++) {
	    	    if (i > 0) {
	    	        columns.push(i);
	    	        columns.push({
	    	        	calc: function (dt, row) {
	 			 	    	  if(dt.getValue(row, i) == 0){
	 			 	    		  return ""; 
	 			 	    	  }else{ 
	 			 	        	return dt.getValue(row, i).toString();
	 			 	    	  }	
	 			 	      },
	 			 	      type: "string",
	 			 	      role: "annotation"
	    	        });

	    	    } else {
	    	        columns.push(i);

	    	    }
	    	}

	    	columns.push(
	    		{
		 	      calc: function (dt, row) {
		 	        return 0;
		 	      },
		 	      label: "Total",
		 	      type: "number"		  		 	   	
	        	},
	        );
	    	columns.push(
	    		{	  	    		
	    		 calc: function (dt, row) {
	    			if(provinceSize == 6){
	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5) + dt.getValue(row, 6);
	    			}
	    			if(provinceSize == 5){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5);
	  	    		}
	    			if(provinceSize == 4){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4);
	  	    		}
	    			},
		 	      type: "number",
		 	      role: "annotation"
	    	
	    	}
	    		);
	    	$.each(columns , function(index, val) { 
	    		  console.log(val)
	    		});
	    	view.setColumns(columns);


	  	          var options = {
	  	            width: 500,
	  	            height: 400,
	  	           	title: 'AQUACULTURE(MARICULTURE) - ${region_title}' ,
	  	            legend: { position: 'top', maxLines: 5 },
	  	            bar: { groupWidth: '75%' },
	  	            isStacked: true,
	  	          vAxis: {
	  	            gridlines: {color: '#007bff;'},
	  	            minValue: 0
	  	          }
	  	            
	  	          };

	  	         var chart = new google.visualization.ColumnChart(document.getElementById('chartMariculture_div'));
	  	      
	  	         google.visualization.events.addListener(chart, 'ready', function () {
	  	          // chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';

	  	          document.getElementById('pngForMariculture').outerHTML = '<a href="' + chart.getImageURI() + '" target="_blank">PRINT AQUACULTURE(MARICULTURE) CHARTS</a>';
	  	         });
	  	       
	   	        chart.draw(view, options);
	  	      }
	  	   

		 	   google.charts.setOnLoadCallback(drawHatchery);

		  	      function drawHatchery(){
		  	    	
		  	    	 var data = google.visualization.arrayToDataTable([
		  	            ['Resources',
		  	            	<c:forEach items="${provinces}" var="province">	
		  	            	'${province.province}',
		  	            </c:forEach>
		  	            	{ role: 'annotation' } ],
		  	            	
		  	            ['BFAR - LEGISLATED',
		  	            	<c:forEach items="${provinceChart}" var="entry">
		  	            		<c:if test = "${entry.resources == 'BFAR_HATCHERY'}">	            		
		  	            			${entry.total},
		  	            		</c:if>		  	            	
		  					</c:forEach>
		  					''
		  	            	],
		  	            ['LGU',
		  	            	
		  	            	<c:forEach items="${provinceChart}" var="entry">
		  	            		<c:if test = "${entry.resources == 'LGU_HATCHERY'}">
		          					${entry.total},
		          				</c:if>
		 					</c:forEach>
		  	            	
		  	            	''],
		  	            ['PRIVATE',
		  	            	
		  	            	<c:forEach items="${provinceChart}" var="entry">
		  	            		<c:if test = "${entry.resources == 'PRIVATE_HATCHERY'}">
		          					${entry.total},
		          				</c:if>
		 					</c:forEach>
		  	            	
		  	            	'']
		  	          ]);
		  	    	 
		  	    	var view = new google.visualization.DataView(data);
		  	    	var columns = [];
		  	    	for (var i = 0; i <= provinceSize ; i++) {
		  	    	    if (i > 0) {
		  	    	        columns.push(i);
		  	    	        columns.push({
		  	    	        	calc: function (dt, row) {
			 			 	    	  if(dt.getValue(row, i) == 0){
			 			 	    		  return ""; 
			 			 	    	  }else{ 
			 			 	        	return dt.getValue(row, i).toString();
			 			 	    	  }	
			 			 	      },
			 			 	      type: "string",
			 			 	      role: "annotation"
		  	    	        });

		  	    	    } else {
		  	    	        columns.push(i);
		  	    	    }
		  	    	}
		  	    	
		  	    	columns.push(
		  	    		{
		  		 	      calc: function (dt, row) {
		  		 	        return 0;
		  		 	      },
		  		 	      label: "Total",
		  		 	      type: "number"		  		 	   	
  	    	        	},
  	    	        );
		  	    	columns.push(
		  	    		{	  	    		
		  	    		 calc: function (dt, row) {
		
		  	    			if(provinceSize == 6){
		  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5) + dt.getValue(row, 6);
		  	    			}
		  	    			if(provinceSize == 5){
			  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5);
			  	    		}
		  	    			if(provinceSize == 4){
			  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4);
			  	    		}
		  	    			},
		 		 	      type: "number",
		 		 	      role: "annotation"
		  	    	
		  	    	}
		  	    		);
		  	    	
		  	    	view.setColumns(columns);

		  	          var options = {
		  	            width: 500,
		  	            height: 400,
		  	           	title: 'AQUACULTURE(HATCHERY) - ${region_title}' ,
		  	            legend: { position: 'top', maxLines: 5 },
		  	            bar: { groupWidth: '75%' },
		  	            isStacked: true,
		  	          	vAxis: {
			  	            gridlines: {color: '#007bff;'},
			  	            minValue: 0,
			  	          	title: 'Number of Resources'
			  	          }
		  	            
		  	          };

		  	         var chart = new google.visualization.ColumnChart(document.getElementById('chartHatchery_div'));
		  	      
		  	         google.visualization.events.addListener(chart, 'ready', function () {
		  	         
		  	        	 document.getElementById('pngForHatchery').outerHTML = '<a href="' + chart.getImageURI() + '" target="_blank">PRINT AQUACULTURE(Hatchery) CHARTS</a>';
		  	         });
		  	       
		   	        chart.draw(view, options);
		  	      }

		  	 /*    google.charts.load('current', {'packages':['table']});
		        google.charts.setOnLoadCallback(drawTable);

		        function drawTable() {
		        	  var data = new google.visualization.DataTable();
		        	  data.addColumn('string', 'Name');
		        	  data.addColumn('number', 'Salary');
		        	  data.addColumn('boolean', 'Full Time');
		        	  data.addRows(5);
		        	  data.setCell(0, 0, 'John');
		        	  data.setCell(0, 1, 10000, '$10,000');
		        	  data.setCell(0, 2, true);
		        	  data.setCell(1, 0, 'Mary');
		        	  data.setCell(1, 1, 25000, '$25,000');
		        	  data.setCell(1, 2, true);
		        	  data.setCell(2, 0, 'Steve');
		        	  data.setCell(2, 1, 8000, '$8,000');
		        	  data.setCell(2, 2, false);
		        	  data.setCell(3, 0, 'Ellen');
		        	  data.setCell(3, 1, 20000, '$20,000');
		        	  data.setCell(3, 2, true);
		        	  data.setCell(4, 0, 'Mike');
		        	  data.setCell(4, 1, 12000, '$12,000');
		        	  data.setCell(4, 2, false);

		        	  var table = new google.visualization.Table(document.getElementById('table_div'));
		        	  table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});

		        	  google.visualization.events.addListener(table, 'select', function() {
		        	    var row = table.getSelection()[0].row;
		        	    alert('You selected ' + data.getValue(row, 0));
		        	  });
		        	}
		         */
		        google.charts.load('current', {
		        	  packages: ['table']
		        	}).then(function () {
		        	  $(".btn").click(function() {
		        		  var data = new google.visualization.DataTable();
			        	  data.addColumn('string', 'Name');
			        	  data.addColumn('number', 'Salary');
			        	  data.addColumn('boolean', 'Full Time');
			        	  data.addRows(5);
			        	  data.setCell(0, 0, 'John');
			        	  data.setCell(0, 1, 10000, '$10,000');
			        	  data.setCell(0, 2, true);
			        	  data.setCell(1, 0, 'Mary');
			        	  data.setCell(1, 1, 25000, '$25,000');
			        	  data.setCell(1, 2, true);
			        	  data.setCell(2, 0, 'Steve');
			        	  data.setCell(2, 1, 8000, '$8,000');
			        	  data.setCell(2, 2, false);
			        	  data.setCell(3, 0, 'Ellen');
			        	  data.setCell(3, 1, 20000, '$20,000');
			        	  data.setCell(3, 2, true);
			        	  data.setCell(4, 0, 'Mike');
			        	  data.setCell(4, 1, 12000, '$12,000');
			        	  data.setCell(4, 2, false);

			        	  var table = new google.visualization.Table(document.getElementById('table_div'));
			        	  table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});

			        	  google.visualization.events.addListener(table, 'select', function() {
			        	    var row = table.getSelection()[0].row;
			        	    alert('You selected ' + data.getValue(row, 0));
			        	  });
		        	  });
		        	});
			        google.charts.load('current', {
			        	  packages: ['table']
			        	}).then(function () {
			        		 $(".checkbox").change(function() {
			        			 if($("#kolom1").is(':checked')) {
			        		  var data = new google.visualization.DataTable();
				        	  data.addColumn('string', 'Name');
				        	  data.addColumn('number', 'Salary');
				        	  data.addColumn('boolean', 'Full Time');
				        	  data.addRows(5);
				        	  data.setCell(0, 0, 'John');
				        	  data.setCell(0, 1, 10000, '$10,000');
				        	  data.setCell(0, 2, true);
				        	  data.setCell(1, 0, 'Mary');
				        	  data.setCell(1, 1, 25000, '$25,000');
				        	  data.setCell(1, 2, true);
				        	  data.setCell(2, 0, 'Steve');
				        	  data.setCell(2, 1, 8000, '$8,000');
				        	  data.setCell(2, 2, false);
				        	  data.setCell(3, 0, 'Ellen');
				        	  data.setCell(3, 1, 20000, '$20,000');
				        	  data.setCell(3, 2, true);
				        	  data.setCell(4, 0, 'Mike');
				        	  data.setCell(4, 1, 12000, '$12,000');
				        	  data.setCell(4, 2, false);

				        	  var table = new google.visualization.Table(document.getElementById('table_div'));
				        	  table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});

				        	  google.visualization.events.addListener(table, 'select', function() {
				        	    var row = table.getSelection()[0].row;
				        	    alert('You selected ' + data.getValue(row, 0));
				        	  });
			        			 }else{
			        				 
			        			 }
			        	  });
			        	});
</script>
</body>

</html>