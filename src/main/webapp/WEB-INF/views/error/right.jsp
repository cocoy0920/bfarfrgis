<div class="content"> 
       <ul class="list-unstyled components mb-5"> 
         <li> 
       		<a href="#aqualist" data-toggle="collapse"  aria-expanded="false"> 
	  			<span class="text-white"><i class="fas fa-list"></i>&nbsp;AQUACULTURE</span></a> 
       		<ul class="collapse list-unstyled" id="aqualist"> 

      			<li class="dropdown-subitem"> 
      				<a href="#farmlist" data-toggle="collapse"  aria-expanded="false"> 
     	 			<span class="text-white">&nbsp;I. Aquafarms</span></a> 
       				<ul class="collapse list-unstyled" id="farmlist"> 
        				<li><input id="aquafarm" type="checkbox"><span class="text-white">Select all</span></li>
      					<li class="dropdown-item"> 
      						<input type="checkbox" id="checked_pond" value="fishpond" name="fishpond" class="aquafarm">
       						<img src="/static/images/pin2022/FishPond.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">Fish Pond</span>
       					</li>
       					<li class="dropdown-item">
      						<input type="checkbox" id="checked_fishpen" value="fishpen" class="aquafarm">
      						<img src="/static/images/pin2022/FishPen.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">Fish Pen</span>
      					</li>
      					<li class="dropdown-item">
       						<input type="checkbox" id="checked_fishcage" value="fishcage" class="aquafarm">
       						<img src="/static/images/pin2022/FishCage.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">Fish Cage</span>
      					</li>
      					<li class="dropdown-item">
      						<input type="checkbox" id="checked_fishcoral" value="fishcoral" class="aquafarm">
      						<img src="/static/images/pin2022/FishCorral.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">Fish Corral</span>
      					</li>
       				  </ul> 
         		  </li> 
         		<li class="dropdown-subitem"> 
      				<a href="#aquaseaweedlist" data-toggle="collapse"  aria-expanded="false"> 
     	 			<span class="text-white">&nbsp;II. Seaweed</span></a> 
       				<ul class="collapse list-unstyled" id="aquaseaweedlist"> 
        				<li><input id="aquaseaweed" type="checkbox"><span class="text-white">Select all</span></li>
      					<li class="dropdown-item"> 
      						<input type="checkbox" id="checked_seaweednursery" value="nursery" name="nursery" class="aquaseaweed">
       						<img src="/static/images/pin2022/Seaweeds_Nursery.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">Seaweed Nursery</span>
       					</li>
       					<li class="dropdown-item">
      						<input type="checkbox" id="checked_seaweedlaboratory" value="laboratory" name="laboratory" class="aquaseaweed">
      						<img src="/static/images/pin2022/Seaweeds_laboratory.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">Seaweed Laboratory</span>
      					</li>
      					
       				</ul> 
         		</li>
         		<li class="dropdown-subitem"> 
      				<a href="#mariculturelist" data-toggle="collapse"  aria-expanded="false"> 
     	 			<span class="text-white">&nbsp;III. Mariculture Park</span></a> 
       				<ul class="collapse list-unstyled" id="mariculturelist"> 
        				<li><input id="aquamariculture" type="checkbox"><span class="text-white">Select all</span></li>
      					<li class="dropdown-item"> 
      						<input type="checkbox" id="checked_mariculturebfar" value="bfar" name="bfar" class="aquamariculture">
       						<img src="/static/images/pin2022/Mariculture_BFAR.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">BFAR Managed</span>
       					</li>
       					<li class="dropdown-item">
      						<input type="checkbox" id="checked_mariculturelgu" value="lgu" name="lgu" class="aquamariculture">
      						<img src="/static/images/pin2022/Mariculture_LGU.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">LGU Managed</span>
      					</li>
      					<li class="dropdown-item">
      						<input type="checkbox" id="checked_maricultureprivate" value="private" name="private" class="aquamariculture">
      						<img src="/static/images/pin2022/Mariculture_PrivateSector.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">Private Sector</span>
      					</li>
      					
       				</ul> 
         		</li>
         		<li class="dropdown-subitem"> 
      				<a href="#hatcherylist" data-toggle="collapse"  aria-expanded="false"> 
     	 			<span class="text-white">&nbsp;IV. Hatchery</span></a> 
       				<ul class="collapse list-unstyled" id="hatcherylist"> 
        			<li><input id="aquahatchery" type="checkbox"><span class="text-white">Select all</span></li>
      					<li class="dropdown-subitem"> 
      						<a href="#bfarmanaged" data-toggle="collapse"  aria-expanded="false"> 
     	 						<span class="text-white">&nbsp;BFAR Managed</span></a> 
       							<ul class="collapse list-unstyled" id="bfarmanaged">  							
      								<li class="dropdown-item">
       									<input type="checkbox" id="checked_legislated" value="hatcheries" class="aquahatchery">
       									<img src="/static/images/pin2022/Hatchery.png" style="width: 20px; height: 25px;"/>
       									<span class="text-white">Legislated</span>
      								</li>
      								<li class="dropdown-item">
      									<input type="checkbox" id="checked_nonlegislated" value="hatcheries" class="aquahatchery">
       									<img src="/static/images/pin/20x34/hatcheries.png" style="width: 20px; height: 25px;"/>
       									<span class="text-white">Non-Legislated</span>
      								</li>
      					
       							</ul> 
         					</li>
       					<li class="dropdown-item">
      						<input type="checkbox" id="checked_hatcherylgu" value="lgu" name="lgu" class="aquahatchery">
      						<img src="/static/images/pin/new/Hatchery_LGU.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">LGU Managed</span>
      					</li>
      					<li class="dropdown-item">
      						<input type="checkbox" id="checked_hatcheryprivate" value="private" name="private" class="aquahatchery">
      						<img src="/static/images/pin2022/Hatchery_PrivateSector.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">Private Sector</span>
      					</li>
      					
       				</ul> 
         		</li>
       			</ul> 
       		</li>
       		<li> 
       			<a href="#capturedList" data-toggle="collapse"  aria-expanded="false"> 
     	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;CAPTURE FISHERIES</span></a> 
       				<ul class="collapse list-unstyled" id="capturedList"> 
        				<li><input id="capture" type="checkbox"><span class="text-white">Select all</span></li>
      					<li class="dropdown-item"> 
      						<input type="checkbox" id="checked_payao" value="payao" name="payao" class="capture">
       						<img src="/static/images/pin2022/Payao.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">Payao</span>
       					</li> 
       					<li class="dropdown-item">
      						<input type="checkbox" id="checked_lambaklad" value="lambaklad" name="lambaklad" class="capture">
       						<img src="/static/images/pin2022/Lambaklad.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">Lambaklad</span>
       					</li>

      					<li class="dropdown-item">
      						<input type="checkbox" id="checked_frpboats" value="frpboats" name="frpboats" class="capture">
       						<img src="/static/images/pin2022/FRP_Boats.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">FRP Boats</span>
       					</li>	
       				</ul>
       		</li>
       		<li> 
       			<a href="#postharvestList" data-toggle="collapse"  aria-expanded="false"> 
     	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;POST HARVEST</span></a> 
       				<ul class="collapse list-unstyled" id="postharvestList"> 
       					<li class="dropdown-item"> 
	  						<span class="text-white">I.</span>
      						<input type="checkbox" id="check_fishlanding" value="fishlanding">
       						<img src="/static/images/pin2022/FishLanding.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">Fish Landing</span>
       					</li>
       					<li class="dropdown-subitem">
       						<a href="#postharvestColdStorage" data-toggle="collapse"  aria-expanded="false">
     	 						<span class="text-white">&nbsp;II. Cold Storage/IPCS</span>
       						</a>
        					<ul class="collapse list-unstyled" id="postharvestColdStorage">
        						<li><input id="coldstorage" type="checkbox"><span class="text-white">Select all</span></li>
        						<li class="dropdown-item">
      									<input type="checkbox" id="check_coldpfda" value="pfda" name="pfda" class="coldstorage">
       									<img src="/static/images/pin2022/IcePlant_BFAR.png" style="width: 20px; height: 25px;"/>
       									<span class="text-white">BFAR</span>
       							</li>
       							<li class="dropdown-item">
      									<input type="checkbox" id="check_coldprivate" value="private" name="private" class="coldstorage">
       									<img src="/static/images/pin2022/IcePlant_PrivateSector.png" style="width: 20px; height: 25px;"/>
       									<span class="text-white">Private Sector</span>
       							</li>
       							<li class="dropdown-item">
      									<input type="checkbox" id="check_coldcooperative" value="cooperative" name="cooperative" class="coldstorage">
       									<img src="/static/images/pin2022/IcePlant_Cooperative.png" style="width: 20px; height: 25px;"/>
       									<span class="text-white">Cooperative</span>
       							</li>
        					</ul>
        				</li>
        				<li class="dropdown-subitem">
       						<a href="#postharvestprocessing" data-toggle="collapse"  aria-expanded="false">
     	 						<span class="text-white">&nbsp;III. Fish Processing Facility</span>
       						</a>
        					<ul class="collapse list-unstyled" id="postharvestprocessing">
        						<li><input id="processing" type="checkbox"><span class="text-white">Select all</span></li>
        						<li class="dropdown-item">
      									<input type="checkbox" id="check_processingbfar" value="bfar" name="bfar" class="processing">
       									<img src="/static/images/pin/new/Fish_Processing.png" style="width: 20px; height: 25px;"/>
       									<span class="text-white">BFAR</span>
       							</li>
       							<li class="dropdown-item">
      									<input type="checkbox" id="check_processinglgu" value="lgu" name="lgu" class="processing">
       									<img src="/static/images/pin2022/Fish_Processing_LGU.png" style="width: 20px; height: 25px;"/>
       									<span class="text-white">LGU</span>
       							</li>
       							<li class="dropdown-item">
      									<input type="checkbox" id="check_processingprivate" value="private" name="private" class="processing">
       									<img src="/static/images/pin2022/Fish_Processing_PrivateSector.png" style="width: 20px; height: 25px;"/>
       									<span class="text-white">Private Sector</span>
       							</li>
       							<li class="dropdown-item">
      									<input type="checkbox" id="check_processingcooperative" value="cooperative" name="cooperative" class="processing">
       									<img src="/static/images/pin2022/Fish_Processing_Cooperative.png" style="width: 20px; height: 25px;"/>
       									<span class="text-white">Cooperative</span>
       							</li>
        					</ul>
        				</li>
        				<li class="dropdown-subitem">
       						<a href="#postharvestseaweed" data-toggle="collapse"  aria-expanded="false">
     	 						<span class="text-white">&nbsp;IV. Seaweed</span>
       						</a>
        					<ul class="collapse list-unstyled" id="postharvestseaweed">
        						<li><input id="postseaweed" type="checkbox"><span class="text-white">Select all</span></li>
        						<li class="dropdown-item">
      									<input type="checkbox" id="check_warehouse" value="warehouse" name="warehouse" class="postseaweed">
       									<img src="/static/images/pin2022/Seaweeds(postharvest).png" style="width: 20px; height: 25px;"/>
       									<span class="text-white">Seaweed Warehouse</span>
       							</li>
       							<li class="dropdown-item">
      									<input type="checkbox" id="check_dryer" value="dryer" name="dryer" class="postseaweed">
       									<img src="/static/images/pin2022/seaweeds_dryer.png" style="width: 20px; height: 25px;"/>
       									<span class="text-white">Seaweed Dryer</span>
       							</li>
       							
        					</ul>
        				</li>
        				
        				<li class="dropdown-item">
        					<span class="text-white">V.</span>
      						<input type="checkbox" id="checked_market" value="market">
       						<img src="/static/images/pin2022/Market.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">Market</span>
       					</li>	
       					<li class="dropdown-item">
       					<span class="text-white">VI.</span>
       						<input type="checkbox" id="checked_fishport" value="FISHPORT">
      						<img src="/static/images/pin2022/FishPort.png" style="width: 20px; height: 25px;"/>
       						<span class="text-white">Fish Port</span>
      					</li>
       			</ul>
       		</li>
       		
       				


	  	<li>         
       		<a href="#pageOther" data-toggle="collapse"  aria-expanded="false"> 
     	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;OTHER RESOURCES</span></a> 
       	<ul class="collapse list-unstyled" id="pageOther"> 
        
        <li class="dropdown-subitem">         
       		<a href="#pageBodiesofWater" data-toggle="collapse"  aria-expanded="false"> 
     	 		<span class="text-white">&nbsp;I. Bodies of Water</span></a> 
       			<ul class="collapse list-unstyled" id="pageBodiesofWater"> 			
         <li class="dropdown-subitem">         
       		<a href="#pageCreate" data-toggle="collapse"  aria-expanded="false"> 
     	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;Fisheries Management Areas</span></a> 
       			<ul class="collapse list-unstyled" id="pageCreate"> 
        			<li><input id="FMA" type="checkbox"><span class="text-white">Select all</span></li>
      					<li class="dropdown-item"> 
      						<input type="checkbox" id="FMA-1" value="FMA-1" name="FMA" class="FMA">
       						<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
       						<span class="text-white">FMA 1</span>
       					</li> 
       				<li class="dropdown-item">
      					<input type="checkbox" id="FMA-2" value="FMA-2" name="FMA" class="FMA">
       					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
       					<span class="text-white">FMA 2</span>
       				</li>

      				<li class="dropdown-item">
      					<input type="checkbox" id="FMA-3" value="FMA-3" name="FMA" class="FMA">
       					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
       					<span class="text-white">FMA 3</span>
       				</li>
       				<li class="dropdown-item">
      					<input type="checkbox" id="FMA-4" value="FMA-4" name="FMA" class="FMA">
       					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
       					<span class="text-white">FMA 4</span>
       				</li>
       				<li class="dropdown-item">
      					<input type="checkbox" id="FMA-5" value="FMA-5" name="FMA" class="FMA">
       					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
       					<span class="text-white">FMA 5</span> 
       				</li> 
       				<li class="dropdown-item">
      					<input type="checkbox" id="FMA-6" value="FMA-6" name="FMA" class="FMA">
       					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
       					<span class="text-white">FMA 6</span>
       				</li>
       				<li class="dropdown-item">
      					<input type="checkbox" id="FMA-7" value="FMA-7" name="FMA" class="FMA">
       					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
       					<span class="text-white">FMA 7</span> 
       				</li>
       				<li class="dropdown-item">
      					<input type="checkbox" id="FMA-8" value="FMA-8" name="FMA" class="FMA">
       					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
       					<span class="text-white">FMA 8</span>
       				</li>
       				<li class="dropdown-item"> 
      					<input type="checkbox" id="FMA-9" value="FMA-9" name="FMA" class="FMA">
       					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
       					<span class="text-white">FMA 9</span>
       				</li>
       				<li class="dropdown-item"> 
      					<input type="checkbox" id="FMA-10" value="FMA-10" name="FMA" class="FMA">
       					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
       					<span class="text-white">FMA 10</span>
       				</li>
       				<li class="dropdown-item"> 
      					<input type="checkbox" id="FMA-11" value="FMA-11" name="FMA" class="FMA">
       					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
       					<span class="text-white">FMA 11</span>
       				</li>
       				<li class="dropdown-item"> 
      					<input type="checkbox" id="FMA-12" value="FMA-12" name="FMA" class="FMA">
       					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
       					<span class="text-white">FMA 12</span> 
	  				</li> 
       	      	
      			</ul>
      		</li>      
      		<li class="dropdown-subitem">
       <a href="#major_fg" data-toggle="collapse"  aria-expanded="false">
     	 <span class="text-white"><i class="fas fa-list"></i>&nbsp;24 Major Fishing Grounds</span>
       </a>
        <ul class="collapse list-unstyled" id="major_fg">
        <li><input id="major24_fg" type="checkbox"><span class="text-white">Select all</span></li>
      	<li class="dropdown-item">
      		<input type="checkbox" id="checked_lingayen_gulf" value="LINGAYEN GULF" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">1 - LINGAYEN GULF</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_manila_bay" value="Manila Bay" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">2 - Manila Bay</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_batangas_coast" value="Batangas Coast" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">3 - Batangas Coast</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_tayabas_bay" value="Tayabas Bay" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">4 - Tayabas Bay</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_wpalawan_waters" value="West Palawan waters" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">5 - West Palawan waters</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_cuyo_pass" value="Cuyo Pass" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">6 - Cuyo Pass</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_wsulu_sea" value="West Sulu Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">7 - West Sulu Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_ssulu_sea" value="South Sulu Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">8- South Sulu Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_esulu_sea" value="East Sulu Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">9 - East Sulu Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_moro_gulf" value="Moro Gulf" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">10 - Moro Gulf</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_davao_gulf" value="Davao Gulf" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">11 - Davao Gulf</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_samar_gulf" value="Samar Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">12 - Samar Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_sibuyan_gulf" value="Sibuyan Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">13 - Sibuyan Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_camotes_gulf" value="Camotes Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">14 - Camotes Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_visayan_gulf" value="Visayan Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">15 - Visayan Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_guimaras_gulf" value="Guimaras Strait" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">16 - Guimaras Strait</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_bohol_gulf" value="Bohol Sea" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">17 - Bohol Sea</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_ragay_gulf" value="Ragay Gulf" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">18 - Ragay Gulf</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_leyte_gulf" value="Leyte Gulf" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">19 - Leyte Gulf</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_lagonoy_gulf" value="Lagonoy Gulf" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">20 - Lagonoy Gulf</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_lamon_bay" value="Lamon Bay" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">21 - Lamon Bay</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_casiguran_sound" value="Casiguran Sound" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">22 - Casiguran Sound</span>
       	</li>
       	<li class="dropdown-item">
      		<input type="checkbox" id="checked_palanan_bay" value="Palanan Bay" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">23 - Palanan Bay</span>
       	</li>
       <li class="dropdown-item">
      		<input type="checkbox" id="checked_babuyan_channel" value="Babuyan Channel" class="FG">
       			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
       			<span class="text-white">24 - Babuyan Channel</span>
       	</li>
       	</ul>
       	</li>
           <li class="dropdown-item">
      	 		<input type="checkbox" id="checked_municipal_waters" value="Municipal Water ">
      	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
     	 		<span class="text-white">Municipal Waters</span>
	  	</li>
	  <li class="dropdown-item">
      	 		<input type="checkbox" id="checked_rivers" value="River">
      	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
     	 		<span class="text-white">Rivers</span>
	  </li>
	  <li class="dropdown-item">
      	 		<input type="checkbox" id="checked_lakes" value="Lakes">
      	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
     	 		<span class="text-white">Lakes</span>
	  </li>
	  <li class="dropdown-item">
      	 		<input type="checkbox" id="checked_bay" value="Bay">
      	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
     	 		<span class="text-white">Bay</span>
	  </li>
	  <li class="dropdown-item">
      	 		<input type="checkbox" id="checked_strait" value="Strait">
      	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
     	 		<span class="text-white">Strait</span>
	  </li>
	  <li class="dropdown-item">
      	 		<input type="checkbox" id="checked_channel" value="Channel">
      	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
     	 		<span class="text-white">Channel</span>
	  </li>
	  <li class="dropdown-item">
      	 		<input type="checkbox" id="checked_gulf" value="Gulf">
      	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
     	 		<span class="text-white">Gulf</span>
	  </li>
	  <li class="dropdown-item">
      	 		<input type="checkbox" id="checked_sea" value="Sea">
      	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
     	 		<span class="text-white">Sea</span>
	  </li>
	  
	  </ul>	  
	  </li>
	  <li class="dropdown-subitem">         
       		<a href="#pagefishhabitat" data-toggle="collapse"  aria-expanded="false"> 
     	 		<span class="text-white">&nbsp;II. Fish Habitat</span></a> 
       			<ul class="collapse list-unstyled" id="pagefishhabitat"> 
       			<li><input id="habitat" type="checkbox"><span class="text-white">Select all</span></li>
      			<li class="dropdown-item">
      				<input type="checkbox" id="checked_seagrass" value="seagrass" class="habitat">
       				<img src="/static/images/pin2022/Seagrass.png" style="width: 20px; height: 25px;"/>
       				<span class="text-white">Sea Grass</span>
      			</li>
      			<li class="dropdown-item">
      				<input type="checkbox" id="checked_mangrove" value="mangrove" class="habitat">
        			<img src="/static/images/pin2022/Mangrove.png" style="width: 20px; height: 25px;"/>
       				<span class="text-white">Mangrove</span>
      			</li>
      			<li class="dropdown-item">
      				<input type="checkbox" id="checked_coralreefs" value="fishcoral" class="habitat">
      				<img src="/static/images/pin2022/Coral-Reefs.png" style="width: 20px; height: 25px;"/>
       				<span class="text-white">Coral Reefs</span>
      			</li>
       			</ul>
       </li>
       <li class="dropdown-subitem">         
       		<a href="#pageinfrustracture" data-toggle="collapse"  aria-expanded="false"> 
     	 		<span class="text-white">&nbsp;III. BFAR Infrastructure</span></a> 
       			<ul class="collapse list-unstyled" id="pageinfrustracture"> 
       				<li><input id="infrastructure" type="checkbox"><span class="text-white">Select all</span></li>
      				<li class="dropdown-item">
      					<input type="checkbox" id="check_nationalcenter" value="national_center" name="infrastructure" class="infrastructure">
       					<img src="/static/images/pin2022/BINTCenter.png" style="width: 20px; height: 25px;"/>
       					<span class="text-white">National Technology Center</span>
       				</li>
       				<li class="dropdown-item">
      					<input type="checkbox" id="check_regionaloffices" value="regional_offices" name="infrastructure" class="infrastructure">
       					<img src="/static/images/pin2022/BIRFO.png" style="width: 20px; height: 25px;"/>
       					<span class="text-white">Regional Fisheries Office</span>
       				</li>
       				<li class="dropdown-item">
      					<input type="checkbox" id="check_rdtc" value="regional_offices" name="infrastructure" class="infrastructure">
       					<img src="/static/images/pin2022/BIRDTC.png" style="width: 20px; height: 25px;"/>
       					<span class="text-white">Regional Development Training Center</span>
       				</li>
       				<li  class="dropdown-item">
       					<input type="checkbox" id="checked_lgu" value="lgu" name="infrastructure" class="infrastructure">
        				<img src="/static/images/pin2022/BFAR_Insfrastructure_ProvincialFisheriesOffice.png" style="width: 20px; height: 25px;"/>
        				<span class="text-white">Provincial Fisheries Office</span>
      				</li>
      				<li class="dropdown-item">
      					<input type="checkbox" id="check_tos" value="technology_outreach_station" name="infrastructure" class="infrastructure">
       					<img src="/static/images/pin2022/BFAR_Insfrastructure_TechnologyOutreachStation.png" style="width: 20px; height: 25px;"/>
       					<span class="text-white">Technology Outreach Station</span>
       				</li>
       				<li class="dropdown-item">
      					<input type="checkbox" id="checked_schoolOfFisheries" value="schoolOfFisheries" name="infrastructure" class="infrastructure">
       					<img src="/static/images/pin2022/BFAR_Insfrastructure_FisheriesSchool.png" style="width: 20px; height: 25px;"/>
       					<span class="text-white">Fisheries School</span>
	  				</li>
       			</ul>
       </li>
       <li class="dropdown-subitem">         
       		<a href="#pageoma" data-toggle="collapse"  aria-expanded="false"> 
     	 		<span class="text-white">&nbsp;IV. Other Management Area</span></a> 
       			<ul class="collapse list-unstyled" id="pageoma"> 
       			<li class="dropdown-item">
      	 		<input type="checkbox" id="checked_fishsanctuaries" value="fishsanctuaries" class="habitat">
      	 		 <img src="/static/images/pin2022/FishSanctuary.png" style="width: 20px; height: 25px;" data-toggle="tooltip" title="Fish Sanctuary"/>
     	 		<span class="text-white">Fish Sanctuary</span>
	  		</li>
       			</ul>
       </li>	
       </ul>
       </li>
           	
       	</ul>

      <ul class="nav justify-content-end">
      <li><a href="/login" class=" dropdown-item"><i class="fa fa-fw fa-user"></i><span class="text">LOGIN</span></a></li>
</ul>
      </div>