  
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!doctype html>
<html lang="en">
<head>
<title>FRGIS - CHART</title>
<meta charset="utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />
  <jsp:include page="../../menu/css.jsp"></jsp:include>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <style>
    .chart {
    display: block;
	margin: 0 auto;
    }
    
    .my-custom-scrollbar {
		position: relative;
		height: 200px;
		overflow: auto;
	}
.table-wrapper-scroll-y {
display: block;
}
table.table-fit {
    width: auto !important;
    table-layout: auto !important;
}
table.table-fit thead th, table.table-fit tfoot th {
    width: auto !important;
}
table.table-fit tbody td, table.table-fit tfoot td {
    width: auto !important;
}
    </style>


</head>
<body>

 <jsp:include page="../../home/header_bar.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">

    <div id="content" class="p-4 p-md-5 pt-5 section-to-print">	   

	<!-- <table class="columns">
      <tr>
        <td>
        	<div id='pngForSeaWeeds'></div>
			<div id="chartSeaWeeds_div" style="border: 1px solid #ccc"></div>
        </td>
      </tr>
    </table>-->
    <div id="table_div"></div>
     <hr>


					<div class="table-wrapper-scroll-y my-custom-scrollbar">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Municipal/City</th>
									<th scope="col">Select</th>
								</tr>
							</thead>
							<tbody class="municipal">

							</tbody>
						</table>
						</div>
						<button type="button" onclick="myFunc()" id="submitMunicipality"
							class="submitMunicipality btn btn-primary">SUBMIT</button>


			
			<hr>
     
     		<div id="chartShow" style="display: none;">
         	<div id="colFilter_div"></div>
			<div id="chart_div" style="border: 1px solid #ccc"></div>
   			</div>
			</div>

</div>

    <script type="text/javascript">
    var chartShowHide = ${show};
    if(!chartShowHide){
    	$('#chartShow').show();
    }
    
/*     window.onbeforeunload = function() {
    	   localStorage.setItem(province, ${provinceID});
    	}
    window.onload = function() {
    	   var province = localStorage.getItem(province);

    	}
    console.log("province: " + province); */
    // var province = ${provinceID};
 
     var slctMunicipal=$('.municipal');
	  var option="";
	  
	  slctMunicipal.empty();
 	option = "";

 $.get("/create/getMunicipalityList/" + '${provinceID}' , function(data, status) {  
		var id=1;
		slctMunicipal.append(option);
		for( let prop in data ){						 					
				//option = option + data[prop].municipal  + "<input type='checkbox' name='myParam[]' class='form-control' value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'/> ";	           
			
				option = option +	"<tr><th>" + id++  + "</th>" + 
				"<td>" + data[prop].municipal  + "</td>" +
				"<td><input type='checkbox' name='myParam' value='"+data[prop].municipal_id+ "-" + data[prop].municipal+"'/> </td></tr>";
		}
		slctMunicipal.append(option);
	});
 
 function myFunc() {
	 let arr = [];
	 let checkboxes = document.querySelectorAll("input[type='checkbox']:checked");
	 for (let i = 0; i < checkboxes.length; i++) {
	 arr.push(checkboxes[i].value)
	 }
		
	 // Converted array to string & alert
	  window.location.replace("/create/municipalList/" + arr.toString());

	 }

    google.load('visualization', '1', {packages: ['controls']});
    google.setOnLoadCallback(drawChart); 

    function drawChart () {
    	
    	 var data = google.visualization.arrayToDataTable([
    	        ['Province', 'CFLC-Operational','CFLC-For Operation','CFLC-For Completion','CFLC-For Transfer','CFLC-Damaged','Traditional','Non-Traditional'],
    	        <c:forEach items="${provinceChart}" var="entry" varStatus="status">
        		['${entry.province}',${entry.operational},${entry.forOperation},${entry.forCompletion},${entry.forTransfer},${entry.damaged},${entry.traditional},${entry.nonTraditional}],
        	
        		
        		 </c:forEach>
    	      ]);

        var columnsTable = new google.visualization.DataTable();
        columnsTable.addColumn('number', 'colIndex');
        columnsTable.addColumn('string', 'colLabel');
        var initState= {selectedValues: []};
        // put the columns into this data table (skip column 0)
        
        for (var i = 1; i < data.getNumberOfColumns(); i++) {
            columnsTable.addRow([i, data.getColumnLabel(i)]);
        }
        initState.selectedValues.push(data.getColumnLabel(7));
        
        var chart = new google.visualization.ChartWrapper({
            chartType: 'BarChart',
            containerId: 'chart_div',
            dataTable: data,
            options: {
                title: '${region_title} - ${province_title}',
                width: 1000,
                height: 800
            }
        }); 

        
        var columnFilter = new google.visualization.ControlWrapper({
            controlType: 'CategoryFilter',
            containerId: 'colFilter_div',
            dataTable: columnsTable,
            options: {
                filterColumnLabel: 'colLabel',
                ui: {
                    label: 'Columns',
                    allowTyping: false,
                    allowMultiple: false,
                    allowNone: false
                  // selectedValuesLayout: 'none'
                }
            },
            state: initState
        });
        
        function setChartView () {
            var state = columnFilter.getState();
            var row;
            var view = {
                columns: [0]
            };
            for (var i = 0; i < state.selectedValues.length; i++) {
                row = columnsTable.getFilteredRows([{column: 1, value: state.selectedValues[i]}])[0];
                view.columns.push(columnsTable.getValue(row, 0));
            }
            // sort the indices into their original order
            view.columns.sort(function (a, b) {
                return (a - b);
            });
            chart.setView(view);
            chart.draw();
        }
        google.visualization.events.addListener(columnFilter, 'statechange', setChartView);
        
        setChartView();
        columnFilter.draw();
    }

/*     google.charts.load('current', {'packages':['table']});
    google.charts.setOnLoadCallback(drawTable);
    function drawTable() {
    	
    	
        var data = new google.visualization.DataTable();
          
        data.addColumn('string', 'Province of ${province_title}');
        data.addColumn('number', 'CFLC-Operational');
        data.addColumn('number', 'CFLC-For Operation');
        data.addColumn('number', 'CFLC-For Completion');
        data.addColumn('number', 'CFLC-For Transfer');
        data.addColumn('number', 'CFLC-Damaged');
        data.addColumn('number', 'Traditional');
        data.addColumn('number', 'Non-Traditional');
        data.addRows([
        	
        	<c:forEach items="${provinceChart}" var="entry">
        	
        		['${entry.province}',
        		${entry.operational},
        		${entry.forOperation},
        		${entry.forCompletion},
        		${entry.forTransfer},
        		${entry.damaged},
        		${entry.traditional},
        		${entry.nonTraditional}],
			</c:forEach>
            
        ]);

        var options = {
                showRowNumber: true,
                width: '100%', 
                height: '100%',
                title: 'POST HARVEST(FISHLANDING) - ${region_title} - ${province_title}'
             };
        var table = new google.visualization.Table(document.getElementById('table_div'));
        table.draw(data, options);
        

      }  */

/* 		google.charts.load("current", {packages:['corechart']});
	
		var municipalSize = '${fn:length(municipalities)}';
  	    
	      
 	 	   google.charts.setOnLoadCallback(drawSeaWeeds);

  	      function drawSeaWeeds(){
  	    	 var data = google.visualization.arrayToDataTable([
  	            ['Resources',
  	            	<c:forEach items="${municipalities}" var="municipal">	
  	            	'${municipal.municipal}',
  	            </c:forEach>
  	            	{ role: 'annotation' } ],
  	            	
  	            ['CFLC-Operational',
  	            	<c:forEach items="${provinceChart}" var="entry">
  	            		<c:if test = "${entry.resources == 'Operational'}">	            		
  	            			${entry.total},
  	            		</c:if>	            	
  					</c:forEach>
  					''
  	            	],
  	            ['CFLC-For Operation',
  	            	
  	            	<c:forEach items="${provinceChart}" var="entry">
  	            		<c:if test = "${entry.resources == 'ForOperation'}">
          					${entry.total},
          				</c:if>
 					</c:forEach>
  	            	
  	            	''],
  	             ['CFLC-For Completion',
  	  	            	
  	  	            	<c:forEach items="${provinceChart}" var="entry">
  	  	            		<c:if test = "${entry.resources == 'ForCompletion'}">
  	          					${entry.total},
  	          				</c:if>
  	 					</c:forEach>
  	  	            	
  	  	         ''],
				['CFLC-For Transfer',
  	  	            	
  	  	            	<c:forEach items="${provinceChart}" var="entry">
  	  	            		<c:if test = "${entry.resources == 'ForTransfer'}">
  	          					${entry.total},
  	          				</c:if>
  	 					</c:forEach>
  	  	            	
  	  	         ''], 
 				['CFLC-Damaged',
	  	            	
	  	            	<c:forEach items="${provinceChart}" var="entry">
	  	            		<c:if test = "${entry.resources == 'Damaged'}">
	          					${entry.total},
	          				</c:if>
	 					</c:forEach>
	  	            	
	  	         ''],  
 				['Traditional',
	  	            	
	  	            	<c:forEach items="${provinceChart}" var="entry">
	  	            		<c:if test = "${entry.resources == 'Traditional'}">
	          					${entry.total},
	          				</c:if>
	 					</c:forEach>
	  	            	
	  	         ''],
 				['Non-Traditional',
	  	            	
	  	            	<c:forEach items="${provinceChart}" var="entry">
	  	            		<c:if test = "${entry.resources == 'Non-Traditional'}">
	          					${entry.total},
	          				</c:if>
	 					</c:forEach>
	  	            	
	  	         '']
  	          
  	          ]);
  	    	 
  	    	var view = new google.visualization.DataView(data);

	    	var columns = [];
	    	for (var i = 0; i <= provinceSize ; i++) {
	    	    if (i > 0) {
	    	        columns.push(i);
	    	        columns.push({
	    	        	calc: function (dt, row) {
	 			 	    	  if(dt.getValue(row, i) == 0){
	 			 	    		  return ""; 
	 			 	    	  }else{ 
	 			 	        	return dt.getValue(row, i).toString();
	 			 	    	  }	
	 			 	      },
	 			 	      type: "string",
	 			 	      role: "annotation"
	    	        });

	    	    } else {
	    	        columns.push(i);
	    	    }
	    	}
	    	
	    	columns.push(
	    		{
		 	      calc: function (dt, row) {
		 	        return 0;
		 	      },
		 	      label: "Total",
		 	      type: "number"		  		 	   	
	        	},
	        );
	    	columns.push(
	    		{	  	    		
	    		 calc: function (dt, row) {
	    			if(municipalSize == 6){
	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5) + dt.getValue(row, 6);
	    			}
	    			if(municipalSize == 5){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5);
	  	    		}
	    			if(municipalSize == 4){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4);
	  	    		}
	    			},
		 	      type: "number",
		 	      role: "annotation"
	    	
	    	}
	    		);
	    	
	    	view.setColumns(columns);


  	          var options = {
  	            width: 1000,
  	            height: 600,
  	           	title: 'POST HARVEST(FISHLANDING) - ${region_title}' ,
  	            legend: { position: 'top', maxLines: 5 },
  	            bar: { groupWidth: '75%' },
  	            isStacked: true,
  	          vAxis: {
	  	            gridlines: {color: '#007bff;'},
	  	            minValue: 0,
	  	          title: 'Number of Resources'
	  	          }
  	          };

  	         var chart = new google.visualization.ColumnChart(document.getElementById('chartSeaWeeds_div'));
  	      
  	         google.visualization.events.addListener(chart, 'ready', function () {
  	          // chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';

  	          document.getElementById('pngForSeaWeeds').outerHTML = '<a href="' + chart.getImageURI() + '" target="_blank">PRINT POST HARVEST(FISHLANDING)  CHARTS</a>';
  	         });
  	       
   	        chart.draw(view, options);
  	      } */


</script>
</body>

</html>