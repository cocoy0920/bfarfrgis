  
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!doctype html>
<html lang="en">
<head>
<title>FRGIS - CHART</title>
<meta charset="utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <style type="text/css">
.chart {
	display: block;
	margin: 0 auto;
}

.my-custom-scrollbar {
	position: relative;
	width: 200px;
	height: 200px;
	overflow: auto;
}

.table-wrapper-scroll-y {
	display: block;
}

/* table.table-fit {
	width: auto !important;
	table-layout: auto !important;
}

table.table-fit thead th, table.table-fit tfoot th {
	width: auto !important;
}

table.table-fit tbody td, table.table-fit tfoot td {
	width: auto !important;
} */

</style>


</head>
<body>

 <jsp:include page="../home/header_bar.jsp"></jsp:include>

 
<div class="wrapper d-flex align-items-stretch">
<nav id="sidebar">
<!-- <div class="custom-menu">
<button type="button" id="sidebarCollapse" class="btn btn-primary">
<i class="fa fa-bars"></i>
<span class="sr-only">Toggle Menu</span>
</button>
</div> -->
<!-- <div class="p-4"> -->
<!-- <div class="sidebar-header">
      <img class=" img img-fluid" alt="" src="/static/images/FRGIS-LOGO.png">
    </div> -->
<ul class="list-unstyled components mb-5">

             <li> 
    		<a href="#aqualist" data-toggle="collapse"  aria-expanded="false"> 
	  			<span class="text-white"><i class="fas fa-list"></i>&nbsp;AQUACULTURE</span></a> 
    		<ul class="collapse list-unstyled" id="aqualist"> 

   			<li class="dropdown-subitem"> 
   				<a href="#farmlist" data-toggle="collapse"  aria-expanded="false"> 
  	 			<span class="text-white">&nbsp;I. Aquafarms</span></a> 
    				<ul class="collapse list-unstyled" id="farmlist"> 
     				<%-- <li>
     				   	<c:if test="${page == true }">	
	    						<a  class=" dropdown-item" tabindex="-1" href="../aquaChart" >
	    						<img src="/static/images/pin2022/FishPond.png" style="width: 20px; height: 20px;"/>
    							 <span class="text-white">Select all</span>
    							</a>
    					</c:if>
    					<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="aquaChart" >
    								<img src="/static/images/pin2022/FishPond.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Select all</span>
    								</a>
    				</c:if>
     			
     				</li> --%>
   					<li class="dropdown-item"> 
   					
   								<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../aquaChart" >
	    							<img src="/static/images/pin2022/FishPond.png" style="width: 20px; height: 20px;"/>
    							 	<span class="text-white">Fish Pond</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="aquaChart" >
    								<img src="/static/images/pin2022/FishPond.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Pond</span>
    								</a>
    							</c:if>
    					</li>
    					<li class="dropdown-item">
    					
    					   					
   								<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../aquaChart" >
	    							<img src="/static/images/pin2022/FishPen.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Pen</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="aquaChart" >
    								<img src="/static/images/pin2022/FishPen.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Pen</span>
    								</a>
    							</c:if>
 
   					</li>
   					<li class="dropdown-item">
   					
   					   		<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../aquaChart" >
	    							<img src="/static/images/pin2022/FishCage.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Cage</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="aquaChart" >
    								<img src="/static/images/pin2022/FishCage.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Cage</span>
    								</a>
    							</c:if>
    						
   					</li>
   					<li class="dropdown-item">
   						<c:if test="${page == true }">	
	    							<a  class=" dropdown-item" tabindex="-1" href="../aquaChart" >
	    							<img src="/static/images/pin2022/FishCorral.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Corral(BAKLAD)</span>
    								</a>
    							</c:if>
    							<c:if test="${page == false }">								
    								<a  class=" dropdown-item" tabindex="-1" href="aquaChart" >
    								<img src="/static/images/pin2022/FishCorral.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Corral(BAKLAD)</span>
    								</a>
    							</c:if>
   						
   					</li>
    				  </ul> 
      		  </li> 
      		<li class="dropdown-subitem"> 
   				<a href="#aquaseaweedlist" data-toggle="collapse"  aria-expanded="false"> 
  	 			<span class="text-white">&nbsp;II. Seaweed</span></a> 
    				<ul class="collapse list-unstyled" id="aquaseaweedlist"> 
     				<!-- <li><input id="aquaseaweed" type="checkbox"><span class="text-white">Select all</span></li> -->
   					<li class="dropdown-item"> 
   						<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../seaweedsNLChart" >
    							<img src="/static/images/pin2022/Seaweeds_Nursery.png" style="width: 20px; height: 20px;"/>
    							<span class="text-white">Seaweed Nursery</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="seaweedsNLChart" >
    							<img src="/static/images/pin2022/Seaweeds_Nursery.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Seaweed Nursery</span>
    							</a>
    							</c:if>

    					</li>
    					<li class="dropdown-item">
    					   	<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../seaweedsNLChart" >
    								<img src="/static/images/pin2022/Seaweeds_laboratory.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Seaweed Laboratory</span>
    							</a>
    						</c:if>
    						<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="seaweedsNLChart" >
    							<img src="/static/images/pin2022/Seaweeds_laboratory.png" style="width: 20px; height: 20px;"/>
    							<span class="text-white">Seaweed Laboratory</span>
    							</a>
    						</c:if>
   						
   					</li>
   					
    				</ul> 
      		</li>
      		<li class="dropdown-subitem"> 
   				<a href="#mariculturelist" data-toggle="collapse"  aria-expanded="false"> 
  	 			<span class="text-white">&nbsp;III. Mariculture Park</span></a> 
    				<ul class="collapse list-unstyled" id="mariculturelist"> 
     				<!-- <li><input id="aquamariculture" type="checkbox"><span class="text-white">Select all</span></li> -->
   					<li class="dropdown-item"> 
   					<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../maricultureChart" >
    							<img src="/static/images/pin2022/Mariculture_BFAR.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">BFAR Managed</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="maricultureChart" >
    							<img src="/static/images/pin2022/Mariculture_BFAR.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">BFAR Managed</span>
    							</a>
    				</c:if>
   						
    					</li>
    					<li class="dropdown-item">
    					<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../maricultureChart" >
    								<img src="/static/images/pin2022/Mariculture_LGU.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">LGU Managed</span>
    							</a>
    					</c:if>
    					<c:if test="${page == false }">	
    								<a  class=" dropdown-item" tabindex="-1" href="maricultureChart" >
    									<img src="/static/images/pin2022/Mariculture_LGU.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">LGU Managed</span>
    								</a>
    					</c:if>
   						
   					</li>
   					<li class="dropdown-item">
   					    <c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../maricultureChart" >
    								<img src="/static/images/pin2022/Mariculture_PrivateSector.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Private Sector</span>
    							</a>
    					</c:if>
    					<c:if test="${page == false }">	
    								<a  class=" dropdown-item" tabindex="-1" href="maricultureChart" >
    									<img src="/static/images/pin2022/Mariculture_PrivateSector.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Private Sector</span>
    								</a>
    					</c:if>
   						
   					</li>
   					
    				</ul> 
      		</li>
      		<li class="dropdown-subitem"> 
   				<a href="#hatcherylist" data-toggle="collapse"  aria-expanded="false"> 
  	 			<span class="text-white">&nbsp;IV. Hatchery</span></a> 
    				<ul class="collapse list-unstyled" id="hatcherylist"> 
     			<!-- <li><input id="aquahatchery" type="checkbox"><span class="text-white">Select all</span></li> -->
   					<li class="dropdown-subitem"> 
   						<a href="#bfarmanaged" data-toggle="collapse"  aria-expanded="false"> 
  	 						<span class="text-white">&nbsp;BFAR Managed</span></a> 
    							<ul class="collapse list-unstyled" id="bfarmanaged">  							
   								<li class="dropdown-item">
   								<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../hatcheryChart" >
    							<img src="/static/images/pin2022/Hatchery.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Legislated</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="hatcheryChart" >
    							<img src="/static/images/pin2022/Hatchery.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Legislated</span>
    							</a>
    							</c:if>

   								</li>
   								<li class="dropdown-item">
   								<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../hatcheryChart" >
    							<img src="/static/images/pin/20x34/hatcheries.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Non-Legislated</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="hatcheryChart" >
    							<img src="/static/images/pin/20x34/hatcheries.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Non-Legislated</span>
    							</a>
    							</c:if>
   									
   								</li>
   					
    							</ul> 
      					</li>
    					<li class="dropdown-item">
    					<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../hatcheryChart" >
    							<img src="/static/images/pin/new/Hatchery_LGU.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">LGU Managed</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="hatcheryChart" >
    							<img src="/static/images/pin/new/Hatchery_LGU.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">LGU Managed</span>
    							</a>
    							</c:if>
   					
   					</li>
   					<li class="dropdown-item">
   					<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../hatcheryChart" >
    							<img src="/static/images/pin2022/Hatchery_PrivateSector.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Private Sector</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="hatcheryChart" >
    							<img src="/static/images/pin2022/Hatchery_PrivateSector.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Private Sector</span>
    							</a>
    							</c:if>
   						
   					</li>
   					
    				</ul> 
      		</li>
    			</ul> 
    		</li>
    		<li> 
    			<a href="#capturedList" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;CAPTURE FISHERIES</span></a> 
    				<ul class="collapse list-unstyled" id="capturedList"> 
     				<!-- <li><input id="capture" type="checkbox"><span class="text-white">Select all</span></li> -->
   					<li class="dropdown-item"> 
   						<input type="checkbox" id="checked_payao" value="payao" name="payao" class="capture">
    						<img src="/static/images/pin2022/Payao.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Payao</span>
    					</li> 
    					<li class="dropdown-item">
   						<input type="checkbox" id="checked_lambaklad" value="lambaklad" name="lambaklad" class="capture">
    						<img src="/static/images/pin2022/Lambaklad.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Lambaklad</span>
    					</li>

   					<li class="dropdown-item">
   						<input type="checkbox" id="checked_frpboats" value="frpboats" name="frpboats" class="capture">
    						<img src="/static/images/pin2022/FRP_Boats.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">FRP Boats</span>
    					</li>	
    				</ul>
    		</li>
    		   <li> 
    			<a href="#postharvestList" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;POST HARVEST</span></a> 
    				<ul class="collapse list-unstyled" id="postharvestList"> 
     				<li class="dropdown-subitem">
    						<a href="#postharvestFishlanding" data-toggle="collapse"  aria-expanded="false">
  	 						<span class="text-white">&nbsp;I. Fish Landing</span>
    						</a>
     					<ul class="collapse list-unstyled" id="postharvestFishlanding">
     						<!-- <li><input id="fishlanding" type="checkbox"><span class="text-white">Select all</span></li>
     						 --><li class="dropdown-subitem">
    								<a href="#postharvestCFLC" data-toggle="collapse"  aria-expanded="false">
  	 							
  	 							<span class="text-white">&nbsp;I.1 CFLC</span>
    								</a>
     								<ul class="collapse list-unstyled" id="postharvestCFLC">
     						
     									<li class="dropdown-item">
     										<c:if test="${page == true }">
    											<a  class=" dropdown-item" tabindex="-1" href="../fishlandingChart" >
    											<img src="/static/images/pin2022/CFLC_operational.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">OPERATIONAL</span>
    											</a>
    										</c:if>
    										<c:if test="${page == false }">
    											<a  class=" dropdown-item" tabindex="-1" href="fishlandingChart" >
    											<img src="/static/images/pin2022/CFLC_operational.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">OPERATIONAL</span>
    											</a>
    										</c:if>

    										</li>
    										<li class="dropdown-item">
    										<c:if test="${page == true }">
    											<a  class=" dropdown-item" tabindex="-1" href="../fishlandingChart" >
    											<img src="/static/images/pin2022/CFLC_foroperational.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">FOR OPERATION</span>
    											</a>
    										</c:if>
    										<c:if test="${page == false }">
    											<a  class=" dropdown-item" tabindex="-1" href="fishlandingChart" >
    											<img src="/static/images/pin2022/CFLC_foroperational.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">FOR OPERATION</span>
    											</a>
    										</c:if>
   											
    										</li>
    										<li class="dropdown-item">
    										<c:if test="${page == true }">
    											<a  class=" dropdown-item" tabindex="-1" href="../fishlandingChart" >
    											<img src="/static/images/pin2022/CFLC_completion.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">FOR COMPLETION</span>
    											</a>
    										</c:if>
    										<c:if test="${page == false }">
    											<a  class=" dropdown-item" tabindex="-1" href="fishlandingChart" >
    											<img src="/static/images/pin2022/CFLC_completion.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">FOR COMPLETION</span>
    											</a>
    										</c:if>
   												
    										</li>
    										<li class="dropdown-item">
    										<c:if test="${page == true }">
    											<a  class=" dropdown-item" tabindex="-1" href="../fishlandingChart" >
    											<img src="/static/images/pin2022/CFLC_transfer.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">FOR TRANSFER</span>
    											</a>
    										</c:if>
    										<c:if test="${page == false }">
    											<a  class=" dropdown-item" tabindex="-1" href="fishlandingChart" >
    											<img src="/static/images/pin2022/CFLC_transfer.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">FOR TRANSFER</span>
    											</a>
    										</c:if>
   												
    										</li>
    										<li class="dropdown-item">
    										<c:if test="${page == true }">
    											<a  class=" dropdown-item" tabindex="-1" href="../fishlandingChart" >
    											<img src="/static/images/pin2022/CFLC_damaged.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">DAMAGED</span>
    											</a>
    										</c:if>
    										<c:if test="${page == false }">
    											<a  class=" dropdown-item" tabindex="-1" href="fishlandingChart" >
    											<img src="/static/images/pin2022/CFLC_damaged.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">DAMAGED</span>
    											</a>
    										</c:if>
   												
    										</li>
   									</ul>
   								</li>		
   								<li class="dropdown-item">
   								<span class="text-white">I. 2</span>
   								
   										<c:if test="${page == true }">
    											<a  class=" dropdown-item" tabindex="-1" href="../fishlandingChart" >
    											<img src="/static/images/pin2022/traditional.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">TRADITIONAL</span>
    											</a>
    										</c:if>
    										<c:if test="${page == false }">
    											<a  class=" dropdown-item" tabindex="-1" href="fishlandingChart" >
    											<img src="/static/images/pin2022/traditional.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">TRADITIONAL</span>
    											</a>
    										</c:if> 
    										
    									
    								</li>
    								<li class="dropdown-item">
    								<span class="text-white">I. 3</span>
    								<c:if test="${page == true }">
    											<a  class=" dropdown-item" tabindex="-1" href="../fishlandingChart" >
    											<img src="/static/images/pin2022/non-traditional.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">NON-TRADITIONAL</span>
    											</a>
    										</c:if>
    										<c:if test="${page == false }">
    											<a  class=" dropdown-item" tabindex="-1" href="fishlandingChart" >
    											<img src="/static/images/pin2022/non-traditional.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">NON-TRADITIONAL</span>
    											</a>
    										</c:if> 
    									
    								</li>
    									
    						</ul>
    					</li>
    					<li class="dropdown-subitem">
    						<a href="#postharvestColdStorage" data-toggle="collapse"  aria-expanded="false">
  	 						<span class="text-white">&nbsp;II. Cold Storage/IPCS</span>
    						</a>
     					<ul class="collapse list-unstyled" id="postharvestColdStorage">
     						<!-- <li><input id="coldstorage" type="checkbox"><span class="text-white">Select all</span></li> -->
     						<li class="dropdown-item">
     							<c:if test="${page == true }">		
    								<a  class=" dropdown-item" tabindex="-1" href="../coldChart" >
    							<img src="/static/images/pin2022/IcePlant_BFAR.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">BFAR</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">		
    							<a  class=" dropdown-item" tabindex="-1" href="coldChart" >
    							<img src="/static/images/pin2022/IcePlant_BFAR.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">BFAR</span>
    							</a>
    							</c:if>
   										
    							</li>
    							<li class="dropdown-item">
    							<c:if test="${page == true }">		
    								<a  class=" dropdown-item" tabindex="-1" href="../coldChart" >
    							<img src="/static/images/pin2022/IcePlant_PrivateSector.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Private Sector</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">		
    							<a  class=" dropdown-item" tabindex="-1" href="coldChart" >
    							<img src="/static/images/pin2022/IcePlant_PrivateSector.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Private Sector</span>
    							</a>
    							</c:if>
   										
    							</li>
    							<li class="dropdown-item">
    							<c:if test="${page == true }">		
    								<a  class=" dropdown-item" tabindex="-1" href="../coldChart" >
    								<img src="/static/images/pin2022/IcePlant_Cooperative.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Cooperative</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">		
    							<a  class=" dropdown-item" tabindex="-1" href="coldChart" >
    							<img src="/static/images/pin2022/IcePlant_Cooperative.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Cooperative</span>
    							</a>
    							</c:if>
   										
    							</li>
     					</ul>
     				</li>
     				<li class="dropdown-subitem">
    						<a href="#postharvestprocessing" data-toggle="collapse"  aria-expanded="false">
  	 						<span class="text-white">&nbsp;III. Fish Processing Facility</span>
    						</a>
     					<ul class="collapse list-unstyled" id="postharvestprocessing">
     						<li><input id="processing" type="checkbox"><span class="text-white">Select all</span></li>
     						<li class="dropdown-item">
     						<c:if test="${page == true }">		
    							<a  class=" dropdown-item" tabindex="-1" href="../fishprocessingChart" >
    							<img src="/static/images/pin/new/Fish_Processing.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">BFAR</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">		
    							<a  class=" dropdown-item" tabindex="-1" href="fishprocessingChart" >
    							<img src="/static/images/pin/new/Fish_Processing.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">BFAR</span>
    							</a>
    							</c:if>
   										
    							</li>
    							<li class="dropdown-item">
    							<c:if test="${page == true }">		
    							<a  class=" dropdown-item" tabindex="-1" href="../fishprocessingChart" >
    							<img src="/static/images/pin2022/Fish_Processing_LGU.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">LGU</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">		
    							<a  class=" dropdown-item" tabindex="-1" href="fishprocessingChart" >
    							<img src="/static/images/pin2022/Fish_Processing_LGU.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">LGU</span>
    							</a>
    							</c:if>
   										
    							</li>
    							<li class="dropdown-item">
    							<c:if test="${page == true }">		
    							<a  class=" dropdown-item" tabindex="-1" href="../fishprocessingChart" >
    							<img src="/static/images/pin2022/Fish_Processing_PrivateSector.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Private Sector</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">		
    							<a  class=" dropdown-item" tabindex="-1" href="fishprocessingChart" >
    							<img src="/static/images/pin2022/Fish_Processing_PrivateSector.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Private Sector</span>
    							</a>
    							</c:if>
   									
    							</li>
    							<li class="dropdown-item">
    							<c:if test="${page == true }">		
    							<a  class=" dropdown-item" tabindex="-1" href="../fishprocessingChart" >
    							<img src="/static/images/pin2022/Fish_Processing_Cooperative.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Cooperative</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">		
    							<a  class=" dropdown-item" tabindex="-1" href="fishprocessingChart" >
    							<img src="/static/images/pin2022/Fish_Processing_Cooperative.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Cooperative</span>
    							</a>
    							</c:if>
   										
    							</li>
     					</ul>
     				</li>
     				<li class="dropdown-subitem">
    						<a href="#postharvestseaweed" data-toggle="collapse"  aria-expanded="false">
  	 						<span class="text-white">&nbsp;IV. Seaweed</span>
    						</a>
     					<ul class="collapse list-unstyled" id="postharvestseaweed">
     					<!-- 	<li><input id="postseaweed" type="checkbox"><span class="text-white">Select all</span></li> -->
     						<li class="dropdown-item">
     						<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../seaweedsWDChart" >
    							<img src="/static/images/pin2022/Seaweeds(postharvest).png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Seaweed Warehouse</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="seaweedsWDChart" >
    							<img src="/static/images/pin2022/Seaweeds(postharvest).png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Seaweed Warehouse</span>
    							</a>
    							</c:if>
   										
    							</li>
    							<li class="dropdown-item">
    							<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../seaweedsWDChart" >
    							<img src="/static/images/pin2022/seaweeds_dryer.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Seaweed Dryer</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="seaweedsWDChart" >
    							<img src="/static/images/pin2022/seaweeds_dryer.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Seaweed Dryer</span>
    							</a>
    							</c:if>
   										
    							</li>
    							
     					</ul>
     				</li>
     				
     				<li class="dropdown-item">
     					<span class="text-white">V.</span>
   						<!-- <input type="checkbox" id="checked_market" value="market"> -->
   						<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../marketChart" >
    							<img src="/static/images/pin2022/Market.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Market</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="marketChart" >
    							<img src="/static/images/pin2022/Market.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Market</span>
    							</a>
    							</c:if>
    					
    					</li>	
    					<li class="dropdown-item">
    					<span class="text-white">VI.</span>
    					<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../fishportChart" >
    								<img src="/static/images/pin2022/FishPort.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Port</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="fishportChart" >
    								<img src="/static/images/pin2022/FishPort.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Fish Port</span>
    							</a>
    							</c:if>
    						
   						
   					</li>
    			</ul>
    		</li>
    			  	<li>         
    		<a href="#pageOther" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;OTHER RESOURCES</span></a> 
    	<ul class="collapse list-unstyled" id="pageOther"> 
     
     <li class="dropdown-subitem">         
    		<a href="#pageBodiesofWater" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white">&nbsp;I. Bodies of Water</span></a> 
    			<ul class="collapse list-unstyled" id="pageBodiesofWater"> 			
      <li class="dropdown-subitem">         
    		<a href="#pageCreate" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;Fisheries Management Areas</span></a> 
    			<ul class="collapse list-unstyled" id="pageCreate"> 
     			<li><input id="FMA" type="checkbox"><span class="text-white">Select all</span></li>
   					<li class="dropdown-item"> 
   						<!-- <input type="checkbox" id="FMA-1" value="FMA-1" name="FMA" class="FMA"> -->
    						<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">FMA 1</span>
    					</li> 
    				<li class="dropdown-item">
   					<!-- <input type="checkbox" id="FMA-2" value="FMA-2" name="FMA" class="FMA"> -->
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 2</span>
    				</li>

   				<li class="dropdown-item">
   					<!-- <input type="checkbox" id="FMA-3" value="FMA-3" name="FMA" class="FMA"> -->
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 3</span>
    				</li>
    				<li class="dropdown-item">
   					<!-- <input type="checkbox" id="FMA-4" value="FMA-4" name="FMA" class="FMA"> -->
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 4</span>
    				</li>
    				<li class="dropdown-item">
   					<!-- <input type="checkbox" id="FMA-5" value="FMA-5" name="FMA" class="FMA"> -->
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 5</span> 
    				</li> 
    				<li class="dropdown-item">
   					<!-- <input type="checkbox" id="FMA-6" value="FMA-6" name="FMA" class="FMA"> -->
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 6</span>
    				</li>
    				<li class="dropdown-item">
   					<!-- <input type="checkbox" id="FMA-7" value="FMA-7" name="FMA" class="FMA"> -->
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 7</span> 
    				</li>
    				<li class="dropdown-item">
   					<!-- <input type="checkbox" id="FMA-8" value="FMA-8" name="FMA" class="FMA"> -->
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 8</span>
    				</li>
    				<li class="dropdown-item"> 
   					<!-- <input type="checkbox" id="FMA-9" value="FMA-9" name="FMA" class="FMA"> -->
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 9</span>
    				</li>
    				<li class="dropdown-item"> 
   					<!-- <input type="checkbox" id="FMA-10" value="FMA-10" name="FMA" class="FMA"> -->
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 10</span>
    				</li>
    				<li class="dropdown-item"> 
   					<!-- <input type="checkbox" id="FMA-11" value="FMA-11" name="FMA" class="FMA"> -->
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 11</span>
    				</li>
    				<li class="dropdown-item"> 
   					<!-- <input type="checkbox" id="FMA-12" value="FMA-12" name="FMA" class="FMA"> -->
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 12</span> 
	  				</li> 
    	      	
   			</ul>
   		</li>      
   		<li class="dropdown-subitem">
    <a href="#major_fg" data-toggle="collapse"  aria-expanded="false">
  	 <span class="text-white"><i class="fas fa-list"></i>&nbsp;24 Major Fishing Grounds</span>
    </a>
     <ul class="collapse list-unstyled" id="major_fg">
     <li><input id="major24_fg" type="checkbox"><span class="text-white">Select all</span></li>
   	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_lingayen_gulf" value="LINGAYEN GULF" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">1 - LINGAYEN GULF</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_manila_bay" value="Manila Bay" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">2 - Manila Bay</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_batangas_coast" value="Batangas Coast" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">3 - Batangas Coast</span>
    	</li>
    	<li class="dropdown-item">
   	<!-- 	<input type="checkbox" id="checked_tayabas_bay" value="Tayabas Bay" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">4 - Tayabas Bay</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_wpalawan_waters" value="West Palawan waters" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">5 - West Palawan waters</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_cuyo_pass" value="Cuyo Pass" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">6 - Cuyo Pass</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_wsulu_sea" value="West Sulu Sea" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">7 - West Sulu Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_ssulu_sea" value="South Sulu Sea" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">8- South Sulu Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_esulu_sea" value="East Sulu Sea" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">9 - East Sulu Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_moro_gulf" value="Moro Gulf" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">10 - Moro Gulf</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_davao_gulf" value="Davao Gulf" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">11 - Davao Gulf</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_samar_gulf" value="Samar Sea" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">12 - Samar Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_sibuyan_gulf" value="Sibuyan Sea" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">13 - Sibuyan Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_camotes_gulf" value="Camotes Sea" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">14 - Camotes Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_visayan_gulf" value="Visayan Sea" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">15 - Visayan Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_guimaras_gulf" value="Guimaras Strait" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">16 - Guimaras Strait</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_bohol_gulf" value="Bohol Sea" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">17 - Bohol Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_ragay_gulf" value="Ragay Gulf" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">18 - Ragay Gulf</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_leyte_gulf" value="Leyte Gulf" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">19 - Leyte Gulf</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_lagonoy_gulf" value="Lagonoy Gulf" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">20 - Lagonoy Gulf</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_lamon_bay" value="Lamon Bay" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">21 - Lamon Bay</span>
    	</li>
    	<li class="dropdown-item">
   	<!-- 	<input type="checkbox" id="checked_casiguran_sound" value="Casiguran Sound" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">22 - Casiguran Sound</span>
    	</li>
    	<li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_palanan_bay" value="Palanan Bay" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">23 - Palanan Bay</span>
    	</li>
    <li class="dropdown-item">
   		<!-- <input type="checkbox" id="checked_babuyan_channel" value="Babuyan Channel" class="FG"> -->
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">24 - Babuyan Channel</span>
    	</li>
    	</ul>
    	</li>
        <li class="dropdown-item">
   	 		<!-- <input type="checkbox" id="checked_municipal_waters" value="Municipal Water "> -->
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Municipal Waters</span>
	  	</li>
	  <li class="dropdown-item">
   	 		<!-- <input type="checkbox" id="checked_rivers" value="River"> -->
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Rivers</span>
	  </li>
	  <li class="dropdown-item">
   	 		<!-- <input type="checkbox" id="checked_lakes" value="Lakes"> -->
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Lakes</span>
	  </li>
	  <li class="dropdown-item">
   	 	<!-- 	<input type="checkbox" id="checked_bay" value="Bay"> -->
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Bay</span>
	  </li>
	  <li class="dropdown-item">
   	 		<!-- <input type="checkbox" id="checked_strait" value="Strait"> -->
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Strait</span>
	  </li>
	  <li class="dropdown-item">
   	 		<!-- <input type="checkbox" id="checked_channel" value="Channel"> -->
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Channel</span>
	  </li>
	  <li class="dropdown-item">
   	 		<!-- <input type="checkbox" id="checked_gulf" value="Gulf"> -->
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Gulf</span>
	  </li>
	  <li class="dropdown-item">
   	 		<!-- <input type="checkbox" id="checked_sea" value="Sea"> -->
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Sea</span>
	  </li>
	  
	  </ul>	  
	  </li>
	  <li class="dropdown-subitem">         
    		<a href="#pagefishhabitat" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white">&nbsp;II. Fish Habitat</span></a> 
    			<ul class="collapse list-unstyled" id="pagefishhabitat"> 
    				<li><input id="habitat" type="checkbox"><span class="text-white">Select all</span></li>
   					<li class="dropdown-item">
   						<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../seagrassChart" >
    							<img src="/static/images/pin2022/Seagrass.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Sea Grass</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    							<a  class=" dropdown-item" tabindex="-1" href="seagrassChart" >
    							<img src="/static/images/pin2022/Seagrass.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Sea Grass</span>
    							</a>
    							</c:if>
    						
   					</li>
   					<li class="dropdown-item">
   						<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../mangroveChart" >
    								<img src="/static/images/pin2022/Mangrove.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Mangrove</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    								<a  class=" dropdown-item" tabindex="-1" href="mangroveChart" >
    									<img src="/static/images/pin2022/Mangrove.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Mangrove</span>
    								</a>
    							</c:if>
     					
   					</li>
   					<li class="dropdown-item">
   						<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../reefChart" >
    								<img src="/static/images/pin2022/Coral-Reefs.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Coral Reefs</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    								<a  class=" dropdown-item" tabindex="-1" href="reefChart" >
    								<img src="/static/images/pin2022/Coral-Reefs.png" style="width: 20px; height: 20px;"/>
    								<span class="text-white">Coral Reefs</span>
    							</a>
    							</c:if>
   					</li>
    			</ul>
    </li>
    <li class="dropdown-subitem">         
    		<a href="#pageinfrustracture" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white">&nbsp;III. BFAR Infrastructure</span></a> 
    			<ul class="collapse list-unstyled" id="pageinfrustracture"> 
    				<li><input id="infrastructure" type="checkbox"><span class="text-white">Select all</span></li>
   				<li class="dropdown-item">
   					<input type="checkbox" id="check_nationalcenter" value="national_center" name="infrastructure" class="infrastructure">
    					<img src="/static/images/pin2022/BINTCenter.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">National Technology Center</span>
    				</li>
    				<li class="dropdown-item">
   					<input type="checkbox" id="check_regionaloffices" value="regional_offices" name="infrastructure" class="infrastructure">
    					<img src="/static/images/pin2022/BFAR_Infrastructure_RegionalFisheriesOffice.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">Regional Fisheries Office</span>
    				</li>
    				<li class="dropdown-item">
   					<input type="checkbox" id="check_rdtc" value="regional_offices" name="infrastructure" class="infrastructure">
    					<img src="/static/images/pin2022/BIRDTC.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">Regional Development Training Center</span>
    				</li>
    				<li  class="dropdown-item">
    					<input type="checkbox" id="checked_lgu" value="lgu" name="infrastructure" class="infrastructure">
     				<img src="/static/images/pin2022/BFAR_Insfrastructure_ProvincialFisheriesOffice.png" style="width: 20px; height: 20px;"/>
     				<span class="text-white">Provincial Fisheries Office</span>
   				</li>
   				<li class="dropdown-item">
   					<input type="checkbox" id="check_tos" value="technology_outreach_station" name="infrastructure" class="infrastructure">
    					<img src="/static/images/pin2022/BFAR_Insfrastructure_TechnologyOutreachStation.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">Technology Outreach Station</span>
    				</li>
    				<li class="dropdown-item">
   					<input type="checkbox" id="checked_schoolOfFisheries" value="schoolOfFisheries" name="infrastructure" class="infrastructure">
    					<img src="/static/images/pin2022/BFAR_Insfrastructure_FisheriesSchool.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">Fisheries School</span>
	  				</li>
    			</ul>
    </li>
    <li class="dropdown-subitem">         
    		<a href="#pageoma" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white">&nbsp;IV. Other Management Area</span></a> 
    			<ul class="collapse list-unstyled" id="pageoma"> 
    				<li class="dropdown-item">
   	 					<c:if test="${page == true }">	
    							<a  class=" dropdown-item" tabindex="-1" href="../fishSanctuaryChart" >
    								<img src="/static/images/pin2022/FishSanctuary.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Sanctuary"/>
  	 								<span class="text-white">Fish Sanctuary</span>
    							</a>
    							</c:if>
    							<c:if test="${page == false }">	
    								<a  class=" dropdown-item" tabindex="-1" href="fishSanctuaryChart" >
    								<img src="/static/images/pin2022/FishSanctuary.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Sanctuary"/>
  	 								<span class="text-white">Fish Sanctuary</span>
    							</a>
    							</c:if>
   	 		 			
	  				</li>
    			</ul>
    </li>
   
    </ul>
    </li>

      <sec:authorize access="isAuthenticated()">
       <li><a href="/logout"><i class="fas fa-sign-out-alt"></i>LOGOUT</a></li>
      </sec:authorize>

</ul>
</nav>
    <div id="content" class="p-4 p-md-5 pt-5 section-to-print">	   
    	  <h1>AQUA FARM</h1>
    	 
			
    	<div class="table-wrapper-scroll-y my-custom-scrollbar">
			
			
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<!-- <th scope="col">#</th> -->
									 <c:if test="${type == 'region' }">	
									<th scope="col">Region</th>
									</c:if>
									 <c:if test="${type == 'province' }">	
									<th scope="col">Province</th>
									</c:if>
									 <c:if test="${type == 'municipality' }">	
									<th scope="col">Municipality</th>
									</c:if>
									<!-- <th scope="col">Select</th> -->
								</tr>
							</thead>
							<tbody class="municipal">

							</tbody>
						</table>
		</div> 
						<button type="button" onclick="myFunc()" id="submitMunicipality"
							class="submitMunicipality btn btn-primary">SUBMIT</button>

    <hr>
    <div id="chartShow" style="display: none;">
    <div id="colFilter_div"></div>
	<div id="chart_div" style="border: 1px solid #ccc"></div>
   		</div> 
	</div>


</div>

    <script type="text/javascript">
    
    
    var chartShowHide = ${show};
    if(!chartShowHide){
    	$('#chartShow').show();
    }
     var regionID = ${regionID};
 
     <c:if test="${type == 'region' }">	
     		getRegions();
	</c:if>
     <c:if test="${type == 'province' }">	
		getProvince();
	</c:if>
	
	<c:if test="${type == 'municipality' }">	
		getMunicipality();
	</c:if>

	function getRegions(){
		
		var slctRegions=$('.municipal');
	    var option="";
	  slctRegions.empty();
			
			$.get("/create/getRegionList" , function(data, status) {  
				var id=1;
				slctRegions.append(option);
				for( let prop in data ){						 					
				//	option = option + "<option value='"+data[prop].region_id+"'>" +data[prop].region+ "</option>";	
					
						option = option +	
					//	"<tr><th>" + id++  + "</th>" + 
						"<tr>" + 
						"<td><input type='checkbox' name='myParam' value='"+data[prop].region_id+ "-" + data[prop].region+"'/> &nbsp; &nbsp;" + data[prop].region  + "</td>" +
						"</tr>";
						/* "<td><input type='checkbox' name='myParam' value='"+data[prop].region_id+ "-" + data[prop].region+"'/> </td></tr>"; */
				}
				slctRegions.append(option);
				//console.log(slctRegions);
			});

		}
	
function getProvince(){
  var slctProvinces=$('.municipal');
	  var option="";
	  
	  slctProvinces.empty();
	option = "";

$.get("/create/getProvinceList/" + '${regionID}' , function(data, status) {  
		var id=1;
		slctProvinces.append(option);
		for( let prop in data ){						 					
				//option = option + data[prop].municipal  + "<input type='checkbox' name='myParam[]' class='form-control' value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'/> ";	           
			
				option = option +	"<tr><th>" + id++  + "</th>" + 
				"<td>" + data[prop].province  + "</td>" +
				"<td><input type='checkbox' name='myParam' value='"+data[prop].province_id+ "-" + data[prop].province+"'/> </td></tr>";
		}
		slctProvinces.append(option);
	});
}

function getMunicipality(){
  var slctMunicipal=$('.municipal');
	  var option="";
	  
	  slctMunicipal.empty();
	option = "";

$.get("/create/getMunicipalityList/" + '${provinceID}' , function(data, status) {  
		var id=1;
		slctMunicipal.append(option);
		for( let prop in data ){						 					
				//option = option + data[prop].municipal  + "<input type='checkbox' name='myParam[]' class='form-control' value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'/> ";	           
			
				option = option +	"<tr><th>" + id++  + "</th>" + 
				"<td>" + data[prop].municipal  + "</td>" +
				"<td><input type='checkbox' name='myParam' value='"+data[prop].municipal_id+ "-" + data[prop].municipal+"'/> </td></tr>";
		}
		slctMunicipal.append(option);
	});
}
 function myFunc() {
	
	/*  var selected = [];
	    for (var option of document.getElementById('selectedValue').options)
	    {
	        if (option.selected) {
	            selected.push(option.value);
	        }
	    }
	    alert(selected);
 */
	 let arr = [];
	 let checkboxes = document.querySelectorAll("input[type='checkbox']:checked");
	 for (let i = 0; i < checkboxes.length; i++) {
	 arr.push(checkboxes[i].value)
	 }
     <c:if test="${type == 'region' }">	
     window.location.replace("/create/regionList/" + arr.toString());
	</c:if>
     <c:if test="${type == 'province' }">	
     window.location.replace("/create/provinceList/" + arr.toString());
	</c:if>
	<c:if test="${type == 'municipality' }">	
	window.location.replace("/create/municipalList/" + arr.toString());
	</c:if>

	 
	 }


   google.load('visualization', '1', {packages: ['controls']});
    google.setOnLoadCallback(drawChart); 
    
    /* google.charts.load("current", {packages:['controls']});    
	google.charts.setOnLoadCallback(drawChart); */
	/* google.charts.load('current', {
		  callback: drawChart,
		  packages: ['bar', 'corechart', 'line', 'table','controls']
		}); */
    function drawChart () {
			 var data = google.visualization.arrayToDataTable([
				 ['Province', 'FISHPEN','FISHCORRAL','FISHCAGE','FISHPOND',{ role: 'annotation' }],
	    	        <c:forEach items="${provinceChart}" var="entry" varStatus="status">
	        		['${entry.location}',${entry.fishpenCount},${entry.fishcoralCount},${entry.fishcageCount},${entry.fishpondCount},""],
	        		/*  <c:if test="${status.last}">
	        		 ['${entry.province}',${entry.operational},${entry.forOperation},${entry.forCompletion},${entry.forTransfer},${entry.damaged},${entry.traditional},${entry.nonTraditional}]         		
	        		 </c:if> */
	        		
	        		 </c:forEach>
	    	      ]);

        var columnsTable = new google.visualization.DataTable();
        columnsTable.addColumn('number', 'colIndex');
        columnsTable.addColumn('string', 'colLabel');
        var initState= {selectedValues: []};
        // put the columns into this data table (skip column 0)
        
        for (var i = 1; i < data.getNumberOfColumns(); i++) {
            columnsTable.addRow([i, data.getColumnLabel(i)]);
        }
        initState.selectedValues.push(data.getColumnLabel(4));
        
        var chart = new google.visualization.ChartWrapper({
            chartType: 'BarChart',
            containerId: 'chart_div',
            dataTable: data,
            
            options: {
            	title: '${region_title}' + ' - ' + '${region_description}  '  + '  ${provinceName}',
                width: 1000,
                height: 800,
                text: 'value'
            }
        });
     
    
        var columnFilter = new google.visualization.ControlWrapper({
            controlType: 'CategoryFilter',
            containerId: 'colFilter_div',
            dataTable: columnsTable,
            options: {
                filterColumnLabel: 'colLabel',
                ui: {
                    label: 'Columns',
                    allowTyping: false,
                    allowMultiple: false,
                    allowNone: false
                  // selectedValuesLayout: 'none'
                }
            },
            state: initState
        });
       
        function setChartView () {
            var state = columnFilter.getState();
          
            var row;
            var type=0;
            var type1=0;
            var view = {
                columns: [0]
            };
            for (var i = 0; i < state.selectedValues.length; i++) {
                row = columnsTable.getFilteredRows([{column: 1, value: state.selectedValues[i]}])[0];
                view.columns.push(columnsTable.getValue(row, 0));
   
                type1 =columnsTable.getValue(row, 0);
                
                view.columns.push({
    	        	calc: function (dt, row) {
    	        		/* if(state.selectedValues == "FISHPOND"){
    	        			type = 4;
    	        		}else if(state.selectedValues == "FISHCAGE"){
    	        			type = 3;
    	        		}else if(state.selectedValues == "FISHCORRAL"){
    	        			type = 2;
    	        		}else if(state.selectedValues == "FISHPEN"){
    	        			type = 1;
    	        		}else{
    	        			
    	        		} */
    			 	    	  if(dt.getValue(row, type1) == 0){
    			 	    		  return ""; 
    			 	    	  }else{ 
    			 	        	return dt.getValue(row, type1).toString();
    			 	    	  }	
    			 	      },
    			 	      type: "string",
    			 	      role: "annotation"
    	        });
            }
            // sort the indices into their original order
            view.columns.sort(function (a, b) {
                return (a - b);
            });
           
            chart.setView(view);
            chart.draw();
        }
        google.visualization.events.addListener(columnFilter, 'statechange', setChartView);
        
        setChartView();
        columnFilter.draw();
    }
/* <table class="columns">
      <tr>
        <td>
        	<div id='png'></div>
			<div id="chart_div" style="border: 1px solid #ccc"></div>
        </td>
      </tr>
    </table>

			</div>



</div>

    <script type="text/javascript">

		google.charts.load("current", {packages:['corechart']});
	
  	    var provinceSize = '${fn:length(provinces)}';
  	    
 	   google.charts.setOnLoadCallback(draw);

 	      function draw(){
 	    	 var data = google.visualization.arrayToDataTable([
 	            ['Resources',
 	            	<c:forEach items="${provinces}" var="province">	
 	            	'${province.province}',
 	            </c:forEach>
 	            	{ role: 'annotation' } ],
 	            	
 	            ['Fish Pen',
 	            	<c:forEach items="${provinceChart}" var="entry">
 	            	<c:if test = "${entry.resources == 'FPen'}">	            		
 	            			${entry.total},
 	            	</c:if>
 	            	
 			</c:forEach>
 					''
 	            	],
 	            ['Fish Cage',
 	            	
 	            	<c:forEach items="${provinceChart}" var="entry">
 	            	<c:if test = "${entry.resources == 'FCoral'}">
         			${entry.total},
         	</c:if>
					</c:forEach>
 	            	
 	            	'']
 	          
 	          ]);
 	    	 
 	    	var view = new google.visualization.DataView(data);

	    	var columns = [];
	    	for (var i = 0; i <= provinceSize ; i++) {
	    	    if (i > 0) {
	    	        columns.push(i);
	    	        columns.push({
	    	        	calc: function (dt, row) {
	 			 	    	  if(dt.getValue(row, i) == 0){
	 			 	    		  return ""; 
	 			 	    	  }else{ 
	 			 	        	return dt.getValue(row, i).toString();
	 			 	    	  }	
	 			 	      },
	 			 	      type: "string",
	 			 	      role: "annotation"
	    	        });

	    	    } else {
	    	        columns.push(i);
	    	    }
	    	}
	    	
	    	columns.push(
	    		{
		 	      calc: function (dt, row) {
		 	        return 0;
		 	      },
		 	      label: "Total",
		 	      type: "number"		  		 	   	
	        	},
	        );
	    	columns.push(
	    		{	  	    		
	    		 calc: function (dt, row) {
	    			if(provinceSize == 6){
	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5) + dt.getValue(row, 6);
	    			}
	    			if(provinceSize == 5){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4)+ dt.getValue(row, 5);
	  	    		}
	    			if(provinceSize == 4){
	  	    			 return dt.getValue(row, 1) + dt.getValue(row, 2) + dt.getValue(row, 3) + dt.getValue(row, 4);
	  	    		}
	    			},
		 	      type: "number",
		 	      role: "annotation"
	    	
	    	}
	    		);
	    	
	    	view.setColumns(columns);


 	          var options = {
 	            width: 1000,
 	            height: 600,
 	           	title: 'AQUACULTURE(AQUAFARM) - ${region_title}' ,
 	            legend: { position: 'top', maxLines: 5 },
 	            bar: { groupWidth: '75%' },
 	            isStacked: true,
 	           vAxis: {
	  	            gridlines: {color: '#007bff;'},
	  	            minValue: 0,
	  	          title: 'Number of Resources'
	  	          }
 	            
 	          };

 	         var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
 	      
 	         google.visualization.events.addListener(chart, 'ready', function () {
 	          document.getElementById('png').outerHTML = '<a href="' + chart.getImageURI() + '" target="_blank">PRINT AQUACULTURE CHARTS</a>';
 	         });
 	       
  	        chart.draw(view, options);
 	      }
 */

</script>
</body>

</html>