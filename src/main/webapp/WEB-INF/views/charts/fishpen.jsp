  
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!doctype html>
<html lang="en">
<head>
<title>FRGIS - CHART</title>
<meta charset="utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <style type="text/css">
.chart {
	display: block;
	margin: 0 auto;
}

.my-custom-scrollbar {
	position: relative;
	width: 200px;
	height: 200px;
	overflow: auto;
}

.table-wrapper-scroll-y {
	display: block;
}

/* table.table-fit {
	width: auto !important;
	table-layout: auto !important;
}

table.table-fit thead th, table.table-fit tfoot th {
	width: auto !important;
}

table.table-fit tbody td, table.table-fit tfoot td {
	width: auto !important;
} */

</style>


</head>
<body>

<jsp:include page="../home/header_region.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">
 <jsp:include page="../menu/userChartMenu.jsp"></jsp:include>
    <div id="content" class="p-4 p-md-5 pt-5 section-to-print">	   
    	  <h1>FISHPEN</h1>
    	 
			
<%--     	<div class="table-wrapper-scroll-y my-custom-scrollbar">
			
			
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<!-- <th scope="col">#</th> -->
									 <c:if test="${type == 'region' }">	
									<th scope="col">Region</th>
									</c:if>
									 <c:if test="${type == 'province' }">	
									<th scope="col">Province</th>
									</c:if>
									 <c:if test="${type == 'municipality' }">	
									<th scope="col">Municipality</th>
									</c:if>
									<!-- <th scope="col">Select</th> -->
								</tr>
							</thead>
							<tbody class="municipal">

							</tbody>
						</table>
		</div> 
						<button type="button" onclick="myFunc()" id="submitMunicipality"
							class="submitMunicipality btn btn-primary">SUBMIT</button> --%>

    <hr>
    <div id="chartShow" style="display: none;">
    <div id="colFilter_div"></div>
    </div> 
        <div class="form-row">
   		<div class="col-md-6">

   		</div>
   		<div class="col-md-6">
			
   				<input class="btn btn-primary float-right" type="button" style="color: black;" value="DOWNLOAD CHART" onclick="save_filename()" />

   		</div>
   		<br>
	<div id="chart_div" style="border: 1px solid #ccc"></div>
   
	</div>


</div>

    <script type="text/javascript">
    
    
    var chartShowHide = ${show};
    if(!chartShowHide){
    	$('#chartShow').show();
    }
     var regionID = ${regionID};
 
     <c:if test="${type == 'region' }">	
     		getRegions();
	</c:if>
     <c:if test="${type == 'province' }">	
		getProvince();
	</c:if>
	
	<c:if test="${type == 'municipality' }">	
		getMunicipality();
	</c:if>

	function getRegions(){
		
		var slctRegions=$('.municipal');
	    var option="";
	  slctRegions.empty();
			
			$.get("/create/getRegionList" , function(data, status) {  
				var id=1;
				slctRegions.append(option);
				for( let prop in data ){						 					
				//	option = option + "<option value='"+data[prop].region_id+"'>" +data[prop].region+ "</option>";	
					
						option = option +	
					//	"<tr><th>" + id++  + "</th>" + 
						"<tr>" + 
						"<td><input type='checkbox' name='myParam' value='"+data[prop].region_id+ "-" + data[prop].region+"'/> &nbsp; &nbsp;" + data[prop].region  + "</td>" +
						"</tr>";
						/* "<td><input type='checkbox' name='myParam' value='"+data[prop].region_id+ "-" + data[prop].region+"'/> </td></tr>"; */
				}
				slctRegions.append(option);
				//console.log(slctRegions);
			});

		}
	
function getProvince(){
  var slctProvinces=$('.municipal');
	  var option="";
	  
	  slctProvinces.empty();
	option = "";

$.get("/create/getProvinceList/" + '${regionID}' , function(data, status) {  
		var id=1;
		slctProvinces.append(option);
		for( let prop in data ){						 					
				//option = option + data[prop].municipal  + "<input type='checkbox' name='myParam[]' class='form-control' value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'/> ";	           
			
				option = option +	"<tr><th>" + id++  + "</th>" + 
				"<td>" + data[prop].province  + "</td>" +
				"<td><input type='checkbox' name='myParam' value='"+data[prop].province_id+ "-" + data[prop].province+"'/> </td></tr>";
		}
		slctProvinces.append(option);
	});
}

function getMunicipality(){
  var slctMunicipal=$('.municipal');
	  var option="";
	  
	  slctMunicipal.empty();
	option = "";

$.get("/create/getMunicipalityList/" + '${provinceID}' , function(data, status) {  
		var id=1;
		slctMunicipal.append(option);
		for( let prop in data ){						 					
				//option = option + data[prop].municipal  + "<input type='checkbox' name='myParam[]' class='form-control' value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'/> ";	           
			
				option = option +	"<tr><th>" + id++  + "</th>" + 
				"<td>" + data[prop].municipal  + "</td>" +
				"<td><input type='checkbox' name='myParam' value='"+data[prop].municipal_id+ "-" + data[prop].municipal+"'/> </td></tr>";
		}
		slctMunicipal.append(option);
	});
}
 function myFunc() {
	
	/*  var selected = [];
	    for (var option of document.getElementById('selectedValue').options)
	    {
	        if (option.selected) {
	            selected.push(option.value);
	        }
	    }
	    alert(selected);
 */
	 let arr = [];
	 let checkboxes = document.querySelectorAll("input[type='checkbox']:checked");
	 for (let i = 0; i < checkboxes.length; i++) {
	 arr.push(checkboxes[i].value)
	 }
     <c:if test="${type == 'region' }">	
     window.location.replace("/create/regionList/" + arr.toString());
	</c:if>
     <c:if test="${type == 'province' }">	
     window.location.replace("/create/provinceList/" + arr.toString());
	</c:if>
	<c:if test="${type == 'municipality' }">	
	window.location.replace("/create/municipalList/" + arr.toString());
	</c:if>

	 
	 }

 var photos;
   google.load('visualization', '1', {packages: ['controls']});
    google.setOnLoadCallback(drawChart); 

    function drawChart () {
			 var data = google.visualization.arrayToDataTable([
				 ['Province', 'FISHPEN','FISHCORRAL','FISHCAGE','FISHPOND',{ role: 'annotation' }],
	    	        <c:forEach items="${provinceChart}" var="entry" varStatus="status">
	        		['${entry.location}',${entry.fishpenCount},${entry.fishcoralCount},${entry.fishcageCount},${entry.fishpondCount},""],

	        		 </c:forEach>
	    	      ]);

        var columnsTable = new google.visualization.DataTable();
        columnsTable.addColumn('number', 'colIndex');
        columnsTable.addColumn('string', 'colLabel');
        var initState= {selectedValues: []};
        // put the columns into this data table (skip column 0)
        
        for (var i = 1; i < data.getNumberOfColumns(); i++) {
            columnsTable.addRow([i, data.getColumnLabel(i)]);
        }
        initState.selectedValues.push(data.getColumnLabel(1));
        
        var chart = new google.visualization.ChartWrapper({
            chartType: 'BarChart',
            containerId: 'chart_div',
            dataTable: data,
            
            options: {
            	//title: '${region_title}' + ' - ' + '${region_description}  ',
                title:'Number of FishPen per ' + '${region_description}' ,
            	width: 800,
                height: 700,
                legend: { position: 'top', maxLines: 3 },
            }
        });
	    google.visualization.events.addListener(chart, 'ready', function () {
	    	photos =chart.getChart().getImageURI();
	    	 });
    
        var columnFilter = new google.visualization.ControlWrapper({
            controlType: 'CategoryFilter',
            containerId: 'colFilter_div',
            dataTable: columnsTable,
            options: {
                filterColumnLabel: 'colLabel',
                ui: {
                    label: 'Columns',
                    allowTyping: false,
                    allowMultiple: false,
                    allowNone: false
                  // selectedValuesLayout: 'none'
                }
            },
            state: initState
        });
       
        function setChartView () {
            var state = columnFilter.getState();
          
            var row;
            var type=0;
            var type1=0;
            var view = {
                columns: [0]
            };
            for (var i = 0; i < state.selectedValues.length; i++) {
                row = columnsTable.getFilteredRows([{column: 1, value: state.selectedValues[i]}])[0];
                view.columns.push(columnsTable.getValue(row, 0));
   
                type1 =columnsTable.getValue(row, 0);
                
                view.columns.push({
    	        	calc: function (dt, row) {
    	        		/* if(state.selectedValues == "FISHPOND"){
    	        			type = 4;
    	        		}else if(state.selectedValues == "FISHCAGE"){
    	        			type = 3;
    	        		}else if(state.selectedValues == "FISHCORRAL"){
    	        			type = 2;
    	        		}else if(state.selectedValues == "FISHPEN"){
    	        			type = 1;
    	        		}else{
    	        			
    	        		} */
    			 	    	  if(dt.getValue(row, type1) == 0){
    			 	    		  return ""; 
    			 	    	  }else{ 
    			 	        	return dt.getValue(row, type1).toString();
    			 	    	  }	
    			 	      },
    			 	      type: "string",
    			 	      role: "annotation"
    	        });
            }
            // sort the indices into their original order
            view.columns.sort(function (a, b) {
                return (a - b);
            });
           
            chart.setView(view);
            chart.draw();
        }
        google.visualization.events.addListener(columnFilter, 'statechange', setChartView);
        
        setChartView();
        columnFilter.draw();
    }
    function save_filename() {
    	
  	  var image = new Image();
  	  image.src = photos;

  	  image.onload = function() {
  		  var canvas = document.createElement("canvas");
  			
  	    	canvas.width = 800;
  	        canvas.height = 700;
  	        var ctx = canvas.getContext("2d");
  	    ctx.drawImage(image, 0,0,800,700);
  	    
    	  var link = document.createElement("a");
		link.download = 'fishpen.png';
		link.href = canvas.toDataURL("image/png");
	  
		document.body.appendChild(link);
		link.click();

	  document.body.removeChild(link);
		delete link;
  	  };  
  	}

</script>
</body>

</html>