<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<title>BFAR-FRGIS</title>
<meta charset="utf-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png" href="/static/images/favicon.png" />

	<jsp:include page="../menu/css.jsp"></jsp:include>
	<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
	<script src="/static/leaflet/bundle.js"></script>
</head>
<body>
 <jsp:include page="header.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch">

    <div id="content" class="p-4 p-md-5 pt-5">	 
              	<div id="map" class="section-to-print"></div>
    </div>

</div>

	<script  src="/static/js/resourcesOnMapSuperAdmin2.js" ></script>

    </body>
</html>