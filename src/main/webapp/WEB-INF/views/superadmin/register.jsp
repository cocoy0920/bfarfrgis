<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!doctype html>
<html lang="en">
<head>
<title>User Registration</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>

 <style>
      .error {
         color: #ff0000;
      }

      .errorblock {
         color: #000;
         background-color: #ffEEEE;
         border: 3px solid #ff0000;
         padding: 8px;
         margin: 16px;
      }
   </style>
</head>
<body>

<jsp:include page="../home/header_region.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">
<div id="content" class="p-4 p-md-5 pt-5">		
<%-- <form:form method="POST" action="register" modelAttribute="userForm"> --%>
<form class="needs-validation" id="myform" novalidate>
<input type="hidden" name="id" id="id" class="id">
<input type="hidden" name="state" id="state" class="state">
<input type="checkbox" name="enabled" id="enabled" class="enabled" hidden="true">

	<fieldset class="form-group border border-dark rounded p-4">
	<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">REGISTRATION FORM</legend>
	<%--  <form:errors path = "*" cssClass = "errorblock" element = "div" /> --%>
			<div class="form-row">
    			<div class="col-md-6 mb-3">
					<label class="col-form-label-lg" for="firstName">First Name:</label>
    						<input type="text" name="firstName"  id="firstName"  class="firstName form-control" required/>
							<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        						Required
      						</div>
    			
    		</div>
    		<div class="col-md-6 mb-3">
					<label class="col-form-label-lg" for="lastName">Last Name:</label>
    						<input type="text" name="lastName"  id="lastName"  class="lastName form-control" required/>
							<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        						Required
      						</div>			
    		</div>
		</div>
		
	
			<div class="form-row">
    			<div class="col-md-6 mb-3">
					<label class="col-form-label-lg" for="username">User Name:</label>
    						<input type="text" name=username  id="username"  class="username form-control" required/>
							<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        						Required
      						</div>
   			
    		</div>
    		<div class="col-md-6 mb-3">
					<label class="col-form-label-lg" for="password">Password:</label>
    						<input type="password" name="password"  id="password"  class="password form-control" required/>
							<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        						Required
      						</div>			
    		</div>
		</div>
		<div class="form-row">
    			<div class="col-md-12 mb-3">
					<label class="col-form-label-lg" for="email">Email:</label>
    						<input type="text" name=email  id="email"  class="email form-control" required/>
							<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        						Required
      						</div>    			
    		</div>
		</div>
		<div class="form-row">
    			<div class="col-md-4 mb-3">
					<label class="col-form-label-lg" for="userProfiles">User Role:</label>
					<select name="userProfiles"  id="userProfiles" class="form-control userProfiles" required>	
					<%-- <c:forEach items="${roles}" var="role">						
    							<option label = "${role.type}" value="${role.id},${role.type}"/>
    				</c:forEach> --%>			
					</select>															
							<div class="valid-feedback">
        							ok.
      						</div>
      						<div class="invalid-feedback">
        						Required
      						</div> 			
    			</div>
    			
    			<div class="col-md-4 mb-3 regionList">
					<label class="col-form-label-lg" for="region">Region</label>
					<select name="region"  id="region" class="form-control region" required>															
					<%-- <option label = "--- PLS SELECT ---" value=""/>
					<c:forEach items="${regions}" var="region">
    					<option label = "${region.region}" value="${region.region_id},${region.region}"/>
    				</c:forEach> --%>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid province.
      				</div>			
    			</div>
		</div>
		</fieldset>
   		<div class="form-group">
			<div class="col-md-6 mb-3">
				 <button type="button" id="submitUseInfo" class="submitUseInfo btn btn-primary">SUBMIT</button>
			</div>
			<div class="col-md-6 mb-3">
				 <button type="button" id="canceltUseInfo" class="canceltUseInfo btn btn-info">CANCEL</button>
			</div>
		</div>
		
		</form>


 <div class="container"> 
  <div class="row">
  <div class="col-md-12 table-responsive">
          <table id="export-buttons-table" class="table table-hover">
                <thead>
                  <tr>
                  <th>Firstname</th>
                   <th>Lastname</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Region</th>
                    <th>Role</th>
                    <th>View</th>
                     <th>Active</th>
                  </tr>
                </thead>
                <tbody id="emp_body">
               </tbody>
              </table>
              </div>
              </div>
              </div>
</div>
</div>

<script src="/static/js/validationForm/sadminRegistration.js" type=" text/javascript"></script>

</body>
</html>