class MyCounter extends HTMLElement{
	constructor(){
		super();
		this.shadow = this.attachShadow({mode: "open"});
	}
	
	get page(){
		if(this.getAttribute("page") === "fishSanctuary"){
			
			return this.title()+ this.firstDOM() + this.h1Tityle() + this.forms() + this.lastForm();
		}
		
	}
	
	set page(val){
		this.setAttribute('page', val);
	}
	
	static get observedAttributes(){
		return ["page"];
	}
	
	attributeChangedCallback(prop, oldVal, newVal){
		if(prop === "page") this.render();
	}
	
	connectedCallback(){
		this.render();
	}
	render(){
		this.shadow.innerHTML = `
		${this.page}
		`;
	}
	
	fishSanctuary(){
		
		return "FISH SANCTUARY";
	}

	firstDOM(){
				
		let firts = `
	
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png"href="/static/images/favicon.png" />
<jsp:include page="../menu/css.jsp"></jsp:include>
	<script src="/static/js/form/deleteMapList.js" type=" text/javascript"></script>
	<script src="/static/js/home/fisheries.js" type=" text/javascript"></script>
	<script src="/static/leaflet/leaflet.js"></script>
	<script src="/static/leaflet/leaflet.ajax.min.js"></script>
	<script src="//d3js.org/d3.v3.min.js"></script>
	<link href='/static/leaflet/leaflet.fullscreen.css' rel='stylesheet' />
    <script src='/static/leaflet/Leaflet.fullscreen.min.js'></script>
    <script src="/static/leaflet/bundle.js"></script>
    <link rel="stylesheet" href="/static/leaflet/L.Control.SlideMenu.css" />
    <script src="/static/leaflet/L.Control.SlideMenu.js"></script>
    <script  src="/static/leaflet/leaflet-bootstrapmodal.js" type=" text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/exif-js"></script>
<style>
#map{ 
	width:100%;
  	height: 600px;
   background: #009dc4;
}
</style>
</head>
<body onload="getFisheriesData()">

<jsp:include page="../home/header_users.jsp"></jsp:include>
<div class="wrapper d-flex align-items-stretch text-dark">
`;
    return firts;

	}
	title(){
		if(this.getAttribute("page") === "fishSanctuary"){
			return `<title>Fish Sanctuary</title>`;
		}
	}
	
	h1Tityle(){
		if(this.getAttribute("page") === "fishSanctuary"){
			return `<h1 align="center">FISH SANCTUARY</h1>`;
		}
	}
	
	forms(){
		let form1 = `
			<div class="form_input" id="form_input">
		<form class="needs-validation" id="myform" novalidate>
				<input type="hidden" name="id" id="id" class="id">
				<input type="hidden" name="user" id="user" class="user"/>
				<input type="hidden" name="uniqueKey" id="uniqueKey" class="uniqueKey"/>
				<input type="hidden" name="dateEncoded" id="dateEncoded" class="dateEncoded"/>
				<input type="hidden" name="dateEdited" id="dateEdited" class="dateEdited" />
				<input type="hidden" name="encodedBy" id="encodedBy" class="encodedBy"/>
				<input type="hidden" name="editedBy" id="editedBy" class="editedBy" />
				<input type="hidden" name="region" id="region" class="region"/>	
				<input type="hidden" name="page" id="page" class="page"/>	
				
				<br>
		
			<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH CAGE LOCATION</legend>
				<div class="form-row">
    			<div class="col-md-4 mb-3">
					<label  for="validationCustom01">Province</label>
					<select name="province"  id="validationCustom01" class="form-control province" required></select>															
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid province.
      				</div>
					</div>


    			<div class="col-md-4 mb-3">	
				<label  for="validationCustom02">Municipality/City </label> 		
				  <select name="municipality" id="validationCustom02"  class="form-control municipality" required></select>	
				 <div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Municipality/City 
      				</div>
					</div>

    			<div class="col-md-4 mb-3">															
				<label  for="validationCustom03">Barangay</label>
					<select name="barangay"  id="validationCustom03"  class="form-control barangay" required>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Please select a valid Barangay
      				</div>
					</div>
			</div>
			
			<div class="form-row">
    			<div class="col-md-12 mb-3">			
					<label for="code">Code:</label>
						<input type="text" name="code" id="code"  class="code" placeholder="Code" readonly="readonly"/>
				</div>
			</div>															
			</fieldset>`;
			
	let form3 = 	`<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ADDITIONAL INFORMATION</legend>
						<div class="form-row">

    			<div class="col-md-6 mb-3">	
					<label  for="dateAsOf">Data as of</label>
					<input type="date" name="dateAsOf" id="dateAsOf" class="form-control dateAsOf"  required>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>

    			<div class="col-md-6 mb-3">	
				<label  for="validationCustom09">Data Source/s</label>
					<input type="text" name="sourceOfData"  id="validationCustom09"  class="form-control sourceOfData" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>

			</div>
		<div class="form-row">
    		<div class="col-md-12 mb-3">					
				<label  for="validationCustom10">Remarks</label>
					<textarea name="remarks" id="validationCustom10"  class="form-control remarks" required></textarea>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

		</div>
		</fieldset>
		<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">GEOGRAPHICAL LOCATION</legend>
			<div class="form-row">
      				<div class="col-md-2 mb-3">
					<label  for="area"><span>Area</span></label>
					<input type="text" name="area"  id="area"  class="area form-control" required/>	
					<!-- <input type="number"  name="area" min="0" value="0" step="any"  id="area"  class="fishcoralarea form-control" required/> -->																
					<div class="valid-feedback">
        					ok.
      					</div>
      					<div class="invalid-feedback">
        					Required
      					</div>
					</div>
					<div class="col-md-2 mb-3">	
					<label  for="unit"><span>Unit</span></label>
					<select name="unit"  id="unit"  class="unit
					 form-control" required>
													<option value=""></option>
													<option value="Minor">Minor</option>
													<option value="Major">Major</option>
						</select>
					<div class="valid-feedback">
		        				ok.
		      				</div>
		      				<div class="invalid-feedback">
		        				Required
		      				</div>
					</div>
    		<div class="col-md-2 mb-3">	
				<label  for="lat">Latitude</label>			
					<input type="text" name="lat"  readonly="readonly"   id="lat" class="form-control lat" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

    		<div class="col-md-2 mb-3">	
				<label  for="lon">Longitude</label>
				<input type="text" name="lon"  readonly="readonly"  id="lon" class="form-control lon" required/>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

		</div>	
			<div class="form-row" id="image_form">
    			<div class="col-md-12 mb-3">				
			  		<label  for="photos">Captured Image</label>		
					<i class="fas fa-upload"></i><input type="file" id="photos" class="form-control file" accept="image/*"/>
    				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
			</div>
     			</fieldset>
     				<div id="img_preview" style="display: none">
     						<img id="preview" name="preview" class="preview" style="width: 450px; height: 200px"></img>
     				</div>
     						<input type="hidden" name="image_src" id="image_src" class="image_src" value="" /><br />
							<input type="hidden" name="image"  id="image" class="image"/>			
		
						<div class="btn-group" role="group" aria-label="Basic example">`;

			   let formlast =`<button type="button" id="cancel" class="cancel btn btn-secondary">CANCEL</button>
					  	</div>
		</form>
		<div id="pdf" style="display: none">
        		<br><br>
        		<button class="exportToPDF btn btn-info">Generate PDF</button>
  		</div>
</div>
		
		`;
		
		return form1 + this.fieldSetPerForm() + form3 + this.formSubmit() + formlast;
	}
	fieldSetPerForm(){
		if(this.getAttribute("page") === "fishSanctuary"){
			return `
			<fieldset class="form-group border border-dark rounded p-4">
		<legend  class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH SANCTUARY INFORMATION</legend>	
		<div class="form-row">
    			<div class="col-md-6 mb-3">	
			 <label  for="validationCustom04">Name of Sanctuary/Protected Area</label>
			 <input type="text" name="nameOfFishSanctuary" id="validationCustom04"  class="form-control input-group-lg nameOfFishSanctuary" required/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>
					

    			<div class="col-md-6 mb-3">			
				<label  for="validationCustom05">Implementer</label>				
				<select name="bfardenr"  id="validationCustom05"  class="form-control bfardenr" required>
				</select>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>

						</div>	
						
			<div id="bfarData" style="display:none;">
				<div class="form-row">
    				<div class="col-md-6 mb-3">	
						<label  for="sheltered">Name of Sheltered Fisheries/Species:</label>
			 			<input type="text" name="sheltered"  id="sheltered"  class="sheltered form-control form-control-sm"/>				
					</div>
				</div>
			</div>
		
		<div id="denrData" style="display:none;">
		<div class="form-row">
    			<div class="col-md-6 mb-3">	
			<label  for="nameOfSensitiveHabitatMPA">Name of Sensitive Habitat with MPA:</label>	
			<input type="text" name="nameOfSensitiveHabitatMPA"  id="nameOfSensitiveHabitatMPA"  class="nameOfSensitiveHabitatMPA form-control"/>
			</div>
			<div class="col-md-6 mb-3">	
			<label  for="OrdinanceNo">Ordinance No.</label>					
			<input type="text" name="ordinanceNo" id="ordinanceNo" class="ordinanceNo form-control"/>
			</div>
			</div>
			<div class="form-row">
    			<div class="col-md-6 mb-3">	
			<label for="OrdinanceTitle">Ordinance Title</label>
			<input type="text" name="ordinanceTitle" id="ordinanceTitle"  class="ordinanceTitle form-control"/>
			</div>
			<div class="col-md-6 mb-3">	
			<label for="DateEstablished">Date Established</label>
			<input type="date" name="dateEstablished" id="dateEstablished" class="dateEstablished form-control "/>
			</div>			
		</div>
		</div>
		
		
		<div class="form-row">

    			<div class="col-md-6 mb-3">	
					<label  for="validationCustom07">Type</label>	
					<select name="type"  id="validationCustom07"  class="form-control type" required>
										<option value="">Select</option>
										<option value="National">National</option>
										<option value="Local">Local</option>
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
					</div>

			</div>
</fieldset>
			`;
		}

	}
	
	formSubmit(){
		if(this.getAttribute("page") === "fishSanctuary"){
			return `
			<button type="submit" id="submitFishSanctuaries" class="submitFishSanctuaries btn btn-primary">SUBMIT</button>							
			      	  `;
	}
	}
	
	lastForm(){
		return `
		<br><br>
  <div id="table">				
<table id="pdfFISHCAGE" style="display: none;">
  <thead>
    <tr>
      <th scope="col">FISH CAGE </th>
      <th scope="col">&nbsp;</th>
    </tr>
  </thead>
  <tbody id="tbodyID">
  </tbody>
</table>

</div>  


 <div class="container"> 
 <button class="create btn-primary">Create</button>
  <input class="exportToExcel btn-info" type="button" value="Export to XLS" onclick="exportToExcel('exTable','FishCage')" />
 
 <div class="row">
       <div class="col-md-12 table-responsive">
	<table id="export-buttons-table" class="table table-hover">
                 <thead>
                  <tr>
                    <th>Region</th>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Operator</th>
                    <th>Coordinates</th>
                  </tr>
                </thead>
                <tbody> 
               </tbody>
              </table>
              <div id="paging">
            
             </div>
 
          <table class="table2excel table table-hover" id="exTable" style="display: none">
                <thead>
                <tr><th align="center" colspan="5">FISH CAGE</th></tr>
                
                <tr>

                
                   
                  </tr>
                  <tr>
                    <th>Province</th>
                    <th>Municipality</th>
                    <th>Barangay</th>
                    <th>Name of Operator</th>
                    <th>Code</th>
                    <th>Area</th>
                    <th>Classification of Operator</th>
                    <th>Cage Dimension</th>
                  	<th>Cage Design</th>
                    <th>Cage Type</th>
                    <th>Cage no of Compartments</th>
                    <th>Indicate species</th>
                    <th>Source of data</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                <tbody id="emp_body">
               </tbody>
              </table>       
       </div>
 </div>
  </div>
 </div>
 <jsp:include page="../menu/fma_details.jsp"></jsp:include>
 
  </div>
  	<div id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
	
<script src="/static/js/imageRPM.js" type=" text/javascript"></script>	
<script src="/static/js/form/provinceMunBarangay.js"></script>	
<script src="/static/js/form/jspdf.debug.js" type=" text/javascript"></script>
<script src="/static/js/form/jspdf.plugin.autotable.js" type=" text/javascript"></script>
<script src="/static/js/excel/jquery.table2excel.js"></script>
<script src="/static/js/excel/exportToExcel.js" defer></script>   
<script  src="/static/js/resource.js" ></script>
<script  src="/static/js/api/regionMap.js" ></script>
<script src="/static/js/map/philippineMap.js"></script>
<script  src="/static/js/resourcesOnMapByResources.js" ></script>
  
	</body>

</html>
		
		`;
	}
}
customElements.define('my-counter',MyCounter);