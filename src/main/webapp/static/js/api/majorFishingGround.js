let albay_gulf;
let asid_gulf;
let babuyan_channel;
let bohol_sea;
let burias_pass2;
let butuan_bay;
let camotes_sea;
let cebu_strait;
let channels;
let davao_gulf;
let east_sulu_sea;
let gulfs;
let iligan_bay;
let illana_bay;
let iloilo_strait;
let imuruan_bay;
let jintotolo_channel;
let lagonoy_gulf;
let lamon_bay;
let leyte_gulf;
let lingayen_gulf;
let manila_bay;
let maqueda_channel;
let mindoro_strait;
let moro_gulf;
let panay_gulf;
let ragay_gulf;
let samar_sea;
let san_miguel_bay;
let sibugay_bay;
let sibuyan_sea;
let south_sulu_sea;
let tablas_strait;
let tanon_strait;
let tawitawi_bay;
let tayabas_bay;
let ticao_pass;
let visayan_sea;
let west_sulu_sea;




//LEGEND
let add_legend;
let remove_legend;
var bays = [];
let bayData="";
///BAY

const tayabas_data =   
	 '<p><i style="background:#003441"></i><h5>TAYABAS BAY</h5>'+ 
		'AREA(Sq.Km.)2,213.00</p>';
const  iligan_data =   
	 '<p><i style="background:#003441"></i><h5>ILIGAN BAY</h5>'+	    	 
		'AREA(Sq.Km.)2,213.00</p>';

let tayabasBollean=false;
let iliganBollean=false;
function getTayabasBay(){
	
	fetch("/static/majorFishingGround/Tayabas_Bay.geojson").then(res => res.json()).then(data => {
		tayabas_bay = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
		       						layer.bindPopup("Tayabas Bay", {closeButton: false});
		       			          })(layer, feature.properties); 
			       					
			  	       		    },
			  	       			 			     	   	       				
			       			});
		tayabas_bay.addTo(map);
			       		
			       		  });
	

	}
function getIliganBay(){
	
	fetch("/static/majorFishingGround/Iligan_Bay.geojson").then(res => res.json()).then(data => {
		iligan_bay = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Iligan Bay", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		iligan_bay.addTo(map);
			       		
			       		  });
	}
function getIllanaBay(){
	
	fetch("/static/majorFishingGround/Illana_Bay.geojson").then(res => res.json()).then(data => {
		illana_bay = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Illana Bay", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		illana_bay.addTo(map);
			       		
			       		  });
	}
function getImuruanBay(){
	
	fetch("/static/majorFishingGround/Imuruan_Bay.geojson").then(res => res.json()).then(data => {
		imuruan_bay = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Imuruan Bay", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		imuruan_bay.addTo(map);
			       		
			       		  });
	}
function getLamonBay(){
	
	fetch("/static/majorFishingGround/Lamon_Bay.geojson").then(res => res.json()).then(data => {
		lamon_bay = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Lamon Bay", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lamon_bay.addTo(map);
			       		
			       		  });
	}
function getManilaBay(){
	
	fetch("/static/majorFishingGround/Manila_Bay.geojson").then(res => res.json()).then(data => {
		manila_bay = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Manila Bay", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		manila_bay.addTo(map);
			       		
			       		  });
	}
function getSanMiguelBay(){
	
	fetch("/static/majorFishingGround/San_Miguel_Bay.geojson").then(res => res.json()).then(data => {
		san_miguel_bay = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("San Miguel Bay", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		san_miguel_bay.addTo(map);
			       		
			       		  });
	}

function getSibugayBay(){
	
	fetch("/static/majorFishingGround/Sibugay_Bay.geojson").then(res => res.json()).then(data => {
		sibugay_bay = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Sibugay Bay", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		sibugay_bay.addTo(map);
			       		
			       		  });
	}

function getTawiTawiBay(){
	
	fetch("/static/majorFishingGround/TawiTawi_Bay.geojson").then(res => res.json()).then(data => {
		tawitawi_bay = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Tawi-Tawi Bay", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		tawitawi_bay.addTo(map);
			       		
			       		  });
	}
////STRAIT

function getCebuStrait(){
	
	fetch("/static/majorFishingGround/Cebu_Strait.geojson").then(res => res.json()).then(data => {
		cebu_strait = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Cebu Strait", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		cebu_strait.addTo(map);
			       		
			       		  });
	}

function getIloIloStrait(){
	
	fetch("/static/majorFishingGround/Iloilo_Strait.geojson").then(res => res.json()).then(data => {
		iloilo_strait = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Iloilo Strait", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		iloilo_strait.addTo(map);
			       		
			       		  });
	}

function getMindoroStrait(){
	
	fetch("/static/majorFishingGround/Mindoro_Strait.geojson").then(res => res.json()).then(data => {
		mindoro_strait = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Mindoro Strait", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		mindoro_strait.addTo(map);
			       		
			       		  });
	}

function getTablasStrait(){
	
	fetch("/static/majorFishingGround/Tablas_Strait.geojson").then(res => res.json()).then(data => {
		tablas_strait = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Tablas Strait", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		tablas_strait.addTo(map);
			       		
			       		  });
	}
function getTanonStrait(){
	
	fetch("/static/majorFishingGround/Tanon_Strait.geojson").then(res => res.json()).then(data => {
		tanon_strait = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Tanon Strait", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		tanon_strait.addTo(map);
			       		
			       		  });
	}

////CHANNEL

function getBabuyanChannel(){
	
	fetch("/static/majorFishingGround/Babuyan_Channel.geojson").then(res => res.json()).then(data => {
		babuyan_channel = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Babuyan Channel", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		babuyan_channel.addTo(map);
			       		
			       		  });
	}

function getJintotoloChannel(){
	
	fetch("/static/majorFishingGround/Jintotolo_Channel.geojson").then(res => res.json()).then(data => {
		jintotolo_channel = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Jintotolo Channel", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		jintotolo_channel.addTo(map);
			       		
			       		  });
	}
function getMaquedaChannel(){
	
	fetch("/static/majorFishingGround/Maqueda_Channel.geojson").then(res => res.json()).then(data => {
		maqueda_channel = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Maqueda Channel", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		maqueda_channel.addTo(map);
			       		
			       		  });
	}

//GULF
function getAlbayGulf(){
	
	fetch("/static/majorFishingGround/Albay_Gulf.geojson").then(res => res.json()).then(data => {
		albay_gulf = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Albay Gulf", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		albay_gulf.addTo(map);
			       		
			       		  });
	}
function getAsidGulf(){
	
	fetch("/static/majorFishingGround/Asid_Gulf.geojson").then(res => res.json()).then(data => {
		asid_gulf = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Asid Gulf", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		asid_gulf.addTo(map);
			       		
			       		  });
	}

function getDavaoGulf(){
	
	fetch("/static/majorFishingGround/Davao_Gulf.geojson").then(res => res.json()).then(data => {
		davao_gulf = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Davao Gulf", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		davao_gulf.addTo(map);
			       		
			       		  });
	}

function getLagonoyGulf(){
	
	fetch("/static/majorFishingGround/Lagonoy_Gulf.geojson").then(res => res.json()).then(data => {
		lagonoy_gulf = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Lagonoy Gulf", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lagonoy_gulf.addTo(map);
			       		
			       		  });
	}
function getLeyteGulf(){
	
	fetch("/static/majorFishingGround/Leyte_Gulf.geojson").then(res => res.json()).then(data => {
		leyte_gulf = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Leyte Gulf", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		leyte_gulf.addTo(map);
			       		
			       		  });
	}
function getLingayenGulf(){
	
	fetch("/static/majorFishingGround/Lingayen_Gulf.geojson").then(res => res.json()).then(data => {
		lingayen_gulf = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Lingayen Gulf", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lingayen_gulf.addTo(map);
			       		
			       		  });
	}

function getMoroGulf(){
	
	fetch("/static/majorFishingGround/Moro_Gulf.geojson").then(res => res.json()).then(data => {
		moro_gulf = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Moro Gulf", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		moro_gulf.addTo(map);
			       		
			       		  });
	}

function getPanay_Gulf(){
	
	fetch("/static/majorFishingGround/Panay_Gulf.geojson").then(res => res.json()).then(data => {
		panay_gulf = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Panay Gulf", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		panay_gulf.addTo(map);
			       		
			       		  });
	}
function getRagayGulf(){
	
	fetch("/static/majorFishingGround/Ragay_Gulf.geojson").then(res => res.json()).then(data => {
		ragay_gulf = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Ragay_Gulf", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		ragay_gulf.addTo(map);
			       		
			       		  });
	}
///SEA
function getBoholSea(){
	
	fetch("/static/majorFishingGround/Bohol_Sea.geojson").then(res => res.json()).then(data => {
		bohol_sea = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Bohol Sea", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		bohol_sea.addTo(map);
			       		
			       		  });
	}
function getCamotesSea(){
	
	fetch("/static/majorFishingGround/Camotes_Sea.geojson").then(res => res.json()).then(data => {
		camotes_sea = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Camotes Sea", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		camotes_sea.addTo(map);
			       		
			       		  });
	}
function getEastSuluSea(){
	
	fetch("/static/majorFishingGround/East_Sulu_Sea.geojson").then(res => res.json()).then(data => {
		east_sulu_sea = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("East Sulu Sea", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		east_sulu_sea.addTo(map);
			       		
			       		  });
	}
function getSamarSea(){
	
	fetch("/static/majorFishingGround/Samar_Sea.geojson").then(res => res.json()).then(data => {
		samar_sea = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Samar Sea", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		samar_sea.addTo(map);
			       		
			       		  });
	}

function getSibuyanSea(){
	
	fetch("/static/majorFishingGround/Sibuyan_Sea.geojson").then(res => res.json()).then(data => {
		sibuyan_sea = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Sibuyan Sea", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		sibuyan_sea.addTo(map);
			       		
			       		  });
	}

function getSouthSuluSea(){
	
	fetch("/static/majorFishingGround/South_Sulu_Sea.geojson").then(res => res.json()).then(data => {
		south_sulu_sea = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("South Sulu Sea", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		south_sulu_sea.addTo(map);
			       		
			       		  });
	}
function getVisayanSea(){
	
	fetch("/static/majorFishingGround/Visayan_Sea.geojson").then(res => res.json()).then(data => {
		visayan_sea = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Visayan Sea", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		visayan_sea.addTo(map);
			       		
			       		  });
	}
function getWestSuluSea(){
	
	fetch("/static/majorFishingGround/West_Sulu_Sea.geojson").then(res => res.json()).then(data => {
		west_sulu_sea = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("West Sulu Sea", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		west_sulu_sea.addTo(map);
			       		
			       		  });
	}
function getBuriasPass(){
	
	fetch("/static/majorFishingGround/Burias_Pass2.geojson").then(res => res.json()).then(data => {
		burias_pass2 = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Burias Pass", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		burias_pass2.addTo(map);
			       		
			       		  });
	}
function getTicaoPass(){
	
	fetch("/static/majorFishingGround/Ticao_Pass2.geojson").then(res => res.json()).then(data => {
		ticao_pass = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Ticao Pass", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		ticao_pass.addTo(map);
			       		
			       		  });
	}

let lake_bato_albay;
let lake_bato_camsur;
let lake_buhi_camsur;
let lake_buluan_maguindanao;
let lake_buluan_sultan_kudarat;
let lake_danao_cebu;
let lake_dapao_lanaodelsur;
let lake_kalibato;
let lake_laguna;
let lake_lakewood;
let lake_lanao;
let lake_mainit_surigao;
let lake_mainit_agusan;
let lake_naujan;
let lake_palakpakin;
let lake_pandin_and_yambao;
let lake_paoay;
let lake_sampalok;
let lake_taal;

function getBatoLakeAlbay(){
	
	fetch("/static/lakes/Bato_Lake_Albay.geojson").then(res => res.json()).then(data => {
		lake_bato_albay = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Albay Bato Lake", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_bato_albay.addTo(map);
		map.fitBounds(lake_bato_albay.getBounds());	       		
			       		  });
	}
function getBatoLakeCamarinesSur(){
	
	fetch("/static/lakes/Bato_Lake_Camarines_Sur.geojson").then(res => res.json()).then(data => {
		lake_bato_camsur = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Bato Lake Camarines Sur", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_bato_camsur.addTo(map);
		map.fitBounds(lake_bato_camsur.getBounds());	  	       		
			       		  });
	}

function getBuhiLakeCamarinesSur(){
	
	fetch("/static/lakes/Buhi_Lake_Camarines_Sur.geojson").then(res => res.json()).then(data => {
		lake_buhi_camsur = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Buhi Lake Camarines Sur", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_buhi_camsur.addTo(map);
		map.fitBounds(lake_buhi_camsur.getBounds());		       		
			       		  });
	}
function getBuluanLakeMaguindanao(){
	
	fetch("/static/lakes/Buluan_Lake_Maguindanao.geojson").then(res => res.json()).then(data => {
		lake_buluan_maguindanao = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Buluan Lake Maguindanao", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_buluan_maguindanao.addTo(map);
		map.fitBounds(lake_buluan_maguindanao.getBounds());	       		
			       		  });
	}
function getBuluanLakeSultanKudarat(){
	
	fetch("/static/lakes/Buluan_Lake_Sultan_Kudarat.geojson").then(res => res.json()).then(data => {
		lake_buluan_sultan_kudarat = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Buluan Lake Sultan Kudarat", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_buluan_sultan_kudarat.addTo(map);
		map.fitBounds(lake_buluan_sultan_kudarat.getBounds());	  	       		
			       		  });
	}
function getDanaoLakeCebu(){
	
	fetch("/static/lakes/Danao_Lake_Cebu.geojson").then(res => res.json()).then(data => {
		lake_danao_cebu = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Danao Lake Cebu", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_danao_cebu.addTo(map);
		map.fitBounds(lake_danao_cebu.getBounds());		       		
			       		  });
	}
function getDapaoLakeLanaodelSur(){
	
	fetch("/static/lakes/Dapao_Lake_Lanao_del_Sur.geojson").then(res => res.json()).then(data => {
		lake_dapao_lanaodelsur = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Dapao Lake Lanao del Sur", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_dapao_lanaodelsur.addTo(map);
		map.fitBounds(lake_dapao_lanaodelsur.getBounds());		       		
			       		  });
	}
function getKalibatoLakeLaguna(){
	
	fetch("/static/lakes/Kalibato_Lake_Laguna.geojson").then(res => res.json()).then(data => {
		lake_kalibato = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Kalibato Lake Laguna", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_kalibato.addTo(map);
		map.fitBounds(lake_kalibato.getBounds());		       		
			       		  });
	}
function getLagunaLakeLaguna(){
	
	fetch("/static/lakes/Laguna_Lake_Laguna.geojson").then(res => res.json()).then(data => {
		lake_laguna = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Laguna Lake Laguna", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_laguna.addTo(map);
		map.fitBounds(lake_laguna.getBounds());	       		
			       		  });
	}
function getLakewoodLakeZamboangadelSur(){
	
	fetch("/static/lakes/Lakewood_Lake_Zamboanga_del_Sur.geojson").then(res => res.json()).then(data => {
		lake_lakewood = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Lakewood Lake Zamboanga del Sur", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_lakewood.addTo(map);
		map.fitBounds(lake_lakewood.getBounds());		       		
			       		  });
	}
function getLanaoLakeLanaodelSur(){
	
	fetch("/static/lakes/Lanao_Lake_Lanao_del_Sur.geojson").then(res => res.json()).then(data => {
		lake_lanao = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Lanao Lake Lanao del Sur", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_lanao.addTo(map);
		map.fitBounds(lake_lanao.getBounds());		       		
			       		  });
	}
function getMainitLakeAgusandelNorte(){
	
	fetch("/static/lakes/Mainit_Lake_Agusan_del_Norte.geojson").then(res => res.json()).then(data => {
		lake_mainit_agusan = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Mainit Lake Agusan del Norte", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_mainit_agusan.addTo(map);
		map.fitBounds(lake_mainit_agusan.getBounds());		       		
			       		  });
	}
function getMainitLakeSurigaodelNorte(){
	
	fetch("/static/lakes/Mainit_Lake_Surigao_del_Norte.geojson").then(res => res.json()).then(data => {
		lake_mainit_surigao = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Mainit Lake Surigao del Norte", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_mainit_surigao.addTo(map);
		map.fitBounds(lake_mainit_surigao.getBounds());		       		
			       		  });
	}
function getNaujanLakeOrientalMindoro(){
	
	fetch("/static/lakes/Naujan_Lake_Oriental_Mindoro.geojson").then(res => res.json()).then(data => {
		lake_naujan = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Naujan Lake Oriental Mindoro", {closeButton: false});
				       			          })(layer, feature.properties);
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_naujan.addTo(map);
		map.fitBounds(lake_naujan.getBounds());	       		
			       		  });
	}
function getPalakpakinLakeLaguna(){
	
	fetch("/static/lakes/Palakpakin_Lake_Laguna.geojson").then(res => res.json()).then(data => {
		lake_palakpakin = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Palakpakin Lake Laguna", {closeButton: false});
				       			          })(layer, feature.properties)
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_palakpakin.addTo(map);
		map.fitBounds(lake_palakpakin.getBounds());	 	       		
			       		  });
	}

function getPandinAndYamboLakeLaguna(){
	
	fetch("/static/lakes/Pandin_And_Yambo_Lake_Laguna.geojson").then(res => res.json()).then(data => {
		lake_pandin_and_yambao = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Pandin And Yambo Lake Laguna", {closeButton: false});
				       			          })(layer, feature.properties)
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_pandin_and_yambao.addTo(map);
		map.fitBounds(lake_pandin_and_yambao.getBounds());	 	       		
			       		  });
	}

function getPaoayLakeIlocosNorte(){
	
	fetch("/static/lakes/Paoay_Lake_Ilocos_Norte.geojson").then(res => res.json()).then(data => {
		lake_paoay = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Paoay Lake Ilocos Norte", {closeButton: false});
				       			          })(layer, feature.properties)
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_paoay.addTo(map);
		map.fitBounds(lake_paoay.getBounds());	       		
			       		  });
	}
function getSampalocLakeLaguna(){
	
	fetch("/static/lakes/Sampaloc_Lake_Laguna.geojson").then(res => res.json()).then(data => {
		lake_taal = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Sampaloc Lake Laguna", {closeButton: false});
				       			          })(layer, feature.properties)
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_taal.addTo(map);
		map.fitBounds(lake_taal.getBounds());		       		
			       		  });
	}
function getTaalLakeBatangas(){
	
	fetch("/static/lakes/Taal_Lake_Batangas.geojson").then(res => res.json()).then(data => {
		lake_sampalok = L.geoJson(data,{
			       				
			       				 onEachFeature: function (feature, layer) {
			       					 
			       					 //layer.setStyle(getFMAStyle);
			       					layer.setStyle(setStyleColor('#FA2E03'));
			       					 (function () {
				       						layer.bindPopup("Taal Lake Batangas", {closeButton: false});
				       			          })(layer, feature.properties)
				  	       		    },
			  	       			 			     	   	       				
			       			});
		lake_sampalok.addTo(map);
		map.fitBounds(lake_sampalok.getBounds());		       		
			       		  });
	}

function getBayName(data,removeData){

	bayData="";
console.log("removeData : " + removeData);
	if(!removeData){
		
		  data.forEach(function (obj) {
			 
			  if( obj.name === "bay_tayabas"){				 
				  tayabasBollean = true;
			  }
			  if( obj.name === "bay_iligan"){				
				 iliganBollean = true;
			  }
		  });

	}else{

		  data.forEach(function (obj) {
			 
			  if( obj.name === "bay_tayabas"){				 
				  tayabasBollean = false;
			  }
			  if( obj.name === "bay_iligan"){				
				 iliganBollean = false;
			  }
		  });
	}
	
	if(tayabasBollean){		
		console.log(tayabasBollean);
		bayData += tayabas_data;
	}
  	if(iliganBollean){
		console.log(iliganBollean);
		bayData += iligan_data;
	} 
		
	add_legend = L.control({position: 'bottomright'});	  
	add_legend.onAdd = function (map) {
	var div = L.DomUtil.create('div', 'info legend')

	div.innerHTML = bayData;
			     
	return div;
	  
  } 
add_legend.addTo(map);			
 
//	    	 
//	    	 
//	    	 
//	    	 
//	     '<i style="background:#003441"></i><h5>ILAGAN BAY</h5><br>' +
//	     '<i style="background:#003441"></i><h5>ILLANA BAY</h5><br>' +
//	     '<i style="background:#003441"></i><h5>IMURAN BAY</h5><br>' +
//	     '<i style="background:#003441"></i><h5>LAMON BAY</h5><br>' +
//	     '<i style="background:#003441"></i><h5>MANILA BAY</h5><br>' +
//	     '<i style="background:#003441"></i><h5>SAN MIGUEL BAY</h5><br>' +
//	     '<i style="background:#003441"></i><h5>SIBUGAY BAY</h5><br>' +
//	     '<i style="background:#003441"></i><h5>TAWI-TAWI BAY</h5><br>' ;
//	     

}
var bays = [];
function removeBayName(data){
	console.log("removeBayName: " + data);
	let removeData =true;
 let bayData="";
 
 bays.push({
		name:data
	});
 getBayName(bays,removeData);
	//  data.forEach(function (obj) {
		
		  if( data === "bay_tayabas"){
			 tayabasBollean = false;
		  }
		  if( data === "bay_iligan"){
			  	 iliganBollean = false;
		  }
	 // });


}
function getBay(data){
	data.forEach(function (obj) {
		console.log("getBay: " + obj.name);
	});
}
//LAKES
