function getAllFishSanctuaryPage(){

	$.get("/create/fishsanctuaries/" + "0", function(content, status) {

		var markup = "";
		for (var x = 0; x < content.fishSanctuaries.length; x++) {
			number_row = x +1;
			markup += "<tr>" +
        
						"<td><span>" + content.fishSanctuaries[x].region + "</span>" +"</td>" +
//						"<span class='spnTooltip'>" +
//						"<strong>CLICK FOR VIEW/EDIT</strong><br/></span>" +"</td>" +
						"<td>" + content.fishSanctuaries[x].province + "</td>" +
						"<td>" + content.fishSanctuaries[x].municipality + "</td>" +
						"<td>" + content.fishSanctuaries[x].barangay + "</td>" +
						"<td>" + content.fishSanctuaries[x].nameOfFishSanctuary + "</td>" +
						"<td>" + content.fishSanctuaries[x].lat + "," + content.fishSanctuaries[x].lon + "</td>" +
						"<td class='map menu_links btn-primary' data-href="+ content.fishSanctuaries[x].uniqueKey + ">MAP</td>" +
						"<td class='view menu_links btn-primary' data-href="+ content.fishSanctuaries[x].uniqueKey + ">View</td>" +
						"</tr>";
		}
		$("#export-buttons-table").find('tbody').empty();
		$("#export-buttons-table").find('tbody').append(markup);
		$("#paging").empty();
		$("#paging").append(content.pageNumber);
	});

}


