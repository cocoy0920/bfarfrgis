
var region_id = "all";
//for generating PDF file
$(function() {
	$(".exportToPDF").click(function(e){

//	var element = document.getElementById("myprogressBar");
//	var width = 1;
//	var identity = setInterval(scene, 10);
//	function scene() {
//		if (width >= 100) {
//			clearInterval(identity);
//		} else {
//			width++;
//			//element.style.width = width + '%'; 
//			//element.innerHTML = width * 1 + '%'; 
//		}
//	}

	var doc = new jsPDF('p', 'pt');

	var elem = document.getElementById('pdfFISHSANCTUARY');
	var imgElements = document.querySelectorAll('#pdfFISHSANCTUARY tbody img');
	
	var data = doc.autoTableHtmlToJson(elem);
	var images = [];
	var i = 0;
	var img = imgElements[i].src;
	
	doc.autoTable(data.columns, data.rows, {
		// bodyStyles: {rowHeight: 30},
		drawCell : function(cell, opts) {
			if (opts.column.dataKey === 1) {
				images.push({
					url : img,
					url : img,
					x : cell.textPos.x,
					y : cell.textPos.y
				});
				
				i++;
			}
		},
		addPageContent : function() {
			
			for (var i = 0; i < images.length; i++) {
				if (i == images.length - 1) {
					doc.addImage(images[i].url, 100, images[i].y, 200, 200);
				}
			}
			
			
		}
	});

	doc.save("fishSanctuaries.pdf");
	window.location.reload();
	});
});

getAllFishSanctuaryPage(region_id);
getAllFishCageFirstPage(region_id);
getFishCoralPerPage(region_id);
getPFoDefaultPage();
getTrainingDefaultPage();
getMaricultureDefaultPage();
getMangroveDefaultPage();
getSeaweedsDefaultPage();
getSeagrassDefaultPage();
getSchoolDefaultPage();
getMarketDefaultPage();
getIPCSDefaultPage();
getHacheryDefaultPerPage();
getFishPenDefaultPage();
getFishPortDefaultPage();
getFishlandingDefaultPage();
getFishProcessingPlantDefaultPage();

getPerPage();
getFishCagePerPage();
geFishCoralPage();
getPFOPerPage();
getTrainingCenterPerPage();
getMariculturePerPage();
getMangrovePerPage();
getSeaweedsPerPage();
getSeagrassPerPage();
getSchoolPerPage();
getMarketPerPage();
getICPSPerPage();
getHatcheryPerPage();
getFishPenPerPage();
getFishPortPerPage();
getFishLandingPerPage();
getFishProcessingPerPage();


getFishSanctuaryList(region_id);
getFishCageList(region_id);
getFishCoralList(region_id);
getPFOList();
getTrainingList();
getAllMaricultureList();
getMangroveList();
getSeaweedsList();
getSeaGrassList();
getSchoolList();
getMarketList();
getIPCSList();
getHatcheryList();
getFishPenList();
getFishPortList();
getFishLandingList();
getFishProcessingPlantList();




$('#Regions').change(function(){

	
  var ddlOption = $("#Regions option:selected").text();
    var ddlValue = $("#Regions option:selected").val();
    
    console.log("Regions: " + ddlValue);
  //  alert('Filtered by Region' + ddlValue);
    
    region_id = ddlValue;
    
    if(region_id == "all"){
    	getPerPage();
    	getFishCagePerPage();
    	geFishCoralPage();
    	getPFOPerPage();
    	getTrainingCenterPerPage();
    	getMariculturePerPage();
    	getMangrovePerPage();
    	getSeaweedsPerPage();
    	getSeagrassPerPage();
    	getSchoolPerPage();
    	getMarketPerPage();
    	getICPSPerPage();
    	getHatcheryPerPage();
    	getFishPenPerPage();
    	getFishPortPerPage();
    	getFishLandingPerPage();
    	getFishProcessingPerPage();
    	
    	$('#emp_body').empty();
    	$('#thead_body').empty();
    	$('#fishcage_body').empty();
    	$('#training_body').empty();
    	$('#seaweeds_body').empty();
    	$('#seagrass_body').empty();
    	$('#school_body').empty();
    	$('#pfo_body').empty();	
    	$('#market_body').empty();	
    	$('#mariculture_body').empty();
    	$('#mangrove_body').empty();
    	$('#cold_body').empty();
    	$('#hatchery_body').empty();
    	$('#processing_body').empty();
    	$('#fishport_body').empty();
    	$('#fishpen_body').empty();
    	$('#fishlanding_body').empty();
    	$('#fishcoral_body').empty();
    	
    	getFishSanctuaryList(region_id);
    	getFishCageList(region_id);
    	getFishCoralList(region_id);
    	getPFOList();
    	getTrainingList();
    	getAllMaricultureList();
    	getMangroveList();
    	getSeaweedsList();
    	getSeaGrassList();
    	getSchoolList();
    	getMarketList();
    	getIPCSList();
    	getHatcheryList();
    	getFishPenList();
    	getFishPortList();
    	getFishLandingList();
    	getFishProcessingPlantList();
    	 var slctProvinces=$('.provincemap').find('option').remove().end();
	   	  var option="";
	   	  
	   	  slctProvinces.empty();
	       option = "<option value=''>-- PLS SELECT--</option>";
	       slctProvinces.append(option);
	       
    	
    }else{	
//    	getPerPage();
//    	getFishCagePerPage();
//    	geFishCoralPage();
//    	getPFOPerPage();
//    	getTrainingCenterPerPage();
//    	getMariculturePerPage();
//    	getMangrovePerPage();
//    	getSeaweedsPerPage();
//    	getSeagrassPerPage();
//    	getSchoolPerPage();
//    	getMarketPerPage();
//    	getICPSPerPage();
//    	getHatcheryPerPage();
//    	getFishPenPerPage();
//    	getFishPortPerPage();
//    	getFishLandingPerPage();
//    	getFishProcessingPerPage();
    	
    	$('#emp_body').empty();
    	$('#thead_body').empty();
    	$('#fishcage_body').empty();
    	$('#training_body').empty();
    	$('#seaweeds_body').empty();
    	$('#seagrass_body').empty();
    	$('#school_body').empty();
    	$('#pfo_body').empty();	
    	$('#market_body').empty();	
    	$('#mariculture_body').empty();
    	$('#mangrove_body').empty();
    	$('#cold_body').empty();
    	$('#hatchery_body').empty();
    	$('#processing_body').empty();
    	$('#fishport_body').empty();
    	$('#fishpen_body').empty();
    	$('#fishlanding_body').empty();
    	$('#fishcoral_body').empty();
  
    	
    	getFishSanctuaryList(region_id);
    	getFishCageList(region_id);
    	getFishCoralList(region_id);
    	getPFOList();
    	getTrainingList();
    	getAllMaricultureList();
    	getMangroveList();
    	getSeaweedsList();
    	getSeaGrassList();
    	getSchoolList();
    	getMarketList();
    	getIPCSList();
    	getHatcheryList();
    	getFishPenList();
    	getFishPortList();
    	getFishLandingList();
    	getFishProcessingPlantList();
    	
    	 var slctProvinces=$('.provincemap').find('option').remove().end();
   	  var option="";
   	  
   	  slctProvinces.empty();
       option = "<option value=''>-- PLS SELECT--</option>";
       $.get("/create/getProvinceList/" + region_id , function(data, status){     	  			
   						for( let prop in data ){						 					
   								option = option + "<option value='"+data[prop].province_id+"'>"+data[prop].province+"</option>";	           
   							}
   					
   					 slctProvinces.append(option);
          		 	});
    	
    }
});

 var slctProvinces=$('.provincemap').find('option').remove().end();
	  var option="";
	  
	  slctProvinces.empty();
   option = "<option value=''>-- PLS SELECT--</option>";
   slctProvinces.append(option);
   

//function getAllFishSanctuaryPage(region){
//
//	$.get("/admin/fishsanctuaries_admin/" + region +"/"+ 0, function(content, status) {
//
//		var markup = "";
//		for (var x = 0; x < content.fishSanctuaries.length; x++) {
//			number_row = x +1;
//			markup += "<tr>" +
//        
//						"<td><span>" + content.fishSanctuaries[x].region + "</span></td>" +
//
//						"<td>" + content.fishSanctuaries[x].province + "</td>" +
//						"<td>" + content.fishSanctuaries[x].municipality + "</td>" +
//						"<td>" + content.fishSanctuaries[x].barangay + "</td>" +
//						"<td>" + content.fishSanctuaries[x].nameOfFishSanctuary + "</td>" +
//						"<td>" + content.fishSanctuaries[x].lat + "," + content.fishSanctuaries[x].lon + "</td>" +
//						"</tr>";
//		}
//		$("#export-buttons-table").find('tbody').empty();
//		$("#export-buttons-table").find('tbody').append(markup);
//		$("#paging").empty();
//		$("#paging").append(content.pageNumber);
//	});
//
//}
//function getPerPage() {
//	$('#paging').delegate('a','click',function() {
//					var $this = $(this), 
//					target = $this.data('target');
//					
//					$.get("/admin/fishsanctuaries_admin/" + region_id +"/"+ target, function(content, status) {
//
//						var markup = "";
//
//						for (var x = 0; x < content.fishSanctuaries.length; x++) {
//			
//							markup += "<tr>" + 
//									"<td><span>" + content.fishSanctuaries[x].region + "</span></td>" +
//									"<td>"+ content.fishSanctuaries[x].province + "</td>" +
//									"<td>"+ content.fishSanctuaries[x].municipality+ "</td>" +
//									"<td>" + content.fishSanctuaries[x].barangay + "</td>" +
//									"<td>"+ content.fishSanctuaries[x].nameOfFishSanctuary+ "</td>" +
//									"<td>" + content.fishSanctuaries[x].lat + "," + content.fishSanctuaries[x].lon+ "</td>" +
//									"</tr>";
//						}
//						$("#export-buttons-table").find('tbody').empty();
//						$("#export-buttons-table").find('tbody').append(markup);
//						$("#paging").empty();
//						$("#paging").append(content.pageNumber);
//					});
//					
//	});

//}
//function getFishSanctuaryList(region){
//	$.get("/admin/getAllfishsanctuariesList/" + region,function(content, status) {
//	
//				var markup = "";
//				var tr;
//				var region="";
//				var dataAsOf="";
//				
//				var excelHead="";
//			
//				for (var x = 0; x < content.length; x++) {
//					
//					 if(content.length - 1 === x) {
//						 region = content[x].region;
//						 dataAsOf = content[x].dateAsOf;
//					        $(".exportToExcel").show();
//					    }
//					
//					tr = $('<tr/>');
//					tr.append("<td>" + content[x].region+ "</td>");
//					tr.append("<td>" + content[x].province+ "</td>");
//					tr.append("<td>" + content[x].municipality+ "</td>");
//					tr.append("<td>" + content[x].barangay+ "</td>");
//					tr.append("<td>"+ content[x].nameOfFishSanctuary+ "</td>");
//					tr.append("<td>" + content[x].code + "</td>");
//					tr.append("<td>" + content[x].area + "</td>");
//					tr.append("<td>" + content[x].type + "</td>");
//					tr.append("<td>"+ content[x].fishSanctuarySource+ "</td>");
//					tr.append("<td>" + content[x].bfardenr+ "</td>");
//					tr.append("<td>" + content[x].sheltered+ "</td>");
//					tr.append("<td>"+ content[x].nameOfSensitiveHabitatMPA+ "</td>");
//					tr.append("<td>" + content[x].ordinanceNo+ "</td>");
//					tr.append("<td>" + content[x].ordinanceTitle+ "</td>");
//					tr.append("<td>" + content[x].dateEstablished+ "</td>");
//					tr.append("<td>" + content[x].lat + "</td>");
//					tr.append("<td>" + content[x].lon + "</td>");
//					tr.append("<td>" + content[x].remarks+ "</td>");
//
//					$('#emp_body').append(tr);
//					//ExportTable();
//				}
//				excelHead +=  "<tr><th align='center' colspan='17'>FISH SANCTUARY</th></tr>"			                
//		                         
//		            + "<tr><th>Region</th>"
//		            + "<th>Province</th>"
//		            + "<th>Municipality</th>"
//		            + "<th>Barangay</th>"
//		            + "<th>Name of FishSanctuary</th>"
//		            + "<th>Code</th>"
//		            + "<th>Area</th>"
//		            + "<th>Type</th>"
//		            + "<th>Data Source</th>"
//		            + "<th>Implementer</th>"
//		            + "<th>Sheltered</th>"
//		            + "<th>Name Of Sensitive Habitat MPA</th>"
//		            + "<th>Ordinance No.</th>"
//		            + "<th>Ordinance Title</th>"
//		            + "<th>Date Established</th>"
//		            + "<th>Latitude</th>"
//		            + "<th>Longitude</th>"
//		            + "<th>Remarks</th></tr>";	
//				
//				
//				$('#thead_body').append(excelHead);
//			});
//
//	}
//

//function getAllFishCageFirstPage(region){
//	$.get("/admin/fishcage_admin/" + region + "/" + 0, function(content, status){
//  
//		var markup = "";
//		for ( var x = 0; x < content.fishCagePage.length; x++) {
//		   markup += "<tr>" +
//		   "<td><span>"+ content.fishCagePage[x].region+ "</span></td>" +
//		   "<td>"+content.fishCagePage[x].province+"</td>" +
//		   "<td>"+content.fishCagePage[x].municipality+"</td>" +
//		   "<td>"+content.fishCagePage[x].barangay+"</td>" +
//		   "<td>"+content.fishCagePage[x].nameOfOperator+"</td>" +
//		    "<td>"+content.fishCagePage[x].lat+","+content.fishCagePage[x].lon+"</td>" +
//		   "</tr>";
//		}
//		$("#fishcage-table").find('tbody').empty();
//		$("#fishcage-table").find('tbody').append(markup);
//	$("#fishcage_paging").empty();		
//	$("#fishcage_paging").append(content.pageNumber);	
//  });
//
//}
//function getFishCagePerPage() {   
//    
//	  $('#paging').delegate('a', 'click' , function(){    	  
//      var $this = $(this),
//         target = $this.data('target');    
//      	
//		 $.get("/admin/fishcage_admin/" + region + "/" + target, function(content, status){
//			  
//			 var markup = "";
//					for ( var x = 0; x < content.fishCage.length; x++) {
//					   markup +="<tr>" +
//					   "<td><span>"+ content.fishCage[x].region+ "</span></td>" +
//					   "<td>"+content.fishCage[x].province+"</td>" +
//					   "<td>"+content.fishCage[x].municipality+"</td>" +
//					   "<td>"+content.fishCage[x].barangay+"</td>" +
//					   "<td>"+content.fishCage[x].nameOfOperator+"</td>" +
//					    "<td>"+content.fishCage[x].lat+","+content.fishCage[x].lon+"</td>" +
//					   "</tr>";
//					}
//					$("#fishcage-table").find('tbody').empty();
//					$("#fishcage-table").find('tbody').append(markup);
//				$("#fishcage_paging").empty();		
//				$("#fishcage_paging").append(content.pageNumber);	
//			  });
//
//    });       
// }
//function getFishCageList(region){
//	$.get("/admin/getAllfishcageList/" + region, function(content, status){
//	        	
//	        	 var markup = "";
//	        	 var tr;
//	       
//	         			for ( var x = 0; x < content.length; x++) {
//	         				
//	         				 if(content.length - 1 === x) {
//	     				        $(".exportToExcel").show();
//	     				    }
//	     				
//						tr = $('<tr/>');
//						tr.append("<td>" + content[x].region + "</td>");				
//						tr.append("<td>" + content[x].province + "</td>");
//						tr.append("<td>" + content[x].municipality + "</td>");
//						tr.append("<td>" + content[x].barangay + "</td>");
//						tr.append("<td>" + content[x].nameOfOperator + "</td>");
//						tr.append("<td>" + content[x].code + "</td>");
//						tr.append("<td>" + content[x].area + "</td>");
//						tr.append("<td>" + content[x].classificationofOperator + "</td>");
//						tr.append("<td>" + content[x].cageDimension + "</td>");
//						tr.append("<td>" + content[x].cageDesign + "</td>");
//						tr.append("<td>" + content[x].cageType + "</td>");
//						tr.append("<td>" + content[x].cageNoOfCompartments + "</td>");
//						tr.append("<td>" + content[x].indicateSpecies + "</td>");
//						tr.append("<td>" + content[x].sourceOfData + "</td>");
//						tr.append("<td>" + content[x].lat + "</td>");
//						tr.append("<td>" + content[x].lon + "</td>");
//						tr.append("<td>" + content[x].remarks + "</td>");
//						$('#fishcage_body').append(tr);	
//						}    		
//	    		  });
//}

//function getFishCoralPerPage(region){
//
//		 $.get("/admin/fishcoral_admin/"+ region + "/" + 0, function(content, status){
//	 
//
//var markup = "";
//		for ( var x = 0; x < content.fishCoral.length; x++) {
//		   markup +="<tr>" +
//		  "<td><span>" + content.fishCoral[x].region + "</span></td>" +
//		   		"<td>"+content.fishCoral[x].province+"</td>" +
//		   		"<td>"+content.fishCoral[x].municipality+"</td>" +
//		   		"<td>"+content.fishCoral[x].barangay+"</td>" +
//		   		"<td>"+content.fishCoral[x].nameOfOperator+"</td>" +
//		   		"<td>"+content.fishCoral[x].lat+ "," +content.fishCoral[x].lon+ "</td>" +
//		   	//	"<td class='map menu_links btn-primary' data-href="+ content.fishCoral[x].uniqueKey + ">MAP</td>" +
//		   	//	"<td class='view menu_links btn-primary' data-href="+ content.fishCoral[x].uniqueKey + ">View</td>" +
//		   		"</tr>";
//		}
//		$("#fishcoral-table").find('tbody').empty();
//		$("#fishcoral-table").find('tbody').append(markup);
//		$("#fishcoral_paging").empty();		
//		$("#fishcoral_paging").append(content.pageNumber);	
// });
//}
//function geFishCoralPage(){   
//    
//	  $('#paging').delegate('a', 'click' , function(){
//	  
//      var $this = $(this),
//         target = $this.data('target');    
//      	
//		 $.get("/admin/fishcoral_admin/"+ region + "/" + target, function(content, status){
//      	 
//	    	 
//	    	 var markup = "";
//	     			for ( var x = 0; x < content.fishCoral.length; x++) {
//	     			   markup +="<tr>" +
//	     			   			"<td><span>" + content.fishCoral[x].region + "</span></td>" +
//	     			   			"<td>"+content.fishCoral[x].province+"</td>" +
//	     			   			"<td>"+content.fishCoral[x].municipality+"</td>" +
//	     			   			"<td>"+content.fishCoral[x].barangay+"</td>" +
//	     			   			"<td>"+content.fishCoral[x].nameOfOperator+"</td>" +
//	     			   		//"<td class='map menu_links btn-primary' data-href="+ content.fishCoral[x].uniqueKey + ">MAP</td>" +
//	     			   		//"<td class='view menu_links btn-primary' data-href="+ content.fishCoral[x].uniqueKey + ">View</td>" +
//	     			   			"</tr>";
//					}
//	     			$("#fishcoral-table").find('tbody').empty();
//	     			$("#fishcoral-table").find('tbody').append(markup);
//	     			$("#fishcoral_paging").empty();		
//	     			$("#fishcoral_paging").append(content.pageNumber);	
//			  });;
//    });
//    
//
// }
//function getFishCoralList(region){	
//	
//	$.get("/admin/getAllfishcoralList/" + region, function(content, status){
//	        	
//	        	 var markup = "";
//	        	 var tr;
//	       
//	         			for ( var x = 0; x < content.length; x++) {
//	         			region = content[x].region;
//	         			
//	         			 if(content.length - 1 === x) {
//	 				       // console.log('loop ends');
//	 				        $(".exportToExcel").show();
//	 				    }
//	 				
//	         			
//						tr = $('<tr/>');
//						tr.append("<td>" + content[x].region + "</td>");
//						tr.append("<td>" + content[x].province + "</td>");
//						tr.append("<td>" + content[x].municipality + "</td>");
//						tr.append("<td>" + content[x].barangay + "</td>");
//						tr.append("<td>" + content[x].nameOfOperator + "</td>");
//						tr.append("<td>" + content[x].code + "</td>");
//						tr.append("<td>" + content[x].area + "</td>");
//						tr.append("<td>" + content[x].operatorClassification + "</td>");
//						tr.append("<td>" + content[x].nameOfStationGearUse + "</td>");
//						tr.append("<td>" + content[x].noOfUnitUse + "</td>");
//						tr.append("<td>" + content[x].fishesCaught + "</td>");
//						tr.append("<td>" + content[x].dataSource + "</td>");
//						tr.append("<td>" + content[x].lat + "</td>");
//						tr.append("<td>" + content[x].lon + "</td>");
//						tr.append("<td>" + content[x].remarks + "</td>");
//						
//						$('#fishcoral_body').append(tr);	
//						//ExportTable();
//						}    		
//	    		  });
//	}
//
//

//
//function getFishlandingDefaultPage(){
//
//	$.get("/admin/fishlanding_admin/" + region_id +"/"+ 0, function(content, status) {
//
//		var markup = "";
//		for (var x = 0; x < content.fishLanding.length; x++) {
//			markup += "<tr>" +
//					"<td><span>" + content.fishLanding[x].region + "</span><td>"
//					+ content.fishLanding[x].province + "</td><td>"
//					+ content.fishLanding[x].municipality + "</td><td>"
//					+ content.fishLanding[x].barangay + "</td><td>"
//					+ content.fishLanding[x].nameOfLanding
//					+ "</td><td>" + content.fishLanding[x].lat + ","
//					+ content.fishLanding[x].lon + "</td>" +
//					//"<td class='map menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">MAP</td>" +
//					//"<td class='view menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">View</td>" +
//					"</tr>";
//		}
//		$("#fishlanding-table").find('tbody').empty();
//		$("#fishlanding-table").find('tbody').append(markup);
//		$("#fishlanding_paging").empty();
//		$("#fishlanding_paging").append(content.pageNumber);
//	});
//}
//function getFishLandingList(){
//	$.get("/admin/getAllfishlandingList/" + region_id, function(content, status) {
//
//		var markup = "";
//		var tr;
//
//		for (var x = 0; x < content.length; x++) {
//			region = content[x].region;
//			
//			 if(content.length - 1 === x) {
//			        $(".exportToExcel").show();
//			    }
//			
//			
//			tr = $('<tr/>');
//			tr.append("<td>" + content[x].region + "</td>");			
//			tr.append("<td>" + content[x].province + "</td>");
//			tr.append("<td>" + content[x].municipality + "</td>");
//			tr.append("<td>" + content[x].barangay + "</td>");
//			tr.append("<td>" + content[x].nameOfLanding + "</td>");
//			tr.append("<td>" + content[x].code + "</td>");
//			tr.append("<td>" + content[x].area + "</td>");
//			tr.append("<td>" + content[x].classification + "</td>");
//			tr.append("<td>" + content[x].volumeOfUnloadingMT + "</td>");
//			tr.append("<td>" + content[x].type + "</td>");
//			tr.append("<td>" + content[x].cflc_status + "</td>");
//			tr.append("<td>" + content[x].dataSource + "</td>");
//			tr.append("<td>" + content[x].lat + "</td>");
//			tr.append("<td>" + content[x].lon + "</td>");
//			tr.append("<td>" + content[x].remarks + "</td>");
//			$('#fishlanding_body').append(tr);
//		}
//	});
//	}
//function getFishLandingPerPage() {
//		 $('#fishlanding_paging').delegate('a','click',function() {
//				var $this = $(this), 
//				target = $this.data('target');
//				
//				$.get("/admin/fishlanding_admin/" + region_id +"/" + target, function(content, status) {
//
//					var markup = "";
//					for (var x = 0; x < content.fishLanding.length; x++) {
//							//console.log(content.fishLanding[x].lat + ","+ content.fishLanding[x].lon);
//							markup += "<tr>" +
//							"<td><span>" + content.fishLanding[x].region + "</span><td>"
//							+ content.fishLanding[x].province + "</td><td>"
//							+ content.fishLanding[x].municipality + "</td><td>"
//							+ content.fishLanding[x].barangay + "</td><td>"
//							+ content.fishLanding[x].nameOfLanding
//							+ "</td><td>" + content.fishLanding[x].lat + ","
//							+ content.fishLanding[x].lon + "</td>" +
//							//"<td class='map menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">MAP</td>" +
//							//"<td class='view menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">View</td>" +
//							"</tr>";
//					}
//					$("#fishlanding-table").find('tbody').empty();
//					$("#fishlanding-table").find('tbody').append(markup);
//					$("#fishlanding_paging").empty();
//					$("#fishlanding_paging").append(content.pageNumber);
//				});
//		 });
//	}
//

//function getFishPenDefaultPage(){
//
//	 $.get("/admin/fishpen_admin/" + region_id +"/" + 0, function(content, status){
//
//var markup = "";
//		for ( var x = 0; x < content.fishPen.length; x++) {
//		  // console.log(content.fishPen[x].id);
//		   markup +="<tr>" +
//		   	"<td><span>" + content.fishPen[x].region + "</span></td>" +
//		   	"<td>"+content.fishPen[x].province+"</td>" +
//		   	"<td>"+content.fishPen[x].municipality+"</td>" +
//		   	"<td>"+content.fishPen[x].barangay+"</td>" +
//		   	"<td>"+content.fishPen[x].nameOfOperator+"</td>" +
//		   	"<td>"+content.fishPen[x].lat+","+content.fishPen[x].lon+"</td>" +
//		  // "<td class='map menu_links btn-primary' data-href="+ content.fishPen[x].uniqueKey + ">MAP</td>" +
//			//"<td class='view menu_links btn-primary' data-href="+ content.fishPen[x].uniqueKey + ">View</td>" +
//		   	"</tr>";
//		}
//		$("#fishpen-table").find('tbody').empty();
//		$("#fishpen-table").find('tbody').append(markup);
//	$("#fishpen_paging").empty();		
//	$("#fishpen_paging").append(content.pageNumber);	
// });
//}
//function getFishPenList(){			
//	$.get("/admin/getAllfishpenList/" + region_id, function(content, status){
//	        	 var markup = "";
//	        	 var tr;
//	       
//	         			for ( var x = 0; x < content.length; x++) {
//	         			region = content[x].region;
//	         			
//	         			 if(content.length - 1 === x) {
//	 				        $(".exportToExcel").show();
//	 				    }
//	 				
//	         			
//						tr = $('<tr/>');
//						tr.append("<td>" + content[x].region + "</td>");
//						tr.append("<td>" + content[x].province + "</td>");
//						tr.append("<td>" + content[x].municipality + "</td>");
//						tr.append("<td>" + content[x].barangay + "</td>");
//						tr.append("<td>" + content[x].nameOfOperator + "</td>");
//						tr.append("<td>" + content[x].code + "</td>");
//						tr.append("<td>" + content[x].area + "</td>");
//						tr.append("<td>" + content[x].noOfFishPen + "</td>");
//						tr.append("<td>" + content[x].speciesCultured + "</td>");
//						tr.append("<td>" + content[x].croppingStart + "</td>");
//						tr.append("<td>" + content[x].croppingEnd + "</td>");
//						tr.append("<td>" + content[x].sourceOfData + "</td>");
//						tr.append("<td>" + content[x].lat + "</td>");
//						tr.append("<td>" + content[x].lon + "</td>");
//						tr.append("<td>" + content[x].remarks + "</td>");
//						$('#fishpen_body').append(tr);	
//						}    		
//	    		  });
//	}
//function getFishPenPerPage(){  
//		
//		  $('#fishpen_paging').delegate('a', 'click' , function(){
//			  var $this = $(this),
//	           target = $this.data('target');    
//
//		     		 $.get("/admin/fishpen_admin/" + region_id +"/" + target, function(content, status){
//	        	 
//	        	 var markup = "";
//	         			for ( var x = 0; x < content.fishPen.length; x++) {
//	         			   //console.log(content.fishPen[x].id);
//	         			   markup +="<tr>" +
//	         			   			"<td><span>" + content.fishPen[x].region + "</span></td>" +
//	         			   			"<td>"+content.fishPen[x].province+"</td>" +
//	         			   			"<td>"+content.fishPen[x].municipality+"</td>" +
//	         			   			"<td>"+content.fishPen[x].barangay+"</td>" +
//	         			   			"<td>"+content.fishPen[x].nameOfOperator+"</td>" +
//	         			   			"<td>"+content.fishPen[x].lat+","+content.fishPen[x].lon+"</td>" +
//	         			   		//"<td class='map menu_links btn-primary' data-href="+ content.fishPen[x].uniqueKey + ">MAP</td>" +
//	    						//"<td class='view menu_links btn-primary' data-href="+ content.fishPen[x].uniqueKey + ">View</td>" +
//	         			   			"</tr>";
//						}
//	    				$("#fishpen-table").find('tbody').empty();
//	    				$("#fishpen-table").find('tbody').append(markup);
//	    			$("#fishpen_paging").empty();		
//	    			$("#fishpen_paging").append(content.pageNumber);	
//	    		  });
//	      });        
//	   }
//

//function getFishPortDefaultPage(){
//	  
//	$.get("/admin/fishport_admin/" + region_id +"/" + 0,function(content, status) {
//	
//	 var markup = "";
//			for ( var x = 0; x < content.fishPort.length; x++) {
//			  // console.log(content.fishPort[x].id);
//			   markup +="<tr>" +
//			   			"<td><span>" + content.fishPort[x].region + "</span></td>" +
//			   			"<td>"+content.fishPort[x].province+"</td>" +
//			   			"<td>"+content.fishPort[x].municipality+"</td>" +
//			   			"<td>"+content.fishPort[x].barangay+"</td>" +
//			   			"<td>"+content.fishPort[x].nameOfFishport+"</td>" +
//			   			"<td>"+content.fishPort[x].lat+","+content.fishPort[x].lon+"</td>" +
//			   			//"<td class='map menu_links btn-primary' data-href="+ content.fishPort[x].uniqueKey + ">MAP</td>" +
//						//"<td class='view menu_links btn-primary' data-href="+ content.fishPort[x].uniqueKey + ">View</td>" +
//			   			"</tr>";
//			}
//			$("#fishport-table").find('tbody').empty();
//			$("#fishport-table").find('tbody').append(markup);
//		$("#fishport_paging").empty();		
//		$("#fishport_paging").append(content.pageNumber);	
//	  });
//}
//function getFishPortList(){
//	
//	$.get("/admin/getAllfishportList/" + region_id, function(content, status){
//	        	 
//	        	 var markup = "";
//	        	 var tr;
//	       
//	         			for ( var x = 0; x < content.length; x++) {
//	         			region = content[x].region;
//	         			
//	         			 if(content.length - 1 === x) {
//	 				       // console.log('loop ends');
//	 				        $(".exportToExcel").show();
//	 				    }
//	 				
//	         			
//						tr = $('<tr/>');
//						tr.append("<td>" + content[x].region + "</td>");
//						tr.append("<td>" + content[x].province + "</td>");
//						tr.append("<td>" + content[x].municipality + "</td>");
//						tr.append("<td>" + content[x].barangay + "</td>");
//						tr.append("<td>" + content[x].nameOfCaretaker + "</td>");
//						tr.append("<td>" + content[x].code + "</td>");
//						tr.append("<td>" + content[x].area + "</td>");
//						tr.append("<td>" + content[x].nameOfCaretaker + "</td>");
//						tr.append("<td>" + content[x].type + "</td>");
//						tr.append("<td>" + content[x].classification + "</td>");
//						tr.append("<td>" + content[x].volumeOfUnloading + "</td>");
//						tr.append("<td>" + content[x].dataSource + "</td>");
//						tr.append("<td>" + content[x].lat + "</td>");
//						tr.append("<td>" + content[x].lon + "</td>");
//						tr.append("<td>" + content[x].remarks + "</td>");
//						
//						
//						$('#fishport_body').append(tr);	
//						}    		
//	    		  });
//	}
//function getFishPortPerPage(){
//		
//		$('#fishport_paging').delegate('a','click',function() {
//					var $this = $(this),
//					target = $this.data('target');
//
//					$.get("/admin/fishport_admin/" + region_id +"/" + target,function(content, status) {
//					   	
//					   	 var markup = "";
//					    			for ( var x = 0; x < content.fishPort.length; x++) {
//					    		
//					    			   markup +="<tr>" +
//					    			   			"<td><span>" + content.fishPort[x].region + "</span></td>" +
//					    			   			"<td>"+content.fishPort[x].province+"</td>" +
//					    			   			"<td>"+content.fishPort[x].municipality+"</td>" +
//					    			   			"<td>"+content.fishPort[x].barangay+"</td>" +
//					    			   			"<td>"+content.fishPort[x].nameOfFishport+"</td>" +
//					    			   			"<td>"+content.fishPort[x].lat+","+content.fishPort[x].lon+"</td>" +
//					    			   			//"<td class='map menu_links btn-primary' data-href="+ content.fishPort[x].uniqueKey + ">MAP</td>" +
//					    						//"<td class='view menu_links btn-primary' data-href="+ content.fishPort[x].uniqueKey + ">View</td>" +
//					    			   			"</tr>";
//									}
//									$("#fishport-table").find('tbody').empty();
//									$("#fishport-table").find('tbody').append(markup);
//								$("#fishport_paging").empty();		
//								$("#fishport_paging").append(content.pageNumber);	
//							  });
//		});
//
//	}

//function getFishProcessingPlantDefaultPage(){
//	
//	$.get("/admin/fishprocessing_admin/"+ region_id +"/" + 0,function(content, status) {
//
//		
//				var markup = "";
//				for (var x = 0; x < content.fishProcessingPlantPage.length; x++) {
//							//console.log("uniqueKey: " + content.fishProcessingPlantPage[x].uniqueKey);
//					markup += "<tr>"+
//							"<td><span>"+ content.fishProcessingPlantPage[x].region+ "</span></td>"+
//							"<td>"+ content.fishProcessingPlantPage[x].province+ "</td>" +
//							"<td>"+ content.fishProcessingPlantPage[x].municipality+"</td>" +
//							"<td>"+ content.fishProcessingPlantPage[x].barangay+ "</td>" +
//							"<td>"+ content.fishProcessingPlantPage[x].nameOfProcessingPlants+"</td>" +
//							"<td>"+ content.fishProcessingPlantPage[x].lat+ ","+ content.fishProcessingPlantPage[x].lon + "</td>"+
//							//"<td class='map menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].uniqueKey + ">MAP</td>" +
//							//"<td class='view menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].uniqueKey + ">View</td>"+ 
//							"</tr>";
//				}
//				$("#processing-table").find('tbody').empty();
//				$("#processing-table").find('tbody').append(markup);
//				$("#processing_paging").empty();
//				$("#processing_paging").append(content.pageNumber);
//			});
//}
//function getFishProcessingPlantList(){
//	$.get("/admin/getAllfishprocessingList/" + region_id,function(content, status) {
//						
//						var markup = "";
//						var tr;
//
//						for (var x = 0; x < content.length; x++) {
//							
//							if(content.length - 1 === x) {
//						        $(".exportToExcel").show();
//						    }
//							
//							tr = $('<tr/>');
//							tr.append("<td>"+ content[x].region+ "</td>");
//							tr.append("<td>"+ content[x].province+ "</td>");
//							tr.append("<td>"+ content[x].municipality+ "</td>");
//							tr.append("<td>"+ content[x].barangay+ "</td>");
//							tr.append("<td>"+ content[x].nameOfProcessingPlants+ "</td>");
//							tr.append("<td>" + content[x].code+ "</td>");
//							tr.append("<td>"+ content[x].nameOfOperator+ "</td>");
//							tr.append("<td>" + content[x].area+ "</td>");
//							tr.append("<td>"+ content[x].operatorClassification+ "</td>");
//							tr.append("<td>"+ content[x].processingTechnique+ "</td>");
//							tr.append("<td>"+ content[x].processingEnvironmentClassification+ "</td>");
//							tr.append("<td>"+ content[x].plantRegistered+ "</td>");
//							tr.append("<td>"+ content[x].bfarRegistered+ "</td>");
//							tr.append("<td>"+ content[x].packagingType+ "</td>");
//							tr.append("<td>"+ content[x].marketReach+ "</td>");
//							tr.append("<td>"+ content[x].businessPermitsAndCertificateObtained+ "</td>");
//							tr.append("<td>"+ content[x].indicateSpecies+ "</td>");
//							tr.append("<td>"+ content[x].sourceOfData+ "</td>");
//							tr.append("<td>" + content[x].lat+ "</td>");
//							tr.append("<td>" + content[x].lon+ "</td>");
//							tr.append("<td>"+ content[x].remarks+ "</td>");
//							$('#processing_body').append(tr);
//							//ExportTable();
//						}
//					});
//
//}
//function getFishProcessingPerPage(){
//	// var trigger = $('ul li a'); 
//	$('#processing_paging').delegate('a','click',function() {
//				var $this = $(this), 
//				target = $this.data('target');
//
//				$.get("/admin/fishprocessing_admin/"+ region_id +"/" + target,function(content, status) {
//
//							var markup = "";
//							for (var x = 0; x < content.fishProcessingPlantPage.length; x++) {
//								markup += "<tr>"+
//								"<td><span>"+ content.fishProcessingPlantPage[x].region+ "</span></td>"+
//								"<td>"+ content.fishProcessingPlantPage[x].province+ "</td>" +
//								"<td>"+ content.fishProcessingPlantPage[x].municipality+"</td>" +
//								"<td>"+ content.fishProcessingPlantPage[x].barangay+ "</td>" +
//								"<td>"+ content.fishProcessingPlantPage[x].nameOfProcessingPlants+"</td>" +
//								"<td>"+ content.fishProcessingPlantPage[x].lat+ ","+ content.fishProcessingPlantPage[x].lon + "</td>"+
//								//"<td class='map menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].uniqueKey + ">MAP</td>" +
//								//"<td class='view menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].uniqueKey + ">View</td>"+ 
//								"</tr>";
//							}
//							$("#processing-table")
//									.find('tbody').empty();
//							$("#processing-table")
//									.find('tbody').append(markup);
//							$("#processing_paging").empty();
//							$("#processing_paging").append(content.pageNumber);
//						});
//					});
//
//					
//}
//
//function getHacheryDefaultPerPage(){
//	
//	 $.get("/admin/hatchery_admin/" + region_id +"/" + 0, function(content, status){
//   	 
//    	
//   	 var markup = "";
//    			for ( var x = 0; x < content.hatcheries.length; x++) {
//    			 
//    			   markup +="<tr>" +
//    			   			"<td><span>" + content.hatcheries[x].region + "</span></td>" +
//    			   			"<td>"+content.hatcheries[x].province+"</td>" +
//    			   			"<td>"+content.hatcheries[x].municipality+"</td>" +
//    			   			"<td>"+content.hatcheries[x].barangay+"</td>" +
//    			   			"<td>"+content.hatcheries[x].nameOfHatchery+"</td>" +
//    			   			"<td>"+content.hatcheries[x].lat+","+content.hatcheries[x].lon+"</td>" +
//    			   			//"<td class='map menu_links btn-primary' data-href="+ content.hatcheries[x].uniqueKey + ">MAP</td>" +
//    			   			//"<td class='view menu_links btn-primary' data-href="+ content.hatcheries[x].uniqueKey + ">View</td>" +			
//    			   			"</tr>";
//				}
//				$("#hatchery-table").find('tbody').empty();
//				$("#hatchery-table").find('tbody').append(markup);
//			$("#hatchery_paging").empty();		
//			$("#hatchery_paging").append(content.pageNumber);	
//		  });
//}
//function getHatcheryList(){
//	$.get("/admin/getAllhatcheryList/" + region_id, function(content, status){
//	        	
//	        	 var markup = "";
//	        	 var tr;
//	       
//	         			for ( var x = 0; x < content.length; x++) {
//	         			region = content[x].region;
//	         			
//	         			 if(content.length - 1 === x) {
//	 				        $(".exportToExcel").show();
//	 				    }
//	 				
//	         			
//						tr = $('<tr/>');
//						tr.append("<td>" + content[x].region + "</td>");
//						tr.append("<td>" + content[x].province + "</td>");
//						tr.append("<td>" + content[x].municipality + "</td>");
//						tr.append("<td>" + content[x].barangay + "</td>");
//						tr.append("<td>" + content[x].nameOfHatchery + "</td>");
//						tr.append("<td>" + content[x].legislated + "</td>");
//						tr.append("<td>" + content[x].status + "</td>");
//						tr.append("<td>" + content[x].ra + "</td>");
//						tr.append("<td>" + content[x].title + "</td>");
//						tr.append("<td>" + content[x].area + "</td>");
//						tr.append("<td>" + content[x].nameOfHatchery + "</td>");
//						tr.append("<td>" + content[x].nameOfOperator + "</td>");
//						tr.append("<td>" + content[x].addressOfOperator + "</td>");
//						tr.append("<td>" + content[x].operatorClassification + "</td>");					
//						tr.append("<td>" + content[x].publicprivate + "</td>");
//						tr.append("<td>" + content[x].indicateSpecies + "</td>");
//						tr.append("<td>" + content[x].prodStocking + "</td>");
//						tr.append("<td>" + content[x].prodCycle + "</td>");
//						tr.append("<td>" + content[x].actualProdForTheYear + "</td>");
//						tr.append("<td>" + content[x].dataSource + "</td>");
//						tr.append("<td>" + content[x].lat + "</td>");
//						tr.append("<td>" + content[x].lon + "</td>");
//						tr.append("<td>" + content[x].remarks + "</td>");
//						
//						$('#hatchery_body').append(tr);	
//						}    		
//	    		  });
//				}
//function getHatcheryPerPage(){   
//
//		  $('#hatchery_paging').delegate('a', 'click' , function(){
//			  var $this = $(this),
//	           target = $this.data('target');    
//
//			  $.get("/admin/hatchery_admin/" + region_id +"/" + target, function(content, status){
//		        	 
//		         	
//		        	 var markup = "";
//		         			for ( var x = 0; x < content.hatcheries.length; x++) {
//		         			 
//		         			   markup +="<tr>" +
//		         			  "<td><span>" + content.hatcheries[x].region + "</span></td>" +
//		         			   				"<td>"+content.hatcheries[x].province+"</td>" +
//		         			   				"<td>"+content.hatcheries[x].municipality+"</td>" +
//		         			   				"<td>"+content.hatcheries[x].barangay+"</td>" +
//		         			   				"<td>"+content.hatcheries[x].nameOfHatchery+"</td>" +
//		         			   				"<td>"+content.hatcheries[x].lat+","+content.hatcheries[x].lon+"</td>" +
//		         			   			//"<td class='map menu_links btn-primary' data-href="+ content.hatcheries[x].uniqueKey + ">MAP</td>" +
//		         			   			//"<td class='view menu_links btn-primary' data-href="+ content.hatcheries[x].uniqueKey + ">View</td>" +		
//		         			   				"</tr>";
//							}
//		    				$("#hatchery-table").find('tbody').empty();
//		    				$("#hatchery-table").find('tbody').append(markup);
//		    			$("#hatchery_paging").empty();		
//		    			$("#hatchery_paging").append(content.pageNumber);	
//		    		  });
//	      });
//	      
//
//	   }

//function getIPCSDefaultPage(){
//	
//	 $.get("/admin/ipcs_admin/"+ region_id +"/" + 0, function(content, status){
//
//	  
//	 var markup = "";
//			for ( var x = 0; x < content.icePlantColdStorage.length; x++) {
//			  // console.log(content.icePlantColdStorage[x].id);
//			   markup +="<tr>" +
//			  "<td><span>" + content.icePlantColdStorage[x].region + "</span></td>" +
//			   				"<td>"+content.icePlantColdStorage[x].province+"</td><td>"+content.icePlantColdStorage[x].municipality+"</td><td>"+content.icePlantColdStorage[x].barangay+"</td><td>"+content.icePlantColdStorage[x].nameOfIcePlant+"</td><td>"+content.icePlantColdStorage[x].lat+","+content.icePlantColdStorage[x].lon+"</td>" +
//			   			//"<td class='map menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].uniqueKey + ">MAP</td>" +
//						//"<td class='view menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].uniqueKey + ">View</td>" +
//						
//			   				"</tr>";
//			}
//			$("#cold-table").find('tbody').empty();
//			$("#cold-table").find('tbody').append(markup);
//		$("#cold_paging").empty();		
//		$("#cold_paging").append(content.pageNumber);	
//	  });
//}
//function getIPCSList(){
//	$.get("/admin/getAllIPCSList/" + region_id, function(content, status){
//	        	
//	        	 var markup = "";
//	        	 var tr;
//	       
//	         			for ( var x = 0; x < content.length; x++) {
//	         			region = content[x].region;
//	         			
//	         			 if(content.length - 1 === x) {
//
//	 				        $(".exportToExcel").show();
//	 				    }
//	 				
//	         			
//						tr = $('<tr/>');
//						tr.append("<td>" + content[x].region + "</td>");
//						tr.append("<td>" + content[x].province + "</td>");
//						tr.append("<td>" + content[x].municipality + "</td>");
//						tr.append("<td>" + content[x].barangay + "</td>");
//						tr.append("<td>" + content[x].nameOfIcePlant + "</td>");
//						tr.append("<td>" + content[x].code + "</td>");
//						tr.append("<td>" + content[x].area + "</td>");
//						tr.append("<td>" + content[x].sanitaryPermit + "</td>");
//						tr.append("<td>" + content[x].operator + "</td>");
//						tr.append("<td>" + content[x].typeOfFacility + "</td>");
//						tr.append("<td>" + content[x].structureType + "</td>");
//						tr.append("<td>" + content[x].capacity + "</td>");
//						tr.append("<td>" + content[x].withValidLicenseToOperate + "</td>");
//						tr.append("<td>" + content[x].withValidSanitaryPermit + "</td>");	
//						tr.append("<td>" + content[x].withValidSanitaryPermitYes + "</td>");
//						tr.append("<td>" + content[x].withValidSanitaryPermitOthers + "</td>");								
//						tr.append("<td>" + content[x].businessPermitsAndCertificateObtained + "</td>");
//						tr.append("<td>" + content[x].typeOfIceMaker + "</td>");
//						tr.append("<td>" + content[x].foodGradule + "</td>");
//						tr.append("<td>" + content[x].dataSource + "</td>");
//						tr.append("<td>" + content[x].lat + "</td>");
//						tr.append("<td>" + content[x].lon + "</td>");
//						tr.append("<td>" + content[x].remarks + "</td>");
//						$('#cold_body').append(tr);	
//						}    		
//	    		  });
//	}
//function getICPSPerPage(){   
//
//		  $('#cold_paging').delegate('a', 'click' , function(){
//
//	        var $this = $(this),
//	           target = $this.data('target');    
//	           
//	        $.get("/admin/ipcs_admin/"+ region_id +"/" + target, function(content, status){
//
//				  
//	       	 var markup = "";
//	        			for ( var x = 0; x < content.icePlantColdStorage.length; x++) {
//	        			  
//	        			   markup +="<tr>" +
//	        			   "<td><span>" + content.icePlantColdStorage[x].region + "</span></td>" +
//	        			   				"<td>"+content.icePlantColdStorage[x].province+"</td><td>"+content.icePlantColdStorage[x].municipality+"</td><td>"+content.icePlantColdStorage[x].barangay+"</td><td>"+content.icePlantColdStorage[x].nameOfIcePlant+"</td><td>"+content.icePlantColdStorage[x].lat+","+content.icePlantColdStorage[x].lon+"</td>" +
//	        			   				//"<td class='map menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].uniqueKey + ">MAP</td>" +
//	            						//"<td class='view menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].uniqueKey + ">View</td>" +		
//	        			   				"</tr>";
//						}
//	   				$("#cold-table").find('tbody').empty();
//	   				$("#cold-table").find('tbody').append(markup);
//	   			$("#cold_paging").empty();		
//	   			$("#cold_paging").append(content.pageNumber);	
//	   		  });
//	      });
//	      
//
//	   } 

function getMangroveDefaultPage(){
	
		 $.get("/admin/mangrove_admin/"+ region_id +"/" + 0, function(content, status){
	 

var markup = "";
		for ( var x = 0; x < content.mangrove.length; x++) {
		   markup +="<tr>" +
		  "<td><span>" + content.mangrove[x].region + "</span</td>" +
		   				"<td>"+content.mangrove[x].province+"</td><td>"+content.mangrove[x].municipality+"</td><td>"+content.mangrove[x].barangay+"</td><td>"+content.mangrove[x].indicateSpecies+"</td><td>"+content.mangrove[x].lat+","+content.mangrove[x].lon+"</td>" +
		   			//"<td class='map menu_links btn-primary' data-href="+ content.mangrove[x].uniqueKey + ">MAP</td>" +
					//"<td class='view menu_links btn-primary' data-href="+ content.mangrove[x].uniqueKey + ">View</td>" +
		   				"</tr>";
		}
		$("#mangrove-table").find('tbody').empty();
		$("#mangrove-table").find('tbody').append(markup);
	$("#mangrove_paging").empty();		
	$("#mangrove_paging").append(content.pageNumber);	
 });
}
function getMangroveList(){
	$.get("/admin/getAllmangroveList/" + region_id, function(content, status){
	        	 
	        	 var markup = "";
	        	 var tr;
	       
	         			for ( var x = 0; x < content.length; x++) {
	         			region = content[x].region;
	         			
	         			 if(content.length - 1 === x) {
	 				        $(".exportToExcel").show();
	 				    }
	 				
	         			
						tr = $('<tr/>');
						tr.append("<td>" + content[x].region + "</td>");
						tr.append("<td>" + content[x].province + "</td>");
						tr.append("<td>" + content[x].municipality + "</td>");
						tr.append("<td>" + content[x].barangay + "</td>");
						tr.append("<td>" + content[x].code + "</td>");
						tr.append("<td>" + content[x].area + "</td>");
						tr.append("<td>" + content[x].indicateSpecies + "</td>");
						tr.append("<td>" + content[x].type + "</td>");
						tr.append("<td>" + content[x].dataSource + "</td>");
						tr.append("<td>" + content[x].lat + "</td>");
						tr.append("<td>" + content[x].lon + "</td>");
						tr.append("<td>" + content[x].remarks + "</td>");
						
						$('#mangrove_body').append(tr);	
						//ExportTable();
						}    		
	    		  });
	}
function getMangrovePerPage(){   

		  $('#mangrove_paging').delegate('a', 'click' , function(){
			  var $this = $(this),
	           target = $this.data('target');    
	        	
			  $.get("/admin/mangrove_admin/"+ region_id +"/" + target, function(content, status){
		        	 
			    	 
			    	 var markup = "";
			     			for ( var x = 0; x < content.mangrove.length; x++) {
			     			   markup +="<tr>" +
			     			  "<td><span>" + content.mangrove[x].region + "</span></td>" +
			     			   				"<td>"+content.mangrove[x].province+"</td><td>"+content.mangrove[x].municipality+"</td><td>"+content.mangrove[x].barangay+"</td><td>"+content.mangrove[x].indicateSpecies+"</td><td>"+content.mangrove[x].lat+","+content.mangrove[x].lon+"</td>" +
			     			   			//"<td class='map menu_links btn-primary' data-href="+ content.mangrove[x].uniqueKey + ">MAP</td>" +
			    						//"<td class='view menu_links btn-primary' data-href="+ content.mangrove[x].uniqueKey + ">View</td>" +
			     			   				"</tr>";
							}
							$("#mangrove-table").find('tbody').empty();
							$("#mangrove-table").find('tbody').append(markup);
						$("#mangrove_paging").empty();		
						$("#mangrove_paging").append(content.pageNumber);	
					  });
	      });
	      

	   }

function getMaricultureDefaultPage(){
	
	 $.get("/admin/mariculturezone_admin/"+ region_id +"/" + 0, function(content, status){
   	          	      	
  	 var markup = "";
   			for ( var x = 0; x < content.maricultureZone.length; x++) {
   			  // console.log(content.maricultureZone[x].id);
   			   markup +="<tr>" +
   			   				"<td><span>" + content.maricultureZone[x].region + "</span></td>" +
   			   				"<td>"+content.maricultureZone[x].province+"</td><td>"+content.maricultureZone[x].municipality+"</td><td>"+content.maricultureZone[x].barangay+"</td><td>"+content.maricultureZone[x].nameOfMariculture+"</td><td>"+content.maricultureZone[x].lat+","+content.maricultureZone[x].lat+"</td>" +
   			   				//"<td class='map menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">MAP</td>" +
   							//"<td class='view menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">View</td>" +
   			   				"</tr>";
				}
				$("#mariculture-table").find('tbody').empty();
				$("#mariculture-table").find('tbody').append(markup);
			$("#mariculture_paging").empty();		
			$("#mariculture_paging").append(content.pageNumber);	
		  });
}
function getAllMaricultureList(){			
	$.get("/admin/getAllmariculturezoneList/" + region_id, function(content, status){
	        	
	        	 var markup = "";
	        	 var tr;
	       
	         			for ( var x = 0; x < content.length; x++) {
	         			region = content[x].region;
						
	         			 if(content.length - 1 === x) {
	 				        $(".exportToExcel").show();
	 				    }
	 				
	         			
	         			tr = $('<tr/>');
						
						tr.append("<td>" + content[x].province + "</td>");
						tr.append("<td>" + content[x].municipality + "</td>");
						tr.append("<td>" + content[x].barangay + "</td>");
						tr.append("<td>" + content[x].nameOfMariculture + "</td>");
						tr.append("<td>" + content[x].code + "</td>");
						tr.append("<td>" + content[x].area + "</td>");
						tr.append("<td>" + content[x].dateLaunched + "</td>");
						tr.append("<td>" + content[x].dateInacted + "</td>");
						tr.append("<td>" + content[x].eccNumber + "</td>");
						tr.append("<td>" + content[x].maricultureType + "</td>");
						tr.append("<td>" + content[x].municipalOrdinanceNo + "</td>");
						tr.append("<td>" + content[x].dateApproved + "</td>");
						/*
						tr.append("<td>" + content.maricultureZone[x].individual + "</td>");
						tr.append("<td>" + content.maricultureZone[x].partnership + "</td>");
						tr.append("<td>" + content.maricultureZone[x].cooperativeAssociation + "</td>");
						tr.append("<td>" + content.maricultureZone[x].cageType + "</td>");
						tr.append("<td>" + content.maricultureZone[x].cageSize + "</td>");
						tr.append("<td>" + content.maricultureZone[x].cageQuantity + "</td>");
						tr.append("<td>" + content.maricultureZone[x].speciesCultured + "</td>");
						tr.append("<td>" + content.maricultureZone[x].quantityMt + "</td>");
						tr.append("<td>" + content.maricultureZone[x].kind + "</td>");
						tr.append("<td>" + content.maricultureZone[x].productionPerCropping + "</td>");
						tr.append("<td>" + content.maricultureZone[x].numberofCropping + "</td>");
						tr.append("<td>" + content.maricultureZone[x].species + "</td>");
						tr.append("<td>" + content.maricultureZone[x].farmProduce + "</td>");
						tr.append("<td>" + content.maricultureZone[x].productDestination + "</td>");
						tr.append("<td>" + content.maricultureZone[x].aquacultureProduction + "</td>");
						*/
						tr.append("<td>" + content[x].dataSource + "</td>");
						tr.append("<td>" + content[x].lat + "</td>");
						tr.append("<td>" + content[x].lon + "</td>");
						tr.append("<td>" + content[x].remarks + "</td>");
						
						$('#mariculture_body').append(tr);	
						//ExportTable();
						}    		
	    		  });
	}
function getMariculturePerPage(){   

		  $('#mariculture_paging').delegate('a', 'click' , function(){
			  var $this = $(this),
	           target = $this.data('target');    
	          
			  $.get("/admin/mariculturezone_admin/"+ region_id +"/" + target, function(content, status){
	    	       	
			       	 var markup = "";
			        			for ( var x = 0; x < content.maricultureZone.length; x++) {
			        			 //  console.log(content.maricultureZone[x].id);
			        			   markup +="<tr>" +
			        			   "<td><span>" + content.maricultureZone[x].region + "</span></td>" +
			        			   				"<td>"+content.maricultureZone[x].province+"</td><td>"+content.maricultureZone[x].municipality+"</td><td>"+content.maricultureZone[x].barangay+"</td><td>"+content.maricultureZone[x].nameOfMariculture+"</td><td>"+content.maricultureZone[x].lat+","+content.maricultureZone[x].lat+"</td>" +
			        			   				//"<td class='map menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">MAP</td>" +
			        							//"<td class='view menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">View</td>" +
			        			   				"</tr>";
								}
			   				$("#mariculture-table").find('tbody').empty();
			   				$("#mariculture-table").find('tbody').append(markup);
			   			$("#mariculture_paging").empty();		
			   			$("#mariculture_paging").append(content.pageNumber);	
			   		  });
	      });
	      

	   }

//function getMarketDefaultPage(){
//	$.get("/admin/market_admin/"+ region_id +"/" + 0, function(content, status) {
//
//		var markup = "";
//		for (var x = 0; x < content.market.length; x++) {
//
//			markup += "<tr>" +
//			"<td><span>" + content.market[x].region + "</span></td>" +
//			"<td>"+ content.market[x].province + "</td>" +
//			"<td>" + content.market[x].municipality + "</td>" +
//			"<td>"+ content.market[x].barangay + "</td>"+
//			"<td>"+ content.market[x].nameOfMarket + "</td>"+
//			"<td>"+ content.market[x].lat + "," + content.market[x].lon + "</td>" +
//			//"<td class='map menu_links btn-primary' data-href="+ content.market[x].uniqueKey + ">MAP</td>" +
//			//"<td class='view menu_links btn-primary' data-href="+ content.market[x].uniqueKey + ">View</td>" +
//			"</tr>";
//		}
//		$("#market-table").find('tbody').empty();
//		$("#market-table").find('tbody').append(markup);
//		$("#market_paging").empty();
//		$("#market_paging").append(content.pageNumber);
//	});
//}
//function getMarketPerPage(){
//
//	$('#market_paging').delegate('a','click',function() {
//			var $this = $(this), 
//			target = $this.data('target');
//
//			$.get("/admin/market_admin/"+ region_id +"/" + target, function(content, status) {
//
//				var markup = "";
//				for (var x = 0; x < content.market.length; x++) {
//
//					markup += "<tr>" +
//					"<td><span>" + content.market[x].region + "</span></td>" +
//					"<td>"+ content.market[x].province + "</td>" +
//					"<td>" + content.market[x].municipality + "</td>" +
//					"<td>"+ content.market[x].barangay + "</td>"+
//					"<td>"+ content.market[x].nameOfMarket + "</td>"+
//					"<td>"+ content.market[x].lat + "," + content.market[x].lon + "</td>" +
//					//"<td class='map menu_links btn-primary' data-href="+ content.market[x].uniqueKey + ">MAP</td>" +
//					//"<td class='view menu_links btn-primary' data-href="+ content.market[x].uniqueKey + ">View</td>" +
//					"</tr>";
//				}
//				$("#market-table").find('tbody').empty();
//				$("#market-table").find('tbody').append(markup);
//				$("#market_paging").empty();
//				$("#market_paging").append(content.pageNumber);
//			});
//			//return target;
//	});
//
//}
//function getMarketList(){
//	$.get("/admin/getAllmarketList/" + region_id, function(content, status) {
//
//		var markup = "";
//		var tr;
//
//		for (var x = 0; x < content.length; x++) {
//			region = content[x].region;
//			
//			 if(content.length - 1 === x) {
//
//			        $(".exportToExcel").show();
//			    }
//			
//			
//			tr = $('<tr/>');
//			tr.append("<td>" + content[x].region + "</td>");
//			tr.append("<td>" + content[x].province + "</td>");
//			tr.append("<td>" + content[x].municipality + "</td>");
//			tr.append("<td>" + content[x].barangay + "</td>");
//			tr.append("<td>" + content[x].code + "</td>");
//			tr.append("<td>" + content[x].area + "</td>");
//			tr.append("<td>" + content[x].nameOfMarket + "</td>");
//			tr.append("<td>" + content[x].publicprivate + "</td>");
//			tr.append("<td>" + content[x].majorMinorMarket + "</td>");
//			tr.append("<td>" + content[x].volumeTraded + "</td>");
//			tr.append("<td>" + content[x].dataSource + "</td>");
//			tr.append("<td>" + content[x].lat + "</td>");
//			tr.append("<td>" + content[x].lon + "</td>");
//			tr.append("<td>" + content[x].remarks + "</td>");
//
//			$('#market_body').append(tr);
//			//ExportTable();
//		}
//	});
//	}

function getPFoDefaultPage(){
	 $.get("/admin/lgu_admin/"+ region_id +"/" + 0, function(content, status){
	      	
	 var markup = "";
			for ( var x = 0; x < content.lgu.length; x++) {
			
			   markup +="<tr>" +
			   "<td><span>" + content.lgu[x].region + "</span></td>" +
			   "<td>"+content.lgu[x].province+"</td>" +
			   	"<td>"+content.lgu[x].municipality+"</td>" +
			   	"<td>"+content.lgu[x].barangay+"</td>" +
			   	"<td>"+content.lgu[x].lguName+"</td>" +
			   	"<td>"+content.lgu[x].lat+","+content.lgu[x].lon+"</td>" +
			   //"<td class='map menu_links btn-primary' data-href="+ content.lgu[x].uniqueKey + ">MAP</td>" +
				//"<td class='view menu_links btn-primary' data-href="+ content.lgu[x].uniqueKey + ">View</td>" +
				
			   "</tr>";
			}
			$("#pfo-table").find('tbody').empty();
			$("#pfo-table").find('tbody').append(markup);
			$("#pfo_paging").empty();		
			$("#pfo_paging").append(content.pageNumber);	
	  });
}
function getPFOList(){			
	$.get("/admin/getAlllguList/" + region_id, function(content, status){
	        	
	        	 var markup = "";
	        	 var tr;
	       
	         			for ( var x = 0; x < content.length; x++) {
	         			region = content[x].region;
	         			
	         			 if(content.length - 1 === x) {
	 				        $(".exportToExcel").show();
	 				    }
	 				
	         			
						tr = $('<tr/>');
						tr.append("<td>" + content[x].region + "</td>");
						tr.append("<td>" + content[x].province + "</td>");
						tr.append("<td>" + content[x].municipality + "</td>");
						tr.append("<td>" + content[x].barangay + "</td>");
						tr.append("<td>" + content[x].code + "</td>");
						tr.append("<td>" + content[x].lguName + "</td>");
						tr.append("<td>" + content[x].noOfBarangayCoastal + "</td>");
						tr.append("<td>" + content[x].noOfBarangayInland + "</td>");
						tr.append("<td>" + content[x].lguCoastalLength + "</td>");
						tr.append("<td>" + content[x].dataSource + "</td>");
						tr.append("<td>" + content[x].lat + "</td>");
						tr.append("<td>" + content[x].lon + "</td>");
						tr.append("<td>" + content[x].remarks + "</td>");
						
						$('#pfo_body').append(tr);	
						//ExportTable();
						}    		
	    		  });
	}
function getPFOPerPage(){   

		  $('#pfo_paging').delegate('a', 'click' , function(){
			  var $this = $(this),
	           target = $this.data('target');    
	          
			  $.get("/admin/lgu_admin/"+ region_id +"/" + target, function(content, status){
			       	 //	console.log("/admin/lgu_admin/: " +content);      	
			       	 var markup = "";
			        for ( var x = 0; x < content.lgu.length; x++) {		        			
			        	markup +="<tr>" +
			        	"<td><span>" + content.lgu[x].region + "</span></td>" +
			        	"<td>"+content.lgu[x].province+"</td>" +
			        	"<td>"+content.lgu[x].municipality+"</td>" +
			        	"<td>"+content.lgu[x].barangay+"</td>" +
			        	"<td>"+content.lgu[x].lguName+"</td>" +
			        	"<td>"+content.lgu[x].lat+","+content.lgu[x].lon+"</td>" +
			        	//"<td class='map menu_links btn-primary' data-href="+ content.lgu[x].uniqueKey + ">MAP</td>" +
						//"<td class='view menu_links btn-primary' data-href="+ content.lgu[x].uniqueKey + ">View</td>" +
			        	"</tr>";
					}
			   				$("#pfo-table").find('tbody').empty();
			   				$("#pfo-table").find('tbody').append(markup);
			   				$("#pfo_paging").empty();		
			   				$("#pfo_paging").append(content.pageNumber);	
			   		  });
	      });       
	   }

//function getSchoolDefaultPage(){
//
//	$.get("/admin/schooloffisheries_admin/"+ region_id +"/" + 0, function(content, status) {
//
//		var markup = "";
//		for (var x = 0; x < content.schoolOfFisheries.length; x++) {
//			markup += "<tr>" +
//					"<td><span>" + content.schoolOfFisheries[x].region + "</span></td>" +
//							
//					"<td>"+ content.schoolOfFisheries[x].province
//					+ "</td><td>"
//					+ content.schoolOfFisheries[x].municipality
//					+ "</td><td>"
//					+ content.schoolOfFisheries[x].barangay
//					+ "</td><td>" + content.schoolOfFisheries[x].name
//					+ "</td><td>" + content.schoolOfFisheries[x].lat
//					+ "," + content.schoolOfFisheries[x].lon
//					+ "</td>" +
//					//"<td class='map menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].uniqueKey + ">MAP</td>" +
//					//"<td class='view menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].uniqueKey + ">View</td>" +
//					
//							"</tr>";
//		}
//		$("#school-table").find('tbody').empty();
//		$("#school-table").find('tbody').append(markup);
//		$("#school_paging").empty();
//		$("#school_paging").append(content.pageNumber);
//	});
//}
//function getSchoolList(){
//	$.get("/admin/getAllSchooloffisheriesList/" + region_id,function(content, status) {
//
//						var markup = "";
//						var tr;
//
//						for (var x = 0; x < content.length; x++) {
//							
//							 if(content.length - 1 === x) {
//							       // console.log('loop ends');
//							        $(".exportToExcel").show();
//							    }
//							
//							
//							tr = $('<tr/>');
//
//							tr.append("<td>"
//									+ content[x].region
//									+ "</td>");
//							tr.append("<td>"
//									+ content[x].province
//									+ "</td>");
//							tr.append("<td>"
//									+ content[x].municipality
//									+ "</td>");
//							tr.append("<td>"
//									+ content[x].barangay
//									+ "</td>");
//							tr.append("<td>" + content[x].code
//									+ "</td>");
//							tr.append("<td>" + content[x].area
//									+ "</td>");
//							tr.append("<td>" + content[x].name
//									+ "</td>");
//							tr.append("<td>"
//									+ content[x].dateEstablished
//									+ "</td>");
//							tr.append("<td>"
//											+ content[x].numberStudentsEnrolled
//											+ "</td>");
//							tr.append("<td>"
//									+ content[x].dataSource
//									+ "</td>");
//							tr.append("<td>" + content[x].lat
//									+ "</td>");
//							tr.append("<td>" + content[x].lon
//									+ "</td>");
//							tr.append("<td>" + content[x].remarks
//									+ "</td>");
//
//							$('#school_body').append(tr);
//							// ExportTable();
//						}
//					});
//	}
//function getSchoolPerPage(){
//		$('#school_paging').delegate('a','click',function() {
//				var $this = $(this), 
//				target = $this.data('target');
//
//				$.get("/admin/schooloffisheries_admin/"+ region_id +"/" + target, function(content, status) {
//
//					var markup = "";
//					for (var x = 0; x < content.schoolOfFisheries.length; x++) {
//						markup += "<tr>" +
//								"<td><span>" + content.schoolOfFisheries[x].region + "</span></td><td>" +
//								+ content.schoolOfFisheries[x].province
//								+ "</td><td>"
//								+ content.schoolOfFisheries[x].municipality
//								+ "</td><td>"
//								+ content.schoolOfFisheries[x].barangay
//								+ "</td><td>" + content.schoolOfFisheries[x].name
//								+ "</td><td>" + content.schoolOfFisheries[x].lat
//								+ "," + content.schoolOfFisheries[x].lon
//								+ "</td>" +
//								//"<td class='map menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].uniqueKey + ">MAP</td>" +
//								//"<td class='view menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].uniqueKey + ">View</td>" +
//										
//								"</tr>";
//					}
//					$("#school-table").find('tbody').empty();
//					$("#school-table").find('tbody').append(markup);
//					$("#school_paging").empty();
//					$("#school_paging").append(content.pageNumber);
//				});
//		});
//	}

//function getSeagrassDefaultPage(){
//
//	 $.get("/admin/seagrass_admin/"+ region_id +"/" + 0, function(content, status){
//		 
//		 var markup = "";
//	 			for ( var x = 0; x < content.seaGrass.length; x++) {
//	 			  // console.log(content.seaGrass[x].id);
//	 			   markup +="<tr>" +
//	 			  "<td><span>" + content.seaGrass[x].region + "</span></td>" +
//	 			   	"<td>"+content.seaGrass[x].province+"</td>" +
//	 			   	"<td>"+content.seaGrass[x].municipality+"</td>" +
//	 			   	"<td>"+content.seaGrass[x].barangay+"</td>" +
////	 			   	"<td>"+content.seaGrass[x].culturedMethodUsed+"</td>" +
//	 			   	"<td>"+content.seaGrass[x].lat+","+content.seaGrass[x].lon+"</td>" +
//	 			  // "<td class='map menu_links btn-primary' data-href="+ content.seaGrass[x].uniqueKey + ">MAP</td>" +
//				//	"<td class='view menu_links btn-primary' data-href="+ content.seaGrass[x].uniqueKey + ">View</td>" +
//	 			   	"</tr>";
//				}
//				$("#seagrass-table").find('tbody').empty();
//				$("#seagrass-table").find('tbody').append(markup);
//			$("#seagrass_paging").empty();		
//			$("#seagrass_paging").append(content.pageNumber);	
//		  });
//}
//function getSeagrassPerPage(){   
//
//	  $('#seagrass_paging').delegate('a', 'click' , function(){
//		  var $this = $(this),
//       target = $this.data('target');    
//        
//			 $.get("/admin/seagrass_admin/"+ region_id +"/" + target, function(content, status){
//				 
//				 var markup = "";
//			 			for ( var x = 0; x < content.seaGrass.length; x++) {
//			 			   //console.log(content.seaGrass[x].id);
//			 			   markup +="<tr>" +
//			 			  "<td><span>" + content.seaGrass[x].region + "</span>" +
//							"</td>" +
//			 			   	"<td>"+content.seaGrass[x].province+"</td><td>"+content.seaGrass[x].municipality+"</td><td>"+content.seaGrass[x].barangay+"</td><td>"+content.seaGrass[x].culturedMethodUsed+"</td><td>"+content.seaGrass[x].lat+","+content.seaGrass[x].lon+"</td>" +
//			 			  // "<td class='map menu_links btn-primary' data-href="+ content.seaGrass[x].uniqueKey + ">MAP</td>" +
//							//"<td class='view menu_links btn-primary' data-href="+ content.seaGrass[x].uniqueKey + ">View</td>" +
//			 			   	"</tr>";
//						}
//						$("#seagrass-table").find('tbody').empty();
//						$("#seagrass-table").find('tbody').append(markup);
//					$("#seagrass_paging").empty();		
//					$("#seagrass_paging").append(content.pageNumber);	
//				  });
//     });       
//  }
//function getSeaGrassList(){			
//	$.get("/admin/getAllSeagrassList/"+ region_id, function(content, status){
//
//	        	 var markup = "";
//	        	 var tr;
//	       
//	         			for ( var x = 0; x < content.length; x++) {
//	         			region = content[x].region;
//	         			
//	         			 if(content.length - 1 === x) {
//	 				        $(".exportToExcel").show();
//	 				    }
//	 				
//						tr = $('<tr/>');
//						
//						tr.append("<td>" + content[x].province + "</td>");
//						tr.append("<td>" + content[x].municipality + "</td>");
//						tr.append("<td>" + content[x].barangay + "</td>");
//						tr.append("<td>" + content[x].code + "</td>");
//						tr.append("<td>" + content[x].area + "</td>");
//						tr.append("<td>" + content[x].indicateGenus + "</td>");
////						tr.append("<td>" + content[x].culturedMethodUsed + "</td>");
////						tr.append("<td>" + content[x].culturedPeriod + "</td>");
////						tr.append("<td>" + content[x].volumePerCropping + "</td>");
////						tr.append("<td>" + content[x].noOfCroppingPerYear + "</td>");
//						tr.append("<td>" + content[x].dataSource + "</td>");
//						tr.append("<td>" + content[x].lat + "</td>");
//						tr.append("<td>" + content[x].lon + "</td>");
//						tr.append("<td>" + content[x].remarks + "</td>");
//						
//						$('#seagrass_body').append(tr);	
//						//ExportTable();
//						}    		
//	    		  });
//}
//
//
//function getSeaweedsDefaultPage(){
//
//	 $.get("/admin/seaweeds_admin/"+ region_id +"/" + 0, function(content, status){
//  	  
//   	 var markup = "";
//    			for ( var x = 0; x < content.seaweeds.length; x++) {
//    			  markup +="<tr>" +
//    			 "<td><span>" + content.seaweeds[x].region + "</span>" +
//					"</td>" +
//    			  				"<td>"+content.seaweeds[x].province+"</td><td>"+content.seaweeds[x].municipality+"</td><td>"+content.seaweeds[x].barangay+"</td><td>"+content.seaweeds[x].culturedMethodUsed+"</td><td>"+content.seaweeds[x].lat+","+content.seaweeds[x].lon+"</td>" +
//    			  				//"<td class='map menu_links btn-primary' data-href="+ content.seaweeds[x].uniqueKey + ">MAP</td>" +
//    							//"<td class='view menu_links btn-primary' data-href="+ content.seaweeds[x].uniqueKey + ">View</td>" +		
//    			  				"</tr>";
//				}
//				$("#seaweeds-table").find('tbody').empty();
//				$("#seaweeds-table").find('tbody').append(markup);
//			$("#seaweeds_paging").empty();		
//			$("#seaweeds_paging").append(content.pageNumber);	
//		  });
//}
//function getSeaweedsList(){			
//	$.get("/admin/getAllSeaweedsList/" + region_id, function(content, status){
//	        	
//	        	 var markup = "";
//	        	 var tr;
//	       
//	         			for ( var x = 0; x < content.length; x++) {
//	         			region = content[x].region;
//	         			
//	         			 if(content.length - 1 === x) {
//	  				        //console.log('loop ends');
//	  				        $(".exportToExcel").show();
//	  				    }
//	  				
//	         			
//						tr = $('<tr/>');
//						tr.append("<td>" + content[x].region + "</td>");						
//						tr.append("<td>" + content[x].province + "</td>");
//						tr.append("<td>" + content[x].municipality + "</td>");
//						tr.append("<td>" + content[x].barangay + "</td>");
//						tr.append("<td>" + content[x].code + "</td>");
//						tr.append("<td>" + content[x].area + "</td>");
//						tr.append("<td>" + content[x].indicateGenus + "</td>");
//						tr.append("<td>" + content[x].culturedMethodUsed + "</td>");
//						tr.append("<td>" + content[x].culturedPeriod + "</td>");
//						tr.append("<td>" + content[x].productionKgPerCropping + "</td>");
//						tr.append("<td>" + content[x].productionNoOfCroppingPerYear + "</td>");
//						tr.append("<td>" + content[x].dataSource + "</td>");
//						tr.append("<td>" + content[x].lat + "</td>");
//						tr.append("<td>" + content[x].lon + "</td>");
//						tr.append("<td>" + content[x].remarks + "</td>");
//						
//						$('#seaweeds_body').append(tr);	
//						//ExportTable();
//						}    		
//	    		  });
//	}
//function getSeaweedsPerPage(){   
//	  $('#seaweeds_paging').delegate('a', 'click' , function(){
//	  
//      var $this = $(this),
//         target = $this.data('target');    
//         
//      $.get("/admin/seaweeds_admin/"+ region_id +"/" + target, function(content, status){
//       	  
//     	 var markup = "";
//      			for ( var x = 0; x < content.seaweeds.length; x++) {
//      			  markup +="<tr>" +
//      			  "<td><span>" + content.seaweeds[x].region + "</span>" +
//						"</td>" +
//      			  				"<td>"+content.seaweeds[x].province+"</td><td>"+content.seaweeds[x].municipality+"</td><td>"+content.seaweeds[x].barangay+"</td><td>"+content.seaweeds[x].culturedMethodUsed+"</td><td>"+content.seaweeds[x].lat+","+content.seaweeds[x].lon+"</td>" +
//      			  				//"<td class='map menu_links btn-primary' data-href="+ content.seaweeds[x].uniqueKey + ">MAP</td>" +
//       							//"<td class='view menu_links btn-primary' data-href="+ content.seaweeds[x].uniqueKey + ">View</td>" +		
//      			  				"</tr>";
// 				}
// 				$("#seaweeds-table").find('tbody').empty();
// 				$("#seaweeds-table").find('tbody').append(markup);
// 			$("#seaweeds_paging").empty();		
// 			$("#seaweeds_paging").append(content.pageNumber);	
// 		  });
//    });
//    
//
// }

function getTrainingDefaultPage(){

	 $.get("/admin/trainings_admin/"+ region_id +"/" + 0, function(content, status){
  	  
var markup = "";
		for ( var x = 0; x < content.trainingCenter.length; x++) {
			//console.log("/admin/trainings_admin/" + content.trainingCenter[x].region);
		   markup +="<tr>" +
		   		"<td><span>" + content.trainingCenter[x].region + "</span></td>" +
			   		"<td>"+content.trainingCenter[x].province+"</td>" +
			 		"<td>"+content.trainingCenter[x].municipality+"</td>" +
			 		"<td>"+content.trainingCenter[x].barangay+"</td>" +
			 		"<td>"+content.trainingCenter[x].name+"</td>" +
			 		"<td>"+content.trainingCenter[x].lat+","+content.trainingCenter[x].lon+"</td>" +
			 		//"<td class='map menu_links btn-primary' data-href="+ content.trainingCenter[x].uniqueKey + ">MAP</td>" +
			 		//"<td class='view menu_links btn-primary' data-href="+ content.trainingCenter[x].uniqueKey + ">View</td>" +
			 		"</tr>";
		}
		$("#tableList").find('tbody').empty();
		$("#tableList").find('tbody').append(markup);
	$("#training_paging").empty();		
	$("#training_paging").append(content.pageNumber);	
 });
}
function getTrainingList(){
	$.get("/admin/getAllTrainingcenterList/" + region_id, function(content, status){
		    	 
	        	 var markup = "";
	        	 var tr;
	       
	         			for ( var x = 0; x < content.length; x++) {
	         			region = content[x].region;
	         			
	         			 if(content.length - 1 === x) {
	 				        $(".exportToExcel").show();
	 				    }
	 				
	         			
						tr = $('<tr/>');
						tr.append("<td>" + content[x].region + "</td>");
						tr.append("<td>" + content[x].province + "</td>");
						tr.append("<td>" + content[x].municipality + "</td>");
						tr.append("<td>" + content[x].barangay + "</td>");
						tr.append("<td>" + content[x].code + "</td>");
						tr.append("<td>" + content[x].name + "</td>");
						tr.append("<td>" + content[x].specialization + "</td>");
						tr.append("<td>" + content[x].facilityWithinTheTrainingCenter + "</td>");
						tr.append("<td>" + content[x].type + "</td>");
						tr.append("<td>" + content[x].dataSource + "</td>");
						tr.append("<td>" + content[x].lat + "</td>");
						tr.append("<td>" + content[x].lon + "</td>");
						tr.append("<td>" + content[x].remarks + "</td>");
						
						$('#training_body').append(tr);	
						//ExportTable();
						}    		
	    		  });
	}
function getTrainingCenterPerPage(){   
	    
		  $('#training_paging').delegate('a', 'click' , function(){
	  	  
	        var $this = $(this),
	           target = $this.data('target');    
	        	
	        $.get("/admin/trainings_admin/"+ region_id +"/" + target, function(content, status){
	         	  
	       	 var markup = "";
	       			for ( var x = 0; x < content.trainingCenter.length; x++) {
	       			   markup +="<tr>" +
	       			   		"<td><span>" + content.trainingCenter[x].region + "</span></td>" +
	       			   		"<td>"+content.trainingCenter[x].province+"</td>" +
	       			 		"<td>"+content.trainingCenter[x].municipality+"</td>" +
	       			 		"<td>"+content.trainingCenter[x].barangay+"</td>" +
	       			 		"<td>"+content.trainingCenter[x].name+"</td><td>"+content.trainingCenter[x].lat+","+content.trainingCenter[x].lon+"</td>" +
	       			 		//"<td class='map menu_links btn-primary' data-href="+ content.trainingCenter[x].uniqueKey + ">MAP</td>" +
	       			 		//"<td class='view menu_links btn-primary' data-href="+ content.trainingCenter[x].uniqueKey + ">View</td>" +
	       			 		"</tr>";
	       			}
	       			$("#tableList").find('tbody').empty();
	       			$("#tableList").find('tbody').append(markup);
	       		$("#training_paging").empty();		
	       		$("#training_paging").append(content.pageNumber);	
	       	  });
	      });
	   }




