//$uery(document).ready(function($) {
var LeafIconMarker = L.Icon.extend({
		options: {
			iconSize:[20,30]
		}
});

var FMAJSON1;
var FMAJSON2;
var FMAJSON3;
var FMAJSON4;
var FMAJSON5;
var FMAJSON6;
var FMAJSON7;
var FMAJSON8;
var FMAJSON9;
var FMAJSON10;
var FMAJSON11;
var FMAJSON12;
var MUNICIPALJSON;
var FISHINGGROUND;

var Lingayen_Gulf;
var Moro_Gulf;
var Davao_Gulf;
var Samar_Sea;
var Sibuyan_Sea;
var Camotes_Sea;
var Visayan_Sea;
var Guimaras_Strait;
var Bohol_Sea;
var Ragay_Gulf;
var Leyte_Gulf;
var Manila_Bay;
var Lagonoy_Gulf;
var Lamon_Bay;
var Casiguran_Sound;
var Palanan_Bay;
var Babuyan_Channel;
var Batangas_Coast;
var Tayabas_Bay;
var W_Palawan_waters;
var Cuyo_Pass;
var W_Sulu_Sea;
var S_Sulu_Sea;
var E_Sulu_Sea;

var RIVERS = new L.LayerGroup();
var LAKES = new L.LayerGroup();
var BAYS = new L.LayerGroup();
var Straits = new L.LayerGroup();
var Channels = new L.LayerGroup();
var Gulfs = new L.LayerGroup();
var Sea = new L.LayerGroup();

var PONDJSON;
//var penicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishpen.png"});
//var FPEN = new L.LayerGroup();

var checked;

$("#region1").change(function(){
	  var $region = $( this );
	  var region = $region.attr( "value" );
	  
		if($region.is(":checked")){
			console.log(region);
			getRegion1();

	}else{
		//map.removeLayer(FMAJSON1);
	
	}
	  
});

function getRegion1(){
	map.spin(true);
	$.getJSON("/create/getGeoJsonRegion1",function(data){
		console.log(data);
		
		setTimeout(function () {
		L.geoJSON(data, {
		  	pointToLayer: pointToLayer,
		  	onEachFeature: featuresByResources
		})
		 map.spin(false);
		}, 3000);
	});
	}

//function getFishPen(){
//	map.spin(true);
//	$.getJSON("/create/getFishPenGeoJson",function(data){
//		console.log(data);
//		
//		setTimeout(function () {
//		L.geoJSON(data, {
//		  	pointToLayer: function(feature, latlng){
//		  		if(feature.properties.resources == "fishpen"){		
//		  		  return L.marker(latlng,{icon: penicon});
//		  		  }
//		  	},
//		  	onEachFeature: function(feature, layer){
//		  		if(feature.properties.resources == "fishpen"){
//		  			 FPEN.addLayer(layer.bindPopup(feature.properties.Information));
//		  				//layer.getPopup().on('remove',clickZoomOut);
//		  				//console.log("fishprocessingplants" + feature.properties.Region);
//		  		  }
//		  	}
//		})
//		 map.spin(false);
//		}, 3000);
//		
//	});
//	}






function getFMA1(){
	$("#modal_fm2").modal("hide");	
	 $("#modal_fm3").modal("hide");
	  $("#modal_fm4").modal("hide");
	  $("#modal_fm5").modal("hide");
	  $("#modal_fm6").modal("hide");
	  $("#modal_fm7").modal("hide");
	  $("#modal_fm8").modal("hide");
	  $("#modal_fm9").modal("hide");
	  $("#modal_fm10").modal("hide");
	  $("#modal_fm11").modal("hide");
	  $("#modal_fm12").modal("hide");
fetch("/static/FMA/FMA_1.geojson").then(res => res.json()).then(data => {
		       		   FMAJSON1 = L.geoJson(data,{
		       				
		       				 onEachFeature: function (feature, layer) {
		       					 
		       					 //layer.setStyle(getFMAStyle);
		       					layer.setStyle(setStyleColor('#00BFFF'));
		       					layer.bindTooltip(
		       							feature.properties.Name,
		       							{
		       						permanent:true,
		       						direction:'top',
		       						sticky: true,
		       						offset: [-10, 0],
		       						opacity: 0.10,
		       						className: 'leaflet-tooltip-own' 	
		       							}); 
		       					layer.bindPopup(
		       						// "</br><a href='#' onclick='openFMA1Modal()' data-toggle='collapse' data-target='.navbar-collapse.in' id='description-btn'>" +
		   				          feature.properties.Name
		   				          //"</a>"
		       					);

		  	       		    },
		  	       			 			     	   	       				
		       			});
		       		FMAJSON1.on('click', function(e) {
		       			console.log(e.latlng) 
		       			openFMA1Modal();
		       			});
		       			FMAJSON1.addTo(map);
		       		
		       		  });
}
function onMarkerClickFixedChart(e) {
	  
	}
function openFMA1Modal() {
	  $("#modal_fm1").modal("show");

	}
function getFMA2(){
	 $("#modal_fm1").modal("hide");
	 $("#modal_fm3").modal("hide");
	  $("#modal_fm4").modal("hide");
	  $("#modal_fm5").modal("hide");
	  $("#modal_fm6").modal("hide");
	  $("#modal_fm7").modal("hide");
	  $("#modal_fm8").modal("hide");
	  $("#modal_fm9").modal("hide");
	  $("#modal_fm10").modal("hide");
	  $("#modal_fm11").modal("hide");
	  $("#modal_fm12").modal("hide");
	fetch("/static/FMA/FMA_2.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		FMAJSON2 = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#FFC0CB'));
				 layer.bindTooltip(
							//"<div style='background:white; padding:1px 3px 1px 3px'><b>" + feature.properties.Name + "</b></div>", 
						 feature.properties.Name,
							{
						permanent:true,
						direction:'top',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(
						 //"</br><a href='#' onclick='openFMA2Modal()' data-toggle='collapse' data-target='.navbar-collapse.in' id='description-btn'>" +
				          feature.properties.Name 
				          //"</a>"
				          );
 		    },
 			 			     	   	       				
		});
		FMAJSON2.on('click', function(e) {
   			console.log(e.latlng) 
   			openFMA2Modal();
   			});
		FMAJSON2.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
}
function openFMA2Modal() {
	  $("#modal_fm2").modal("show");
	}
function getFMA3(){
	$("#modal_fm1").modal("hide");
	$("#modal_fm2").modal("hide");
	  $("#modal_fm4").modal("hide");
	  $("#modal_fm5").modal("hide");
	  $("#modal_fm6").modal("hide");
	  $("#modal_fm7").modal("hide");
	  $("#modal_fm8").modal("hide");
	  $("#modal_fm9").modal("hide");
	  $("#modal_fm10").modal("hide");
	  $("#modal_fm11").modal("hide");
	  $("#modal_fm12").modal("hide");
	fetch("/static/FMA/FMA_3.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded

		FMAJSON3 = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#87CEFA'));
				 layer.bindTooltip(
							feature.properties.Name, 
							{
						permanent:true,
						direction:'top',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(
						 //"</br><a href='#' onclick='openFMA3Modal()' data-toggle='collapse' data-target='.navbar-collapse.in' id='description-btn'>" +
				          feature.properties.Name
				          //"</a>"
				          );
 		    },
 			 			     	   	       				
		});
		FMAJSON3.on('click', function(e) {
   			console.log(e.latlng) 
   			openFMA3Modal();
   			});
		FMAJSON3.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
}
function openFMA3Modal() {
	  $("#modal_fm3").modal("show");
	}
function getFMA4(){
	$("#modal_fm1").modal("hide");
	$("#modal_fm2").modal("hide");
	 $("#modal_fm3").modal("hide");
	  $("#modal_fm5").modal("hide");
	  $("#modal_fm6").modal("hide");
	  $("#modal_fm7").modal("hide");
	  $("#modal_fm8").modal("hide");
	  $("#modal_fm9").modal("hide");
	  $("#modal_fm10").modal("hide");
	  $("#modal_fm11").modal("hide");
	  $("#modal_fm12").modal("hide");
	fetch("/static/FMA/FMA_4.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		FMAJSON4 = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#FF1493'));
				 layer.bindTooltip(
							feature.properties.Name, 
							{
						permanent:true,
						direction:'top',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(
						 //"</br><a href='#' onclick='openFMA4Modal()' data-toggle='collapse' data-target='.navbar-collapse.in' id='description-btn'>" +
				          feature.properties.Name 
				          //"</a>"
				          );
 		    },
 			 			     	   	       				
		});
		FMAJSON4.on('click', function(e) {
   			console.log(e.latlng) 
   			openFMA4Modal();
   			});
		FMAJSON4.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
}
function openFMA4Modal() {
	  $("#modal_fm4").modal("show");
	}
function getFMA5(){
	$("#modal_fm1").modal("hide");
	$("#modal_fm2").modal("hide");
	 $("#modal_fm3").modal("hide");
	  $("#modal_fm4").modal("hide");
	  $("#modal_fm6").modal("hide");
	  $("#modal_fm7").modal("hide");
	  $("#modal_fm8").modal("hide");
	  $("#modal_fm9").modal("hide");
	  $("#modal_fm10").modal("hide");
	  $("#modal_fm11").modal("hide");
	  $("#modal_fm12").modal("hide");
	fetch("/static/FMA/FMA_5.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded

		FMAJSON5 = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#8A2BE2'));
				 layer.bindTooltip(
							feature.properties.Name, 
							{
						permanent:true,
						direction:'top',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(
						 
						// "</br><a href='#' onclick='openFMA5Modal()' data-toggle='collapse' data-target='.navbar-collapse.in' id='description-btn'>" +
				          feature.properties.Name 
				          //"</a>"
				         
						 );
 		    },
 			 			     	   	       				
		});
		FMAJSON5.on('click', function(e) {
   			console.log(e.latlng) 
   			openFMA5Modal();
   			});
		FMAJSON5.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
}
function openFMA5Modal() {
	  $("#modal_fm5").modal("show");
	}

function getFMA6(){
	$("#modal_fm1").modal("hide");
	$("#modal_fm2").modal("hide");
	 $("#modal_fm3").modal("hide");
	  $("#modal_fm4").modal("hide");
	  $("#modal_fm5").modal("hide");
	  $("#modal_fm7").modal("hide");
	  $("#modal_fm8").modal("hide");
	  $("#modal_fm9").modal("hide");
	  $("#modal_fm10").modal("hide");
	  $("#modal_fm11").modal("hide");
	  $("#modal_fm12").modal("hide");
	fetch("/static/FMA/FMA_6.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		FMAJSON6 = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#F0E68C'));
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'top',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(
						 //"</br><a href='#' onclick='openFMA6Modal()' data-toggle='collapse' data-target='.navbar-collapse.in' id='description-btn'>" +
				          feature.properties.Name 
				          //"</a>"
				          );
 		    },
 			 			     	   	       				
		});
		FMAJSON6.on('click', function(e) {
   			console.log(e.latlng) 
   			openFMA6Modal();
   			});
		FMAJSON6.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
}
function openFMA6Modal() {
	  $("#modal_fm6").modal("show");
	}

function getFMA7(){
	$("#modal_fm1").modal("hide");
	$("#modal_fm2").modal("hide");
	 $("#modal_fm3").modal("hide");
	  $("#modal_fm4").modal("hide");
	  $("#modal_fm5").modal("hide");
	  $("#modal_fm6").modal("hide");
	  $("#modal_fm8").modal("hide");
	  $("#modal_fm9").modal("hide");
	  $("#modal_fm10").modal("hide");
	  $("#modal_fm11").modal("hide");
	  $("#modal_fm12").modal("hide");
	fetch("/static/FMA/FMA_7.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		FMAJSON7 = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#FFDAB9'));
				 layer.bindTooltip(
							feature.properties.Name, 
							{
						permanent:true,
						direction:'top',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(
						 //"</br><a href='#' onclick='openFMA7Modal()' data-toggle='collapse' data-target='.navbar-collapse.in' id='description-btn'>" +
				          feature.properties.Name 
				          //"</a>"
				          );
 		    },
 			 			     	   	       				
		});
		FMAJSON7.on('click', function(e) {
   			console.log(e.latlng) 
   			openFMA7Modal();
   			});
		FMAJSON7.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
}
function openFMA7Modal() {
	  $("#modal_fm7").modal("show");
	}

function getFMA8(){
	$("#modal_fm1").modal("hide");
	$("#modal_fm2").modal("hide");
	 $("#modal_fm3").modal("hide");
	  $("#modal_fm4").modal("hide");
	  $("#modal_fm5").modal("hide");
	  $("#modal_fm6").modal("hide");
	  $("#modal_fm7").modal("hide");
	  $("#modal_fm9").modal("hide");
	  $("#modal_fm10").modal("hide");
	  $("#modal_fm11").modal("hide");
	  $("#modal_fm12").modal("hide");
	fetch("/static/FMA/FMA_8.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		FMAJSON8 = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#FF00FF'));
				 layer.bindTooltip(
							feature.properties.Name, 
							{
						permanent:true,
						direction:'top',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(
						 //"</br><a href='#' onclick='openFMA8Modal()' data-toggle='collapse' data-target='.navbar-collapse.in' id='description-btn'>" +
				          feature.properties.Name 
				          //"</a>"
				          );
 		    },
 			 			     	   	       				
		});
		FMAJSON8.on('click', function(e) {
   			console.log(e.latlng) 
   			openFMA8Modal();
   			});
		FMAJSON8.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
}
function openFMA8Modal() {
	  $("#modal_fm8").modal("show");
	}

function getFMA9(){
	$("#modal_fm1").modal("hide");
	$("#modal_fm2").modal("hide");
	 $("#modal_fm3").modal("hide");
	  $("#modal_fm4").modal("hide");
	  $("#modal_fm5").modal("hide");
	  $("#modal_fm6").modal("hide");
	  $("#modal_fm7").modal("hide");
	  $("#modal_fm8").modal("hide");
	  $("#modal_fm10").modal("hide");
	  $("#modal_fm11").modal("hide");
	  $("#modal_fm12").modal("hide");
	fetch("/static/FMA/FMA_9.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
	
		FMAJSON9 = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#FFFFFF'));
				 layer.bindTooltip(
						feature.properties.Name, 
							{
						permanent:true,
						direction:'top',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(
						 //"</br><a href='#' onclick='openFMA9Modal()' data-toggle='collapse' data-target='.navbar-collapse.in' id='description-btn'>" +
				          feature.properties.Name
				          //"</a>"
				          );
 		    },
 			 			     	   	       				
		});
		FMAJSON9.on('click', function(e) {
   			console.log(e.latlng) 
   			openFMA9Modal();
   			});
		FMAJSON9.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
}
function openFMA9Modal() {
	  $("#modal_fm9").modal("show");
	}

function getFMA10(){
	$("#modal_fm1").modal("hide");
	$("#modal_fm2").modal("hide");
	 $("#modal_fm3").modal("hide");
	  $("#modal_fm4").modal("hide");
	  $("#modal_fm5").modal("hide");
	  $("#modal_fm6").modal("hide");
	  $("#modal_fm7").modal("hide");
	  $("#modal_fm8").modal("hide");
	  $("#modal_fm9").modal("hide");
	  $("#modal_fm11").modal("hide");
	  $("#modal_fm12").modal("hide");
fetch("/static/FMA/FMA_10.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded

		FMAJSON10 = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#1E90FF'));
				 layer.bindTooltip(
							feature.properties.Name, 
							{
						permanent:true,
						direction:'top',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(
						 //"</br><a href='#' onclick='openFMA10Modal()' data-toggle='collapse' data-target='.navbar-collapse.in' id='description-btn'>" +
				          feature.properties.Name
				          //"</a>"
				          );
 		    },
 			 			     	   	       				
		});
		FMAJSON10.on('click', function(e) {
   			console.log(e.latlng) 
   			openFMA10Modal();
   			});
		FMAJSON10.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
}
function openFMA10Modal() {
	  $("#modal_fm10").modal("show");
	}

function getFMA11(){
	$("#modal_fm1").modal("hide");
	$("#modal_fm2").modal("hide");
	 $("#modal_fm3").modal("hide");
	  $("#modal_fm4").modal("hide");
	  $("#modal_fm5").modal("hide");
	  $("#modal_fm6").modal("hide");
	  $("#modal_fm7").modal("hide");
	  $("#modal_fm8").modal("hide");
	  $("#modal_fm9").modal("hide");
	  $("#modal_fm10").modal("hide");
	  $("#modal_fm12").modal("hide");
	fetch("/static/FMA/FMA_11.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded

		FMAJSON11 = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#DC143C'));
				 layer.bindTooltip(
							feature.properties.Name, 
							{
						permanent:true,
						direction:'top',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				layer.bindPopup(
						//"</br><a href='#' onclick='openFMA11Modal()' data-toggle='collapse' data-target='.navbar-collapse.in' id='description-btn'>" +
				          feature.properties.Name
				          //"</a>"
				          );
 		    },
 			 			     	   	       				
		});
		FMAJSON11.on('click', function(e) {
   			console.log(e.latlng) 
   			openFMA11Modal();
   			});
		FMAJSON11.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
}
function openFMA11Modal() {
	  $("#modal_fm11").modal("show");
	}

function getFMA12(){
	$("#modal_fm1").modal("hide");
	$("#modal_fm2").modal("hide");
	 $("#modal_fm3").modal("hide");
	  $("#modal_fm4").modal("hide");
	  $("#modal_fm5").modal("hide");
	  $("#modal_fm6").modal("hide");
	  $("#modal_fm7").modal("hide");
	  $("#modal_fm8").modal("hide");
	  $("#modal_fm9").modal("hide");
	  $("#modal_fm10").modal("hide");
	  $("#modal_fm11").modal("hide");
	fetch("/static/FMA/FMA_12.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded

		FMAJSON12 = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#0000CD'));
				 layer.bindTooltip(
							feature.properties.Name, 
							{
						permanent:true,
						direction:'top',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(
						 //"</br><a href='#' onclick='openFMA12Modal()' data-toggle='collapse' data-target='.navbar-collapse.in' id='description-btn'>" +
				          feature.properties.Name
				          //"</a>"
				          );
 		    },
 			 			     	   	       				
		});
		FMAJSON12.on('click', function(e) {
   			console.log(e.latlng) 
   			openFMA12Modal();
   			});
		FMAJSON12.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
}
function openFMA12Modal() {
	  $("#modal_fm12").modal("show");
	}

function getMunicipalWaters(){
	

fetch("/static/geojson/Maris_Municipal_Waters.geojson").then(res => res.json()).then(data => {
    // add FMAJSON layer to the map once the file is loaded
	
	MUNICIPALJSON = L.geoJson(data,{
		
		 onEachFeature: function (feature, layer) {
			 layer.setStyle(setStyleColor('#244A18'));
			// layer.bindTooltip(feature.properties.Name, {permanent:true,direction:'center',className: 'countryLabel'});
		    },
			 			     	   	       				
	});
	MUNICIPALJSON.addTo(map);
	//map.fitBounds(FMAJSON.getBounds());
  });
}
function get24FishingGrounds(){
	

	fetch("/static/FMA/24_Fishing_Grounds.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		FISHINGGROUND = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(layer.setStyle({fillColor :'green'}) );
				 layer.bindTooltip(feature.properties.Name, {permanent:true,direction:'center',className: 'countryLabel'});
			    },
				 			     	   	       				
		});
		FISHINGGROUND.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}



function getLingayen_Gulf(){
	

	fetch("/static/FG/1_Lingayen_Gulf.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Lingayen_Gulf = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [5, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Lingayen_Gulf.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}

function getLeyte_Gulf(){
	

	fetch("/static/FG/19_Leyte_Gulf.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Leyte_Gulf = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Leyte_Gulf.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}

function getManila_Bay(){
	

	fetch("/static/FG/2_Manila_Bay.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Manila_Bay = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Manila_Bay.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}

function getLagonoy_Gulf(){
	

	fetch("/static/FG/20_Lagonoy_Gulf.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Lagonoy_Gulf = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Lagonoy_Gulf.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}

function getLamon_Bay(){
	

	fetch("/static/FG/21_Lamon_Bay.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Lamon_Bay = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Lamon_Bay.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}

function getCasiguran_Sound(){
	

	fetch("/static/FG/22_Casiguran_Sound.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Casiguran_Sound = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Casiguran_Sound.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}

function getPalanan_Bay(){
	

	fetch("/static/FG/23_Palanan_Bay.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Palanan_Bay = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Palanan_Bay.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}

function getBabuyan_Channel(){
	

	fetch("/static/FG/24_Babuyan_Channel.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Babuyan_Channel = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Babuyan_Channel.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}

function getBatangas_Coast(){
	

	fetch("/static/FG/3_Batangas_Coast.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Batangas_Coast = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Batangas_Coast.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}


function getTayabas_Bay(){
	

	fetch("/static/FG/4_Tayabas_Bay.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Tayabas_Bay = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Tayabas_Bay.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}


function getW_Palawan_waters(){
	

	fetch("/static/FG/5_W_Palawan_waters.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		W_Palawan_waters = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		W_Palawan_waters.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}

function getCuyo_Pass(){
	

	fetch("/static/FG/6_Cuyo_Pass.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Cuyo_Pass = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Cuyo_Pass.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}

function getW_Sulu_Sea(){
	

	fetch("/static/FG/7_W_Sulu_Sea.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		W_Sulu_Sea = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		W_Sulu_Sea.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}


function getS_Sulu_Sea(){
	

	fetch("/static/FG/8_S_Sulu_Sea.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		S_Sulu_Sea = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		S_Sulu_Sea.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}


function getE_Sulu_Sea(){
	

	fetch("/static/FG/9_E_Sulu_Sea.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		E_Sulu_Sea = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		E_Sulu_Sea.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}
function getMoro_Gulf(){
	

	fetch("/static/FG/10_Moro_Gulf.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Moro_Gulf = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Moro_Gulf.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}
function getDavao_Gulf(){
	

	fetch("/static/FG/11_Davao_Gulf.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Davao_Gulf = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Davao_Gulf.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}
function getSamar_Sea(){
	

	fetch("/static/FG/12_Samar_Sea.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Samar_Sea = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Samar_Sea.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}
function getSibuyan_Sea(){
	

	fetch("/static/FG/13_Sibuyan_Sea.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Sibuyan_Sea = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Sibuyan_Sea.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}
function getCamotes_Sea(){
	

	fetch("/static/FG/14_Camotes_Sea.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Camotes_Sea = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Camotes_Sea.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}
function getVisayan_Sea(){
	

	fetch("/static/FG/15_Visayan_Sea.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Visayan_Sea = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Visayan_Sea.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}
function getGuimaras_Strait(){
	

	fetch("/static/FG/16_Guimaras_Strait.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Guimaras_Strait = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Guimaras_Strait.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}
function getBohol_Sea(){
	

	fetch("/static/FG/17_Bohol_Sea.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Bohol_Sea = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Bohol_Sea.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}
function getRagay_Gulf(){
	

	fetch("/static/FG/18_Ragay_Gulf.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Ragay_Gulf = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(FishingGroundtStyle);
				 layer.bindTooltip(
							 feature.properties.Name, 
							{
						permanent:true,
						direction:'center',
						sticky: true,
						offset: [10, 0],
						opacity: 0.10,
						className: 'leaflet-tooltip-own' 	
							});
				 layer.bindPopup(feature.properties.Name);
			    },
				 			     	   	       				
		});
		Ragay_Gulf.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}

function getRivers(){
	

	fetch("/static/riverLake/River.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		RIVERS = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#0096FF'));
				// layer.bindTooltip(feature.properties.Name, {permanent:true,direction:'center',className: 'countryLabel'});
			    },
				 			     	   	       				
		});
		RIVERS.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}
function getLAKES(){
	

	fetch("/static/riverLake/Lakes.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		LAKES = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#244A18'));
				layer.bindTooltip(feature.properties.NAME, {permanent:true,direction:'center',className: 'countryLabel'});
			    },
				 			     	   	       				
		});
		LAKES.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}
function getBAYS(){
//	BAYS = new L.GeoJSON.AJAX('/static/majorFishingGround/PASSAGES2.geojson', {
//		  onEachFeature: function (feature, layer) {
//			  console.log(layer);
//				 layer.setStyle(setStyleColor('#000000'));
//				// layer.bindTooltip(feature.properties.Name, {permanent:true,direction:'center',className: 'countryLabel'});
//			    },
//		});
	BAYS = new L.GeoJSON.AJAX('/static/majorFishingGround/Burias_Pass2.geojson', {
		  onEachFeature: function (feature, layer) {
			  console.log(layer);
				 layer.setStyle(setStyleColor('#000000'));
				// layer.bindTooltip(feature.properties.Name, {permanent:true,direction:'center',className: 'countryLabel'});
			    },
		});
	BAYS.addTo(map);
//	fetch("/static/majorFishingGround/Bays.geojson").then(res => res.json()).then(data => {
//	   
//		BAYS = L.geoJson(data,{
//			
//			 onEachFeature: function (feature, layer) {
//				 layer.setStyle(setStyleColor('#244A18'));
//				 },
//				 			     	   	       				
//		});
//
//	  });
	}

function getStraits(){
	

	fetch("/static/majorFishingGround/Ticao_Pass2.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Straits = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#244A18'));
				// layer.bindTooltip(feature.properties.Name, {permanent:true,direction:'center',className: 'countryLabel'});
			    },
				 			     	   	       				
		});
		Straits.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}
function getChannels(){
	

	fetch("/static/majorFishingGround/Maqueda_Channel.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Channels = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#244A18'));
				// layer.bindTooltip(feature.properties.Name, {permanent:true,direction:'center',className: 'countryLabel'});
			    },
				 			     	   	       				
		});
		Channels.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}
function getGulfs(){
	

	fetch("/static/majorFishingGround/Gulfs.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Gulfs = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#244A18'));
				// layer.bindTooltip(feature.properties.Name, {permanent:true,direction:'center',className: 'countryLabel'});
			    },
				 			     	   	       				
		});
		Gulfs.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}
function getSea(){
	

	fetch("/static/majorFishingGround/Seas.geojson").then(res => res.json()).then(data => {
	    // add FMAJSON layer to the map once the file is loaded
		
		Sea = L.geoJson(data,{
			
			 onEachFeature: function (feature, layer) {
				 layer.setStyle(setStyleColor('#244A18'));

				// layer.bindTooltip(feature.properties.Name, {permanent:true,direction:'center',className: 'countryLabel'});
			    },
				 			     	   	       				
		});
		Sea.addTo(map);
		//map.fitBounds(FMAJSON.getBounds());
	  });
	}


var FishingGroundtStyle = {
color: '#4D42BC',
weight: 2,
opacity: 0.3,
fillOpacity: 0.2,
fillColor: '#FCFBFD'
};
