function Person(firstName,lastName, dob){
	
	this.firstName = firstName;
	this.lastName = lastName;
	this.dob = dob;
	
	console.log(firstName + ':' + lastName);
	
}

const person = new Person('Francisco', 'Vejerano', '09-20-1980');

console.log(person);