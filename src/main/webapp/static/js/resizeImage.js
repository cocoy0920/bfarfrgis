  function encodeImageFileAsURL(element) {
    console.log(element.files[0].name);
        var file = element.files[0];
        var reader = new FileReader();
        
        reader.onloadend = function() {
      //  var base64result = reader.result.substr(reader.result.indexOf(',') + 1);
      //      document.getElementById("image").value = base64result;
      //      preview.setAttribute('src', reader.result);
                var img = document.createElement("img");
                img.src = reader.result;

                var canvas = document.createElement("canvas");
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);

                var MAX_WIDTH = 400;
                var MAX_HEIGHT = 400;
                var width = img.width;
                var height = img.height;

                if (width > height) {
                    if (width > MAX_WIDTH) {
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                    }
                } else {
                    if (height > MAX_HEIGHT) {
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                    }
                }
                canvas.width = width;
                canvas.height = height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0, width, height);

                dataurl = canvas.toDataURL(file.type);
               // document.getElementById('output').src = dataurl;
               //  output.setAttribute('src', dataurl);
      			 preview.setAttribute('src', dataurl);
      			document.getElementById("image_src").value = dataurl;
        }
        reader.readAsDataURL(file);
        document.getElementById("image_name").value = element.files[0].name;
      
    }

  $(document).ready(function () {
      $('#file').change(function () {
          if (this.files.length > 0) {

              $.each(this.files, function (i, v) {
                  var reader = new FileReader();

                  reader.onload = function (e) {
                      var img = new Image();
                      img.src = e.target.result;

                      img.onload = function () {

                          // CREATE A CANVAS ELEMENT AND ASSIGN THE IMAGES TO IT.
                          var canvas = document.createElement("canvas");

                          var value = $('#size').val();

                          // RESIZE THE IMAGES ONE BY ONE.
                          //img.width = (img.width * value) / 100
                          //img.height = (img.height * value) / 100
                          
                          ///updated
                          var MAX_WIDTH = 400;
              			var MAX_HEIGHT = 400;
              			var width = img.width;
              			var height = img.height;

              			if (width > height) {
                  			if (width > MAX_WIDTH) {
                      		height *= MAX_WIDTH / width;
                      		width = MAX_WIDTH;
                  			}
              			} else {
                  		if (height > MAX_HEIGHT) {
                      	width *= MAX_HEIGHT / height;
                      	height = MAX_HEIGHT;
                  		}
              			}
              canvas.width = width;
              canvas.height = height;
                          ///updated

                          var ctx = canvas.getContext("2d");
                          ctx.clearRect(0, 0, canvas.width, canvas.height);
                          canvas.width = img.width;
                          canvas.height = img.height;
                          ctx.drawImage(img, 0, 0, width, height);
                          
                          dataurl = canvas.toDataURL(file.type);
                          document.getElementById('output_image').src = dataurl;
							output.setAttribute('src', dataurl);
                          $('#img').append(img);      // SHOW THE IMAGES OF THE BROWSER.

                          // AUTO DOWNLOAD THE IMAGES, ONCES RESIZED.
                         // var a = document.createElement('a');
                         // a.href = canvas.toDataURL("image/png");
                         // a.download = 'sample.jpg';
                         // document.body.appendChild(a);
                         // a.click();
                      }
                  };
                  reader.readAsDataURL(this);
              });
          }
      });
  });
