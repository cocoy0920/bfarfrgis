
var ddlOption = "";
var region_id = "all";
var province_id = "all";
var region;
var geoJSON;	
var legentRO;
var map =L.map('map',{scrollWheelZoom:false,fullscreenControl: {pseudoFullscreen: false,position:'topright'}}).setView([14.656098, 121.048466],6);

map.zoomControl.setPosition('topright');

var tiles = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}.png', {
		maxZoom: 18});

//  tiles.addTo(map);
	var Esri_WorldGrayCanvas = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {		
		maxZoom: 15,
		attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'});
	
	//Esri_WorldGrayCanvas.addTo(map);
	
	var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
		maxZoom: 18,
		attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'});
	//osm.addTo(map);
	var lyrGoogleMap = L.tileLayer('http://mts3.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
		maxZoom:22,
		maxNativeZoom:18
	});
	//lyrGoogleMap.addTo(map);
	
	lyrGoogleHybrid = L.tileLayer("http://mts2.google.com/vt/lyrs=y&x={x}&y={y}&z={z}",{
	maxZoom:22,
	maxNativeZoom:18
	});
	//lyrGoogleHybrid.addTo(map);
	var frgis_map = getPhilippineMap();
	
	
	Depth();
	DepthByValue();
	Menu();
	FMA_SEA();	
	getAllCheckByResources();

	/*async function getAll(){
		let response = await fetch('http://localhost:8899/api/all');
		let data = await response.json();
		return data;
	}
	getAll().then(data => console.log(data));*/
 // getAllMap();
	
	 var dropdownlist = L.control({
		    position: 'topright'
		  });
		//	  $.get("/home/getAllRegion", function(content, status){
				 

					  dropdownlist.onAdd = function(map) {
						  var select;
						  var option;
						  var div = L.DomUtil.create('div', 'dropdownlist regions'); 
				  
				    
						  div.innerHTML = '<select id="Regions">'+
						  				'<option value = "all">select Region</option>' +
						  				'<option value = "010000000">Region 1</option>' +
						  				'<option value = "020000000">Region 2</option>' +
						  				'<option value = "030000000">Region 3</option>' +
						  				'<option value = "040000000">Region 4-A</option>' +
						  				'<option value = "050000000">Region 5</option>' +
						  				'<option value = "060000000">Region 6</option>' +
						  				'<option value = "070000000">Region 7</option>' +
						  				'<option value = "080000000">Region 8</option>' +
						  				'<option value = "090000000">Region 9</option>' +
						  				'<option value = "100000000">Region 10</option>' +
						  				'<option value = "110000000">Region 11</option>' +
						  				'<option value = "120000000">Region 12</option>' +
						  				'<option value = "130000000">NCR</option>' +
						  				'<option value = "140000000">CAR</option>' +
						  				'<option value = "150000000">ARMM</option>' +
						  				'<option value = "160000000">CARAGA</option>' +
						  				'<option value = "170000000">Region 4-B</option>' +
						  				'</select>'
						  					
				    	
				    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
				    return div;
				  };

				 // dropdownlist.addTo(map);  

		  $('#Regions').change(function(){
			  console.log();
 				clearMap();
 				unchecked();
 				
		      var ddlOption = $("#Regions option:selected").text();
			    var ddlValue = $("#Regions option:selected").val();
			    
			   // console.log("Regions: " + ddlValue);
			  //  alert('Filtered by Region' + ddlValue);
			    
			    region_id = ddlValue;
			    
			    if(region_id == "all"){
			    	getAllMap();
			    	
			    }else{	

			    	getMapPerRegion(region_id);
			    	
			    }
		  });


	
	
L.control.scale().addTo(map);




function style(feature) {
    return {
        fillColor: 'green', 
        fillOpacity: 0.5,  
        weight: 2,
        opacity: 1,
        color: '#ffffff',
        dashArray: '3'
    };
}

	var highlight = {
		'fillColor': 'yellow',
		'weight': 2,
		'opacity': 1
	};


  		function polySelect(a){
  		console.log("polySelect(a): " + a);
			map._layers[a].fire('click'); 
			var layer = map._layers[a];
			map.fitBounds(layer.getBounds());
        }
  		
  		
  		
      


			$(".se-pre-con").show();
			
			var LamMarkerList = [];
			var group;
			var layerGroup;
	

		    
	        var geojsonMarkerOptions = {
					radius: 8,
					fillColor: "#ff7800",
					color: "#000",
					weight: 1,
					opacity: 1,
					fillOpacity: 0.8
					};
	        
       
var baseMaps = {
			"Open Street Map": osm,
		   	"Esri World Gray Canvas":Esri_WorldGrayCanvas
    /* "Open Street Map": osm,
   	"OSM B&W":OpenStreetMap_BlackAndWhite,
   	"Esri WorldI magery":Esri_WorldImagery,
   	"Esri World Gray Canvas":Esri_WorldGrayCanvas, */
		
/*    "<img src='/static/images/iconCircle/hatcheries.png' style='width: 25px; height: 25px;'/>  Hatchery Complete":HatcheryComplete,
    "<img src='/static/images/iconCircle/hatcheries.png' style='width: 25px; height: 25px;'/> Hatchery Incomplete ":HatcheryinComplete,
    "<img src='/static/images/iconCircle/fishsanctuary.png' style='width: 25px; height: 25px;'/> Fish Sanctuary":FS,
    "<img src='/static/images/iconCircle/fishprocessing.png' style='width: 25px; height: 25px;'/> Fish Processing":FP,
    "<img src='/static/images/iconCircle/fish-landing.png' style='width: 25px; height: 25px;'/> Fish Landing":FL,
    "<img src='/static/images/iconCircle/fishport.png' style='width: 25px; height: 25px;'/> Fish Port":FPT,
    "<img src='/static/images/iconCircle/fishpen.png' style='width: 25px; height: 25px;'/> Fish Pen":FPEN,
    "<img src='/static/images/iconCircle/fishcage.png' style='width: 25px; height: 25px;'/> Fish Cage":FCage,
    "<img src='/static/images/iconCircle/iceplant.png' style='width: 25px; height: 25px;'/> Cold Storage":IPCS,
    "<img src='/static/images/iconCircle/market.png' style='width: 25px; height: 25px;'/> Market":Market,
    "<img src='/static/images/iconCircle/schoolof-fisheries.png' style='width: 25px; height: 25px;'/> School of Fisheries":School,
    "<img src='/static/images/iconCircle/fishcorral.png' style='width: 25px; height: 25px;'/> Fish Corals":Fishcorals,
    "<img src='/static/images/iconCircle/seagrass.png' style='width: 25px; height: 25px;'/> Sea Grass":Seagrass,
    "<img src='/static/images/iconCircle/seaweeds.png' style='width: 25px; height: 25px;'/> Sea Weeds":Seaweeds,
    "<img src='/static/images/iconCircle/mangrove.png' style='width: 25px; height: 25px;'/> Mangrove":Mangrove,
    "<img src='/static/images/iconCircle/lgu.png' style='width: 25px; height: 25px;'/> LGU":LGU,
    "<img src='/static/images/iconCircle/mariculturezone.png' style='width: 25px; height: 25px;'/> Mariculture":Mariculture,
    "<img src='/static/images/iconCircle/fisheriestraining.png' style='width: 25px; height: 25px;'/> Trainingcenter":Trainingcenter*/
};

// var overlayMaps = {
//   "Hatchery Complete":HatcheryComplete,
//   "Hatchery Incomplete ":HatcheryinComplete
//};

//L.control.layers(null,baseMaps,{collapsed:false}).addTo(map);
//L.control.layers(baseMaps).addTo(map);	
		var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});
		
		
		var printer = L.easyPrint({
      		tileLayer: tiles,
      		sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
      		filename: 'myMap',
      		exportOnly: true,
      		position:'topright',
      		hideControlContainer: true
		}).addTo(map);

		function manualPrint () {
			printer.printMap('CurrentSize', 'MyManualPrint')
		}
	

				/* new L.Control.BootstrapModal({
				        modalId: 'modal_help',
				        tooltip: "How to use this thing",
				        glyph: 'question-sign',
				        info:'HELP'
				    }).addTo(map);*/
//				    new L.Control.BootstrapModal({
//				        modalId: 'modal_about',
//				        tooltip: "Fisheries Management Areas",
//				        glyph: 'info-sign',
//				        info:'FMA'
//				    }).addTo(map);
				    
			  function getColor(d) {
				  return d > 5000000 ? '#7a0177' :
				  d > 200000? '#BD0026' :
				  d > 80000? '#E31A1C' :
				  d > 10000? '#FC4E2A' :
				  d > 5000 ? '#FD8D3C' :
				  d > 500 ? '#FEB24C' :
				  d > 0 ? '#FED976' :
				  '#FFEDA0';
				  }
			  
			var clicked;

var clickStyle =  {
  radius: 10,
  fillColor: "#ff0000",
  color: "#ff0000",
  opacity: 1,
  weight: 2,
  fillOpacity: 1,
}
var unclickStyle = {
  radius: 10,
  fillColor: "#09f9df",
  color: "#ff0000",
  weight: 1,
  opacity: 1,
  fillOpacity: 1,
}

var coords;
var pMarker;
function clickOnMarker(e){
	coords = e.latlng;
  if (clicked) {
	  pMarker.remove();
	//  pMarker = new L.circleMarker(coords,unclickStyle);
   // clicked.setStyle(unclickStyle);
  }
  pMarker = new L.circleMarker(coords,clickStyle);
  //e.target.setStyle(clickStyle);
  clicked = e.target;
  pMarker.addTo(map);		      
			      
}

map.on('click', function(e) { 
    
	if (prevLayerClicked !== null) {          
        prevLayerClicked.setStyle(philStyle);
      }
    if(coords){
		  // console.log(coords);
	   }  
	   if (clicked) {
	  	pMarker.remove();
  		}
}); 