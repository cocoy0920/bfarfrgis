	
	var map =L.map('map',{
		scrollWheelZoom:false,
		fullscreenControl: {
	        pseudoFullscreen: false,
	        position:'topright'
	    }
		})//.setView(center,8);
	map.zoomControl.setPosition('topright');
	
	var tiles = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}.png', {
		maxZoom: 18});

	//tiles.addTo(map);

	var OpenTopoMap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
	    maxZoom: 17,
	    attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
	    opacity: 0.90
	  });

	var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
					maxZoom: 18,
					attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'});
		
	var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {	
			 maxZoom: 10,
			attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
		});

	var Esri_WorldGrayCanvas = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {		
			attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'});
			
	var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {	
		attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
		});

	//  tiles.addTo(map);
	
	 // getPhilippineMap();
	// getAllFeaturesByRegion();
	  Depth();
	  DepthByValue();
	  Menu(); 
	  FMA_SEA();
	  getAllCheckByResources();
	  viewMapByRegion();
	//  getMapByRegion();

/* 	var wmsLayer = L.tileLayer.wms('http://geo.bfar.da.gov.ph/gwc/service/wms', {
    layers: 'region:Region_I'
	});
	 */
	
L.control.scale().addTo(map);
/*L.control.bigImage({position: 'topright'}).addTo(map);*/
    
//geojsonLayer.addTo(map);       
 var fishSancturiesGroup = [];
 
 
      	/*var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});*/
			$(".se-pre-con").show();
			
			var LamMarkerList = [];
			var group;
			var layerGroup;
	        

 
	        var result;
			var regionName;
			var cRegion;
			var fRegion;
			var isEquel;
			       		
				       		
			      

        
			  	var baseMaps = {
			  		    "Open Street Map": osm,
			  		   	"Esri WorldI magery":Esri_WorldImagery,
			  		   	"Esri World Gray Canvas":Esri_WorldGrayCanvas,
			  		   	"Topo Map":OpenTopoMap,
			  		   	"Word Street Map":tiles
//		
//    "<img src='/static/images/iconCircle/hatcheries.png' style='width: 25px; height: 25px;'/>  Hatchery Complete":HatcheryComplete,
//    "<img src='/static/images/iconCircle/hatcheries.png' style='width: 25px; height: 25px;'/> Hatchery Incomplete ":HatcheryinComplete,
//    "<img src='/static/images/iconCircle/fishsanctuary.png' style='width: 25px; height: 25px;'/> Fish Sanctuary":FS,
//    "<img src='/static/images/iconCircle/fishprocessing.png' style='width: 25px; height: 25px;'/> Fish Processing":FP,
//    "<img src='/static/images/iconCircle/fish-landing.png' style='width: 25px; height: 25px;'/> Fish Landing":FL,
//    "<img src='/static/images/iconCircle/fishport.png' style='width: 25px; height: 25px;'/> Fish Port":FPT,
//    "<img src='/static/images/iconCircle/fishpen.png' style='width: 25px; height: 25px;'/> Fish Pen":FPEN,
//    "<img src='/static/images/iconCircle/fishcage.png' style='width: 25px; height: 25px;'/> Fish Cage":FCage,
//    "<img src='/static/images/iconCircle/iceplant.png' style='width: 25px; height: 25px;'/> Cold Storage":IPCS,
//    "<img src='/static/images/iconCircle/market.png' style='width: 25px; height: 25px;'/> Market":Market,
//    "<img src='/static/images/iconCircle/schoolof-fisheries.png' style='width: 25px; height: 25px;'/> School of Fisheries":School,
//    "<img src='/static/images/iconCircle/fishcorral.png' style='width: 25px; height: 25px;'/> Fish Corals":Fishcorals,
//    "<img src='/static/images/iconCircle/seagrass.png' style='width: 25px; height: 25px;'/> Sea Grass":Seagrass,
//    "<img src='/static/images/iconCircle/seaweeds.png' style='width: 25px; height: 25px;'/> Sea Weeds":Seaweeds,
//    "<img src='/static/images/iconCircle/mangrove.png' style='width: 25px; height: 25px;'/> Mangrove":Mangrove,
//    "<img src='/static/images/iconCircle/lgu.png' style='width: 25px; height: 25px;'/> LGU":LGU,
//    "<img src='/static/images/iconCircle/mariculturezone.png' style='width: 25px; height: 25px;'/> Mariculture":Mariculture,
//    "<img src='/static/images/iconCircle/fisheriestraining.png' style='width: 25px; height: 25px;'/> Trainingcenter":Trainingcenter
};

			  	L.control.layers(baseMaps).addTo(map);


// var layerscontrol  = L.control.layers(null,baseMaps,{collapsed:false}).addTo(map);
//L.control.layers(baseMaps).addTo(map);	
		var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});

//		L.easyPrint({
//			title: 'My awesome print button',
//			position: 'bottomright',
//			sizeModes: ['A4Portrait', 'A4Landscape'],
//		    hideClasses: ['leaflet-control-zoom']
//		}).addTo(map);
		/*  function getFisheriesData(){
			getAllResources(map_location);
		} */
//		  function clickZoom(e) {
//              map.setView(e.target.getLatLng(),17);
//          }
//   	  function clickZoomOut(e) {
//              map.setView(e.target.getLatLng(),8);
//          }
		var printer = L.easyPrint({
      		tileLayer: tiles,
      		sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
      		filename: 'myMap',
      		exportOnly: true,
      		position:'topright',
      		hideControlContainer: true
		}).addTo(map);

/*		    var centerPoint = [58.0060984592271, 56.2445307230809];*/

		    // Create leaflet map.
		    var baseExportOptions = {
		      caption: {
		        text: 'Карта',
		        font: '30px Arial',
		        fillStyle: 'black',
		        position: [100, 200]
		      }
		    };
		    /*
		    var map = L.map('map', {
		      editable: true,
		      printable: true,
		      downloadable: true,
		    }).setView(centerPoint, 12);
*/
		    // Add OSM layer.
		    /*L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
		      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		    }).addTo(map);*/
		    // Create custom measere tools instances.
		    var measure = L.measureBase(map, {});

		/*    var mimeTypes = map.supportedCanvasMimeTypes();*/
		    var mimeArray = [];
//		    for (var type in mimeTypes) {
//		      mimeArray.push(mimeTypes[type]);
//		    }
//		    document.getElementById('suportedMimeTypes').innerHTML = mimeArray.join(',&nbsp;');

		    function exportMap() {
		      var exportOptions = {
		        container: map._container,
		        caption: {
		          text: 'Карта',
		          font: '30px Arial',
		          fillStyle: 'black',
		          position: [100, 200]
		        },
		        exclude: ['.leaflet-control-zoom', '.leaflet-control-attribution'],
		        format: 'image/jpg'
		      };
		      var exportedlement = map.export(exportOptions).then(
		        result = function (value) {
		          var i = 1;
		        }
		      );
		    }

		    function afterRender(result) {
		      return result;
		    }

		    function afterExport(result) {
		      return result;
		    }

		    function downloadMap(caption) {
		      var downloadOptions = {
		        container: map._container,
		        caption: {
		          text: caption,
		          font: '30px Arial',
		          fillStyle: 'black',
		          position: [100, 200]
		        },
		        exclude: ['.leaflet-control-zoom', '.leaflet-control-attribution'],
		        format: 'image/png',
		        fileName: 'Map.png',
		        afterRender: afterRender,
		        afterExport: afterExport
		      };
		      var promise = map.downloadExport(downloadOptions);
		      var data = promise.then(function (result) {
		        return result;
		      });
		    }

		    function printMap(caption) {
		      var printOptions = {
		        container: map._container,
		        exclude: ['.leaflet-control-zoom'],
		        format: 'image/png',
		        afterRender: afterRender,
		        afterExport: afterExport
		      };
		      printOptions.caption = {
		        text: caption,
		        font: '30px Arial',
		        fillStyle: 'black',
		        position: [50, 50]
		      };
		      var promise = map.printExport(printOptions);
		      var data = promise.then(function (result) {
		        return result;
		      });
		    }


  	// });