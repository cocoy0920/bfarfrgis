function FISH_PROCESSING(){	
var dataListContent=$('#content');
	  dataListContent.empty();
	  dataListContent.append('<h4 class="btn-info">FISH PROCESSING PLANTS</h4>');
	  dataListContent.append('<div style="overflow-x:auto;">');
	  dataListContent.append('<table id="fishprocessingplants" class="table table-striped table-bordered table-hover" style="width:100%">');
      dataListContent.append('<thead>');
	  dataListContent.append('<tr>');
		dataListContent.append('<th>Province</th>');
		dataListContent.append('<th>Municipality</th>');
		 dataListContent.append('<th>Barangay</th>');
		dataListContent.append('<th>Name Of Processing Plants</th>');
		dataListContent.append('<th>Name Of Operator</th>');
		dataListContent.append('<th>Date Encoded</th>');
	      			
		dataListContent.append('<th>Operator Classification</th>');
		dataListContent.append('<th>Indicate Species</th>');
		dataListContent.append('<th>Processing Technique</th>');
		dataListContent.append('<th>Packaging Type</th>');
		dataListContent.append('<th>Processing Environment Classification</th>');
		dataListContent.append('<th>Business Permits and Certification Obtained</th>');
	dataListContent.append('<th>Market Reach</th>');
	dataListContent.append('<th>Code</th>');
	dataListContent.append('<th>Data Source</th>');
	dataListContent.append('<th>Remarks</th>');
	dataListContent.append('<th>Encoded By</th>');
	dataListContent.append('<th>Edited By</th>');
	dataListContent.append('<th>Date Encoded</th>');
	dataListContent.append('<th>Date Edited</th>');
	dataListContent.append('<th>Latitude</th>');
	dataListContent.append('<th>Longitude</th>');
	      			
	dataListContent.append('</tr>');
	dataListContent.append('</thead>');       
	dataListContent.append('</table>');
	 dataListContent.append('</div>');
}