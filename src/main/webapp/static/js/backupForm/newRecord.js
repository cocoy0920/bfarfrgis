
   			
      
    	  function newFishSanctuary(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
        	  
      		 $(".id").val("0");
		    $(".user").val("0");
		    $(".uniqueKey").val("0");
		    $(".dateEncoded").val("0");
		    $(".encodedBy").val("0");
		    
      		
      		slctProvinces.empty();
      		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
      		ProvinceOptions = ProvinceOptions + optionValue;         
      		slctProvinces.append(ProvinceOptions); 
	  		
	  		slctMunicipality.empty();
         			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
         			slctMunicipality.append(MunicipalityOption);
	            
			selectBarangay.empty();
				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
				selectBarangay.append(BarangayOption);

     
        	$(".nameOfFishSanctuary").val("");
        	$(".area").val("");
        	$(".bfardenr").val("");
        	$(".sheltered").val("");
        	$(".type").val("");
        	$(".code").val("");
        	$(".dateAsOf").val("");
        	$(".fishSanctuarySource").val("");
        	$(".remarks").val("");
        	$(".lat").val("");
        	$(".lon").val("");
        	$(".image_src").val("0");
        	$(".image").val("");
        	$(".preview").attr("src", "");
      		 
      		 
      		 $("#contentR").attr("data-target","#myFishSanctuaryModal");
      		// $("#contentList").attr("data-target","#myFishSanctuaryModalList");
      		 }
    	  
    	  function newFishprocessingplants(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);


         
            	$(".nameOfProcessingPlants").val("");
            	$(".nameOfOperator").val("");
            	$(".operatorClassification").val("");
            	$(".processingTechnique").val("");
            	$(".processingEnvironmentClassification").val("");
            	$(".bfarRegistered").val("");          	
            	$(".packagingType").val("");
            	$(".marketReach").val("");
            	$(".indicateSpecies").val("");
            	$(".businessPermitsAndCertificateObtained").val("");
            	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".sourceOfData").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myFishProcessingPlants");
          		// $("#contentList").attr("data-target","#myFishProcessingPlantsList");
          		 
    	  }
    	  
    	  function newFishPen(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfOperator").val("");             
                 	$(".noOfFishPen").val("");
                 	$(".speciesCultured").val("");
                 	$(".croppingStart").val("");
                 	$(".croppingEnd").val("");
                 	
            	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".sourceOfData").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	 $("#contentR").attr("data-target","#myFishPen");
          		 //$("#contentList").attr("data-target","#myFishPenList");
          		 
    	  }
    	  
    	  function newFishLanding(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfLanding").val("");             
                 	$(".classification").val("");
                 	$(".volumeOfUnloadingMT").val("");
                 	$(".type").val("");
                 	$(".croppingEnd").val("");
                 	
            	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".dataSource").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myFishLanding");
    		 	//$("#contentList").attr("data-target","#myFishLandingList");
          		 
    	  }
    	  function newFishCage(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfOperator").val("");             
                 	$(".classificationofOperator").val("");
                 	$(".cageDimension").val("");
                 	$(".cageTotalArea").val("");
                 	$(".cageType").val("");
                 	$(".cageNoOfCompartments").val("");
                 	$(".indicateSpecies").val("");
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".sourceOfData").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myFishCage");
     		 //	$("#contentList").attr("data-target","#myFishCageModalList");
     		 
          		 
    	  }
    	  
    	  function newFishPond(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfFishPondOperator").val("");             
                 	$(".nameOfCaretaker").val("");
                 	$(".typeOfWater").val("");
                 	$(".fishPond").val("");
                 	$(".fishPondType").val("");
                 	$(".speciesCultured").val("");
                 	$(".status").val("");
                 	$(".kind").val("");
                 	$(".hatchery").val("");
                 	$(".productPer").val("");
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myFishPond");
      		 	//$("#contentList").attr("data-target","#myFishPondList");
      		 
          		 
    	  }
    	  
    	  function newHatchery(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfHatchery").val("");             
                 	$(".nameOfOperator").val("");
                 	$(".addressOfOperator").val("");
                 	$(".operatorClassification").val("");
                 	$(".publicprivate").val("");
                 	$(".indicateSpecies").val("");
                 	$(".prodStocking").val("");
                 	$(".prodCycle").val("");
                 	$(".actualProdForTheYear").val("");
                 	$(".monthStart").val("");
                 	$(".monthEnd").val("");
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myHatchery");
      		 //	$("#contentList").attr("data-target","#myHatcheryList");
      		 
          		 
    	  }
    	  
    	  function newIceplantorcoldstorage(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfIcePlant").val("");             
                 	$(".sanitaryPermit").val("");
                 	$(".operator").val("");
                 	$(".typeOfFacility").val("");
                 	$(".structureType").val("");
                 	$(".capacity").val("");
                 	$(".withValidLicenseToOperate").val("");
                 	$(".withValidSanitaryPermit").val("");
                 	$(".businessPermitsAndCertificateObtained").val("");
                 	$(".typeOfIceMaker").val("");
                 	$(".foodGradule").val("");
                 	$(".sourceOfEquipment").val("");
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myIcePlantorColdStorage");
       		// 	$("#contentList").attr("data-target","#myIcePlantorColdStorageList");
       		 
      		 
          		 
    	  }
    	  
    	  
    	  function newFishCoral(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfOperator").val("");             
                 	$(".operatorClassification").val("");
                 	$(".nameOfStationGearUse").val("");
                 	$(".noOfUnitUse").val("");
                 	$(".fishesCaught").val("");
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myFishCorals");
    		// 	$("#contentList").attr("data-target","#myFishCoralsList");
    		 
          		 
    	  }
    	  
    	  function newFishPort(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfFishport").val("");             
                 	$(".nameOfCaretaker").val("");
                 	$(".operatorClassification").val("");
                 	$(".pfdaPrivateMunicipal").val("");
                 	$(".volumeOfUnloading").val("");
                 	$(".classification").val("");
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myFISHPORT");
     		 //	$("#contentList").attr("data-target","#myFISHPORTList");
     		 
          		 
    	  }
    	  function newMarineProtected(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfMarineProtectedArea").val("");             
                 	$(".type").val("");
                 	$(".sensitiveHabitatWithMPA").val("");
                 	$(".dateEstablishment").val("");
                 	$(".volumeOfUnloading").val("");
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myMarineProtected");
      		 //	$("#contentList").attr("data-target","#myMarineProtectedList");
      		 
          		 
    	  }
    	  function newMarket(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfMarket").val("");             
                 	$(".publicprivate").val("");
                 	$(".majorMinorMarket").val("");
                 	$(".volumeTraded").val("");
                 	$(".volumeOfUnloading").val("");
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myMarket");
       		// 	$("#contentList").attr("data-target","#mymarketList");
       		 
    	  }
    	  function newSchoolOfFisheries(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".name").val("");             
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#mySchool");
       		 //	$("#contentList").attr("data-target","#mySchoolList");
       		 
    	  }
     	  
    	  function newSeagrass(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#mySeagrass");
    		 //	$("#contentList").attr("data-target","#mySeagrassList");
    		 
       		 
    	  }
    	  
    	  
    	  function newSeaweeds(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				$(".indicateGenus").val("");
    				$(".culturedMethodUsed").val("");
    				$(".culturedPeriod").val("");
    				$(".productionKgPerCropping").val("");
    				$(".productionNoOfCroppingPerYear").val("");
    				
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#mySeaweeds");
     		// 	$("#contentList").attr("data-target","#mySeaweedsList");
     		 
    		 
       		 
    	  }
    	  
    	  
    	  function newMangrove(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				$(".indicateSpecies").val("");
    				$(".type").val("");
    				
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	 
      		 	$("#contentR").attr("data-target","#myMangrove");
      		 //	$("#contentList").attr("data-target","#myMangroveList");
      		       		 
    	  }
    	  
    	  function newLGU(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				$(".lguName").val("");
    				$(".noOfBarangayCoastal").val("");
    				$(".noOfBarangayInland").val("");
    				$(".lguCoastalLength").val("");
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
            	
            	$("#contentR").attr("data-target","#myLGU");
       		 //	$("#contentList").attr("data-target","#myLGUList");
       		 
      		       		 
    	  }
    	  function newTrainingCenter(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				$(".name").val("");
    				$(".specialization").val("");
    				$(".facilityWithinTheTrainingCenter").val("");
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
            	
            	$("#contentR").attr("data-target","#myTrainingCenter");
    		 //	$("#contentList").attr("data-target","#myTrainingCenterList");
    		     		 
      		       		 
    	  }
    	  function newMaricultureZone(){
    		  var slctProvinces=$('.province');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.municipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.barangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				$(".nameOfMariculture").val("");
    				$(".maricultureType").val("");
    				$(".eCcNumber").val("");
    				$(".individual").val("");
    				$(".partnership").val("");
    				$(".cooperativeAssociation").val("");
    				$(".cageType").val("");
    				$(".cageSize").val("");
    				$(".cageQuantity").val("");
    				$(".speciesCultured").val("");
    				$(".quantityMt").val("");
    				$(".kind").val("");
    				$(".productionPerCropping").val("");
    				$(".numberofCropping").val("");
                 	$(".species").val("");
                 	$(".farmProduce").val("");
                 	$(".productDestination").val("");
                 	$(".aquacultureProduction").val("");
                 	$(".dateLaunched").val("");
                 	$(".dateInacted").val("");
                 	$(".dateApproved").val("");
                 	$(".publicprivate").val("");
                 	$(".volumeTraded").val("");
                 	$(".accreditations").val("");
                 	$(".municipalOrdinanceNo").val("");
                 	$(".dataSource").val("");
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
            	
            	$("#contentR").attr("data-target","#myMaricultureZone");
     		 //	$("#contentList").attr("data-target","#myMaricultureZoneList");
     		 
    	  }
