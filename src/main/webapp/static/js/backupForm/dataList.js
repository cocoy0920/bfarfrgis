	jQuery.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
	{
		return {
			"iStart":         oSettings._iDisplayStart,
			"iEnd":           oSettings.fnDisplayEnd(),
			"iLength":        oSettings._iDisplayLength,
			"iTotal":         oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage":          oSettings._iDisplayLength === -1 ?
				0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
			"iTotalPages":    oSettings._iDisplayLength === -1 ?
				0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
		};
	};
	
function FisProcessingList(){
	$("#fishprocessingplants").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
            
            {
                extend: 'excelHtml5',
                messageTop: 'FISH PROCESSING PLANTS LIST'
            },
        ],
     /*   "columnDefs": [
            {
                "targets": [ 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21],
                "visible": false,
                "searchable": false
            }
        ],
        */
     
        "sAjaxSource": "fishprocessingPlantsDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex, iDisplayIndexFull ) {
      
            $(nRow).click(function() {
            target = mData.processingPlant.uniqueKey;
        	   console.log(mData.processingPlant.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerFishProcessingDel();
         	   refreshRecord(content,"fishprocessingplants","old");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                //document.location.href = "mapview/" + mData.processingPlant.uniqueKey ;
            });
        },
        "aoColumns": [
            { "mData": "processingPlant.province" },
            { "mData": "processingPlant.municipality" },
            { "mData": "processingPlant.barangay" },
            { "mData": "processingPlant.nameOfProcessingPlants" },
            { "mData": "processingPlant.dateEncoded" },
            { "mData": "processingPlant.nameOfOperator" },
            { "mData": "processingPlant.operatorClassification" },
            { "mData": "processingPlant.indicateSpecies" },
            { "mData": "processingPlant.processingTechnique" },
            { "mData": "processingPlant.packagingType" },
            { "mData": "processingPlant.processingEnvironmentClassification" },
            { "mData": "processingPlant.businessPermitsAndCertificateObtained" },
            { "mData": "processingPlant.marketReach" },
            { "mData": "processingPlant.code" },
            { "mData": "processingPlant.sourceOfData" },
          
            { "mData": "processingPlant.remarks" },
            { "mData": "processingPlant.encodedBy" },
            { "mData": "processingPlant.editedBy" },
            { "mData": "processingPlant.dateEncoded" },
            { "mData": "processingPlant.dateEdited" },
            { "mData": "processingPlant.lat" },
            { "mData": "processingPlant.lon" }
             
        ]
    } );
	



}
function FishSanctuaryList(){
	 $("#fishsanctuaries").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
            
            {
                extend: 'excelHtml5',
                messageTop: 'FISH SANCTUARY LIST'
            },
            
        ],
      /*  "columnDefs": [
            {
                "targets": [ 5,6,7,8,9,10,11,12,13,14,15],
                "visible": false,
                "searchable": false
            }
        ],
        */
        "sAjaxSource": "fishsanctuaryDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
/*         	$('td:eq(0)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.province + '</a>');
        	$('td:eq(1)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.municipality + '</a>');
            $('td:eq(2)', nRow).html('<a href="edit-' + mData.id+ '">' +
                mData.barangay + '</a>');   
            $('td:eq(3)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.nameOfFishSanctuary + '</a>');
            $('td:eq(4)', nRow).html('<a href="edit-' + mData.id+ '">' +
                        mData.dateEncoded + '</a>');
            
            
            return nRow; */
            
        	   $(nRow).click(function() {
        	   
        	   target = mData.sanctuaries.uniqueKey;
        	   console.log(mData.sanctuaries.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerFishSancturiesDel();
         	   refreshRecord(content,"fishsanctuaries","old");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                  // document.location.href = "mapview/" + mData.sanctuaries.uniqueKey ;
               });
        },
        "aoColumns": [
            { "mData": "sanctuaries.province" },
            { "mData": "sanctuaries.municipality" },
            { "mData": "sanctuaries.barangay" },       
            { "mData": "sanctuaries.nameOfFishSanctuary" },
            { "mData": "sanctuaries.dateEncoded" },
            { "mData": "sanctuaries.dateAsOf" },
            { "mData": "sanctuaries.code" },
            { "mData": "sanctuaries.area" },
            { "mData": "sanctuaries.type" },           
            { "mData": "sanctuaries.fishSanctuarySource" },
            { "mData": "sanctuaries.remarks" },
            { "mData": "sanctuaries.encodedBy" },
            { "mData": "sanctuaries.editedBy" },          
            { "mData": "sanctuaries.dateEdited" },
            { "mData": "sanctuaries.lat" },
            { "mData": "sanctuaries.lon" }
            
           /*  { "mData": "lat" },
            { "mData": "lon" }, */
             
        ]
    } );



}
function FishCageList(){

	$("#fishcageData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,

        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
           
            {
                extend: 'excelHtml5',
                messageTop: 'Fish Cages List'
            },
            
        ],
        
        /*"columnDefs": [

            {
                "targets": [ 5,6,7,8,9,10,11,12,13,14,15,16,17],
                "visible": false,
                "searchable": false
            }
            
        ],
     */
        "sAjaxSource": "fishcageDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
            
        	   $(nRow).click(function() {
        	   target =  mData.fishCage.uniqueKey;
        	   console.log(mData.fishCage.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerFishCageDel();
         	   refreshRecord(content,"fishcage","old");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
        	   
                   //document.location.href = "mapview/" + mData.fishCage.uniqueKey;
               });
        },
        "aoColumns": [
            { "mData": "fishCage.province" },
            { "mData": "fishCage.municipality" },
            { "mData": "fishCage.barangay" },
            { "mData": "fishCage.nameOfOperator" },
            { "mData": "fishCage.dateEncoded" },
            { "mData": "fishCage.cageDimension" },
            { "mData": "fishCage.cageTotalArea" },
            { "mData": "fishCage.cageType" },
            { "mData": "fishCage.cageNoOfCompartments" },
            { "mData": "fishCage.indicateSpecies" },
            { "mData": "fishCage.sourceOfData" },
            { "mData": "fishCage.code" },
            { "mData": "fishCage.remarks" },
            { "mData": "fishCage.encodedBy" },
            { "mData": "fishCage.editedBy" },
            { "mData": "fishCage.dateEdited" },
			{ "mData": "fishCage.lat" },
            { "mData": "fishCage.lon" } 
             
        ]
    } );
}
function FishCorralList(){
	$("#fishcoralData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                messageTop: 'FISH CORRAL LIST'
            },
            
        ],
        /*
        "columnDefs": [

            {
                "targets": [ 5,6,7,8,9,10,11,12,13,14,15,16,17],
                "visible": false,
                "searchable": false
            }
            
        ],
        */
        "sAjaxSource": "fishcorralDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
        	/* $('td:eq(0)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.province + '</a>');
        	$('td:eq(1)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.municipality + '</a>');
            $('td:eq(2)', nRow).html('<a href="edit-' + mData.id+ '">' +
                mData.barangay + '</a>');   
            $('td:eq(3)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.nameOfOperator + '</a>');
            $('td:eq(4)', nRow).html('<a href="edit-' + mData.id+ '">' +
                        mData.dateEncoded + '</a>');                      
            return nRow; */
            
        	   $(nRow).click(function() {
        	   target = mData.fishCoral.uniqueKey;
        	   console.log(mData.fishCoral.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerFishcorralDel();
         	   refreshRecord(content,"fishcorrals","old");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                   //document.location.href = "mapview/" + mData.fishCoral.uniqueKey ;
               });
        },
        "aoColumns": [
            { "mData": "fishCoral.province" },
            { "mData": "fishCoral.municipality" },
            { "mData": "fishCoral.barangay" },            
            { "mData": "fishCoral.nameOfOperator" },
            { "mData": "fishCoral.dateEncoded" },
            { "mData": "fishCoral.area" },
            { "mData": "fishCoral.operatorClassification" },
            { "mData": "fishCoral.nameOfStationGearUse" },
            { "mData": "fishCoral.noOfUnitUse" },
            { "mData": "fishCoral.fishesCaught" },
            { "mData": "fishCoral.dataSource" },
            { "mData": "fishCoral.code" },
            { "mData": "fishCoral.remarks" },
            { "mData": "fishCoral.encodedBy" },
            { "mData": "fishCoral.editedBy" },
            { "mData": "fishCoral.dateEdited" },          
            { "mData": "fishCoral.lat" },
            { "mData": "fishCoral.lon" }
            
             
        ]
    } );

}
function FishLandingList(){

	$("#fishlandingData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                messageTop: 'FISH LANDING LIST'
            },
            
        ],
        /*
        "columnDefs": [
            {
                "targets": [ 5,6,7,8,9,10,11,12,13],
                "visible": false,
                "searchable": false
            }
        ],
        */
        "sAjaxSource": "fishLandingDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
            
        	   $(nRow).click(function() {
        	    target = mData.fishLanding.uniqueKey;
        	   console.log(mData.fishLanding.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerFishlandingDel();
         	   refreshRecord(content,"fishslanding","old");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                  // document.location.href = "mapview/" + mData.fishLanding.uniqueKey ;
               });
        },
        "aoColumns": [
            { "mData": "fishLanding.province" },
            { "mData": "fishLanding.municipality" },
            { "mData": "fishLanding.barangay" },
            { "mData": "fishLanding.nameOfLanding" },
            { "mData": "fishLanding.dateEncoded" },
            { "mData": "fishLanding.classification" },
            { "mData": "fishLanding.volumeOfUnloadingMT" },
            { "mData": "fishLanding.code" },
            { "mData": "fishLanding.remarks" },
            { "mData": "fishLanding.encodedBy" },
            { "mData": "fishLanding.editedBy" },
            { "mData": "fishLanding.dateEdited" },
            { "mData": "fishLanding.lat" },
            { "mData": "fishLanding.lon" }
        ]
    } );

}

function FishPenList(){
	
	$("#fishpenData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                messageTop: 'FISH PEN LIST'
            },
        ],
        /*
        "columnDefs": [
            {
                "targets": [ 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19],
                "visible": false,
                "searchable": false
            }
        ],
        */
        "sAjaxSource": "fishPenDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
        	   $(nRow).click(function() {
        	   target = mData.fishPen.uniqueKey;
        	   console.log(mData.fishPen.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerFishPenDel();
         	   refreshRecord(content,"fishpen","old");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                  // document.location.href = "mapview/" + mData.fishPen.uniqueKey ;
               });
        },
        "aoColumns": [
            { "mData": "fishPen.province" },
            { "mData": "fishPen.municipality" },
            { "mData": "fishPen.barangay" },
            { "mData": "fishPen.nameOfOperator" },
            { "mData": "fishPen.dateEncoded" },
            { "mData": "fishPen.dateAsOf" },
            { "mData": "fishPen.area" },
            { "mData": "fishPen.noOfFishPen" },
            { "mData": "fishPen.speciesCultured" },
            { "mData": "fishPen.code" },
            { "mData": "fishPen.croppingStart" },
            { "mData": "fishPen.croppingEnd" },
            { "mData": "fishPen.sourceOfData" },
            { "mData": "fishPen.sourceOfData" },         
            { "mData": "fishPen.remarks" },
            { "mData": "fishPen.encodedBy" },
            { "mData": "fishPen.editedBy" },
            { "mData": "fishPen.dateEdited" },
            { "mData": "fishPen.lat" },
            { "mData": "fishPen.lon" }
             
        ]
    } );

}
function FishPondList(){
	

	$("#fishpondData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                messageTop: 'FISH POND LIST'
            },
            
        ],
        /*
        "columnDefs": [
        	 {
                 "targets": [ 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19],
                 "visible": false,
                 "searchable": false
             }
             
        ],
        */
        "sAjaxSource": "fishPoundDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
            
        	   $(nRow).click(function() {
        	   target = mData.fishPond.uniqueKey;
        	   console.log(mData.fishPond.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerFishPondDel();
         	   refreshRecord(content,"fishpond","old");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                   //document.location.href = "mapview/" + mData.fishPond.uniqueKey ;
               });
        },
        "aoColumns": [
            { "mData": "fishPond.province" },
            { "mData": "fishPond.municipality" },
            { "mData": "fishPond.barangay" },
            { "mData": "fishPond.nameOfFishPondOperator" },
            { "mData": "fishPond.dateEncoded" },
            { "mData": "fishPond.area" },
            { "mData": "fishPond.nameOfCaretaker" },
            { "mData": "fishPond.typeOfWater" },
            { "mData": "fishPond.speciesCultured" },
            { "mData": "fishPond.kind" },
            { "mData": "fishPond.hatchery" },
            { "mData": "fishPond.productPer" },
            { "mData": "fishPond.code" },
            { "mData": "fishPond.dataSource" },           
            { "mData": "fishPond.remarks" },
            { "mData": "fishPond.encodedBy" },
            { "mData": "fishPond.editedBy" },
            { "mData": "fishPond.dateEdited" },          
            { "mData": "fishPond.lat" },
            { "mData": "fishPond.lon" }
             
        ]
    } );

}

function FishPortList(){
	

	$("#fishportData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
        	 {
                 extend: 'excelHtml5',
                 messageTop: 'FISH POND LIST'
             },
        ],
        /*
        "columnDefs": [
        	 {
                 "targets": [ 5,6,7,8,9,10,11,12,13,14,15,16],
                 "visible": false,
                 "searchable": false
             }
        ],
        */
        "sAjaxSource": "fishportDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
        	/* $('td:eq(0)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.province + '</a>');
        	$('td:eq(1)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.municipality + '</a>');
            $('td:eq(2)', nRow).html('<a href="edit-' + mData.id+ '">' +
                mData.barangay + '</a>');   
            $('td:eq(3)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.nameOfFishport + '</a>');
            $('td:eq(4)', nRow).html('<a href="edit-' + mData.id+ '">' +
                        mData.dateEncoded + '</a>');
            
            
            return nRow; */
            
        	   $(nRow).click(function() {
        	   target = mData.fishport.uniqueKey;
        	   console.log(mData.fishport.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerFishPortDel();
         	   refreshRecord(content,"FISHPORT","old");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                   //document.location.href = "mapview/" + mData.fishport.uniqueKey ;
               });
        },
        "aoColumns": [
            { "mData": "fishport.province" },
            { "mData": "fishport.municipality" },
            { "mData": "fishport.barangay" },
            { "mData": "fishport.nameOfFishport" },
            { "mData": "fishport.dateEncoded" },
            { "mData": "fishport.nameOfCaretaker" },
            { "mData": "fishport.pfdaPrivateMunicipal" },
            { "mData": "fishport.volumeOfUnloading" },
            { "mData": "fishport.classification" },
            { "mData": "fishport.code" },
            { "mData": "fishport.dataSource" },
            { "mData": "fishport.remarks" },
            { "mData": "fishport.encodedBy" },
            { "mData": "fishport.editedBy" },
            { "mData": "fishport.dateEdited" },
            { "mData": "fishport.lat" },
            { "mData": "fishport.lon" }
           
             
        ]
    } );
}
function HatcheryList(){

	$("#hatcheryData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                messageTop: 'HATCHERY LIST'
            },
        ],
        /*
        "columnDefs": [
       	 {
             "targets": [ 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],
             "visible": false,
             "searchable": false
         }
         
        ],
        */
        "sAjaxSource": "fishHatcheryDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
            
        	   $(nRow).click(function() {
        	   target = mData.hatchery.uniqueKey;
        	   console.log(mData.hatchery.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerHatcheryDel();
         	   refreshRecord(content,"hatchery","old");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                  // document.location.href = "mapview/" + mData.hatchery.uniqueKey ;
               });
        },
        "aoColumns": [
            { "mData": "hatchery.province" },
            { "mData": "hatchery.municipality" },
            { "mData": "hatchery.barangay" },
            { "mData": "hatchery.nameOfHatchery" },
            { "mData": "hatchery.dateEncoded" },
            
            { "mData": "hatchery.nameOfOperator" },
            { "mData": "hatchery.operatorClassification" },
            { "mData": "hatchery.area" },
            { "mData": "hatchery.publicprivate" },
            { "mData": "hatchery.indicateSpecies" },
            
            { "mData": "hatchery.prodStocking" },
            { "mData": "hatchery.prodCycle" },
            { "mData": "hatchery.actualProdForTheYear" },
            { "mData": "hatchery.monthStart" },
            { "mData": "hatchery.monthEnd" },
            { "mData": "hatchery.dataSource" },
            { "mData": "hatchery.remarks" },
            { "mData": "hatchery.encodedBy" },
            { "mData": "hatchery.editedBy" },
            
            { "mData": "hatchery.dateEdited" },
            { "mData": "hatchery.lat" },
            { "mData": "hatchery.lon" }
              
        ]
    } );

}

function IcePlantColdStorage(){
	
	$("#icePlantColdData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                messageTop: 'ICE PLANT COLD STORAGE LIST'
            },
        ],
        /*
        "columnDefs": [
            {
                "targets": [ 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22],
                "visible": false,
                "searchable": false
            }
        ],
        */
        "sAjaxSource": "iceplantDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
            
        	   $(nRow).click(function() {
        	   target = mData.icePlantColdStorage.uniqueKey;
        	   console.log(mData.icePlantColdStorage.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerIcePlantDel();
         	   refreshRecord(content,"iceplantorcoldstorage","old");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                  // document.location.href = "mapview/" + mData.icePlantColdStorage.uniqueKey ;
               });
        },
        "aoColumns": [
            { "mData": "icePlantColdStorage.province" },
            { "mData": "icePlantColdStorage.municipality" },
            { "mData": "icePlantColdStorage.barangay" },
            { "mData": "icePlantColdStorage.nameOfIcePlant" },
            { "mData": "icePlantColdStorage.dateEncoded" },
            { "mData": "icePlantColdStorage.name" },
            { "mData": "icePlantColdStorage.operator" },
            { "mData": "icePlantColdStorage.typeOfFacility" },
            { "mData": "icePlantColdStorage.structureType" },
            { "mData": "icePlantColdStorage.capacity" },
            { "mData": "icePlantColdStorage.withValidLicenseToOperate" },
            { "mData": "icePlantColdStorage.withValidSanitaryPermit" },
            { "mData": "icePlantColdStorage.businessPermitsAndCertificateObtained" },
            { "mData": "icePlantColdStorage.typeOfIceMaker" },
            { "mData": "icePlantColdStorage.foodGradule" },
            { "mData": "icePlantColdStorage.sourceOfEquipment" },
            { "mData": "icePlantColdStorage.code" },
            { "mData": "icePlantColdStorage.remarks" },
            { "mData": "icePlantColdStorage.encodedBy" },
            { "mData": "icePlantColdStorage.editedBy" },
            { "mData": "icePlantColdStorage.dateEdited" },
            { "mData": "icePlantColdStorage.lat" },
            { "mData": "icePlantColdStorage.lon" }
        
        ]
    } );

}

function LGUList(){
	

	$("#lguData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
        	{
                extend: 'excelHtml5',
                messageTop: 'LOCAL GOVERNMENT UNIT LIST'
            },
        ],
        /*
        "columnDefs": [
        	{
                "targets": [ 5,6,7,8,9,10,11,12,13,14,15],
                "visible": false,
                "searchable": false
            }
        ],
        */
        "sAjaxSource": "lguDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
        	/* $('td:eq(0)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.province + '</a>');
        	$('td:eq(1)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.municipality + '</a>');
            $('td:eq(2)', nRow).html('<a href="edit-' + mData.id+ '">' +
                mData.barangay + '</a>');   
            $('td:eq(3)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.lguName + '</a>');
            $('td:eq(4)', nRow).html('<a href="edit-' + mData.id+ '">' +
                        mData.dateEncoded + '</a>');
            
            
            return nRow; */
            
        	   $(nRow).click(function() {
        	   target = mData.lgu.uniqueKey;
        	   console.log(mData.lgu.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerLGUDel();
         	   refreshRecord(content,"lgu","old");
         	   
    			$('#myLGUModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                   //document.location.href = "mapview/" + mData.lgu.uniqueKey ;
               });
        },
        "aoColumns": [
            { "mData": "lgu.province" },
            { "mData": "lgu.municipality" },
            { "mData": "lgu.barangay" },
            { "mData": "lgu.lguName" },
            { "mData": "lgu.dateEncoded" },
            { "mData": "lgu.code" },
            { "mData": "lgu.dataSource" }, 
            { "mData": "lgu.noOfBarangayCoastal" }, 
            { "mData": "lgu.noOfBarangayInland" }, 
            { "mData": "lgu.lguCoastalLength" }, 
          
            { "mData": "lgu.remarks" },
            { "mData": "lgu.encodedBy" },
            { "mData": "lgu.editedBy" },
            { "mData": "lgu.dateEdited" },
            { "mData": "lgu.lat" },
            { "mData": "lgu.lon" }
            
           /*  { "mData": "lat" },
            { "mData": "lon" }, */
             
        ]
    } );
}

function MangroveList(){
	

	$("#mangroveData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
        	{
                extend: 'excelHtml5',
                messageTop: 'MANGROVE LIST'
            },
        ],
        /*
        "columnDefs": [
        	{
                "targets": [ 5,6,7,8,9,10,11,12,13],
                "visible": false,
                "searchable": false
            }
        ],
        */
        "sAjaxSource": "mangroveDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
   
            
        	   $(nRow).click(function() {
        	   target = mData.mangrove.uniqueKey;
        	   console.log(mData.mangrove.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerMangroveDel();
         	   refreshRecord(content,"mangrove","old");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                  // document.location.href = "mapview/" + mData.mangrove.uniqueKey ;
               });
        },
        "aoColumns": [
            { "mData": "mangrove.province" },
            { "mData": "mangrove.municipality" },
            { "mData": "mangrove.barangay" },
            { "mData": "mangrove.indicateSpecies" },
            { "mData": "mangrove.dateEncoded" },
            { "mData": "mangrove.type" },
            { "mData": "mangrove.code" },
            { "mData": "mangrove.dataSource" },        
            { "mData": "mangrove.remarks" },
            { "mData": "mangrove.encodedBy" },
            { "mData": "mangrove.editedBy" },
            { "mData": "mangrove.dateEdited" },
            { "mData": "mangrove.lat" },
            { "mData": "mangrove.lon" }
             
        ]
    } );
}

function MaricultureZoneList(){
	

	$("#maricultureData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
        	 {
                 extend: 'excelHtml5',
                 messageTop: 'MARICULTURE PARK LIST'
             },
        ],
        /*
        "columnDefs": [
        	{
                "targets": [ 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32],
                "visible": false,
                "searchable": false
            }
        ],
        */
        "sAjaxSource": "maricultureDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
            
        	   $(nRow).click(function() {
        	   target = mData.maricultureZone.uniqueKey;
        	   console.log(mData.maricultureZone.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerMaricultureParkDel();
         	   refreshRecord(content,"mariculturezone","old");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                  // document.location.href = "mapview/" + mData.maricultureZone.uniqueKey ;
               });
            
        },
        "aoColumns": [
            { "mData": "maricultureZone.province" },
            { "mData": "maricultureZone.municipality" },
            { "mData": "maricultureZone.barangay" },
            { "mData": "maricultureZone.nameOfMariculture" },
            { "mData": "maricultureZone.dateEncoded" },
            { "mData": "maricultureZone.area" },
            { "mData": "maricultureZone.maricultureType" },
            { "mData": "maricultureZone.municipalOrdinanceNo" },
            { "mData": "maricultureZone.dateInacted" },
            { "mData": "maricultureZone.eCcNumber" },
            { "mData": "maricultureZone.individual" },
            { "mData": "maricultureZone.partnership" },
            { "mData": "maricultureZone.cooperativeAssociation" },
            { "mData": "maricultureZone.cageType" },
            { "mData": "maricultureZone.cageSize" },
            { "mData": "maricultureZone.cageQuantity" },
            { "mData": "maricultureZone.speciesCultured" },
            { "mData": "maricultureZone.quantityMt" },
            { "mData": "maricultureZone.kind" },
            { "mData": "maricultureZone.productionPerCropping" },
            { "mData": "maricultureZone.numberofCropping" },
            { "mData": "maricultureZone.species" },
            { "mData": "maricultureZone.farmProduce" },
            { "mData": "maricultureZone.productDestination" },
            { "mData": "maricultureZone.aquacultureProduction" },
            { "mData": "maricultureZone.code" },
            { "mData": "maricultureZone.dataSource" },         
            { "mData": "maricultureZone.remarks" },
            { "mData": "maricultureZone.encodedBy" },
            { "mData": "maricultureZone.editedBy" },
            { "mData": "maricultureZone.dateEdited" },
            { "mData": "maricultureZone.lat" },
            { "mData": "maricultureZone.lon" }
           
             
        ]
    } );

}

function MarineProtectedAreaList(){
	$("#marineData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
        	{
                extend: 'excelHtml5',
                messageTop: 'MARINE PROTECTED AREA LIST'
            },
        ],
        /*
        "columnDefs": [
        	{
                "targets": [ 5,6,7,8,9,10,11,12,13,14,15,16,17],
                "visible": false,
                "searchable": false
            }
        ],
        */
        "sAjaxSource": "marineprotectedareaDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
        	   $(nRow).click(function() {
        	   target = mData.marineProtectedArea.uniqueKey;
        	   console.log(mData.marineProtectedArea.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerMarineProtectedDel();
         	   refreshRecord(content,"marineprotectedarea","old");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                  // document.location.href = "mapview/" + mData.marineProtectedArea.uniqueKey ;
               });
            
        },
        "aoColumns": [
            { "mData": "marineProtectedArea.province" },
            { "mData": "marineProtectedArea.municipality" },
            { "mData": "marineProtectedArea.barangay" },
            { "mData": "marineProtectedArea.nameOfMarineProtectedArea" },
            { "mData": "marineProtectedArea.dateEncoded" },
            { "mData": "marineProtectedArea.area" },
            { "mData": "marineProtectedArea.type" },
            { "mData": "marineProtectedArea.sensitiveHabitatWithMPA" },
            { "mData": "marineProtectedArea.dateEstablishment" },
            { "mData": "marineProtectedArea.code" },
            { "mData": "marineProtectedArea.dataSource" },
            { "mData": "marineProtectedArea.dataSource" },           
            { "mData": "marineProtectedArea.remarks" },
            { "mData": "marineProtectedArea.encodedBy" },
            { "mData": "marineProtectedArea.editedBy" },
            { "mData": "marineProtectedArea.dateEdited" },
            { "mData": "marineProtectedArea.lat" },
            { "mData": "marineProtectedArea.lon" }
             
        ]
    } );
}
function MarketList(){
	

	$("#marketData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
        	{
                extend: 'excelHtml5',
                messageTop: 'MARKET LIST'
            },
        ],
        /*
        "columnDefs": [
        	{
                "targets": [ 5,6,7,8,9,10,11,12,13,14,15],
                "visible": false,
                "searchable": false
            }
        ],
        */
        "sAjaxSource": "marketDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {

            
        	   $(nRow).click(function() {
        	   target = mData.market.uniqueKey;
        	   console.log(mData.market.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerMarketDel();
         	   refreshRecord(content,"market");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                 //  document.location.href = "mapview/" + mData.market.uniqueKey ;
               });
            
        },
        "aoColumns": [
            { "mData": "market.province" },
            { "mData": "market.municipality" },
            { "mData": "market.barangay" },
            { "mData": "market.nameOfMarket" },
            { "mData": "market.dateEncoded" },
            { "mData": "market.publicprivate" },
            { "mData": "market.majorMinorMarket" },
            { "mData": "market.volumeTraded" },
            { "mData": "market.code" },
            { "mData": "market.dataSource" },         
            { "mData": "market.remarks" },
            { "mData": "market.encodedBy" },
            { "mData": "market.editedBy" },
            { "mData": "market.dateEdited" },
            { "mData": "market.lat" },
            { "mData": "market.lon" }
            
         
             
        ]
    } );

}
function SchoolOfFisheriesList(){
	
	$("#schoolsData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
        	{
                extend: 'excelHtml5',
                messageTop: 'SCHOOL OF FISHERIES LIST'
            },
        ],
        /*
        "columnDefs": [
        	{
                "targets": [ 5,6,7,8,9,10,11,12],
                "visible": false,
                "searchable": false
            }
        ],
        */
        "sAjaxSource": "schoolOfFisheriesDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
               
        	   $(nRow).click(function() {
        	   target = mData.schoolOfFisheries.uniqueKey;
        	   console.log(mData.schoolOfFisheries.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerSchoolOfFisheriesDel();
         	   refreshRecord(content,"schooloffisheries");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                   //document.location.href = "mapview/" + mData.schoolOfFisheries.uniqueKey ;
               });
            
        },
        "aoColumns": [
            { "mData": "schoolOfFisheries.province" },
            { "mData": "schoolOfFisheries.municipality" },
            { "mData": "schoolOfFisheries.barangay" },
            { "mData": "schoolOfFisheries.name" },
            { "mData": "schoolOfFisheries.dateEncoded" },
            { "mData": "schoolOfFisheries.code" },
            { "mData": "schoolOfFisheries.dataSource" },         
            { "mData": "schoolOfFisheries.remarks" },
            { "mData": "schoolOfFisheries.encodedBy" },
            { "mData": "schoolOfFisheries.editedBy" },
            { "mData": "schoolOfFisheries.dateEdited" },
            { "mData": "schoolOfFisheries.lat" },
            { "mData": "schoolOfFisheries.lon" }
            
        ]
    } );

}

function SeaGrassList(){
	

	$("#seagrassData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
        	{
                extend: 'excelHtml5',
                messageTop: 'SEA GRASS LIST'
            },
        ],
        /*
        "columnDefs": [
        	{
                "targets": [ 5,6,7,8,9,10,11],
                "visible": false,
                "searchable": false
            }
        ],
        */
        "sAjaxSource": "seagrassDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
        	/* $('td:eq(0)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.province + '</a>');
        	$('td:eq(1)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.municipality + '</a>');
            $('td:eq(2)', nRow).html('<a href="edit-' + mData.id+ '">' +
                mData.barangay + '</a>');   
            $('td:eq(3)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.dataSource + '</a>');
            $('td:eq(4)', nRow).html('<a href="edit-' + mData.id+ '">' +
                        mData.dateEncoded + '</a>');
            
            
            return nRow; */
            
        	   $(nRow).click(function() {
        	   target = mData.seaGrass.uniqueKey;
        	   console.log(mData.seaGrass.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerSeaGrassDel();
         	   refreshRecord(content,"seagrass");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                  // document.location.href = "mapview/" + mData.seaGrass.uniqueKey ;
               });
            
            
        },
        "aoColumns": [
            { "mData": "seaGrass.province" },
            { "mData": "seaGrass.municipality" },
            { "mData": "seaGrass.barangay" },
            { "mData": "seaGrass.dateEncoded" },
            { "mData": "seaGrass.code" },
            { "mData": "seaGrass.dataSource" },         
            { "mData": "seaGrass.remarks" },
            { "mData": "seaGrass.encodedBy" },
            { "mData": "seaGrass.editedBy" },
            { "mData": "seaGrass.dateEdited" },
            { "mData": "seaGrass.lat" },
            { "mData": "seaGrass.lon" }
           
        ]
    } );

}

function SeaweedsList(){
	
	$("#seaweedsData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
        	 {
                 extend: 'excelHtml5',
                 messageTop: 'SEAWEEDS LIST'
             },
        ],
        /*
        "columnDefs": [
        	{
                "targets": [ 5,6,7,8,9,10,11],
                "visible": false,
                "searchable": false
            }
        ],
        */
        "sAjaxSource": "seawedsDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
        	/* $('td:eq(0)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.province + '</a>');
        	$('td:eq(1)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.municipality + '</a>');
            $('td:eq(2)', nRow).html('<a href="edit-' + mData.id+ '">' +
                mData.barangay + '</a>');   
            $('td:eq(3)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.dataSource + '</a>');
            $('td:eq(4)', nRow).html('<a href="edit-' + mData.id+ '">' +
                        mData.dateEncoded + '</a>');
            
            
            return nRow; */
            
        	   $(nRow).click(function() {
        	   target = mData.seaweeds.uniqueKey;
        	   console.log(mData.seaweeds.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerSeaWeedsDel();
         	   refreshRecord(content,"seaweeds");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                   //document.location.href = "mapview/" + mData.seaweeds.uniqueKey ;
               });
            
        },
        "aoColumns": [
        	{ "mData": "seaweeds.province" },
            { "mData": "seaweeds.municipality" },
            { "mData": "seaweeds.barangay" },
            
            { "mData": "seaweeds.dateEncoded" },
            { "mData": "seaweeds.code" },
            { "mData": "seaweeds.dataSource" },         
            { "mData": "seaweeds.remarks" },
            { "mData": "seaweeds.encodedBy" },
            { "mData": "seaweeds.editedBy" },
            { "mData": "seaweeds.dateEdited" },
            { "mData": "seaweeds.lat" },
            { "mData": "seaweeds.lon" }
          
        ]
    } );

}

function TrainingCenterList(){
	
	$("#trainingData").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
        	 {
                 extend: 'excelHtml5',
                 messageTop: 'FISHERIES TRAINING LIST'
             },
        ],
        /*
        "columnDefs": [
        	 {
                 "targets": [ 5,6,7,8,9,10,11,12,13,14],
                 "visible": false,
                 "searchable": false
             }
        ],
        */
        "sAjaxSource": "trainingDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
            
        	   $(nRow).click(function() {
        	   target = mData.trainingCenter.uniqueKey;
        	   console.log(mData.trainingCenter.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerFisheriesTrainingDel();
         	   refreshRecord(content,"trainingcenter");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                   //document.location.href = "mapview/" + mData.trainingCenter.uniqueKey ;
               });
        },
        "aoColumns": [
            { "mData": "trainingCenter.province" },
            { "mData": "trainingCenter.municipality" },
            { "mData": "trainingCenter.barangay" },
            { "mData": "trainingCenter.name" },
            { "mData": "trainingCenter.dateEncoded" },
            { "mData": "trainingCenter.specialization" },
            { "mData": "trainingCenter.facilityWithinTheTrainingCenter" },
            { "mData": "trainingCenter.code" },
            { "mData": "trainingCenter.dataSource" },        
            { "mData": "trainingCenter.remarks" },
            { "mData": "trainingCenter.encodedBy" },
            { "mData": "trainingCenter.editedBy" },
            { "mData": "trainingCenter.dateEdited" },
            { "mData": "trainingCenter.lat" },
            { "mData": "trainingCenter.lon" }
             
        ]
    } );

}