	jQuery(document).ready(function($) {


	    var token = $("meta[name='_csrf']").attr("content");
	            var header = $("meta[name='_csrf_header']").attr("content");
	            
	            $(document).ajaxSend(function(e, xhr, options) {
	                xhr.setRequestHeader(header, token);
	              });
	            
		$(".submitFishSanctuaries").click(function(e){
		e.preventDefault();
			var developerData = {};
			
			/* CHANGE ONLY */
        	developerData["dateAsOf"] = $(".dateAsOf").val();
        	developerData["region"] = $(".region").val();
			developerData["province"] = $(".fishsanctuaryprovince").val();
        	developerData["municipality"] =	$(".fishsanctuarymunicipality").val();
        	developerData["barangay"] = $(".fishsanctuarybarangay").val();
        	developerData["nameOfFishSanctuary"] = $(".nameOfFishSanctuary").val();
        	developerData["code"] = $(".code").val();
        	developerData["area"] = $(".area").val();
        	developerData["type"] = $(".type").val();
        	developerData["fishSanctuarySource"] = $(".fishSanctuarySource").val();
        	developerData["remarks"] = $(".remarks").val();
        	developerData["bfardenr"] = $(".bfardenr").val();
        	developerData["sheltered"] = $(".sheltered").val();
        	developerData["lat"] = $(".lat").val();
        	developerData["lon"] = $(".lon").val();
        	developerData["image"] = $(".image").val();
        	
        	
        	//add //

            $(".error").remove();
            
            if ($('.dateAsOf').val().length < 1) {

            	$('.dateAsOf').after('<span class="error">This field is required</span>');

            	$('.dateAsOf').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.dateAsOf').removeClass("invalid").addClass("valid");

            	error_free = true;

            }    
            
            
            if ($('.region').val().length < 1) {

            	$('.region').after('<span class="error">This field is required</span>');

            	$('.region').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.region').removeClass("invalid").addClass("valid");

            	error_free = true;

            }   
            
            
            if ($('.fishsanctuaryprovince').val().length < 1) {

            	$('.fishsanctuaryprovince').after('<span class="error">This field is required</span>');

            	$('.fishsanctuaryprovince').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.fishsanctuaryprovince').removeClass("invalid").addClass("valid");

            	error_free = true;

            }  
            
            if ($('.fishsanctuarymunicipality').val().length < 1) {

            	$('.fishsanctuarymunicipality').after('<span class="error">This field is required</span>');

            	$('.fishsanctuarymunicipality').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.fishsanctuarymunicipality').removeClass("invalid").addClass("valid");

            	error_free = true;

            }           
            
            if ($('.fishsanctuarybarangay').val().length < 1) {

            	$('.fishsanctuarybarangay').after('<span class="error">This field is required</span>');

            	$('.fishsanctuarybarangay').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.fishsanctuarybarangay').removeClass("invalid").addClass("valid");

            	error_free = true;

            }
            
            if ($('.nameOfFishSanctuary').val().length < 1) {

            	$('.nameOfFishSanctuary').after('<span class="error">This field is required</span>');

            	$('.nameOfFishSanctuary').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.nameOfFishSanctuary').removeClass("invalid").addClass("valid");

            	error_free = true;

            }
            
            
            if ($('.code').val().length < 1) {

            	$('.code').after('<span class="error">This field is required</span>');

            	$('.code').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.code').removeClass("invalid").addClass("valid");

            	error_free = true;

            }
            
            
            if ($('.area').val().length < 1) {

            	$('.area').after('<span class="error">This field is required</span>');

            	$('.area').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.area').removeClass("invalid").addClass("valid");

            	error_free = true;

            }
            
            
            if ($('.type').val().length < 1) {

            	$('.type').after('<span class="error">This field is required</span>');

            	$('.type').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.type').removeClass("invalid").addClass("valid");

            	error_free = true;

            }
            
            
            if ($('.fishSanctuarySource').val().length < 1) {

            	$('.fishSanctuarySource').after('<span class="error">This field is required</span>');

            	$('.fishSanctuarySource').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.fishSanctuarySource').removeClass("invalid").addClass("valid");

            	error_free = true;

            }
            
            
            if ($('.remarks').val().length < 1) {

            	$('.remarks').after('<span class="error">This field is required</span>');

            	$('.remarks').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.remarks').removeClass("invalid").addClass("valid");

            	error_free = true;

            }
            
            
            if ($('.bfardenr').val().length < 1) {

            	$('.bfardenr').after('<span class="error">This field is required</span>');

            	$('.bfardenr').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.bfardenr').removeClass("invalid").addClass("valid");

            	error_free = true;

            }
            
            
            if ($('.sheltered').val().length < 1) {

            	$('.sheltered').after('<span class="error">This field is required</span>');

            	$('.sheltered').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.sheltered').removeClass("invalid").addClass("valid");

            	error_free = true;

            }           
            
            if ($('.lat').val().length < 1) {

            	$('.lat').after('<span class="error">This field is required</span>');

            	$('.lat').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.lat').removeClass("invalid").addClass("valid");

            	error_free = true;

            }
            
            
            if ($('.lon').val().length < 1) {

            	$('.lon').after('<span class="error">This field is required</span>');

            	$('.lon').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.lon').removeClass("invalid").addClass("valid");

            	error_free = true;

            }
            
            
            if ($('.image').val().length < 1) {

            	$('.image').after('<span class="error">This field is required</span>');

            	$('.image').removeClass("valid").addClass("invalid");

            	error_free = false;

            }else{

            	$('.image').removeClass("invalid").addClass("valid");

            	error_free = true;

            }
                                   
            if (!error_free){

        		e.preventDefault(); 

        		return false;

        	}

            //end//
            
            
        	      	
	        	/* CHANGE END */
	        	
			$.ajax({
				type : "POST",
				contentType : "application/json",
				
				/* CHANGE DEFEND ON CLASS NAME */
				url : "getFishSanctuary",
				/* CHANGE END */
				
				data : JSON.stringify(developerData),
				dataType : 'json',				
				success : function(data) {
					console.log("SUCCESS", data);
					
				refreshRecord(data,"FishSanctuary","old");
					
		        	$(".dateAsOf").val("");
		        	$(".region").val("");
					$(".fishsanctuaryprovince").val("");
		        	$(".fishsanctuarymunicipality").val("");
		        	$(".fishsanctuarybarangay").val("");
		        	$(".nameOfFishSanctuary").val("");
		        	$(".code").val("");
		        	$(".area").val("");
		        	$(".type").val("");
		        	$(".fishSanctuarySource").val("");
		        	$(".remarks").val("");
		        	$(".bfardenr").val("");
		        	$(".sheltered").val("");
		        	$(".lat").val("");
		        	$(".lon").val("");
		        	$(".image").val("");
					
				},
				error: function(data){
				console.log("error", data);
				}
			});
		});
		
		
		$(".submitFishProcessingPlant").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fishprocessingplantprovince").val();
	        	developerData["municipality"] =	$(".fishprocessingplantmunicipality").val();
	        	developerData["barangay"] = $(".fishprocessingplantbarangay").val();
	        	developerData["nameOfProcessingPlants"] = $(".nameOfProcessingPlants").val();
	        	developerData["nameOfOperator"] = $(".nameOfOperator").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["operatorClassification"] = $(".operatorClassification").val();
	        	developerData["processingTechnique"] = $(".processingTechnique").val();
	        	developerData["processingEnvironmentClassification"] = $(".processingEnvironmentClassification").val();
	        	developerData["packagingType"] = $(".packagingType").val();
	        	developerData["businessPermitsAndCertificateObtained"] = $(".businessPermitsAndCertificateObtained").val();
	        	developerData["bfarRegistered"] = $(".bfarRegistered").val();
	        	developerData["marketReach"] = $(".marketReach").val();
	        	developerData["indicateSpecies"] = $(".indicateSpecies").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["sourceOfData"] = $(".sourceOfData").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	
	        	
	        	//add //

	            $(".error").remove();

	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.fishprocessingplantprovince').val().length < 1) {

	            	$('.fishprocessingplantprovince').after('<span class="error">This field is required</span>');

	            	$('.fishprocessingplantprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishprocessingplantprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  	            
	            
	            if ($('.fishprocessingplantmunicipality').val().length < 1) {

	            	$('.fishprocessingplantmunicipality').after('<span class="error">This field is required</span>');

	            	$('.fishprocessingplantmunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishprocessingplantmunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }	            
	            
	            if ($('.fishprocessingplantbarangay').val().length < 1) {

	            	$('.fishprocessingplantbarangay').after('<span class="error">This field is required</span>');

	            	$('.fishprocessingplantbarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishprocessingplantbarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.nameOfProcessingPlants').val().length < 1) {

	            	$('.nameOfProcessingPlants').after('<span class="error">This field is required</span>');

	            	$('.nameOfProcessingPlants').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfProcessingPlants').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.nameOfOperator').val().length < 1) {

	            	$('.nameOfOperator').after('<span class="error">This field is required</span>');

	            	$('.nameOfOperator').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfOperator').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.operatorClassification').val().length < 1) {

	            	$('.operatorClassification').after('<span class="error">This field is required</span>');

	            	$('.operatorClassification').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.operatorClassification').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.processingTechnique').val().length < 1) {

	            	$('.processingTechnique').after('<span class="error">This field is required</span>');

	            	$('.processingTechnique').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.processingTechnique').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.processingEnvironmentClassification').val().length < 1) {

	            	$('.processingEnvironmentClassification').after('<span class="error">This field is required</span>');

	            	$('.processingEnvironmentClassification').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.processingEnvironmentClassification').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.packagingType').val().length < 1) {

	            	$('.packagingType').after('<span class="error">This field is required</span>');

	            	$('.packagingType').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.packagingType').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.businessPermitsAndCertificateObtained').val().length < 1) {

	            	$('.businessPermitsAndCertificateObtained').after('<span class="error">This field is required</span>');

	            	$('.businessPermitsAndCertificateObtained').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.businessPermitsAndCertificateObtained').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.bfarRegistered').val().length < 1) {

	            	$('.bfarRegistered').after('<span class="error">This field is required</span>');

	            	$('.bfarRegistered').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.bfarRegistered').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.marketReach').val().length < 1) {

	            	$('.marketReach').after('<span class="error">This field is required</span>');

	            	$('.marketReach').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.marketReach').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.indicateSpecies').val().length < 1) {

	            	$('.indicateSpecies').after('<span class="error">This field is required</span>');

	            	$('.indicateSpecies').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.indicateSpecies').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.sourceOfData').val().length < 1) {

	            	$('.sourceOfData').after('<span class="error">This field is required</span>');

	            	$('.sourceOfData').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.sourceOfData').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }	            
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	                                   
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}

	            //end//
	        	
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getFishProcessingPlant",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
					refreshRecord(data,"FishProcessingPlant","old");	
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".fishprocessingplantprovince").val("");
			        	$(".fishprocessingplantmunicipality").val("");
			        	$(".fishprocessingplantbarangay").val("");
			        	$(".nameOfProcessingPlants").val("");
			        	$(".nameOfOperator").val("");
			        	$(".area").val("");
			        	$(".operatorClassification").val("");
			        	$(".processingTechnique").val("");
			        	$(".processingEnvironmentClassification").val("");
			        	$(".packagingType").val("");
			        	$(".businessPermitsAndCertificateObtained").val("");
			        	$(".bfarRegistered").val("");
			        	$(".marketReach").val("");
			        	$(".indicateSpecies").val("");
			        	$(".code").val("");
			        	$(".sourceOfData").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
		
		
		
		$(".submitFishport").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fishportprovince").val();
	        	developerData["municipality"] =	$(".fishportmunicipality").val();
	        	developerData["barangay"] = $(".fishportbarangay").val();
	        	developerData["nameOfFishport"] = $(".nameOfFishport").val();
	        	developerData["nameOfCaretaker"] = $(".nameOfCaretaker").val();
	        	developerData["operatorClassification"] = $(".operatorClassification").val();
	        	developerData["pfdaPrivateMunicipal"] = $(".pfdaPrivateMunicipal").val();
	        	developerData["volumeOfUnloading"] = $(".volumeOfUnloading").val();
	        	developerData["classification"] = $(".classification").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	
	        	
	        	//add //

	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.fishportprovince').val().length < 1) {

	            	$('.fishportprovince').after('<span class="error">This field is required</span>');

	            	$('.fishportprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishportprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            if ($('.fishportmunicipality').val().length < 1) {

	            	$('.fishportmunicipality').after('<span class="error">This field is required</span>');

	            	$('.fishportmunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishportmunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.fishportbarangay').val().length < 1) {

	            	$('.fishportbarangay').after('<span class="error">This field is required</span>');

	            	$('.fishportbarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishportbarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.nameOfFishport').val().length < 1) {

	            	$('.nameOfFishport').after('<span class="error">This field is required</span>');

	            	$('.nameOfFishport').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfFishport').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.nameOfCaretaker').val().length < 1) {

	            	$('.nameOfCaretaker').after('<span class="error">This field is required</span>');

	            	$('.nameOfCaretaker').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfCaretaker').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	           	            	            
	            if ($('.operatorClassification').val().length < 1) {

	            	$('.operatorClassification').after('<span class="error">This field is required</span>');

	            	$('.operatorClassification').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.operatorClassification').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.pfdaPrivateMunicipal').val().length < 1) {

	            	$('.pfdaPrivateMunicipal').after('<span class="error">This field is required</span>');

	            	$('.pfdaPrivateMunicipal').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.pfdaPrivateMunicipal').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.volumeOfUnloading').val().length < 1) {

	            	$('.volumeOfUnloading').after('<span class="error">This field is required</span>');

	            	$('.volumeOfUnloading').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.volumeOfUnloading').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.classification').val().length < 1) {

	            	$('.classification').after('<span class="error">This field is required</span>');

	            	$('.classification').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.classification').removeClass("invalid").addClass("valid");

	            	error_free = true;
	            
	            }
	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }	            
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	                                   
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}

	            //end//
	        	
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getFishport",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"FishPort","old");		
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".fishportprovince").val("");
			        	$(".fishportmunicipality").val("");
			        	$(".fishportbarangay").val("");
			        	$(".nameOfFishport").val("");
			        	$(".nameOfCaretaker").val("");
			        	$(".operatorClassification").val("");
			        	$(".pfdaPrivateMunicipal").val("");
			        	$(".volumeOfUnloading").val("");
			        	$(".classification").val("");
			        	$(".area").val("");
			        	$(".code").val("");
			        	$(".dataSource").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
								
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
		
		
		$(".submitFishPond").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fishpondprovince").val();
	        	developerData["municipality"] =	$(".fishpondmunicipality").val();
	        	developerData["barangay"] = $(".fishpondbarangay").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["nameOfFishPondOperator"] = $(".nameOfFishPondOperator").val();
	        	developerData["nameOfCaretaker"] = $(".nameOfCaretaker").val();
	        	developerData["typeOfWater"] = $(".typeOfWater").val();
	        	developerData["fishPond"] = $(".fishPond").val();
	        	developerData["fishPondType"] = $(".fishPondType").val();
	        	developerData["speciesCultured"] = $(".speciesCultured").val();
	        	developerData["status"] = $(".status").val();
	        	developerData["kind"] = $(".kind").val();
	        	developerData["hatchery"] = $(".hatchery").val();
	        	developerData["productPer"] = $(".productPer").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        		        	
	        	
	        	//add //

	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.fishpondprovince').val().length < 1) {

	            	$('.fishpondprovince').after('<span class="error">This field is required</span>');

	            	$('.fishpondprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishpondprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            if ($('.fishpondmunicipality').val().length < 1) {

	            	$('.fishpondmunicipality').after('<span class="error">This field is required</span>');

	            	$('.fishpondmunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishpondmunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.fishpondbarangay').val().length < 1) {

	            	$('.fishpondbarangay').after('<span class="error">This field is required</span>');

	            	$('.fishpondbarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishpondbarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.nameOfFishPondOperator').val().length < 1) {

	            	$('.nameOfFishPondOperator').after('<span class="error">This field is required</span>');

	            	$('.nameOfFishPondOperator').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfFishPondOperator').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.nameOfCaretaker').val().length < 1) {

	            	$('.nameOfCaretaker').after('<span class="error">This field is required</span>');

	            	$('.nameOfCaretaker').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfCaretaker').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	           	            	            
	            if ($('.typeOfWater').val().length < 1) {

	            	$('.typeOfWater').after('<span class="error">This field is required</span>');

	            	$('.typeOfWater').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.typeOfWater').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.fishPond').val().length < 1) {

	            	$('.fishPond').after('<span class="error">This field is required</span>');

	            	$('.fishPond').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishPond').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.fishPondType').val().length < 1) {

	            	$('.fishPondType').after('<span class="error">This field is required</span>');

	            	$('.fishPondType').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishPondType').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.speciesCultured').val().length < 1) {

	            	$('.speciesCultured').after('<span class="error">This field is required</span>');

	            	$('.speciesCultured').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.speciesCultured').removeClass("invalid").addClass("valid");

	            	error_free = true;
	            
	            }
	            
	            
	            if ($('.status').val().length < 1) {

	            	$('.status').after('<span class="error">This field is required</span>');

	            	$('.status').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.status').removeClass("invalid").addClass("valid");

	            	error_free = true;
	            
	            }
	            
	            
	            if ($('.kind').val().length < 1) {

	            	$('.kind').after('<span class="error">This field is required</span>');

	            	$('.kind').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.kind').removeClass("invalid").addClass("valid");

	            	error_free = true;
	            
	            }
	            
	            
	            if ($('.hatchery').val().length < 1) {

	            	$('.hatchery').after('<span class="error">This field is required</span>');

	            	$('.hatchery').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.hatchery').removeClass("invalid").addClass("valid");

	            	error_free = true;
	            
	            }
	            
	            
	            if ($('.productPer').val().length < 1) {

	            	$('.productPer').after('<span class="error">This field is required</span>');

	            	$('.productPer').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.productPer').removeClass("invalid").addClass("valid");

	            	error_free = true;
	            
	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }

	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	                                   
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}

	            //end//
	        		 	        
	        			        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getFishPond",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"FishPond","old");			
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".fishpondprovince").val("");
			        	$(".fishpondmunicipality").val("");
			        	$(".fishpondbarangay").val("");
			        	$(".area").val("");
			        	$(".nameOfFishPondOperator").val("");
			        	$(".nameOfCaretaker").val("");
			        	$(".typeOfWater").val("");
			        	$(".fishPond").val("");
			        	$(".fishPondType").val("");
			        	$(".speciesCultured").val("");
			        	$(".status").val("");
			        	$(".kind").val("");
			        	$(".hatchery").val("");
			        	$(".productPer").val("");
			        	$(".dataSource").val("");
			        	$(".code").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
	       });

		
		
		$(".submitFishPen").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fishpenprovince").val();
	        	developerData["municipality"] =	$(".fishpenmunicipality").val();
	        	developerData["barangay"] = $(".fishpenbarangay").val();
	        	developerData["nameOfOperator"] = $(".nameOfOperator").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["noOfFishPen"] = $(".noOfFishPen").val();
	        	developerData["speciesCultured"] = $(".speciesCultured").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["croppingStart"] = $(".croppingStart").val();
	        	developerData["croppingEnd"] = $(".croppingEnd").val();
	        	developerData["sourceOfData"] = $(".sourceOfData").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        		        	
	        	
	        	//add //

	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.fishpenprovince').val().length < 1) {

	            	$('.fishpenprovince').after('<span class="error">This field is required</span>');

	            	$('.fishpenprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishpenprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            if ($('.fishpenmunicipality').val().length < 1) {

	            	$('.fishpenmunicipality').after('<span class="error">This field is required</span>');

	            	$('.fishpenmunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishpenmunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.fishpenbarangay').val().length < 1) {

	            	$('.fishpenbarangay').after('<span class="error">This field is required</span>');

	            	$('.fishpenbarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishpenbarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.nameOfOperator').val().length < 1) {

	            	$('.nameOfOperator').after('<span class="error">This field is required</span>');

	            	$('.nameOfOperator').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfOperator').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            
	            	           	            	            
	            if ($('.noOfFishPen').val().length < 1) {

	            	$('.noOfFishPen').after('<span class="error">This field is required</span>');

	            	$('.noOfFishPen').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.noOfFishPen').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.speciesCultured').val().length < 1) {

	            	$('.speciesCultured').after('<span class="error">This field is required</span>');

	            	$('.speciesCultured').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.speciesCultured').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.croppingStart').val().length < 1) {

	            	$('.croppingStart').after('<span class="error">This field is required</span>');

	            	$('.croppingStart').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.croppingStart').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.croppingEnd').val().length < 1) {

	            	$('.croppingEnd').after('<span class="error">This field is required</span>');

	            	$('.croppingEnd').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.croppingEnd').removeClass("invalid").addClass("valid");

	            	error_free = true;
	            
	            }
	            
	            
	            if ($('.sourceOfData').val().length < 1) {

	            	$('.sourceOfData').after('<span class="error">This field is required</span>');

	            	$('.sourceOfData').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.sourceOfData').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	                                   
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}

	            //end//
	        	
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getFishPen",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"FishPen","old");	
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".fishpenprovince").val("");
			        	$(".fishpenmunicipality").val("");
			        	$(".fishpenbarangay").val("");
			        	$(".nameOfOperator").val("");
			        	$(".area").val("");
			        	$(".noOfFishPen").val("");
			        	$(".speciesCultured").val("");
			        	$(".code").val("");
			        	$(".croppingStart").val("");
			        	$(".croppingEnd").val("");
			        	$(".sourceOfData").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
		
		
		$(".submitFishLanding").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
				
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fishlandingprovince").val();
	        	developerData["municipality"] =	$(".fishlandingmunicipality").val();
	        	developerData["barangay"] = $(".fishlandingbarangay").val();
	        	developerData["nameOfLanding"] = $(".nameOfLanding").val();
	        	developerData["classification"] = $(".classification").val();
	        	developerData["volumeOfUnloadingMT"] = $(".volumeOfUnloadingMT").val();
	        	developerData["type"] = $(".type").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	
	        	
	        	//add //

	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.fishlandingprovince').val().length < 1) {

	            	$('.fishlandingprovince').after('<span class="error">This field is required</span>');

	            	$('.fishlandingprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishlandingprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            if ($('.fishlandingmunicipality').val().length < 1) {

	            	$('.fishlandingmunicipality').after('<span class="error">This field is required</span>');

	            	$('.fishlandingmunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishlandingmunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.fishlandingbarangay').val().length < 1) {

	            	$('.fishlandingbarangay').after('<span class="error">This field is required</span>');

	            	$('.fishlandingbarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishlandingbarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.nameOfLanding').val().length < 1) {

	            	$('.nameOfLanding').after('<span class="error">This field is required</span>');

	            	$('.nameOfLanding').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfLanding').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
   	            
	            if ($('.classification').val().length < 1) {

	            	$('.classification').after('<span class="error">This field is required</span>');

	            	$('.classification').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.classification').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.volumeOfUnloadingMT').val().length < 1) {

	            	$('.volumeOfUnloadingMT').after('<span class="error">This field is required</span>');

	            	$('.volumeOfUnloadingMT').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.volumeOfUnloadingMT').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.type').val().length < 1) {

	            	$('.type').after('<span class="error">This field is required</span>');

	            	$('.type').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.type').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	                                   
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}

	            //end//
	        	
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getFishLanding",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"FishLanding","old");	
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".fishlandingprovince").val("");
			        	$(".fishlandingmunicipality").val("");
			        	$(".fishlandingbarangay").val("");
			        	$("nameOfLanding").val("");
			        	$(".classification").val("");
			        	$(".volumeOfUnloadingMT").val("");
			        	$(".type").val("");
			        	$(".area").val("");
			        	$(".code").val("");
			        	$(".dataSource").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});

		
			
		$(".submitFishCoral").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fishcorralsprovince").val();
	        	developerData["municipality"] =	$(".fishcorralsmunicipality").val();
	        	developerData["barangay"] = $(".fishcorralsbarangay").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["nameOfOperator"] = $(".nameOfOperator").val();
	        	developerData["operatorClassification"] = $(".operatorClassification").val();
	        	developerData["nameOfStationGearUse"] = $(".nameOfStationGearUse").val();
	        	developerData["noOfUnitUse"] = $(".noOfUnitUse").val();
	        	developerData["fishesCaught"] = $(".fishesCaught").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	
	        	
	        	//add //

	            $(".error").remove();

	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.fishcorralsprovince').val().length < 1) {

	            	$('.fishcorralsprovince').after('<span class="error">This field is required</span>');

	            	$('.fishcorralsprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishcorralsprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            if ($('.fishcorralsmunicipality').val().length < 1) {

	            	$('.fishcorralsmunicipality').after('<span class="error">This field is required</span>');

	            	$('.fishcorralsmunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishcorralsmunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.fishcorralsbarangay').val().length < 1) {

	            	$('.fishcorralsbarangay').after('<span class="error">This field is required</span>');

	            	$('.fishcorralsbarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishcorralsbarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }	            	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.nameOfOperator').val().length < 1) {

	            	$('.nameOfOperator').after('<span class="error">This field is required</span>');

	            	$('.nameOfOperator').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfOperator').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
   	            
	            if ($('.operatorClassification').val().length < 1) {

	            	$('.operatorClassification').after('<span class="error">This field is required</span>');

	            	$('.operatorClassification').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.operatorClassification').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.nameOfStationGearUse').val().length < 1) {

	            	$('.nameOfStationGearUse').after('<span class="error">This field is required</span>');

	            	$('.nameOfStationGearUse').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfStationGearUse').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.noOfUnitUse').val().length < 1) {

	            	$('.noOfUnitUse').after('<span class="error">This field is required</span>');

	            	$('.noOfUnitUse').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.noOfUnitUse').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            	            
	            if ($('.fishesCaught').val().length < 1) {

	            	$('.fishesCaught').after('<span class="error">This field is required</span>');

	            	$('.fishesCaught').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishesCaught').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	                                   
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}

	            //end//
	        	
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getFishCoral",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"fishcorals","old");	
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						//$(".fishcoralprovince").val("");
			        	$(".fishcorralsmunicipality").val("");
			        	$(".fishcorralsbarangay").val("");
			        	$(".area").val("");
			        	$(".nameOfOperator").val("");
			        	$(".operatorClassification").val("");
			        	$(".nameOfStationGearUse").val("");
			        	$(".noOfUnitUse").val("");
			        	$(".fishesCaught").val("");
			        	$(".dataSource").val("");
			        	$(".code").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
					
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
	
				
		$(".submitFishCage").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fishcageprovince").val();
	        	developerData["municipality"] =	$(".fishcagemunicipality").val();
	        	developerData["barangay"] = $(".fishcagebarangay").val();
	        	developerData["nameOfOperator"] = $(".nameOfOperator").val();
	        	developerData["classificationofOperator"] = $(".classificationofOperator").val();
	        	developerData["cageDimension"] = $(".cageDimension").val();
	        	developerData["cageTotalArea"] = $(".cageTotalArea").val();
	        	developerData["cageType"] = $(".cageType").val();
	        	developerData["cageNoOfCompartments"] = $(".cageNoOfCompartments").val();
	        	developerData["indicateSpecies"] = $(".indicateSpecies").val();
	        	developerData["sourceOfData"] = $(".sourceOfData").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	
	        	
	        	//add//
	        	
	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.fishcageprovince').val().length < 1) {

	            	$('.fishcageprovince').after('<span class="error">This field is required</span>');

	            	$('.fishcageprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishcageprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            if ($('.fishcagemunicipality').val().length < 1) {

	            	$('.fishcagemunicipality').after('<span class="error">This field is required</span>');

	            	$('.fishcagemunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishcagemunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.fishcagebarangay').val().length < 1) {

	            	$('.fishcagebarangay').after('<span class="error">This field is required</span>');

	            	$('.fishcagebarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fishcagebarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.nameOfOperator').val().length < 1) {

	            	$('.nameOfOperator').after('<span class="error">This field is required</span>');

	            	$('.nameOfOperator').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfOperator').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.classificationofOperator').val().length < 1) {

	            	$('.classificationofOperator').after('<span class="error">This field is required</span>');

	            	$('.classificationofOperator').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.classificationofOperator').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.cageDimension').val().length < 1) {

	            	$('.cageDimension').after('<span class="error">This field is required</span>');

	            	$('.cageDimension').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.cageDimension').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.cageTotalArea').val().length < 1) {

	            	$('.cageTotalArea').after('<span class="error">This field is required</span>');

	            	$('.cageTotalArea').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.cageTotalArea').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.cageType').val().length < 1) {

	            	$('.cageType').after('<span class="error">This field is required</span>');

	            	$('.cageType').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.cageType').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.cageNoOfCompartments').val().length < 1) {

	            	$('.cageNoOfCompartments').after('<span class="error">This field is required</span>');

	            	$('.cageNoOfCompartments').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.cageNoOfCompartments').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.indicateSpecies').val().length < 1) {

	            	$('.indicateSpecies').after('<span class="error">This field is required</span>');

	            	$('.indicateSpecies').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.indicateSpecies').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.sourceOfData').val().length < 1) {

	            	$('.sourceOfData').after('<span class="error">This field is required</span>');

	            	$('.sourceOfData').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.sourceOfData').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	                                   
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}
	            
	            //end//
	        			        	
		        	/* CHANGE END */
	            
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getFishCage",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"FishCage","old");							
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".fishcageprovince").val("");
			        	$(".fishcagemunicipality").val("");
			        	$(".fishcagebarangay").val("");
			        	$(".nameOfOperator").val("");
			        	$(".classificationofOperator").val("");
			        	$(".cageDimension").val("");
			        	$(".cageTotalArea").val("");
			        	$(".cageType").val("");
			        	$(".cageNoOfCompartments").val("");
			        	$(".indicateSpecies").val("");
			        	$(".sourceOfData").val("");
			        	$(".area").val("");
			        	$(".code").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});

		
		
		$(".submitConsignation").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".province").val();
	        	developerData["municipality"] =	$(".municipality").val();
	        	developerData["barangay"] = $(".barangay").val();
	        	developerData["nameOfConsignation"] = $(".nameOfConsignation").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["publicprivate"] = $(".publicprivate").val();
	        	developerData["volumeTraded"] = $(".volumeTraded").val();
	        	developerData["accreditations"] = $(".accreditations").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["namePerPage"] = $(".namePerPage").val();
	        	
	        	//add //
	        	
	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.province').val().length < 1) {

	            	$('.province').after('<span class="error">This field is required</span>');

	            	$('.province').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.province').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            
	            if ($('.municipality').val().length < 1) {

	            	$('.municipality').after('<span class="error">This field is required</span>');

	            	$('.municipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.municipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.barangay').val().length < 1) {

	            	$('.barangay').after('<span class="error">This field is required</span>');

	            	$('.barangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.barangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.nameOfConsignation').val().length < 1) {

	            	$('.nameOfConsignation').after('<span class="error">This field is required</span>');

	            	$('.nameOfConsignation').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfConsignation').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.publicprivate').val().length < 1) {

	            	$('.publicprivate').after('<span class="error">This field is required</span>');

	            	$('.publicprivate').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.publicprivate').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.volumeTraded').val().length < 1) {

	            	$('.volumeTraded').after('<span class="error">This field is required</span>');

	            	$('.volumeTraded').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.volumeTraded').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.accreditations').val().length < 1) {

	            	$('.accreditations').after('<span class="error">This field is required</span>');

	            	$('.accreditations').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.accreditations').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }	            
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.namePerPage').val().length < 1) {

	            	$('.namePerPage').after('<span class="error">This field is required</span>');

	            	$('.namePerPage').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.namePerPage').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	                                   
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}
	            
	            //end //
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getCosignation",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"Consignation","old");	
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".province").val("");
			        	$(".municipality").val("");
			        	$(".barangay").val("");
			        	$(".nameOfConsignation").val("");
			        	$(".area").val("");
			        	$(".code").val("");
			        	$(".publicprivate").val("");
			        	$(".volumeTraded").val("");
			        	$(".accreditations").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".namePerPage").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
		
		
		
		$(".submitHatchery").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".province").val();
	        	developerData["municipality"] =	$(".municipality").val();
	        	developerData["barangay"] = $(".barangay").val();
	        	developerData["nameOfHatchery"] = $(".nameOfHatchery").val();
	        	developerData["nameOfOperator"] = $(".nameOfOperator").val();
	        	developerData["addressOfOperator"] = $(".addressOfOperator").val();
	        	developerData["operatorClassification"] = $(".operatorClassification").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["publicprivate"] = $(".publicprivate").val();
	        	developerData["indicateSpecies"] = $(".indicateSpecies").val();
	        	developerData["prodStocking"] = $(".prodStocking").val();
	        	developerData["prodCycle"] = $(".prodCycle").val();
	        	developerData["actualProdForTheYear"] = $(".actualProdForTheYear").val();
	        	developerData["monthStart"] = $(".monthStart").val();
	        	developerData["monthEnd"] = $(".monthEnd").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	
	        	//add //
	        	
	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.province').val().length < 1) {

	            	$('.province').after('<span class="error">This field is required</span>');

	            	$('.province').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.province').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            
	            if ($('.municipality').val().length < 1) {

	            	$('.municipality').after('<span class="error">This field is required</span>');

	            	$('.municipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.municipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.barangay').val().length < 1) {

	            	$('.barangay').after('<span class="error">This field is required</span>');

	            	$('.barangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.barangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            
	            if ($('.nameOfHatchery').val().length < 1) {

	            	$('.nameOfHatchery').after('<span class="error">This field is required</span>');

	            	$('.nameOfHatchery').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfHatchery').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.nameOfOperator').val().length < 1) {

	            	$('.nameOfOperator').after('<span class="error">This field is required</span>');

	            	$('.nameOfOperator').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfOperator').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.addressOfOperator').val().length < 1) {

	            	$('.addressOfOperator').after('<span class="error">This field is required</span>');

	            	$('.addressOfOperator').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.addressOfOperator').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.operatorClassification').val().length < 1) {

	            	$('.operatorClassification').after('<span class="error">This field is required</span>');

	            	$('.operatorClassification').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.operatorClassification').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.publicprivate').val().length < 1) {

	            	$('.publicprivate').after('<span class="error">This field is required</span>');

	            	$('.publicprivate').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.publicprivate').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.indicateSpecies').val().length < 1) {

	            	$('.indicateSpecies').after('<span class="error">This field is required</span>');

	            	$('.indicateSpecies').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.indicateSpecies').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.prodStocking').val().length < 1) {

	            	$('.prodStocking').after('<span class="error">This field is required</span>');

	            	$('.prodStocking').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.prodStocking').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.prodCycle').val().length < 1) {

	            	$('.prodCycle').after('<span class="error">This field is required</span>');

	            	$('.prodCycle').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.prodCycle').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            
	            
	            if ($('.actualProdForTheYear').val().length < 1) {

	            	$('.actualProdForTheYear').after('<span class="error">This field is required</span>');

	            	$('.actualProdForTheYear').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.actualProdForTheYear').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            
	            
	            if ($('.monthStart').val().length < 1) {

	            	$('.monthStart').after('<span class="error">This field is required</span>');

	            	$('.monthStart').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.monthStart').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            
	            
	            if ($('.monthEnd').val().length < 1) {

	            	$('.monthEnd').after('<span class="error">This field is required</span>');

	            	$('.monthEnd').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.monthEnd').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	                                   
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}
	            
	        	//end //
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getHatchery",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"Hatchery","old");	
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".province").val("");
			        	$(".municipality").val("");
			        	$(".barangay").val("");
			        	$(".nameOfHatchery").val("");
			        	$(".nameOfOperator").val("");
			        	$(".addressOfOperator").val("");
			        	$(".operatorClassification").val("");
			        	$(".area").val("");
			        	$(".publicprivate").val("");
			        	$(".indicateSpecies").val("");
			        	$(".prodStocking").val("");
			        	$(".prodCycle").val("");
			        	$(".actualProdForTheYear").val("");
			        	$(".monthStart").val("");
			        	$(".monthEnd").val("");
			        	$(".dataSource").val("");
			        	$(".code").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
		
		
		
		$(".submitIcePlantColdStorage").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".iceplantorcoldstorageprovince").val();
	        	developerData["municipality"] =	$(".iceplantorcoldstoragemunicipality").val();
	        	developerData["barangay"] = $(".iceplantorcoldstoragebarangay").val();
	        	developerData["nameOfIcePlant"] = $(".nameOfIcePlant").val();
	        	developerData["sanitaryPermit"] = $(".sanitaryPermit").val();
	        	developerData["operator"] = $(".operator").val();
	        	developerData["typeOfFacility"] = $(".typeOfFacility").val();
	        	developerData["structureType"] = $(".structureType").val();
	        	developerData["capacity"] = $(".capacity").val();
	        	developerData["withValidLicenseToOperate"] = $(".withValidLicenseToOperate").val();
	        	developerData["withValidSanitaryPermit"] = $(".withValidSanitaryPermit").val();
	        	developerData["businessPermitsAndCertificateObtained"] = $(".businessPermitsAndCertificateObtained").val();
	        	developerData["typeOfIceMaker"] = $(".typeOfIceMaker").val();
	        	developerData["foodGradule"] = $(".foodGradule").val();
	        	developerData["sourceOfEquipment"] = $(".sourceOfEquipment").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	
	        	//add //
	        	
	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.iceplantorcoldstorageprovince').val().length < 1) {

	            	$('.iceplantorcoldstorageprovince').after('<span class="error">This field is required</span>');

	            	$('.iceplantorcoldstorageprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.iceplantorcoldstorageprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            if ($('.iceplantorcoldstoragemunicipality').val().length < 1) {

	            	$('.iceplantorcoldstoragemunicipality').after('<span class="error">This field is required</span>');

	            	$('.iceplantorcoldstoragemunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.iceplantorcoldstoragemunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.iceplantorcoldstoragebarangay').val().length < 1) {

	            	$('.iceplantorcoldstoragebarangay').after('<span class="error">This field is required</span>');

	            	$('.iceplantorcoldstoragebarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.iceplantorcoldstoragebarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            	            
	            if ($('.nameOfIcePlant').val().length < 1) {

	            	$('.nameOfIcePlant').after('<span class="error">This field is required</span>');

	            	$('.nameOfIcePlant').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfIcePlant').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.sanitaryPermit').val().length < 1) {

	            	$('.sanitaryPermit').after('<span class="error">This field is required</span>');

	            	$('.sanitaryPermit').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.sanitaryPermit').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.operator').val().length < 1) {

	            	$('.operator').after('<span class="error">This field is required</span>');

	            	$('.operator').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.operator').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.typeOfFacility').val().length < 1) {

	            	$('.typeOfFacility').after('<span class="error">This field is required</span>');

	            	$('.typeOfFacility').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.typeOfFacility').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.structureType').val().length < 1) {

	            	$('.structureType').after('<span class="error">This field is required</span>');

	            	$('.structureType').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.structureType').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.capacity').val().length < 1) {

	            	$('.capacity').after('<span class="error">This field is required</span>');

	            	$('.capacity').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.capacity').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.withValidLicenseToOperate').val().length < 1) {

	            	$('.withValidLicenseToOperate').after('<span class="error">This field is required</span>');

	            	$('.withValidLicenseToOperate').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.withValidLicenseToOperate').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.withValidSanitaryPermit').val().length < 1) {

	            	$('.withValidSanitaryPermit').after('<span class="error">This field is required</span>');

	            	$('.withValidSanitaryPermit').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.withValidSanitaryPermit').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            
	            
	            if ($('.businessPermitsAndCertificateObtained').val().length < 1) {

	            	$('.businessPermitsAndCertificateObtained').after('<span class="error">This field is required</span>');

	            	$('.businessPermitsAndCertificateObtained').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.businessPermitsAndCertificateObtained').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            
	            
	            if ($('.typeOfIceMaker').val().length < 1) {

	            	$('.typeOfIceMaker').after('<span class="error">This field is required</span>');

	            	$('.typeOfIceMaker').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.typeOfIceMaker').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            
	            
	            if ($('.monthEnd').val().length < 1) {

	            	$('.monthEnd').after('<span class="error">This field is required</span>');

	            	$('.monthEnd').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.monthEnd').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	                                   
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}
	            
	            //end //
	        			        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getIcePlantColdStorage",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"IcePlantColdStorage","old");	
						
			        	$(".region").val("");
						$(".iceplantorcoldstorageprovince").val("");
			        	$(".iceplantorcoldstoragemunicipality").val("");
			        	$(".iceplantorcoldstoragebarangay").val("");
			        	$(".nameOfIcePlant").val("");
			        	$(".sanitaryPermit").val("");
			        	$(".operator").val("");
			        	$(".typeOfFacility").val("");
			        	$(".structureType").val("");
			        	$(".capacity").val("");
			        	$(".withValidLicenseToOperate").val("");
			        	$(".withValidSanitaryPermit").val("");
			        	$(".businessPermitsAndCertificateObtained").val("");
			        	$(".typeOfIceMaker").val("");
			        	$(".foodGradule").val("");
			        	$(".sourceOfEquipment").val("");
			        	$(".area").val("");
			        	$(".code").val("");
			        	$(".dataSource").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
					
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});

		
		
		$(".submitLGU").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".localgovernmentunitprovince").val();
	        	developerData["municipality"] =	$(".localgovernmentunitmunicipality").val();
	        	developerData["barangay"] = $(".localgovernmentunitbarangay").val();
	        	developerData["lguName"] = $(".lguName").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["noOfBarangayCoastal"] = $(".noOfBarangayCoastal").val();
	        	developerData["noOfBarangayInland"] = $(".noOfBarangayInland").val();
	        	developerData["lguCoastalLength"] = $(".lguCoastalLength").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	
	        	//add //
	        	
	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.localgovernmentunitprovince').val().length < 1) {

	            	$('.localgovernmentunitprovince').after('<span class="error">This field is required</span>');

	            	$('.localgovernmentunitprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.localgovernmentunitprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            if ($('.localgovernmentunitmunicipality').val().length < 1) {

	            	$('.localgovernmentunitmunicipality').after('<span class="error">This field is required</span>');

	            	$('.localgovernmentunitmunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.localgovernmentunitmunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.localgovernmentunitbarangay').val().length < 1) {

	            	$('.localgovernmentunitbarangay').after('<span class="error">This field is required</span>');

	            	$('.localgovernmentunitbarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.localgovernmentunitbarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            
	            if ($('.lguName').val().length < 1) {

	            	$('.lguName').after('<span class="error">This field is required</span>');

	            	$('.lguName').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lguName').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.noOfBarangayCoastal').val().length < 1) {

	            	$('.noOfBarangayCoastal').after('<span class="error">This field is required</span>');

	            	$('.noOfBarangayCoastal').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.noOfBarangayCoastal').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.noOfBarangayInland').val().length < 1) {

	            	$('.noOfBarangayInland').after('<span class="error">This field is required</span>');

	            	$('.noOfBarangayInland').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.noOfBarangayInland').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lguCoastalLength').val().length < 1) {

	            	$('.lguCoastalLength').after('<span class="error">This field is required</span>');

	            	$('.lguCoastalLength').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lguCoastalLength').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	              
	            
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}
	            
	        	//end //
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getLGU",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
					refreshRecord(data,"LGU","old");
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".localgovernmentunitprovince").val("");
			        	$(".localgovernmentunitmunicipality").val("");
			        	$(".localgovernmentunitbarangay").val("");
			        	$(".dataSource").val("");
			        	$(".noOfBarangayCoastal").val("");
			        	$(".noOfBarangayInland").val("");
			        	$(".lguCoastalLength").val("");
			        	$(".area").val("");
			        	$(".code").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
		
		
		
		$(".submitMangrove").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".mangroveprovince").val();
	        	developerData["municipality"] =	$(".mangrovemunicipality").val();
	        	developerData["barangay"] = $(".mangrovebarangay").val();
	        	developerData["indicateSpecies"] = $(".indicateSpecies").val();
	        	developerData["type"] = $(".type").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	
	        	//add //
	        	
	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.mangroveprovince').val().length < 1) {

	            	$('.mangroveprovince').after('<span class="error">This field is required</span>');

	            	$('.mangroveprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.mangroveprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            if ($('.mangrovemunicipality').val().length < 1) {

	            	$('.mangrovemunicipality').after('<span class="error">This field is required</span>');

	            	$('.mangrovemunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.mangrovemunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.mangrovebarangay').val().length < 1) {

	            	$('.mangrovebarangay').after('<span class="error">This field is required</span>');

	            	$('.mangrovebarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.mangrovebarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            
	            if ($('.indicateSpecies').val().length < 1) {

	            	$('.indicateSpecies').after('<span class="error">This field is required</span>');

	            	$('.indicateSpecies').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.indicateSpecies').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.type').val().length < 1) {

	            	$('.type').after('<span class="error">This field is required</span>');

	            	$('.type').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.type').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	              
	            
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}
	            
	        	//end //
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getMangrove",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
					refreshRecord(data,"Mangrove","old");
						
			        	$(".dateAsOf").val('');
			        	$(".region").val("");
						$(".mangroveprovince").val("");
			        	$(".mangrovemunicipality").val("");
			        	$(".mangrovebarangay").val("");
			        	$(".indicateSpecies").val("");
			        	$(".type").val("");
			        	$(".area").val("");
			        	$(".code").val("");
			        	$(".dataSource").val("");
			        	$(".remarks").val("");
			        	$(".encodedBy").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
		
		
		
		$(".submitMaricultureZone").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */

	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".mariculturezoneprovince").val();
	        	developerData["municipality"] =	$(".mariculturezonemunicipality").val();
	        	developerData["barangay"] = $(".mariculturezonebarangay").val();
	        	developerData["nameOfMariculture"] = $(".nameOfMariculture").val();
	        	developerData["maricultureType"] = $(".maricultureType").val();
	        	developerData["eCcNumber"] = $(".eCcNumber").val();
	        	developerData["individual"] = $(".individual").val();
	        	developerData["partnership"] = $(".partnership").val();
	        	developerData["cooperativeAssociation"] = $(".cooperativeAssociation").val();
	        	developerData["cageType"] = $(".cageType").val();
	        	developerData["cageSize"] = $(".cageSize").val();
	        	developerData["cageQuantity"] = $(".cageQuantity").val();
	        	developerData["speciesCultured"] = $(".speciesCultured").val();
	        	developerData["quantityMt"] = $(".quantityMt").val();
	        	developerData["kind"] = $(".kind").val();
	        	developerData["productionPerCropping"] = $(".productionPerCropping").val();
	        	developerData["numberofCropping"] = $(".numberofCropping").val();
	        	developerData["species"] = $(".species").val();
	        	developerData["farmProduce"] = $(".farmProduce").val();
	        	developerData["productDestination"] = $(".productDestination").val();
	        	developerData["aquacultureProduction"] = $(".aquacultureProduction").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["dateLaunched"] = $(".dateLaunched").val();
	        	developerData["dateInacted"] = $(".dateInacted").val();
	        	developerData["dateApproved"] = $(".dateApproved").val();
	        	developerData["publicprivate"] = $(".publicprivate").val();
	        	developerData["volumeTraded"] = $(".volumeTraded").val();
	        	developerData["accreditations"] = $(".accreditations").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["municipalOrdinanceNo"] = $(".municipalOrdinanceNo").val();
	        	
	        	//add //
	        	
	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.mariculturezoneprovince').val().length < 1) {

	            	$('.mariculturezoneprovince').after('<span class="error">This field is required</span>');

	            	$('.mariculturezoneprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.mariculturezoneprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  	            
	            
	            if ($('.mariculturezonemunicipality').val().length < 1) {

	            	$('.mariculturezonemunicipality').after('<span class="error">This field is required</span>');

	            	$('.mariculturezonemunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.mariculturezonemunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.mariculturezonebarangay').val().length < 1) {

	            	$('.mariculturezonebarangay').after('<span class="error">This field is required</span>');

	            	$('.mariculturezonebarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.mariculturezonebarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            	            
	            if ($('.nameOfMariculture').val().length < 1) {

	            	$('.nameOfMariculture').after('<span class="error">This field is required</span>');

	            	$('.nameOfMariculture').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfMariculture').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.maricultureType').val().length < 1) {

	            	$('.maricultureType').after('<span class="error">This field is required</span>');

	            	$('.maricultureType').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.maricultureType').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.eCcNumber').val().length < 1) {

	            	$('.eCcNumber').after('<span class="error">This field is required</span>');

	            	$('.eCcNumber').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.eCcNumber').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.individual').val().length < 1) {

	            	$('.individual').after('<span class="error">This field is required</span>');

	            	$('.individual').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.individual').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.partnership').val().length < 1) {

	            	$('.partnership').after('<span class="error">This field is required</span>');

	            	$('.partnership').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.partnership').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.cooperativeAssociation').val().length < 1) {

	            	$('.cooperativeAssociation').after('<span class="error">This field is required</span>');

	            	$('.cooperativeAssociation').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.cooperativeAssociation').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.cageType').val().length < 1) {

	            	$('.cageType').after('<span class="error">This field is required</span>');

	            	$('.cageType').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.cageType').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.cageSize').val().length < 1) {

	            	$('.cageSize').after('<span class="error">This field is required</span>');

	            	$('.cageSize').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.cageSize').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.cageQuantity').val().length < 1) {

	            	$('.cageQuantity').after('<span class="error">This field is required</span>');

	            	$('.cageQuantity').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.cageQuantity').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.speciesCultured').val().length < 1) {

	            	$('.speciesCultured').after('<span class="error">This field is required</span>');

	            	$('.speciesCultured').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.speciesCultured').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.quantityMt').val().length < 1) {

	            	$('.quantityMt').after('<span class="error">This field is required</span>');

	            	$('.quantityMt').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.quantityMt').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.kind').val().length < 1) {

	            	$('.kind').after('<span class="error">This field is required</span>');

	            	$('.kind').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.kind').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.productionPerCropping').val().length < 1) {

	            	$('.productionPerCropping').after('<span class="error">This field is required</span>');

	            	$('.productionPerCropping').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.productionPerCropping').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.numberofCropping').val().length < 1) {

	            	$('.numberofCropping').after('<span class="error">This field is required</span>');

	            	$('.numberofCropping').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.numberofCropping').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.species').val().length < 1) {

	            	$('.species').after('<span class="error">This field is required</span>');

	            	$('.species').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.species').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.farmProduce').val().length < 1) {

	            	$('.farmProduce').after('<span class="error">This field is required</span>');

	            	$('.farmProduce').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.farmProduce').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.productDestination').val().length < 1) {

	            	$('.productDestination').after('<span class="error">This field is required</span>');

	            	$('.productDestination').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.productDestination').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.aquacultureProduction').val().length < 1) {

	            	$('.aquacultureProduction').after('<span class="error">This field is required</span>');

	            	$('.aquacultureProduction').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.aquacultureProduction').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dateLaunched').val().length < 1) {

	            	$('.dateLaunched').after('<span class="error">This field is required</span>');

	            	$('.dateLaunched').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateLaunched').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dateInacted').val().length < 1) {

	            	$('.dateInacted').after('<span class="error">This field is required</span>');

	            	$('.dateInacted').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateInacted').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dateApproved').val().length < 1) {

	            	$('.dateApproved').after('<span class="error">This field is required</span>');

	            	$('.dateApproved').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateApproved').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.publicprivate').val().length < 1) {

	            	$('.publicprivate').after('<span class="error">This field is required</span>');

	            	$('.publicprivate').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.publicprivate').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.volumeTraded').val().length < 1) {

	            	$('.volumeTraded').after('<span class="error">This field is required</span>');

	            	$('.volumeTraded').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.volumeTraded').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.accreditations').val().length < 1) {

	            	$('.accreditations').after('<span class="error">This field is required</span>');

	            	$('.accreditations').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.accreditations').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }            
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	              
	            
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}
	            
	        	//end //
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getMaricultureZone",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"MaricultureZone","old");
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".mariculturezoneprovince").val("");
			        	$(".mariculturezonemunicipality").val("");
			        	$(".mariculturezonebarangay").val("");
			        	$(".nameOfMariculture").val("");
			        	$(".maricultureType").val("");
			        	$(".eCcNumber").val("");
			        	$(".individual").val("");
			        	$(".partnership").val("");
			        	$(".cooperativeAssociation").val("");
			        	$(".cageType").val("");
			        	$(".cageSize").val("");
			        	$(".cageQuantity").val("");
			        	$(".speciesCultured").val("");
			        	$(".quantityMt").val("");
			        	$(".kind").val("");
			        	$(".productionPerCropping").val("");
			        	$(".numberofCropping").val("");
			        	$(".species").val("");
			        	$(".farmProduce").val("");
			        	$(".productDestination").val("");
			        	$(".aquacultureProduction").val("");
			        	$(".code").val("");
			        	$(".area").val("");
			        	$(".dateLaunched").val("");
			        	$(".dateInacted").val("");
			        	$(".dateApproved").val("");
			        	$(".publicprivate").val("");
			        	$(".volumeTraded").val("");
			        	$(".accreditations").val("");
			        	$(".remarks").val("");
			        	$(".dataSource").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".municipalOrdinanceNo").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
		
		
		
		$(".submitMarineProtectedArea").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".marineprotectedareaprovince").val();
	        	developerData["municipality"] =	$(".marineprotectedareamunicipality").val();
	        	developerData["barangay"] = $(".marineprotectedareabarangay").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["nameOfMarineProtectedArea"] = $(".nameOfMarineProtectedArea").val();
	        	developerData["type"] = $(".type").val();
	        	developerData["sensitiveHabitatWithMPA"] = $(".sensitiveHabitatWithMPA").val();
	        	developerData["dateEstablishment"] = $(".dateEstablishment").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	
	        	
	        	//add //
	        	
	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.marineprotectedareaprovince').val().length < 1) {

	            	$('.marineprotectedareaprovince').after('<span class="error">This field is required</span>');

	            	$('.marineprotectedareaprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.marineprotectedareaprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            if ($('.marineprotectedareamunicipality').val().length < 1) {

	            	$('.marineprotectedareamunicipality').after('<span class="error">This field is required</span>');

	            	$('.marineprotectedareamunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.marineprotectedareamunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.marineprotectedareabarangay').val().length < 1) {

	            	$('.marineprotectedareabarangay').after('<span class="error">This field is required</span>');

	            	$('.marineprotectedareabarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.marineprotectedareabarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            	            
	            if ($('.nameOfMarineProtectedArea').val().length < 1) {

	            	$('.nameOfMarineProtectedArea').after('<span class="error">This field is required</span>');

	            	$('.nameOfMarineProtectedArea').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfMarineProtectedArea').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.type').val().length < 1) {

	            	$('.type').after('<span class="error">This field is required</span>');

	            	$('.type').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.type').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.sensitiveHabitatWithMPA').val().length < 1) {

	            	$('.sensitiveHabitatWithMPA').after('<span class="error">This field is required</span>');

	            	$('.sensitiveHabitatWithMPA').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.sensitiveHabitatWithMPA').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dateEstablishment').val().length < 1) {

	            	$('.dateEstablishment').after('<span class="error">This field is required</span>');

	            	$('.dateEstablishment').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateEstablishment').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	              
	            
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}
	            
	        	//end //
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getMarineProtectedArea",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"MarineProtectedArea","old");
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".marineprotectedareaprovince").val("");
			        	$(".marineprotectedareamunicipality").val("");
			        	$(".marineprotectedareabarangay").val("");
			        	$(".area").val("");
			        	$(".nameOfMarineProtectedArea").val("");
			        	$(".type").val("");
			        	$(".sensitiveHabitatWithMPA").val("");
			        	$(".dateEstablishment").val();
			        	$(".code").val("");
			        	$(".dataSource").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});

		
		
		$(".submitMarket").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".marketprovince").val();
	        	developerData["municipality"] =	$(".marketmunicipality").val();
	        	developerData["barangay"] = $(".marketbarangay").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["nameOfMarket"] = $(".nameOfMarket").val();
	        	developerData["publicprivate"] = $(".publicprivate").val();
	        	developerData["majorMinorMarket"] = $(".majorMinorMarket").val();
	        	developerData["volumeTraded"] = $(".volumeTraded").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	
	        	
	        	//add //
	        	
	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.marketprovince').val().length < 1) {

	            	$('.marketprovince').after('<span class="error">This field is required</span>');

	            	$('.marketprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.marketprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            if ($('.marketmunicipality').val().length < 1) {

	            	$('.marketmunicipality').after('<span class="error">This field is required</span>');

	            	$('.marketmunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.marketmunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.marketbarangay').val().length < 1) {

	            	$('.marketbarangay').after('<span class="error">This field is required</span>');

	            	$('.marketbarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.marketbarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            	            
	            if ($('.nameOfMarket').val().length < 1) {

	            	$('.nameOfMarket').after('<span class="error">This field is required</span>');

	            	$('.nameOfMarket').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.nameOfMarket').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
   	            
	            if ($('.publicprivate').val().length < 1) {

	            	$('.publicprivate').after('<span class="error">This field is required</span>');

	            	$('.publicprivate').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.publicprivate').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.majorMinorMarket').val().length < 1) {

	            	$('.majorMinorMarket').after('<span class="error">This field is required</span>');

	            	$('.majorMinorMarket').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.majorMinorMarket').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.volumeTraded').val().length < 1) {

	            	$('.volumeTraded').after('<span class="error">This field is required</span>');

	            	$('.volumeTraded').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.volumeTraded').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	              
	            
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}
	            
	        	//end //
	        	
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getMarket",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"Market","old");
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".marketprovince").val("");
			        	$(".marketmunicipality").val("");
			        	$(".marketbarangay").val("");
			        	$(".area").val("");
			        	$(".nameOfMarket").val("");
			        	$(".publicprivate").val("");
			        	$(".majorMinorMarket").val("");
			        	$(".volumeTraded").val("");
			        	$(".code").val("");
			        	$(".dataSource").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
		
		
		
		$(".submitSchoolOfFisheries").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".schooloffisheriesprovince").val();
	        	developerData["municipality"] =	$(".schooloffisheriesmunicipality").val();
	        	developerData["barangay"] = $(".schooloffisheriesbarangay").val();
	        	developerData["name"] = $(".name").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	
	        	
	        	//add //
	        	
	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.schooloffisheriesprovince').val().length < 1) {

	            	$('.schooloffisheriesprovince').after('<span class="error">This field is required</span>');

	            	$('.schooloffisheriesprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.schooloffisheriesprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            if ($('.schooloffisheriesmunicipality').val().length < 1) {

	            	$('.schooloffisheriesmunicipality').after('<span class="error">This field is required</span>');

	            	$('.schooloffisheriesmunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.schooloffisheriesmunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.schooloffisheriesbarangay').val().length < 1) {

	            	$('.schooloffisheriesbarangay').after('<span class="error">This field is required</span>');

	            	$('.schooloffisheriesbarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.schooloffisheriesbarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.name').val().length < 1) {

	            	$('.name').after('<span class="error">This field is required</span>');

	            	$('.name').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.name').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	              
	            
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}
	            
	        	//end //
	        	
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getSchoolOfFisheries",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"SchoolOfFisheries","old");
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".schooloffisheriesprovince").val("");
			        	$(".schooloffisheriesmunicipality").val("");
			        	$(".schooloffisheriesbarangay").val("");
			        	$(".name").val("");
			        	$(".area").val("");
			        	$(".code").val("");
			        	$(".dataSource").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
		
		
		
		$(".submitSeaGrass").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".seagrassprovince").val();
	        	developerData["municipality"] =	$(".seagrassmunicipality").val();
	        	developerData["barangay"] = $(".seagrassbarangay").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	
	        	
	        	//add //
	        	
	            $(".error").remove();

	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.seagrassprovince').val().length < 1) {

	            	$('.seagrassprovince').after('<span class="error">This field is required</span>');

	            	$('.seagrassprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.seagrassprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            if ($('.seagrassmunicipality').val().length < 1) {

	            	$('.seagrassmunicipality').after('<span class="error">This field is required</span>');

	            	$('.seagrassmunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.seagrassmunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.seagrassbarangay').val().length < 1) {

	            	$('.seagrassbarangay').after('<span class="error">This field is required</span>');

	            	$('.seagrassbarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.seagrassbarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }	            
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	              
	            
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}
	            
	        	//end //
	        	
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getSeaGrass",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"SeaGrass","old");
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".seagrassprovince").val("");
			        	$(".seagrassmunicipality").val("");
			        	$(".seagrassbarangay").val("");
			        	$(".area").val("");
			        	$(".code").val("");
			        	$(".dataSource").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
		
		
		
		$(".submitSeaWeeds").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".seaweedsprovince").val();
	        	developerData["municipality"] =	$(".seaweedsmunicipality").val();
	        	developerData["barangay"] = $(".seaweedsbarangay").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["indicateGenus"] = $(".indicateGenus").val();
	        	developerData["culturedMethodUsed"] = $(".culturedMethodUsed").val();
	        	developerData["culturedPeriod"] = $(".culturedPeriod").val();
	        	developerData["productionKgPerCropping"] = $(".productionKgPerCropping").val();
	        	developerData["productionNoOfCroppingPerYear"] = $(".productionNoOfCroppingPerYear").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	
	        	
	        	//add //
	        	
	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.seaweedsprovince').val().length < 1) {

	            	$('.seaweedsprovince').after('<span class="error">This field is required</span>');

	            	$('.seaweedsprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.seaweedsprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  
	            
	            if ($('.seaweedsmunicipality').val().length < 1) {

	            	$('.seaweedsmunicipality').after('<span class="error">This field is required</span>');

	            	$('.seaweedsmunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.seaweedsmunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.seaweedsbarangay').val().length < 1) {

	            	$('.seaweedsbarangay').after('<span class="error">This field is required</span>');

	            	$('.seaweedsbarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.seaweedsbarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.indicateGenus').val().length < 1) {

	            	$('.indicateGenus').after('<span class="error">This field is required</span>');

	            	$('.indicateGenus').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.indicateGenus').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.culturedMethodUsed').val().length < 1) {

	            	$('.culturedMethodUsed').after('<span class="error">This field is required</span>');

	            	$('.culturedMethodUsed').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.culturedMethodUsed').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.culturedPeriod').val().length < 1) {

	            	$('.culturedPeriod').after('<span class="error">This field is required</span>');

	            	$('.culturedPeriod').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.culturedPeriod').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.productionKgPerCropping').val().length < 1) {

	            	$('.productionKgPerCropping').after('<span class="error">This field is required</span>');

	            	$('.productionKgPerCropping').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.productionKgPerCropping').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.productionNoOfCroppingPerYear').val().length < 1) {

	            	$('.productionNoOfCroppingPerYear').after('<span class="error">This field is required</span>');

	            	$('.productionNoOfCroppingPerYear').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.productionNoOfCroppingPerYear').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	              
	            
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}
	            
	        	//end //
	        	
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getSeaWeeds",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"SeaWeeds","old");
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".seaweedsprovince").val("");
			        	$(".seaweedsmunicipality").val("");
			        	$(".seaweedsbarangay").val("");
			        	$(".area").val("");
			        	$(".indicateGenus").val("");
			        	$(".culturedMethodUsed").val("");
			        	$(".culturedPeriod").val("");
			        	$(".productionKgPerCropping").val("");
			        	$(".productionNoOfCroppingPerYear").val("");
			        	$(".code").val("");
			        	$(".dataSource").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
		
		
		
		$(".submitTrainingCenter").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fisheriestrainingcenterprovince").val();
	        	developerData["municipality"] =	$(".fisheriestrainingcentermunicipality").val();
	        	developerData["barangay"] = $(".fisheriestrainingcenterbarangay").val();
	        	developerData["name"] = $(".name").val();
	        	developerData["specialization"] = $(".specialization").val();
	        	developerData["facilityWithinTheTrainingCenter"] = $(".facilityWithinTheTrainingCenter").val();
	        	developerData["area"] = $(".area").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["type"] = $(".type").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	
	        	
	        	//add //
	        	
	            $(".error").remove();
	            
	            if ($('.dateAsOf').val().length < 1) {

	            	$('.dateAsOf').after('<span class="error">This field is required</span>');

	            	$('.dateAsOf').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dateAsOf').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }    
	            
	            
	            if ($('.region').val().length < 1) {

	            	$('.region').after('<span class="error">This field is required</span>');

	            	$('.region').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.region').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }   
	            
	            
	            if ($('.fisheriestrainingcenterprovince').val().length < 1) {

	            	$('.fisheriestrainingcenterprovince').after('<span class="error">This field is required</span>');

	            	$('.fisheriestrainingcenterprovince').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fisheriestrainingcenterprovince').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }  	            
	            
	            if ($('.fisheriestrainingcentermunicipality').val().length < 1) {

	            	$('.fisheriestrainingcentermunicipality').after('<span class="error">This field is required</span>');

	            	$('.fisheriestrainingcentermunicipality').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fisheriestrainingcentermunicipality').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.fisheriestrainingcenterbarangay').val().length < 1) {

	            	$('.fisheriestrainingcenterbarangay').after('<span class="error">This field is required</span>');

	            	$('.fisheriestrainingcenterbarangay').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.fisheriestrainingcenterbarangay').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.name').val().length < 1) {

	            	$('.name').after('<span class="error">This field is required</span>');

	            	$('.name').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.name').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.specialization').val().length < 1) {

	            	$('.specialization').after('<span class="error">This field is required</span>');

	            	$('.specialization').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.specialization').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.facilityWithinTheTrainingCenter').val().length < 1) {

	            	$('.facilityWithinTheTrainingCenter').after('<span class="error">This field is required</span>');

	            	$('.facilityWithinTheTrainingCenter').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.facilityWithinTheTrainingCenter').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.area').val().length < 1) {

	            	$('.area').after('<span class="error">This field is required</span>');

	            	$('.area').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.area').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.code').val().length < 1) {

	            	$('.code').after('<span class="error">This field is required</span>');

	            	$('.code').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.code').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.type').val().length < 1) {

	            	$('.type').after('<span class="error">This field is required</span>');

	            	$('.type').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.type').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.dataSource').val().length < 1) {

	            	$('.dataSource').after('<span class="error">This field is required</span>');

	            	$('.dataSource').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.dataSource').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.remarks').val().length < 1) {

	            	$('.remarks').after('<span class="error">This field is required</span>');

	            	$('.remarks').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.remarks').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            if ($('.lat').val().length < 1) {

	            	$('.lat').after('<span class="error">This field is required</span>');

	            	$('.lat').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lat').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.lon').val().length < 1) {

	            	$('.lon').after('<span class="error">This field is required</span>');

	            	$('.lon').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.lon').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	            
	            
	            if ($('.image').val().length < 1) {

	            	$('.image').after('<span class="error">This field is required</span>');

	            	$('.image').removeClass("valid").addClass("invalid");

	            	error_free = false;

	            }else{

	            	$('.image').removeClass("invalid").addClass("valid");

	            	error_free = true;

	            }
	              
	            
	            if (!error_free){

	        		e.preventDefault(); 

	        		return false;

	        	}
	            
	        	//end //
	        	
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getTrainingCenter",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS", data);
						
				refreshRecord(data,"TrainingCenter","old");
						
			        	$(".dateAsOf").val("");
			        	$(".region").val("");
						$(".fisheriestrainingcenterprovince").val("");
			        	$(".fisheriestrainingcentermunicipality").val("");
			        	$(".fisheriestrainingcenterbarangay").val("");
			        	$(".name").val("");
			        	$(".specialization").val("");
			        	$(".facilityWithinTheTrainingCenter").val("");
			        	$(".area").val("");
			        	$(".code").val("");
			        	$(".type").val("");
			        	$(".dataSource").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
		
	    $(".submitForm").click(function(e){
    	    //var formData= $('form').serialize();
    	    var developerData = {};
    	    e.preventDefault(); 
    	    
    	    developerData["firstName"] = $(".firstName").val();
    	    developerData["lastName"] = $(".lastName").val();
    		developerData["ssoId"] = $(".ssoId").val();
    		developerData["pass"] = $(".pass").val();
    		developerData["password"] = $(".pass").val();
    		developerData["email"] = $(".email").val();
    		developerData["state"] = $(".state").val();
    		developerData["region"] = $(".region").val();
    		developerData["userProfiles"] = $(".userProfiles").val();
    		
    		console.log("firstName: " + $(".firstName").val());
    		console.log("lastName: " + $(".lastName").val());
    		console.log("ssoId: " + $(".ssoId").val());
    		console.log("pass: " + $(".pass").val());
    		console.log("email: " + $(".email").val());
    		console.log("state: " + $(".state").val());
    		console.log("region: " + $(".region").val());
    		console.log("userProfiles: " + $(".userProfiles").val());
  		$.ajax({  		
				type : "POST",
				contentType : "application/json",
				url : "userRegistration",
				data : JSON.stringify(developerData),
				dataType : 'json',				
				success : function(data) {
					console.log("SUCCESS: ok", data);
					
	            	
				},
				error: function(data){
				console.log("error", data);
				}
			});
       
    });

		
	});