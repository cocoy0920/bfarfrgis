
function getFishSanctuaryInfoById(data, status,resProvince ){
	
	
         for ( var x = 0; x < data.length; x++) {
				var obj = data[x];

		    $(".id").val(obj.id);
		    $(".user").val(obj.user);
		    $(".uniqueKey").val(obj.uniqueKey);
		    $(".dateEncoded").val(obj.dateEncoded);
		    $(".encodedBy").val(obj.encodedBy);
    	 	$(".region").val(obj.region);
       
         var slctProvice=$('.province'), option="";
         slctProvice.empty();
	            option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
	           
	            JSON.parse(resProvince).forEach(
					function(element) {
	 					console.log(element.province);
	 					
	 					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
	           
	  			})  
	  			
	  			
	  			
	  			 slctProvice.append(option);
	     
	     var slctMunicipal=$('.municipality'), option="";
         slctMunicipal.empty();
	            option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            slctMunicipal.append(option);
	            
		var slctBarangay=$('.barangay'), option="";
         slctBarangay.empty();
	            option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	            slctBarangay.append(option);

     
        	$(".nameOfFishSanctuary").val(obj.nameOfFishSanctuary);
        	$(".area").val(obj.area);
        	$(".bfardenr").val(obj.bfardenr);
        	$(".sheltered").val(obj.sheltered);
        	$(".type").val(obj.type);
        	$(".code").val(obj.code);
        	$(".dateAsOf").val(obj.dateAsOf);
        	$(".fishSanctuarySource").val(obj.fishSanctuarySource);
        	$(".remarks").val(obj.remarks);
        	$(".lat").val(obj.lat);
        	$(".lon").val(obj.lon);
        	$(".image_src").val(obj.image);
        	$(".image").val(obj.image);
        	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);
        
					
		}
}

function getFishProcessingInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);


		$(".nameOfProcessingPlants").val(obj.nameOfProcessingPlants);
    $(".nameOfOperator").val(obj.nameOfOperator);
    $(".operatorClassification").val(obj.operatorClassification);
    $(".processingTechnique").val(obj.processingTechnique);
    $(".processingEnvironmentClassification").val(obj.processingEnvironmentClassification);
    $(".bfarRegistered").val(obj.bfarRegistered);          	
    $(".packagingType").val(obj.packagingType);
    $(".marketReach").val(obj.marketReach);
    $(".indicateSpecies").val(obj.indicateSpecies);
    $(".businessPermitsAndCertificateObtained").val(obj.businessPermitsAndCertificateObtained);
		$(".sourceOfData").val(obj.sourceOfData);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}

function getFishLandingInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);


        $(".nameOfLanding").val(obj.nameOfLanding);             
     	$(".classification").val(obj.classification);
     	$(".volumeOfUnloadingMT").val(obj.volumeOfUnloadingMT);
     	$(".type").val(obj.type);
     	$(".croppingEnd").val(obj.croppingEnd);
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}
function getFishCageInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);


        $(".nameOfOperator").val(obj.nameOfOperator);           
     	$(".classificationofOperator").val(obj.classificationofOperator);
     	$(".cageDimension").val(obj.cageDimension);
     	$(".cageTotalArea").val(obj.cageTotalArea);
     	$(".cageType").val(obj.cageType);
     	$(".cageNoOfCompartments").val(obj.cageNoOfCompartments);
     	$(".indicateSpecies").val(obj.indicateSpecies);
     	$(".sourceOfData").val(obj.sourceOfData);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}

function getFishPondInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);


        $(".nameOfFishPondOperator").val(obj.nameOfFishPondOperator);            
     	$(".nameOfCaretaker").val(obj.nameOfCaretaker);
     	$(".typeOfWater").val(obj.typeOfWater);
     	$(".fishPond").val(obj.fishPond);
     	$(".fishPondType").val(obj.fishPondType);
     	$(".speciesCultured").val(obj.speciesCultured);
     	$(".status").val(obj.status);
     	$(".kind").val(obj.kind);
     	$(".hatchery").val(obj.hatchery);
     	$(".productPer").val(obj.productPer);
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}

function getFishPenInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);


        $(".nameOfOperator").val(obj.nameOfOperator);      
     	$(".noOfFishPen").val(obj.noOfFishPen);
     	$(".speciesCultured").val(obj.speciesCultured);
     	$(".croppingStart").val(obj.croppingStart);
     	$(".croppingEnd").val(obj.croppingEnd);
     	$(".sourceOfData").val(obj.sourceOfData);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}
function getHatcheryInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);


        $(".nameOfHatchery").val(obj.nameOfHatchery);           
     	$(".nameOfOperator").val(obj.nameOfOperator);
     	$(".addressOfOperator").val(obj.addressOfOperator);
     	$(".operatorClassification").val(obj.operatorClassification);
     	$(".publicprivate").val(obj.publicprivate);
     	$(".indicateSpecies").val(obj.indicateSpecies);
     	$(".prodStocking").val(obj.prodStocking);
     	$(".prodCycle").val(obj.prodCycle);
     	$(".actualProdForTheYear").val(obj.actualProdForTheYear);
     	$(".monthStart").val(obj.monthStart);
     	$(".monthEnd").val(obj.monthEnd);
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}

function getIceInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);


        $(".nameOfIcePlant").val(obj.nameOfIcePlant);            
     	$(".sanitaryPermit").val(obj.sanitaryPermit);
     	$(".operator").val(obj.operator);
     	$(".typeOfFacility").val(obj.typeOfFacility);
     	$(".structureType").val(obj.structureType);
     	$(".capacity").val(obj.capacity);
     	$(".withValidLicenseToOperate").val(obj.withValidLicenseToOperate);
     	$(".withValidSanitaryPermit").val(obj.withValidSanitaryPermit);
     	$(".businessPermitsAndCertificateObtained").val(obj.businessPermitsAndCertificateObtained);
     	$(".typeOfIceMaker").val(obj.typeOfIceMaker);
     	$(".foodGradule").val(obj.foodGradule);
     	$(".sourceOfEquipment").val(obj.sourceOfEquipment);
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}

function getFishCorralInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);


        $(".nameOfOperator").val(obj.nameOfOperator);         
     	$(".operatorClassification").val(obj.operatorClassification);
     	$(".nameOfStationGearUse").val(obj.nameOfStationGearUse);
     	$(".noOfUnitUse").val(obj.noOfUnitUse);
     	$(".fishesCaught").val(obj.fishesCaught);
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}

function getFishPortInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);


        $(".nameOfFishport").val(obj.nameOfFishport);          
     	$(".nameOfCaretaker").val(obj.nameOfCaretaker);
     	$(".operatorClassification").val(obj.operatorClassification);
     	$(".pfdaPrivateMunicipal").val(obj.pfdaPrivateMunicipal);
     	$(".volumeOfUnloading").val(obj.volumeOfUnloading);
     	$(".classification").val(obj.classification);
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}

function getMarineInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);


        $(".nameOfMarineProtectedArea").val(obj.nameOfMarineProtectedArea);         
     	$(".type").val(obj.type);
     	$(".sensitiveHabitatWithMPA").val(obj.sensitiveHabitatWithMPA);
     	$(".dateEstablishment").val(obj.dateEstablishment);
     	$(".volumeOfUnloading").val(obj.volumeOfUnloading);
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}

function getMarketInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);


        $(".nameOfMarket").val(obj.nameOfMarket);          
     	$(".publicprivate").val(obj.publicprivate);
     	$(".majorMinorMarket").val(obj.majorMinorMarket);
     	$(".volumeTraded").val(obj.volumeTraded);
     	$(".volumeOfUnloading").val(obj.volumeOfUnloading);
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}

function getSchoolInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);


        $(".nameOfMarket").val(obj.nameOfMarket);          
     	$(".publicprivate").val(obj.publicprivate);
     	$(".majorMinorMarket").val(obj.majorMinorMarket);
     	$(".volumeTraded").val(obj.volumeTraded);
     	$(".volumeOfUnloading").val(obj.volumeOfUnloading);
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}

function getSeagrassInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);


        
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}


function getSeaweedsInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);

        $(".indicateGenus").val(obj.indicateGenus);
		$(".culturedMethodUsed").val(obj.culturedMethodUsed);
		$(".culturedPeriod").val(obj.culturedPeriod);
		$(".productionKgPerCropping").val(obj.productionKgPerCropping);
		$(".productionNoOfCroppingPerYear").val(obj.productionNoOfCroppingPerYear);
        
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}


function getMangroveInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);

        $(".indicateSpecies").val(obj.indicateSpecies);
		$(".type").val(obj.type);
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}


function getLGUInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);

        $(".lguName").val(obj.lguName);
		$(".noOfBarangayCoastal").val(obj.noOfBarangayCoastal);
		$(".noOfBarangayInland").val(obj.noOfBarangayInland);
		$(".lguCoastalLength").val(obj.lguCoastalLength);
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}


function getTrainingInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);

        $(".name").val(obj.name);
		$(".specialization").val(obj.specialization);
		$(".facilityWithinTheTrainingCenter").val(obj.facilityWithinTheTrainingCenter);
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}


function getMaricultureZoneInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $("dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	$(".region").val(obj.region);

 var slctProvice=$('.province'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
 var slctMunicipal=$('.municipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.barangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);

        $(".nameOfMariculture").val(obj.nameOfMariculture);
		$(".maricultureType").val(obj.maricultureType);
		$(".eCcNumber").val(obj.eCcNumber);
		$(".individual").val(obj.individual);
		$(".partnership").val(obj.partnership);
		$(".cooperativeAssociation").val(obj.cooperativeAssociation);
		$(".cageType").val(obj.cageType);
		$(".cageSize").val(obj.cageSize);
		$(".cageQuantity").val(obj.cageQuantity);
		$(".speciesCultured").val(obj.speciesCultured);
		$(".quantityMt").val(obj.quantityMt);
		$(".kind").val(obj.kind);
		$(".productionPerCropping").val(obj.productionPerCropping);
		$(".numberofCropping").val(obj.numberofCropping);
     	$(".species").val(obj.species);
     	$(".farmProduce").val(obj.farmProduce);
     	$(".productDestination").val(productDestination);
     	$(".aquacultureProduction").val(obj.aquacultureProduction);
     	$(".dateLaunched").val(obj.dateLaunched);
     	$(".dateInacted").val(obj.dateInacted);
     	$(".dateApproved").val(obj.dateApproved);
     	$(".publicprivate").val(obj.publicprivate);
     	$(".volumeTraded").val(obj.volumeTraded);
     	$(".accreditations").val(obj.accreditations);
     	$(".municipalOrdinanceNo").val(obj.municipalOrdinanceNo);
     	$(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image);
	$(".image").val(obj.image);
	$(".preview").attr("src", "../../../../frgis_captured_images/" + obj.image);

			
}

	
}










