	
		jQuery(document).ready(function($) {
		
		$("#clearFishsanctuaries").click(function(e){
		
					$("#id").val("0");
		        	$("#user").val("0");
		        	$("#uniqueKey").val("0");	
		        	$("#dateEncoded").val("0");
		        	$("#encodedBy").val("0");
		        	$('#municipality').val("");	            
		        	$('#barangay').val("");   
		        	$("#nameOfFishSanctuary").val("");
		        	$("#area").val("");
		        	$("#bfardenr").val("");
		        	$("#sheltered").val("");
		        	$("#type").val("");
		        	$("#code").val("");
		        	$("#dateAsOf").val("");
		        	$("#fishSanctuarySource").val("");
		        	$("#remarks").val("");
		        	$("#lat").val("");
		        	$("#lon").val("");
		        	$("#image").val("");
		        	$("#image_src").val("0");
		        	$("#preview").attr("src", "");
				});
		
		$('#clearFishProcessingPlants').click(function(e){
			$(".id").val("0");
		    $(".user").val("0");
		    $(".uniqueKey").val("0");
		    $(".dateEncoded").val("0");
		    $(".encodedBy").val("0");
		    
		    slctProvinces.empty();
      		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
      		ProvinceOptions = ProvinceOptions + optionValue;         
      		slctProvinces.append(ProvinceOptions); 
	  		
	  		slctMunicipality.empty();
         			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
         			slctMunicipality.append(MunicipalityOption);
	            
			selectBarangay.empty();
				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
				selectBarangay.append(BarangayOption);


     
        	$(".nameOfProcessingPlants").val("");
        	$(".nameOfOperator").val("");
        	$(".operatorClassification").val("");
        	$(".processingTechnique").val("");
        	$(".processingEnvironmentClassification").val("");
        	$(".bfarRegistered").val("");          	
        	$(".packagingType").val("");
        	$(".marketReach").val("");
        	$(".indicateSpecies").val("");
        	$(".businessPermitsAndCertificateObtained").val("");
        	
        	$(".area").val("");
        	$(".code").val("");
        	$(".dateAsOf").val("");
        	$(".sourceOfData").val("");
        	$(".remarks").val("");
        	$(".lat").val("");
        	$(".lon").val("");
        	$(".image_src").val("0");
        	$(".image").val("");
        	$(".preview").attr("src", "");
		});
		});
		/*
		$("#submitFishProcessingPlants").click(function(e){
			
			var error_free=true;
			var developerData = {};
			
        	developerData["id"] = $("#id").val();
        	developerData["user"] = $("#user").val();
        	developerData["uniqueKey"] = $("#uniqueKey").val();	
        	developerData["dateEncoded"] = $("#dateEncoded").val();	
        	developerData["encodedBy"] = $("#encodedBy").val();	
        	developerData["region"] = $('#region').val();
        	developerData["province"] = $('#province').val();
        	developerData["municipality"] = $('#municipality').val();		            
        	developerData["barangay"] = $('#barangay').val();	     
        	
        	developerData["nameOfProcessingPlants"] =$("#nameOfProcessingPlants").val();
        	developerData["nameOfOperator"] =$("#nameOfOperator").val();
        	developerData["operatorClassification"] =$("#operatorClassification").val();
        	developerData["processingTechnique"] =$("#processingTechnique").val();
        	developerData["processingEnvironmentClassification"] =$("#processingEnvironmentClassification").val();
        	developerData["bfarRegistered"] =$("#bfarRegistered").val();          	
        	developerData["packagingType"] =$("#packagingType").val();
        	developerData["marketReach"] =$("#marketReach").val();
        	developerData["indicateSpecies"] =$("#indicateSpecies").val();
        	developerData["businessPermitsAndCertificateObtained"] =$("#businessPermitsAndCertificateObtained").val();
        	
        	developerData["sourceOfData"] =$("#sourceOfData").val();
        	developerData["dateAsOf"] = $("#dateAsOf").val();
        	developerData["area"] = $("#area").val();
        	developerData["code"] = $("#code").val();
        	developerData["remarks"] = $("#remarks").val();
        	developerData["lat"] = $("#lat").val();
        	developerData["lon"] = $("#lon").val();
        	developerData["image"] = $("#image").val();
        	developerData["image_src"] = $("#image_src").val();
	        	
        	
            $(".error").remove();
        
            if ($('#nameOfProcessingPlants').val().length < 1) {
            	$('#nameOfProcessingPlants').after('<span class="error">This field is required</span>');
            	$('#nameOfProcessingPlants').removeClass("valid").addClass("invalid");
            	error_free = false;
            }else{
            	$('#nameOfProcessingPlants').removeClass("invalid").addClass("valid");
            	error_free = true;
            }           
            if ($('#sourceOfData').val().length < 1) {
            	$('#sourceOfData').after('<span class="error">This field is required</span>');
            	$('#sourceOfData').removeClass("valid").addClass("invalid");
            	error_free = false;
            }else{
            	$('#sourceOfData').removeClass("invalid").addClass("valid");
            	error_free = true;
            }
            if (!error_free){
        		e.preventDefault(); 
        		return false;
        	}
            
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "getFishProcessingPlant",
				data : JSON.stringify(developerData),
				dataType : 'json',				
				success : function(data) {
					console.log("SUCCESS: ok", data);
					$("#id").val("0");
		        	$("#user").val("0");
		        	$("#uniqueKey").val("0");	
		        	$("#dateEncoded").val("0");
		        	$("#encodedBy").val("0");
		        	$('#region').val("");
		        	$('#province').val("");
		        	$('#municipality').val("");	            
		        	$('#barangay').val("");   
		        	
		        	$("#nameOfProcessingPlants").val("");
	            	$("#nameOfOperator").val("");
	            	$("#operatorClassification").val("");
	            	$("#processingTechnique").val("");
	            	$("#processingEnvironmentClassification").val("");
	            	$("#bfarRegistered").val("");          	
	            	$("#packagingType").val("");
	            	$("#marketReach").val("");
	            	$("#indicateSpecies").val("");
	            	$("#businessPermitsAndCertificateObtained").val("");
	            	
	            	$("#area").val("");
	            	$("#code").val("");
	            	$("#dateAsOf").val("");
	            	$("#sourceOfData").val("");
	            	$("#remarks").val("");
	            	$("#lat").val("");
	            	$("#lon").val("");
	            	$("#image_src").val("0");
	            	$("#image").val("");
	            	$("#preview").attr("src", "");
				},
				error: function(data){
				console.log("error", data);
				}
			});
		});		
		$("#submitFishPen").click(function(e){
			e.preventDefault();
			var error_free=true;
				var developerData = {};
	        	developerData["id"] = $("#id").val();
	        	developerData["user"] = $("#user").val();
	        	developerData["uniqueKey"] = $("#uniqueKey").val();	
	        	developerData["dateEncoded"] = $("#dateEncoded").val();	
	        	developerData["encodedBy"] = $("#encodedBy").val();	
	        	developerData["region"] = $('#region').val();
	        	developerData["province"] = $('#province').val();
	        	developerData["municipality"] = $('#municipality').val();		            
	        	developerData["barangay"] = $('#barangay').val();	     
	        	developerData["nameOfOperator"] = $("#nameOfOperator").val();
	        	developerData["area"] = $("#area").val();
	        	developerData["noOfFishPen"] = $("#noOfFishPen").val();
	        	developerData["speciesCultured"] = $("#speciesCultured").val();
	        	developerData["croppingStart"] = $("#croppingStart").val();
	        	developerData["croppingEnd"] = $("#croppingEnd").val();
	        	developerData["code"] = $("#code").val();
	        	developerData["dateAsOf"] = $("#dateAsOf").val();
	        	developerData["lat"] = $("#lat").val();
	        	developerData["lon"] = $("#lon").val();
	        	developerData["sourceOfData"] = $("#sourceOfData").val();
	        	developerData["remarks"] = $("#remarks").val();
	        	developerData["fishPenImageSrc"] = $("#fishPenImageSrc").val();
	        	developerData["image"] = $("#image").val();
	        	developerData["preview"] = $("#preview").val();
		       
	            $(".error").remove();
	        
	            if ($('#nameOfOperator').val().length < 1) {
	            	$('#nameOfOperator').after('<span class="error">This field is required</span>');
	            	$('#nameOfOperator').removeClass("valid").addClass("invalid");
	            	error_free = false;
	            }else{
	            	$('#nameOfOperator').removeClass("invalid").addClass("valid");
	            	error_free = true;
	            }           
	            if ($('#noOfFishPen').val().length < 1) {
	            	$('#noOfFishPen').after('<span class="error">This field is required</span>');
	            	$('#noOfFishPen').removeClass("valid").addClass("invalid");
	            	error_free = false;
	            }else{
	            	$('#noOfFishPen').removeClass("invalid").addClass("valid");
	            	error_free = true;
	            }
	            if (!error_free){
	        		e.preventDefault(); 
	        		return false;
	        	}
	          
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "getFishProcessingPlant",
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						console.log("SUCCESS FISHPEN", data);
						$("#id").val("0");
			        	$("#user").val("0");
			        	$("#uniqueKey").val("0");	
			        	$("#dateEncoded").val("0");
			        	$("#encodedBy").val("0");
			        	$('#region').val("");
			        	$('#province').val("");
			        	$('#municipality').val("");	            
			        	$('#barangay').val("");   
			        	$("#nameOfOperator").val("");
		             	$("#area").val("");
		             	$("#noOfFishPen").val("");
		             	$("#speciesCultured").val("");
		             	$("#croppingStart").val("");
		             	$("#croppingEnd").val("");
		             	$("#code").val("");
		             	$("#lat").val("");
		             	$("#lon").val("");
		             	$("#dateAsOf").val("");
		             	$("#sourceOfData").val("");
		             	$("#remarks").val("");
		             	$("#fishPenImageSrc").val("");
		             	$("#image").val("");
		             	$("#preview").attr("src", "");
		             	alert("Records Already save");
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});*/
		
