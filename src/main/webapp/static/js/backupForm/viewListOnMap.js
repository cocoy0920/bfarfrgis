	

          var fishSancturiesGroup = new Array();
          var fishprocessingplantsLayerGroup  = new Array();
          var fishlandingLayerGroup  = new Array();
          var fishpenLayerGroup  = new Array();
          var fishcageLayerGroup  = new Array();
          var fishpondLayerGroup  = new Array();
          var hatcheryLayerGroup  = new Array();
          var fishcageLayerGroup  = new Array();
          var iceplantLayerGroup  = new Array();
          var fishcorralLayerGroup  = new Array();
          var fishportLayerGroup  = new Array();
          var marineProtectedLayerGroup  = new Array();
          var marketLayerGroup  = new Array();
          var schoolOfFisheriesLayerGroup  = new Array();
          var seagrassLayerGroup  = new Array();
          var seaweedsLayerGroup  = new Array();
          var mangroveLayerGroup  = new Array();
          var lguLayerGroup  = new Array();
          var fisheriesTrainingLayerGroup  = new Array();
          var maricultureParkLayerGroup  = new Array();


function fishsanctuariesGetListMap(data,role){	
	
	
	markerFishProcessingDel();
	markerMaricultureParkDel();
	markerFisheriesTrainingDel();
	markerLGUDel();
	markerMangroveDel();
	markerSeaWeedsDel();
	markerSeaGrassDel();
	markerSchoolOfFisheriesDel();
	markerMarketDel();
	markerMarineProtectedDel();
	markerFishPortDel();
	markerFishcorralDel();
	markerIcePlantDel();
	markerFishPondDel();
	markerHatcheryDel();
	markerFishProcessingDel();
	markerFishlandingDel();
	markerFishPenDel();
	markerFishCageDel();
	

	for ( var i = 0; i < data.length; i++) {
		var obj = data[i];
				
			lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="fishsanctuaries_id"><a href="#" id="'+uniqueKey+'" class="btn btn-primary"">EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    var LamMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
    				fishSancturiesGroup.push(LamMarker);
	}
 	 	}

 function fishprocessingplantsGetListMap(data,role){
	 	
	 	markerFishSancturiesDel()
		markerMaricultureParkDel();
		markerFisheriesTrainingDel();
		markerLGUDel();
		markerMangroveDel();
		markerSeaWeedsDel();
		markerSeaGrassDel();
		markerSchoolOfFisheriesDel();
		markerMarketDel();
		markerMarineProtectedDel();
		markerFishPortDel();
		markerFishcorralDel();
		markerIcePlantDel();
		markerFishPondDel();
		markerHatcheryDel();
		markerFishProcessingDel();
		markerFishlandingDel();
		markerFishPenDel();
		markerFishCageDel();
	 
	 
	 for ( var i = 0; i < data.length; i++) {		 
			var obj = data[i];
			
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="fishprocessingplants_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var fishprocessingplantsMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		fishprocessingplantsLayerGroup.push(fishprocessingplantsMarker);
        		
	 }		
 	 	}
 	 	
 function fishlandingGetListMap(data,role){
	 
			 	markerFishProcessingDel();
			 	markerFishSancturiesDel()
				markerMaricultureParkDel();
				markerFisheriesTrainingDel();
				markerLGUDel();
				markerMangroveDel();
				markerSeaWeedsDel();
				markerSeaGrassDel();
				markerSchoolOfFisheriesDel();
				markerMarketDel();
				markerMarineProtectedDel();
				markerFishPortDel();
				markerFishcorralDel();
				markerIcePlantDel();
				markerFishPondDel();
				markerHatcheryDel();
				markerFishProcessingDel();
				
				markerFishPenDel();
				markerFishCageDel();
				
				for ( var i = 0; i < data.length; i++) {				 
					var obj = data[i];	
					
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="fishlanding_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var fishlandingMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		fishlandingLayerGroup.push(fishlandingMarker);

          
				}	
 	 	} 
 	function fishpenGetListMap(data,role){
 	 	
 		markerFishProcessingDel();
	 	markerFishSancturiesDel()
		markerMaricultureParkDel();
		markerFisheriesTrainingDel();
		markerLGUDel();
		markerMangroveDel();
		markerSeaWeedsDel();
		markerSeaGrassDel();
		markerSchoolOfFisheriesDel();
		markerMarketDel();
		markerMarineProtectedDel();
		markerFishPortDel();
		markerFishcorralDel();
		markerIcePlantDel();
		markerFishPondDel();
		markerHatcheryDel();
		markerFishProcessingDel();
		markerFishlandingDel();
		
		markerFishCageDel();
		
		 for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="fishpen_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var fishpenMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		fishpenLayerGroup.push(fishpenMarker);
		 }	
        		
 	 	} 
 	function fishcageGetListMap(data,role){ 
		
 		markerFishProcessingDel();
	 	markerFishSancturiesDel()
		markerMaricultureParkDel();
		markerFisheriesTrainingDel();
		markerLGUDel();
		markerMangroveDel();
		markerSeaWeedsDel();
		markerSeaGrassDel();
		markerSchoolOfFisheriesDel();
		markerMarketDel();
		markerMarineProtectedDel();
		markerFishPortDel();
		markerFishcorralDel();
		markerIcePlantDel();
		markerFishPondDel();
		markerHatcheryDel();
		markerFishProcessingDel();
		markerFishlandingDel();
		markerFishPenDel();
		
		
		 for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="fishcage_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var fishcageMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		fishcageLayerGroup.push(fishcageMarker);
		 }	
        		
 	 	} 
 	function fishpondGetListMap(data,role){ 
 	 	 
		
 		markerFishProcessingDel();
	 	markerFishSancturiesDel()
		markerMaricultureParkDel();
		markerFisheriesTrainingDel();
		markerLGUDel();
		markerMangroveDel();
		markerSeaWeedsDel();
		markerSeaGrassDel();
		markerSchoolOfFisheriesDel();
		markerMarketDel();
		markerMarineProtectedDel();
		markerFishPortDel();
		markerFishcorralDel();
		markerIcePlantDel();
		
		markerHatcheryDel();
		markerFishProcessingDel();
		markerFishlandingDel();
		markerFishPenDel();
		markerFishCageDel();
		
		for ( var i = 0; i < data.length; i++) {		 
			var obj = data[i];
		
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="fishpond_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var fishpondMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		fishpondLayerGroup.push(fishpondMarker);
        		
			}	
 	 	} 	
 	function hatcheryGetListMap(data,role){ 
        
 		markerFishProcessingDel();
	 	markerFishSancturiesDel()
		markerMaricultureParkDel();
		markerFisheriesTrainingDel();
		markerLGUDel();
		markerMangroveDel();
		markerSeaWeedsDel();
		markerSeaGrassDel();
		markerSchoolOfFisheriesDel();
		markerMarketDel();
		markerMarineProtectedDel();
		markerFishPortDel();
		markerFishcorralDel();
		markerIcePlantDel();
		markerFishPondDel();
	
		markerFishProcessingDel();
		markerFishlandingDel();
		markerFishPenDel();
		markerFishCageDel();
			
		for ( var i = 0; i < data.length; i++) {		 
			var obj = data[i];
		
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="hatchery_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var hatcheryMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		hatcheryLayerGroup.push(hatcheryMarker);
        		
		}	
 	 	}
	function iceplantorcoldstorageGetListMap(data,role){
 	 	
		markerFishProcessingDel();
	 	markerFishSancturiesDel()
		markerMaricultureParkDel();
		markerFisheriesTrainingDel();
		markerLGUDel();
		markerMangroveDel();
		markerSeaWeedsDel();
		markerSeaGrassDel();
		markerSchoolOfFisheriesDel();
		markerMarketDel();
		markerMarineProtectedDel();
		markerFishPortDel();
		markerFishcorralDel();
		
		markerFishPondDel();
		markerHatcheryDel();
		markerFishProcessingDel();
		markerFishlandingDel();
		markerFishPenDel();
		markerFishCageDel();
			
		for ( var i = 0; i < data.length; i++) {		 
			var obj = data[i];
		
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="iceplantorcoldstorage_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var iceplantorcoldstorageMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		iceplantLayerGroup.push(iceplantorcoldstorageMarker);
		}	
        		
 	 	}
	function fishcorralsGetListMap(data,role){
		 
		markerFishProcessingDel();
	 	markerFishSancturiesDel()
		markerMaricultureParkDel();
		markerFisheriesTrainingDel();
		markerLGUDel();
		markerMangroveDel();
		markerSeaWeedsDel();
		markerSeaGrassDel();
		markerSchoolOfFisheriesDel();
		markerMarketDel();
		markerMarineProtectedDel();
		markerFishPortDel();
		
		markerIcePlantDel();
		markerFishPondDel();
		markerHatcheryDel();
		markerFishProcessingDel();
		markerFishlandingDel();
		markerFishPenDel();
		markerFishCageDel();
			
		for ( var i = 0; i < data.length; i++) {		 
			var obj = data[i];
		

				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="fishcorrals_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var fishcorralsMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		fishcoralLayerGroup.push(fishcorralsMarker);
		}	
        		
 	 	} 
		function FISHPORTGetListMap(data,role){
			 
			markerFishProcessingDel();
		 	markerFishSancturiesDel()
			markerMaricultureParkDel();
			markerFisheriesTrainingDel();
			markerLGUDel();
			markerMangroveDel();
			markerSeaWeedsDel();
			markerSeaGrassDel();
			markerSchoolOfFisheriesDel();
			markerMarketDel();
			markerMarineProtectedDel();
			
			markerFishcorralDel();
			markerIcePlantDel();
			markerFishPondDel();
			markerHatcheryDel();
			markerFishProcessingDel();
			markerFishlandingDel();
			markerFishPenDel();
			markerFishCageDel();
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
			

 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="FISHPORT_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var FISHPORTMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		fishportLayerGroup.push(FISHPORTMarker);
			}	
        		
 	 	}
		function marineprotectedGetListMap(data,role){
 	 	
 	 		 
			markerFishProcessingDel();
		 	markerFishSancturiesDel()
			markerMaricultureParkDel();
			markerFisheriesTrainingDel();
			markerLGUDel();
			markerMangroveDel();
			markerSeaWeedsDel();
			markerSeaGrassDel();
			markerSchoolOfFisheriesDel();
			markerMarketDel();
			
			markerFishPortDel();
			markerFishcorralDel();
			markerIcePlantDel();
			markerFishPondDel();
			markerHatcheryDel();
			markerFishProcessingDel();
			markerFishlandingDel();
			markerFishPenDel();
			markerFishCageDel();
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
			
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="marineprotected_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var marineprotectedMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		marineProtectedLayerGroup.push(marineprotectedMarker);
			}	
        		
 	 	}
 	 	function marketGetListMap(data,role){
 	 		 
			markerFishProcessingDel();
		 	markerFishSancturiesDel()
			markerMaricultureParkDel();
			markerFisheriesTrainingDel();
			markerLGUDel();
			markerMangroveDel();
			markerSeaWeedsDel();
			markerSeaGrassDel();
			markerSchoolOfFisheriesDel();
			
			markerMarineProtectedDel();
			markerFishPortDel();
			markerFishcorralDel();
			markerIcePlantDel();
			markerFishPondDel();
			markerHatcheryDel();
			markerFishProcessingDel();
			markerFishlandingDel();
			markerFishPenDel();
			markerFishCageDel();
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
			
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="market_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var marketMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		marketLayerGroup.push(marketMarker);
        		
			}	
 	 	} 
 	 	function schooloffisheriesGetListMap(data,role){
 	 		 
			markerFishProcessingDel();
		 	markerFishSancturiesDel()
			markerMaricultureParkDel();
			markerFisheriesTrainingDel();
			markerLGUDel();
			markerMangroveDel();
			markerSeaWeedsDel();
			markerSeaGrassDel();
		
			markerMarketDel();
			markerMarineProtectedDel();
			markerFishPortDel();
			markerFishcorralDel();
			markerIcePlantDel();
			markerFishPondDel();
			markerHatcheryDel();
			markerFishProcessingDel();
			markerFishlandingDel();
			markerFishPenDel();
			markerFishCageDel();
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
			
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="schooloffisheries_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var schooloffisheriesMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		schoolOfFisheriesLayerGroup.push(schooloffisheriesMarker);
			}	
        		
 	 	} 
 	 	function seagrassGetListMap(data,role){
 	 	 	
 	 		markerFishProcessingDel();
		 	markerFishSancturiesDel()
			markerMaricultureParkDel();
			markerFisheriesTrainingDel();
			markerLGUDel();
			markerMangroveDel();
			markerSeaWeedsDel();
			
			markerSchoolOfFisheriesDel();
			markerMarketDel();
			markerMarineProtectedDel();
			markerFishPortDel();
			markerFishcorralDel();
			markerIcePlantDel();
			markerFishPondDel();
			markerHatcheryDel();
			markerFishProcessingDel();
			markerFishlandingDel();
			markerFishPenDel();
			markerFishCageDel();
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="seagrass_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var seagrassMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		seagrassLayerGroup.push(seagrassMarker);
        		
			}	
 	 	} 
 	 	function seaweedsGetListMap(data,role){
 	 		
 	 	
 	 		markerFishProcessingDel();
		 	markerFishSancturiesDel()
			markerMaricultureParkDel();
			markerFisheriesTrainingDel();
			markerLGUDel();
			markerMangroveDel();
			
			markerSeaGrassDel();
			markerSchoolOfFisheriesDel();
			markerMarketDel();
			markerMarineProtectedDel();
			markerFishPortDel();
			markerFishcorralDel();
			markerIcePlantDel();
			markerFishPondDel();
			markerHatcheryDel();
			markerFishProcessingDel();
			markerFishlandingDel();
			markerFishPenDel();
			markerFishCageDel();
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="seaweeds_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var seaweedsMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		seaweedsLayerGroup.push(seaweedsMarker);
			}	
        		
 	 	}
		function mangroveGetListMap(data,role){

 	 		markerFishProcessingDel();
		 	markerFishSancturiesDel()
			markerMaricultureParkDel();
			markerFisheriesTrainingDel();
			markerLGUDel();
			
			markerSeaWeedsDel();
			markerSeaGrassDel();
			markerSchoolOfFisheriesDel();
			markerMarketDel();
			markerMarineProtectedDel();
			markerFishPortDel();
			markerFishcorralDel();
			markerIcePlantDel();
			markerFishPondDel();
			markerHatcheryDel();
			markerFishProcessingDel();
			markerFishlandingDel();
			markerFishPenDel();
			markerFishCageDel();
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="mangrove_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var mangroveMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		mangroveLayerGroup.push(mangroveMarker);
			}	
        		
 	 	}
		function lguGetListMap(data,role){

 	 		markerFishProcessingDel();
		 	markerFishSancturiesDel()
			markerMaricultureParkDel();
			markerFisheriesTrainingDel();
			
			markerMangroveDel();
			markerSeaWeedsDel();
			markerSeaGrassDel();
			markerSchoolOfFisheriesDel();
			markerMarketDel();
			markerMarineProtectedDel();
			markerFishPortDel();
			markerFishcorralDel();
			markerIcePlantDel();
			markerFishPondDel();
			markerHatcheryDel();
			markerFishProcessingDel();
			markerFishlandingDel();
			markerFishPenDel();
			markerFishCageDel();
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="lgu_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var lguMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		lguLayerGroup.push(lguMarker);
			}	
        		
 	 	}
		function trainingcenterGetListMap(data,role){
 	 	
 	 		markerFishProcessingDel();
		 	markerFishSancturiesDel()
			markerMaricultureParkDel();
			
			markerLGUDel();
			markerMangroveDel();
			markerSeaWeedsDel();
			markerSeaGrassDel();
			markerSchoolOfFisheriesDel();
			markerMarketDel();
			markerMarineProtectedDel();
			markerFishPortDel();
			markerFishcorralDel();
			markerIcePlantDel();
			markerFishPondDel();
			markerHatcheryDel();
			markerFishProcessingDel();
			markerFishlandingDel();
			markerFishPenDel();
			markerFishCageDel();
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
			
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="trainingcenter_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var trainingcenterMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		fisheriesTrainingLayerGroup.push(trainingcenterMarker);
        		
			}	
 	 	}
		function mariculturezoneGetListMap(data,role){

 	 		markerFishProcessingDel();
		 	markerFishSancturiesDel()
			
			markerFisheriesTrainingDel();
			markerLGUDel();
			markerMangroveDel();
			markerSeaWeedsDel();
			markerSeaGrassDel();
			markerSchoolOfFisheriesDel();
			markerMarketDel();
			markerMarineProtectedDel();
			markerFishPortDel();
			markerFishcorralDel();
			markerIcePlantDel();
			markerFishPondDel();
			markerHatcheryDel();
			markerFishProcessingDel();
			markerFishlandingDel();
			markerFishPenDel();
			markerFishCageDel();
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
			

 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="mariculturezone_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var mariculturezoneMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		maricultureParkLayerGroup.push(mariculturezoneMarker);
			}	
        		
 	 	}
  