
  $(document).ready(function () {

      
      $('.file').change(function () {
          
         // var fullPath = document.getElementById('file').value;
    	  var fullPath = $('.file').val();
    if (fullPath) {
        var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
        var filename = fullPath.substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
        }
       // document.getElementById("image").value = filename;
        $('.image').val(filename);
        
    }
              if (this.files.length > 0) {

                  $.each(this.files, function (i, v) {
                      var reader = new FileReader();

                      reader.onload = function (e) {
                          var img = new Image();
                          img.src = e.target.result;
    						
    						img.onload = function () {

                              // CREATE A CANVAS ELEMENT AND ASSIGN THE IMAGES TO IT.
                              var canvas = document.createElement("canvas");

                              var value = $('#size').val();

                              // RESIZE THE IMAGES ONE BY ONE.
                              //img.width = (img.width * value) / 100
                              //img.height = (img.height * value) / 100
                              
                              ///updated
                              var MAX_WIDTH = 400;
                  			var MAX_HEIGHT = 400;
                  			var width = img.width;
                  			var height = img.height;

                  			if (width > height) {
                      			if (width > MAX_WIDTH) {
                          		height *= MAX_WIDTH / width;
                          		width = MAX_WIDTH;
                      			}
                  			} else {
                      		if (height > MAX_HEIGHT) {
                          	width *= MAX_HEIGHT / height;
                          	height = MAX_HEIGHT;
                      		}
                  			}
                  			canvas.width = width;
                  			canvas.height = height;
                              ///updated

                              var ctx = canvas.getContext("2d");
                              ctx.clearRect(0, 0, canvas.width, canvas.height);
                              canvas.width = img.width;
                              canvas.height = img.height;
                              ctx.drawImage(img, 0, 0, width, height);
                              
                              dataurl = canvas.toDataURL(file.type);
                             // document.getElementById('image_srcfpp').value = dataurl;
                              $('.image_src').val(dataurl);
    							//preview.setAttribute('src', dataurl);
                                  $('.preview').attr('src', dataurl);
                               
                              $('#img').append(img);      // SHOW THE IMAGES OF THE BROWSER.

                              // AUTO DOWNLOAD THE IMAGES, ONCES RESIZED.
                             // var a = document.createElement('a');
                             // a.href = canvas.toDataURL("image/png");
                             // a.download = 'sample.jpg';
                             // document.body.appendChild(a);
                             // a.click();
                          }
                      };
                      reader.readAsDataURL(this);
                     // console.log(this.files.name);
                       
                  });
              }
          });
  });

  $(document).ready(function(){
  
  $('.fishcorralsprovince').change(function(){
    var province = $(this).val();
    console.log(province);
    $.ajax({
        type: 'GET',
        url: "getMunicipality/" + province,
        success: function(data){
            var slctSubcat=$('.fishcorralsmunicipality'), optionChanges="";
            slctSubcat.empty();
            optionChanges = "<option value=''>-- PLS SELECT --</option>";
            
            for(var i=0; i<data.length; i++){
            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
            }
            slctSubcat.append(optionChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
  $('.fishportprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.fishportmunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	}); 

  
  $('.fishpondprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.fishpondmunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	}); 
  $('.fishpenprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.fishpenmunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.fishlandingprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.fishlandingmunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.hatcheryprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.hatcherymunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.iceplantorcoldstorageprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.iceplantorcoldstoragemunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.localgovernmentunitprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.localgovernmentunitmunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.mangroveprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.mangrovemunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.mariculturezoneprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.mariculturezonemunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.marineprotectedareaprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.marineprotectedareamunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.marketprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.marketmunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.schooloffisheriesprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.schooloffisheriesmunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.seagrassprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.seagrassmunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.seaweedsprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.seaweedsmunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.fisheriestrainingcenterprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.fisheriestrainingcentermunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.fishprocessingplantprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.fishprocessingplantmunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.fishsanctuaryprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.fishsanctuarymunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.fishcageprovince').change(function(){
	    var province = $(this).val();
	    console.log(province);
	    $.ajax({
	        type: 'GET',
	        url: "getMunicipality/" + province,
	        success: function(data){
	            var slctSubcat=$('.fishcagemunicipality'), optionChanges="";
	            slctSubcat.empty();
	            optionChanges = "<option value=''>-- PLS SELECT --</option>";
	            
	            for(var i=0; i<data.length; i++){
	            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
	            }
	            slctSubcat.append(optionChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
$('.fishcorralsmunicipality').change(function(){
	    var municipal = $(this).val();
	    console.log(municipal);
	    $.ajax({
	        type: 'GET',
	        url: "getBarangay/" + municipal,
	        success: function(data){
	            var slctSubcat=$('.fishcorralsbarangay'), optionMunicipalChanges="";
	            slctSubcat.empty();
	            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
	            for(var i=0; i<data.length; i++){
	            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
	            }
	            slctSubcat.append(optionMunicipalChanges);
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
$('.fishportmunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.fishportbarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.fishpondmunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.fishpondbarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.fishpenmunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.fishpenbarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.fishlandingmunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.fishlandingbarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.hatcherymunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.hatcherybarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.iceplantorcoldstoragemunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.iceplantorcoldstoragebarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.localgovernmentunitmunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.localgovernmentunitbarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.mangrovemunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.mangrovebarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.mariculturezonemunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.mariculturezonebarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.marineprotectedareamunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.marineprotectedareabarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.marketmunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.marketbarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.schooloffisheriesmunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.schooloffisheriesbarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.seagrassmunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.seagrassbarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.seaweedsmunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.seaweedsbarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.fisheriestrainingcentermunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.fisheriestrainingcenterbarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.fishprocessingplantmunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.fishprocessingplantbarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.fishsanctuarymunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.fishsanctuarybarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});
$('.fishcagemunicipality').change(function(){
    var municipal = $(this).val();
    console.log(municipal);
    $.ajax({
        type: 'GET',
        url: "getBarangay/" + municipal,
        success: function(data){
            var slctSubcat=$('.fishcagebarangay'), optionMunicipalChanges="";
            slctSubcat.empty();
            optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
            for(var i=0; i<data.length; i++){
            	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
            }
            slctSubcat.append(optionMunicipalChanges);
        },
        error:function(){
            alert("error");
        }

    });
});


  $('.fishcorralsbarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"fishcorrals",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.fishportbarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"FISHPORT",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.fishpondbarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"fishpond",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.fishpenbarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"fishpen",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.fishlandingbarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"fishlanding",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.hatcherybarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"hatchery",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.iceplantorcoldstoragebarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"iceplantorcoldstorage",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.localgovernmentunitbarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"lgu",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.mangrovebarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"mangrove",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.mariculturezonebarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"mariculturezone",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.marineprotectedareabarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"marineprotected",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.marketbarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"market",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.schooloffisheriesbarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"schooloffisheries",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.seagrassbarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"seagrass",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.seaweedsbarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"seaweeds",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.fisheriestrainingcenterbarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"trainingcenter",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.fishprocessingplantbarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"fishprocessingplants",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});
  $('.fishsanctuarybarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"fishsanctuaries",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	}); 
  $('.fishcagebarangay').change(function(){
	    var barangay = $(this).val();
	    console.log(barangay);
	    $.ajax({
	        type: 'GET',
	        url: "code/" + barangay+"/" +"fishcage",
	        success: function(data){
	        console.log(data);
	        $('.code').val(data);
	        //	document.getElementById('code').value = data;
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	}); 
  $('.lat').change(function(){
	    var latValue = $(this).val();
	  
	    if(latValue == ''){
	    	return;
	    }
	    $.ajax({
	        type: 'GET',
	        url: "latValue/" + latValue,
	        success: function(data){
	        console.log(data);
	        	if(data === 'invalid'){
	        		$('.laterror').innerHTML =  "INVALID LATITUDE";
	        		$('.lat').val("");
	        	///document.getElementById('laterror').innerHTML = "INVALID LATITUDE";
	        	//document.getElementById('lat').value = "";
	        	}else{
	        		$('.laterror').innerHTML =  "";
	        		$('.lat').val(data);
	        	//document.getElementById('laterror').innerHTML = "";
	        	//document.getElementById('lat').value = data;
	        	}
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});


	  $('.lon').change(function(){
	    var lonValue = $(this).val();
	 
	    if(lonValue == ''){
	    	return;
	    }
	    $.ajax({
	        type: 'GET',
	        url: "lonValue/" + lonValue,
	        success: function(data){
	        console.log(data);
	        	if(data === 'invalid'){
	        		$('.lonerror').innerHTML =  "INVALID LATITUDE";
	        		$('.lon').val("");
	        	//document.getElementById('lonerror').innerHTML = "INVALID LONGITUDE";
	        	//document.getElementById('lon').value = "";
	        	}else{
	        		$('.lonerror').innerHTML =  "INVALID LATITUDE";
	        		$('.lon').val(data);
	        	//document.getElementById('lonerror').innerHTML = "";
	        	//document.getElementById('lon').value = data;
	        	}
	             
	        },
	        error:function(){
	            alert("error");
	        }

	    });
	});

	  $('.area').change(function(){
		    var areaValue = $(this).val();
		    
		    if(areaValue == ''){
		    	return;
		    }
		    $.ajax({
		        type: 'GET',
		        url: "areaValue/" + areaValue,
		        success: function(data){
		        console.log(data);
		        	if(data == 'invalid'){
		        		$('.areaerror').innerHTML =  "INVALID LATITUDE";
		        		$('.area').val("");
		        	///document.getElementById('areaerror').innerHTML = "INVALID AREA";
		        	//document.getElementById('area').value = "";
		        	}else{
		        		$('.areaerror').innerHTML =  "INVALID LATITUDE";
		        		$('.area').val(data);
		        	//document.getElementById('areaerror').innerHTML = "";
		        	//document.getElementById('area').value = data;
		        	}
		             
		        },
		        error:function(){
		            alert("error");
		        }

		    });
		});
	  
	  
 
		  
	$('.form_date').datetimepicker({
      weekStart: 1,
      todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
  });
	
    $('.form_croppingStart').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
    
     $('.form_croppingEnd').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
     
     $('.form_monthStart').datetimepicker({
         weekStart: 1,
         todayBtn:  1,
 		autoclose: 1,
 		todayHighlight: 1,
 		startView: 2,
 		minView: 2,
 		forceParse: 0
     });
     
     $('.form_monthEnd').datetimepicker({
         weekStart: 1,
         todayBtn:  1,
 		autoclose: 1,
 		todayHighlight: 1,
 		startView: 2,
 		minView: 2,
 		forceParse: 0
     });
     $('.form_dateInacted').datetimepicker({
         weekStart: 1,
         todayBtn:  1,
 		autoclose: 1,
 		todayHighlight: 1,
 		startView: 2,
 		minView: 2,
 		forceParse: 0
     });
     $('.form_dateLaunched').datetimepicker({
         weekStart: 1,
         todayBtn:  1,
 		autoclose: 1,
 		todayHighlight: 1,
 		startView: 2,
 		minView: 2,
 		forceParse: 0
     });
     $('.form_dateApproved').datetimepicker({
         weekStart: 1,
         todayBtn:  1,
 		autoclose: 1,
 		todayHighlight: 1,
 		startView: 2,
 		minView: 2,
 		forceParse: 0
     });

     $('.form_dateEstablishment').datetimepicker({
         weekStart: 1,
         todayBtn:  1,
 		autoclose: 1,
 		todayHighlight: 1,
 		startView: 2,
 		minView: 2,
 		forceParse: 0
     });
});
 
