var region_id = "all";

	getPerPage();
	getAllFishSanctuaryPage();
	getFishSanctuaryList();


function getPerPage() {
	$('#paging').delegate('a','click',function() {
					var $this = $(this), 
					target = $this.data('target');
					
					$.get("/excel/fishsanctuaries_user/"+ target, function(content, status) {

						var markup = "";
						var ShowHide ="";
						for (var x = 0; x < content.fishSanctuaries.length; x++) {
							var active = content.fishSanctuaries[x].enabled;
							if(active){
								ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
							}else{
								ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
							}
							markup += "<tr>" + 
									"<td><span>" + content.fishSanctuaries[x].region + "</span></td>" +
									"<td>"+ content.fishSanctuaries[x].province + "</td>" +
									"<td>"+ content.fishSanctuaries[x].municipality+ "</td>" +
									"<td>" + content.fishSanctuaries[x].barangay + "</td>" +
									"<td>"+ content.fishSanctuaries[x].nameOfFishSanctuary+ "</td>" +
									"<td>" + content.fishSanctuaries[x].lat + "," + content.fishSanctuaries[x].lon+ "</td>" +
									ShowHide + 
									//"<td class='map menu_links btn-primary' data-href="+ content.fishSanctuaries[x].uniqueKey + ">MAP</td>" +
									//"<td class='view menu_links btn-primary' data-href="+ content.fishSanctuaries[x].uniqueKey + ">View</td>" +
									"</tr>";
						}
						$("#export-buttons-table").find('tbody').empty();
						$("#export-buttons-table").find('tbody').append(markup);
						$("#paging").empty();
						$("#paging").append(content.pageNumber);
					});
					//return target;
	});

}
function getAllFishSanctuaryPage(){

	$.get("/excel/fishsanctuaries_user/"+ 0, function(content, status) {

		var markup = "";
		var ShowHide ="";
		for (var x = 0; x < content.fishSanctuaries.length; x++) {
			number_row = x +1;
			var active = content.fishSanctuaries[x].enabled;
			if(active){
				ShowHide = "<td class='del menu_links btn-primary'>ACTIVE</td>";
			}else{
				ShowHide = "<td class='del menu_links btn-primary'>INACTIVE</td>";
			}
			markup += "<tr>" +
		
						"<td><span>" + content.fishSanctuaries[x].region + "</span></td>" +
//						"<span class='spnTooltip'>" +
//						"<strong>CLICK FOR VIEW/EDIT</strong><br/></span>" +"</td>" +
						"<td>" + content.fishSanctuaries[x].province + "</td>" +
						"<td>" + content.fishSanctuaries[x].municipality + "</td>" +
						"<td>" + content.fishSanctuaries[x].barangay + "</td>" +
						"<td>" + content.fishSanctuaries[x].nameOfFishSanctuary + "</td>" +
						"<td>" + content.fishSanctuaries[x].lat + "," + content.fishSanctuaries[x].lon + "</td>" +
						ShowHide + 
						//"<td class='map menu_links btn-primary' data-href="+ content.fishSanctuaries[x].uniqueKey + ">MAP</td>" +
						//"<td class='view menu_links btn-primary' data-href="+ content.fishSanctuaries[x].uniqueKey + ">View</td>" +
						"</tr>";
		}
		$("#export-buttons-table").find('tbody').empty();
		$("#export-buttons-table").find('tbody').append(markup);
		$("#paging").empty();
		$("#paging").append(content.pageNumber);
	});

}

function getFishSanctuaryList(){
	$.get("/excel/getAllfishsanctuariesList",function(content, status) {
	
				var markup = "";
				var tr;
				var region="";
				var dataAsOf="";
				
				var excelHead="";
				$('#emp_body').empty();
				for (var x = 0; x < content.length; x++) {
					
					 if(content.length - 1 === x) {
						 region = content[x].region;
						 dataAsOf = content[x].dateAsOf;
					        $(".exportToExcel").show();
					    }
					
					tr = $('<tr/>');
					tr.append("<td>" + content[x].region+ "</td>");
					tr.append("<td>" + content[x].province+ "</td>");
					tr.append("<td>" + content[x].municipality+ "</td>");
					tr.append("<td>" + content[x].barangay+ "</td>");
					tr.append("<td>"+ content[x].nameOfFishSanctuary+ "</td>");
					tr.append("<td>" + content[x].code + "</td>");
					tr.append("<td>" + content[x].area + "</td>");
					tr.append("<td>" + content[x].type + "</td>");
					tr.append("<td>"+ content[x].fishSanctuarySource+ "</td>");
					tr.append("<td>" + content[x].bfardenr+ "</td>");
					tr.append("<td>" + content[x].sheltered+ "</td>");
					tr.append("<td>"+ content[x].nameOfSensitiveHabitatMPA+ "</td>");
					tr.append("<td>" + content[x].ordinanceNo+ "</td>");
					tr.append("<td>" + content[x].ordinanceTitle+ "</td>");
					tr.append("<td>" + content[x].dateEstablished+ "</td>");
					tr.append("<td>" + content[x].lat + "</td>");
					tr.append("<td>" + content[x].lon + "</td>");
					tr.append("<td>" + content[x].remarks+ "</td>");

					$('#emp_body').append(tr);
					//ExportTable();
				}
				excelHead +=  "<tr><th align='center' colspan='17'>FISH SANCTUARY</th></tr>"			                
		                         
		            + "<tr><th>Region</th>"
		            + "<th>Province</th>"
		            + "<th>Municipality</th>"
		            + "<th>Barangay</th>"
		            + "<th>Name of FishSanctuary</th>"
		            + "<th>Code</th>"
		            + "<th>Area</th>"
		            + "<th>Type</th>"
		            + "<th>Data Source</th>"
		            + "<th>Implementer</th>"
		            + "<th>Sheltered</th>"
		            + "<th>Name Of Sensitive Habitat MPA</th>"
		            + "<th>Ordinance No.</th>"
		            + "<th>Ordinance Title</th>"
		            + "<th>Date Established</th>"
		            + "<th>Latitude</th>"
		            + "<th>Longitude</th>"
		            + "<th>Remarks</th></tr>";	
				
				
				$('#thead_body').append(excelHead);
			});

	}

