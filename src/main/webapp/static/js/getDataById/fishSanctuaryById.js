
function getFishSanctuaryInfoById(data, status){
	
	var content = $("#pdfFISHSANCTUARY").find('tbody').empty();
	var markup = null
	//$('#customers').hide();
	
         for ( var x = 0; x < data.length; x++) {
				var obj = data[x];

		    $(".id").val(obj.id);
		    $(".user").val(obj.user);
		    $(".uniqueKey").val(obj.uniqueKey);
		    $(".dateEncoded").val(obj.dateEncoded);
		    $(".encodedBy").val(obj.encodedBy);
    	 	//$(".region").val(obj.region);
		    
		    var province = obj.province_id;
		    var slctProvice=$('.province'), optionProvince="";
				slctProvice.empty();
				optionProvince = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";

			 $.get("getProvinceList", function(data, status){     	  			
				for( let prop in data ){
				var getprovince	= data[prop].province_id;	
				if(getprovince != province){			
				optionProvince = optionProvince + "<option value='"+data[prop].province_id+ "," + data[prop].province+"'>"+data[prop].province+"</option>";	           
				}
				}
			
			 slctProvice.append(optionProvince);
		 	}); 
		 	 					
			var slctMunicipal=$('.municipality'), option="";
			var municipal =obj.municipality_id;
			slctMunicipal.empty();
			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
			
			$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
					for( let prop in data ){
						var getMunicipal = data[prop].municipal_id;
						if(getMunicipal != municipal){
						option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
						}
						}
			slctMunicipal.append(option);
		}); 
			
		var slctBarangay=$('.barangay'), brgyoption="";
		var barangay = obj.barangay_id;
				slctBarangay.empty();
			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";

		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
		console.log("getBarangayList: " + data);  					
					for( let prop in data ){
						var getbarangay = data[prop].barangay_id;
						if(getbarangay != barangay){					
							brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
						}
						
						}
			slctBarangay.append(brgyoption);
		}); 
		
	            $(".nameOfFishSanctuary").val(obj.nameOfFishSanctuary);
	        	$(".area").val(obj.area);
	        	$(".bfardenr").val(obj.bfardenr);
	        	$(".sheltered").val(obj.sheltered);
	        	$(".nameOfSensitiveHabitatMPA").val(obj.nameOfSensitiveHabitatMPA);
	        	$(".OrdinanceNo").val(obj.OrdinanceNo);
	        	$(".OrdinanceTitle").val(obj.OrdinanceTitle);
	        	$(".DateEstablished").val(obj.DateEstablished);
	        	$(".type").val(obj.type);
	        	$(".code").val(obj.code);        	
	        	$(".dateAsOf").val(obj.dateAsOf);
	        	$(".fishSanctuarySource").val(obj.fishSanctuarySource);
	        	$(".remarksContent").val(obj.remarks);
	        	$(".lat").val(obj.lat);
	        	$(".lon").val(obj.lon);
	        	$(".image_src").val(obj.image_src);
	        	$(".image").val(obj.image);
	        	$(".preview").attr("src", obj.image_src);
	            
	            
	            markup = 
	            	  
		        "<tr><td scope=\"col\">FISH SANCTUARY LOCATION</td><td scope=\"col\">&nbsp;</td></tr>"+
	            "<tr><td scope=\"row\">Region</td><td>" + obj.region + "</td></tr>"+ 
	            "<tr><td scope=\"row\">Province</td><td>" + obj.province + "</td></tr>"+ 
	            "<tr><td scope=\"row\">Municipality</td><td>" + obj.municipality + "</td></tr>"+
	            "<tr><td scope=\"row\">Barangay</td><td>" + obj.barangay + "</td></tr>"+
	            "<tr><td scope=\"col\">FISH SANCTUARY INFORMATION</td><td scope=\"col\">&nbsp;</td></tr>"+
	            "<tr><td scope=\"row\">Name of Sanctuary/Protected Area:</td><td>"+obj.nameOfFishSanctuary+"</td></tr>"+
	            "<tr><td scope=\"row\">Implementer:</td><td scope=\"col\">&nbsp;</tdd></tr>"+
	            "<tr><td scope=\"col\">BFAR</td><td scope=\"col\">&nbsp;</td></tr>"+
	           "<tr><td scope=\"row\">Name of Sheltered Fisheries/Species:</td><td>" + obj.sheltered + "</td></tr>"+
	           "<tr><td scope=\"col\">DENR</td><td scope=\"col\">&nbsp;</td></tr>"+
	           "<tr><td scope=\"row\">Name of Sensitive Habitat with MPA:</td><td>" + obj.nameOfSensitiveHabitatMPA + "</td></tr>"+
	           "<tr><td scope=\"row\">Ordinance No:</td><td>" + obj.OrdinanceNo + "</td></tr>"+
	          "<tr><td scope=\"row\">Ordinance Title:</td><td>" + obj.OrdinanceTitle + "</td></tr>"+
	          "<tr><td scope=\"row\">Data Established:</td><td>" + obj.DateEstablished + "</td></tr>"+
	          "<tr><td scope=\"row\">Area:</td><td>" + obj.area + "</td></tr>"+
	          "<tr><td scope=\"row\">Type:</td><td>" + obj.type + "</td></tr>"+
	           "<tr><td scope=\"col\">ADDITIONAL INFORMATION</td><td scope=\"col\">&nbsp;</td></tr>"+
	          "<tr><td scope=\"row\">Date as of:</td><td>" + obj.dateAsOf + "</td></tr>"+
	          "<tr><td scope=\"row\">Data Sources:</td><td>" + obj.fishSanctuarySource + "</td></tr>"+
	          "<tr><td scope=\"row\">Remarks:</td><td>" + obj.remarks + "</td></tr>"+
	          "<tr><td  scope=\"col\">GEOGRAPHICAL LOCATION</td><td scope=\"col\">&nbsp;</td></tr>"+
	          "<tr><td scope=\"row\">Latitude:</td><td>" + obj.lat + "</td></tr>"+
	          "<tr><td scope=\"row\">Longitude:</td><td>" + obj.lon + "</td></tr>"+
	          "<tr><td  scope=\"col\">IMAGE</td><td scope=\"col\">&nbsp;</td></tr>"+
	          
	          "<tr><td scope=\"row\"><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td></tr>"; 
	        
	            content.append(markup);
	            
	           
	            markup = "";
	            $('#img_preview').show();
	            $('#pdf').show();
	            $('#table').hide();
	         //   $('.provinceForm').text(obj.province);
	          //  $('.regionForm').text(obj.region);
        	
        
					
		}
}
