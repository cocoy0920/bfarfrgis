
function getMarineInfoById(data, status,resProvince ){
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $(".dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	//$(".region").val(obj.region);

 var slctProvice=$('.marineprotectedareaprovince'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
 
/* var slctMunicipal=$('.marineprotectedareamunicipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.marineprotectedareabarangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);

*/
    $(".nameOfMarineProtectedArea").val(obj.nameOfMarineProtectedArea);         
    $(".type").val(obj.type);
    $(".sensitiveHabitatWithMPA").val(obj.sensitiveHabitatWithMPA);
    $(".dateEstablish").val(obj.dateEstablishment);
    $(".volumeOfUnloading").val(obj.volumeOfUnloading);
    $(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image_src);
	$(".image").val(obj.image);
	$(".preview").attr("src",obj.image_src);


    var markup = 
    "<tr><th scope=\"col\">FISH SANCTUARY LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
    "<tr><th scope=\"col\">FISH SANCTUARY INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">Name of Sanctuary/Protected Area:</th><td>"+obj.nameOfFishSanctuary+"</td></tr>"+
    "<tr><th scope=\"row\">Implementer:</th><th scope=\"row\">&nbsp;</th></tr>"+
    "<tr><th scope=\"col\">BFAR</th><th scope=\"col\">&nbsp;</th></tr>"+
   "<tr><th scope=\"row\">Name of Sheltered Fisheries/Species:</th><td>" + obj.sheltered + "</td></tr>"+
   "<tr><th scope=\"col\">DENR</th><th scope=\"col\">&nbsp;</th></tr>"+
   "<tr><th scope=\"row\">Name of Sensitive Habitat with MPA:</th><td>" + obj.nameOfSensitiveHabitatMPA + "</td></tr>"+
   "<tr><th scope=\"row\">Ordinance No:</th><td>" + obj.OrdinanceNo + "</td></tr>"+
  "<tr><th scope=\"row\">Ordinance Title:</th><td>" + obj.OrdinanceTitle + "</td></tr>"+
  "<tr><th scope=\"row\">Data Established:</th><td>" + obj.DateEstablished + "</td></tr>"+
  "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
  "<tr><th scope=\"row\">Type:</th><td>" + obj.type + "</td></tr>"+
   "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.fishSanctuarySource + "</td></tr>"+
  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
 
    $("table tbody").append(markup);		
}

	
}
