function getHatcheryInfoById(data, status){
	
	var content = $("#pdfHATCHERY").find('tbody').empty();
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		$(".id").val(obj.id);
	    $(".user").val(obj.user);
	    $(".uniqueKey").val(obj.uniqueKey);
	    $(".dateEncoded").val(obj.dateEncoded);
	    $(".encodedBy").val(obj.encodedBy);
	 	//$(".region").val(obj.region);
	    
	    var province = obj.province_id;
	    var slctProvice=$('.province'), optionProvince="";
			slctProvice.empty();
			optionProvince = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";

		 $.get("getProvinceList", function(data, status){     	  			
			for( let prop in data ){
			var getprovince	= data[prop].province_id;	
			if(getprovince != province){			
			optionProvince = optionProvince + "<option value='"+data[prop].province_id+ "," + data[prop].province+"'>"+data[prop].province+"</option>";	           
			}
			}
		
		 slctProvice.append(optionProvince);
	 	}); 
	 	 					
		var slctMunicipal=$('.municipality'), option="";
		var municipal =obj.municipality_id;
		slctMunicipal.empty();
		option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
		
		$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
				for( let prop in data ){
					var getMunicipal = data[prop].municipal_id;
					if(getMunicipal != municipal){
					option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
					}
					}
		slctMunicipal.append(option);
	}); 
		
	var slctBarangay=$('.barangay'), brgyoption="";
	var barangay = obj.barangay_id;
			slctBarangay.empty();
		brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";

	$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	console.log("getBarangayList: " + data);  					
				for( let prop in data ){
					var getbarangay = data[prop].barangay_id;
					if(getbarangay != barangay){					
						brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
					}
					
					}
		slctBarangay.append(brgyoption);
	}); 
	
    $(".nameOfHatchery").val(obj.nameOfHatchery);           
    $(".hatcherynameOfOperator").val(obj.nameOfOperator);
    $(".hatcheryaddressOfOperator").val(obj.addressOfOperator);
    $(".hatcheryoperatorClassification").val(obj.operatorClassification);
    $(".hatcherypublicprivate").val(obj.publicprivate);
    $(".hatcheryindicateSpecies").val(obj.indicateSpecies);
    $(".prodStocking").val(obj.prodStocking);
    $(".prodCycle").val(obj.prodCycle);
    $(".actualProdForTheYear").val(obj.actualProdForTheYear);
    $(".monthStart").val(obj.monthStart);
    $(".monthEnd").val(obj.monthEnd);
    $(".hatcherydataSource").val(obj.dataSource);
	$(".hatcheryarea").val(obj.area);
	$(".code").val(obj.code);
	$(".hatcherydateAsOf").val(obj.dateAsOf);
	$(".hatcheryremarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image_src);
	$(".image").val(obj.image);
	$(".preview").attr("src", obj.image_src);


    var markup = 
    "<tr><th scope=\"col\">HATCHERY LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
    "<tr><th scope=\"col\">HATCHERY INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">Name of Hatchery:</th><td>"+obj.nameOfHatchery+"</td></tr>"+
    "<tr><th scope=\"row\">Address of Operator:</th><td>" + obj.addressOfOperator + "</td></tr>"+
    "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
    "<tr><th scope=\"row\">Indicate Species:</th><td>" + obj.indicateSpecies + "</td></tr>"+
    "<tr><th scope=\"row\">Type:</th><td>" + obj.type + "</td></tr>"+
    "<tr><th scope=\"row\">Stocking Density/Cycle:</th><td>" + obj.prodStocking + "</td></tr>"+
  "<tr><th scope=\"row\">Cycle/Year:</th><td>" + obj.prodCycle + "</td></tr>"+
  "<tr><th scope=\"row\">Actual Production for the year:</th><td>" + obj.actualProdForTheYear + "</td></tr>"+
  "<tr><th scope=\"col\">Month range of production cycle:</th><th scope=\"col\">&nbsp;</th></tr>"+
  
  "<tr><th scope=\"row\">Month Start:</th><td>" + obj.monthStart + "</td></tr>"+
  "<tr><th scope=\"row\">Month End:</th><td>" + obj.monthEnd + "</td></tr>"+
  

  
  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
  
  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
 
    content.append(markup);
    
    
    markup = "";
    $('#img_preview').show();
    $('#pdf').show();
    $('#table').hide();			
}

	
}



