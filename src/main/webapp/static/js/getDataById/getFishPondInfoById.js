function getFishPondInfoById(data, status,resProvince ){
	
	var content =  $("#pdfFISHPOND").find('tbody').empty();
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		$(".id").val(obj.id);
	    $(".user").val(obj.user);
	    $(".uniqueKey").val(obj.uniqueKey);
	    $(".dateEncoded").val(obj.dateEncoded);
	    $(".encodedBy").val(obj.encodedBy);
	 	//$(".region").val(obj.region);
	    
	    var province = obj.province_id;
	    var slctProvice=$('.province'), optionProvince="";
			slctProvice.empty();
			optionProvince = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";

		 $.get("getProvinceList", function(data, status){     	  			
			for( let prop in data ){
			var getprovince	= data[prop].province_id;	
			if(getprovince != province){			
			optionProvince = optionProvince + "<option value='"+data[prop].province_id+ "," + data[prop].province+"'>"+data[prop].province+"</option>";	           
			}
			}
		
		 slctProvice.append(optionProvince);
	 	}); 
	 	 					
		var slctMunicipal=$('.municipality'), option="";
		var municipal =obj.municipality_id;
		slctMunicipal.empty();
		option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
		
		$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
				for( let prop in data ){
					var getMunicipal = data[prop].municipal_id;
					if(getMunicipal != municipal){
					option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
					}
					}
		slctMunicipal.append(option);
	}); 
		
	var slctBarangay=$('.barangay'), brgyoption="";
	var barangay = obj.barangay_id;
			slctBarangay.empty();
		brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";

	$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	console.log("getBarangayList: " + data);  					
				for( let prop in data ){
					var getbarangay = data[prop].barangay_id;
					if(getbarangay != barangay){					
						brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
					}
					
					}
		slctBarangay.append(brgyoption);
	}); 

    $(".nameOfFishPondOperator").val(obj.nameOfFishPondOperator);            
    $(".nameOfActualOccupant").val(obj.nameOfActualOccupant);
    $(".typeOfWater").val(obj.typeOfWater);
    $(".fishPond").val(obj.fishPond);
    $(".fishPondType").val(obj.fishPondType);
    $(".yearIssued").val(obj.yearIssued);
    $(".yearExpired").val(obj.yearExpired);
    $(".fishPondCondition").val(obj.fishPondCondition);
    $(".nonActive").val(obj.nonActive);
    $(".fishpondspeciesCultured").val(obj.speciesCultured);
    $(".status").val(obj.status);
    $(".kind").val(obj.kind);
    $(".hatchery").val(obj.hatchery);
    $(".productPer").val(obj.productPer);
    $(".fishponddataSource").val(obj.dataSource);
	$(".fishpondarea").val(obj.area);
	$(".code").val(obj.code);
	$(".fishponddateAsOf").val(obj.dateAsOf);
	$(".fishpondremarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image_src);
	$(".image").val(obj.image);
	$(".preview").attr("src", obj.image_src);


    var markup = 
    "<tr><th scope=\"col\">FISH POND LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
    "<tr><th scope=\"col\">FISH POND INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">Name of Fishpond Lessee:</th><td>"+obj.nameOfFishPondOperator+"</td></tr>"+
    "<tr><th scope=\"row\">Name of Actual Occupant:</th><td>" + obj.nameOfActualOccupant + "</td></tr>"+
   "<tr><th scope=\"row\">Fish Pond Type:</th><td>" + obj.fishPondType + "</td></tr>"+
   
   "<tr><th scope=\"row\">Year Issued:</th><td>" + obj.yearIssued + "</td></tr>"+
   "<tr><th scope=\"row\">Year Expired:</th><td>" + obj.yearExpired + "</td></tr>"+
  "<tr><th scope=\"row\">Fish Pond Condition:</th><td>" + obj.fishPondCondition + "</td></tr>"+
  "<tr><th scope=\"row\">if Non-Active:</th><td>" + obj.nonActive + "</td></tr>"+
  "<tr><th scope=\"row\">Area (sqm.):</th><td>" + obj.area + "</td></tr>"+
  
  "<tr><th scope=\"row\">Type of Water:</th><td>" + obj.typeOfWater + "</td></tr>"+
  "<tr><th scope=\"row\">Status:</th><td>" + obj.status + "</td></tr>"+
  "<tr><th scope=\"row\">Species Commodities:</th><td>" + obj.speciesCultured + "</td></tr>"+
  "<tr><th scope=\"row\">Kind:</th><td>" + obj.kind + "</td></tr>"+
  "<tr><th scope=\"row\">Hatchery:</th><td>" + obj.hatchery + "</td></tr>"+
  "<tr><th scope=\"row\">Product Per Cropping:</th><td>" + obj.productPer + "</td></tr>"+
  
  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
  
  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
 
    
    
    content.append(markup);
    
    
    markup = "";
    $('#img_preview').show();
    $('#pdf').show();
    $('#table').hide();			
}

	
}