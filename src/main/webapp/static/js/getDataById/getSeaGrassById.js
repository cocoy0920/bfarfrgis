
function getSeagrassInfoById(data, status,resProvince ){
	var content = $("#pdfSEAGRASS").find('tbody').empty();
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		console.log("obj" + obj);
    $(".id").val(obj.id);
    $(".user").val(obj.user);
    $(".uniqueKey").val(obj.uniqueKey);
    $(".dateEncoded").val(obj.dateEncoded);
    $(".encodedBy").val(obj.encodedBy);
 	//$(".region").val(obj.region);

 var slctProvice=$('.seagrassprovince'), option="";
 slctProvice.empty();
        option = "<option value='"+obj.province_id+ "," + obj.province+"'>"+obj.province+"</option>";
       
        JSON.parse(resProvince).forEach(
			function(element) {
					console.log(element.province);
					
					option = option + "<option value='"+element.province_id+ "," + element.province+"'>"+element.province+"</option>";
       
			})  
			
			
			
			 slctProvice.append(option);
/* 
 var slctMunicipal=$('.seagrassmunicipality'), option="";
 slctMunicipal.empty();
        option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
        slctMunicipal.append(option);
        
var slctBarangay=$('.seagrassbarangay'), option="";
 slctBarangay.empty();
        option = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
        slctBarangay.append(option);


        */
        $(".seagrassindicateGenus").val(obj.indicateGenus);
    	$(".seagrassculturedMethodUsed").val(obj.culturedMethodUsed);
    	$(".seagrassculturedPeriod").val(obj.culturedPeriod);
    	$(".seagrassvolumePerCropping").val(obj.volumePerCropping);
    	$(".noOfCroppingPerYear").val(obj.noOfCroppingPerYear);
    	
    $(".seagrassdataSource").val(obj.dataSource);
	$(".seagrassarea").val(obj.area);
	$(".code").val(obj.code);
	$(".seagrassdateAsOf").val(obj.dateAsOf);
	$(".seagrassremarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image_src);
	$(".image").val(obj.image);
	$(".preview").attr("src", obj.image_src);


    var markup = 
    "<tr><th scope=\"col\">SEAGRASS LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
    
    "<tr><th scope=\"col\">SEAGRASS INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">Indicate Genus:</th><td>"+obj.indicateGenus+"</td></tr>"+
    
   "<tr><th scope=\"row\">Cultured Method Used:</th><td>" + obj.culturedMethodUsed + "</td></tr>"+

   "<tr><th scope=\"row\">Cultured Period:</th><td>" + obj.culturedPeriod + "</td></tr>"+
   "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
   "<tr><th scope=\"col\">PRODUCTION</th><th scope=\"col\">&nbsp;</th></tr>"+
   
   "<tr><th scope=\"row\">Volume per Cropping (Kgs.):</th><td>" + obj.volumePerCropping + "</td></tr>"+
  "<tr><th scope=\"row\">No. of Cropping per Year:</th><td>" + obj.noOfCroppingPerYear + "</td></tr>"+
 
  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.fishSanctuarySource + "</td></tr>"+
  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
 
  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
 
    
   content.append(markup);
    
    
    markup = "";
    $('#img_preview').show();
    $('#pdf').show();
    $('#table').hide();	
}

	
}

