	
	
var map =L.map('map',{scrollWheelZoom:false, zoomControl: false }).setView([8.678300, 125.800369],6);

  var OpenTopoMap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
    opacity: 0.90
  });
  
 // OpenTopoMap.addTo(map);

//	map.zoomControl.setPosition('topright');
	var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
				maxZoom: 18,
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'});
	
	
	

	//osm.addTo(map);
	// https: also suppported.
	 var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {	
		 maxZoom: 10,
		attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
	});
	 Esri_WorldImagery.addTo(map);
	 
	// https: also suppported.
	var Esri_WorldGrayCanvas = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'});
		
	var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {	
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
	});
	
	
/* 	var wmsLayer = L.tileLayer.wms('http://geo.bfar.da.gov.ph/gwc/service/wms', {
    layers: 'region:Region_I'
	});
	 */
	
L.control.scale().addTo(map);

function style(feature) {
    return {
        fillColor: 'green', 
        fillOpacity: 0.5,  
        weight: 2,
        opacity: 1,
        color: '#ffffff',
        dashArray: '3'
    };
}

	var highlight = {
		'fillColor': 'yellow',
		'weight': 2,
		'opacity': 1
	};


		
  		function polySelect(a){
  		console.log("polySelect(a): " + a);
			map._layers[a].fire('click'); 
			var layer = map._layers[a];
			map.fitBounds(layer.getBounds());
        }
  //    var geojsonLayer = new L.GeoJSON.AJAX(map_data);

     
//geojsonLayer.addTo(map);       
 var fishSancturiesGroup = [];
 
 
      	/*var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});*/
			$(".se-pre-con").show();
			
			var LamMarkerList = [];
			var group;
			var layerGroup;
		    /*function setMarkers(data) {
	        	
	        	
	        	  data.forEach(function (obj) {
	        		  
	        		 
	        		  LamMarker = new L.marker([obj.lat,obj.lon],
	        				  {icon: obj.greenIcon}).bindPopup(obj.html + '<img class="imgicon" id="myImg" src="'+obj.image+'" style="width:300px;max-width:250px;height:250px"></img>');
					
	        		  
	        		  LamMarker.getPopup().on('remove', function() {
							 console.log("REMOVE");
							// window.location.reload();

								clearClickMark();
								$(".se-pre-con").hide();
						});
	        		  LamMarkerList.push(LamMarker);
	        		  
	        	  });*/
	        	  function clickZoom(e) {
	                   map.setView(e.target.getLatLng(),17);
	               }
	        	  function clickZoomOut(e) {
	                   map.setView(e.target.getLatLng(),6);
	               }
	        	  
	        //	 group = L.featureGroup(LamMarkerList).addTo(map);
	        //	  map.fitBounds(group.getBounds());
	        	// layerGroup = L.layerGroup(LamMarkerList);
	           //  layerGroup.addTo(map);

	        //	}
	
/*	        $('#fishprocessingplants').on('click', function () {
	        	
	        	if ($('#fishprocessingplants').is(':checked') == true){	        	
	        	 	var page = $('#fishprocessingplants').val();
	        	 
	        		$.get("/home/getFPData/" + page, function(content, status){
	        			console.log(status + ": " + content);
	        			getResources(content);
	        		});	
	        	}

	        });
 				$('#fishsanctuaries').on('click', function () {
	        	
	        	if ($('#fishsanctuaries').is(':checked') == true){	        	
	        	 	var page = $('#fishsanctuaries').val();
	        	 
	        		$.get("/home/getFPData/" + page, function(content, status){
	        			console.log(status + ": " + content);
	        			getResources(content);
	        		});	
	        	}
	        	if ($('#fishsanctuaries').is(':checked') == false){	 
	        		console.log("FALSE");
	        		 window.location.reload();
	        		getAllCheckResources();
	        	}

	        });
 			$('#fishlanding').on('click', function () {
 		        	
 		        	if ($('#fishlanding').is(':checked') == true){	        	
 		        	 	var page = $('#fishlanding').val();
 		        	 
 		        		$.get("/home/getFPData/" + page, function(content, status){
 		        			console.log(status + ": " + content);
 		        			getResources(content);
 		        		});	
 		        	}

 		    });	
 			$('#FISHPORT').on('click', function () {
		        	
		        	if ($('#FISHPORT').is(':checked') == true){	        	
		        	 	var page = $('#FISHPORT').val();
		        	 
		        		$.get("/home/getFPData/" + page, function(content, status){
		        			console.log(status + ": " + content);
		        			getResources(content);
		        		});	
		        	}

		    });
 			$('#fishpen').on('click', function () {
	        	
	        	if ($('#fishpen').is(':checked') == true){	        	
	        	 	var page = $('#fishpen').val();
	        	 
	        		$.get("/home/getFPData/" + page, function(content, status){
	        			console.log(status + ": " + content);
	        			getResources(content);
	        		});	
	        	}

	   		 });
			$('#fishcage').on('click', function () {
	        	
	        	if ($('#fishcage').is(':checked') == true){	        	
	        	 	var page = $('#fishcage').val();
	        	 
	        		$.get("/home/getFPData/" + page, function(content, status){
	        			console.log(status + ": " + content);
	        			getResources(content);
	        		});	
	        	}

	   		 });
			$('#iceplantorcoldstorage').on('click', function () {
	        	
	        	if ($('#iceplantorcoldstorage').is(':checked') == true){	        	
	        	 	var page = $('#iceplantorcoldstorage').val();
	        	 
	        		$.get("/home/getFPData/" + page, function(content, status){
	        			console.log(status + ": " + content);
	        			getResources(content);
	        		});	
	        	}

	   		 });
			$('#market').on('click', function () {
	        	
	        	if ($('#market').is(':checked') == true){	        	
	        	 	var page = $('#market').val();
	        	 
	        		$.get("/home/getFPData/" + page, function(content, status){
	        			console.log(status + ": " + content);
	        			getResources(content);
	        		});	
	        	}

	   		 });
			$('#schooloffisheries').on('click', function () {
	        	
	        	if ($('#schooloffisheries').is(':checked') == true){	        	
	        	 	var page = $('#schooloffisheries').val();
	        	 
	        		$.get("/home/getFPData/" + page, function(content, status){
	        			console.log(status + ": " + content);
	        			getResources(content);
	        		});	
	        	}

	   		 });
			$('#fishcorals').on('click', function () {
	        	
	        	if ($('#fishcorals').is(':checked') == true){	        	
	        	 	var page = $('#fishcorals').val();
	        	 
	        		$.get("/home/getFPData/" + page, function(content, status){
	        			console.log(status + ": " + content);
	        			getResources(content);
	        		});	
	        	}

	   		 });
			$('#seagrass').on('click', function () {
	        	
	        	if ($('#seagrass').is(':checked') == true){	        	
	        	 	var page = $('#seagrass').val();
	        	 
	        		$.get("/home/getFPData/" + page, function(content, status){
	        			console.log(status + ": " + content);
	        			getResources(content);
	        		});	
	        	}

	   		 });
			$('#seaweeds').on('click', function () {
	        	
	        	if ($('#seaweeds').is(':checked') == true){	        	
	        	 	var page = $('#seaweeds').val();
	        	 
	        		$.get("/home/getFPData/" + page, function(content, status){
	        			console.log(status + ": " + content);
	        			getResources(content);
	        		});	
	        	}

	   		 });
			$('#mangrove').on('click', function () {
	        	
	        	if ($('#mangrove').is(':checked') == true){	        	
	        	 	var page = $('#mangrove').val();
	        	 
	        		$.get("/home/getFPData/" + page, function(content, status){
	        			console.log(status + ": " + content);
	        			getResources(content);
	        		});	
	        	}

	   		 });
			$('#mariculturezone').on('click', function () {
	        	
	        	if ($('#mariculturezone').is(':checked') == true){	        	
	        	 	var page = $('#mariculturezone').val();
	        	 
	        		$.get("/home/getFPData/" + page, function(content, status){
	        			console.log(status + ": " + content);
	        			getResources(content);
	        		});	
	        	}

	   		 });*/
			/*$('#lgu').on('click', function () {
	        	
	        	if ($('#lgu').is(':checked') == true){	        	
	        	 	var page = $('#lgu').val();
	        	 
	        		$.get("/home/getFPData/" + page, function(content, status){
	        			console.log(status + ": " + content);
	        			getResources(content);
	        		});	
	        	}

	   		 });
			$('#trainingcenter').on('click', function () {
	        	
	        	if ($('#trainingcenter').is(':checked') == true){	        	
	        	 	var page = $('#trainingcenter').val();
	        	 
	        		$.get("/home/getFPData/" + page, function(content, status){
	        			console.log(status + ": " + content);
	        			getResources(content);
	        		});	
	        	}

	   		 });*/
 				
// 				$('#hatchery').on('click', function () {
// 					
// 					var LeafIconMarker = L.Icon.extend({
// 						 options: {
// 					iconSize:     [30, 48]
// 				}
// 				});
// 		        	
// 		        	if ($('#hatchery').is(':checked') == true){	        	
// 		        	 	var page = $('#hatchery').val();
// 		        	 	
// 		        	 	var iconComplete = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/hatcheries.png"});
// 		        	 	var iconNComplete = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/mpa.png"});
// 		        	 	$.getJSON("/static/geojson/StatusofLegislatedHatcheries.geojson",function(data){
// 		       	 		L.geoJson(data  ,{
// 		       		    pointToLayer: function(feature,latlng){
// 		       		    	if(feature.properties.Status == "Completed"){
// 		       		    		
// 		       		    	HatchMarker = new L.marker(latlng,{icon: iconComplete}).addTo(map).bindPopup(feature.properties.Location + "<br>" + feature.properties.RANo + "<br>" + feature.properties.Status);
// 		       		    	}else{
// 		       		    	HatchMarker = new L.marker(latlng,{icon: iconNComplete}).addTo(map).bindPopup(feature.properties.Location + "<br>" + feature.properties.RANo + "<br>" + feature.properties.Status);	
// 		       		    	}
// 		       		    	HatchMarker.getPopup().on('remove', function() {
//
// 		       					// window.location.reload();
//
// 		       				});
// 		       		    }
// 		       	 		
// 		       		  })
// 		       	  function clickZoom(e) {
//  		                   map.setView(e.target.getLatLng(),17);
//  		               }
//  		       		}); 
 		       		 
 		        		/* $.get("/home/getFPData/" + page, function(content, status){
 		        			console.log(status + ": " + content);
 		        			getResources(content);
 		        		}); */	
 		      //  	}

 		       // });
		   /* function getResources(map_location) {
			
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				

					fishSancturiesGroup
							.push({
								lon : obj.lon,
								lat : obj.lat,
								icon : obj.icon,
								html : obj.html,
								image:obj.image,								
								greenIcon : new LeafIcon({
									iconUrl : obj.icon
								})
							});
				
			}
			
			setMarkers(fishSancturiesGroup);
			}*/


		    
	   		
	   		
/*	        function getAllCheckResources() {
	            var resources = [];
		   		  var pages="";
	            
	      	  var markedCheckbox = document.getElementsByName('page');  
	            for (var checkbox of markedCheckbox) {  
	              if (checkbox.checked)  

	          	   resources.push(checkbox.value);
	              
	            } 
					console.log("GET" + resources);
	           $.ajax({
	                    type : "GET",
	                   url : '/home/resources-get-by-pages-home',
	                    data : {resources: resources},
	                   success : function(content) {
	                  	 		console.log("success --> home " + "\r" + content);
	                  	 		getResources(content);
	                        if(content){
	                       	// console.log(data);
	                            //  loadChannels();
	                           //location.reload();
	                            }else{
	                        alert("failed to upload");

	                       }
	                    }
	           });

	   	  }*/
		    
	        
			var LeafIconMarker = L.Icon.extend({
				 options: {
						iconSize:     [30, 48]
						}
				});
		    
	        var geojsonMarkerOptions = {
					radius: 8,
					fillColor: "#ff7800",
					color: "#000",
					weight: 1,
					opacity: 1,
					fillOpacity: 0.8
					};
	        
	        var HatcheryComplete = new L.LayerGroup();
	        var HatcheryinComplete = new L.LayerGroup();
	        var iconComplete = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/hatcheries.png"});
	        var iconNComplete = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/mpa.png"});
		       		$.getJSON("/static/geojson/StatusofLegislatedHatcheries.geojson",function(data){
		       			console.log("/static/geojson/StatusofLegislatedHatcheries.geojson" + data);
		       		L.geoJSON(data, {
					pointToLayer: function (feature, latlng) {
						if(feature.properties.Status == "Completed"){
							return L.marker(latlng,{icon: iconComplete});
						}else{
							return L.marker(latlng,{icon: iconNComplete});
						}
    				
    				},
						onEachFeature: function (feature, layer) {
							if(feature.properties.Status == "Completed"){
							HatcheryComplete.addLayer(layer.bindPopup('<p>Location: '+feature.properties.Region +","+feature.properties.Location+'<br>RANo: '+feature.properties.RANo+'<br>Status: '+feature.properties.Status+'<br>Coordinates: '+feature.geometry.coordinates+'</p>').on('click', clickZoom));
							layer.getPopup().on('remove',clickZoomOut);
							}else{
							HatcheryinComplete.addLayer(layer.bindPopup('<p>Location:'+feature.properties.Region +"," +feature.properties.Location+'<br>RANo: '+feature.properties.RANo+'<br>Status: '+feature.properties.Status+ '<br>Coordinates: '+feature.geometry.coordinates+'</p>').on('click', clickZoom));
							layer.getPopup().on('remove',clickZoomOut);	
							}
							}
				})//.addTo(map);
		       		
		       	}); 
		       	 var fsicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishsanctuary.png"});
		       		var FS = new L.LayerGroup();
		       		
		       		$.getJSON("/home/getFSGeoJson",function(data){
		       			
		       			L.geoJSON(data, {
		       				
							pointToLayer: function (feature, latlng) {	
								
									return L.marker(latlng,{icon: fsicon});
										    				
		    				},
								onEachFeature: function (feature, layer) {
									FS.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: Fish Sanctuary' + '</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									}
						})
		       		}); 
		       	 var fpicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishprocessing.png"});
		    		var FP = new L.LayerGroup();
		    		
		       		$.getJSON("/home/getFPGeoJson",function(data){
		       			
		       			L.geoJSON(data, {
		       				
							pointToLayer: function (feature, latlng) {	
								
									return L.marker(latlng,{icon: fpicon});
										    				
		    				},
								onEachFeature: function (feature, layer) {
									FP.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+'<br>Resources: Fish Processing' + '</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									}
						})
		       		});
		       		
		       	 var flicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fish-landing.png"});
		    		var FL = new L.LayerGroup();
		    		
		       		$.getJSON("/home/getFLGeoJson",function(data){
		       			
		       			L.geoJSON(data, {
		       				
							pointToLayer: function (feature, latlng) {	
								
									return L.marker(latlng,{icon: flicon});
										    				
		    				},
								onEachFeature: function (feature, layer) {
									FL.addLayer(layer.bindPopup('<p>' +
											'Location: '+feature.properties.Location+
											'<br>Resources: Fish Landing' + 
											'</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									}
						})
		       		});
		       		
		       		
			       	 var fpticon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishport.png"});
			    		var FPT = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getFPTGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: fpticon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										FPT.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+
												'<br>Resources: Fish Port' + 
												'</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var fpenicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishpen.png"});
			    		var FPEN = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getFPenGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: fpenicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										FPEN.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
												'<br>Resources: Fish Pen'+'</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var fcageicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishcage.png"});
			    		var FCage = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getFCageGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: fcageicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										FCage.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
												'<br>Resources: Fish Cages' + '</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var ipcsicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/iceplant.png"});
			    		var IPCS = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getIPCSGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: ipcsicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										IPCS.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+
												'<br>Name: '+feature.properties.Name+
												'<br>Resources: Cold Storage' + 
												'</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var marketicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/market.png"});
			    		var Market = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getMarketGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: marketicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										Market.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+
												'<br>Resources: Market' + '</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var schoolicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/schooloffisheries.png"});
			    		var School = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getSchoolGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: schoolicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										School.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+'<br>Resources: School of Fisheries' +'</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		var fishcoralsicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishcorral.png"});
			    		var Fishcorals = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getFishCoralsGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: fishcoralsicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										Fishcorals.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
												'<br>Resources: Fish Corals' +'</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var seagrassicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/seagrass.png"});
			    		var Seagrass = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getSeagrassGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: seagrassicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										Seagrass.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
												'<br>Resources: Seagrass' + '</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var seaweedsicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/seaweeds.png"});
			    		var Seaweeds = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getSeaweedsGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: seaweedsicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										Seaweeds.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
												'<br>Resources: Seaweeds' +'</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		var mangroveicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/mangrove.png"});
			    		var Mangrove = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getMangroveGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: mangroveicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										Mangrove.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
												'<br>Resources: Mangrove' +'</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		var lguicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/lgu.png"});
			    		var LGU = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getLGUGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: lguicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										LGU.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
												'<br>Resources: PFO' + '</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var maricultureicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/mariculture.png"});
			    		var Mariculture = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getMaricultureGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: maricultureicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										Mariculture.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
												'<br>Resources: Mariculture Zone' + '</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var trainingcentericon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fisheriestraining.png"});
			    		var Trainingcenter = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getTrainingcenterGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: trainingcentericon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										Trainingcenter.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
												'<br>Resources: Training Center' + '</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
        
var baseMaps = {
    /* "Open Street Map": osm,
   	"OSM B&W":OpenStreetMap_BlackAndWhite,
   	"Esri WorldI magery":Esri_WorldImagery,
   	"Esri World Gray Canvas":Esri_WorldGrayCanvas, */
		
    "<img src='/static/images/iconCircle/hatcheries.png' style='width: 25px; height: 25px;'/>  Hatchery Complete":HatcheryComplete,
    "<img src='/static/images/iconCircle/hatcheries.png' style='width: 25px; height: 25px;'/> Hatchery Incomplete ":HatcheryinComplete,
    "<img src='/static/images/iconCircle/fishsanctuary.png' style='width: 25px; height: 25px;'/> Fish Sanctuary":FS,
    "<img src='/static/images/iconCircle/fishprocessing.png' style='width: 25px; height: 25px;'/> Fish Processing":FP,
    "<img src='/static/images/iconCircle/fish-landing.png' style='width: 25px; height: 25px;'/> Fish Landing":FL,
    "<img src='/static/images/iconCircle/fishport.png' style='width: 25px; height: 25px;'/> Fish Port":FPT,
    "<img src='/static/images/iconCircle/fishpen.png' style='width: 25px; height: 25px;'/> Fish Pen":FPEN,
    "<img src='/static/images/iconCircle/fishcage.png' style='width: 25px; height: 25px;'/> Fish Cage":FCage,
    "<img src='/static/images/iconCircle/iceplant.png' style='width: 25px; height: 25px;'/> Cold Storage":IPCS,
    "<img src='/static/images/iconCircle/market.png' style='width: 25px; height: 25px;'/> Market":Market,
    "<img src='/static/images/iconCircle/schoolof-fisheries.png' style='width: 25px; height: 25px;'/> School of Fisheries":School,
    "<img src='/static/images/iconCircle/fishcorral.png' style='width: 25px; height: 25px;'/> Fish Corals":Fishcorals,
    "<img src='/static/images/iconCircle/seagrass.png' style='width: 25px; height: 25px;'/> Sea Grass":Seagrass,
    "<img src='/static/images/iconCircle/seaweeds.png' style='width: 25px; height: 25px;'/> Sea Weeds":Seaweeds,
    "<img src='/static/images/iconCircle/mangrove.png' style='width: 25px; height: 25px;'/> Mangrove":Mangrove,
    "<img src='/static/images/iconCircle/lgu.png' style='width: 25px; height: 25px;'/> LGU":LGU,
    "<img src='/static/images/iconCircle/mariculturezone.png' style='width: 25px; height: 25px;'/> Mariculture":Mariculture,
    "<img src='/static/images/iconCircle/fisheriestraining.png' style='width: 25px; height: 25px;'/> Trainingcenter":Trainingcenter
};

 var overlayMaps = {
   "Hatchery Complete":HatcheryComplete,
   "Hatchery Incomplete ":HatcheryinComplete
};

L.control.layers(null,baseMaps,{collapsed:false}).addTo(map);
//L.control.layers(baseMaps).addTo(map);	
		var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});

		L.easyPrint({
			title: 'My awesome print button',
			position: 'bottomright',
			sizeModes: ['A4Portrait', 'A4Landscape'],
		    hideClasses: ['leaflet-control-zoom']
		}).addTo(map);
		/*  function getFisheriesData(){
			getAllResources(map_location);
		} */
 