	
	var map =L.map('map',{
		scrollWheelZoom:false,
		fullscreenControl: {
	        pseudoFullscreen: false,
	        position:'topright'
	    }
		})//.setView(center,8);
	map.zoomControl.setPosition('topright');
	
	var tiles = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}.png', {
		maxZoom: 18});

	//tiles.addTo(map);

	var OpenTopoMap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
	    maxZoom: 17,
	    attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
	    opacity: 0.90
	  });

	var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
					maxZoom: 18,
					attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'});
		
	var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {	
			 maxZoom: 10,
			attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
		});

	var Esri_WorldGrayCanvas = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {		
			attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'});
			
	var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {	
		attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
		});

	  Depth();
	  DepthByValue();
	 viewMapByRegion();
	 /* getMapByRegion();*/

/* 	var wmsLayer = L.tileLayer.wms('http://geo.bfar.da.gov.ph/gwc/service/wms', {
    layers: 'region:Region_I'
	});
	 */
	
L.control.scale().addTo(map);
       
			  	var baseMaps = {
			  		    "Open Street Map": osm,
			  		   	"Esri WorldI magery":Esri_WorldImagery,
			  		   	"Esri World Gray Canvas":Esri_WorldGrayCanvas,
			  		   	"Topo Map":OpenTopoMap,
			  		   	"Word Street Map":tiles
			  		};

			  	L.control.layers(baseMaps).addTo(map);

		var printer = L.easyPrint({
      		tileLayer: tiles,
      		sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
      		filename: 'myMap',
      		exportOnly: true,
      		position:'topright',
      		hideControlContainer: true
		}).addTo(map);
