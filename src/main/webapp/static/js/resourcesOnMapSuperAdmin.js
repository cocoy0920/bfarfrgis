//getRregions();
//14.655999, 121.048471

var region_id = "all";
var province_id = "all";
var region;

	 
var map =L.map('map',{
	scrollWheelZoom:false,
	fullscreenControl: {
        pseudoFullscreen: false,
        position:'topright'
    }
	})//.setView([14.656098, 121.048466],8);

map.zoomControl.setPosition('topright');

var tiles = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}.png', {
	maxZoom: 18});

//tiles.addTo(map);

var OpenTopoMap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
    opacity: 0.90
  });

var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
				maxZoom: 18,
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'});

var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {	
		 maxZoom: 10,
		attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
	});

var Esri_WorldGrayCanvas = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'});
Esri_WorldGrayCanvas.addTo(map);	
var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {	
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
	});

	
	var southWest = new L.LatLng(4.692551, 119.402422),
    northEast = new L.LatLng(20.809490, 121.860375),
    bounds = new L.LatLngBounds(southWest, northEast);
	map.fitBounds(bounds);
	

	// getAllGeoJson(); 
	// getPhilippineMap();
	  Depth();
	  DepthByValue();
	  Menu();
	  FMA_SEA();
	  getAllCheckByResources();
	 

	  $('#Regions').change(function(){

			clearMap();
			unchecked();
			
	      var ddlOption = $("#Regions option:selected").text();
		    var ddlValue = $("#Regions option:selected").val();
		    
		   // console.log("Regions: " + ddlValue);
		  //  alert('Filtered by Region' + ddlValue);
		    
		    region_id = ddlValue;
		    
		    if(region_id == "all"){
		    	location.reload();
		    	//getAllFeaturesByRegion();
		    	 var slctProvinces=$('.provincemap').find('option').remove().end();
			   	  var option="";
			   	  
			   	  slctProvinces.empty();
			       option = "<option value=''>-- PLS SELECT--</option>";
			       slctProvinces.append(option);
		    	getAllGeoJson();
		    	//getAllMap();
		    	clearMap();
		    	unchecked();
		    	
		    }else{	
		    	clearMap();
		    	unchecked();
		    	console.log('#Regions' + region_id);
		    	getMapByRegionAdmin();
		    	 getGeoJsonByRegion(region_id);
		    	 var slctProvinces=$('.provincemap').find('option').remove().end();
		   	  var option="";
		   	  
		   	  slctProvinces.empty();
		       option = "<option value=''>-- PLS SELECT--</option>";
		       $.get("/create/getProvinceList/" + region_id , function(data, status){     	  			
		   						for( let prop in data ){						 					
		   								option = option + "<option value='"+data[prop].province_id+"'>"+data[prop].province+"</option>";	           
		   							}
		   					
		   					 slctProvinces.append(option);
		          		 	});
		       		//getAllGeoJson();
		      
		    	//	getMapPerRegion(region_id);
		    	
		    }
	  });
  
	  $(".provincemap").change(function(){
		   ddlOption = $("#provincemap option:selected").text();
		   province_id = $("#provincemap option:selected").val();
		   console.log("Regions: " + region_id + " : " + "Province: " + province_id + " : " + ddlOption);
		  clearMap();
		  unchecked();
		  if(province_id == "" || province_id == "all"){
			  console.log("Regions: " + region_id + " : " + "Province: " + province_id + " : " + ddlOption);
			  getGeoJsonByRegion(region_id);
		  }else{
			  getGeoJsonByRegionAndProvince(region_id,province_id);
		  }
	  });
  
				  
//function getGeoJsonByRegion(region_id){
//				    $.getJSON("/admin/getAllGeoJson",function(data){
//		       			L.geoJSON(data, {
//				     	   		
//				     	   pointToLayer: pointToLayer,
//				     	   onEachFeature:feature
//				 		})
//				 		
//		       			
//				 });
//}
	  
	  function getGeoJsonByRegion(region){
			map.spin(true);
			$.getJSON("/create/getGeoJsonRegion/" + region,function(data){
				console.log(data);
				setTimeout(function () {
				L.geoJSON(data, {
			   		
		   	   pointToLayer: pointToLayer,
		   	   onEachFeature: function(feature, layer){

		   		   
		 		  if(feature.properties.resources == "fishsanctuaries"){
		 			//  console.log(feature.properties.Image);
		 			 FS.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 			layer.getPopup().on('remove',clickZoomOut);
		 			  
		 		  }
		 		 if(feature.properties.resources == "fishprocessingplants"){
		 			 if(feature.properties.Operator === "BFAR MANAGED"){
		 				 FP_BFAR.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 					layer.getPopup().on('remove',clickZoomOut); 
		 			 }
		 			 if(feature.properties.Operator === "LGU MANAGED"){
		 				 FP_LGU.addLayer(layer.bindPopup( feature.properties.Information).on('click', clickZoom));
		 					layer.getPopup().on('remove',clickZoomOut);
		 			 }
		 			 if(feature.properties.Operator === "PFDA"){
		 				 FP_PFDA.addLayer(layer.bindPopup( feature.properties.Information).on('click', clickZoom));
		 					layer.getPopup().on('remove',clickZoomOut); 
		 			 }
		 			 if(feature.properties.Operator === "PRIVATE SECTOR"){
		 				 FP_PRIVATE.addLayer(layer.bindPopup( feature.properties.Information).on('click', clickZoom));
		 					layer.getPopup().on('remove',clickZoomOut); 
		 			 }
		 		  }
		 		if(feature.properties.resources == "fishlanding"){
		 			
		 			if(feature.properties.CFLCSTATUS === "Operational"){
		 				FL_operational.addLayer(layer.bindPopup( feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 			}
		 			if(feature.properties.CFLCSTATUS === "For Operation"){
		 				FL_for_operation.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 			}
		 			if(feature.properties.CFLCSTATUS === "For Completion"){
		 				FL_for_completion.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 			}
		 			if(feature.properties.CFLCSTATUS === "For Transfer"){
		 				FL_for_transfer.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 			}
		 			if(feature.properties.CFLCSTATUS === "Damaged"){
		 				FL_damaged.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 			}
		 			if(feature.properties.Type === "Non-CFLC"){
		 				FL_noncflc.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 			}
		 			if(feature.properties.Type === "Traditional"){
		 				FL_traditional.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 			}
		 		  }
		 		if(feature.properties.resources == "FISHPORT"){
		 			 FPT.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		if(feature.properties.resources == "fishpen"){
		 			 FPEN.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		if(feature.properties.resources == "fishcage"){
		 			 FCage.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		if(feature.properties.resources == "coldstorage"){
		 			 IPCS.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		if(feature.properties.resources == "market"){
		 			 Market.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		if(feature.properties.resources == "schoolOfFisheries"){
		 			 School.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		if(feature.properties.resources == "fishcoral"){
		 			 Fishcorals.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		if(feature.properties.resources == "seagrass"){
		 			 Seagrass.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		if(feature.properties.resources == "seaweeds"){
		 			 Seaweeds.addLayer(layer.bindPopup( feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		if(feature.properties.resources == "mangrove"){
		 			 Mangrove.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		if(feature.properties.resources == "lgu"){
		 			 LGU.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		if(feature.properties.resources == "mariculturezone"){
		 			 Mariculture.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		if(feature.properties.resources == "trainingcenter"){
		 			 Trainingcenter.addLayer(layer.bindPopup( feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		if(feature.properties.resources == "hatchery"){

		 				if(feature.properties.Legislative == "legislated"){
		 					Hatchery.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 					layer.getPopup().on('remove',clickZoomOut);
		 				}
		 				if(feature.properties.Legislative == "nonLegislated"){
		 					HatcheryinComplete.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 					layer.getPopup().on('remove',clickZoomOut);
		 				}

		 		  }
		 		
		 		if(feature.properties.resources == "nationalcenter"){
		 		
		 			NCENTER.addLayer(layer.bindPopup( feature.properties.Information).on('click', clickZoom));
		 			layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		if(feature.properties.resources == "regionaloffice"){
		 				ROFFICE.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		 				layer.getPopup().on('remove',clickZoomOut);
		 		  }
		 		
		 	  
		 }
				})
				map.spin(false);
				}, 3000);
		});
		}
	function getGeoJsonByRegionAndProvince(region_id,province_id){

					    $.getJSON("/create/getAllGeoJson",function(data){
			       			
					        L.geoJSON(data, {
					     	   		
					     	   pointToLayer: pointToLayer,
					     	   onEachFeature: featuresByProvince					     	   
					 		})
					 		
			       			
					 });
					   
				  }

			function getRegionName(region_id){

				console.log(region_id);
				switch (region_id) {
				  case "010000000":
					  region = "Region 1";
				    break;
				  case "020000000":
					  region = "Region 2";
				    break;
				  case "030000000":
					  region = "Region 3";
				    break;
				  case "040000000":
					  region = "Region 4-A";
				    break;
				  case "050000000":
					  region = "Region 5";
				    break;
				  case "060000000":
					  region = "Region 6";
				    break;
				  case "070000000":
					  region = "Region 7";
					  break;
					  
				  case "080000000":
					  region = "Region 8";
					  break;
				  case "090000000":
					  region = "Region 9";
					  break;
				  case "100000000":
					  region = "Region 10";
					  break;
				  case "110000000":
					  region = "Region 11";
					  break;
				  case "120000000":
					  region = "Region 12";
					  break;
				  case "160000000":
					  region = "Region 13";
					  break;
				  case "170000000":
					  region = "Region 4-B";
					  break;
				  case "140000000":
					  region = "CAR";
					  break;
				  case "150000000":
					  region = "BARMM";
					  break;
				  default:
					  region = "all";
					  break;
					  
					  
				}
				console.log(region);
				return region;
				
			}
	
	
L.control.scale().addTo(map);
	
     
 var fishSancturiesGroup = [];

			$(".se-pre-con").show();
			
			var LamMarkerList = [];
			var group;
			var layerGroup;
        
var baseMaps = {
    "Open Street Map": osm,
   	"Esri WorldI magery":Esri_WorldImagery,
   	"Esri World Gray Canvas":Esri_WorldGrayCanvas,
   	"Topo Map":OpenTopoMap,
   	"Word Street Map":tiles
		
//    "<img src='/static/images/iconCircle/hatcheries.png' style='width: 25px; height: 25px;'/>  Hatchery Complete":HatcheryComplete,
//    "<img src='/static/images/iconCircle/hatcheries.png' style='width: 25px; height: 25px;'/> Hatchery Incomplete ":HatcheryinComplete,
//    "<img src='/static/images/iconCircle/fishsanctuary.png' style='width: 25px; height: 25px;'/> Fish Sanctuary":FS,
//    "<img src='/static/images/iconCircle/fishprocessing.png' style='width: 25px; height: 25px;'/> Fish Processing":FP,
//    "<img src='/static/images/iconCircle/fish-landing.png' style='width: 25px; height: 25px;'/> Fish Landing":FL,
//    "<img src='/static/images/iconCircle/fishport.png' style='width: 25px; height: 25px;'/> Fish Port":FPT,
//    "<img src='/static/images/iconCircle/fishpen.png' style='width: 25px; height: 25px;'/> Fish Pen":FPEN,
//    "<img src='/static/images/iconCircle/fishcage.png' style='width: 25px; height: 25px;'/> Fish Cage":FCage,
//    "<img src='/static/images/iconCircle/iceplant.png' style='width: 25px; height: 25px;'/> Cold Storage":IPCS,
//    "<img src='/static/images/iconCircle/market.png' style='width: 25px; height: 25px;'/> Market":Market,
//    "<img src='/static/images/iconCircle/schoolof-fisheries.png' style='width: 25px; height: 25px;'/> School of Fisheries":School,
//    "<img src='/static/images/iconCircle/fishcorral.png' style='width: 25px; height: 25px;'/> Fish Corals":Fishcorals,
//    "<img src='/static/images/iconCircle/seagrass.png' style='width: 25px; height: 25px;'/> Sea Grass":Seagrass,
//    "<img src='/static/images/iconCircle/seaweeds.png' style='width: 25px; height: 25px;'/> Sea Weeds":Seaweeds,
//    "<img src='/static/images/iconCircle/mangrove.png' style='width: 25px; height: 25px;'/> Mangrove":Mangrove,
//    "<img src='/static/images/iconCircle/lgu.png' style='width: 25px; height: 25px;'/> LGU":LGU,
//    "<img src='/static/images/iconCircle/mariculturezone.png' style='width: 25px; height: 25px;'/> Mariculture":Mariculture,
//    "<img src='/static/images/iconCircle/fisheriestraining.png' style='width: 25px; height: 25px;'/> Trainingcenter":Trainingcenter

};




//L.control.layers(null,baseMaps,{collapsed:false}).addTo(map);
L.control.layers(baseMaps).addTo(map);	
		var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});
		
	
        var printer = L.easyPrint({
      		tileLayer: tiles,
      		sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
      		filename: 'myMap',
      		exportOnly: true,
      		position:'topright',
      		hideControlContainer: true
		}).addTo(map);
        
        
        
//
//		check_aquafarm = $('#aquafarm');
//		// identify the other checkboxes in the group
//		aquafarm_class = $('.aquafarm');
//		check_aquafarm.on('click',function(){
//			aquafarm_class.click();
//		});
//
//		check_aquaseaweed = $('#aquaseaweed');
//		// identify the other checkboxes in the group
//		aquaseaweed_class = $('.aquaseaweed');
//		check_aquaseaweed.on('click',function(){
//			aquaseaweed_class.click();
//		});
//		
//		check_aquamariculture = $('#aquamariculture');
//		// identify the other checkboxes in the group
//		aquamariculture_class = $('.aquamariculture');
//		check_aquamariculture.on('click',function(){
//			aquamariculture_class.click();
//		});
//
//		check_aquahatchery = $('#aquahatchery');
//		// identify the other checkboxes in the group
//		aquahatchery_class = $('.aquahatchery');
//		check_aquahatchery.on('click',function(){
//			aquahatchery_class.click();
//		});
//		
//		check_capture = $('#capture');
//		// identify the other checkboxes in the group
//		capture_class = $('.capture');
//		check_capture.on('click',function(){
//			capture_class.click();
//		});
//		
//		check_fishlanding = $('#fishlanding');
//		// identify the other checkboxes in the group
//		fishlanding_class = $('.fishlanding');
//		check_fishlanding.on('click',function(){
//			fishlanding_class.click();
//		});
//		
//		check_coldstorage = $('#coldstorage');
//		// identify the other checkboxes in the group
//		coldstorage_class = $('.coldstorage');
//		check_coldstorage.on('click',function(){
//			coldstorage_class.click();
//		});
//		
//		check_processing = $('#processing');
//		// identify the other checkboxes in the group
//		processing_class = $('.processing');
//		check_processing.on('click',function(){
//			processing_class.click();
//		});
//		
//		check_postseaweed = $('#postseaweed');
//		// identify the other checkboxes in the group
//		postseaweed_class = $('.postseaweed');
//		check_postseaweed.on('click',function(){
//			postseaweed_class.click();
//		});
//		
//		check_habitat = $('#habitat');
//		// identify the other checkboxes in the group
//		habitat_class = $('.habitat');
//		check_habitat.on('click',function(){
//			habitat_class.click();
//		});
//		
//		check_infrastructure = $('#infrastructure');
//		// identify the other checkboxes in the group
//		infrastructure_class = $('.infrastructure');
//		check_infrastructure.on('click',function(){
//			infrastructure_class.click();
//		});
//		
//		check_all = $('#FMA');
//		// identify the other checkboxes in the group
//		check_class = $('.FMA');
//		check_all.on('click',function(){
//		  check_class.click();
//		});
//		
//		major24_fg_all = $('#major24_fg');
//		//identify the other checkboxes in the group
//		major24_fg_class = $('.FG');
//		major24_fg_all.on('click',function(){
//		major24_fg_class.click();
//		});
//
/////AQUAFARM		
//		$("#checked_pond").change(function(){
//			console.log("#checked_pond");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//				//	getFMA1();
//
//			}else{
//			//	map.removeLayer(FMAJSON1);
//			
//			}
//			  
//		});	
//		$("#checked_fishpen").change(function(){
//
//			console.log("#checked_fishpen");	
//			  var $pen = $( this );
//			  var pen = $pen.attr( "value" );
//			  
//				if($pen.is(":checked")){
//					
//					//getFishPen();
//					FPEN.addTo(map);
//
//			}else{
//				map.removeLayer(FPEN);
//			
//			}
//			  
//		});	
//
//		$("#checked_fishcage").change(function(){
//			console.log("#checked_fishcage");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					FCage.addTo(map);
//
//			}else{
//				map.removeLayer(FCage);
//			
//			}
//			  
//		});	
//		$("#checked_fishcoral").change(function(){
//			console.log("#checked_fishcoral");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					Fishcorals.addTo(map);
//
//			}else{
//				map.removeLayer(Fishcorals);
//			
//			}
//			  
//		});	
//		
//		////SEAWEED AQUACULTURE
//		$("#checked_seaweednursery").change(function(){
//			console.log("#checked_seaweednursery");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					Seaweed_nursery.addTo(map);
//
//			}else{
//				map.removeLayer(Seaweed_nursery);
//			
//			}
//			  
//		});			
//				
//		$("#checked_seaweedlaboratory").change(function(){
//			  var $seaweeds = $( this );
//			  var seaweeds = $seaweeds.attr( "value" );
//				
//				if($seaweeds.is(":checked")){
//					console.log(seaweeds);
//						//getAllByRegion();
//						Seaweed_laboratory.addTo(map);
//			
//				}else{
//					map.removeLayer(Seaweed_laboratory);
//				}
//			  
//		});
//			
//	///MARICULTURE PARK
//		$("#checked_mariculturebfar").change(function(){
//			console.log("#checked_mariculturebfar");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					Mariculture_BFAR.addTo(map);
//
//			}else{
//				map.removeLayer(Mariculture_BFAR);
//			
//			}
//			  
//		});		
//		$("#checked_mariculturelgu").change(function(){
//			console.log("#checked_mariculturelgu");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					Mariculture_LGU.addTo(map);
//
//			}else{
//				map.removeLayer(Mariculture_LGU);
//			
//			}
//			  
//		});		
//		$("#checked_maricultureprivate").change(function(){
//			console.log("#checked_maricultureprivate");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					Mariculture_PRIVATE.addTo(map);
//
//			}else{
//				map.removeLayer(Mariculture_PRIVATE);
//			
//			}
//			  
//		});			
//	///HATCHERY
//		$("#checked_legislated").change(function(){
//			console.log("#checked_legislated");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					Hatchery.addTo(map);
//
//			}else{
//				map.removeLayer(Hatchery);
//			
//			}
//			  
//		});		
//		$("#checked_nonlegislated").change(function(){
//			console.log("#checked_nonlegislated");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					HatcheryinComplete.addTo(map);
//
//			}else{
//			map.removeLayer(HatcheryinComplete);
//			
//			}
//			  
//		});	
//	
//		$("#checked_hatcherylgu").change(function(){
//			console.log("#checked_hatcherylgu");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					Hatchery_LGU.addTo(map);
//
//			}else{
//				map.removeLayer(Hatchery_LGU);
//			
//			}
//			  
//		});
//		$("#checked_hatcheryprivate").change(function(){
//			console.log("#checked_hatcheryprivate");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					Hatchery_PRIVATE.addTo(map);
//
//			}else{
//				map.removeLayer(Hatchery_PRIVATE);
//			
//			}
//			  
//		});	
//		
//		///CAPTURE FISHERIES
//		
//		$("#checked_payao").change(function(){
//
//			  var $checked_lakes = $( this );
//			  var checked_lakes = $checked_lakes.attr( "value" );
//			  
//				if($checked_lakes.is(":checked")){
//					getPAYAO();
//					PAYAO.addTo(map);
//
//			}else{
//				map.removeLayer(PAYAO);
//
//			}
//			  
//			});
//		$("#checked_lambaklad").change(function(){
//			console.log("#checked_lambaklad");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					getLAMBAKLAD();
//					LAMBAKLAD.addTo(map);
//
//			}else{
//				map.removeLayer(LAMBAKLAD);
//			
//			}
//			  
//		});	
//		
//		$("#checked_frpboats").change(function(){
//			console.log("#checked_frpboats");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//				//	getFMA1();
//
//			}else{
//			//	map.removeLayer(FMAJSON1);
//			
//			}
//			  
//		});	
//		
//		//POST HARVEST
//		$("#check_cflc_operational").change(function(){
//
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					FL_operational.addTo(map);
//
//			}else{
//				map.removeLayer(FL_operational);
//			
//			}
//			  
//		});
//		
//		//FISH LANDING
//		$("#check_cflc_for_operation").change(function(){
//			console.log("#check_cflc_for_operation");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					FL_for_operation.addTo(map);
//
//			}else{
//				map.removeLayer(FL_for_operation);
//			
//			}
//			  
//		});		
//		$("#check_cflc_for_complition").change(function(){
//			console.log("#check_cflc_for_complition");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					FL_for_completion.addTo(map);	
//
//			}else{
//				map.removeLayer(FL_for_completion);
//			
//			}
//			  
//		});	
//		
//		$("#check_cflc_for_transfer").change(function(){
//			console.log("#check_cflc_for_transfer");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					FL_for_transfer.addTo(map);
//
//			}else{
//				map.removeLayer(FL_for_transfer);
//			
//			}
//			  
//		});	
//		
//		$("#check_cflc_damaged").change(function(){
//			console.log("#check_cflc_damaged");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					FL_damaged.addTo(map);
//
//			}else{
//				map.removeLayer(FL_damaged);
//			
//			}
//			  
//		});	
//		
//		$("#check_traditional").change(function(){
//			console.log("#check_traditional");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					FL_traditional.addTo(map);
//
//			}else{
//				map.removeLayer(FL_traditional);
//			
//			}
//			  
//		});	
//		
//		
//		$("#check_noncflc").change(function(){
//			console.log("#check_noncflc");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					 FL_noncflc.addTo(map);	
//
//			}else{
//				map.removeLayer(FL_noncflc);
//			
//			}
//			  
//		});		
//		
//		///COLD STORAGE/IPCS
//		$("#check_coldpfda").change(function(){
//			console.log("#check_coldpfda");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					IPCS_PFDA.addTo(map);
//
//			}else{
//			map.removeLayer(IPCS_PFDA);
//			
//			}
//			  
//		});	
//		
//		$("#check_coldprivate").change(function(){
//			console.log("#check_coldprivate");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					IPCS_PRIVATEORGANIZATION.addTo(map);
//
//			}else{
//				map.removeLayer(IPCS_PRIVATEORGANIZATION);
//			
//			}
//			  
//		});	
//		
//		$("#check_coldcooperative").change(function(){
//			console.log("#check_coldcooperative");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					IPCS_COOPERATIVE.addTo(map);
//
//			}else{
//				map.removeLayer(IPCS_COOPERATIVE);
//			
//			}
//			  
//		});		
//		
//		/// FISH PROCESSING FACILITY
//		
//		$("#check_processingbfar").change(function(){
//			console.log("#check_processingbfar");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					FP_BFAR.addTo(map);
//
//			}else{
//				map.removeLayer(FP_BFAR);
//			
//			}
//			  
//		});
//		
//		$("#check_processinglgu").change(function(){
//			console.log("#check_processinglgu");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					FP_LGU.addTo(map);
//
//			}else{
//				map.removeLayer(FP_LGU);
//			
//			}
//			  
//		});	
//		
//		$("#check_processingprivate").change(function(){
//			console.log("#check_processingprivate");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					FP_PRIVATE.addTo(map);
//
//			}else{
//				map.removeLayer(FP_PRIVATE);
//			
//			}
//			  
//		});
//		
//		$("#check_processingcooperative").change(function(){
//			console.log("#check_processingcooperative");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					FP_COOPERATIVE.addTo(map);
//
//			}else{
//				map.removeLayer(FP_COOPERATIVE);
//			
//			}
//			  
//		});
//		
//		///POST HARVEST SEAWEED
//		$("#check_warehouse").change(function(){
//			console.log("#check_warehouse");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					Seaweed_warehouse.addTo(map);
//
//			}else{
//				map.removeLayer(Seaweed_warehouse);
//			
//			}
//			  
//		});		
//
//		$("#check_dryer").change(function(){
//			console.log("#check_dryer");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					Seaweed_warehouse.addTo(map);
//
//			}else{
//				map.removeLayer(Seaweed_warehouse);
//			
//			}
//			  
//		});	
//		$("#checked_market").change(function(){
//			console.log("#check_dryer");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					Market.addTo(map);
//
//			}else{
//				map.removeLayer(Market);
//			
//			}
//			  
//		});	
//		
//		$("#checked_fishport").change(function(){
//			console.log("#checked_fishport");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//					FPT.addTo(map);
//
//			}else{
//				map.removeLayer(FPT);
//			
//			}
//			  
//		});	
//		
//		$("#checked_kadiwa").change(function(){
//			console.log("#checked_kadiwa");
//			  var $pond = $( this );
//			  var pond = $pond.attr( "value" );
//			  
//				if($pond.is(":checked")){
//				//	getFMA1();
//
//			}else{
//			//	map.removeLayer(FMAJSON1);
//			
//			}
//			  
//		});		
//	
//		
//		//FISH HABITAT
//		$("#checked_seagrass").change(function(){
//			console.log("#checked_seagrass");
//			  var $checked_lakes = $( this );
//			  var checked_lakes = $checked_lakes.attr( "value" );
//			  
//				if($checked_lakes.is(":checked")){
//					Seagrass.addTo(map);
//
//			}else{
//				map.removeLayer(Seagrass);
//			
//			}
//			  
//		});	
//		
//		$("#checked_mangrove").change(function(){
//			console.log("#checked_mangrove");
//			  var $checked_lakes = $( this );
//			  var checked_lakes = $checked_lakes.attr( "value" );
//			  
//				if($checked_lakes.is(":checked")){
//					Mangrove.addTo(map);
//
//			}else{
//				map.removeLayer(Mangrove);
//			
//			}
//			  
//		});	
//		
//		$("#checked_coralreefs").change(function(){
//			console.log("#checked_coralreefs");
//			  var $checked_lakes = $( this );
//			  var checked_lakes = $checked_lakes.attr( "value" );
//			  
//				if($checked_lakes.is(":checked")){
//				//	getLAKES();
//
//			}else{
//			//	map.removeLayer(LAKES);
//			
//			}
//			  
//		});	
//		
//		///BFAR INFRASTRUCTURE
//		
//		$("#check_nationalcenter").change(function(){
//			console.log("#check_nationalcenter");
//			  var $checked_lakes = $( this );
//			  var checked_lakes = $checked_lakes.attr( "value" );
//			  
//				if($checked_lakes.is(":checked")){
//					NCENTER.addTo(map);
//
//			}else{
//				map.removeLayer(NCENTER);
//			
//			}
//			  
//		});	
//		
//		$("#check_regionaloffices").change(function(){
//			console.log("#check_regionaloffices");
//			  var $checked_lakes = $( this );
//			  var checked_lakes = $checked_lakes.attr( "value" );
//			  
//				if($checked_lakes.is(":checked")){
//					ROFFICE.addTo(map);
//
//			}else{
//				map.removeLayer(ROFFICE);
//			
//			}
//			  
//		});	
//		
//		$("#check_rdtc").change(function(){
//			console.log("#check_rdtc");
//			  var $checked_lakes = $( this );
//			  var checked_lakes = $checked_lakes.attr( "value" );
//			  
//				if($checked_lakes.is(":checked")){
//				//	getLAKES();
//
//			}else{
//			//	map.removeLayer(LAKES);
//			
//			}
//			  
//		});	
//		
//		$("#checked_lgu").change(function(){
//			console.log("#checked_lgu");
//			  var $checked_lakes = $( this );
//			  var checked_lakes = $checked_lakes.attr( "value" );
//			  
//				if($checked_lakes.is(":checked")){
//					LGU.addTo(map);
//
//			}else{
//				map.removeLayer(LGU);
//			
//			}
//			  
//		});		
//
//		$("#check_tos").change(function(){
//			console.log("#check_tos");
//			  var $checked_lakes = $( this );
//			  var checked_lakes = $checked_lakes.attr( "value" );
//			  
//				if($checked_lakes.is(":checked")){
//					TOS.addTo(map);
//
//			}else{
//				map.removeLayer(TOS);
//			
//			}
//			  
//		});			
//
//		$("#checked_schoolOfFisheries").change(function(){
//			console.log("#checked_schoolOfFisheries");
//			  var $checked_lakes = $( this );
//			  var checked_lakes = $checked_lakes.attr( "value" );
//			  
//				if($checked_lakes.is(":checked")){
//					School.addTo(map);
//
//			}else{
//				map.removeLayer(School);
//			
//			}
//			  
//		});	
//		
//		$("#checked_fishsanctuaries").change(function(){
//			console.log("#checked_fishsanctuaries");
//			  var $checked_lakes = $( this );
//			  var checked_lakes = $checked_lakes.attr( "value" );
//			  
//				if($checked_lakes.is(":checked")){
//					FS.addTo(map);
//
//			}else{
//				map.removeLayer(FS);
//			
//			}
//			  
//		});		
//		

        
        
        
        
        
