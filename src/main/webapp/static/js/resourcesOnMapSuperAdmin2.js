
var region_id= "all";	
var missedShots;


	 
function clickZoom(e) {
	   map.setView(e.target.getLatLng(),17);
}

function clickZoomOut(e) {
	   map.setView(e.target.getLatLng(),6);
}

	        
var LeafIconMarker = L.Icon.extend({
		options: {
			iconSize:     [30, 48]
		}
});

var sanctuaryicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishsanctuary.png"});
var FS = new L.LayerGroup();

var processingicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishprocessing.png"});
var FP = new L.LayerGroup();

var landingicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fish-landing.png"});
var FL = new L.LayerGroup();

var porticon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishport.png"});
var FPT = new L.LayerGroup();

var penicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishpen.png"});
var FPEN = new L.LayerGroup();

var cageicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishcage.png"});
var FCage = new L.LayerGroup();

var ipcsicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/iceplant.png"});
var IPCS = new L.LayerGroup();

var marketicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/market.png"});
var Market = new L.LayerGroup();

var schoolicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/schooloffisheries.png"});
var School = new L.LayerGroup();

var fishcoralsicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishcorral.png"});
var Fishcorals = new L.LayerGroup();

var seagrassicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/seagrass.png"});
var Seagrass = new L.LayerGroup();

var seaweedsicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/seaweeds.png"});
var Seaweeds = new L.LayerGroup();

var mangroveicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/mangrove.png"});
var Mangrove = new L.LayerGroup();

var lguicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/lgu.png"});
var LGU = new L.LayerGroup();

var maricultureicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/mariculture.png"});
var Mariculture = new L.LayerGroup();

var trainingcentericon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fisheriestraining.png"});
var Trainingcenter = new L.LayerGroup();


var map =L.map('map',{scrollWheelZoom:false, zoomControl: false }).setView([8.678300, 125.800369],6);

  var OpenTopoMap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
    opacity: 0.90
  });
  
 // OpenTopoMap.addTo(map);

//	map.zoomControl.setPosition('topright');
	var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
				maxZoom: 18,
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'});
	
	
	

	//osm.addTo(map);
	// https: also suppported.
	 var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {	
		 maxZoom: 10,
		attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
	});
	 Esri_WorldImagery.addTo(map);
	 
	// https: also suppported.
	var Esri_WorldGrayCanvas = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'});
		
	var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {	
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
	});
	
	
/* 	var wmsLayer = L.tileLayer.wms('http://geo.bfar.da.gov.ph/gwc/service/wms', {
    layers: 'region:Region_I'
	});
	 */
		
	 var dropdownlist = L.control({
		    position: 'topleft'
		  });
		//	  $.get("/home/getAllRegion", function(content, status){
				 

					  dropdownlist.onAdd = function(map) {
						  var select;
						  var option;
						  var div = L.DomUtil.create('div', 'dropdownlist regions'); 
				  
				    
						  div.innerHTML = '<select id="Regions">'+
						  				'<option value = "">select Region</option>' +
						  				'<option value = "010000000">Region 1</option>' +
						  				'<option value = "020000000">Region 2</option>' +
						  				'<option value = "030000000">Region 3</option>' +
						  				'<option value = "040000000">Region 4-A</option>' +
						  				'<option value = "050000000">Region 5</option>' +
						  				'<option value = "060000000">Region 6</option>' +
						  				'<option value = "070000000">Region 7</option>' +
						  				'<option value = "080000000">Region 8</option>' +
						  				'<option value = "090000000">Region 9</option>' +
						  				'<option value = "100000000">Region 10</option>' +
						  				'<option value = "110000000">Region 11</option>' +
						  				'<option value = "120000000">Region 12</option>' +
						  				'<option value = "130000000">NCR</option>' +
						  				'<option value = "140000000">CAR</option>' +
						  				'<option value = "150000000">ARMM</option>' +
						  				'<option value = "160000000">CARAGA</option>' +
						  				'<option value = "170000000">Region 4-B</option>' +
						  				'</select>'
						  					
				    	
				    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
				    return div;
				  };

				  dropdownlist.addTo(map);  
			//  });
			  


//		  var legend = L.control({position: 'topright'});
//		  legend.onAdd = function (map) {
//		      var div = L.DomUtil.create('div', 'info legend');
//		      div.innerHTML = '<select><option>1</option><option>2</option><option>3</option></select>';
//		      div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
//		      return div;
//		  };
//		  legend.addTo(map);
		  

		  $('#Regions').change(function(){

 				FS.clearLayers();
 				map.removeLayer(FS);
 				
 				FP.clearLayers();
 				map.removeLayer(FP);
 				
 				FL.clearLayers();
 				map.removeLayer(FL);
 				
 				FPT.clearLayers();
 				map.removeLayer(FS);
 				
 				FPEN.clearLayers();
 				map.removeLayer(FPEN);
 				
 				FCage.clearLayers();
 				map.removeLayer(FCage);
 				
 				IPCS.clearLayers();
 				map.removeLayer(IPCS);
 				
 				Market.clearLayers();
 				map.removeLayer(Market);
 				
 				School.clearLayers();
 				map.removeLayer(School);
 				
 				Fishcorals.clearLayers();
 				map.removeLayer(Fishcorals);
 				
 				Seagrass.clearLayers();
 				map.removeLayer(Seagrass);
 				
 				Seaweeds.clearLayers();
 				map.removeLayer(Seaweeds);
 				
 				Mangrove.clearLayers();
 				map.removeLayer(Mangrove);
 				
 				LGU.clearLayers();
 				map.removeLayer(LGU);
 				
 				Mariculture.clearLayers();
 				map.removeLayer(Mariculture);
 				
 				Trainingcenter.clearLayers();
 				map.removeLayer(Trainingcenter);
 				
		      var ddlOption = $("#Regions option:selected").text();
			    var ddlValue = $("#Regions option:selected").val();
			    
			    console.log("Regions: " + ddlValue);
			    alert('Filtered by Region' + ddlValue);
			    
			    region_id = ddlValue;
			    
				    $.getJSON("/admin/getAllGeoJson",function(data){
		       			console.log("/admin/getAllGeoJson: " + region_id);
		       			
				        L.geoJSON(data, {
				     	   		
				     	   pointToLayer: function (feature, latlng) {
				     		  if(region_id == feature.properties.Region){
				     			  if(feature.properties.resources == "fishsanctuaries"){		
				     				  return L.marker(latlng,{icon: sanctuaryicon});
				     			  }
				     		  }
				     		 if(region_id == feature.properties.Region){
				     			  if(feature.properties.resources == "fishprocessingplants"){		
				     				  return L.marker(latlng,{icon: processingicon});
				     			  }
				     		  }
				     		 if(region_id == feature.properties.Region){
				     			  if(feature.properties.resources == "fishlanding"){		
				     				  return L.marker(latlng,{icon: landingicon});
				     			  }
				     		  }
				     		if(region_id == feature.properties.Region){
				     		if(feature.properties.resources == "FISHPORT"){		
			     				  return L.marker(latlng,{icon: porticon});
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "fishpen"){		
			     				  return L.marker(latlng,{icon: penicon});
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "fishcage"){		
			     				  return L.marker(latlng,{icon: cageicon});
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "coldstorage"){		
			     				  return L.marker(latlng,{icon: ipcsicon});
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "market"){		
			     				  return L.marker(latlng,{icon: marketicon});
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "schoolOfFisheries"){		
			     				  return L.marker(latlng,{icon: schoolicon});
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "fishcoral"){		
			     				  return L.marker(latlng,{icon: fishcoralsicon});
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "seagrass"){		
			     				  return L.marker(latlng,{icon: seagrassicon});
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "seaweeds"){		
			     				  return L.marker(latlng,{icon: seagrassicon});
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "mangrove"){		
			     				  return L.marker(latlng,{icon: mangroveicon});
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "lgu"){		
			     				  return L.marker(latlng,{icon: lguicon});
			     			  }
				     		}
			     			if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "mariculturezone"){		
			     				  return L.marker(latlng,{icon: maricultureicon});
			     			  }
			     			}
			     			if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "trainingcenter"){		
			     				  return L.marker(latlng,{icon: trainingcentericon});
			     			  }
			     			}
				     		 
				     	   },
				     	   onEachFeature: function (feature, layer) {
				     		   if(region_id == feature.properties.Region){
				     			  if(feature.properties.resources == "fishsanctuaries"){
				     				 FS.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH SANCTUARY' + '</p>'));
										layer.getPopup().on('remove',clickZoomOut);

				     			  }
				     		   }
				     		  if(region_id == feature.properties.Region){
				     			  if(feature.properties.resources == "fishprocessingplants"){
				     				 FP.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH PROCESSING' + '</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										
				     			  }
				     		   }
				     		 if(region_id == feature.properties.Region){
				     			  if(feature.properties.resources == "fishlanding"){
				     				 FP.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH LANDING' + '</p>'));
										layer.getPopup().on('remove',clickZoomOut);
										
				     			  }
				     		   }
				     		if(region_id == feature.properties.Region){
				     		if(feature.properties.resources == "fishpen"){
			     				 FPEN.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH PEN' + '</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									//console.log("fishprocessingplants" + feature.properties.Region);
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "fishcage"){
			     				 FCage.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH CAGE' + '</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									//console.log("fishprocessingplants" + feature.properties.Region);
			     			  }
			     			if(feature.properties.resources == "coldstorage"){
			     				 IPCS.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: COLD STORAGE' + '</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									//console.log("fishprocessingplants" + feature.properties.Region);
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "market"){
			     				 Market.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: MARKET' + '</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									//console.log("fishprocessingplants" + feature.properties.Region);
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "schoolOfFisheries"){
			     				 School.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: SCHOOL OF FISHERIES' + '</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									//console.log("fishprocessingplants" + feature.properties.Region);
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "fishcoral"){
			     				 Fishcorals.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH CORALS' + '</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									//console.log("fishprocessingplants" + feature.properties.Region);
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "seagrass"){
			     				 Seagrass.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: SEAGRASS' + '</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									//console.log("fishprocessingplants" + feature.properties.Region);
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "seaweeds"){
			     				 Seaweeds.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: SEAWEEDS' + '</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									//console.log("fishprocessingplants" + feature.properties.Region);
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
				
			     			if(feature.properties.resources == "mangrove"){
			     				 Mangrove.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: MANGROVE' + '</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									//console.log("fishprocessingplants" + feature.properties.Region);
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "lgu"){
			     				 LGU.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: PFO' + '</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									//console.log("fishprocessingplants" + feature.properties.Region);
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "mariculturezone"){
			     				 Mariculture.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: MARICULTURE ZONE' + '</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									//console.log("fishprocessingplants" + feature.properties.Region);
			     			  }
				     		}
				     		if(region_id == feature.properties.Region){
			     			if(feature.properties.resources == "trainingcenter"){
			     				 Trainingcenter.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: TRAINING CENTER' + '</p>'));
									layer.getPopup().on('remove',clickZoomOut);
									//console.log("fishprocessingplants" + feature.properties.Region);
			     			  }
				     		}
				     	   }
				 		})
				 		
		       			
				 });

		  });
		  $.getJSON("/admin/getAllGeoJson",function(data){
			 L.geoJSON(data, {
	     	   		
		     	   pointToLayer: function (feature, latlng) {
		     		  
		     			  if(feature.properties.resources == "fishsanctuaries"){		
		     				  return L.marker(latlng,{icon: sanctuaryicon});
		     			  }
		     			  if(feature.properties.resources == "fishprocessingplants"){		
		     				  return L.marker(latlng,{icon: processingicon});
		     			  }
		     			 if(feature.properties.resources == "fishlanding"){		
		     				  return L.marker(latlng,{icon: landingicon});
		     			  }
		     			if(feature.properties.resources == "FISHPORT"){		
		     				  return L.marker(latlng,{icon: porticon});
		     			  }
		     			if(feature.properties.resources == "fishpen"){		
		     				  return L.marker(latlng,{icon: penicon});
		     			  }
		     			if(feature.properties.resources == "fishcage"){		
		     				  return L.marker(latlng,{icon: cageicon});
		     			  }
		     			if(feature.properties.resources == "coldstorage"){		
		     				  return L.marker(latlng,{icon: ipcsicon});
		     			  }
		     			if(feature.properties.resources == "market"){		
		     				  return L.marker(latlng,{icon: marketicon});
		     			  }
		     			if(feature.properties.resources == "schoolOfFisheries"){		
		     				  return L.marker(latlng,{icon: schoolicon});
		     			  }
		     			if(feature.properties.resources == "fishcoral"){		
		     				  return L.marker(latlng,{icon: fishcoralsicon});
		     			  }
		     			if(feature.properties.resources == "seagrass"){		
		     				  return L.marker(latlng,{icon: seagrassicon});
		     			  }
		     			if(feature.properties.resources == "seaweeds"){		
		     				  return L.marker(latlng,{icon: seagrassicon});
		     			  }
		     			if(feature.properties.resources == "mangrove"){		
		     				  return L.marker(latlng,{icon: mangroveicon});
		     			  }
		     			if(feature.properties.resources == "lgu"){		
		     				  return L.marker(latlng,{icon: lguicon});
		     			  }
		     			if(feature.properties.resources == "mariculturezone"){		
		     				  return L.marker(latlng,{icon: maricultureicon});
		     			  }
		     			if(feature.properties.resources == "trainingcenter"){		
		     				  return L.marker(latlng,{icon: trainingcentericon});
		     			  }
		     		  
		     	   },
		     	   onEachFeature: function (feature, layer) {
		     		   
		     			  if(feature.properties.resources == "fishsanctuaries"){
		     				 FS.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH SANCTUARY' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
		     				  
		     			  }
		     			 if(feature.properties.resources == "fishprocessingplants"){
		     				 FP.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH PROCESSING' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     			if(feature.properties.resources == "fishlanding"){
		     				 FL.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH LANDING' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     			if(feature.properties.resources == "FISHPORT"){
		     				 FPT.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH PORT' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     			if(feature.properties.resources == "fishpen"){
		     				 FPEN.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH PEN' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     			if(feature.properties.resources == "fishcage"){
		     				 FCage.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH CAGE' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     			if(feature.properties.resources == "coldstorage"){
		     				 IPCS.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: COLD STORAGE' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     			if(feature.properties.resources == "market"){
		     				 Market.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: MARKET' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     			if(feature.properties.resources == "schoolOfFisheries"){
		     				 School.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: SCHOOL OF FISHERIES' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     			if(feature.properties.resources == "fishcoral"){
		     				 Fishcorals.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH CORALS' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     			if(feature.properties.resources == "seagrass"){
		     				 Seagrass.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: SEAGRASS' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     			if(feature.properties.resources == "seaweeds"){
		     				 Seaweeds.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: SEAWEEDS' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     			if(feature.properties.resources == "mangrove"){
		     				 Mangrove.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: MANGROVE' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     			if(feature.properties.resources == "lgu"){
		     				 LGU.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: PFO' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     			if(feature.properties.resources == "mariculturezone"){
		     				 Mariculture.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: MARICULTURE ZONE' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     			if(feature.properties.resources == "trainingcenter"){
		     				 Trainingcenter.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: TRAINING CENTER' + '</p>'));
								layer.getPopup().on('remove',clickZoomOut);
								//console.log("fishprocessingplants" + feature.properties.Region);
		     			  }
		     		   
		     	   }
		 		})
		  
		  });
	
	
	
L.control.scale().addTo(map);

function style(feature) {
    return {
        fillColor: 'green', 
        fillOpacity: 0.5,  
        weight: 2,
        opacity: 1,
        color: '#ffffff',
        dashArray: '3'
    };
}

	var highlight = {
		'fillColor': 'yellow',
		'weight': 2,
		'opacity': 1
	};


		
  		function polySelect(a){
  		console.log("polySelect(a): " + a);
			map._layers[a].fire('click'); 
			var layer = map._layers[a];
			map.fitBounds(layer.getBounds());
        }
  		
  		
  		
      
 var fishSancturiesGroup = [];

			$(".se-pre-con").show();
			
			var LamMarkerList = [];
			var group;
			var layerGroup;
	

		    
	        var geojsonMarkerOptions = {
					radius: 8,
					fillColor: "#ff7800",
					color: "#000",
					weight: 1,
					opacity: 1,
					fillOpacity: 0.8
					};
	        
	        
	        
	        
	        var HatcheryComplete = new L.LayerGroup();
	        var HatcheryinComplete = new L.LayerGroup();
	        var iconComplete = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/hatcheries.png"});
	        var iconNComplete = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/mpa.png"});
		       		$.getJSON("/static/geojson/StatusofLegislatedHatcheries.geojson",function(data){
		       		
		       		L.geoJSON(data, {
					pointToLayer: function (feature, latlng) {
						if(feature.properties.Status == "Completed"){
							return L.marker(latlng,{icon: iconComplete});
						}else{
							return L.marker(latlng,{icon: iconNComplete});
						}
    				
    				},
						onEachFeature: function (feature, layer) {
							if(feature.properties.Status == "Completed"){
							HatcheryComplete.addLayer(layer.bindPopup('<p>Location: '+feature.properties.Region +","+feature.properties.Location+'<br>RANo: '+feature.properties.RANo+'<br>Status: '+feature.properties.Status+'<br>Coordinates: '+feature.geometry.coordinates+'</p>').on('click', clickZoom));
							layer.getPopup().on('remove',clickZoomOut);
							}else{
							HatcheryinComplete.addLayer(layer.bindPopup('<p>Location:'+feature.properties.Region +"," +feature.properties.Location+'<br>RANo: '+feature.properties.RANo+'<br>Status: '+feature.properties.Status+ '<br>Coordinates: '+feature.geometry.coordinates+'</p>').on('click', clickZoom));
							layer.getPopup().on('remove',clickZoomOut);	
							}
							}
				})//.addTo(map);
		       		
		       	}); 
		       	

		    		
//		       		$.getJSON("/home/getFPGeoJson",function(data){
//		       			
//		       			L.geoJSON(data, {
//		       				
//							pointToLayer: function (feature, latlng) {	
//								
//									return L.marker(latlng,{icon: fpicon});
//										    				
//		    				},
//								onEachFeature: function (feature, layer) {
//									FP.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+'<br>Resources: Fish Processing' + '</p>'));
//									layer.getPopup().on('remove',clickZoomOut);
//									}
//						})
//		       		});
//		       		

		    		
//		       		$.getJSON("/home/getFLGeoJson",function(data){
//		       			
//		       			L.geoJSON(data, {
//		       				
//							pointToLayer: function (feature, latlng) {	
//								
//									return L.marker(latlng,{icon: flicon});
//										    				
//		    				},
//								onEachFeature: function (feature, layer) {
//									FL.addLayer(layer.bindPopup('<p>' +
//											'Location: '+feature.properties.Location+
//											'<br>Resources: Fish Landing' + 
//											'</p>'));
//									layer.getPopup().on('remove',clickZoomOut);
//									}
//						})
//		       		});
//		       		
		       		
			       	 
			    		
//			       		$.getJSON("/home/getFPTGeoJson",function(data){
//			       			
//			       			L.geoJSON(data, {
//			       				
//								pointToLayer: function (feature, latlng) {	
//									
//										return L.marker(latlng,{icon: fpticon});
//											    				
//			    				},
//									onEachFeature: function (feature, layer) {
//										FPT.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+
//												'<br>Resources: Fish Port' + 
//												'</p>'));
//										layer.getPopup().on('remove',clickZoomOut);
//										}
//							})
//			       		});
//			       		
			       		
			    		
//			       		$.getJSON("/home/getFPenGeoJson",function(data){
//			       			
//			       			L.geoJSON(data, {
//			       				
//								pointToLayer: function (feature, latlng) {	
//									
//										return L.marker(latlng,{icon: fpenicon});
//											    				
//			    				},
//									onEachFeature: function (feature, layer) {
//										FPEN.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
//												'<br>Resources: Fish Pen'+'</p>'));
//										layer.getPopup().on('remove',clickZoomOut);
//										}
//							})
//			       		});
//			       		
			       		
			    		
//			       		$.getJSON("/home/getFCageGeoJson",function(data){
//			       			
//			       			L.geoJSON(data, {
//			       				
//								pointToLayer: function (feature, latlng) {	
//									
//										return L.marker(latlng,{icon: fcageicon});
//											    				
//			    				},
//									onEachFeature: function (feature, layer) {
//										FCage.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
//												'<br>Resources: Fish Cages' + '</p>'));
//										layer.getPopup().on('remove',clickZoomOut);
//										}
//							})
//			       		});
//			       		
			       		
			    		
//			       		$.getJSON("/home/getIPCSGeoJson",function(data){
//			       			
//			       			L.geoJSON(data, {
//			       				
//								pointToLayer: function (feature, latlng) {	
//									
//										return L.marker(latlng,{icon: ipcsicon});
//											    				
//			    				},
//									onEachFeature: function (feature, layer) {
//										IPCS.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+
//												'<br>Name: '+feature.properties.Name+
//												'<br>Resources: Cold Storage' + 
//												'</p>'));
//										layer.getPopup().on('remove',clickZoomOut);
//										}
//							})
//			       		});
//			       		
			       		
			    		
//			       		$.getJSON("/home/getMarketGeoJson",function(data){
//			       			
//			       			L.geoJSON(data, {
//			       				
//								pointToLayer: function (feature, latlng) {	
//									
//										return L.marker(latlng,{icon: marketicon});
//											    				
//			    				},
//									onEachFeature: function (feature, layer) {
//										Market.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+
//												'<br>Resources: Market' + '</p>'));
//										layer.getPopup().on('remove',clickZoomOut);
//										}
//							})
//			       		});
			       		
			       		
			    		
//			       		$.getJSON("/home/getSchoolGeoJson",function(data){
//			       			
//			       			L.geoJSON(data, {
//			       				
//								pointToLayer: function (feature, latlng) {	
//									
//										return L.marker(latlng,{icon: schoolicon});
//											    				
//			    				},
//									onEachFeature: function (feature, layer) {
//										School.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+'<br>Resources: School of Fisheries' +'</p>'));
//										layer.getPopup().on('remove',clickZoomOut);
//										}
//							})
//			       		});
			       		
			    		
//			       		$.getJSON("/home/getFishCoralsGeoJson",function(data){
//			       			
//			       			L.geoJSON(data, {
//			       				
//								pointToLayer: function (feature, latlng) {	
//									
//										return L.marker(latlng,{icon: fishcoralsicon});
//											    				
//			    				},
//									onEachFeature: function (feature, layer) {
//										Fishcorals.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
//												'<br>Resources: Fish Corals' +'</p>'));
//										layer.getPopup().on('remove',clickZoomOut);
//										}
//							})
//			       		});
			       		
			       		
			    		
//			       		$.getJSON("/home/getSeagrassGeoJson",function(data){
//			       			
//			       			L.geoJSON(data, {
//			       				
//								pointToLayer: function (feature, latlng) {	
//									
//										return L.marker(latlng,{icon: seagrassicon});
//											    				
//			    				},
//									onEachFeature: function (feature, layer) {
//										Seagrass.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
//												'<br>Resources: Seagrass' + '</p>'));
//										layer.getPopup().on('remove',clickZoomOut);
//										}
//							})
//			       		});
			       		
			       		
			    		
//			       		$.getJSON("/home/getSeaweedsGeoJson",function(data){
//			       			
//			       			L.geoJSON(data, {
//			       				
//								pointToLayer: function (feature, latlng) {	
//									
//										return L.marker(latlng,{icon: seaweedsicon});
//											    				
//			    				},
//									onEachFeature: function (feature, layer) {
//										Seaweeds.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
//												'<br>Resources: Seaweeds' +'</p>'));
//										layer.getPopup().on('remove',clickZoomOut);
//										}
//							})
//			       		});
			       		
			    		
//			       		$.getJSON("/home/getMangroveGeoJson",function(data){
//			       			
//			       			L.geoJSON(data, {
//			       				
//								pointToLayer: function (feature, latlng) {	
//									
//										return L.marker(latlng,{icon: mangroveicon});
//											    				
//			    				},
//									onEachFeature: function (feature, layer) {
//										Mangrove.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
//												'<br>Resources: Mangrove' +'</p>'));
//										layer.getPopup().on('remove',clickZoomOut);
//										}
//							})
//			       		});
			       		
			    		
//			       		$.getJSON("/home/getLGUGeoJson",function(data){
//			       			
//			       			L.geoJSON(data, {
//			       				
//								pointToLayer: function (feature, latlng) {	
//									
//										return L.marker(latlng,{icon: lguicon});
//											    				
//			    				},
//									onEachFeature: function (feature, layer) {
//										LGU.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
//												'<br>Resources: PFO' + '</p>'));
//										layer.getPopup().on('remove',clickZoomOut);
//										}
//							})
//			       		});
			       		
			       		
			    		
//			       		$.getJSON("/home/getMaricultureGeoJson",function(data){
//			       			
//			       			L.geoJSON(data, {
//			       				
//								pointToLayer: function (feature, latlng) {	
//									
//										return L.marker(latlng,{icon: maricultureicon});
//											    				
//			    				},
//									onEachFeature: function (feature, layer) {
//										Mariculture.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
//												'<br>Resources: Mariculture Zone' + '</p>'));
//										layer.getPopup().on('remove',clickZoomOut);
//										}
//							})
//			       		});
//			       		
			       		
			    		
//			       		$.getJSON("/home/getTrainingcenterGeoJson",function(data){
//			       			
//			       			L.geoJSON(data, {
//			       				
//								pointToLayer: function (feature, latlng) {	
//									
//										return L.marker(latlng,{icon: trainingcentericon});
//											    				
//			    				},
//									onEachFeature: function (feature, layer) {
//										Trainingcenter.addLayer(layer.bindPopup('<p>'+'Location: '+feature.properties.Location+
//												'<br>Resources: Training Center' + '</p>'));
//										layer.getPopup().on('remove',clickZoomOut);
//										}
//							})
//			       		});
			       		
        
var baseMaps = {
    /* "Open Street Map": osm,
   	"OSM B&W":OpenStreetMap_BlackAndWhite,
   	"Esri WorldI magery":Esri_WorldImagery,
   	"Esri World Gray Canvas":Esri_WorldGrayCanvas, */
		
    "<img src='/static/images/iconCircle/hatcheries.png' style='width: 25px; height: 25px;'/>  Hatchery Complete":HatcheryComplete,
    "<img src='/static/images/iconCircle/hatcheries.png' style='width: 25px; height: 25px;'/> Hatchery Incomplete ":HatcheryinComplete,
    "<img src='/static/images/iconCircle/fishsanctuary.png' style='width: 25px; height: 25px;'/> Fish Sanctuary":FS,
    "<img src='/static/images/iconCircle/fishprocessing.png' style='width: 25px; height: 25px;'/> Fish Processing":FP,
    "<img src='/static/images/iconCircle/fish-landing.png' style='width: 25px; height: 25px;'/> Fish Landing":FL,
    "<img src='/static/images/iconCircle/fishport.png' style='width: 25px; height: 25px;'/> Fish Port":FPT,
    "<img src='/static/images/iconCircle/fishpen.png' style='width: 25px; height: 25px;'/> Fish Pen":FPEN,
    "<img src='/static/images/iconCircle/fishcage.png' style='width: 25px; height: 25px;'/> Fish Cage":FCage,
    "<img src='/static/images/iconCircle/iceplant.png' style='width: 25px; height: 25px;'/> Cold Storage":IPCS,
    "<img src='/static/images/iconCircle/market.png' style='width: 25px; height: 25px;'/> Market":Market,
    "<img src='/static/images/iconCircle/schoolof-fisheries.png' style='width: 25px; height: 25px;'/> School of Fisheries":School,
    "<img src='/static/images/iconCircle/fishcorral.png' style='width: 25px; height: 25px;'/> Fish Corals":Fishcorals,
    "<img src='/static/images/iconCircle/seagrass.png' style='width: 25px; height: 25px;'/> Sea Grass":Seagrass,
    "<img src='/static/images/iconCircle/seaweeds.png' style='width: 25px; height: 25px;'/> Sea Weeds":Seaweeds,
    "<img src='/static/images/iconCircle/mangrove.png' style='width: 25px; height: 25px;'/> Mangrove":Mangrove,
    "<img src='/static/images/iconCircle/lgu.png' style='width: 25px; height: 25px;'/> LGU":LGU,
    "<img src='/static/images/iconCircle/mariculturezone.png' style='width: 25px; height: 25px;'/> Mariculture":Mariculture,
    "<img src='/static/images/iconCircle/fisheriestraining.png' style='width: 25px; height: 25px;'/> Trainingcenter":Trainingcenter

};

 var overlayMaps = {
   "Hatchery Complete":HatcheryComplete,
   "Hatchery Incomplete ":HatcheryinComplete
};

L.control.layers(null,baseMaps,{collapsed:false}).addTo(map);
//L.control.layers(baseMaps).addTo(map);	
		var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});
//
//		var overLayers = [
//			{
//				name: "Fish Sanctuary",
//				icon: "<img src='/static/images/iconCircle/fishsanctuary.png' style='width: 25px; height: 25px;'/>",
//				layer: missedShots
//			},
//			{
//				name: "Fish Processing Plant",
//				icon: "<img src='/static/images/iconCircle/fishprocessing.png' style='width: 25px; height: 25px;'/>",
//				layer: missedShots
//			}
//		];
//
//		
//		
//		new L.Control.PanelLayers(overLayers, {
//			collapsibleGroups: true,
//			collapsed: true
//		}).addTo(map);
//
//		

//		L.easyPrint({
//			title: 'My awesome print button',
//			position: 'bottomright',
//			sizeModes: ['A4Portrait', 'A4Landscape'],
//		    hideClasses: ['leaflet-control-zoom']
//		}).addTo(map);
		/*  function getFisheriesData(){
			getAllResources(map_location);
		} */
 