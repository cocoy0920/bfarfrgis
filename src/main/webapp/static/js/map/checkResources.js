$.getJSON("/home/getAllGeoJsonHome/fishpond",function(data){L.geoJSON(data,{
									 	pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: pondicon}).on('click', clickOnMarker);},
							     	   onEachFeature: function (feature, layer) {FPond.addLayer(layer.bindPopup(feature.properties.Information));}})});

$.getJSON("/home/getAllGeoJsonHome/fishpen",function(data){ L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: penicon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {FPEN.addLayer(layer.bindPopup(feature.properties.Information));}})});	

$.getJSON("/home/getAllGeoJsonHome/fishcage",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: cageicon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {FCage.addLayer(layer.bindPopup(feature.properties.Information));}})});	

$.getJSON("/home/getAllGeoJsonHome/fishcoral",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: fishcoralsicon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {Fishcorals.addLayer(layer.bindPopup(feature.properties.Information));}})});						     	   					     	   

$.getJSON("/home/getAllGeoJsonHome/seaweeds/SEAWEEDNURSERY",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: seaweedsNurseryicon}).on('click', clickOnMarker);},
						     	 onEachFeature: function (feature, layer) {if(feature.properties.Type === "SEAWEEDNURSERY"){Seaweed_nursery.addLayer(layer.bindPopup(feature.properties.Information));}}})});	

$.getJSON("/home/getAllGeoJsonHome/seaweeds/SEAWEEDLABORATORY",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: seaweedsLaboratoryicon}).on('click', clickOnMarker); },
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Type === "SEAWEEDLABORATORY"){Seaweed_laboratory.addLayer(layer.bindPopup(feature.properties.Information));	}}})});

$.getJSON("/home/getAllGeoJsonHome/mariculture/BFAR",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: maricultureicon}).on('click', clickOnMarker);},
						     	 onEachFeature: function (feature, layer) {if(feature.properties.Type === "BFAR"){Mariculture_BFAR.addLayer(layer.bindPopup(feature.properties.Information));}}})});

 $.getJSON("/home/getAllGeoJsonHome/mariculture/LGU",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: maricultureLGUicon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Type === "LGU"){Mariculture_LGU.addLayer(layer.bindPopup(feature.properties.Information));}}})});
				
$.getJSON("/home/getAllGeoJsonHome/mariculture/PRIVATE",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: mariculturePrivateicon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Type === "PRIVATE"){Mariculture_PRIVATE.addLayer(layer.bindPopup(feature.properties.Information));}}})});

 $.getJSON("/home/getAllGeoJsonHome/hatchery/legislated",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: iconComplete}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Legislative === "legislated"){Hatchery.addLayer(layer.bindPopup(feature.properties.Information));}}})});
				  
$.getJSON("/home/getAllGeoJsonHome/hatchery/nonLegislated",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: iconNComplete}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Legislative === "nonLegislated"){HatcheryinComplete.addLayer(layer.bindPopup(feature.properties.Information));}}})});
				  
  $.getJSON("/home/getAllGeoJsonHome/hatchery/LGU",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: hatchLGU}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Operator === "LGU"){Hatchery_LGU.addLayer(layer.bindPopup(feature.properties.Information));}}})});

 $.getJSON("/home/getAllGeoJsonHome/hatchery/PRIVATE",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: hatchPrivate}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Operator === "PRIVATE"){Hatchery_PRIVATE.addLayer(layer.bindPopup(feature.properties.Information));}}})});
 
 $.getJSON("/home/getAllGeoJsonHome/lambaklad",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: corals}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {LAMBAKLAD.addLayer(layer.bindPopup(feature.properties.Information));}})});
	
$.getJSON("/home/getAllGeoJsonHome/fishlanding/Operational",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: FLOperationalIcon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.CFLCSTATUS === "Operational"){FL_operational.addLayer(layer.bindPopup(feature.properties.Information));}}})});

$.getJSON("/home/getAllGeoJsonHome/fishlanding/ForOperation",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: FLForOperationalIcon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.CFLCSTATUS === "ForOperation"){FL_for_operation.addLayer(layer.bindPopup(feature.properties.Information));}}})});
				   				   				  				  				  
$.getJSON("/home/getAllGeoJsonHome/fishlanding/ForCompletion",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: FLForCompletionIcon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.CFLCSTATUS === "ForCompletion"){FL_for_completion.addLayer(layer.bindPopup(feature.properties.Information));	}}})});
				 
$.getJSON("/home/getAllGeoJsonHome/fishlanding/ForTransfer",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: FLForTransferIcon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.CFLCSTATUS === "ForTransfer"){FL_for_transfer.addLayer(layer.bindPopup(feature.properties.Information));	}}})});
				  
 $.getJSON("/home/getAllGeoJsonHome/fishlanding/Damaged",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: FLDamagedIcon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.CFLCSTATUS === "Damaged"){FL_damaged.addLayer(layer.bindPopup(feature.properties.Information));}}})});
							  
$.getJSON("/home/getAllGeoJsonHome/fishlanding/Traditional",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: FLTraditionalIcon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.CFLCSTATUS === "Traditional"){FL_traditional.addLayer(layer.bindPopup(feature.properties.Information));}}})});
							 
$.getJSON("/home/getAllGeoJsonHome/fishlanding/NonTraditional",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: FLNontraditionalIcon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.CFLCSTATUS === "NonTraditional"){FL_noncflc.addLayer(layer.bindPopup(feature.properties.Information));}}})});
							  
 $.getJSON("/home/getAllGeoJsonHome/cold",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: ipcsbfaricon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Operator === "BFAR"){IPCS_PFDA.addLayer(layer.bindPopup(feature.properties.Information));}}})});
					 		   
$.getJSON("/home/getAllGeoJsonHome/cold",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: ipcsprivateicon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Operator === "PRIVATE"){IPCS_PRIVATEORGANIZATION.addLayer(layer.bindPopup(feature.properties.Information));	}}})});

$.getJSON("/home/getAllGeoJsonHome/cold",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: ipcscoopicon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Operator === "COOPERATIVE"){IPCS_COOPERATIVE.addLayer(layer.bindPopup(feature.properties.Information));	}}})});
					 		 					 		 

$.getJSON("/home/getAllGeoJsonHome/fishprocessing",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){if(feature.properties.Operator === "BFAR_MANAGED"){return L.marker(latlng,{icon: FP_BFAR_ICON}).on('click', clickOnMarker);}},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Operator === "BFAR_MANAGED"){FP_BFAR.addLayer(layer.bindPopup(feature.properties.Information));	}}})});

 $.getJSON("/home/getAllGeoJsonHome/fishprocessing",function(data){ L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){if(feature.properties.Operator === "LGU_MANAGED"){return L.marker(latlng,{icon: FP_LGU_ICON}).on('click', clickOnMarker);}},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Operator === "LGU_MANAGED"){FP_LGU.addLayer(layer.bindPopup(feature.properties.Information));}}})});
					 		 
 $.getJSON("/home/getAllGeoJsonHome/fishprocessing",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){if(feature.properties.Operator === "PRIVATE_SECTOR"){return L.marker(latlng,{icon: FP_PRIVATE_ICON}).on('click', clickOnMarker);}},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Operator === "PRIVATE_SECTOR"){FP_PRIVATE.addLayer(layer.bindPopup(feature.properties.Information));}}})});

$.getJSON("/home/getAllGeoJsonHome/fishprocessing",function(data){L.geoJSON(data, {						
								 pointToLayer: function (feature, latlng){ if(feature.properties.Operator === "COOPERATIVE"){return L.marker(latlng,{icon: FP_COOPERATIVE_ICON}).on('click', clickOnMarker);}},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Operator === "COOPERATIVE"){FP_COOPERATIVE.addLayer(layer.bindPopup(feature.properties.Information));}}})});

 $.getJSON("/home/getAllGeoJsonHome/seaweeds",function(data){L.geoJSON(data, {
									pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: seaweedsWarehouseicon});},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Operator === "SEAWEEDWAREHOUSE"){Seaweed_warehouse.addLayer(layer.bindPopup(feature.properties.Information));}}})});

$.getJSON("/home/getAllGeoJsonHome/seaweeds",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: seaweedsDryericon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {if(feature.properties.Operator === "SEAWEEDDRYER"){Seaweed_dryer.addLayer(layer.bindPopup(feature.properties.Information));}}})});

 $.getJSON("/home/getAllGeoJsonHome/market",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: marketicon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {Market.addLayer(layer.bindPopup(feature.properties.Information));}})});

 $.getJSON("/home/getAllGeoJsonHome/fishport",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: porticon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {FPT.addLayer(layer.bindPopup(feature.properties.Information));}})});

 $.getJSON("/home/getAllGeoJsonHome/seagrass",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: seagrassicon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {Seagrass.addLayer(layer.bindPopup(feature.properties.Information));	}})});

 $.getJSON("/home/getAllGeoJsonHome/mangrove",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: mangroveicon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {Mangrove.addLayer(layer.bindPopup(feature.properties.Information));}})});
 $.getJSON("/home/getAllGeoJsonHome/coralreefs",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: coralreefsicon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {CORALREEFS.addLayer(layer.bindPopup(feature.properties.Information));}})});
 
 $.getJSON("/home/getAllGeoJsonHome/nationalcenter",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng, {icon:ncentericon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {NCENTER.addLayer(layer.bindPopup(feature.properties.Information));}})});

 $.getJSON("/home/getAllGeoJsonHome/regionaloffices",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: rofficeicon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {ROFFICE.addLayer(layer.bindPopup(feature.properties.Information));}})});
					 		 					 		  					 		  					 		   					 		  					 		  					 		  					 		  					 		 					 		  					 		 
 $.getJSON("/home/getAllGeoJsonHome/trainingcenter",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: trainingcentericon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {Trainingcenter.addLayer(layer.bindPopup(feature.properties.Information));}})});
					 		  
 $.getJSON("/home/getAllGeoJsonHome/lgu",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){ return L.marker(latlng,{icon: lguicon}).on('click', clickOnMarker);},
 								onEachFeature: function (feature, layer) {LGU.addLayer(layer.bindPopup(feature.properties.Information));}})});

 $.getJSON("/home/getAllGeoJsonHome/tos",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: toscon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {TOS.addLayer(layer.bindPopup(feature.properties.Information));}})});

 $.getJSON("/home/getAllGeoJsonHome/schoolOfFisheries",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: schoolicon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {School.addLayer(layer.bindPopup(feature.properties.Information));}})});
					 		  					 		  					 		  
$.getJSON("/home/getAllGeoJsonHome/fishsanctuary",function(data){L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){return L.marker(latlng,{icon: sanctuaryicon}).on('click', clickOnMarker);},
						     	   onEachFeature: function (feature, layer) {FS.addLayer(layer.bindPopup(feature.properties.Information));}})});
					 		  
function getAllCheckByResources(){


	check_aquafarm = $('#aquafarm');
	// identify the other checkboxes in the group
	aquafarm_class = $('.aquafarm');
	check_aquafarm.on('click',function(){
		aquafarm_class.click();
	});

	check_aquaseaweed = $('#aquaseaweed');
	// identify the other checkboxes in the group
	aquaseaweed_class = $('.aquaseaweed');
	check_aquaseaweed.on('click',function(){
		aquaseaweed_class.click();
	});
	
	check_aquamariculture = $('#aquamariculture');
	// identify the other checkboxes in the group
	aquamariculture_class = $('.aquamariculture');
	check_aquamariculture.on('click',function(){
		aquamariculture_class.click();
	});

	check_aquahatchery = $('#aquahatchery');
	// identify the other checkboxes in the group
	aquahatchery_class = $('.aquahatchery');
	check_aquahatchery.on('click',function(){
		aquahatchery_class.click();
	});
	
	check_capture = $('#capture');
	// identify the other checkboxes in the group
	capture_class = $('.capture');
	check_capture.on('click',function(){
		capture_class.click();
	});
	
	check_fishlanding = $('#fishlanding');
	// identify the other checkboxes in the group
	fishlanding_class = $('.fishlanding');
	check_fishlanding.on('click',function(){
		fishlanding_class.click();
	});
	
	check_coldstorage = $('#coldstorage');
	// identify the other checkboxes in the group
	coldstorage_class = $('.coldstorage');
	check_coldstorage.on('click',function(){
		coldstorage_class.click();
	});
	
	check_processing = $('#processing');
	// identify the other checkboxes in the group
	processing_class = $('.processing');
	check_processing.on('click',function(){
		processing_class.click();
	});
	
	check_postseaweed = $('#postseaweed');
	// identify the other checkboxes in the group
	postseaweed_class = $('.postseaweed');
	check_postseaweed.on('click',function(){
		postseaweed_class.click();
	});
	
	check_habitat = $('#habitat');
	// identify the other checkboxes in the group
	habitat_class = $('.habitat');
	check_habitat.on('click',function(){
		habitat_class.click();
	});
	
	check_infrastructure = $('#infrastructure');
	// identify the other checkboxes in the group
	infrastructure_class = $('.infrastructure');
	check_infrastructure.on('click',function(){
		infrastructure_class.click();
	});

	///AQUAFARM	

	$("#checked_pond").change(function(){
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  if($pond.is(":checked")){$pond.attr("disabled", true); map.spin(true);
			   setTimeout(function () {FPond.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
		}else{map.removeLayer(FPond);if (clicked) {pMarker.remove();}}

	});	
	
	$("#checked_fishpen").change(function(){	
		
		  var $pen = $( this );
		  var pen = $pen.attr( "value" );
		  
			if($pen.is(":checked")){$pen.attr("disabled", true);map.spin(true);
					setTimeout(function (){FPEN.addTo(map); map.spin(false);$pen.removeAttr("disabled");}, 3000);				
			}else{map.removeLayer(FPEN);if (clicked) {pMarker.remove();}}		  
	});	

	$("#checked_fishcage").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				  setTimeout(function (){FCage.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(FCage);if (clicked) {pMarker.remove();}}
		  
	});	
	$("#checked_fishcoral").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				  setTimeout(function () { Fishcorals.addTo(map); map.spin(false);$pond.attr("disabled", false);}, 3000); 
			}else{map.removeLayer(Fishcorals);if (clicked) {pMarker.remove();}}
	});	
	
	////SEAWEED AQUACULTURE
	$("#checked_seaweednursery").change(function(){

		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				   setTimeout(function () {Seaweed_nursery.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);	 
			}else{map.removeLayer(Seaweed_nursery);if (clicked) {pMarker.remove();}}
		  
	});			
			
	$("#checked_seaweedlaboratory").change(function(){
		  var $seaweeds = $( this );
		  var seaweeds = $seaweeds.attr( "value" );
			
			if($seaweeds.is(":checked")){$seaweeds.attr("disabled", true); map.spin(true);
				  setTimeout(function () {Seaweed_laboratory.addTo(map); map.spin(false);$seaweeds.attr("disabled", false);}, 3000);
			}else{map.removeLayer(Seaweed_laboratory);if (clicked) {pMarker.remove();}}
		  
	});
		
///MARICULTURE PARK
	$("#checked_mariculturebfar").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				  setTimeout(function () {Mariculture_BFAR.addTo(map);map.spin(false); $pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(Mariculture_BFAR);if (clicked) {pMarker.remove();}}
		  
	});	
		
	$("#checked_mariculturelgu").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				  setTimeout(function () {Mariculture_LGU.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(Mariculture_LGU);if (clicked) {pMarker.remove();}}
		  
	});	
		
	$("#checked_maricultureprivate").change(function(){

		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				  setTimeout(function () {Mariculture_PRIVATE.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(Mariculture_PRIVATE);if (clicked) {pMarker.remove();}}		  
	});			
///HATCHERY
	$("#checked_legislated").change(function(){

		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				  setTimeout(function () {Hatchery.addTo(map); map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(Hatchery);if (clicked) {pMarker.remove();}}
		  
	});		
	
	$("#checked_nonlegislated").change(function(){

		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				   setTimeout(function () {HatcheryinComplete.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(HatcheryinComplete);if (clicked) {pMarker.remove();}}		  
	});	

	$("#checked_hatcherylgu").change(function(){

		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true); 
				setTimeout(function () {Hatchery_LGU.addTo(map); map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(Hatchery_LGU);if (clicked) {pMarker.remove();}}
		  
	});
	
	$("#checked_hatcheryprivate").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				  setTimeout(function () {Hatchery_PRIVATE.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(Hatchery_PRIVATE);if (clicked) {pMarker.remove();}}
		  
	});	
	
	///CAPTURE FISHERIES
	
	$("#checked_payao").change(function(){

		  var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
			if($checked_lakes.is(":checked")){
			//	getPAYAO();
				PAYAO.addTo(map);

		}else{
			map.removeLayer(PAYAO);

		}
		  
		});
	$("#checked_lambaklad").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				  setTimeout(function (){LAMBAKLAD.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(LAMBAKLAD);if (clicked) {pMarker.remove();}}
		  
	});	
	
	$("#checked_frpboats").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){
			//	getFMA1();

		}else{
		//	map.removeLayer(FMAJSON1);
		
		}
		  
	});	

	$("#check_cflc_operational").change(function(){

		  var $operational = $( this );
		  var operational = $operational.attr( "value" );
		  
			if($operational.is(":checked")){$operational.attr("disabled", true);map.spin(true);
				  setTimeout(function () {FL_operational.addTo(map);map.spin(false);$operational.attr("disabled", false);}, 3000);	 
			}else{map.removeLayer(FL_operational);if (clicked) {pMarker.remove();}}
		  
	});
	
	$("#check_cflc_for_operation").change(function(){
	
		  var $for_operation = $( this );
		  var for_operation = $for_operation.attr( "value" );
		  
			if($for_operation.is(":checked")){$for_operation.attr("disabled", true);map.spin(true);
				setTimeout(function () {FL_for_operation.addTo(map);map.spin(false);$for_operation.attr("disabled", false);}, 3000);
			}else{map.removeLayer(FL_for_operation);if (clicked) {pMarker.remove();}}
		  
	});		
	$("#check_cflc_for_complition").change(function(){
	
		  var $for_complition = $( this );
		  var for_complition = $for_complition.attr( "value" );
		  
			if($for_complition.is(":checked")){$for_complition.attr("disabled", true);map.spin(true);
				setTimeout(function () {FL_for_completion.addTo(map);map.spin(false);$for_complition.attr("disabled", false);}, 3000);
			}else{map.removeLayer(FL_for_completion);if (clicked) {pMarker.remove();}}
		  
	});	
	
	$("#check_cflc_for_transfer").change(function(){
	
		  var $for_transfer = $( this );
		  var for_transfer = $for_transfer.attr( "value" );
		  
			if($for_transfer.is(":checked")){$for_transfer.attr("disabled", true);map.spin(true);
				setTimeout(function () {FL_for_transfer.addTo(map);map.spin(false);$for_transfer.attr("disabled", false);}, 3000);
			}else{map.removeLayer(FL_for_transfer);if (clicked) {pMarker.remove();}}
		  
	});	
	
	$("#check_cflc_damaged").change(function(){
	
		  var $damaged = $( this );
		  var damaged = $damaged.attr( "value" );
		  
			if($damaged.is(":checked")){$damaged.attr("disabled", true);map.spin(true);				 
					  setTimeout(function (){FL_damaged.addTo(map); map.spin(false);$damaged.attr("disabled", false);}, 3000);			
			}else{map.removeLayer(FL_damaged);if (clicked) {pMarker.remove();}}
		  
	});	
	
	$("#check_traditional").change(function(){
	
		  var $traditional = $( this );
		  var traditional = $traditional.attr( "value" );
		  
			if($traditional.is(":checked")){$traditional.attr("disabled", true);map.spin(true);				  
					  setTimeout(function () {FL_traditional.addTo(map);map.spin(false);$traditional.attr("disabled", false);}, 3000);
			}else{map.removeLayer(FL_traditional);if (clicked) {pMarker.remove();}}
		  
	});	
	
	
	$("#check_noncflc").change(function(){
		
		  var $noncflc = $( this );
		  var noncflc = $noncflc.attr( "value" );
		  
			if($noncflc.is(":checked")){$noncflc.attr("disabled", true);map.spin(true);
				  setTimeout(function () {FL_noncflc.addTo(map);map.spin(false);$noncflc.attr("disabled", false);}, 3000);
			}else{map.removeLayer(FL_noncflc);if (clicked) {pMarker.remove();}}		  
	});		
	
	///COLD STORAGE/IPCS
	$("#check_coldpfda").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
			 setTimeout(function () {IPCS_PFDA.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(IPCS_PFDA);if (clicked) {pMarker.remove();}}
		  
	});	
	
	$("#check_coldprivate").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
			 setTimeout(function () {IPCS_PRIVATEORGANIZATION.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);				
			}else{map.removeLayer(IPCS_PRIVATEORGANIZATION);if (clicked) {pMarker.remove();}}		  
	});	
	
	$("#check_coldcooperative").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
			 setTimeout(function () {IPCS_COOPERATIVE.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(IPCS_COOPERATIVE);if (clicked) {pMarker.remove();}}
		  
	});		
	
	/// FISH PROCESSING FACILITY
	
	$("#check_processingbfar").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true); map.spin(true);
				setTimeout(function () {FP_BFAR.addTo(map); map.spin(false); $pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(FP_BFAR);if (clicked) {pMarker.remove();}}
		  
	});
	
	$("#check_processinglgu").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
			 setTimeout(function () {FP_LGU.addTo(map);map.spin(false);$pond.attr("disabled", false);},3000);
			}else{map.removeLayer(FP_LGU);if (clicked) {pMarker.remove();}}
		  
	});	
	
	$("#check_processingprivate").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				setTimeout(function () {FP_PRIVATE.addTo(map); map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(FP_PRIVATE);if (clicked) {pMarker.remove();}}		  
	});
	
	$("#check_processingcooperative").change(function(){
		
		  var $FCOO = $( this );
		  var FCOO = $FCOO.attr( "value" );
		  
			if($FCOO.is(":checked")){$FCOO.attr("disabled", true);map.spin(true);
			 	setTimeout(function () {FP_COOPERATIVE.addTo(map);map.spin(false);$FCOO.attr("disabled", false);}, 3000);
		}else{map.removeLayer(FP_COOPERATIVE);if (clicked) {pMarker.remove();}}
		  
	});
	
	///POST HARVEST SEAWEED
	$("#check_warehouse").change(function(){

		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
			 	setTimeout(function () {Seaweed_warehouse.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(Seaweed_warehouse);if (clicked) {pMarker.remove();}}
		  
	});		

	$("#check_dryer").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				setTimeout(function () {Seaweed_dryer.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(Seaweed_dryer);if (clicked) {pMarker.remove();}}
		  
	});	
	$("#checked_market").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
			setTimeout(function () {Market.addTo(map);  map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(Market);if (clicked) {pMarker.remove();}}
		  
	});	
	
	$("#checked_fishport").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
			setTimeout(function () {FPT.addTo(map); map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(FPT);if (clicked) {pMarker.remove();}}
		  
	});	
	
	$("#checked_kadiwa").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){
			//	getFMA1();

		}else{
		//	map.removeLayer(FMAJSON1);
		
		}
		  
	});		

	
	//FISH HABITAT
	$("#checked_seagrass").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
			setTimeout(function () {Seagrass.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(Seagrass);if (clicked) {pMarker.remove();}}
		  
	});	
	
	$("#checked_mangrove").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
			 setTimeout(function () {Mangrove.addTo(map);  map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(Mangrove);if (clicked) {pMarker.remove();}}
		  
	});	
	
	$("#checked_coralreefs").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($ponds.is(":checked")){$pond.attr("disabled", true);map.spin(true);
			 setTimeout(function () {CORALREEFS.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(CORALREEFS);if (clicked) {pMarker.remove();}}
		  
	});	
	
	///BFAR INFRASTRUCTURE

	$("#check_nationalcenter").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				setTimeout(function () {NCENTER.addTo(map);  map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(NCENTER);if (clicked) {pMarker.remove();}}
		  
	});	
	
	$("#check_regionaloffices").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
			setTimeout(function () {ROFFICE.addTo(map); map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(ROFFICE);if (clicked) {pMarker.remove();}}
		  
	});	
	
	$("#check_rdtc").change(function(){

		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
			 setTimeout(function () {Trainingcenter.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(Trainingcenter);if (clicked) {pMarker.remove();}}
		  
	});	
	
	$("#checked_lgu").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				setTimeout(function () {LGU.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(LGU);if (clicked) {pMarker.remove();}}
		  
	});		

	$("#check_tos").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				setTimeout(function () {TOS.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(TOS);if (clicked) {pMarker.remove();}}
		  
	});			

	$("#checked_schoolOfFisheries").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
			 setTimeout(function () {School.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(School);if (clicked) {pMarker.remove();}}
		  
	});	
	
	$("#checked_fishsanctuaries").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){$pond.attr("disabled", true);map.spin(true);
				setTimeout(function () {FS.addTo(map);map.spin(false);$pond.attr("disabled", false);}, 3000);
			}else{map.removeLayer(FS);if (clicked) {pMarker.remove();}}
		  
	});	
	

	

	$("#checked_10000").change(function(){
			
			  var $depth = $( this );
			  var depth = $depth.attr( "value" );
			  
				if($depth.is(":checked")){
					fetch("/static/FMA/Bathymetry.geojson").then(res => res.json()).then(data => {
					  	L.geoJson(data,{
					  					       				
					  	onEachFeature: function (feature, layer) {
					  		/*if(layer.feature.properties.DEPTH == '0') {    
					  			depth_1000.addLayer(layer.setStyle(setStyleColor('#a9edff')));
					  			
					  		  }
					  		
					  		if(layer.feature.properties.DEPTH == '0') {  
					  			console.log(layer.feature.properties.DEPTH == '0');
					  			depth_2000.addLayer(layer.setStyle(setStyleColor('#00a9d4')));
					  			
					  		  }
					  		if(layer.feature.properties.DEPTH == '2000') {    
					  			depth_3000.addLayer(layer.setStyle(setStyleColor('#0082a3')));
					  			
					  		  }
					  		if(layer.feature.properties.DEPTH == '3000') {    
					  			depth_5000.addLayer(layer.setStyle(setStyleColor('#005b72')));
					  			
					  		  }*/
					  		if(layer.feature.properties.DEPTH == '7000') {    
					  			depth_10000.addLayer(layer.setStyle(setStyleColor('#003441')));
					  			
					  		  }

					  	}, 			     	   	       				
					  	});
					  	depth_10000.addTo(map);
					});

			}else{
				map.removeLayer(depth_10000);
			
			}
			  
		});
		
		$("#checked_5000").change(function(){
			
			  var $depth = $( this );
			  var depth = $depth.attr( "value" );
			  
				if($depth.is(":checked")){
					fetch("/static/FMA/Bathymetry.geojson").then(res => res.json()).then(data => {
					  	L.geoJson(data,{
					  					       				
					  	onEachFeature: function (feature, layer) {
					  	
					  		if(layer.feature.properties.DEPTH == '3000') {    
					  			depth_5000.addLayer(layer.setStyle(setStyleColor('#005b72')));
					  			
					  		  }


					  	}, 			     	   	       				
					  	});
					  	depth_5000.addTo(map);
					});
					

			}else{
				map.removeLayer(depth_5000);
			
			}
			  
		});
		$("#checked_3000").change(function(){
			console.log("#checked_300");
			  var $depth = $( this );
			  var depth = $depth.attr( "value" );
			  
				if($depth.is(":checked")){
					fetch("/static/FMA/Bathymetry.geojson").then(res => res.json()).then(data => {
					  	L.geoJson(data,{
					  					       				
					  	onEachFeature: function (feature, layer) {
					  		
					  		if(layer.feature.properties.DEPTH == '2000') {    
					  			depth_3000.addLayer(layer.setStyle(setStyleColor('#0082a3')));
					  			
					  		  }

					  	}, 			     	   	       				
					  	});
					  	depth_3000.addTo(map);
					});
					

			}else{
				map.removeLayer(depth_3000);
			
			}
			  
		});
		$("#checked_2000").change(function(){
			console.log("#checked_200");
			  var $depth = $( this );
			  var depth = $depth.attr( "value" );
			  
				if($depth.is(":checked")){
					fetch("/static/FMA/Bathymetry.geojson").then(res => res.json()).then(data => {
					  	L.geoJson(data,{
					  					       				
					  	onEachFeature: function (feature, layer) {
					  		
					  		if(layer.feature.properties.DEPTH == '0') {  
					  			console.log(layer.feature.properties.DEPTH == '0');
					  			depth_2000.addLayer(layer.setStyle(setStyleColor('#00a9d4')));
					  			
					  		  }

					  	}, 			     	   	       				
					  	});
					  	depth_2000.addTo(map);
					});
					
					

			}else{
				map.removeLayer(depth_2000);
			
			}
			  
		});
		$("#checked_1000").change(function(){
			console.log("#checked_1000");
			  var $depth = $( this );
			  var depth = $depth.attr( "value" );
			  
				if($depth.is(":checked")){
					fetch("/static/FMA/Bathymetry.geojson").then(res => res.json()).then(data => {
					  	L.geoJson(data,{
					  					       				
					  	onEachFeature: function (feature, layer) {
					  		if(layer.feature.properties.DEPTH == '0') {    
					  			depth_1000.addLayer(layer.setStyle(setStyleColor('#a9edff')));
					  			
					  		  }

					  	}, 			     	   	       				
					  	});
					  	depth_1000.addTo(map);
					});
					
					

			}else{
				map.removeLayer(depth_1000);
			
			}
			  
		});
		
/*	    new L.Control.BootstrapModal({
	        modalId: 'modal_about',
	        tooltip: "Fisheries Management Areas",
	        glyph: 'info-sign',
	        info:'FMA'
	    }).addTo(map);
	    

		*/	

	function init() {
        init.called = true;
    }
    
/* mapClick function */

    function mapClick () {
	    if(init.called) {
	        init.called = false;
	    }
	    else{
	      $('.modal').modal('hide');
	    }
    }

	function markerOnClick(e) {
		 console.log("markerOnClick");
		  $(".modal-content").html('This is marker ');
		  $('#emptymodal').modal('show');
		  $('#modal_help').modal('show');
		  map.setView(e.target.getLatLng());
		  init();
		}
	// map.on('click', mapClick);
	
/*		var clicked;

var clickStyle =  {
  radius: 10,
  fillColor: "#ff0000",
  color: "#ff0000",
  opacity: 1,
  weight: 2,
  fillOpacity: 1,
}
var unclickStyle = {
  radius: 10,
  fillColor: "#09f9df",
  color: "#ff0000",
  weight: 1,
  opacity: 1,
  fillOpacity: 1,
}

var coords;
var pMarker;
function clickOnMarker(e){
	coords = e.latlng;
  if (clicked) {
	  pMarker.remove();
	//  pMarker = new L.circleMarker(coords,unclickStyle);
   // clicked.setStyle(unclickStyle);
  }
  pMarker = new L.circleMarker(coords,clickStyle);
  //e.target.setStyle(clickStyle);
  clicked = e.target;
  pMarker.addTo(map);		      
			      
}

map.on('click', function(e) { 
    
	if (prevLayerClicked !== null) {          
        prevLayerClicked.setStyle(philStyle);
      }
    if(coords){
		  // console.log(coords);
	   }  
	   if (clicked) {
	  	pMarker.remove();
  		}
});*/
}
			  
			  
			  
			  
			  
			  
			  
//			  var markers = L.markerClusterGroup({
//				  spiderfyOnMaxZoom: true,
//				  showCoverageOnHover: false,
//				  zoomToBoundsOnClick: true
//				});
/*			  var markers = L.markerClusterGroup();
			  $.getJSON(pUrl, function(data) {
				  setTimeout(function () {	  
				  var geoJsonLayer = L.geoJson(data, {
				    filter: function(feature, layer) {
				      return true;
				      return (feature.properties.Region);
				    },
				    pointToLayer: function(feature, latlng) {
				    
				          label = feature.properties.Information;
				     
				      var pMarker = new L.Marker(latlng, {
				        title: feature.properties.Name
				      });*/
//				      pMarker = new L.CircleMarker(latlng, {
//				        title: feature.properties.Name,
//				        radius: 5,
//				        color: '#FFFFFF',
//				        weight: 2,
//				        fillOpacity: 0.5,
//				        fillColor: 'green'
//				      });
/*				      pMarker = new L.marker(latlng,{icon: pondicon});
				      pMarker.bindPopup(label);
				      markers.addLayer(pMarker);
				      return pMarker;
				    }
				  });
				  map.spin(false);
					 $pond.attr("disabled", false);
					}, 3000);
				  
				  markers.on('clusterclick', function(a) {
				    console.log('Cluster Clicked:' + a);
				  });
				  markers.on('click', function(a) {
				    console.log('Marker Clicked:' + a);
				  });


				  map.addLayer(markers);
				});
 */


	