
function getAllCheckByResources(){
	check_aquafarm = $('#aquafarm');
	// identify the other checkboxes in the group
	aquafarm_class = $('.aquafarm');
	check_aquafarm.on('click',function(){
		aquafarm_class.click();
	});

	check_aquaseaweed = $('#aquaseaweed');
	// identify the other checkboxes in the group
	aquaseaweed_class = $('.aquaseaweed');
	check_aquaseaweed.on('click',function(){
		aquaseaweed_class.click();
	});
	
	check_aquamariculture = $('#aquamariculture');
	// identify the other checkboxes in the group
	aquamariculture_class = $('.aquamariculture');
	check_aquamariculture.on('click',function(){
		aquamariculture_class.click();
	});

	check_aquahatchery = $('#aquahatchery');
	// identify the other checkboxes in the group
	aquahatchery_class = $('.aquahatchery');
	check_aquahatchery.on('click',function(){
		aquahatchery_class.click();
	});
	
	check_capture = $('#capture');
	// identify the other checkboxes in the group
	capture_class = $('.capture');
	check_capture.on('click',function(){
		capture_class.click();
	});
	
	check_fishlanding = $('#fishlanding');
	// identify the other checkboxes in the group
	fishlanding_class = $('.fishlanding');
	check_fishlanding.on('click',function(){
		fishlanding_class.click();
	});
	
	check_coldstorage = $('#coldstorage');
	// identify the other checkboxes in the group
	coldstorage_class = $('.coldstorage');
	check_coldstorage.on('click',function(){
		coldstorage_class.click();
	});
	
	check_processing = $('#processing');
	// identify the other checkboxes in the group
	processing_class = $('.processing');
	check_processing.on('click',function(){
		processing_class.click();
	});
	
	check_postseaweed = $('#postseaweed');
	// identify the other checkboxes in the group
	postseaweed_class = $('.postseaweed');
	check_postseaweed.on('click',function(){
		postseaweed_class.click();
	});
	
	check_habitat = $('#habitat');
	// identify the other checkboxes in the group
	habitat_class = $('.habitat');
	check_habitat.on('click',function(){
		habitat_class.click();
	});
	
	check_infrastructure = $('#infrastructure');
	// identify the other checkboxes in the group
	infrastructure_class = $('.infrastructure');
	check_infrastructure.on('click',function(){
		infrastructure_class.click();
	});

	///AQUAFARM		
	$("#checked_pond").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/fishpond",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: pondicon});
							 },
					     	   onEachFeature: function (feature, layer) {

					     		  FPond.addLayer(layer.bindPopup(feature.properties.Information));	

					     		 FPond.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(FPond);
	
	}
	  
	});	
	$("#checked_fishpen").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/fishpen",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: penicon});
							 },
					     	   onEachFeature: function (feature, layer) {

					     		  FPEN.addLayer(layer.bindPopup(feature.properties.Information));	

					     		 FPEN.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(FPEN);
	
	}
	  
	});	

	$("#checked_fishcage").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/fishcage",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: cageicon});
							 },
					     	   onEachFeature: function (feature, layer) {

					     		  FCage.addLayer(layer.bindPopup(feature.properties.Information));	

					     			FCage.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(FCage);
	
	}
	  
	});	
	$("#checked_fishcoral").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/fishcoral",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: fishcoralsicon});
							 },
					     	   onEachFeature: function (feature, layer) {

					     			 Fishcorals.addLayer(layer.bindPopup(feature.properties.Information));	

					     		 Fishcorals.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Fishcorals);
	
	}
	  
	});	
	
	////SEAWEED AQUACULTURE
	$("#checked_seaweednursery").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/seaweeds/SEAWEEDNURSERY",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: seaweedsNurseryicon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.Type === "SEAWEEDNURSERY"){
					     			 Seaweed_nursery.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 Seaweed_nursery.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Seaweed_nursery);
	
	}
	  
	});			
			
	$("#checked_seaweedlaboratory").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/seaweeds/SEAWEEDLABORATORY",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: seaweedsLaboratoryicon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.Type === "SEAWEEDLABORATORY"){
					     			 Seaweed_laboratory.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 Seaweed_laboratory.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Seaweed_laboratory);
	
	}
  
	});
		
///MARICULTURE PARK
	$("#checked_mariculturebfar").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/mariculture/BFAR",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: maricultureicon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.Type === "BFAR"){
					     			 Mariculture_BFAR.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 Mariculture_BFAR.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Mariculture_BFAR);
	
	}
	  
	});		
	$("#checked_mariculturelgu").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/mariculture/LGU",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: maricultureLGUicon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.Type === "LGU"){
					     			 Mariculture_LGU.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 Mariculture_LGU.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Mariculture_LGU);
	
	}
	  
	});		
	$("#checked_maricultureprivate").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/mariculture/PRIVATE",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: mariculturePrivateicon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.Type === "PRIVATE"){
					     			 Mariculture_PRIVATE.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 Mariculture_PRIVATE.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Mariculture_PRIVATE);
	
	}
	  
	});			
///HATCHERY
	$("#checked_legislated").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/hatchery/legislated",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: iconComplete});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.Legislative === "legislated"){
					     			 Hatchery.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 Hatchery.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Hatchery);
	
	}


		  
	});		
	$("#checked_nonlegislated").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/hatchery/nonLegislated",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: iconNComplete});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.Legislative === "nonLegislated"){
					     			 HatcheryinComplete.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 HatcheryinComplete.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(HatcheryinComplete);
	
	}
	  
	});	

	$("#checked_hatcherylgu").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/hatchery/LGU",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: hatchLGU});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.Operator === "LGU"){
					     			 Hatchery_LGU.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 Hatchery_LGU.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Hatchery_LGU);
	
	}
	  
	});
	$("#checked_hatcheryprivate").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/hatchery/PRIVATE",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: hatchPrivate});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.Operator === "PRIVATE"){
					     			 Hatchery_PRIVATE.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 Hatchery_PRIVATE.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Hatchery_PRIVATE);
	
	}
	  
	});	
	
	///CAPTURE FISHERIES
	
	$("#checked_payao").change(function(){

		  var $checked_payao = $( this );
		  var checked_payao = $checked_payao.attr( "value" );
		  
			if($checked_payao.is(":checked")){
				$checked_payao.attr("disabled", true);
				  map.spin(true);
				  $.getJSON("/create/getGeoJsonByRegion/payao",function(data){
					  setTimeout(function () {
							 L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){
									 return L.marker(latlng,{icon: payaoicon});
								 },
						     	   onEachFeature: function (feature, layer) {
						     		  PAYAO.addLayer(layer.bindPopup(feature.properties.Information));	
						     			PAYAO.addTo(map);
						     	   }
								
							 })
					 		  map.spin(false);
							 $checked_payao.attr("disabled", false);
							}, 3000);
				  });

			

		}else{
			map.removeLayer(PAYAO);

		}
		  
		});
	$("#checked_lambaklad").change(function(){
		console.log("#checked_lambaklad");
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){
				$pond.attr("disabled", true);
				  map.spin(true);
				  $.getJSON("/create/getGeoJsonByRegion/lambaklad",function(data){
					  setTimeout(function () {
							 L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){
									 return L.marker(latlng,{icon: corals});
								 },
						     	   onEachFeature: function (feature, layer) {
						     		  LAMBAKLAD.addLayer(layer.bindPopup(feature.properties.Information));	
							     		 LAMBAKLAD.addTo(map);
						     	   }
								
							 })
					 		  map.spin(false);
							 $pond.attr("disabled", false);
							}, 3000);
				  });

		}else{
			map.removeLayer(LAMBAKLAD);
		
		}
		  
	});	
	
	$("#checked_frpboats").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){
			//	getFMA1();

		}else{
		//	map.removeLayer(FMAJSON1);
		
		}
		  
	});	

//$("#check_fishlanding").change(function(){
//		
//		  var $pond = $( this );
//		  var pond = $pond.attr( "value" );
//		  
//			if($pond.is(":checked")){
//				FL.addTo(map);
//
//		}else{
//			map.removeLayer(FL);
//		
//		}
//		  
//	});
	$("#check_cflc_operational").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/fishlanding/Operational",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: FLOperationalIcon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.CFLCSTATUS === "Operational"){
					     			 FL_operational.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 FL_operational.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(FL_operational);
	
	}
	  
	});
	
	$("#check_cflc_for_operation").change(function(){
		
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/fishlanding/ForOperation",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: FLForOperationalIcon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.CFLCSTATUS === "ForOperation"){
					     			 FL_for_operation.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 FL_for_operation.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(FL_for_operation);
	
	}
	  
	});		
	$("#check_cflc_for_complition").change(function(){
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/fishlanding/ForCompletion",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: FLForCompletionIcon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.CFLCSTATUS === "ForCompletion"){
					     			 FL_for_completion.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 FL_for_completion.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(FL_for_completion);
	
	}
	  
	});	
	
	$("#check_cflc_for_transfer").change(function(){
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/fishlanding/ForTransfer",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: FLForTransferIcon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.CFLCSTATUS === "ForTransfer"){
					     			 FL_for_transfer.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 FL_for_transfer.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(FL_for_transfer);
	
	}

		  
	});	
	
	$("#check_cflc_damaged").change(function(){
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/fishlanding/Damaged",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: FLDamagedIcon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.CFLCSTATUS === "Damaged"){
					     			 FL_damaged.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 FL_damaged.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(FL_damaged);
	
	}
	  
	});	
	
	$("#check_traditional").change(function(){
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/fishlanding/Traditional",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: FLTraditionalIcon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.CFLCSTATUS === "Traditional"){
					     			 FL_traditional.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 FL_traditional.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(FL_traditional);
	
	}
	
	  
	});	
	
	
	$("#check_noncflc").change(function(){
		
		  var $noncflc = $( this );
		  var noncflc = $noncflc.attr( "value" );
		  
			if($noncflc.is(":checked")){
				$noncflc.attr("disabled", true);
				  map.spin(true);
				  $.getJSON("/create/getGeoJsonByRegion/fishlanding/NonTraditional",function(data){
					  setTimeout(function () {
							 L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){
									 return L.marker(latlng,{icon: FLNontraditionalIcon});
								 },
						     	   onEachFeature: function (feature, layer) {
						     		  if(feature.properties.CFLCSTATUS === "NonTraditional"){
						     			 FL_noncflc.addLayer(layer.bindPopup(feature.properties.Information));	
						     		  }
						     		 FL_noncflc.addTo(map);   
						     	   }
								
							 })
					 		  map.spin(false);
							 $noncflc.attr("disabled", false);
							}, 3000);
				  });
				 

		}else{
			map.removeLayer(FL_noncflc);
		
		}
		  
	});		
	
	///COLD STORAGE/IPCS
	$("#check_coldpfda").change(function(){
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/cold/BFAR",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: ipcsbfaricon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.Operator === "BFAR"){
					     			 IPCS_PFDA.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 IPCS_PFDA.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(IPCS_PFDA);
	
	}
	  
	});	
	
	$("#check_coldprivate").change(function(){
		var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/cold/PRIVATE",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: ipcsprivateicon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.Operator === "PRIVATE"){
					     			 IPCS_PRIVATEORGANIZATION.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 IPCS_PRIVATEORGANIZATION.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(IPCS_PRIVATEORGANIZATION);
	
	}

		  
	});	
	
	$("#check_coldcooperative").change(function(){
		
		  var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/cold/COOPERATIVE",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: ipcscoopicon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.Operator === "COOPERATIVE"){
					     			 IPCS_COOPERATIVE.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 IPCS_COOPERATIVE.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(IPCS_COOPERATIVE);
	
	}
	  
	});		
	///POST HARVEST SEAWEED
	$("#check_warehouse").change(function(){

		  var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/seaweeds/SEAWEEDWAREHOUSE",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: seaweedsWarehouseicon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.Operator === "SEAWEEDWAREHOUSE"){
					     			 Seaweed_warehouse.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 Seaweed_warehouse.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Seaweed_warehouse);
	
	}

		  
	});		

	$("#check_dryer").change(function(){

		  var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/seaweeds/SEAWEEDDRYER",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: seaweedsDryericon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  if(feature.properties.Operator === "SEAWEEDDRYER"){
					     		  Seaweed_dryer.addLayer(layer.bindPopup(feature.properties.Information));	
					     		  }
					     		 Seaweed_dryer.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Seaweed_dryer);
	
	}
	  
	});	
	$("#checked_market").change(function(){

		  var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/market",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: marketicon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  Market.addLayer(layer.bindPopup(feature.properties.Information));	
					     		 Market.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Market);
	
	}
	  
	});	
	
	$("#checked_fishport").change(function(){

		  var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
		  if($checked_lakes.is(":checked")){
			  
			  $checked_lakes.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/fishport",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: porticon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  FPT.addLayer(layer.bindPopup(feature.properties.Information));	
					     		 FPT.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $checked_lakes.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(FPT);
	
	}

		  
	});	
	
	$("#checked_kadiwa").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){
			//	getFMA1();

		}else{
		//	map.removeLayer(FMAJSON1);
		
		}
		  
	});		

	
	//FISH HABITAT
	$("#checked_seagrass").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
		  if($pond.is(":checked")){
			  $pond.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/seagrass",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: seagrassicon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  Seagrass.addLayer(layer.bindPopup(feature.properties.Information));	
					     		 Seagrass.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $pond.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Seagrass);
	
	}
  
	  
	});	
	
	$("#checked_mangrove").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
		  if($pond.is(":checked")){
			  
			  $pond.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/mangrove",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: mangroveicon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  Mangrove.addLayer(layer.bindPopup(feature.properties.Information));	
					     		 Mangrove.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $pond.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Mangrove);
	
	}
  
	});	
	
	$("#checked_coralreefs").change(function(){

		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
		  if($pond.is(":checked")){
			  
			  $pond.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/coralreefs",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: coralreefsicon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  CORALREEFS.addLayer(layer.bindPopup(feature.properties.Information));	
					     		 CORALREEFS.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $pond.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(CORALREEFS);
	
	}
	  
	});	
	
	///BFAR INFRASTRUCTURE
	
	$("#check_nationalcenter").change(function(){

		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
		  if($pond.is(":checked")){
			  $pond.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/nationalcenter",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: ncentericon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  NCENTER.addLayer(layer.bindPopup(feature.properties.Information));	
					     		 NCENTER.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $pond.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(NCENTER);
	
	}

		  
	});	
	
	$("#check_regionaloffices").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
		  if($pond.is(":checked")){
			  
			  $pond.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/regionaloffices",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: rofficeicon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  ROFFICE.addLayer(layer.bindPopup(feature.properties.Information));	
					     		 ROFFICE.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $pond.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(ROFFICE);
	
	}
  
	});	
	
	$("#check_rdtc").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
		  if($pond.is(":checked")){
			  $pond.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/trainingcenter",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: trainingcentericon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  Trainingcenter.addLayer(layer.bindPopup(feature.properties.Information));	
					     		 Trainingcenter.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $pond.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(Trainingcenter);
	
	}

		  
	});	
	
	$("#checked_lgu").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
		  if($pond.is(":checked")){
			  $pond.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/lgu",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: lguicon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  LGU.addLayer(layer.bindPopup(feature.properties.Information));	
					     		 LGU.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $pond.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(LGU);
	
	}

		  
	});		

	$("#check_tos").change(function(){

		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
		  if($pond.is(":checked")){
			  
			  $pond.attr("disabled", true);
			  map.spin(true);
			  $.getJSON("/create/getGeoJsonByRegion/tos",function(data){
				  setTimeout(function () {
						 L.geoJSON(data, {
							 pointToLayer: function (feature, latlng){
								 return L.marker(latlng,{icon: toscon});
							 },
					     	   onEachFeature: function (feature, layer) {
					     		  TOS.addLayer(layer.bindPopup(feature.properties.Information));	
					     		 TOS.addTo(map);   
					     	   }
							
						 })
				 		  map.spin(false);
						 $pond.attr("disabled", false);
						}, 3000);
			  });
			 

	}else{
		map.removeLayer(TOS);
	
	}

		  
	});			

	$("#checked_schoolOfFisheries").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
			if($pond.is(":checked")){
				$pond.attr("disabled", true);
				  map.spin(true);
				  $.getJSON("/create/getGeoJsonByRegion/schoolOfFisheries",function(data){
					  setTimeout(function () {
							 L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){
									 return L.marker(latlng,{icon: schoolicon});
								 },
						     	   onEachFeature: function (feature, layer) {
						     		  School.addLayer(layer.bindPopup(feature.properties.Information));	
						     		 School.addTo(map);   
						     	   }
								
							 })
					 		  map.spin(false);
							 $pond.attr("disabled", false);
							}, 3000);
				  });
				 

		}else{
			map.removeLayer(School);
		
		}

		  
	});	
	
$("#check_fishsanctuaries").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){
				 $pond.attr("disabled", true);
				  map.spin(true);
				  $.getJSON("/create/getGeoJsonByRegion/fishsanctuary",function(data){
					  setTimeout(function () {
							 L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){
									 return L.marker(latlng,{icon: sanctuaryicon});
								 },
						     	   onEachFeature: function (feature, layer) {
						     		  FS.addLayer(layer.bindPopup(feature.properties.Information));	
						     		FS.addTo(map);   
						     	   }
								
							 })
					 		  map.spin(false);
							 $pond.attr("disabled", false);
							}, 3000);
				  });
				 

		}else{
			map.removeLayer(FS);
		
		}
		  
	});

$("#check_processingbfar").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){
				 $pond.attr("disabled", true);
				 map.spin(true);
				  $.getJSON("/create/getGeoJsonByRegion/fishprocessing/BFAR_MANAGED",function(data){
					  setTimeout(function () {
							 L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){
									 if(feature.properties.Operator === "BFAR_MANAGED"){
										  return L.marker(latlng,{icon: FP_BFAR_ICON});
										 }
								 },
						     	   onEachFeature: function (feature, layer) {
						     		  if(feature.properties.Operator === "BFAR_MANAGED"){						     			
						     			FP_BFAR.addLayer(layer.bindPopup(feature.properties.Information));	
						     		  }						     								     		 
										FP_BFAR.addTo(map);  
						     	   }
								
							 })
					 		  map.spin(false);
							 $pond.attr("disabled", false);
							}, 3000);
				  });
				

		}else{
			map.removeLayer(FP_BFAR);
		
		}
		  
	});
	
	$("#check_processinglgu").change(function(){
	
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){
				 $pond.attr("disabled", true);
				map.spin(true);
				  $.getJSON("/create/getGeoJsonByRegion/fishprocessing/LGU_MANAGED",function(data){
					  setTimeout(function () {
							 L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){
									 if(feature.properties.Operator === "LGU_MANAGED"){
									 return L.marker(latlng,{icon: FP_LGU_ICON});
									 }
								 },
						     	   onEachFeature: function (feature, layer) {
						     		  if(feature.properties.Operator === "LGU_MANAGED"){
						     			 FP_LGU.addLayer(layer.bindPopup(feature.properties.Information));
						     		  }							     								     		 
						     			FP_LGU.addTo(map); 
						     	   }
								
							 })
					 		  map.spin(false);
							 $pond.attr("disabled", false);
							}, 3000);
				  });
				
		}else{
			map.removeLayer(FP_LGU);
		
		}
		  
	});	
	
	$("#check_processingprivate").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){
				 $pond.attr("disabled", true);
				map.spin(true);
				  $.getJSON("/create/getGeoJsonByRegion/fishprocessing/PRIVATE_SECTOR",function(data){
					  setTimeout(function () {
							 L.geoJSON(data, {
								 pointToLayer: function (feature, latlng){
									 if(feature.properties.Operator === "PRIVATE_SECTOR"){
									 return L.marker(latlng,{icon: FP_PRIVATE_ICON});
									 }
								 },
						     	   onEachFeature: function (feature, layer) {
						     		  if(feature.properties.Operator === "PRIVATE_SECTOR"){
						     			 FP_PRIVATE.addLayer(layer.bindPopup(feature.properties.Information));
						     		  }
						     			FP_PRIVATE.addTo(map);
						     	   }
								
							 })
					 		  map.spin(false);
							 $pond.attr("disabled", false);
							}, 3000);
				  });
				

		}else{
			map.removeLayer(FP_PRIVATE);
		
		}
		  
	});
	
	$("#check_processingcooperative").change(function(){
		
		  var $pond = $( this );
		  var pond = $pond.attr( "value" );
		  
			if($pond.is(":checked")){
				 $pond.attr("disabled", true);
				map.spin(true);
				  $.getJSON("/create/getGeoJsonByRegion/fishprocessing/COOPERATIVE",function(data){
					  
					  setTimeout(function () {
							 L.geoJSON(data, {
								
								 pointToLayer: function (feature, latlng){
									 if(feature.properties.Operator === "COOPERATIVE"){
									 return L.marker(latlng,{icon: FP_COOPERATIVE_ICON}); 
									 }
								 },
						     	   onEachFeature: function (feature, layer) {
						     		  if(feature.properties.Operator === "COOPERATIVE"){
						     			  FP_COOPERATIVE.addLayer(layer.bindPopup(feature.properties.Information)); 							     								     		 
						     		  }	
						     		  FP_COOPERATIVE.addTo(map);
						     	   }
								
							 })
					 		  map.spin(false);
							 $pond.attr("disabled", false);
							}, 3000);
				  });
				

		}else{
			map.removeLayer(FP_COOPERATIVE);
		
		}
		  
	});
}