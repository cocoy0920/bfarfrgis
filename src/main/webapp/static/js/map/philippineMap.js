var philippineMap = new L.LayerGroup();
var regionMap = new L.LayerGroup();
var bays = [];
var Ilocos;
var CAR;
var CALABARZON;
var CentralLuzon;
var Bicol;
var MIMAROPA;
var NCR;
var CagayanValley;
var Eastern;
var Western;
var Central;
var Caraga;
var NorthernMindanao;
var SOCCSKSARGEN;
var ARMM;
var DavaoRegion;
var FLA;
var dataAPI = [];
var prevLayerClicked = null;
var myarray;


Ilocos = L.geoJSON.ajax('/static/geojson/Ilocos.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources}).addTo(philippineMap);	
CAR = L.geoJSON.ajax('/static/geojson/CAR.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);	
CALABARZON = L.geoJSON.ajax('/static/geojson/CALABARZON.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);	
CentralLuzon = L.geoJSON.ajax('/static/geojson/CentralLuzon.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);
Bicol  = L.geoJSON.ajax('/static/geojson/Bicol.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);	
MIMAROPA = L.geoJSON.ajax('/static/geojson/MIMAROPA.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);
NCR  = L.geoJSON.ajax('/static/geojson/NCR.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);
CagayanValley = L.geoJSON.ajax('/static/geojson/CagayanValley.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);
Eastern =L.geoJSON.ajax('/static/geojson/Eastern.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);
Western=L.geoJSON.ajax('/static/geojson/Western.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap); 
Central =L.geoJSON.ajax('/static/geojson/Central.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);
Caraga =L.geoJSON.ajax('/static/geojson/Caraga.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);
NorthernMindanao =L.geoJSON.ajax('/static/geojson/NorthernMindanao.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);
SOCCSKSARGEN =L.geoJSON.ajax('/static/geojson/SOCCSKSARGEN.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);
ARMM =L.geoJSON.ajax('/static/geojson/ARMM.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);
DavaoRegion =L.geoJSON.ajax('/static/geojson/DavaoRegion.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);
ZamboangaPeninsula = L.geoJSON.ajax('/static/geojson/ZamboangaPeninsula.geojson',{ style:setStyleColorPhilMap(), onEachFeature: MapResources }).addTo(philippineMap);
/*FLA = L.geoJSON.ajax('/static/geojson/fla_layer.json',{ style:setStyleColorFLA(), 
onEachFeature: function (feature, layer){
					var features = feature.properties.plot_data;
					console.log(features);
					myarray = JSON.parse(features);
					console.log(myarray);
					console.log(myarray.plot[0]);
	       		layer.bindPopup(feature.properties.region + "<br>" +feature.properties.province + "<br>" + feature.properties.municipality + "<br>" + feature.properties.name_lessee , {closeButton: false});
 
},
}).addTo(philippineMap);*/


console.log(FLA);

function getPhilippineMap(){
				
				philippineMap.addTo(map);
				
				//philMap = L.geoJSON(philippineMap.toGeoJSON()).addTo(map);
				//map.fitBounds(philMap.getBounds().pad(1));
}

function viewMapByRegion(){
	$.get("/home/regionMap", function(content, status) {
		fetch("/static/geojson/" + content + ".geojson").then(res => res.json()).then(data => {
			var visible;
			geoJSON = L.geoJson(data, {
				 style:setStyleColorPhilMap(), 
				 onEachFeature: MapResources ,
			});
			geoJSON.addTo(map);
			map.fitBounds(geoJSON.getBounds());

		});
	});
	
}		  
function Depth(){	
//	getBayName(bays);
//	removeBayName(bays);
			  var legend = L.control({position: 'bottomright'});
			  
			  legend.onAdd = function (map) {
				  var div = L.DomUtil.create('div', 'info legend')

					  div.innerHTML = 	'DEPTH<br>' +
					  					'<ul class="list-unstyled components mb-5">' + 			        
					  					'<li>' +
					  					'<input type="checkbox" id="checked_10000" value="10000" name="10000" class="depth">'+
				  						'<i style="background:#003441"></i> 5001 - 10000' + 
				  						'</li>'+
				  						'<li>' +
					  					'<input type="checkbox" id="checked_5000" value="5000" name="5000" class="depth">'+	
						  				'<i style="background:#005b72"></i> 3001 - 5000' +
						  				'</li>'+
						  				'<li>' +
					  					'<input type="checkbox" id="checked_3000" value="3000" name="3000" class="depth">'+			  						
					  					'<i style="background:#0082a3"></i> 2001 - 3000' + 
					  					'</li>'+
					  					'<li>' +
					  					'<input type="checkbox" id="checked_1000" value="1000" name="1000" class="depth">'+			  						
					  					'<i style="background:#0082a3"></i> 1001 - 2000' + 
					  					'</li>'+
//					  					'REGIONS<br>' +
//					  					'<li><i style="background:#0082a3"></i>Ilocos Region (Region I)</li>'+
//					  					'<li><i style="background:#68BBE3"></i>Cordillera Administrative Region </li>'+
//				  						'<li><i style="background:#2E8BC0"></i>Cagayan Valley (Region II)</li>'+
//						  				'<li><i style="background:#055C9D"></i>Central Luzon (Region III)</li>'+
//						  				'<li><i style="background:#68BBE3"></i>CALABARZON (Region IV‑A)</li>'+
//						  				'<li><i style="background:#189AB4"></i>MIMAROPA Region (Region IV-B )</li>'+
//						  				'<li><i style="background:#0C2D48"></i>National Capital Region </li>'+
//						  				'<li><i style="background:#B1D4E0"></i>Bicol Region (Region V)</li>'+
//						  				'<li><i style="background:#3D550C"></i>Western Visayas (Region VI)</li>'+
//						  				'<li><i style="background:#59981A"></i>Central Visayas (Region VII)</li>'+
//						  				'<li><i style="background:#ffffff"></i>Zamboanga Peninsula (Region IX)</li>'+
//						  				'<li><i style="background:#ffffff"></i>Northern Mindanao (Region X)</li>'+
//						  				'<li><i style="background:#ffffff"></i>Davao Region (Region XI) </li>'+
//						  				'<li><i style="background:#ffffff"></i>SOCCSKSARGEN (Region XII) </li>'+
//						  				'<li><i style="background:#ffffff"></i>Caraga (Region XIII) </li>'+	
//					  					
					  					'</ul>';
					  
					  return div;

				  
			  } 
			//  legend.addTo(map);
			  
}



function DepthByValue(){
	

			$("#checked_10000").change(function(){
					console.log("#checked_10000");
					  var $depth = $( this );
					  var depth = $depth.attr( "value" );
					  
						if($depth.is(":checked")){
							depth_10000.addTo(map);

					}else{
						map.removeLayer(depth_10000);
					
					}
					  
				});
				
				$("#checked_5000").change(function(){
					console.log("#checked_500");
					  var $depth = $( this );
					  var depth = $depth.attr( "value" );
					  
						if($depth.is(":checked")){
							depth_5000.addTo(map);

					}else{
						map.removeLayer(depth_5000);
					
					}
					  
				});
				$("#checked_3000").change(function(){
					console.log("#checked_300");
					  var $depth = $( this );
					  var depth = $depth.attr( "value" );
					  
						if($depth.is(":checked")){
							depth_3000.addTo(map);

					}else{
						map.removeLayer(depth_3000);
					
					}
					  
				});
				$("#checked_2000").change(function(){
					console.log("#checked_200");
					  var $depth = $( this );
					  var depth = $depth.attr( "value" );
					  
						if($depth.is(":checked")){
							depth_2000.addTo(map);
							

					}else{
						map.removeLayer(depth_2000);
					
					}
					  
				});
				$("#checked_1000").change(function(){
					console.log("#checked_1000");
					  var $depth = $( this );
					  var depth = $depth.attr( "value" );
					  
						if($depth.is(":checked")){
							depth_1000.addTo(map);

					}else{
						map.removeLayer(depth_1000);
					
					}
					  
				});
				
/*			    new L.Control.BootstrapModal({
			        modalId: 'modal_about',
			        tooltip: "Fisheries Management Areas",
			        glyph: 'info-sign',
			        info:'FMA'
			    }).addTo(map);*/
			    

					
}	

function FMA_SEA(){
	
	$("#FMA-1").change(function(){
		  var $fma1 = $( this );
		  var fma1 = $fma1.attr( "value" );
		  
			if($fma1.is(":checked")){
				getFMA1();

		}else{
			map.removeLayer(FMAJSON1);
		
		}
		  
	});
	$("#FMA-2").change(function(){
		
		  var $fma2 = $( this );
		  var fma2 = $fma2.attr( "value" );
		  
			if($fma2.is(":checked")){
				getFMA2();

		}else{
			map.removeLayer(FMAJSON2);
		
		}
		  
	});
	$("#FMA-3").change(function(){

		  var $fma3 = $( this );
		  var fma3 = $fma3.attr( "value" );
		  
			if($fma3.is(":checked")){
				getFMA3();

		}else{
			map.removeLayer(FMAJSON3);
		
		}
		  
	});
	$("#FMA-4").change(function(){

		  var $fma4 = $( this );
		  var fma4 = $fma4.attr( "value" );
		  
			if($fma4.is(":checked")){
				getFMA4();

		}else{
			map.removeLayer(FMAJSON4);
		
		}
		  
	});

	$("#FMA-5").change(function(){

		  var $fma5 = $( this );
		  var fma5 = $fma5.attr( "value" );
		  
			if($fma5.is(":checked")){
				getFMA5();

		}else{
			map.removeLayer(FMAJSON5);
		
		}
		  
	});
	$("#FMA-6").change(function(){

		  var $fma6 = $( this );
		  var fma6 = $fma6.attr( "value" );
		  
			if($fma6.is(":checked")){
				getFMA6();

		}else{
			map.removeLayer(FMAJSON6);
		
		}
		  
	});
	$("#FMA-7").change(function(){

		  var $fma7 = $( this );
		  var fma7 = $fma7.attr( "value" );
		  
			if($fma7.is(":checked")){
				getFMA7();

		}else{
			map.removeLayer(FMAJSON7);
		
		}
		  
	});
	$("#FMA-8").change(function(){

		  var $fma8 = $( this );
		  var fma8 = $fma8.attr( "value" );
		  
			if($fma8.is(":checked")){
				getFMA8();

		}else{
			map.removeLayer(FMAJSON8);
		
		}
		  
	});
	$("#FMA-9").change(function(){

		  var $fma9 = $( this );
		  var fma9 = $fma9.attr( "value" );
		  
			if($fma9.is(":checked")){
				getFMA9();

		}else{
			map.removeLayer(FMAJSON9);
		
		}
		  
	});
	$("#FMA-10").change(function(){
		console.log("#FMA-10");
		  var $fma10 = $( this );
		  var fma10 = $fma10.attr( "value" );
		  
			if($fma10.is(":checked")){
				getFMA10();

		}else{
			map.removeLayer(FMAJSON10);
		
		}
		  
	});
	$("#FMA-11").change(function(){

		  var $fma11 = $( this );
		  var fma11 = $fma11.attr( "value" );
		  
			if($fma11.is(":checked")){
				getFMA11();

		}else{
			map.removeLayer(FMAJSON11);
		
		}
		  
	});
	$("#FMA-12").change(function(){

		  var $fma12 = $( this );
		  var fma12 = $fma12.attr( "value" );
		  
			if($fma12.is(":checked")){
				getFMA12();

		}else{
			map.removeLayer(FMAJSON12);
		
		}
		  
	});
	
	$("#checked_bay_tayabas").change(function(){
		
//		let name = "bay_tayabas";
//		bays.push({
//			name:"bay_tayabas"
//		});
//		
		  var $checked_tayabas_bay = $( this );
		  var checked_tayabas_bay = $checked_tayabas_bay.attr( "value" );
		  
			if($checked_tayabas_bay.is(":checked")){
			//	map.removeControl(add_legend);
			//	let removeData =false;
				getTayabasBay();
				//getBayName(bays,removeData);
			
		}else{
			map.removeLayer(tayabas_bay);
			//map.removeControl(add_legend);
			//removeBayName(name);
			//map.removeControl(add_legend);
			
		}
		  
	});
	$("#checked_bay_iligan").change(function(){
		
//		let name = "bay_iligan";
//		bays.push({
//			name:"bay_iligan"
//		});
		
		  var $checked_bay_iligan = $( this );
		  var checked_bay_iligan = $checked_bay_iligan.attr( "value" );
		  
			if($checked_bay_iligan.is(":checked")){
			//	let removeData =false;
			//	map.removeControl(add_legend);
				getIliganBay();
				//getBayName(bays,removeData);
				
		}else{
			map.removeLayer(iligan_bay);
			//map.removeControl(add_legend);
		//	removeBayName(name);
		//	map.removeControl(add_legend);
			
		}
		  
	});
	$("#checked_bay_illana").change(function(){
		let name = "bay_iligan";
		  var $checked_bay_illana = $( this );
		  var checked_bay_illana = $checked_bay_illana.attr( "value" );
		  
			if($checked_bay_illana.is(":checked")){
			//	let removeData =false;
			//	map.removeControl(add_legend);
				getIllanaBay();
				//getBayName(bays,removeData);

		}else{
			map.removeLayer(illana_bay);
		
		}
		  
	});
	$("#checked_bay_imuruan").change(function(){

		  var $checked_bay_imuruan = $( this );
		  var checked_bay_imuruan = $checked_bay_imuruan.attr( "value" );
		  
			if($checked_bay_imuruan.is(":checked")){
				getImuruanBay();

		}else{
			map.removeLayer(imuruan_bay);
		
		}
		  
	});
	$("#checked_bay_lamon").change(function(){

		  var $checked_bay_lamon = $( this );
		  var checked_bay_lamon = $checked_bay_lamon.attr( "value" );
		  
			if($checked_bay_lamon.is(":checked")){
				getLamonBay();

		}else{
			map.removeLayer(lamon_bay);
		
		}
		  
	});
	$("#checked_bay_manila").change(function(){

		  var $checked_bay_manila = $( this );
		  var checked_bay_manila = $checked_bay_manila.attr( "value" );
		  
			if($checked_bay_manila.is(":checked")){
				getManilaBay();

		}else{
			map.removeLayer(manila_bay);
		
		}
		  
	});
	$("#checked_bay_sanmiguel").change(function(){

		  var $checked_bay_sanmiguel = $( this );
		  var checked_bay_sanmiguel = $checked_bay_sanmiguel.attr( "value" );
		  
			if($checked_bay_sanmiguel.is(":checked")){
				getSanMiguelBay();

		}else{
			map.removeLayer(san_miguel_bay);
		
		}
		  
	});
	$("#checked_bay_sibugay").change(function(){

		  var $checked_bay_sibugay = $( this );
		  var checked_bay_sibugay = $checked_bay_sibugay.attr( "value" );
		  
			if($checked_bay_sibugay.is(":checked")){
				getSibugayBay();

		}else{
			map.removeLayer(sibugay_bay);
		
		}
		  
	});
	$("#checked_bay_tawitawi").change(function(){

		  var $checked_bay_tawitawi = $( this );
		  var checked_bay_tawitawi = $checked_bay_tawitawi.attr( "value" );
		  
			if($checked_bay_tawitawi.is(":checked")){
				getTawiTawiBay();

		}else{
			map.removeLayer(tawitawi_bay);
		
		}
		  
	});
////STRAIT
	$("#checked_strait_cebu").change(function(){

		  var $checked_strait_cebu = $( this );
		  var checked_strait_cebu = $checked_strait_cebu.attr( "value" );
		  
			if($checked_strait_cebu.is(":checked")){
				getCebuStrait();

		}else{
			map.removeLayer(cebu_strait);
		
		}
		  
	});	
	$("#checked_strait_iloilo").change(function(){

		  var $checked_strait_iloilo = $( this );
		  var checked_strait_iloilo = $checked_strait_iloilo.attr( "value" );
		  
			if($checked_strait_iloilo.is(":checked")){
				getIloIloStrait();

		}else{
			map.removeLayer(iloilo_strait);
		
		}
		  
	});	
	
	$("#checked_strait_mindoro").change(function(){

		  var $checked_strait_mindoro = $( this );
		  var checked_strait_mindoro = $checked_strait_mindoro.attr( "value" );
		  
			if($checked_strait_mindoro.is(":checked")){
				getMindoroStrait();

		}else{
			map.removeLayer(mindoro_strait);
		
		}
		  
	});	
	
	$("#checked_strait_tablas").change(function(){

		  var $checked_strait_tablas = $( this );
		  var checked_strait_tablas = $checked_strait_tablas.attr( "value" );
		  
			if($checked_strait_tablas.is(":checked")){
				getTablasStrait();

		}else{
			map.removeLayer(tablas_strait);
		
		}
		  
	});
	
	$("#checked_strait_Tanan").change(function(){

		  var $checked_strait_Tanan = $( this );
		  var checked_strait_Tanan = $checked_strait_Tanan.attr( "value" );
		  
			if($checked_strait_Tanan.is(":checked")){
				getTanonStrait();

		}else{
			map.removeLayer(tanon_strait);
		
		}
		  
	});
	
////CHANNEL	
	
	
	$("#checked_channel_babuyan").change(function(){

		  var $checked_channel_babuyan = $( this );
		  var checked_channel_babuyan = $checked_channel_babuyan.attr( "value" );
		  
			if($checked_channel_babuyan.is(":checked")){
				getBabuyanChannel();

		}else{
			map.removeLayer(babuyan_channel);
		
		}
		  
	});
	
	$("#checked_channel_jintotolo").change(function(){

		  var $checked_channel_jintotolo = $( this );
		  var checked_channel_jintotolo = $checked_channel_jintotolo.attr( "value" );
		  
			if($checked_channel_jintotolo.is(":checked")){
				getJintotoloChannel();

		}else{
			map.removeLayer(jintotolo_channel);
		
		}
		  
	});
	$("#checked_channel_maqueda").change(function(){

		  var $checked_channel_maqueda = $( this );
		  var checked_channel_maqueda = $checked_channel_maqueda.attr( "value" );
		  
			if($checked_channel_maqueda.is(":checked")){
				getMaquedaChannel();

		}else{
			map.removeLayer(maqueda_channel);
		
		}
		  
	});

//GULF
	$("#checked_gulf_albay").change(function(){

		  var $checked_gulf_albay = $( this );
		  var checked_gulf_albay = $checked_gulf_albay.attr( "value" );
		  
			if($checked_gulf_albay.is(":checked")){
				getAlbayGulf();

		}else{
			map.removeLayer(albay_gulf);
		
		}
		  
	});
	$("#checked_gulf_asid").change(function(){

		  var $checked_gulf_asid = $( this );
		  var checked_gulf_asid = $checked_gulf_asid.attr( "value" );
		  
			if($checked_gulf_asid.is(":checked")){
				getAsidGulf();

		}else{
			map.removeLayer(asid_gulf);
		
		}
		  
	});
	
	$("#checked_gulf_davao").change(function(){

		  var $checked_gulf_davao = $( this );
		  var checked_gulf_davao = $checked_gulf_davao.attr( "value" );
		  
			if($checked_gulf_davao.is(":checked")){
				getDavaoGulf();

		}else{
			map.removeLayer(davao_gulf);
		
		}
		  
	});
	
	$("#checked_gulf_lagonoy").change(function(){

		  var $checked_gulf_lagonoy = $( this );
		  var checked_gulf_lagonoy = $checked_gulf_lagonoy.attr( "value" );
		  
			if($checked_gulf_lagonoy.is(":checked")){
				getLagonoyGulf();

		}else{
			map.removeLayer(lagonoy_gulf);
		
		}
		  
	});
	
	
	$("#checked_gulf_leyte").change(function(){

		  var $checked_gulf_leyte = $( this );
		  var checked_gulf_leyte = $checked_gulf_leyte.attr( "value" );
		  
			if($checked_gulf_leyte.is(":checked")){
				getLeyteGulf();

		}else{
			map.removeLayer(leyte_gulf);
		
		}
		  
	});
	
	
	$("#checked_gulf_lingayen").change(function(){

		  var $checked_gulf_lingayen = $( this );
		  var checked_gulf_lingayen = $checked_gulf_lingayen.attr( "value" );
		  
			if($checked_gulf_lingayen.is(":checked")){
				getLingayenGulf();

		}else{
			map.removeLayer(lingayen_gulf);
		
		}
		  
	});
	
	
	$("#checked_gulf_moro").change(function(){

		  var $checked_gulf_moro = $( this );
		  var checked_gulf_moro = $checked_gulf_moro.attr( "value" );
		  
			if($checked_gulf_moro.is(":checked")){
				getMoroGulf();

		}else{
			map.removeLayer(moro_gulf);
		
		}
		  
	});
	
	$("#checked_gulf_panay").change(function(){

		  var $checked_gulf_panay = $( this );
		  var checked_gulf_panay = $checked_gulf_panay.attr( "value" );
		  
			if($checked_gulf_panay.is(":checked")){
				getPanay_Gulf();

		}else{
			map.removeLayer(panay_gulf);
		
		}
		  
	});
	
	$("#checked_gulf_ragay").change(function(){

		  var $checked_gulf_ragay = $( this );
		  var checked_gulf_ragay = $checked_gulf_ragay.attr( "value" );
		  
			if($checked_gulf_ragay.is(":checked")){
				getRagayGulf();

		}else{
			map.removeLayer(ragay_gulf);
		
		}
		  
	});
	$("#checked_sea_bohol").change(function(){

		  var $checked_sea_bohol = $( this );
		  var checked_sea_bohol = $checked_sea_bohol.attr( "value" );
		  
			if($checked_sea_bohol.is(":checked")){
				getBoholSea();

		}else{
			map.removeLayer(bohol_sea);
		
		}
		  
	});
	$("#checked_sea_camotes").change(function(){

		  var $checked_sea_camotes = $( this );
		  var checked_sea_camotes = $checked_sea_camotes.attr( "value" );
		  
			if($checked_sea_camotes.is(":checked")){
				getCamotesSea();

		}else{
			map.removeLayer(camotes_sea);
		
		}
		  
	});
	
	$("#checked_sea_eastsulu").change(function(){

		  var $checked_sea_eastsulu = $( this );
		  var checked_sea_eastsulu = $checked_sea_eastsulu.attr( "value" );
		  
			if($checked_sea_eastsulu.is(":checked")){
				getEastSuluSea();

		}else{
			map.removeLayer(east_sulu_sea);
		
		}
		  
	});
	$("#checked_sea_samar").change(function(){

		  var $checked_sea_samar = $( this );
		  var checked_sea_samar = $checked_sea_samar.attr( "value" );
		  
			if($checked_sea_samar.is(":checked")){
				getSamarSea();

		}else{
			map.removeLayer(samar_sea);
		
		}
		  
	});
	$("#checked_sea_sibuyan").change(function(){

		  var $checked_sea_sibuyan = $( this );
		  var checked_sea_sibuyan = $checked_sea_sibuyan.attr( "value" );
		  
			if($checked_sea_sibuyan.is(":checked")){
				getSibuyanSea();

		}else{
			map.removeLayer(sibuyan_sea);
		
		}
		  
	});
	$("#checked_sea_southsulu").change(function(){

		  var $checked_sea_southsulu = $( this );
		  var checked_sea_southsulu = $checked_sea_southsulu.attr( "value" );
		  
			if($checked_sea_southsulu.is(":checked")){
				getSouthSuluSea();

		}else{
			map.removeLayer(south_sulu_sea);
		
		}
		  
	});
	$("#checked_sea_visayan").change(function(){

		  var $checked_sea_visayan = $( this );
		  var checked_sea_visayan = $checked_sea_visayan.attr( "value" );
		  
			if($checked_sea_visayan.is(":checked")){
				getVisayanSea();

		}else{
			map.removeLayer(visayan_sea);
		
		}
		  
	});
	$("#checked_sea_westsulu").change(function(){

		  var $checked_sea_westsulu = $( this );
		  var checked_sea_westsulu = $checked_sea_westsulu.attr( "value" );
		  
			if($checked_sea_westsulu.is(":checked")){
				getWestSuluSea();

		}else{
			map.removeLayer(west_sulu_sea);
		
		}
		  
	});
	$("#checked_pass_burias").change(function(){
		
		  var $checked_pass_burias = $( this );
		  var checked_pass_burias = $checked_pass_burias.attr( "value" );
		  
			if($checked_pass_burias.is(":checked")){
				getBuriasPass();

		}else{
			map.removeLayer(burias_pass2);
		
		}
		  
	});
	$("#checked_pass_ticao").change(function(){
		
		  var $checked_pass_ticao = $( this );
		  var checked_pass_ticao = $checked_pass_ticao.attr( "value" );
		  
			if($checked_pass_ticao.is(":checked")){
				getTicaoPass();

		}else{
			map.removeLayer(ticao_pass);
		
		}
		  
	});
	
///LAKES
	$("#checked_lakes_bato_albay").change(function(){
		
		  var $checked_lakes_bato_albay = $( this );
		  var checked_lakes_bato_albay = $checked_lakes_bato_albay.attr( "value" );
		  
			if($checked_lakes_bato_albay.is(":checked")){
				getBatoLakeAlbay();

		}else{
			map.removeLayer(lake_bato_albay);
			
		}
		  
	});
	$("#checked_lakes_bato_camsur").change(function(){
		
		  var $checked_lakes_bato_camsur = $( this );
		  var checked_lakes_bato_camsur = $checked_lakes_bato_camsur.attr( "value" );
		  
			if($checked_lakes_bato_camsur.is(":checked")){
				getBatoLakeCamarinesSur();

		}else{
			map.removeLayer(lake_bato_camsur);
		
		}
		  
	});
	$("#checked_lakes_buhi_camsur").change(function(){
		
		  var $checked_lakes_buhi_camsur = $( this );
		  var checked_lakes_buhi_camsur = $checked_lakes_buhi_camsur.attr( "value" );
		  
			if($checked_lakes_buhi_camsur.is(":checked")){
				getBuhiLakeCamarinesSur();

		}else{
			map.removeLayer(lake_buhi_camsur);
		
		}
		  
	});
	$("#checked_lakes_buluan_mag").change(function(){
		
		  var $checked_lakes_buluan_mag = $( this );
		  var checked_lakes_buluan_mag = $checked_lakes_buluan_mag.attr( "value" );
		  
			if($checked_lakes_buluan_mag.is(":checked")){
				getBuluanLakeMaguindanao();

		}else{
			map.removeLayer(lake_buluan_maguindanao);
		
		}
		  
	});
	$("#checked_lakes_buluan_sultan").change(function(){
		
		  var $checked_lakes_buluan_sultan = $( this );
		  var checked_lakes_buluan_sultan = $checked_lakes_buluan_sultan.attr( "value" );
		  
			if($checked_lakes_buluan_sultan.is(":checked")){
				getBuluanLakeSultanKudarat();

		}else{
			map.removeLayer(lake_buluan_sultan_kudarat);
		
		}
		  
	});
	$("#checked_lakes_danao_cebu").change(function(){
		
		  var $checked_lakes_danao_cebu = $( this );
		  var checked_lakes_danao_cebu = $checked_lakes_danao_cebu.attr( "value" );
		  
			if($checked_lakes_danao_cebu.is(":checked")){
				getDanaoLakeCebu();

		}else{
			map.removeLayer(lake_danao_cebu);
		
		}
		  
	});		
	$("#checked_lakes_dapao").change(function(){
		
		  var $checked_lakes_dapao = $( this );
		  var checked_lakes_dapao = $checked_lakes_dapao.attr( "value" );
		  
			if($checked_lakes_dapao.is(":checked")){
				getDapaoLakeLanaodelSur();

		}else{
			map.removeLayer(lake_dapao_lanaodelsur);
		
		}
		  
	});		
	$("#checked_lakes_dapao").change(function(){
		
		  var $checked_lakes_kalibato = $( this );
		  var checked_lakes_kalibato = $checked_lakes_kalibato.attr( "value" );
		  
			if($checked_lakes_kalibato.is(":checked")){
				getKalibatoLakeLaguna();

		}else{
			map.removeLayer(lake_kalibato);
		
		}
		  
	});		
	$("#checked_lakes_laguna").change(function(){
		
		  var $checked_lakes_laguna = $( this );
		  var checked_lakes_laguna = $checked_lakes_laguna.attr( "value" );
		  
			if($checked_lakes_laguna.is(":checked")){
				getLagunaLakeLaguna();

		}else{
			map.removeLayer(lake_laguna);
		
		}
		  
	});	
	$("#checked_lakes_lakewood").change(function(){
		
		  var $checked_lakes_lakewood = $( this );
		  var checked_lakes_lakewood = $checked_lakes_lakewood.attr( "value" );
		  
			if($checked_lakes_lakewood.is(":checked")){
				getLakewoodLakeZamboangadelSur();

		}else{
			map.removeLayer(lake_lakewood);
		
		}
		  
	});	
	$("#checked_lakes_lanao").change(function(){
		
		  var $checked_lakes_lanao = $( this );
		  var checked_lakes_lanao = $checked_lakes_lanao.attr( "value" );
		  
			if($checked_lakes_lanao.is(":checked")){
				getLanaoLakeLanaodelSur();

		}else{
			map.removeLayer(lake_lanao);
		
		}
		  
	});
	$("#checked_lakes_mainit_surigao").change(function(){
		
		  var $checked_lakes_mainit_surigao = $( this );
		  var checked_lakes_mainit_surigao = $checked_lakes_mainit_surigao.attr( "value" );
		  
			if($checked_lakes_mainit_surigao.is(":checked")){
				getMainitLakeSurigaodelNorte();

		}else{
			map.removeLayer(lake_mainit_surigao);
		
		}
		  
	});
	$("#checked_lakes_mainit_agusan").change(function(){
		
		  var $checked_lakes_mainit_agusan = $( this );
		  var checked_lakes_mainit_agusan = $checked_lakes_mainit_agusan.attr( "value" );
		  
			if($checked_lakes_mainit_agusan.is(":checked")){
				getMainitLakeAgusandelNorte();

		}else{
			map.removeLayer(lake_mainit_agusan);
		
		}
		  
	});
	$("#checked_lakes_naujan").change(function(){
		
		  var $checked_lakes_naujan = $( this );
		  var checked_lakes_naujan = $checked_lakes_naujan.attr( "value" );
		  
			if($checked_lakes_naujan.is(":checked")){
				getNaujanLakeOrientalMindoro();

		}else{
			map.removeLayer(lake_naujan);
		
		}
		  
	});
	$("#checked_lakes_palakpakin").change(function(){
		
		  var $checked_lakes_palakpakin = $( this );
		  var checked_lakes_palakpakin = $checked_lakes_palakpakin.attr( "value" );
		  
			if($checked_lakes_palakpakin.is(":checked")){
				getPalakpakinLakeLaguna();

		}else{
			map.removeLayer(lake_palakpakin);
		
		}
		  
	});
	$("#checked_lakes_pandi").change(function(){
		
		  var $checked_lakes_pandi = $( this );
		  var checked_lakes_pandi = $checked_lakes_pandi.attr( "value" );
		  
			if($checked_lakes_pandi.is(":checked")){
				getPandinAndYamboLakeLaguna();

		}else{
			map.removeLayer(lake_pandin_and_yambao);
		
		}
		  
	});
	$("#checked_lakes_paoay").change(function(){
		
		  var $checked_lakes_paoay = $( this );
		  var checked_lakes_paoay = $checked_lakes_paoay.attr( "value" );
		  
			if($checked_lakes_paoay.is(":checked")){
				getPaoayLakeIlocosNorte();

		}else{
			map.removeLayer(lake_paoay);
		
		}
		  
	});
	$("#checked_lakes_sampaloc").change(function(){
		
		  var $checked_lakes_sampaloc = $( this );
		  var checked_lakes_sampaloc = $checked_lakes_sampaloc.attr( "value" );
		  
			if($checked_lakes_sampaloc.is(":checked")){
				getSampalocLakeLaguna();

		}else{
			map.removeLayer(lake_sampalok);
		
		}
		  
	});			
	$("#checked_lakes_taal").change(function(){
		
		  var $checked_lakes_taal = $( this );
		  var checked_lakes_taal = $checked_lakes_taal.attr( "value" );
		  
			if($checked_lakes_taal.is(":checked")){
				getTaalLakeBatangas();

		}else{
			map.removeLayer(lake_taal);
		
		}
		  
	});	
	$("#checked_municipal_waters").change(function(){
		
		  var $checked_municipal_waters = $( this );
		  var checked_municipal_waters = $checked_municipal_waters.attr( "value" );
		  
			if($checked_municipal_waters.is(":checked")){
				getMunicipalWaters();

		}else{
			map.removeLayer(MUNICIPALJSON);
		
		}
		  
	});

	$("#checked_fishing_grounds").change(function(){
		
		  var $checked_fishing_grounds = $( this );
		  var checked_fishing_grounds = $checked_fishing_grounds.attr( "value" );
		  
			if($checked_fishing_grounds.is(":checked")){
				get24FishingGrounds();

		}else{
			map.removeLayer(FISHINGGROUND);
		
		}
		  
	});
	$("#checked_rivers").change(function(){
		
		  var $checked_rivers = $( this );
		  var checked_rivers = $checked_rivers.attr( "value" );
		  
			if($checked_rivers.is(":checked")){
				getRivers();

		}else{
			map.removeLayer(RIVERS);
		
		}
		  
	});
	$("#checked_lakes").change(function(){

		  var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
			if($checked_lakes.is(":checked")){
				getLAKES();

		}else{
			map.removeLayer(LAKES);
		
		}
		  
	});

	$("#checked_bay").change(function(){

		  var $checked_bay = $( this );
		  var checked_bay = $checked_bay.attr( "value" );
		  
			if($checked_bay.is(":checked")){
				getBAYS();
				BAYS.addTo(map);

		}else{
		map.removeLayer(BAYS);
		
		}
		  
	});

	$("#checked_strait").change(function(){
		console.log("#checked_strait");
		  var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
			if($checked_lakes.is(":checked")){
				getStraits();

		}else{
			map.removeLayer(Straits);
		
		}
		  
	});	
	
	$("#checked_channel").change(function(){
		console.log("#checked_channel");
		  var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
			if($checked_lakes.is(":checked")){
				getChannels();

		}else{
			map.removeLayer(Channels);
		
		}
		  
	});	
	
	$("#checked_gulf").change(function(){
		console.log("#checked_gulf");
		  var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
			if($checked_lakes.is(":checked")){
				getGulfs();

		}else{
			map.removeLayer(Gulfs);
		
		}
		  
	});	
	
	$("#checked_sea").change(function(){
		console.log("#checked_sea");
		  var $checked_lakes = $( this );
		  var checked_lakes = $checked_lakes.attr( "value" );
		  
			if($checked_lakes.is(":checked")){
				getSea();

		}else{
			map.removeLayer(Sea);
		
		}
		  
	});
	
	$("#checked_lingayen_gulf").change(function(){
		console.log("#checked_lingayen_gulf");
		  var $checked_lingayen_gulf = $( this );
		  var checked_lingayen_gulf = $checked_lingayen_gulf.attr( "value" );
		  
			if($checked_lingayen_gulf.is(":checked")){
				getLingayen_Gulf();

		}else{
			map.removeLayer(Lingayen_Gulf);
		
		}
		  
	});


	$("#checked_moro_gulf").change(function(){
		
		  var $checked_moro_gulf = $( this );
		  var checked_moro_gulf = $checked_moro_gulf.attr( "value" );
		  
			if($checked_moro_gulf.is(":checked")){
				getMoro_Gulf();

		}else{
			map.removeLayer(Moro_Gulf);
		
		}
		  
	});


	$("#checked_davao_gulf").change(function(){
		
		  var $checked_davao_gulf = $( this );
		  var checked_davao_gulf = $checked_davao_gulf.attr( "value" );
		  
			if($checked_davao_gulf.is(":checked")){
				getDavao_Gulf();

		}else{
			map.removeLayer(Davao_Gulf);
		
		}
		  
	});


	$("#checked_samar_gulf").change(function(){
		
		  var $checked_samar_gulf = $( this );
		  var checked_samar_gulf = $checked_samar_gulf.attr( "value" );
		  
			if($checked_samar_gulf.is(":checked")){
				getSamar_Sea();

		}else{
			map.removeLayer(Samar_Sea);
		
		}
		  
	});

	$("#checked_sibuyan_gulf").change(function(){
		
		  var $checked_sibuyan_gulf = $( this );
		  var checked_sibuyan_gulf = $checked_sibuyan_gulf.attr( "value" );
		  
			if($checked_sibuyan_gulf.is(":checked")){
				 getSibuyan_Sea();

		}else{
			map.removeLayer(Sibuyan_Sea);
		
		}
		  
	});

	$("#checked_camotes_gulf").change(function(){
		
		  var $checked_camotes_gulf = $( this );
		  var checked_camotes_gulf = $checked_camotes_gulf.attr( "value" );
		  
			if($checked_camotes_gulf.is(":checked")){
				getCamotes_Sea();

		}else{
			map.removeLayer(Camotes_Sea);
		
		}
		  
	});

	$("#checked_visayan_gulf").change(function(){
		
		  var $checked_visayan_gulf = $( this );
		  var checked_visayan_gulf = $checked_visayan_gulf.attr( "value" );
		  
			if($checked_visayan_gulf.is(":checked")){
				getVisayan_Sea();

		}else{
			map.removeLayer(Visayan_Sea);
		
		}
	});	

	$("#checked_guimaras_gulf").change(function(){
		
		  var $checked_guimaras_gulf = $( this );
		  var checked_guimaras_gulf = $checked_guimaras_gulf.attr( "value" );
		  
			if($checked_guimaras_gulf.is(":checked")){
				getGuimaras_Strait();

		}else{
			map.removeLayer(Guimaras_Strait);
		
		}
		  
	});

	$("#checked_bohol_gulf").change(function(){
		
		  var $checked_bohol_gulf= $( this );
		  var checked_bohol_gulf = $checked_bohol_gulf.attr( "value" );
		  
			if($checked_bohol_gulf.is(":checked")){
				getBohol_Sea();

		}else{
			map.removeLayer(Bohol_Sea);
		
		}
		  
	});

	$("#checked_ragay_gulf").change(function(){
		
		  var $checked_ragay_gulf = $( this );
		  var checked_ragay_gulf = $checked_ragay_gulf.attr( "value" );
		  
			if($checked_ragay_gulf.is(":checked")){
				getRagay_Gulf();

		}else{
			map.removeLayer(Ragay_Gulf);
		
		}
		  
	});

	$("#checked_leyte_gulf").change(function(){
		
		  var $checked_leyte_gulf = $( this );
		  var checked_leyte_gulf = $checked_leyte_gulf.attr( "value" );
		  
			if($checked_leyte_gulf.is(":checked")){
				getLeyte_Gulf();

		}else{
			map.removeLayer(Leyte_Gulf);
		
		}
		  
	});

	$("#checked_manila_bay").change(function(){
		
		  var $checked_manila_bay = $( this );
		  var checked_manila_bay = $checked_manila_bay.attr( "value" );
		  
			if($checked_manila_bay.is(":checked")){
				getManila_Bay();

		}else{
			map.removeLayer(Manila_Bay);
		
		}
		  
	});

	$("#checked_lagonoy_gulf").change(function(){
		
		  var $checked_lagonoy_gulf = $( this );
		  var checked_lagonoy_gulf = $checked_lagonoy_gulf.attr( "value" );
		  
			if($checked_lagonoy_gulf.is(":checked")){
				getLagonoy_Gulf();

		}else{
			map.removeLayer(Lagonoy_Gulf);
		
		}
		  
	});

	$("#checked_lamon_bay").change(function(){
		
		  var $checked_lamon_bay = $( this );
		  var checked_lamon_bay = $checked_lamon_bay.attr( "value" );
		  
			if($checked_lamon_bay.is(":checked")){
				getLamon_Bay();

		}else{
			map.removeLayer(Lamon_Bay);
		
		}
		  
	});

	$("#checked_casiguran_sound").change(function(){
		
		  var $checked_casiguran_sound = $( this );
		  var checked_casiguran_sound = $checked_casiguran_sound.attr( "value" );
		  
			if($checked_casiguran_sound.is(":checked")){
				getCasiguran_Sound();

		}else{
			map.removeLayer(Casiguran_Sound);
		
		}
		  
	});

	$("#checked_palanan_bay").change(function(){
		
		  var $checked_palanan_bay = $( this );
		  var checked_palanan_bay = $checked_palanan_bay.attr( "value" );
		  
			if($checked_palanan_bay.is(":checked")){
				getPalanan_Bay();

		}else{
			map.removeLayer(Palanan_Bay);
		
		}
		  
	});


	$("#checked_babuyan_channel").change(function(){
		
		  var $checked_babuyan_channel = $( this );
		  var checked_babuyan_channel = $checked_babuyan_channel.attr( "value" );
		  
			if($checked_babuyan_channel.is(":checked")){
				getBabuyan_Channel();

		}else{
			map.removeLayer(Babuyan_Channel);
		
		}
		  
	});


	$("#checked_batangas_coast").change(function(){
		
		  var $checked_batangas_coast = $( this );
		  var checked_batangas_coast = $checked_batangas_coast.attr( "value" );
		  
			if($checked_batangas_coast.is(":checked")){
				getBatangas_Coast();

		}else{
			map.removeLayer(Batangas_Coast);
		
		}
		  
	});


	$("#checked_tayabas_bay").change(function(){
		
		  var $checked_tayabas_bay = $( this );
		  var checked_tayabas_bay = $checked_tayabas_bay.attr( "value" );
		  
			if($checked_tayabas_bay.is(":checked")){
				getTayabas_Bay();

		}else{
			map.removeLayer(Tayabas_Bay);
		
		}
		  
	});

	$("#checked_wpalawan_waters").change(function(){
		
		  var $checked_wpalawan_waters = $( this );
		  var checked_wpalawan_waters = $checked_wpalawan_waters.attr( "value" );
		  
			if($checked_wpalawan_waters.is(":checked")){
				getW_Palawan_waters();

		}else{
			map.removeLayer(W_Palawan_waters);
		
		}
		  
	});


	$("#checked_cuyo_pass").change(function(){
		
		  var $checked_cuyo_pass = $( this );
		  var checked_cuyo_pass = $checked_cuyo_pass.attr( "value" );
		  
			if($checked_cuyo_pass.is(":checked")){
				getCuyo_Pass();

		}else{
			map.removeLayer(Cuyo_Pass);
		
		}
		  
	});


	$("#checked_wsulu_sea").change(function(){
		
		  var $checked_wsulu_sea = $( this );
		  var checked_wsulu_sea = $checked_wsulu_sea.attr( "value" );
		  
			if($checked_wsulu_sea.is(":checked")){
				getW_Sulu_Sea();

		}else{
			map.removeLayer(W_Sulu_Sea);
		
		}
		  
	});


	$("#checked_ssulu_sea").change(function(){
		
		  var $checked_ssulu_sea = $( this );
		  var checked_ssulu_sea = $checked_ssulu_sea.attr( "value" );
		  
			if($checked_ssulu_sea.is(":checked")){
				getS_Sulu_Sea();

		}else{
			map.removeLayer(S_Sulu_Sea);
		
		}
		  
	});


	$("#checked_esulu_sea").change(function(){
		
		  var $checked_esulu_sea = $( this );
		  var checked_esulu_sea = $checked_esulu_sea.attr( "value" );
		  
			if($checked_esulu_sea.is(":checked")){
				getE_Sulu_Sea();

		}else{
			map.removeLayer(E_Sulu_Sea);
		
		}
		  
	});

	check_all = $('#FMA');
	// identify the other checkboxes in the group
	check_class = $('.FMA');
	check_all.on('click',function(){
	  check_class.click();
	});
	
	major24_fg_all = $('#major24_fg');
	//identify the other checkboxes in the group
	major24_fg_class = $('.FG');
	major24_fg_all.on('click',function(){
	major24_fg_class.click();
	});


	
	//POST HARVEST

	//FISH LANDING
	
	//for home page only


}

var philStyle = {
	      color: '#ffffff',
	      weight: 2,
	      opacity: 0.5,
	      fillOpacity: 0.5,
	      fillColor: '#ffffff'
	    };
	    
	var regionStyle = {  
		weight: 1,
        color: '#fff',
        dashArray: '',
        fillOpacity: 0.9,
        fillColor: 'red'
        };
function setStyleColorPhilMap() {
	
	return {
		      color: '#ffffff',
		      weight: 2,
		      opacity: 0.5,
		      fillOpacity: 0.5,
		      fillColor: '#ffffff'
		    };
}
function setStyleColorFLA() {
	
	return {
		      color: 'red',
		      weight: 2,
		      opacity: 0.5,
		      fillOpacity: 0.5,
		      fillColor: 'red'
		    };
}  
	function setStyleColor(color) {
	
	return {
		      color: color,
		      weight: 2,
		      opacity: 0.5,
		      fillOpacity: 0.5,
		      fillColor: color
		    };
} 

var geojsonMarkerOptions = {
		radius: 8,
		fillColor: "#ff7800",
		color: "#000",
		weight: 1,
		opacity: 1,
		fillOpacity: 0.8
		};






function getClassifiedStyle(num, min, max) {
// D3 Quantize to classfy into 3 range!
var quantize = d3.scale.quantize().domain([min, max]).range(['low', 'middle', 'high']);
var range = quantize(num);
// 3 types of highlight color
var color;
var style;
switch (range) {
  case 'low': {
    color = '#FFEB3B';
    break;
  }
  case 'middle': {
    color = '#4CAF50';
    break;
  }
  case 'high': {
    color = '#f44336';
    break;
  }
  default: {
    color = '#ffffff';
  }
}
style = {
  color: '#4CAF50',
  weight: 5,
  opacity: 0.6,
  fillOpacity: 0.65,
  fillColor: '#4CAF50'
};
return style;
}

var defaultStyle = {
  color: '#ffffff',
  weight: 2,
  opacity: 1,
  fillOpacity: 0.7,
  fillColor: '#ffffff'
};

function style(feature) {
    return {
        fillColor: 'green', 
        fillOpacity: 0.5,  
        weight: 2,
        opacity: 1,
        color: '#ffffff',
        dashArray: '3'
    };
}

	var highlight = {
		'fillColor': 'yellow',
		'weight': 2,
		'opacity': 1
	};


		
 function polySelect(a){
  		console.log("polySelect(a): " + a);
			map._layers[a].fire('click'); 
			var layer = map._layers[a];
			map.fitBounds(layer.getBounds());
        }
  
 var geojsonMarkerOptions = {
			radius: 8,
			fillColor: "#ff7800",
			color: "#000",
			weight: 1,
			opacity: 1,
			fillOpacity: 0.8
			};
 Array.prototype.remove = function() {
	    var what, a = arguments, L = a.length, ax;
	    while (L && this.length) {
	        what = a[--L];
	        while ((ax = this.indexOf(what)) !== -1) {
	            this.splice(ax, 1);
	        }
	    }
	    return this;
	};
/*$.get('http://localhost:8899/api/all', function(content, status) {
	dataAPI = content;
	console.log(dataAPI);
});
*/

function MapResources(feature, layer){
	       		layer.bindPopup(feature.properties.NAME +"," + feature.properties.PRV_NM, {closeButton: false});
	       		layer.on({

    // mouseover: highlightFeature,
    // mouseout: resetHighlight,

    click: function(e){
		
      if (prevLayerClicked !== null) {
          
        prevLayerClicked.setStyle(philStyle);
      }
   //  map.fitBounds(e.target.getBounds(),7);
  //   map.setView(e.target.getBounds().getCenter());
   //  map.setCenter(e.target.getCenter());
   //  map.setZoom(8);
    
      var layer = e.target;
      layer.setStyle({
        weight: 1,
        color: '#fff',
        dashArray: '',
        fillOpacity: 0.9,
        fillColor: 'red'
      });
      if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
      }
      prevLayerClicked = layer;
    }
  });
		  	
}


/*					fetch("/static/FMA/Bathymetry.geojson").then(res => res.json()).then(data => {
					  	L.geoJson(data,{
					  					       				
					  	onEachFeature: function (feature, layer) {
					  		if(layer.feature.properties.DEPTH == '0') {    
					  			depth_1000.addLayer(layer.setStyle(setStyleColor('#a9edff')));
					  			
					  		  }
					  		
					  		if(layer.feature.properties.DEPTH == '0') {  
					  			console.log(layer.feature.properties.DEPTH == '0');
					  			depth_2000.addLayer(layer.setStyle(setStyleColor('#00a9d4')));
					  			
					  		  }
					  		if(layer.feature.properties.DEPTH == '2000') {    
					  			depth_3000.addLayer(layer.setStyle(setStyleColor('#0082a3')));
					  			
					  		  }
					  		if(layer.feature.properties.DEPTH == '3000') {    
					  			depth_5000.addLayer(layer.setStyle(setStyleColor('#005b72')));
					  			
					  		  }
					  		if(layer.feature.properties.DEPTH == '7000') {    
					  			depth_10000.addLayer(layer.setStyle(setStyleColor('#003441')));
					  			
					  		  }

					  	}, 			     	   	       				
					  	});

					});
*/
/*
					fetch("/static/geojson/Bicol_1.geojson").then(res => res.json()).then(data => {
					  	L.geoJson(data,{
					  					       				
					  	onEachFeature: function (feature, layer) {
					  		//philippineMap.addLayer(layer.setStyle(setStyleColorPhilMap('#B1D4E0')));
					  		philippineMap.addLayer(layer.setStyle(philStyle));
					  		(function () {
								  if(dataAPI[5].id === 120){
									  name = dataAPI[5].username;
								  }
	       						layer.bindPopup(feature.properties.NAME +"," + feature.properties.PRV_NM + name,{closeButton: false});
	       			          })(layer, feature.properties);
					  	}, 			     	   	       				
					  	});
					  
					});*/