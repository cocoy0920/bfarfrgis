


var region_map;
var set_center;
var role;
var mapsData;

function getMapDetails(region_mapValue,set_centerValue,roleValue,mapsDataValue){
	
	region_map = region_mapValue;
	set_center = set_centerValue;
	role = roleValue;
	mapsData = mapsDataValue;
}


//var url = '/frgis/static/geojson/'+ ${region_map} +'.geojson'; 
var url = '/frgis/static/geojson/'+ region_map +'.geojson'; 
//var arr = [];
//var arr1 = [];
//var region = ${region_map};
var region = region_map
console.log(region);
 //var map_location = ${mapsData}; 
 var map_location = mapsData
 // var map_location;
 /*
 function getJSONData(){
  $.getJSON("data", function(data) {
	console.log(data);
	map_location = data;								
	});
	}
	window.onload = getJSONData;
*/	
localStorage.setItem('regionmap', JSON.stringify(url));
const map_data = JSON.parse(localStorage.getItem('regionmap'));
  // Initialize autocomplete with empty source.
 // $( "#autocomplete" ).autocomplete(); zoomControl: false,
	
//var map =L.map('map',{scrollWheelZoom:false}).setView([${set_center}],6);
var map =L.map('map',{scrollWheelZoom:false}).setView([set_center],6);

  var OpenTopoMap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
    opacity: 0.90
  });
  
  OpenTopoMap.addTo(map);

	map.zoomControl.setPosition('topright');
	var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
				maxZoom: 18,
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'});
	

	//osm.addTo(map);
	// https: also suppported.
	 var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
	});
	
	// https: also suppported.
	var Esri_WorldGrayCanvas = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'});
		
	var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {	
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
	});
	
	var roadsMap = L.tileLayer.wms('https://geo.bfar.da.gov.ph/roads/gwc/service/wms?',{
	layers:"roads:roads",
	attribution: 'FRGIS'});
	
L.control.scale().addTo(map);

function style(feature) {
    return {
        fillColor: 'green', 
        fillOpacity: 0.5,  
        weight: 2,
        opacity: 1,
        color: '#ffffff',
        dashArray: '3'
    };
}

	var highlight = {
		'fillColor': 'yellow',
		'weight': 2,
		'opacity': 1
	};

	
		function forEachFeature(feature, layer) {
            layer._leaflet_id = feature.properties.REGION;
            var popupContent = "<p><b>REGION: </b>" + feature.properties.REG_NM + "(" + feature.properties.REG_VAR_NM +")" + "</br><b>PROVINCE: </b>" + feature.properties.PRV_NM + "</br><b>MUNICIPALITY: </b>" + feature.properties.NAME +'</p>';

            layer.bindPopup(popupContent);
            
            layer.on("click", function (e) { 
                stateLayer.setStyle(style);
                layer.setStyle(highlight);
                
            }); 
            
		}
/*
var stateLayer = L.geoJson(null, {onEachFeature: forEachFeature, style: style});

	$.getJSON(map_data, function(data) {
	
        stateLayer.addData(data);

    });

 stateLayer.addTo(map);
 */
  		function polySelect(a){
  		console.log("polySelect(a): " + a);
			map._layers[a].fire('click'); 
			var layer = map._layers[a];
			map.fitBounds(layer.getBounds());
        }
        var geojsonLayer = new L.GeoJSON.AJAX(map_data);
// var geojsonLayer = new L.GeoJSON.AJAX("/frgis/static/geojson/Bicol.geojson");  
//  geojsonLayer.refilter(function(feature){
 //   return feature.properties.key === values;
//});
     
geojsonLayer.addTo(map);       
        
var baseMaps = {
    "Open Street Map": osm,
   	"OSM B&W":OpenStreetMap_BlackAndWhite,
   	"Esri WorldI magery":Esri_WorldImagery,
   	"Esri World Gray Canvas":Esri_WorldGrayCanvas,
   	"ROADS MAP":roadsMap
   	
};

var overlayMaps = {
   "PHILIPPINES":geojsonLayer
};	
	
L.control.layers(baseMaps, overlayMaps).addTo(map);
//L.control.layers(baseMaps).addTo(map);	
		var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});

  		  var fishSancturiesGroup = new Array();
  		  var fishSancturiesGroup = new Array();
          var fishprocessingplantsLayerGroup  = new Array();
          var fishlandingLayerGroup  = new Array();
          var fishpenLayerGroup  = new Array();
          var fishcageLayerGroup  = new Array();
          var fishpondLayerGroup  = new Array();
          var hatcheryLayerGroup  = new Array();
          var fishcageLayerGroup  = new Array();
          var iceplantLayerGroup  = new Array();
          var fishcoralLayerGroup  = new Array();
          var fishportLayerGroup  = new Array();
          var marineProtectedLayerGroup  = new Array();
          var marketLayerGroup  = new Array();
          var schoolOfFisheriesLayerGroup  = new Array();
          var seagrassLayerGroup  = new Array();
          var seaweedsLayerGroup  = new Array();
          var mangroveLayerGroup  = new Array();
          var lguLayerGroup  = new Array();
          var fisheriesTrainingLayerGroup  = new Array();
          var maricultureParkLayerGroup  = new Array();

     
	$('#fishs').click(function() {
	var table = "<table>" +  
		"<thead><tr>" +
      	"<th>Province</th>" +
		"</tr>" +
    	"</thead>" + 		
		"<tbody>"+
    	"</tbody></table>";
	/* $.getJSON("getTotalFS", function(data) {
			console.log("getTotalFS: " +data);
			var municipal_data;
			html="";
			municipal_html="";
				for ( var i = 0; i < data.length; i++) {
					var obj = data[i];
					html = html+ "<tr><td>" +obj.province + "</td></tr>";
					
					municipal_data = obj.municipal;
						for ( var x = 0; x < municipal_data.length; x++) {
							var municipal = municipal_data[x];
							html = html+ "<tr><td>&nbsp</td><td>"+municipal.municipal+"</td><td>"+municipal.totalmunicipal+"</td></tr>";
							
						}
				}
	 		$('#view_user').append(table); 
     		$('#view_user table tbody').append(html);	
     						
	});*/
		if ($('#fishs').is(":checked")) {
			$(".se-pre-con").show();
			/*$.getJSON("data", function(data) {
				console.log(data);
				map_location = data;								
			});
			*/
			for ( var i = 0; i < map_location.length; i++) {
					var obj = map_location[i];
					var fishsanctuaries;	
				if (obj.page == "fishsanctuaries") {
					lon = obj.lon;
					lat = obj.lat;
					icon = obj.icon;
					html = obj.html;
					var image = obj.image;
					var latlon = lat + ',' + lon;
					var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
					uniqueKey =  obj.uniqueKey;     
			
		    		if (role == 'ROLE_USER') {
						var edit = '<div class="fishsanctuaries_id"><a href="#" id="' + uniqueKey + '"> <img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
				
		    		} else {
						var edit = '';
					}
		    
		    		var greenIcon = new LeafIcon({iconUrl: icon});
		    		var LamMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
						console.log(e);
						onMapClick(this.getLatLng());
						//$('#details').html(html);
				//getCoord(this.getLatLng());
					});
					LamMarker.getPopup().on('remove', function() {
   						 console.log("REMOVE");
   						 $('#details').empty();
							clearClickMark();
							$(".se-pre-con").hide();
					});

					
		    fishSancturiesGroup.push(LamMarker);
			
			}
		}
	
							//report("getTotalFS");
							$('#details').empty();
							clearClickMark();
							$(".se-pre-con").hide();
						} else {
							markerFishSancturiesDel();
							$('#total').find('tbody').empty();
							$('#details').empty();
							clearClickMark();
						}

	});
	
$('#fishprocessingplants').click(function() {
						if ($('#fishprocessingplants').is(":checked")) {
							$(".se-pre-con").show();
							
							for ( var i = 0; i < map_location.length; i++) {		 
								var obj = map_location[i];
			if (obj.page == "fishprocessingplants") {
			console.log("HTML: " + obj.html);
 	 							lon = obj.lon;
								lat = obj.lat;
								icon = obj.icon;
								html = obj.html;
								uniqueKey =  obj.uniqueKey;  
								var latlon = lat + ',' + lon;
								image = obj.image;
								var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
								if (role == 'ROLE_USER') {
									var edit = '<div class="fishprocessingplants_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
								} else {
									var edit = '';
								}
        		
        		    				var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        							var fishprocessingplantsMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    								onMapClick(this.getLatLng());
    				//getCoord(this.getLatLng());
    								});
    								
    								fishprocessingplantsMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
									});
    								
    								
        							fishprocessingplantsLayerGroup.push(fishprocessingplantsMarker);
        		}
	 							}		
							$('#details').empty();
							clearClickMark();
							$(".se-pre-con").hide();
						} else {
							markerFishProcessingDel();
							$('#total').find('tbody').empty();
							$('#details').empty();
							clearClickMark();
						}

					});

$('#fishlanding').click(function() {
	if ($('#fishlanding').is(":checked")) {
		$(".se-pre-con").show();
				for ( var i = 0; i < map_location.length; i++) {				 
					var obj = map_location[i];	
					if (obj.page == "fishlanding") {
 	 					lon = obj.lon;
						lat = obj.lat;
						icon = obj.icon;
						html = obj.html;
						uniqueKey =  obj.uniqueKey;      		   

        				var latlon = lat + ',' + lon;
        				image = obj.image;
        				var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
    						if (role == 'ROLE_USER') {
    							var edit = '<div class="fishlanding_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    						} else {
							var edit = '';
							}
				
    		    		var greenIcon = new LeafIcon({iconUrl: icon});
    					var fishlandingMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {

						onMapClick(this.getLatLng());

						});
						
						fishlandingMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});
    						fishlandingLayerGroup.push(fishlandingMarker);
          				}
					}
				
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();
	} else {
			markerFishlandingDel();
			$('#total').find('tbody').empty();
			$('#details').empty();
			clearClickMark();
	}

});

$('#fishpen').click(function() {
	if ($('#fishpen').is(":checked")) {
		$(".se-pre-con").show();
		for ( var i = 0; i < map_location.length; i++) {		 
				var obj = map_location[i];
			if (obj.page == "fishpen") {	
 	 			lon = obj.lon;
				lat = obj.lat;
				icon = obj.icon;
				html = obj.html;
				image = obj.image;
				uniqueKey =  obj.uniqueKey;  
		
        		var latlon = lat + ',' + lon;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="fishpen_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';    			
    		    } else {
					var edit = '';
				}
				   		   
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    			var fishpenMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {		    	
					onMapClick(this.getLatLng());
				});
				
						fishpenMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});				
    		fishpenLayerGroup.push(fishpenMarker);
    		}
		 }
		 
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();
	} else {
		markerFishPenDel();
		$('#total').find('tbody').empty();
		$('#details').empty();
		clearClickMark();
	}

});

$('#fishcage').click(function() {
	if ($('#fishcage').is(":checked")) {
		$(".se-pre-con").show();

		for ( var i = 0; i < map_location.length; i++) {		 
				var obj = map_location[i];
			if (obj.page == "fishcage") {		
 	 			lon = obj.lon;
				lat = obj.lat;
				icon = obj.icon;
				html = obj.html;
				uniqueKey =  obj.uniqueKey;      		   
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        			if (role == 'ROLE_USER') {
    					var edit = '<div class="fishcage_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    				} else {
							var edit = '';
					}    		   
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    			var fishcageMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {		    	
					onMapClick(this.getLatLng());
					}); 
					
				fishcageMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});	
							   		
    			fishcageLayerGroup.push(fishcageMarker);
    		}	
		 }
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();
	} else {
		markerFishCageDel();
		$('#total').find('tbody').empty();
		$('#details').empty();
		clearClickMark();
	}

});

$('#fishpond').click(function() {
	if ($('#fishpond').is(":checked")) {
		$(".se-pre-con").show();
		for ( var i = 0; i < map_location.length; i++) {		 
			var obj = map_location[i];
		if (obj.page == "fishpond") {
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	var latlon = lat + ',' + lon;
        	image = obj.image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="fishpond_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
				
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    			var fishpondMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {		    	
					onMapClick(this.getLatLng());
				});
				
				fishpondMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});	
						
						
    		fishpondLayerGroup.push(fishpondMarker);
        	}	
			}
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();			
	} else {
		markerFishPondDel();
		$('#total').find('tbody').empty();
		$('#details').empty();
		clearClickMark();
	}
});
$('#hatchery').click(function() {
	if ($('#hatchery').is(":checked")) {
		$(".se-pre-con").show();
		for ( var i = 0; i < map_location.length; i++) {		 
			var obj = map_location[i];
			
		if (obj.page == "hatchery") {
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	var latlon = lat + ',' + lon;
        	image = obj.image;
        	var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="hatchery_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
				
    		    var greenIcon = new LeafIcon({iconUrl: icon});   		    
    		    var hatcheryMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    				onMapClick(this.getLatLng());
    				});
    				
    				hatcheryMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});	
    		hatcheryLayerGroup.push(hatcheryMarker);
        	}	
		}
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();
	} else {
		markerHatcheryDel();
		$('#total').find('tbody').empty();
		$('#details').empty();
		clearClickMark();
	}

});

$('#iceplantorcoldstorage').click(function() {
	 if ($('#iceplantorcoldstorage').is(":checked")) {
		 $(".se-pre-con").show();
 			for ( var i = 0; i < map_location.length; i++) {		 
			var obj = map_location[i];
		if (obj.page == "iceplantorcoldstorage") {
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="iceplantorcoldstorage_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			
    		   } else {
							var edit = '';
				}
    		   var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var iceplantorcoldstorageMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {    		    	
    				onMapClick(this.getLatLng());
    				});
    				
    				iceplantorcoldstorageMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});
						
						
    		iceplantLayerGroup.push(iceplantorcoldstorageMarker);
    	}	
		}
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();
	 } else {
		 markerIcePlantDel();
		 $('#total').find('tbody').empty();
		$('#details').empty();
		clearClickMark();
	 }

 });
 
 $('#FISHPORT').click(function() {
	if ($('#FISHPORT').is(":checked")) {
		$(".se-pre-con").show();
		
		for ( var i = 0; i < map_location.length; i++) {		 
				var obj = map_location[i];
		if (obj.page == "FISHPORT") {	

 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="FISHPORT_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
    		   
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var FISHPORTMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    				onMapClick(this.getLatLng());
    				
    				});
    				
    		FISHPORTMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});	
							
    		fishportLayerGroup.push(FISHPORTMarker);
    		}
			}
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();					
	} else {
	  markerFishPortDel();
	 $('#total').find('tbody').empty();
	 $('#details').empty();
		clearClickMark();
	}

});

$('#fishcorals').click(function() {
	if ($('#fishcorals').is(":checked")) {
		$(".se-pre-con").show();
			for ( var i = 0; i < map_location.length; i++) {		 
			var obj = map_location[i];
		if (obj.page == "fishcorals") {
				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="fishcorals_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
    		   
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var fishcorralsMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {   		    	
    				onMapClick(this.getLatLng());
    				});
    				
    			fishcorralsMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});	
							
    		fishcoralLayerGroup.push(fishcorralsMarker);
    		}
		}
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();
	} else {
		markerFishcorralDel();
		$('#total').find('tbody').empty();
		$('#details').empty();
		clearClickMark();
	}

});

$('#market').click(function() {
	if ($('#market').is(":checked")) {
		$(".se-pre-con").show();
		for ( var i = 0; i < map_location.length; i++) {		 
				var obj = map_location[i];
		if (obj.page == "market") {	
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
           		var latlon = lat + ',' + lon;
           		image = obj.image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="market_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
    		    
    		    var greenIcon = new LeafIcon({iconUrl: icon});   		    
    		    var marketMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    				onMapClick(this.getLatLng());

    				});
    				
    		marketMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});	
									
    		marketLayerGroup.push(marketMarker);
        	}	
			}
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();
	} else {
		markerMarketDel();
		$('#total').find('tbody').empty();
		$('#details').empty();
		clearClickMark();
	}
});

$('#schooloffisheries').click(function() {
console.log("School of Fisheries");
	if ($('#schooloffisheries').is(":checked")) {
		$(".se-pre-con").show();
		for ( var i = 0; i < map_location.length; i++) {		 
				var obj = map_location[i];
		if (obj.page == "schooloffisheries") {		
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	var latlon = lat + ',' + lon;
           		image = obj.image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="schooloffisheries_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';    			
    		   }else{
					var edit = '';
				}    		    
    		    var greenIcon = new LeafIcon({iconUrl: icon});    		    
    		    var schooloffisheriesMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {    		    
    				onMapClick(this.getLatLng());
    				});
    				
    		schooloffisheriesMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});	
									
    		schoolOfFisheriesLayerGroup.push(schooloffisheriesMarker);
    		}
			}
		
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();	
	} else {
	    markerSchoolOfFisheriesDel();
		$('#total').find('tbody').empty();
		$('#details').empty();
		clearClickMark();
	}
});

$('#seagrass').click(function() {
	if ($('#seagrass').is(":checked")) {
		$(".se-pre-con").show();
		
		for ( var i = 0; i < map_location.length; i++) {		 
				var obj = map_location[i];
		if (obj.page == "seagrass") {			
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
           		var latlon = lat + ',' + lon;
           		image = obj.image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="seagrass_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
					var edit = '';
				}   		    
    		    var greenIcon = new LeafIcon({iconUrl: icon});    		    
    		    var seagrassMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    				onMapClick(this.getLatLng());
    			});
    			
    			seagrassMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});	
							
    				seagrassLayerGroup.push(seagrassMarker);
        	}	
			}
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();
	}else{
		markerSeaGrassDel();
		$('#total').find('tbody').empty();
		$('#details').empty();
		clearClickMark();
	}
});

$('#seaweeds').click(function() {
	if ($('#seaweeds').is(":checked")) {
		$(".se-pre-con").show();

		for ( var i = 0; i < map_location.length; i++) {		 
				var obj = map_location[i];
			if (obj.page == "seaweeds") {		
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	var latlon = lat + ',' + lon;
        	image = obj.image;
        	var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="seaweeds_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
					var edit = '';
				}				   		   
    		    var greenIcon = new LeafIcon({iconUrl: icon});   		    
    			var seaweedsMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    				onMapClick(this.getLatLng());
    			});
    			
    			seaweedsMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});	
						
						
    			seaweedsLayerGroup.push(seaweedsMarker);
    			}
			}
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();
	}else{
		markerSeaWeedsDel();
		$('#total').find('tbody').empty();
		$('#details').empty();
		clearClickMark();
	}
});

$('#mangrove').click(function() {
	if ($('#mangrove').is(":checked")) {
		$(".se-pre-con").show();		
		for ( var i = 0; i < map_location.length; i++) {		 
				var obj = map_location[i];
		if (obj.page == "mangrove") {			
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	var latlon = lat + ',' + lon;
        	image = obj.image;
        	var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="mangrove_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			}else{
					var edit = '';
				}    		   
    		    var greenIcon = new LeafIcon({iconUrl: icon});    		    
    		    var mangroveMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    				onMapClick(this.getLatLng());
    			});
    			
    			mangroveMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});	
						
						
    		mangroveLayerGroup.push(mangroveMarker);
    		}
			}
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();
	}else{
		markerMangroveDel();
		$('#total').find('tbody').empty();
		$('#details').empty();
		clearClickMark();
	}
});	

$('#lgu').click(function() {
	if ($('#lgu').is(":checked")) {
		$(".se-pre-con").show();
		for ( var i = 0; i < map_location.length; i++) {		 
				var obj = map_location[i];
		if (obj.page == "lgu") {		
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	var latlon = lat + ',' + lon;
        	image = obj.image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="lgu_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
    		    
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var lguMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    				onMapClick(this.getLatLng());
    				});
    				
    				lguMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});	
						
    		lguLayerGroup.push(lguMarker);
    		}
			}
		
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();
	}else{
		markerLGUDel();
		$('#total').find('tbody').empty();
		$('#details').empty();
		clearClickMark();
	}
});

$('#trainingcenter').click(function() {
	if ($('#trainingcenter').is(":checked")) {
		$(".se-pre-con").show();
		
		for ( var i = 0; i < map_location.length; i++) {		 
				var obj = map_location[i];
		if (obj.page == "trainingcenter") {	
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		 
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="trainingcenter_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
					var edit = '';
				}  		    
    		    var greenIcon = new LeafIcon({iconUrl: icon});   		    
    		    var trainingcenterMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    				onMapClick(this.getLatLng());
    			});
    			
    			trainingcenterMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});	
						
    		fisheriesTrainingLayerGroup.push(trainingcenterMarker);
        	}	
			}
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();
	}else{
		markerFisheriesTrainingDel();
		$('#total').find('tbody').empty();
		$('#details').empty();
		clearClickMark()
	}
});	

$('#mariculturezone').click(function() {
	if ($('#mariculturezone').is(":checked")) {
		$(".se-pre-con").show();
		
		for ( var i = 0; i < map_location.length; i++) {		 
				var obj = map_location[i];
			if (obj.page == "mariculturezone") {	

 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="mariculturezone_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
    		    
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var mariculturezoneMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    				onMapClick(this.getLatLng());
    				});
    				
    			mariculturezoneMarker.getPopup().on('remove', function() {
   						 				console.log("REMOVE");
   						 				$('#details').empty();
										clearClickMark();
										$(".se-pre-con").hide();
						});	
						
    		maricultureParkLayerGroup.push(mariculturezoneMarker);
    		}
			}
		$('#details').empty();
		clearClickMark();
		$(".se-pre-con").hide();
	}else{
		markerMaricultureParkDel();
		$('#total').find('tbody').empty();
		$('#details').empty();
		clearClickMark()
	}
});
	
	function clearClickMark() {
    
		if (clickmark != undefined) {
			  map.removeLayer(clickmark);
			};
   
	}

  //var role = ${role}; 
  var role = role; 
  /*
  $.getJSON("data", function(data) {
	console.log(data);
	map_location = data;								
	});
*/

/*
var role = 	${role};
     function refreshRecord(value,link,status)
    {
	
       data = value;
		page = link;

			if(page == "fishsanctuaries"){	
			//viewListOnMap.js				
		fishsanctuariesGetListMap(data,role);  	
				
 	 		}
 	 		if(page == "fishprocessingplants"){
 	 			fishprocessingplantsGetListMap(data,role); 	
      				
 	 		}
		 	if(page == "fishlanding"){
				fishlandingGetListMap(data,role);         		
 	 		}   
 	 	 	if(page == "fishpen"){
 	 	 		fishpenGetListMap(data,role);
 	 		} 

			if(page == "fishcage"){
				fishcageGetListMap(data,role);
			} 
 	 	 	if(page == "fishpond"){
 				fishpondGetListMap(data,role);
 			} 	 	        		
        	if(page == "hatchery"){
 	 			hatcheryGetListMap(data,role);
 	 		}
 	 	 	if(page == "iceplantorcoldstorage"){
 	 			iceplantorcoldstorageGetListMap(data,role);
 	 		}
 	 		if(page == "fishcorals"){
 	 			fishcorralsGetListMap(data,role);
 	 		}  
 	 		if(page == "FISHPORT"){
 	 			FISHPORTGetListMap(data,role);
 	 		}   
 	 		if(page == "marineprotected"){
 	 			marineprotectedGetListMap(data,role);
 	 		} 
 	 		if(page == "market"){ 
 	 			marketGetListMap(data,role);
 	 		} 
 	 		if(page == "schooloffisheries"){ 
 	 			schooloffisheriesGetListMap(data,role);
 	 		} 
 	 		if(page == "seagrass"){ 	 	
 	 			seagrassGetListMap(data,role);
 	 		} 
 	 		if(page == "seaweeds"){
        			seaweedsGetListMap(data,role);
 	 		}
 	 		if(page == "mangrove"){      		
 	 			mangroveGetListMap(data,role);
 	 		}
 	 		if(page == "lgu"){        		
        		lguGetListMap(data,role);
 	 		}
 	 		if(page == "trainingcenter"){
        		trainingcenterGetListMap(data,role);
 	 		}
 	 		if(page == "mariculturezone"){
 	 			mariculturezoneGetListMap(data,role);
 	 		}
     	 
    }
*/

// click marker
  var clickmark;

  // When you click on a circle, it calls the onMapClick function and passes the layers coordinates.
  // I grab the coords which are X,Y, and I need to flip them to latLng for a marker,  
  function onMapClick(coords) {
		console.log(coords + " " + coords.lat);
		//if prior marker exists, remove it.
		if (clickmark != undefined) {
		  map.removeLayer(clickmark);
		};

			 for ( var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				if(coords.lat == obj.lat && coords.lng == obj.lon){
					console.log("true");
					$('#details').html(obj.html);
					$('#clockToronto').html(obj.html);
					
					addContenToMap(obj.html);	
				}
				}	

		
  
		 clickmark = L.circleMarker([coords.lat,coords.lng],{
			radius: 20,
			color: "yellow",
			fillColor:  "yellow",
			fillOpacity: 0.8}
		 ).addTo(map);
	}
	
	function addContenToMap(html){
		console.log("html: " + html);
	}

//paste this code under the head tag or in a separate js file.
	// Wait for window load
	 $(window).on('load', function () {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");
	});

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
