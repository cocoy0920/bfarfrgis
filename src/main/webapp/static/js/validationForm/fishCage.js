
var save=false;

	jQuery(document).ready(function($) {
		$('.file').prop('required',true);
		$(".exportToExcel").hide();
		
		
				//refreshLocation();
				getPage("fishcage");
                
            	
            	getFishCageList();
            	getAllFishCageFirstPage();
            	getPerPage();
            	
  	});       		
            	var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {

               $(".submitFishCage").click(function(event){

                    if (form.checkValidity() === false) {
                      event.preventDefault();
                      event.stopPropagation();
        
                      form.classList.add('was-validated'); 
                    }
                    if (form.checkValidity() === true) {
                        event.preventDefault();
                        document.getElementById("submitFishCage").disabled = true;
                        var array = jQuery(form).serializeArray();
                    	var json = {};

                    	jQuery.each(array, function() {

                    		json[this.name] = this.value || '';
                    	});
                    	
                    	var formData = JSON.stringify(json);
                    	
                    	sendingForm(formData);
                    	form.classList.remove('was-validated'); 
                      }
                    
                  
            		});   	
            	
                }); 
                var validations = Array.prototype.filter.call(forms, function(form) {
                $(".cancel").click(function(event) {
                	 document.getElementById("submitFishCage").disabled = false;
                	 form.classList.remove('was-validated'); 
                	 window.scrollTo(0, 0);
					 $('.form_input').hide();
                	 $('#mapview').hide();
                	 $('.container').show();
				  });
                }); 
                $(".create").click(function(event) {
					/*getFormsByResources("fishcage");*/
                	  document.querySelector('#submitFishCage').innerHTML = 'SAVE';
           		 
                	  document.getElementById("myform").reset();
      			  	document.getElementById("preview").src = "";
      			  	var img_preview = document.getElementById("img_preview");
      			  	var pdf_view = document.getElementById("pdf");
      			  	
      			   	img_preview.style.display = "none";
      			   	pdf_view.style.display = "none";
      				save = true;
      				$('.province').find('option').remove().end();
      				refreshLocation();
      				/*getMunicipality();
      				getBarangay();
      				createCode();*/
      				$('.container').hide();
                	  $('.form_input').show();
                	  $('#mapview').hide();
           	  });
            	
	  
	function getAllFishCageFirstPage(){
		$.get("/create/fishcage/" + "0", function(content, status){
	  
			var markup = "";
			for ( var x = 0; x < content.fishCagePage.length; x++) {
				var active = content.fishCagePage[x].enabled;
    			if(active){
    				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.fishCagePage[x].id + ">ACTIVE</td>"+
    				"<td class='map menu_links btn-primary' data-href="+ content.fishCagePage[x].uniqueKey + ">MAP/UPDATE</td>";
    			}else{
    				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
    				"<td class='menu_links btn-primary'>Not applicable</td>";
    			}
			   markup +="<tr>" +
			   "<td><span>"+ content.fishCagePage[x].region+ "</span></td>" +
			   "<td>"+content.fishCagePage[x].province+"</td>" +
			   "<td>"+content.fishCagePage[x].municipality+"</td>" +
			   "<td>"+content.fishCagePage[x].barangay+"</td>" +
			   "<td>"+content.fishCagePage[x].nameOfOperator+"</td>" +
			    "<td>"+content.fishCagePage[x].lat+","+content.fishCagePage[x].lon+"</td>" +
			    ShowHide +
			   "</tr>";
			}
			$("#export-buttons-table").find('tbody').empty();
			$("#export-buttons-table").find('tbody').append(markup);
		$("#paging").empty();		
		$("#paging").append(content.pageNumber);	
	  });

}

function getPerPage() {   
	    
		  $('#paging').delegate('a', 'click' , function(){    	  
	        var $this = $(this),
	           target = $this.data('target');    
	        	
			 $.get("/create/fishcage/" + target, function(content, status){
				  
				 var markup = "";
						for ( var x = 0; x < content.fishCagePage.length; x++) {
							
							var active = content.fishCagePage[x].enabled;
			    			if(active){
			    				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.fishCagePage[x].id + ">ACTIVE</td>"+
			    				"<td class='map menu_links btn-primary' data-href="+ content.fishCagePage[x].uniqueKey + ">MAP/UPDATE</td>";
			    			}else{
			    				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
			    				"<td class='menu_links btn-primary'>Not applicable</td>";
			    			}
						   markup +="<tr>" +
						   "<td><span>"+ content.fishCagePage[x].region+ "</span></td>" +
						   "<td>"+content.fishCagePage[x].province+"</td>" +
						   "<td>"+content.fishCagePage[x].municipality+"</td>" +
						   "<td>"+content.fishCagePage[x].barangay+"</td>" +
						   "<td>"+content.fishCagePage[x].nameOfOperator+"</td>" +
						    "<td>"+content.fishCagePage[x].lat+","+content.fishCagePage[x].lon+"</td>" +
						    ShowHide +
						    
						    // "<td class='map menu_links btn-primary' data-href="+ content.fishCage[x].uniqueKey + ">MAP/UPDATE</td>" +
						   // "<td class='del menu_links btn-primary' data-href="+ content.fishCage[x].id + ">ACTIVE</td>" +
							
							//"<td class='view menu_links btn-primary' data-href="+ content.fishCage[x].uniqueKey + ">View</td>" +
						   "</tr>";
						}
						$("#export-buttons-table").find('tbody').empty();
						$("#export-buttons-table").find('tbody').append(markup);
					$("#paging").empty();		
					$("#paging").append(content.pageNumber);	
				  });
	       // return target;
	      });       
	   }


//    var token = $("meta[name='_csrf']").attr("content");
//
//            var header = $("meta[name='_csrf_header']").attr("content");
//
//            $(document).ajaxSend(function(e, xhr, options) {
//
//                xhr.setRequestHeader(header, token);
//
//              });
//        

   	function sendingForm(form){
        	var json = form;
	        	
			$.ajax({				
				url : "/create/savingFishCage",
				type: "POST",
				contentType : "application/json",
				data : json,
				dataType : 'json',				
				success : function(content) {
					getAllFishCageFirstPage();
					
				document.getElementById("myform").reset();
			  	document.getElementById("preview").src = "";
			  	var img_preview = document.getElementById("img_preview");
			  	var pdf_view = document.getElementById("pdf");
			  	
			   	img_preview.style.display = "none";
			   	pdf_view.style.display = "none";
				save = true;
				$('.province').find('option').remove().end();
				refreshLocation();
			
					$(".se-pre-con").hide();
					alert("SUCCESSFULLY SAVE");
					window.location.replace("/create/fishCageMap/" + content.uniqueKey);
					//window.location.reload(true); 
					//window.location.reload();
					
				},
				error: function(data){
				console.log("error", data);
				$(".se-pre-con").hide();
				return;
				}
			});
		}
 	
$('#export-buttons-table').delegate('tr', 'click' , function(){
$("#tbodyID").empty();
  var uniqueKey = $(this).data('href');
	   	save = false;
  $.get("/create/getFishCage/" + uniqueKey, function(data, status){
      
//			getFishCageInfoById(data, status);
    		  });
  
  document.querySelector('#submitFishCage').innerHTML = 'UPDATE';
             
});

$('#export-buttons-table').delegate('td.view', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;

	$.get("/create/getFishCage/" + uniqueKey, function(data, status) {

		$('.create').show();
		getFishCageInfoById(data, status);
	});
	 document.querySelector('#submitFishCage').innerHTML = 'UPDATE'; 
});

$('#export-buttons-table').delegate('td.map', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;
	
	$('.mapview').show();
	
	window.location.replace("/create/fishCageMap/" + uniqueKey);

});

$('#export-buttons-table').delegate('td.del', 'click', function() {
	$("#tbodyID").empty();
	var id = $(this).data('href');
	save = false;
//	 let text = "are you sure you want to INACTIVE";
//	  if (confirm(text) == true) {
//			window.location.replace("/create/fishCageDeleteById/" + uniqueKey);
//	  } else {
//	    text = "You canceled!";
//	  }
//	  
		let text;
		  let reason = prompt("Please enter your reason:","");
		  if (reason == null || reason == "") {
		    text = "User cancelled the prompt.";
		  } else {
			  window.location.replace("/create/fishCageDeleteById/" + id +"/" + reason);
			  console.log(reason);

		  }
});


$(function() {
	$(".exportToPDF").click(function(e){
      var doc = new jsPDF('p', 'pt');

      var elem = document.getElementById('pdfFISHCAGE');
      var imgElements = document.querySelectorAll('#pdfFISHCAGE tbody img');
      var data = doc.autoTableHtmlToJson(elem);
      var images = [];
      var i = 0;
    
      var img = imgElements[i].src;
      doc.autoTable(data.columns, data.rows, {
       // bodyStyles: {rowHeight: 30},
        drawCell: function(cell, opts) {
          if (opts.column.dataKey === 1) {
            images.push({
              url: img,
              x: cell.textPos.x,
              y: cell.textPos.y
            });
            i++;
          }
        },
        addPageContent: function() {
      
          for (var i = 0; i < images.length; i++) {
          	if(i == images.length-1){
            doc.addImage(images[i].url, 100, images[i].y, 200, 200);
            }
          }
        }
      });

      doc.save("fishcage.pdf");
         window.location.reload();
	});
});


			$(function() {
				$(".exportToExcel").click(function(e){
					var table = $(this).prev('.table2excel');
					if(table && table.length){
						var preserveColors = (table.hasClass('table2excel_with_colors') ? true : false);
						$(table).table2excel({
							exclude: ".noExl",
							name: "Excel Document Name",
							filename: "FishCage.xls",
							fileext: ".xls",
							exclude_img: true,
							exclude_links: true,
							exclude_inputs: true,
							preserveColors: preserveColors
						});
					}
				});
				
			});

function getFishCageList(){
$.get("/create/fishcageList", function(content, status){
        	
        	 var markup = "";
        	 var tr;
       
         			for ( var x = 0; x < content.length; x++) {
         				
         				 if(content.length - 1 === x) {
     				        $(".exportToExcel").show();
     				    }
     				
					tr = $('<tr/>');
									
					tr.append("<td>" + content[x].province + "</td>");
					tr.append("<td>" + content[x].municipality + "</td>");
					tr.append("<td>" + content[x].barangay + "</td>");
					tr.append("<td>" + content[x].nameOfOperator + "</td>");
					tr.append("<td>" + content[x].code + "</td>");
					tr.append("<td>" + content[x].area + "</td>");
					tr.append("<td>" + content[x].classificationofOperator + "</td>");
					tr.append("<td>" + content[x].cageDimension + "</td>");
					tr.append("<td>" + content[x].cageDesign + "</td>");
					tr.append("<td>" + content[x].cageType + "</td>");
					tr.append("<td>" + content[x].cageNoOfCompartments + "</td>");
					tr.append("<td>" + content[x].indicateSpecies + "</td>");
					tr.append("<td>" + content[x].sourceOfData + "</td>");
					tr.append("<td>" + content[x].lat + "</td>");
					tr.append("<td>" + content[x].lon + "</td>");
					tr.append("<td>" + content[x].remarks + "</td>");
					$('#emp_body').append(tr);	
					}    		
    		  });
}

	 
function getFishCageInfoById(data, status,resProvince ){
	 	var content =  $("#pdfFISHCAGE").find('tbody').empty();
	 	
	     for ( var x = 0; x < data.length; x++) {
	 		var obj = data[x];
	 		$(".id").val(obj.id);
	 	    $(".user").val(obj.user);
	 	    $(".uniqueKey").val(obj.uniqueKey);
	 	    $(".dateEncoded").val(obj.dateEncoded);
	 	    $(".encodedBy").val(obj.encodedBy);

	 	   
	 	    getProvinceMunicipalityBarangay(obj); 

	     $(".fishcagenameOfOperator").val(obj.nameOfOperator);           
	     $(".classificationofOperator").val(obj.classificationofOperator);
	     $(".cageDimension").val(obj.cageDimension);
	     $(".area").val(obj.area);
	     $(".cageType").val(obj.cageType);
	     $(".cageNoOfCompartments").val(obj.cageNoOfCompartments);
	     $(".fishcageindicateSpecies").val(obj.indicateSpecies);
	     $(".cageDesign").val(obj.cageDesign);
	     $(".sourceOfData").val(obj.sourceOfData);
	 	$(".code").val(obj.code);
	 	$(".dateAsOf").val(obj.dateAsOf);
	 	$(".remarks").val(obj.remarks);
	 	$(".lat").val(obj.lat);
	 	$(".lon").val(obj.lon);
	 	$(".image_src").val(obj.image_src);
	 	$(".image").val(obj.image);
	 	$(".preview").attr("src", obj.image);


	     var markup = 
	     "<tr><th scope=\"col\">FISH CAGE LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
	     "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
	     "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
	     "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
	     "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
	     "<tr><td scope=\"row\">Code</td><td>" + obj.code + "</td></tr>" +
	     
	     "<tr><th scope=\"col\">FISH CAGE INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
	     "<tr><th scope=\"row\">Name of Operator:</th><td>"+obj.nameOfOperator+"</td></tr>"+
	    "<tr><th scope=\"row\">Classification of Operator:</th><td>" + obj.classificationofOperator + "</td></tr>"+

	    "<tr><th scope=\"row\">Cage Dimension:</th><td>" + obj.cageDimension + "</td></tr>"+
	    "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
	   "<tr><th scope=\"row\">Cage Type:</th><td>" + obj.cageType + "</td></tr>"+
	   "<tr><th scope=\"row\">Cage Design:</th><td>" + obj.cageDesign + "</td></tr>"+
	   "<tr><th scope=\"row\">Cage No. of Compartments:</th><td>" + obj.cageNoOfCompartments + "</td></tr>"+
	   "<tr><th scope=\"row\">Indicate Species:</th><td>" + obj.indicateSpecies + "</td></tr>"+

	    "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
	   "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
	   "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.sourceOfData + "</td></tr>"+
	   "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
	   
	   "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
	   "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
	   "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
	   "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
	  
	     content.append(markup);
	     
	    // reset();
	     markup = "";
	     $('#img_preview').show();
	     $('#pdf').show();
	     $('#table').hide();	
	     $('.file').prop('required',false);
	 }

	 	
	 }
//	 function printTest(data) {
//		 $.get("/create/fishcagePrintList", function(content, status){
//
//			 printJS({
//				 
//				  	printable: content,
//				  	properties:['region','province',
//				  		'municipality','barangay',
//				  		'nameOfOperator','classificationofOperator',
//				  		'cageDimension','cageType',
//				  		'indicateSpecies','dateAsOf',
//				  		'sourceOfData','area',
//				  		'remarks','area',
//				  		'lat','lon'],
//				    type: 'json',
//				    style: '@page { size: Letter landscape; }'
//	
//				  })
//		 });
//	
//			
//		}

	//	document.getElementById('print-button').addEventListener('click', printTest)

 
	