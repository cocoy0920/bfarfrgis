
var save=false;

	jQuery(document).ready(function($) {
		$('.file').prop('required',true);
		$(".exportToExcel").hide();
		
				//refreshLocation();
				getPage("lgu");
            	getPFoPerPage();
            	getPFOList();  
            	getPerPage();
	});           	
            	var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
            	$(".submitLGU").click(function(event){

                    if (form.checkValidity() === false) {
                      event.preventDefault();
                      event.stopPropagation();
                  
                      form.classList.add('was-validated'); 
                    }
                    if (form.checkValidity() === true) {
                        event.preventDefault();
                        document.getElementById("submitLGU").disabled = true;
                        var array = jQuery(form).serializeArray();
                    	var json = {};

                    	jQuery.each(array, function() {

                    		json[this.name] = this.value || '';
                    	});
                    	
                    	var formData = JSON.stringify(json);
                    	
                    	sendingForm(formData);
                    	form.classList.remove('was-validated'); 
                      }
                    
            	});
            		});
                var validations = Array.prototype.filter.call(forms, function(form) {
                $(".cancel").click(function(event) {
                	document.getElementById("submitLGU").disabled = false;
                	form.classList.remove('was-validated'); 
                	window.scrollTo(0, 0); 
                	$('.form_input').hide();
               	 $('.container').show();
               	 $('#mapview').hide();
					  
				  });
                });
                
                $(".create").click(function(event) {
                	  document.querySelector('#submitLGU').innerHTML = 'SAVE';  
                	document.getElementById("myform").reset();
        		  	document.getElementById("preview").src = "";
        		  	var img_preview = document.getElementById("img_preview");
        		  	var pdf_view = document.getElementById("pdf");
        		   	img_preview.style.display = "none";
        		   	pdf_view.style.display = "none";
        			save = true;
        			$('.province').find('option').remove().end();
        			refreshLocation();
                	$('.form_input').show();
               	 $('.container').hide();
               	 $('#mapview').hide();
           		  
           	  });
	function getPFoPerPage(){
			 $.get("/create/lgu/" + "0", function(content, status){
       	         	
       	 var markup = "";
        			for ( var x = 0; x < content.lgu.length; x++) {
        				var active = content.lgu[x].enabled;
            			if(active){
            				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.lgu[x].id + ">ACTIVE</td>"+
            				"<td class='map menu_links btn-primary' data-href="+ content.lgu[x].uniqueKey + ">MAP/UPDATE</td>";
            			}else{
            				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
            				"<td class='menu_links btn-primary'>Not applicable</td>";
            			}
        			   markup +="<tr>" +
        			   "<td><span>" + content.lgu[x].region + "</span></td>" +
        			   "<td>"+content.lgu[x].province+"</td><td>"+content.lgu[x].municipality+"</td><td>"+content.lgu[x].barangay+"</td><td>"+content.lgu[x].lguName+"</td><td>"+content.lgu[x].lat+","+content.lgu[x].lon+"</td>" +
        			ShowHide +
        			   //  "<td class='map menu_links btn-primary' data-href="+ content.lgu[x].uniqueKey + ">MAP/UPDATE</td>" +
        			 //  "<td class='del menu_links btn-primary' data-href="+ content.lgu[x].id + ">ACTIVE</td>" +
        			   //	"<td class='view menu_links btn-primary' data-href="+ content.lgu[x].uniqueKey + ">View</td>" +
						
        			   "</tr>";
					}
   				$("#export-buttons-table").find('tbody').empty();
   				$("#export-buttons-table").find('tbody').append(markup);
   				$("#paging").empty();		
   				$("#paging").append(content.pageNumber);	
   		  });
	}
	
//	 var token = $("meta[name='_csrf']").attr("content");
//
//     var header = $("meta[name='_csrf_header']").attr("content");
//
//     
//
//     $(document).ajaxSend(function(e, xhr, options) {
//
//         xhr.setRequestHeader(header, token);
//
//       });

function sendingForm(form){
 	var json = form;	
 	
		$.ajax({						
			url : "/create/saveLGU",
			type : "POST",
			contentType : "application/json",
			data :json,
			dataType : 'json',				
			success : function(content) {
				getPFoPerPage();	

			document.getElementById("myform").reset();
		  	document.getElementById("preview").src = "";
		  	var img_preview = document.getElementById("img_preview");
		  	var pdf_view = document.getElementById("pdf");
		   	img_preview.style.display = "none";
		   	pdf_view.style.display = "none";
			save = true;
			$('.province').find('option').remove().end();
			refreshLocation();
		
	  		
		
				$(".se-pre-con").hide();
				alert("SUCCESSFULLY SAVE");
				window.location.replace("/create/lguMap/" + content.uniqueKey);						 
			},
			error: function(data){
			console.log("error", data);
			$(".se-pre-con").hide();
			return;
			}
		});
	}
	
//$('#export-buttons-table').delegate('tr', 'click' , function(){
//$("#tbodyID").empty();
//  var uniqueKey = $(this).data('href');
//  save = false;
//  $.get("/create/getLGU/" + uniqueKey, function(data, status){
//        
//			getLGUInfoById(data, status);
//    		  });
//  document.querySelector('#submitLGU').innerHTML = 'UPDATE';  
//});
//
//$('#export-buttons-table').delegate('td.view', 'click', function() {
//	$("#tbodyID").empty();
//	var uniqueKey = $(this).data('href');
//	save = false;
//
//	$.get("/create/getLGU/" + uniqueKey, function(data, status) {
//
//		$('.create').show();
//		getLGUInfoById(data, status);
//	});
//	 document.querySelector('#submitLGU').innerHTML = 'UPDATE'; 
//});

$('#export-buttons-table').delegate('td.map', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;
	
	window.location.replace("/create/lguMap/" + uniqueKey);

});
$('#export-buttons-table').delegate('td.del', 'click', function() {
	$("#tbodyID").empty();
	var id = $(this).data('href');
	save = false;
/*	 let text = "are you sure you want to delete";
	  if (confirm(text) == true) {
			window.location.replace("/create/lguDeleteById/" + id);
	  } else {
	    text = "You canceled!";
	  }*/
	let text;
	  let reason = prompt("Please enter your reason:","");
	  if (reason == null || reason == "") {
	    text = "User cancelled the prompt.";
	  } else {
		  window.location.replace("/create/lguDeleteById/" + id +"/" + reason);
		  console.log(reason);

	  }
});

$(function() {
	$(".exportToPDF").click(function(e){
      var doc = new jsPDF('p', 'pt');

      var elem = document.getElementById('pdfPFO');
      var imgElements = document.querySelectorAll('#pdfPFO tbody img');
      var data = doc.autoTableHtmlToJson(elem);
      var images = [];
      var i = 0;
      
      var img = imgElements[i].src;
      doc.autoTable(data.columns, data.rows, {
       // bodyStyles: {rowHeight: 30},
        drawCell: function(cell, opts) {
          if (opts.column.dataKey === 1) {
            images.push({
              url: img,
              x: cell.textPos.x,
              y: cell.textPos.y
            });
            i++;
          }
        },
        addPageContent: function() {
       // console.log(images.length);
          for (var i = 0; i < images.length; i++) {
          	if(i == images.length-1){
            doc.addImage(images[i].url, 100, images[i].y, 200, 200);
            }
          }
        }
      });

      doc.save("pfo.pdf");
      window.location.reload();
	});
});
 	  
			$(function() {
				$(".exportToExcel").click(function(e){
					var table = $(this).prev('.table2excel');
					if(table && table.length){
						var preserveColors = (table.hasClass('table2excel_with_colors') ? true : false);
						$(table).table2excel({
							exclude: ".noExl",
							name: "Excel Document Name",
							filename: "PFO.xls",
							fileext: ".xls",
							exclude_img: true,
							exclude_links: true,
							exclude_inputs: true,
							preserveColors: preserveColors
						});
					}
				});
				
			});

function getPFOList(){			
$.get("/create/pfoList", function(content, status){
        	
        	 var markup = "";
        	 var tr;
       
         			for ( var x = 0; x < content.length; x++) {
         			region = content[x].region;
         			
         			 if(content.length - 1 === x) {
 				        $(".exportToExcel").show();
 				    }
 				
         			
					tr = $('<tr/>');
					
					tr.append("<td>" + content[x].province + "</td>");
					tr.append("<td>" + content[x].municipality + "</td>");
					tr.append("<td>" + content[x].barangay + "</td>");
					tr.append("<td>" + content[x].code + "</td>");
					tr.append("<td>" + content[x].lguName + "</td>");
					tr.append("<td>" + content[x].noOfBarangayCoastal + "</td>");
					tr.append("<td>" + content[x].noOfBarangayInland + "</td>");
					tr.append("<td>" + content[x].lguCoastalLength + "</td>");
					tr.append("<td>" + content[x].dataSource + "</td>");
					tr.append("<td>" + content[x].lat + "</td>");
					tr.append("<td>" + content[x].lon + "</td>");
					tr.append("<td>" + content[x].remarks + "</td>");
					
					$('#emp_body').append(tr);	
					//ExportTable();
					}    		
    		  });
}
function getPerPage(){   

	  $('#paging').delegate('a', 'click' , function(){
		  var $this = $(this),
           target = $this.data('target');    
          
		  $.get("/create/lgu/" + target, function(content, status){
		       	 	        	
		       	 var markup = "";
		        for ( var x = 0; x < content.lgu.length; x++) {		    
		        	var active = content.lgu[x].enabled;
        			if(active){
        				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.lgu[x].id + ">ACTIVE</td>"+
        				"<td class='map menu_links btn-primary' data-href="+ content.lgu[x].uniqueKey + ">MAP/UPDATE</td>";
        			}else{
        				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
        				"<td class='menu_links btn-primary'>Not applicable</td>";
        			}
		        	markup +="<tr>" +
		        	"<td><span>" + content.lgu[x].region + "</span></td>" +
		        	"<td>"+content.lgu[x].province+"</td><td>"+content.lgu[x].municipality+"</td><td>"+content.lgu[x].barangay+"</td><td>"+content.lgu[x].lguName+"</td><td>"+content.lgu[x].lat+","+content.lgu[x].lon+"</td>" +
		        	ShowHide +
		        	//"<td class='map menu_links btn-primary' data-href="+ content.lgu[x].uniqueKey + ">MAP/UPDATE</td>" +
		        	//"<td class='del menu_links btn-primary' data-href="+ content.lgu[x].id + ">ACTIVE</td>" +
		        	//"<td class='view menu_links btn-primary' data-href="+ content.lgu[x].uniqueKey + ">View</td>" +
		        	"</tr>";
				}
		   				$("#export-buttons-table").find('tbody').empty();
		   				$("#export-buttons-table").find('tbody').append(markup);
		   				$("#paging").empty();		
		   				$("#paging").append(content.pageNumber);	
		   		  });
      });       
   }

function getLGUInfoById(data, status){
	
	
	var content = $("#pdfPFO").find('tbody').empty();
	
	
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		
		getProvinceMunicipalityBarangay(obj); 
		$(".id").val(obj.id);
	    $(".user").val(obj.user);
	    $(".uniqueKey").val(obj.uniqueKey);
	    $(".dateEncoded").val(obj.dateEncoded);
	    $(".encodedBy").val(obj.encodedBy);	 	 
    $(".lguName").val(obj.lguName);
	$(".noOfBarangayCoastal").val(obj.noOfBarangayCoastal);
	$(".noOfBarangayInland").val(obj.noOfBarangayInland);
	$(".lguCoastalLength").val(obj.lguCoastalLength);
    $(".dataSource").val(obj.dataSource);
	//$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image_src);
	$(".image").val(obj.image);
	$(".preview").attr("src", obj.image);


    var markup = 
    "<tr><th scope=\"col\">PROVINCIAL FISHERIES OFFICE LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
    "<tr><th scope=\"col\">PROVINCIAL FISHERIES OFFICEINFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">PFO Name:</th><td>"+obj.lguName+"</td></tr>"+
    "<tr><th scope=\"col\">No. of Municipality</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">Coastal:</th><td>" + obj.noOfBarangayCoastal + "</td></tr>"+  
   "<tr><th scope=\"row\">Inland:</th><td>" + obj.noOfBarangayInland + "</td></tr>"+
   "<tr><th scope=\"row\">LGU Coastal Length:</th><td>" + obj.lguCoastalLength + "</td></tr>"+

  
  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
 
  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
  "<tr><td  scope=\"col\">IMAGE</td><td scope=\"col\">&nbsp;</td></tr>"+
  "<tr><td  scope=\"col\"></td><td scope=\"col\">&nbsp;</td></tr>"+
  
  "<tr><td scope=\"row\"><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td></tr>"; 
 
    content.append(markup);
    
   // reset();
    markup = "";
    $('#img_preview').show();
    $('#pdf').show();
    $('#table').hide();		
    $('.file').prop('required',false);
}

	
}

