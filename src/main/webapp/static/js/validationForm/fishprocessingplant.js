


jQuery(document).ready(function($) {
	$('#plantData').hide();
	$('.file').prop('required',true);
	$(".exportToExcel").hide();
	var save = false;
					//refreshLocation();
					getPage("fishprocessingplants");
					getFishProcessingPlantList();
					getAllFishProcessingPlant();
					getPerPage();
});					
					
					 var forms = document.getElementsByClassName('needs-validation');
					  var validation = Array.prototype.filter.call(forms, function(form) {
					$(".submitFishProcessingPlant").click(function(event) {

				        if (form.checkValidity() === false) {
				          event.preventDefault();
				          event.stopPropagation();
				          form.classList.add('was-validated'); 
				        }
				        if (form.checkValidity() === true) {
				            event.preventDefault();
				            document.getElementById("submitFishProcessingPlant").disabled = true;
				            var array = jQuery(form).serializeArray();
				        	var json = {};

				        	jQuery.each(array, function() {

				        		json[this.name] = this.value || '';
				        	});
				        	
				        	var formData = JSON.stringify(json);
				        	
				        	sendingForm(formData);
				        	form.classList.remove('was-validated'); 
				          }
				        				      				      
					});	
					  });	
					  
					  var validations = Array.prototype.filter.call(forms, function(form) {
					  $(".cancel").click(function(event) {
						  window.scrollTo(0, 0);  
						  document.getElementById("submitFishProcessingPlant").disabled = false;
						  form.classList.remove('was-validated'); 
						  $('.form_input').hide();
					    	 $('#mapview').hide();
					    	 $('.container').show();
						  
					  });
					  });
					  
					  $(".create").click(function(event) {
						
						  $('#mapview').hide();
						  $('.container').hide();
						  document.querySelector('#submitFishProcessingPlant').innerHTML = 'SAVE'; 
						  document.getElementById("preview").src = "";
							var img_preview = document.getElementById("img_preview");
							var pdf_view = document.getElementById("pdf");
							img_preview.style.display = "none";
							pdf_view.style.display = "none";
							save = true;
							
							$('.province').find('option').remove().end();
							refreshLocation();
						  $('.form_input').show();
						  window.scrollTo(0, 0);   
						  });
					function sendingForm(form) {
						
						var json = form;
						
						$.ajax({
							//method:'post',
							url : "/create/saveFishprocessingplants",
							type : "POST",
							contentType : "application/json",
							dataType:'json',
							data : json,				
							success : function(content) {
								getAllFishProcessingPlant();
											/*$('#myform').trigger("reset");
								document.getElementById("preview").src = "";
								var img_preview = document.getElementById("img_preview");
								var pdf_view = document.getElementById("pdf");
								img_preview.style.display = "none";
								pdf_view.style.display = "none";
								save = true;
								
								$('.province').find('option').remove().end();
								refreshLocation();*/
								$(".se-pre-con").hide();
								alert("SUCCESSFULLY SAVE");
								window.location.replace("/create/fishProcessingMap/" + content.uniqueKey);
							},
							error : function(data) {
								console.log("error", data);
								$(".se-pre-con").hide();
								return;
							}
						});
					}



					
					


function getAllFishProcessingPlant(){
	
	$.get("/create/fishprocessingplants/" + "0",function(content, status) {

		
				var markup = "";
				for (var x = 0; x < content.fishProcessingPlantPage.length; x++) {
					var active = content.fishProcessingPlantPage[x].enabled;
        			if(active){
        				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].id + ">ACTIVE</td>"+
        				"<td class='map menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].uniqueKey + ">MAP/UPDATE</td>";
        			}else{
        				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
        				"<td class='menu_links btn-primary'>Not applicable</td>";
        			}
					markup += "<tr>"+
							"<td><span>"+ content.fishProcessingPlantPage[x].region+ "</span></td>"+
							"<td>"+ content.fishProcessingPlantPage[x].province+ "</td>" +
							"<td>"+ content.fishProcessingPlantPage[x].municipality+"</td>" +
							"<td>"+ content.fishProcessingPlantPage[x].barangay+ "</td>" +
							"<td>"+ content.fishProcessingPlantPage[x].nameOfProcessingPlants+"</td>" +
							"<td>"+ content.fishProcessingPlantPage[x].lat+ ","+ content.fishProcessingPlantPage[x].lon + "</td>"+
							ShowHide +
							//"<td class='map menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].uniqueKey + ">MAP/UPDATE</td>" +
							//"<td class='del menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].id + ">DELETE</td>" +
							//"<td class='view menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].uniqueKey + ">View</td>"+ 
							"</tr>";
				}
				$("#export-buttons-table").find('tbody').empty();
				$("#export-buttons-table").find('tbody').append(markup);
				$("#paging").empty();
				$("#paging").append(content.pageNumber);
			});
}
///////////////////

////////////for saving DATA/////////////////


//for viewing of data


$(function() {
	$(".exportToPDF").click(function(e){
	var doc = new jsPDF('p', 'pt');

	var elem = document.getElementById('pdfFISHPROCESSINGPLANT');
	var imgElements = document.querySelectorAll('#pdfFISHPROCESSINGPLANT tbody img');
	var data = doc.autoTableHtmlToJson(elem);
	var images = [];
	var i = 0;
	
	var img = imgElements[i].src;
	doc.autoTable(data.columns, data.rows, {
		// bodyStyles: {rowHeight: 30},
		drawCell : function(cell, opts) {
			if (opts.column.dataKey === 1) {
				images.push({
					url : img,
					x : cell.textPos.x,
					y : cell.textPos.y
				});
				i++;
			}
		},
		addPageContent : function() {

			for (var i = 0; i < images.length; i++) {
				if (i == images.length - 1) {
					doc.addImage(images[i].url, 100, images[i].y, 200, 200);
				}
			}
		}
	});

	doc.save("FISHPROCESSINGPLANT.pdf");
	window.location.reload();
	});
});


function getFishProcessingInfoById(data, status) {
	
	for (var x = 0; x < data.length; x++) {
	 var obj = data[x];

		$(".id").val(obj.id);
		$(".user").val(obj.user);
		$(".uniqueKey").val(obj.uniqueKey);
		$(".dateEncoded").val(obj.dateEncoded);
		$(".encodedBy").val(obj.encodedBy);

		getProvinceMunicipalityBarangay(obj);

		$(".nameOfProcessingPlants").val(obj.nameOfProcessingPlants);
		$(".nameOfOperator").val(obj.nameOfOperator);
		$(".operatorClassification").val(obj.operatorClassification);
		$(".processingTechnique").val(obj.processingTechnique);
		$(".processingEnvironmentClassification").val(
				obj.processingEnvironmentClassification);
		$(".bfarRegistered").val(obj.bfarRegistered);
		$(".plantRegistered").val(obj.plantRegistered);
		$(".packagingType").val(obj.packagingType);
		$(".marketReach").val(obj.marketReach);
		$(".indicateSpecies").val(obj.indicateSpecies);
		$(".businessPermitsAndCertificateObtained").val(obj.businessPermitsAndCertificateObtained);
		$(".sourceOfData").val(obj.sourceOfData);
		$(".area").val(obj.area);
		$(".code").val(obj.code);
		$(".dateAsOf").val(obj.dateAsOf);
		$(".remarks").val(obj.remarks);
		$(".lat").val(obj.lat);
		$(".lon").val(obj.lon);
		$(".image_src").val(obj.image_src);
		$(".image").val(obj.image);
		$(".preview").attr("src", obj.image);

		var markup = "";
		markup = "<tr><th scope=\"col\">FISH PROCESSING PLANT LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"
//				
//				+ "<tr><th scope=\"row\">#</th><td>"
//				+ obj.id
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Province</th><td>"
				+ obj.province
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Municipality</th><td>"
				+ obj.municipality
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Barangay</th><td>"
				+ obj.barangay
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Code:</th><td>"
				+ obj.code
				+ "</td></tr>"
				+ "<tr><th scope=\"col\">FISH PROCESSING PLANT INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"
				+ "<tr><th scope=\"row\">Name of Processing Plant:</th><td>"
				+ obj.nameOfProcessingPlants
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Name of Operator:</th><td>"
				+ obj.nameOfOperator
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Area:</th><td>"
				+ obj.area
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Operator Classification:</th><td>"
				+ obj.operatorClassification
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Processing Technique:</th><td>"
				+ obj.processingTechnique
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Processing Environment Classification:</th><td>"
				+ obj.processingEnvironmentClassification
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">BFAR Plant Registered:</th><td>"
				+ obj.plantRegistered
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">BFAR Registered:</th><td>"
				+ obj.bfarRegistered
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Packaging Type:</th><td>"
				+ obj.packagingType
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Market Reach:</th><td>"
				+ obj.marketReach
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Indicate Species:</th><td>"
				+ obj.indicateSpecies
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Business permits and Certificates Obtained:</th><td>"
				+ obj.businessPermitsAndCertificateObtained
				+ "</td></tr>"
				+ "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"
				+ "<tr><th scope=\"row\">Date as of:</th><td>"
				+ obj.dateAsOf
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Data Sources:</th><td>"
				+ obj.sourceOfData
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Remarks:</th><td>"
				+ obj.remarks
				+ "</td></tr>"
				+"<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"
				+ "<tr><th scope=\"row\">Latitude:</th><td>"
				+ obj.lat
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Longitude:</th><td>"
				+ obj.lon
				+ "</td></tr>"
				+ "<tr align=\"left\"><td><img id=\"imgview\" src=\""
				+ obj.image_src + "\"></img></td>&nbsp;<td></td></tr>";
		$("#pdfFISHPROCESSINGPLANT").find('tbody').append(markup);

		//reset();

		markup = "";
		$('#img_preview').show();
		$('#pdf').show();
		$('#table').hide();
		$('.file').prop('required',false);
	}
	


}

//	

//$('#export-buttons-table').delegate('tr', 'click', function() {
//	$("#tbodyID").empty();
//	var uniqueKey = $(this).data('href');
//	console.log("*[data-href]" + uniqueKey);
//	save = false;
//	$.get("/create/getFishProcessingplants/" + uniqueKey, function(data, status) {
//		//getdata.js      
//
//		for (var x = 0; x < data.length; x++) {
//			var obj = data[x];
//			var plantRegistered = obj.processingEnvironmentClassification;
//
//			if (plantRegistered == 'Plant') {
//				$('#plantData').show();
//			}
//			if (plantRegistered == 'Backyard') {
//				$('#plantData').hide();
//			}
//
//		}
//		getFishProcessingInfoById(data, status);
//	});
//	 document.querySelector('#submitFishProcessingPlant').innerHTML = 'UPDATE'; 
//});

//$('#export-buttons-table').delegate('td.view', 'click', function() {
//	$("#tbodyID").empty();
//	var uniqueKey = $(this).data('href');
//	save = false;
//
//	$.get("/create/getFishProcessingplants/" + uniqueKey, function(data, status) {
//
//		for (var x = 0; x < data.length; x++) {
//			var obj = data[x];
//			var plantRegistered = obj.processingEnvironmentClassification;
//
//			if (plantRegistered == 'Plant') {
//				$('#plantData').show();
//			}
//			if (plantRegistered == 'Backyard') {
//				$('#plantData').hide();
//			}
//
//		}
//		$('.create').show();
//		getFishProcessingInfoById(data, status);
//	});
//	 document.querySelector('#submitFishProcessingPlant').innerHTML = 'UPDATE'; 
//});

$('#export-buttons-table').delegate('td.map', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;
	$('#mapview').show();
	window.location.replace("/create/fishProcessingMap/" + uniqueKey);

});
$('#export-buttons-table').delegate('td.del', 'click', function() {
	$("#tbodyID").empty();
	var id = $(this).data('href');
	save = false;
/*	 let text = "are you sure you want to delete";
	  if (confirm(text) == true) {
			window.location.replace("/create/fishProcessingDeleteById/" + uniqueKey);
	  } else {
	    text = "You canceled!";
	  }*/

		let text;
		  let reason = prompt("Please enter your reason:","");
		  if (reason == null || reason == "") {
		    text = "User cancelled the prompt.";
		  } else {
			  window.location.replace("/create/fishProcessingDeleteById/" + id +"/" + reason);
			  console.log(reason);

		  }

});



function getPerPage(){
	// var trigger = $('ul li a'); 
	$('#paging').delegate('a','click',function() {
				var $this = $(this), 
				target = $this.data('target');

				$.get("/create/fishprocessingplants/" + target,function(content, status) {

							var markup = "";
							for (var x = 0; x < content.fishProcessingPlantPage.length; x++) {
								
								var active = content.fishProcessingPlantPage[x].enabled;
			        			if(active){
			        				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].id + ">ACTIVE</td>"+
			        				"<td class='map menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].uniqueKey + ">MAP/UPDATE</td>";
			        			}else{
			        				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
			        				"<td class='menu_links btn-primary'>Not applicable</td>";
			        			}
								markup += "<tr>"+
								"<td><span>"+ content.fishProcessingPlantPage[x].region+ "</span></td>"+
								"<td>"+ content.fishProcessingPlantPage[x].province+ "</td>" +
								"<td>"+ content.fishProcessingPlantPage[x].municipality+"</td>" +
								"<td>"+ content.fishProcessingPlantPage[x].barangay+ "</td>" +
								"<td>"+ content.fishProcessingPlantPage[x].nameOfProcessingPlants+"</td>" +
								"<td>"+ content.fishProcessingPlantPage[x].lat+ ","+ content.fishProcessingPlantPage[x].lon + "</td>"+
								ShowHide +
								//"<td class='map menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].uniqueKey + ">MAP/UPDATE</td>" +
								//"<td class='del menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].id + ">DELETE</td>" +
								//"<td class='view menu_links btn-primary' data-href="+ content.fishProcessingPlantPage[x].uniqueKey + ">View</td>"+ 
								"</tr>";
							}
							$("#export-buttons-table")
									.find('tbody').empty();
							$("#export-buttons-table")
									.find('tbody').append(markup);
							$("#paging").empty();
							$("#paging").append(content.pageNumber);
						});
					});

					
}


$(function() {
	$(".exportToExcel")
			.click(
					function(e) {
						var table = $(this).prev('.table2excel');
						if (table && table.length) {
							var preserveColors = (table
									.hasClass('table2excel_with_colors') ? true
									: false);
							$(table).table2excel({
								exclude : ".noExl",
								name : "Excel Document Name",
								filename : "FishProcessingPlant.xls",
								fileext : ".xls",
								exclude_img : true,
								exclude_links : true,
								exclude_inputs : true,
								preserveColors : preserveColors
							});
						}
					});
});

function getFishProcessingPlantList(){
	$.get("/create/fishprocessingsList",function(content, status) {
						
						var markup = "";
						var tr;

						for (var x = 0; x < content.length; x++) {
							
							if(content.length - 1 === x) {
						        $(".exportToExcel").show();
						    }
							
							tr = $('<tr/>');

							tr.append("<td>"+ content[x].province+ "</td>");
							tr.append("<td>"+ content[x].municipality+ "</td>");
							tr.append("<td>"+ content[x].barangay+ "</td>");
							tr.append("<td>"+ content[x].nameOfProcessingPlants+ "</td>");
							tr.append("<td>" + content[x].code+ "</td>");
							tr.append("<td>"+ content[x].nameOfOperator+ "</td>");
							tr.append("<td>" + content[x].area+ "</td>");
							tr.append("<td>"+ content[x].operatorClassification+ "</td>");
							tr.append("<td>"+ content[x].processingTechnique+ "</td>");
							tr.append("<td>"+ content[x].processingEnvironmentClassification+ "</td>");
							tr.append("<td>"+ content[x].plantRegistered+ "</td>");
							tr.append("<td>"+ content[x].bfarRegistered+ "</td>");
							tr.append("<td>"+ content[x].packagingType+ "</td>");
							tr.append("<td>"+ content[x].marketReach+ "</td>");
							tr.append("<td>"+ content[x].businessPermitsAndCertificateObtained+ "</td>");
							tr.append("<td>"+ content[x].indicateSpecies+ "</td>");
							tr.append("<td>"+ content[x].sourceOfData+ "</td>");
							tr.append("<td>" + content[x].lat+ "</td>");
							tr.append("<td>" + content[x].lon+ "</td>");
							tr.append("<td>"+ content[x].remarks+ "</td>");
							$('#emp_body').append(tr);
							//ExportTable();
						}
					});

}

function printTest(data) {
	 $.get("/create/fishprocessingsPrintList", function(content, status){

		 printJS({
			 
			  	printable: content,
			  	properties:['region','province',
			  		'municipality','barangay',
			  		'nameOfOperator','operatorClassification',
			  		'processingTechnique','processingEnvironmentClassification',
			  		'plantRegistered','bfarRegistered',
			  		'sourceOfData',
			  		'remarks','area',
			  		'lat','lon'],
			    type: 'json',
			    style: '@page { size: Letter landscape; }'

			  })
	 });

		
	}

//	document.getElementById('print-button').addEventListener('click', printTest)


