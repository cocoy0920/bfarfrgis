$(document).ready(function () {
	refreshLocation();
	getPage("fishsanctuaries");
	getAllFirstPage();
//    $("#myform").submit(function (event) {
//
//        event.preventDefault();
//
//        fire_ajax_submit();
//
//    });
    $(".submitFishSanctuaries").click(function(e) {
    	e.preventDefault();
    	//$(".se-pre-con").show();
    	fire_ajax_submit();
    	//checkInputs();
    });

});

function getAllFirstPage(){
	$.get("fishsanctuaries/" + "1", function(content, status) {

		var markup = "";
		for (var x = 0; x < content.fishSanctuaries.length; x++) {
			markup += "<tr data-href="
					+ content.fishSanctuaries[x].uniqueKey + "><td>"
					+ content.fishSanctuaries[x].region + "</td><td>"
					+ content.fishSanctuaries[x].province + "</td><td>"
					+ content.fishSanctuaries[x].municipality
					+ "</td><td>" + content.fishSanctuaries[x].barangay
					+ "</td><td>"
					+ content.fishSanctuaries[x].nameOfFishSanctuary
					+ "</td><td>" + content.fishSanctuaries[x].lat
					+ "," + content.fishSanctuaries[x].lon
					+ "</td></tr>";
		}
		$("#export-buttons-table").find('tbody').empty();
		$("#export-buttons-table").find('tbody').append(markup);
		$("#paging").empty();
		$("#paging").append(content.pageNumber);
	});

}

function ConvertFormToJSON() {
	var array = $("#myform").serializeArray();
	var json = {};

	jQuery.each(array, function() {
		console.log(json[this.name] = this.value || '');
		json[this.name] = this.value || '';
	});

	return JSON.stringify(json);
}

function fire_ajax_submit() {

   // var search = {}
   // search["username"] = $("#username").val();
   
    $("#submitFishSanctuaries").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/create/api/saveFishSanctuary",
        data: ConvertFormToJSON(),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            $('#feedback').html(data.msg);
            $('#myform').trigger("reset");
            document.getElementById("preview").src = "";
            getAllFirstPage();
            console.log("SUCCESS : ", data);
            $("#submitFishSanctuaries").prop("disabled", false);

        },
        error: function (e) {

            var json = "<h4>Response</h4><pre>"
                + e.responseText + "</pre>";
            $('#feedback').html(json);

            console.log("ERROR : ", e);
           $("#submitFishSanctuaries").prop("disabled", false);

        }
    });

}

//for generating PDF file
function generatepdfFISHSANCTUARY() {

	var element = document.getElementById("myprogressBar");
	var width = 1;
	var identity = setInterval(scene, 10);
	function scene() {
		if (width >= 100) {
			clearInterval(identity);
		} else {
			width++;
			//element.style.width = width + '%'; 
			//element.innerHTML = width * 1 + '%'; 
		}
	}

	var doc = new jsPDF('p', 'pt');

	var elem = document.getElementById('pdfFISHSANCTUARY');
	var imgElements = document.querySelectorAll('#pdfFISHSANCTUARY tbody img');
	var data = doc.autoTableHtmlToJson(elem);
	var images = [];
	var i = 0;
	var img = imgElements[i].src;
	doc.autoTable(data.columns, data.rows, {
		// bodyStyles: {rowHeight: 30},
		drawCell : function(cell, opts) {
			if (opts.column.dataKey === 1) {
				images.push({
					url : img,
					url : img,
					x : cell.textPos.x,
					y : cell.textPos.y
				});
				i++;
			}
		},
		addPageContent : function() {

			for (var i = 0; i < images.length; i++) {
				if (i == images.length - 1) {
					doc.addImage(images[i].url, 100, images[i].y, 200, 200);
				}
			}
		}
	});

	doc.save("fishSanctuaries.pdf");
	// window.location.reload();
}

//GENERATE EXCEL FORM FROM LISTING OF DATA 
$(function() {
	$(".exportToExcel")
			.click(
					function(e) {
						var table = $(this).prev('.table2excel');
						if (table && table.length) {
							var preserveColors = (table
									.hasClass('table2excel_with_colors') ? true
									: false);
							$(table).table2excel({
								exclude : ".noExl",
								name : "Excel Document Name",
								filename : "FishSanctuaries.xls",
								fileext : ".xls",
								exclude_img : true,
								exclude_links : true,
								exclude_inputs : true,
								preserveColors : preserveColors
							});
						}
					});

});

//viewing of DATA PER ID AND PDF FILE CREATION
function getFishSanctuaryInfoById(data, status) {

	var content = $("#pdfFISHSANCTUARY").find('tbody').empty();
	var markup = null
	//$('#customers').hide();

	for (var x = 0; x < data.length; x++) {
		var obj = data[x];

		$(".id").val(obj.id);
		$(".user").val(obj.user);
		$(".uniqueKey").val(obj.uniqueKey);
		$(".dateEncoded").val(obj.dateEncoded);
		$(".encodedBy").val(obj.encodedBy);
		
		getProvinceMunicipalityBarangay(obj); 
		
		$(".nameOfFishSanctuary").val(obj.nameOfFishSanctuary);
		$(".area").val(obj.area);
		$(".bfardenr").val(obj.bfardenr);
		$(".sheltered").val(obj.sheltered);
		$(".nameOfSensitiveHabitatMPA").val(obj.nameOfSensitiveHabitatMPA);
		$(".OrdinanceNo").val(obj.OrdinanceNo);
		$(".OrdinanceTitle").val(obj.OrdinanceTitle);
		$(".DateEstablished").val(obj.DateEstablished);
		$(".type").val(obj.type);
		$(".code").val(obj.code);
		$(".dateAsOf").val(obj.dateAsOf);
		$(".fishSanctuarySource").val(obj.fishSanctuarySource);
		$(".remarksContent").val(obj.remarks);
		$(".lat").val(obj.lat);
		$(".lon").val(obj.lon);
		$(".image_src").val(obj.image_src);
		$(".image").val(obj.image);
		$(".preview").attr("src", obj.image_src);

		markup =

		"<tr><td scope=\"col\">FISH SANCTUARY LOCATION</td><td scope=\"col\">&nbsp;</td></tr>"
				+ "<tr><td scope=\"row\">Region</td><td>"
				+ obj.region
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Province</td><td>"
				+ obj.province
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Municipality</td><td>"
				+ obj.municipality
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Barangay</td><td>"
				+ obj.barangay
				+ "</td></tr>"
				+ "<tr><td scope=\"col\">FISH SANCTUARY INFORMATION</td><td scope=\"col\">&nbsp;</td></tr>"
				+ "<tr><td scope=\"row\">Name of Sanctuary/Protected Area:</td><td>"
				+ obj.nameOfFishSanctuary
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Implementer:</td><td scope=\"col\">&nbsp;</tdd></tr>"
				+ "<tr><td scope=\"col\">BFAR</td><td scope=\"col\">&nbsp;</td></tr>"
				+ "<tr><td scope=\"row\">Name of Sheltered Fisheries/Species:</td><td>"
				+ obj.sheltered
				+ "</td></tr>"
				+ "<tr><td scope=\"col\">DENR</td><td scope=\"col\">&nbsp;</td></tr>"
				+ "<tr><td scope=\"row\">Name of Sensitive Habitat with MPA:</td><td>"
				+ obj.nameOfSensitiveHabitatMPA
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Ordinance No:</td><td>"
				+ obj.OrdinanceNo
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Ordinance Title:</td><td>"
				+ obj.OrdinanceTitle
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Data Established:</td><td>"
				+ obj.DateEstablished
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Area:</td><td>"
				+ obj.area
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Type:</td><td>"
				+ obj.type
				+ "</td></tr>"
				+ "<tr><td scope=\"col\">ADDITIONAL INFORMATION</td><td scope=\"col\">&nbsp;</td></tr>"
				+ "<tr><td scope=\"row\">Date as of:</td><td>"
				+ obj.dateAsOf
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Data Sources:</td><td>"
				+ obj.fishSanctuarySource
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Remarks:</td><td>"
				+ obj.remarks
				+ "</td></tr>"
				+ "<tr><td  scope=\"col\">GEOGRAPHICAL LOCATION</td><td scope=\"col\">&nbsp;</td></tr>"
				+ "<tr><td scope=\"row\">Latitude:</td><td>"
				+ obj.lat
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Longitude:</td><td>"
				+ obj.lon
				+ "</td></tr>"
				+ "<tr><td  scope=\"col\">IMAGE</td><td scope=\"col\">&nbsp;</td></tr>"
				+ "<tr><td  scope=\"col\"></td><td scope=\"col\">&nbsp;</td></tr>"
				+ "<tr><td scope=\"col\"><img id=\"imgview\" src=\""
				+ obj.image_src + "\"></img></td></tr>";

		content.append(markup);

		//reset();
		$('#feedback').html("");
		markup = "";
		$('#img_preview').show();
		$('#pdf').show();
		$('#table').hide();
		//   $('.provinceForm').text(obj.province);
		//  $('.regionForm').text(obj.region);

	}
}

//clicking on table row for viewing of data to form
$('#export-buttons-table').delegate('tr', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	console.log("*[data-href]" + uniqueKey);
	save = false;

	$.get("info/" + uniqueKey, function(data, status) {

		for (var x = 0; x < data.length; x++) {
			var obj = data[x];
			var bfardenr = obj.bfardenr;
			console.log(bfardenr);
			if (bfardenr == 'BFAR') {
				$('#bfarData').show();
				$('#denrData').hide();
			}
			if (bfardenr == 'DENR') {
				$('#bfarData').hide();
				$('#denrData').show();
			}

		}
		getFishSanctuaryInfoById(data, status);
	});
});

$(document)
		.ready(
				function() {

					// var trigger = $('ul li a'); 
					$('#paging')
							.delegate(
									'a',
									'click',
									function() {
										//  trigger.on('click', function(){

										var $this = $(this), target = $this
												.data('target');

										$
												.get(
														"fishsanctuaries/"
																+ target,
														function(content,
																status) {
															data = content.fishSanctuaries;
															var markup = "";
															for (var x = 0; x < content.fishSanctuaries.length; x++) {

																markup += "<tr data-href="
																		+ content.fishSanctuaries[x].uniqueKey
																		+ "><td>"
																		+ content.fishSanctuaries[x].region
																		+ "</td><td>"
																		+ content.fishSanctuaries[x].province
																		+ "</td><td>"
																		+ content.fishSanctuaries[x].municipality
																		+ "</td><td>"
																		+ content.fishSanctuaries[x].barangay
																		+ "</td><td>"
																		+ content.fishSanctuaries[x].nameOfFishSanctuary
																		+ "</td><td>"
																		+ content.fishSanctuaries[x].lat
																		+ ","
																		+ content.fishSanctuaries[x].lon
																		+ "</td></tr>";
															}

															$(
																	"#export-buttons-table")
																	.find(
																			'tbody')
																	.empty();
															$(
																	"#export-buttons-table")
																	.find(
																			'tbody')
																	.append(
																			markup);
															$("#paging")
																	.empty();
															$("#paging")
																	.append(
																			content.pageNumber);
														});

										return false;
									});

				});

$.get("fishsanctuariesList",
		function(content, status) {
			var markup = "";
			var tr;

			for (var x = 0; x < content.fishSanctuaries.length; x++) {
				
				tr = $('<tr/>');

				tr.append("<td>" + content.fishSanctuaries[x].province
						+ "</td>");
				tr.append("<td>" + content.fishSanctuaries[x].municipality
						+ "</td>");
				tr.append("<td>" + content.fishSanctuaries[x].barangay
						+ "</td>");
				tr.append("<td>"
						+ content.fishSanctuaries[x].nameOfFishSanctuary
						+ "</td>");
				tr.append("<td>" + content.fishSanctuaries[x].code + "</td>");
				tr.append("<td>" + content.fishSanctuaries[x].area + "</td>");
				tr.append("<td>" + content.fishSanctuaries[x].type + "</td>");
				tr.append("<td>"
						+ content.fishSanctuaries[x].fishSanctuarySource
						+ "</td>");
				tr.append("<td>" + content.fishSanctuaries[x].bfardenr
						+ "</td>");
				tr.append("<td>" + content.fishSanctuaries[x].sheltered
						+ "</td>");
				tr.append("<td>"
						+ content.fishSanctuaries[x].nameOfSensitiveHabitatMPA
						+ "</td>");
				tr.append("<td>" + content.fishSanctuaries[x].OrdinanceNo
						+ "</td>");
				tr.append("<td>" + content.fishSanctuaries[x].OrdinanceTitle
						+ "</td>");
				tr.append("<td>" + content.fishSanctuaries[x].DateEstablished
						+ "</td>");
				tr.append("<td>" + content.fishSanctuaries[x].lat + "</td>");
				tr.append("<td>" + content.fishSanctuaries[x].lon + "</td>");
				tr
						.append("<td>" + content.fishSanctuaries[x].remarks
								+ "</td>");

				$('#emp_body').append(tr);
				//ExportTable();
			}
		});


function checkInputs() {
	var valid = false;
	const provinceValue = province.value.trim();
	const municipalityValue = municipality.value.trim();
	const barangayValue = barangay.value.trim();
	const remarksValue = remarks.value.trim();
	const latitudeValue = latitude.value.trim();
	const longitudeValue = longitude.value.trim();
	const fileValue = file.value.trim();
	const filePreviewValue = filePreview.value.trim();
	const fileDisabledValue = fileDisabled;
	const pageValue = page.value.trim();
	const fishSanctuarySourceValue = fishSanctuarySource.value.trim();
	//const areaValue = area.value.trim();
	
		const nameOfFishSanctuaryValue = nameOfFishSanctuary.value.trim();
		const bfardenrValue = bfardenr.value.trim();		
		const typeValue = type.value.trim();
	//	const dateAsOfValue = dateAsOf.value.trim();
		
		

		if (nameOfFishSanctuaryValue === '') {
			setErrorFor(nameOfFishSanctuary,
					'name Of Fish Sanctuary cannot be blank');
			valid = false;
		} else {
			setSuccessFor(nameOfFishSanctuary);
			valid = true;
		}
		
		if (bfardenrValue === '') {
			setErrorFor(bfardenr, 'Implementer cannot be blank');
			valid = false;
		} else {
			setSuccessFor(bfardenr);
			valid = true;
		}
		
		if (typeValue === '') {
			setErrorFor(type, 'Type cannot be blank');
			valid = false;
		} else {
			setSuccessFor(type);
			valid = true;
		}
	

	

	

	if (provinceValue === 'select') {
		setErrorFor(province, 'Pls select Province');
		valid = false;
	} else {
		setSuccessFor(province);
		valid = true;
	}
	if (municipalityValue === 'select') {
		setErrorFor(municipality, 'Pls select Municipality');
		valid = false;
	} else {
		setSuccessFor(municipality);
		valid = true;
	}
	if (barangayValue === 'select') {
		setErrorFor(barangay, 'Pls select Barangay');
		valid = false;
	} else {
		setSuccessFor(barangay);
		valid = true;
	}

	if (area.value.trim() === '') {
		setErrorFor(area, 'Area cannot be blank');
		valid = false;
	} else {
		setSuccessFor(area);
		valid = true;
	}

	if (dateAsOf.value.trim() === '') {
		setErrorFor(dateAsOf, 'Date cannot be blank');
		valid = false;
	} else {
		setSuccessFor(dateAsOf);
		valid = true;
	}
	if (fishSanctuarySourceValue === '') {
		setErrorFor(fishSanctuarySource, 'Data Source cannot be blank');
		valid = false;
	} else {
		setSuccessFor(fishSanctuarySource);
		valid = true;
	}

	if (latitudeValue === '') {
		setErrorFor(latitude, 'Latitude cannot be blank');
		valid = false;
	} else {
		setSuccessFor(latitude);
		valid = true;
	}

	if (longitudeValue === '') {
		setErrorFor(longitude, 'Longitude cannot be blank');
		valid = false;
	} else {
		setSuccessFor(longitude);
		valid = true;
	}

	if (save) {
		console.log("if fileDisabledValue" + save);
		valid = false;
		if (fileValue === '') {
			setErrorFor(file, 'Select Image');
			valid = false;
		} else {
			setSuccessFor(file);
			valid = true;
		}

	} else {

	}

	if (valid) {
		//return true;
		//sendingForm();
		setReset(province);
		setReset(municipality);
		setReset(barangay);
		setReset(nameOfFishSanctuary);
		setReset(bfardenr);
		setReset(area);
		setReset(type);
		setReset(dateAsOf);
		setReset(fishSanctuarySource);
		setReset(latitude);
		setReset(longitude);
		setReset(file);
		//window.location = 'success.jsp';
	} else {
		$(".se-pre-con").hide();
	}

}

function setErrorFor(input, message) {

	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	if (message != null) {
		small.innerHTML = message;
	}
	formControl.className = 'control error';
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');

	small.innerHTML = "";

	formControl.className = 'control success';
}
function setReset(input) {
	const formControl = input.parentElement;
	formControl.className = 'control form-reset';
}
