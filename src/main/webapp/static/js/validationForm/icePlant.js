var save=false;

	jQuery(document).ready(function($) {
		$('.file').prop('required',true);
		$(".exportToExcel").hide();

	 		//refreshLocation();
	 		getPage("iceplantorcoldstorage");                	
            getIPCSList();
            getIPCSPerPage();
            getPerPage();
            
	});          
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
    		$(".submitIcePlantColdStorage").click(function(event){

    	        if (form.checkValidity() === false) {
    	          event.preventDefault();
    	          event.stopPropagation();
    	 
    	          form.classList.add('was-validated'); 
    	        }
    	        if (form.checkValidity() === true) {
    	            event.preventDefault();
    	            document.getElementById("submitIcePlantColdStorage").disabled = true;
    	            var array = jQuery(form).serializeArray();
    	        	var json = {};

    	        	jQuery.each(array, function() {

    	        		json[this.name] = this.value || '';
    	        	});
    	        	
    	        	var formData = JSON.stringify(json);
    	        	
    	        	sendingForm(formData);
    	        	form.classList.remove('was-validated'); 
    	          }
    	        
    			});
    			});
            
            var validations = Array.prototype.filter.call(forms, function(form) {
            $(".cancel").click(function(event) {
            	 document.getElementById("submitIcePlantColdStorage").disabled = false;
            	 form.classList.remove('was-validated'); 
            	window.scrollTo(0, 0); 
            	 $('.form_input').hide();
            	 $('#mapview').hide();
            	 $('.container').show();
				  
			  });
            });
            
            
            $(".create").click(function(event) {
            	 $('#mapview').hide();
            	 $('.container').hide();
            	   document.querySelector('#submitIcePlantColdStorage').innerHTML = 'SAVE';
            	 document.getElementById("myform").reset();
				  	document.getElementById("preview").src = "";
				  	var img_preview = document.getElementById("img_preview");
				  	var pdf_view = document.getElementById("pdf");
				  	img_preview.style.display = "none";
				   	pdf_view.style.display = "none";
					save = true;
					
					$('.province').find('option').remove().end();
					refreshLocation();
       		 $('.form_input').show();
       		  
       	  });

	function getIPCSPerPage(){
		
			 $.get("/create/iceplantorcoldstorage/" + "0", function(content, status){

        	 var markup = "";
         			for ( var x = 0; x < content.icePlantColdStorage.length; x++) {

    					var active = content.icePlantColdStorage[x].enabled;
    	    			if(active){
    	    				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].id + ">ACTIVE</td>"+
    	    				"<td class='map menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].uniqueKey + ">MAP/UPDATE</td>";
    	    			}else{
    	    				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
    	    				"<td class='menu_links btn-primary'>Not applicable</td>";
    	    			}
         			   markup +="<tr>" +
         			   			"<td><span>" + content.icePlantColdStorage[x].region + "</span></td>" +
         			   			"<td>"+content.icePlantColdStorage[x].province+"</td>" +
         			   			"<td>"+content.icePlantColdStorage[x].municipality+"</td>" +
         			   			"<td>"+content.icePlantColdStorage[x].barangay+"</td>" +
         			   			"<td>"+content.icePlantColdStorage[x].nameOfIcePlant+"</td>" +
         			   			"<td>"+content.icePlantColdStorage[x].lat+","+content.icePlantColdStorage[x].lon+"</td>" +
         			   			ShowHide +
         			   			//"<td class='map menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].uniqueKey + ">MAP/UPDATE</td>" +
         			   			//"<td class='del menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].id + ">ACTIVE</td>" +
         			   			//"<td class='view menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].uniqueKey + ">View</td>" +
        						
         			   				"</tr>";
					}
    				$("#export-buttons-table").find('tbody').empty();
    				$("#export-buttons-table").find('tbody').append(markup);
    			$("#paging").empty();		
    			$("#paging").append(content.pageNumber);	
    		  });
	}
	
// var token = $("meta[name='_csrf']").attr("content");
//
//	            var header = $("meta[name='_csrf_header']").attr("content");
//
//	            
//
//	            $(document).ajaxSend(function(e, xhr, options) {
//
//	                xhr.setRequestHeader(header, token);
//
//	              });
            

		function sendingForm(form){
				
	        	var json = form;	
	        	
				$.ajax({										
					url : "/create/saveIcePlant",
					type : "POST",
					contentType : "application/json",
					data : json,
					dataType : 'json',				
					success : function(content) {
						getIPCSPerPage();
    		  		
					document.getElementById("myform").reset();
				  	document.getElementById("preview").src = "";
				  	var img_preview = document.getElementById("img_preview");
				  	var pdf_view = document.getElementById("pdf");
				  	img_preview.style.display = "none";
				   	pdf_view.style.display = "none";
					save = true;
					
					$('.province').find('option').remove().end();
					refreshLocation();
				
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						window.location.replace("/create/ipcsMap/" + content.uniqueKey);
						//window.location.reload(true); 
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					}
				});
			}

   	
//$('#export-buttons-table').delegate('tr', 'click' , function(){
//$("#tbodyID").empty();
//  var uniqueKey = $(this).data('href');
// 	save = false;
//   $.get("/create/getIce/" + uniqueKey, function(data, status){
////                for ( var x = 0; x < data.length; x++) {
////						var obj = data[x];
////						 var withValidSanitaryPermit = obj.withValidSanitaryPermit;
////	            		
////	            		if(withValidSanitaryPermit == 'YES'){
////	            			$('#yesData').show();
////	            		}else{
////	            			$('#yesData').hide();
////	            		}    					
////     				
////	            	}
//			getIceInfoById(data, status);
//			$('.form_input').show();
//    		  });
//   document.querySelector('#submitIcePlantColdStorage').innerHTML = 'UPDATE';     
//});
//
//$('#export-buttons-table').delegate('td.view', 'click', function() {
//	$("#tbodyID").empty();
//	var uniqueKey = $(this).data('href');
//	save = false;
//
//	$.get("/create/getIce/" + uniqueKey, function(data, status) {
//
//		$('.form_input').show();
//		getIceInfoById(data, status);
//	});
//	 document.querySelector('#submitIcePlantColdStorage').innerHTML = 'UPDATE'; 
//});

$('#export-buttons-table').delegate('td.map', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;
	$('#mapview').show();
	window.location.replace("/create/ipcsMap/" + uniqueKey);

});
$('#export-buttons-table').delegate('td.del', 'click', function() {
	$("#tbodyID").empty();
	var id = $(this).data('href');
	save = false;
/*	 let text = "are you sure you want to delete";
	  if (confirm(text) == true) {
			window.location.replace("/create/ipcsMapDeleteById/" + id);
	  } else {
	    text = "You canceled!";
	  }*/
	
	let text;
	  let reason = prompt("Please enter your reason:","");
	  if (reason == null || reason == "") {
	    text = "User cancelled the prompt.";
	  } else {
		  window.location.replace("/create/ipcsMapDeleteById/" + id +"/" + reason);
		  console.log(reason);

	  }
});

$(function() {
	$(".exportToPDF").click(function(e){
		
      var doc = new jsPDF('p', 'pt');

      var elem = document.getElementById('pdfICE');
      var imgElements = document.querySelectorAll('#pdfICE tbody img');
      var data = doc.autoTableHtmlToJson(elem);
      var images = [];
      var i = 0;
     // console.log(imgElements[i].src);
      var img = imgElements[i].src;
      doc.autoTable(data.columns, data.rows, {
       // bodyStyles: {rowHeight: 30},
        drawCell: function(cell, opts) {
          if (opts.column.dataKey === 1) {
            images.push({
              url: img,
              x: cell.textPos.x,
              y: cell.textPos.y
            });
            i++;
          }
        },
        addPageContent: function() {
       // console.log(images.length);
          for (var i = 0; i < images.length; i++) {
          	if(i == images.length-1){
            doc.addImage(images[i].url, images[i].x, images[i].y, 200, 200);
            }
          }
        }
      });

      doc.save("ice.pdf");
         window.location.reload();
	});
});

//	 $(document).ready(function () {
//	 	  $('.icearea').change(function(){
//		    var areaValue = $(this).val();
//		    var regexp = /^\d+(\.\d{1,4})?$/;
//		    if(areaValue == ''){
//		    	return;
//		    }
//		    regexp.test('10.5');
//		    console.log( areaValue + " returns " + regexp.test(areaValue));
//		  if(!regexp.test(areaValue)){
//			  alert("INVALID AREA");
//			  $(".icearea").val("");
//		  }
//
//		});
//	  
//	  
//	 });

			$(function() {
				$(".exportToExcel").click(function(e){
					var table = $(this).prev('.table2excel');
					if(table && table.length){
						var preserveColors = (table.hasClass('table2excel_with_colors') ? true : false);
						$(table).table2excel({
							exclude: ".noExl",
							name: "Excel Document Name",
							filename: "ICEPLANTCOLDSTORAGE.xls",
							fileext: ".xls",
							exclude_img: true,
							exclude_links: true,
							exclude_inputs: true,
							preserveColors: preserveColors
						});
					}
				});
				
			});
function getIPCSList(){
$.get("/create/icecoldList", function(content, status){
        	
        	 var markup = "";
        	 var tr;
       
         			for ( var x = 0; x < content.length; x++) {
         			region = content[x].region;
         			
         			 if(content.length - 1 === x) {
 				        console.log('loop ends');
 				        $(".exportToExcel").show();
 				    }
 				
         			
					tr = $('<tr/>');
					
					tr.append("<td>" + content[x].province + "</td>");
					tr.append("<td>" + content[x].municipality + "</td>");
					tr.append("<td>" + content[x].barangay + "</td>");
					tr.append("<td>" + content[x].nameOfIcePlant + "</td>");
					tr.append("<td>" + content[x].code + "</td>");
					tr.append("<td>" + content[x].area + "</td>");
					tr.append("<td>" + content[x].sanitaryPermit + "</td>");
					tr.append("<td>" + content[x].operator + "</td>");
					tr.append("<td>" + content[x].typeOfFacility + "</td>");
					tr.append("<td>" + content[x].structureType + "</td>");
					tr.append("<td>" + content[x].capacity + "</td>");
					tr.append("<td>" + content[x].withValidLicenseToOperate + "</td>");
					tr.append("<td>" + content[x].withValidSanitaryPermit + "</td>");	
					tr.append("<td>" + content[x].withValidSanitaryPermitYes + "</td>");
					tr.append("<td>" + content[x].withValidSanitaryPermitOthers + "</td>");								
					tr.append("<td>" + content[x].businessPermitsAndCertificateObtained + "</td>");
					tr.append("<td>" + content[x].typeOfIceMaker + "</td>");
					tr.append("<td>" + content[x].foodGradule + "</td>");
					tr.append("<td>" + content[x].dataSource + "</td>");
					tr.append("<td>" + content[x].lat + "</td>");
					tr.append("<td>" + content[x].lon + "</td>");
					tr.append("<td>" + content[x].remarks + "</td>");
					$('#emp_body').append(tr);	
					}    		
    		  });
}
function getPerPage(){   

	  $('#paging').delegate('a', 'click' , function(){

        var $this = $(this),
           target = $this.data('target');    
           
        $.get("/create/iceplantorcoldstorage/" + target, function(content, status){

			  
       	 var markup = "";
        			for ( var x = 0; x < content.icePlantColdStorage.length; x++) {

    					var active = content.icePlantColdStorage[x].enabled;
    	    			if(active){
    	    				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].id + ">ACTIVE</td>"+
    	    				"<td class='map menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].uniqueKey + ">MAP/UPDATE</td>";
    	    			}else{
    	    				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
    	    				"<td class='menu_links btn-primary'>Not applicable</td>";
    	    			}
        			   markup +="<tr>" +
        			   			"<td><span>" + content.icePlantColdStorage[x].region + "</span></td>" +
        			   				"<td>"+content.icePlantColdStorage[x].province+"</td>" +
        			   				"<td>"+content.icePlantColdStorage[x].municipality+"</td>" +
        			   				"<td>"+content.icePlantColdStorage[x].barangay+"</td>" +
        			   				"<td>"+content.icePlantColdStorage[x].nameOfIcePlant+"</td>" +
        			   				"<td>"+content.icePlantColdStorage[x].lat+","+content.icePlantColdStorage[x].lon+"</td>" +
        			   			ShowHide +
        			   				//	"<td class='map menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].uniqueKey + ">MAP/UPDATE</td>" +
        			   			//	"<td class='del menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].id + ">ACTIVE</td>" +
        			   				//"<td class='view menu_links btn-primary' data-href="+ content.icePlantColdStorage[x].uniqueKey + ">View</td>" +		
        			   				"</tr>";
					}
   				$("#export-buttons-table").find('tbody').empty();
   				$("#export-buttons-table").find('tbody').append(markup);
   			$("#paging").empty();		
   			$("#paging").append(content.pageNumber);	
   		  });
      });
      

   }  
      function getIceInfoById(data, status){
    		var content = $("#pdfICE").find('tbody').empty();
    	    for ( var x = 0; x < data.length; x++) {
    			var obj = data[x];
    			
    			getProvinceMunicipalityBarangay(obj); 
    			
    			$(".id").val(obj.id);
    		    $(".user").val(obj.user);
    		    $(".uniqueKey").val(obj.uniqueKey);
    		    $(".dateEncoded").val(obj.dateEncoded);
    		    $(".encodedBy").val(obj.encodedBy);  		 	
    	    $(".nameOfIcePlant").val(obj.nameOfIcePlant);            
    	    $(".sanitaryPermit").val(obj.sanitaryPermit);
    	    $(".operator").val(obj.operator);
    	    $(".typeOfFacility").val(obj.typeOfFacility);
    	    $(".structureType").val(obj.structureType);
    	    $(".capacity").val(obj.capacity);
    	    $(".withValidLicenseToOperate").val(obj.withValidLicenseToOperate);
    	    $(".withValidSanitaryPermit").val(obj.withValidSanitaryPermit);
    	    if(obj.withValidSanitaryPermit == 'Yes'){        	 
    	    	$('#yesData').show(); 
    	    	$('#othersData').hide(); 
    	    }
    	    $(".withValidSanitaryPermitYes").val(obj.withValidSanitaryPermitYes);
    	    
    	    if(obj.withValidSanitaryPermitYes == 'Others'){        	 
    	    	$('#othersData').show();        	        	
    	    }
    	    $(".withValidSanitaryPermitOthers").val(obj.withValidSanitaryPermitOthers);
    	    $(".iceplantbusinessPermitsAndCertificateObtained").val(obj.businessPermitsAndCertificateObtained);
    	    $(".typeOfIceMaker").val(obj.typeOfIceMaker);
    	    $(".foodGradule").val(obj.foodGradule);
    	   // $(".sourceOfEquipment").val(obj.sourceOfEquipment);
    	    $(".dataSource").val(obj.dataSource);
    		$(".area").val(obj.area);
    		$(".code").val(obj.code);
    		$(".dateAsOf").val(obj.dateAsOf);
    		$(".remarks").val(obj.remarks);
    		$(".lat").val(obj.lat);
    		$(".lon").val(obj.lon);
    		$(".image_src").val(obj.image_src);
    		$(".image").val(obj.image);
    		$(".preview").attr("src", obj.image);
    			
    		
    	    var markup = 
    	    "<tr><th scope=\"col\">ICE PLANT/COLD STORAGE LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    	    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
    	    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
    	    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
    	    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
    	    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
    	    "<tr><th scope=\"col\">ICE PLANT/COLD STORAGE INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    	    "<tr><th scope=\"row\">Name of Ice Plant or Cold Storage:</th><td>"+obj.nameOfIcePlant+"</td></tr>"+
    	   "<tr><th scope=\"row\">Valid Sanitary Permit:</th><td>" + obj.sanitaryPermit + "</td></tr>"+
    	   "<tr><th scope=\"row\">Operator:</th><td>" + obj.operator + "</td></tr>"+
    	   "<tr><th scope=\"row\">Type of Facility:</th><td>" + obj.typeOfFacility + "</td></tr>"+
    	  "<tr><th scope=\"row\">Structure Type:</th><td>" + obj.structureType + "</td></tr>"+
    	  "<tr><th scope=\"row\">Capacity:</th><td>" + obj.capacity + "</td></tr>"+
    	  "<tr><th scope=\"row\">With Valid License to Operate:</th><td>" + obj.withValidLicenseToOperate + "</td></tr>"+
    	  "<tr><th scope=\"row\">With Valid Sanitary Permit:</th><td>" + obj.withValidSanitaryPermit + "</td></tr>"+
    	  "<tr><th scope=\"row\">Type Sanitary Permit:</th><td>" + obj.withValidSanitaryPermitYes + "</td></tr>"+
    	  "<tr><th scope=\"row\">Others:</th><td>" + obj.withValidSanitaryPermitOthers + "</td></tr>"+
    	  "<tr><th scope=\"row\">Business Permits and Certificate Obtained:</th><td>" + obj.businessPermitsAndCertificateObtained + "</td></tr>"+
    	  "<tr><th scope=\"row\">Type of Ice Maker:</th><td>" + obj.typeOfIceMaker + "</td></tr>"+
    	  "<tr><th scope=\"row\">Food Gradule:</th><td>" + obj.foodGradule + "</td></tr>"+
    	  "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
    	  
    	  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    	  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
    	  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
    	  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
    	 
    	  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    	  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
    	  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
    	  "<tr><td  scope=\"col\">IMAGE</td><td scope=\"col\">&nbsp;</td></tr>"+
          
    	  "<tr><td scope=\"row\"><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td></tr>"; 
    	 
    	    content.append(markup);
    	    
    	   // reset();
    	    markup = "";
    	    $('#img_preview').show();
    	    $('#pdf').show();
    	    $('#table').hide();	
    	    $('.file').prop('required',false);
    	}

    		
    	}
	  

