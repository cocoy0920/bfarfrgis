  
var save=false;

jQuery(document).ready(function($) {
	
	$(".exportToExcel").hide();
	$('.file').prop('required',true);
	//refreshLocation();
	getPage("nationalcenter");
	getNationalCenterPerPage();
	getNationalCenterList();
	getPerPage();
});
	var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
	$(".submitNationalCenter").click(function(event){

        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
         
          form.classList.add('was-validated'); 
        }
        if (form.checkValidity() === true) {
            event.preventDefault();
            document.getElementById("submitNationalCenter").disabled = true;
            var array = jQuery(form).serializeArray();
        	var json = {};

        	jQuery.each(array, function() {

        		json[this.name] = this.value || '';
        	});
        	
        	var formData = JSON.stringify(json);
        	
        	sendingForm(formData);
        	form.classList.remove('was-validated'); 
          }
        
		});
		});	
    
    var validations = Array.prototype.filter.call(forms, function(form) {
    $(".cancel").click(function(event) {
    	document.getElementById("submitNationalCenter").disabled = false;
    	form.classList.remove('was-validated'); 
    	window.scrollTo(0, 0); 
    	$('.form_input').hide();
   	 $('#mapview').hide();
   	 $('.container').show();
		  
	  });
    });
    $(".create").click(function(event) {
    	document.querySelector('#submitNationalCenter').innerHTML = 'SAVE';   
    	document.getElementById("myform").reset();
	  	document.getElementById("preview").src = "";
	  	var img_preview = document.getElementById("img_preview");
	  	var pdf_view = document.getElementById("pdf");
	   	img_preview.style.display = "none";
	   	pdf_view.style.display = "none";
		save = true;
		$('.province').find('option').remove().end();
		refreshLocation();
    	$('.form_input').show();
    	 $('.container').hide();	
    	 $('#mapview').hide();
		  
	  });
function getNationalCenterPerPage(){

	 $.get("/create/nationalcenter/" + "0", function(content, status){
		 
		 var markup = "";
	 			for ( var x = 0; x < content.center.length; x++) {
	 			  
	 				var active = content.center[x].enabled;
	    			if(active){
	    				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.center[x].id + ">ACTIVE</td>"+
	    				"<td class='map menu_links btn-primary' data-href="+ content.center[x].unique_key + ">MAP/UPDATE</td>";
	    			}else{
	    				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
	    				"<td class='menu_links btn-primary'>Not applicable</td>";
	    			}
	 				markup +="<tr>" +
	 			  "<td><span>" + content.center[x].region + "</span></td>" +
	 			   	"<td>"+content.center[x].province+"</td>" +
	 			   	"<td>"+content.center[x].municipality+"</td>" +
	 			   	"<td>"+content.center[x].barangay+"</td>" +
	 			   "<td>"+content.center[x].name_of_center+"</td>" +
	 			   	"<td>"+content.center[x].lat+","+content.center[x].lon+"</td>" +
	 			   	ShowHide +
	 			   	//  "<td class='map menu_links btn-primary' data-href="+ content.center[x].unique_key + ">MAP/UPDATE</td>" +
	 			 // "<td class='del menu_links btn-primary' data-href="+ content.center[x].id + ">ACTIVE</td>" +
	 			   //"<td class='view menu_links btn-primary' data-href="+ content.center[x].unique_key + ">View</td>" +
	 			   	"</tr>";
				}
				$("#export-buttons-table").find('tbody').empty();
				$("#export-buttons-table").find('tbody').append(markup);
			$("#paging").empty();		
			$("#paging").append(content.pageNumber);	
		  });
}
// var token = $("meta[name='_csrf']").attr("content");
//
//	            var header = $("meta[name='_csrf_header']").attr("content");
//
//	            $(document).ajaxSend(function(e, xhr, options) {
//
//	                xhr.setRequestHeader(header, token);
//
//	              });
	

function sendingForm(form){
	
	        	var json = form;
				$.ajax({										
					url : "/create/saveNationalCenter",
					type : "POST",
					contentType : "application/json",
					data : json,
					dataType : 'json',				
					success : function(content) {
					
						getNationalCenterPerPage();

					document.getElementById("myform").reset();
				  	document.getElementById("preview").src = "";
				  	var img_preview = document.getElementById("img_preview");
				  	var pdf_view = document.getElementById("pdf");
				   	img_preview.style.display = "none";
				   	pdf_view.style.display = "none";
					save = true;
					$('.province').find('option').remove().end();
					refreshLocation();
					
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						window.location.replace("/create/nationalcenterMap/" + content.uniqueKey);
						
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					return;
					}
				});
			}
		
		
//$('#export-buttons-table').delegate('tr', 'click' , function(){
//$("#tbodyID").empty();
//  var uniqueKey = $(this).data('href');
//  save = false;
//   $.get("/create/getNationalCenter/" + uniqueKey, function(data, status){
//        
//	   getNationalCenterInfoById(data, status);
//    		  });
//   document.querySelector('#submitNationalCenter').innerHTML = 'UPDATE';       
//});
//
//$('#export-buttons-table').delegate('td.view', 'click', function() {
//	$("#tbodyID").empty();
//	var uniqueKey = $(this).data('href');
//	save = false;
//
//	$.get("/create/getNationalCenter/" + uniqueKey, function(data, status) {
//
//		$('.form_input').show();
//		getNationalCenterInfoById(data, status);
//	});
//	 document.querySelector('#submitNationalCenter').innerHTML = 'UPDATE'; 
//});

$('#export-buttons-table').delegate('td.map', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;
	
	window.location.replace("/create/nationalcenterMap/" + uniqueKey);

});
$('#export-buttons-table').delegate('td.del', 'click', function() {
	$("#tbodyID").empty();
	var id = $(this).data('href');
	save = false;
/*	 let text = "are you sure you want to delete";
	  if (confirm(text) == true) {
			window.location.replace("/create/nationalcenterDeleteById/" + id);
	  } else {
	    text = "You canceled!";
	  }*/
	let text;
	  let reason = prompt("Please enter your reason:","");
	  if (reason == null || reason == "") {
	    text = "User cancelled the prompt.";
	  } else {
		  window.location.replace("/create/nationalcenterDeleteById/" + id +"/" + reason);
		  console.log(reason);

	  }
});

$(function() {
	$(".exportToPDF").click(function(e){
      var doc = new jsPDF('p', 'pt');

      var elem = document.getElementById('pdfNATIONAL');
      var imgElements = document.querySelectorAll('#pdfNATIONAL tbody img');
      var data = doc.autoTableHtmlToJson(elem);
      var images = [];
      var i = 0;
      console.log(imgElements[i].src);
      var img = imgElements[i].src;
      doc.autoTable(data.columns, data.rows, {
       // bodyStyles: {rowHeight: 30},
        drawCell: function(cell, opts) {
          if (opts.column.dataKey === 1) {
            images.push({
              url: img,
              x: cell.textPos.x,
              y: cell.textPos.y
            });
            i++;
          }
        },
        addPageContent: function() {
        console.log(images.length);
          for (var i = 0; i < images.length; i++) {
          	if(i == images.length-1){
            doc.addImage(images[i].url, 100, images[i].y, 200, 200);
            }
          }
        }
      });

      doc.save("NationalCenter.pdf");
         window.location.reload();
	});
});


 function getPerPage(){   

	  $('#paging').delegate('a', 'click' , function(){
		  var $this = $(this),
         target = $this.data('target');    
          
			 $.get("/create/nationalcenter/" + target, function(content, status){
				 
				 var markup = "";
				 for ( var x = 0; x < content.center.length; x++) {
					 var active = content.center[x].enabled;
		    			if(active){
		    				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.center[x].id + ">ACTIVE</td>"+
		    				"<td class='map menu_links btn-primary' data-href="+ content.center[x].unique_key + ">MAP/UPDATE</td>";
		    			}else{
		    				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
		    				"<td class='menu_links btn-primary'>Not applicable</td>";
		    			}
					 
					 markup +="<tr>" +
		 			  "<td><span>" + content.center[x].region + "</span></td>" +
		 			   	"<td>"+content.center[x].province+"</td>" +
		 			   	"<td>"+content.center[x].municipality+"</td>" +
		 			   	"<td>"+content.center[x].barangay+"</td>" +
		 			   "<td>"+content.center[x].name_of_center+"</td>" +
		 			   	"<td>"+content.center[x].lat+","+content.center[x].lon+"</td>" +
		 			   	ShowHide +
		 			   	//   "<td class='map menu_links btn-primary' data-href="+ content.center[x].unique_key + ">MAP/UPDATE</td>" +
		 			//  "<td class='del menu_links btn-primary' data-href="+ content.center[x].id + ">ACTIVE</td>" +
						
		 			   //"<td class='view menu_links btn-primary' data-href="+ content.center[x].unique_key + ">View</td>" +
		 			   	"</tr>";
					}
						$("#export-buttons-table").find('tbody').empty();
						$("#export-buttons-table").find('tbody').append(markup);
					$("#paging").empty();		
					$("#paging").append(content.pageNumber);	
				  });
       });       
    }
			$(function() {
				$(".exportToExcel").click(function(e){
					var table = $(this).prev('.table2excel');
					if(table && table.length){
						var preserveColors = (table.hasClass('table2excel_with_colors') ? true : false);
						$(table).table2excel({
							exclude: ".noExl",
							name: "Excel Document Name",
							filename: "NATIONAL_CENTER.xls",
							fileext: ".xls",
							exclude_img: true,
							exclude_links: true,
							exclude_inputs: true,
							preserveColors: preserveColors
						});
					}
				});
				
			});
			
function getNationalCenterList(){			
$.get("/create/nationalcenterList", function(content, status){
        		
        	 var markup = "";
        	 var tr;
       
         			for ( var x = 0; x < content.length; x++) {
         			region = content[x].region;
         			
         			 if(content.length - 1 === x) {
 				        $(".exportToExcel").show();
 				    }
 				
					tr = $('<tr/>');
					
					tr.append("<td>" + content[x].province + "</td>");
					tr.append("<td>" + content[x].municipality + "</td>");
					tr.append("<td>" + content[x].barangay + "</td>");
					tr.append("<td>" + content[x].code + "</td>");
					//tr.append("<td>" + content[x].area + "</td>");
					//tr.append("<td>" + content[x].indicateGenus + "</td>");
//					tr.append("<td>" + content[x].culturedMethodUsed + "</td>");
//					tr.append("<td>" + content[x].culturedPeriod + "</td>");
//					tr.append("<td>" + content[x].volumePerCropping + "</td>");
//					tr.append("<td>" + content[x].noOfCroppingPerYear + "</td>");
					tr.append("<td>" + content[x].source_of_data + "</td>");
					tr.append("<td>" + content[x].lat + "</td>");
					tr.append("<td>" + content[x].lon + "</td>");
					tr.append("<td>" + content[x].remarks + "</td>");
					
					$('#emp_body').append(tr);	
					//ExportTable();
					}    		
    		  });
}
		function getNationalCenterInfoById(data, status){
			
			var content = $("#pdfNATIONALCENTER").find('tbody').empty();
			
		    for ( var x = 0; x < data.length; x++) {
				var obj = data[x];
				getProvinceMunicipalityBarangay(obj); 
		    $(".id").val(obj.id);
		    $(".user").val(obj.user);
		    $(".unique_key").val(obj.unique_key);
		    $(".date_encoded").val(obj.date_encoded);
		    $(".encode_by").val(obj.encode_by);
		 	
		        $(".name_of_center").val(obj.name_of_center);
		    	$(".description").val(obj.description);
		    	$(".center_head").val(obj.center_head);
		    	$(".number_of_personnel").val(obj.number_of_personnel);
//		    	$(".noOfCroppingPerYear").val(obj.noOfCroppingPerYear);
		    	$(".dateAsOf").val(obj.date_as_of);
		    	$(".source_of_data").val(obj.source_of_data);
		    	$(".remarks").val(obj.remarks);
		    	$(".code").val(obj.code);		
		    	$(".lat").val(obj.lat);
		    	$(".lon").val(obj.lon);
		    	$(".image_src").val(obj.image_src);
		    	$(".image").val(obj.image);
		    	$(".preview").attr("src", obj.image_src);


		    var markup = 
		    "<tr><th scope=\"col\">NATIONAL CENTER LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
		    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
		    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
		    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
//		    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
		    
		    "<tr><th scope=\"col\">NATIONAL CENTER INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
//		    "<tr><th scope=\"row\">Indicate Genus:</th><td>"+obj.indicateGenus+"</td></tr>"+
//		    
//		   "<tr><th scope=\"row\">Cultured Method Used:</th><td>" + obj.culturedMethodUsed + "</td></tr>"+
//
//		   "<tr><th scope=\"row\">Cultured Period:</th><td>" + obj.culturedPeriod + "</td></tr>"+
		   "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
		   "<tr><th scope=\"col\">PRODUCTION</th><th scope=\"col\">&nbsp;</th></tr>"+
		   
		//   "<tr><th scope=\"row\">Volume per Cropping (Kgs.):</th><td>" + obj.volumePerCropping + "</td></tr>"+
		//  "<tr><th scope=\"row\">No. of Cropping per Year:</th><td>" + obj.noOfCroppingPerYear + "</td></tr>"+
		 
		  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.date_as_of + "</td></tr>"+
		  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.source_of_data + "</td></tr>"+
		  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
		 
		  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
		  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
		  "<tr><td  scope=\"col\">IMAGE</td><td scope=\"col\">&nbsp;</td></tr>"+
          "<tr><td  scope=\"col\"></td><td scope=\"col\">&nbsp;</td></tr>"+       
		  "<tr><td scope=\"col\"><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td></tr>"; 
		 
		    
		   content.append(markup);
		    //reset();
		    
		    markup = "";
		    $('#img_preview').show();
		    $('#pdf').show();
		    $('#table').hide();	
		    $('.file').prop('required',false);
		}

			
		}

