	
var save=false;

jQuery(document).ready(function($) {
	
	$('.file').prop('required',true);
	$(".exportToExcel").hide();
		//refreshLocation();
		getPage("fishcorals");
        getFishCoralList();
        getFishCoralPerPage();
        getPerPage();
});         
        
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
        $(".submitFishCoral").click(function(event){

            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();

              form.classList.add('was-validated'); 
            }
            if (form.checkValidity() === true) {
                event.preventDefault();
                document.getElementById("submitFishCoral").disabled = true;
                var array = jQuery(form).serializeArray();
            	var json = {};

            	jQuery.each(array, function() {

            		json[this.name] = this.value || '';
            	});
            	
            	var formData = JSON.stringify(json);
            	
            	sendingForm(formData);
            	form.classList.remove('was-validated'); 
              }
            
        	});
    		});
        var validations = Array.prototype.filter.call(forms, function(form) {
        $(".cancel").click(function(event) {
        	document.getElementById("submitFishCoral").disabled = false;
        	form.classList.remove('was-validated'); 
        	window.scrollTo(0, 0); 
        	 $('.form_input').hide();
        	 $('#mapview').hide();	
        	 $('.container').show();
		  });
        });
        $(".create").click(function(event) { 
        	$('#mapview').hide(); 
        	$('.container').hide();
        	document.querySelector('#submitFishCoral').innerHTML = 'SAVE'; 
        	document.getElementById("myform").reset();
          	document.getElementById("preview").src = "";
          	var img_preview = document.getElementById("img_preview");
          	var pdf_view = document.getElementById("pdf");
           	img_preview.style.display = "none";
           	pdf_view.style.display = "none";
        	save = true;
        	$('.province').find('option').remove().end();
        	refreshLocation();
        	$('.form_input').show();
   		 	   	
   		 	 
   	  	});


function getFishCoralPerPage(){

      		 $.get("/create/fishcorals/" + 0, function(content, status){
        	 
    	 
    	 var markup = "";
     			for ( var x = 0; x < content.fishCoral.length; x++) {
        			var active = content.fishCoral[x].enabled;
        			if(active){
        				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.fishCoral[x].id + ">ACTIVE</td>"+
        				"<td class='map menu_links btn-primary' data-href="+ content.fishCoral[x].uniqueKey + ">MAP/UPDATE</td>";
        			}else{
        				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
        				"<td class='menu_links btn-primary'>Not applicable</td>";
        			}
     			   markup +="<tr>" +
     			  "<td><span>" + content.fishCoral[x].region + "</span></td>" +
     			   		"<td>"+content.fishCoral[x].province+"</td>" +
     			   		"<td>"+content.fishCoral[x].municipality+"</td>" +
     			   		"<td>"+content.fishCoral[x].barangay+"</td>" +
     			   		"<td>"+content.fishCoral[x].nameOfOperator+"</td>" +
     			   		"<td>"+content.fishCoral[x].lat+ "," +content.fishCoral[x].lon+ "</td>" +
     			   		ShowHide +
     			   		//"<td class='map menu_links btn-primary' data-href="+ content.fishCoral[x].uniqueKey + ">MAP/UPDATE</td>" +
     			   		//"<td class='del menu_links btn-primary' data-href="+ content.fishCoral[x].id + ">ACTIVE</td>" +
     			   		//"<td class='view menu_links btn-primary' data-href="+ content.fishCoral[x].uniqueKey + ">View</td>" +
     			   		"</tr>";
				}
				$("#export-buttons-table").find('tbody').empty();
				$("#export-buttons-table").find('tbody').append(markup);
				$("#paging").empty();		
				$("#paging").append(content.pageNumber);	
		  });
}

//var token = $("meta[name='_csrf']").attr("content");
//
//var header = $("meta[name='_csrf_header']").attr("content");
//
//
//
//$(document).ajaxSend(function(e, xhr, options) {
//
//    xhr.setRequestHeader(header, token);
//
//  });


function sendingForm(form){
var json = form;	
$.ajax({	
	url : "/create/saveFishCoral",
	type : "POST",
	contentType : "application/json",
	data : json,
	dataType : 'json',				
	success : function(content) {
		
		getFishCoralPerPage();
		
	document.getElementById("myform").reset();
  	document.getElementById("preview").src = "";
  	var img_preview = document.getElementById("img_preview");
  	var pdf_view = document.getElementById("pdf");
   	img_preview.style.display = "none";
   	pdf_view.style.display = "none";
	save = true;
	$('.province').find('option').remove().end();
	refreshLocation();
		$(".se-pre-con").hide();
		alert("SUCCESSFULLY SAVE");
		window.location.replace("/create/fishcoralMap/" + content.uniqueKey);
		//window.location.reload(true); 
		//window.location.reload();
		
	},
	error: function(data){
	console.log("error", data);
	$(".se-pre-con").hide();
	}
});
}

//$('#export-buttons-table').delegate('tr', 'click' , function(){
//$("#tbodyID").empty();
//  var uniqueKey = $(this).data('href');
//    $.get("/create/getFishCoral/" + uniqueKey, function(data, status){
//        	
//			getFishCoralInfoById(data, status);
//    		  });
//    document.querySelector('#submitFishCoral').innerHTML = 'UPDATE';
//});

//$('#export-buttons-table').delegate('td.view', 'click', function() {
//	$("#tbodyID").empty();
//	var uniqueKey = $(this).data('href');
//	save = false;
//
//	$.get("/create/getFishCoral/" + uniqueKey, function(data, status) {
//
//		$('.form_imput').show();
//		getFishCoralInfoById(data, status);
//	});
//	 document.querySelector('#submitFishCoral').innerHTML = 'UPDATE'; 
//});

//function viewDeatails(uniqueKey) {
//
//	save = false;
//
//	$.get("/create/getFishCoral/" + uniqueKey, function(data, status) {
//
//		$('.form_input').show();
//		getFishCoralInfoById(data, status);
//	});
//	 document.querySelector('#submitFishCoral').innerHTML = 'UPDATE'; 
//}

$('#export-buttons-table').delegate('td.map', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;
	$('#mapview').show();
	window.location.replace("/create/fishcoralMap/" + uniqueKey);

});

$('#export-buttons-table').delegate('td.del', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;
	/* let text = "are you sure you want to delete";
	  if (confirm(text) == true) {
			window.location.replace("/create/fishcoralDeleteById/" + uniqueKey);
	  } else {
	    text = "You canceled!";
	  }*/
	
	let text;
	  let reason = prompt("Please enter your reason:","");
	  if (reason == null || reason == "") {
	    text = "User cancelled the prompt.";
	  } else {
		  window.location.replace("/create/fishcoralDeleteById/" + id +"/" + reason);
		  console.log(reason);

	  }
});


$(function() {
	$(".exportToPDF").click(function(e){
      var doc = new jsPDF('p', 'pt');

      var elem = document.getElementById('pdfFISHCORAL');
      var imgElements = document.querySelectorAll('#pdfFISHCORAL tbody img');
      var data = doc.autoTableHtmlToJson(elem);
      var images = [];
      var i = 0;

      var img = imgElements[i].src;
      doc.autoTable(data.columns, data.rows, {
       // bodyStyles: {rowHeight: 30},
        drawCell: function(cell, opts) {
          if (opts.column.dataKey === 1) {
            images.push({
              url: img,
              x: cell.textPos.x,
              y: cell.textPos.y
            });
            i++;
          }
        },
        addPageContent: function() {

          for (var i = 0; i < images.length; i++) {
          	if(i == images.length-1){
            doc.addImage(images[i].url, 100, images[i].y, 200, 200);
            }
          }
        }
      });

      doc.save("fishcoral.pdf");
         window.location.reload();
	});
});

//	 $(document).ready(function () {
//	 	  $('.fishcoralarea').change(function(){
//		    var areaValue = $(this).val();
//		    var regexp = /^\d+(\.\d{1,4})?$/;
//		    if(areaValue == ''){
//		    	return;
//		    }
//		    regexp.test('10.5');
//		    console.log( areaValue + " returns " + regexp.test(areaValue));
//		  if(!regexp.test(areaValue)){
//			  alert("INVALID AREA");
//			  $(".fishcoralarea").val("");
//		  }
//
//		});
//	  
//	  
//	 });


$(function() {
				$(".exportToExcel").click(function(e){
					var table = $(this).prev('.table2excel');
					if(table && table.length){
						var preserveColors = (table.hasClass('table2excel_with_colors') ? true : false);
						$(table).table2excel({
							exclude: ".noExl",
							name: "Excel Document Name",
							filename: "FishCoral.xls",
							fileext: ".xls",
							exclude_img: true,
							exclude_links: true,
							exclude_inputs: true,
							preserveColors: preserveColors
						});
					}
				});
				
			});

function getFishCoralList(){			
$.get("/create/fishcoralList", function(content, status){
        	
        	 var markup = "";
        	 var tr;
       
         			for ( var x = 0; x < content.length; x++) {
         			region = content[x].region;
         			
         			 if(content.length - 1 === x) {
 				        $(".exportToExcel").show();
 				    }
 				
         			
					tr = $('<tr/>');
					
					tr.append("<td>" + content[x].province + "</td>");
					tr.append("<td>" + content[x].municipality + "</td>");
					tr.append("<td>" + content[x].barangay + "</td>");
					tr.append("<td>" + content[x].nameOfOperator + "</td>");
					tr.append("<td>" + content[x].code + "</td>");
					tr.append("<td>" + content[x].area + "</td>");
					tr.append("<td>" + content[x].operatorClassification + "</td>");
					tr.append("<td>" + content[x].nameOfStationGearUse + "</td>");
					tr.append("<td>" + content[x].noOfUnitUse + "</td>");
					tr.append("<td>" + content[x].fishesCaught + "</td>");
					tr.append("<td>" + content[x].dataSource + "</td>");
					tr.append("<td>" + content[x].lat + "</td>");
					tr.append("<td>" + content[x].lon + "</td>");
					tr.append("<td>" + content[x].remarks + "</td>");
					
					$('#emp_body').append(tr);	
					//ExportTable();
					}    		
    		  });
}

 

function getPerPage(){   
	    
	  $('#paging').delegate('a', 'click' , function(){
	  
       var $this = $(this),
          target = $this.data('target');    
       	
		 $.get("/create/fishcorals/" + target, function(content, status){
       	 
	    	 
	    	 var markup = "";
	     			for ( var x = 0; x < content.fishCoral.length; x++) {
	     				
	     				var active = content.fishCoral[x].enabled;
	        			if(active){
	        				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.fishCoral[x].id + ">ACTIVE</td>"+
	        				"<td class='map menu_links btn-primary' data-href="+ content.fishCoral[x].uniqueKey + ">MAP/UPDATE</td>";
	        			}else{
	        				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
	        				"<td class='menu_links btn-primary'>Not applicable</td>";
	        			}
	     			   markup +="<tr>" +
	     			  "<td><span>" + content.fishCoral[x].region + "</span></td>" +
	     			   		"<td>"+content.fishCoral[x].province+"</td>" +
	     			   		"<td>"+content.fishCoral[x].municipality+"</td>" +
	     			   		"<td>"+content.fishCoral[x].barangay+"</td>" +
	     			   		"<td>"+content.fishCoral[x].nameOfOperator+"</td>" +
	     			   		"<td>"+content.fishCoral[x].lat+ "," +content.fishCoral[x].lon+ "</td>" +
	     			   		ShowHide +
	     			   			"</tr>";
					}
					$("#export-buttons-table").find('tbody').empty();
					$("#export-buttons-table").find('tbody').append(markup);
					$("#paging").empty();		
					$("#paging").append(content.pageNumber);	
			  });;
     });
     

  }


function getFishCoralInfoById(data, status,resProvince ){
			
		    for ( var x = 0; x < data.length; x++) {
				var obj = data[x];
				
				getProvinceMunicipalityBarangay(obj); 
				
				$(".id").val(obj.id);
			    $(".user").val(obj.user);
			    $(".uniqueKey").val(obj.uniqueKey);
			    $(".dateEncoded").val(obj.dateEncoded);
			    $(".encodedBy").val(obj.encodedBy);
			 	
		    $(".fishcoralnameOfOperator").val(obj.nameOfOperator);         
		    $(".fishcoraloperatorClassification").val(obj.operatorClassification);
		    $(".nameOfStationGearUse").val(obj.nameOfStationGearUse);
		    $(".noOfUnitUse").val(obj.noOfUnitUse);
		    $(".fishesCaught").val(obj.fishesCaught);
		    $(".dataSource").val(obj.dataSource);
			$(".area").val(obj.area);
			$(".code").val(obj.code);
			$(".dateAsOf").val(obj.dateAsOf);
			$(".remarks").val(obj.remarks);
			$(".lat").val(obj.lat);
			$(".lon").val(obj.lon);
			$(".image_src").val(obj.image_src);
			$(".image").val(obj.image);
			$(".preview").attr("src",obj.image);


		    var markup = 
		    "<tr><th scope=\"col\">FISH CORAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
		    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
		    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
		    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
		    "<tr><th scope=\"row\">Code</th><td>" + obj.code + "</td></tr>"+
		    "<tr><th scope=\"col\">FISH CORAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		    "<tr><th scope=\"row\">Name of Operator:</th><td>"+obj.nameOfOperator+"</td></tr>"+
		   
		   "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
		   "<tr><th scope=\"row\">Operator Classification:</th><td>" + obj.operatorClassification + "</td></tr>"+
		   "<tr><th scope=\"row\">Name of Station Gear Use:</th><td>" + obj.nameOfStationGearUse + "</td></tr>"+
		  "<tr><th scope=\"row\">Name of Unite Use:</th><td>" + obj.noOfUnitUse + "</td></tr>"+
		  "<tr><th scope=\"row\">Fishes Caught:</th><td>" + obj.fishesCaught + "</td></tr>"+
		  
		   "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
		  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
		  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
		  
		  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
		  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
		  "<tr><td  scope=\"col\">IMAGE</td><td scope=\"col\">&nbsp;</td></tr>"+
          "<tr><td  scope=\"col\"></td><td scope=\"col\">&nbsp;</td></tr>"+
          
		  "<tr><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td></tr>"; 
		 
		    $("#pdfFISHCORAL").find('tbody').append(markup);
		    
		   // reset();
		    markup = "";
		    $('#img_preview').show();
		    $('#pdf').show();
		    $('#table').hide();	
		    $('.file').prop('required',false);
		}

			
		}
