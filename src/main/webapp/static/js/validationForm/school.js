var save=false;


jQuery(document).ready(function($) {
	
	$('.file').prop('required',true);
	$(".exportToExcel").hide();
			//refreshLocation();
			getPage("schooloffisheries");
			getSchoolPerPage();
			 getSchoolList();
			 getPerPage();
});			 
			 
				var forms = document.getElementsByClassName('needs-validation');
			    // Loop over them and prevent submission
			    var validation = Array.prototype.filter.call(forms, function(form) {
			 $(".submitSchoolOfFisheries").click(function(event) {

			        if (form.checkValidity() === false) {
			          event.preventDefault();
			          event.stopPropagation();
			         
			          form.classList.add('was-validated'); 
			        }
			        if (form.checkValidity() === true) {
			            event.preventDefault();
			            document.getElementById("submitSchoolOfFisheries").disabled = true;
			            var array = jQuery(form).serializeArray();
			        	var json = {};

			        	jQuery.each(array, function() {

			        		json[this.name] = this.value || '';
			        	});
			        	
			        	var formData = JSON.stringify(json);
			        	
			        	sendingForm(formData);
			        	form.classList.remove('was-validated'); 
			          }
			        
			 		});
					});

			    var validations = Array.prototype.filter.call(forms, function(form) {
			    $(".cancel").click(function(event) {
			    	document.getElementById("submitSchoolOfFisheries").disabled = false;
			    	form.classList.remove('was-validated'); 
			    	window.scrollTo(0, 0);
			    	 $('.form_input').hide();
			    	 $('.container').show();
			    	 $('#mapview').hide();
					  
				  });
			    });
			    $(".create").click(function(event) {
			    	document.querySelector('#submitSchoolOfFisheries').innerHTML = 'SAVE'; 
			    	document.getElementById("myform").reset();
					document.getElementById("preview").src = "";
					var img_preview = document
							.getElementById("img_preview");
					img_preview.style.display = "none";

					save = true;
					$('.province').find('option').remove().end();
					refreshLocation();
			    	 $('.form_input').show();
			    	 $('.container').hide();
			    	 $('#mapview').hide();
					  
				  });
			    
function getSchoolPerPage(){

	$.get("/create/schooloffisheries/" + "0", function(content, status) {

		var markup = "";
		for (var x = 0; x < content.schoolOfFisheries.length; x++) {
			
			var active = content.schoolOfFisheries[x].enabled;
			if(active){
				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].id + ">ACTIVE</td>"+
				"<td class='map menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].uniqueKey + ">MAP/UPDATE</td>";
			}else{
				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
				"<td class='menu_links btn-primary'>Not applicable</td>";
			}
			markup += "<tr>" +
					"<td><span>" + content.schoolOfFisheries[x].region + "</span></td>" +
							
					"<td>"+ content.schoolOfFisheries[x].province
					+ "</td><td>"
					+ content.schoolOfFisheries[x].municipality
					+ "</td><td>"
					+ content.schoolOfFisheries[x].barangay
					+ "</td><td>" + content.schoolOfFisheries[x].name
					+ "</td><td>" + content.schoolOfFisheries[x].lat
					+ "," + content.schoolOfFisheries[x].lon
					+ "</td>" +
					ShowHide +
					//"<td class='map menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].uniqueKey + ">MAP/UPDATE</td>" +
					//"<td class='del menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].id + ">ACTIVE</td>" +
					//"<td class='view menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].uniqueKey + ">View</td>" +
					
							"</tr>";
		}
		$("#export-buttons-table").find('tbody').empty();
		$("#export-buttons-table").find('tbody').append(markup);
		$("#paging").empty();
		$("#paging").append(content.pageNumber);
	});
}

//var token = $("meta[name='_csrf']").attr("content");
//
//var header = $("meta[name='_csrf_header']").attr("content");
//
//$(document).ajaxSend(function(e, xhr, options) {
//
//	xhr.setRequestHeader(header, token);
//
//});

function sendingForm(form){
		var json = form;
					$.ajax({																
								url : "/create/saveSchool",
								type : "POST",
								contentType : "application/json",
								data : json,
								dataType : 'json',
								success : function(content) {
									
									getSchoolPerPage();
									document.getElementById("myform").reset();
									document.getElementById("preview").src = "";
									var img_preview = document
											.getElementById("img_preview");
									img_preview.style.display = "none";

									save = true;
									$('.province').find('option').remove().end();
									refreshLocation();

									$(".se-pre-con").hide();
									alert("SUCCESSFULLY SAVE");
									window.location.replace("/create/schoolMap/" + content.uniqueKey);
								},
								error : function(data) {
									console.log("error", data);
									$(".se-pre-con").hide();
								}
							});
				}

 	
//$('#export-buttons-table').delegate('tr', 'click', function() {
//	$("#tbodyID").empty();
//	var uniqueKey = $(this).data('href');
//	save=false;
//	$.get("/create/getSchool/" + uniqueKey, function(data, status) {
//
//		getSchoolInfoById(data, status);
//	});
//	 document.querySelector('#submitSchoolOfFisheries').innerHTML = 'UPDATE';  
//});
//$('#export-buttons-table').delegate('td.view', 'click', function() {
//	$("#tbodyID").empty();
//	var uniqueKey = $(this).data('href');
//	save = false;
//
//	$.get("/create/getSchool/" + uniqueKey, function(data, status) {
//
//		$('.create').show();
//		getSchoolInfoById(data, status);
//	});
//	 document.querySelector('#submitSchoolOfFisheries').innerHTML = 'UPDATE'; 
//});

$('#export-buttons-table').delegate('td.map', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;
	$('#mapview').show();
	window.location.replace("/create/schoolMap/" + uniqueKey);

});
$('#export-buttons-table').delegate('td.del', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;
/*	 let text = "are you sure you want to delete";
	  if (confirm(text) == true) {
			window.location.replace("/create/schoolDeleteById/" + uniqueKey);
	  } else {
	    text = "You canceled!";
	  }*/
	let text;
	  let reason = prompt("Please enter your reason:","");
	  if (reason == null || reason == "") {
	    text = "User cancelled the prompt.";
	  } else {
		  window.location.replace("/create/schoolDeleteById/" + id +"/" + reason);
		  console.log(reason);

	  }
});

$(function() {
	$(".exportToPDF").click(function(e){
	var doc = new jsPDF('p', 'pt');

	var elem = document.getElementById('pdfSCHOOLOFFISHERIES');
	var imgElements = document
			.querySelectorAll('#pdfSCHOOLOFFISHERIES tbody img');
	var data = doc.autoTableHtmlToJson(elem);
	var images = [];
	var i = 0;
	//console.log(imgElements[i].src);
	var img = imgElements[i].src;
	doc.autoTable(data.columns, data.rows, {
		// bodyStyles: {rowHeight: 30},
		drawCell : function(cell, opts) {
			if (opts.column.dataKey === 1) {
				images.push({
					url : img,
					x : cell.textPos.x,
					y : cell.textPos.y
				});
				i++;
			}
		},
		addPageContent : function() {
			//console.log(images.length);
			for (var i = 0; i < images.length; i++) {
				if (i == images.length - 1) {
					doc.addImage(images[i].url, 100, images[i].y, 200,
							200);
				}
			}
		}
	});

	doc.save("school.pdf");
	window.location.reload();
	});
});

//$(document).ready(function() {
//	$('.schoolarea').change(function() {
//		var areaValue = $(this).val();
//		var regexp = /^\d+(\.\d{1,4})?$/;
//		if (areaValue == '') {
//			return;
//		}
//		regexp.test('10.5');
//		console.log(areaValue + " returns " + regexp.test(areaValue));
//		if (!regexp.test(areaValue)) {
//			alert("INVALID AREA");
//			$(".schoolarea").val("");
//		}
//
//	});
//
//});

$(function() {
	$(".exportToExcel")
			.click(
					function(e) {
						var table = $(this).prev('.table2excel');
						if (table && table.length) {
							var preserveColors = (table
									.hasClass('table2excel_with_colors') ? true
									: false);
							$(table).table2excel({
								exclude : ".noExl",
								name : "Excel Document Name",
								filename : "SchoolofFisheries.xls",
								fileext : ".xls",
								exclude_img : true,
								exclude_links : true,
								exclude_inputs : true,
								preserveColors : preserveColors
							});
						}
					});

});
function getSchoolList(){
$.get("/create/schoolList",function(content, status) {

					var markup = "";
					var tr;

					for (var x = 0; x < content.length; x++) {
						
						 if(content.length - 1 === x) {
						        console.log('loop ends');
						        $(".exportToExcel").show();
						    }
						
						
						tr = $('<tr/>');

						tr.append("<td>"
								+ content[x].province
								+ "</td>");
						tr.append("<td>"
								+ content[x].municipality
								+ "</td>");
						tr.append("<td>"
								+ content[x].barangay
								+ "</td>");
						tr.append("<td>" + content[x].code
								+ "</td>");
						tr.append("<td>" + content[x].area
								+ "</td>");
						tr.append("<td>" + content[x].name
								+ "</td>");
						tr.append("<td>"
								+ content[x].dateEstablished
								+ "</td>");
						tr.append("<td>"
										+ content[x].numberStudentsEnrolled
										+ "</td>");
						tr.append("<td>"
								+ content[x].dataSource
								+ "</td>");
						tr.append("<td>" + content[x].lat
								+ "</td>");
						tr.append("<td>" + content[x].lon
								+ "</td>");
						tr.append("<td>" + content[x].remarks
								+ "</td>");

						$('#emp_body').append(tr);
						// ExportTable();
					}
				});
}
function getPerPage() {
	$('#paging').delegate('a','click',function() {
			var $this = $(this), 
			target = $this.data('target');

			$.get("/create/schooloffisheries/" + target, function(content, status) {

				var markup = "";
				for (var x = 0; x < content.schoolOfFisheries.length; x++) {
					
					var active = content.schoolOfFisheries[x].enabled;
					if(active){
						ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].id + ">ACTIVE</td>"+
						"<td class='map menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].uniqueKey + ">MAP/UPDATE</td>";
					}else{
						ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
						"<td class='menu_links btn-primary'>Not applicable</td>";
					}
					markup += "<tr>" +
							"<td><span>" + content.schoolOfFisheries[x].region + "</span></td><td>" +
							+ content.schoolOfFisheries[x].province
							+ "</td><td>"
							+ content.schoolOfFisheries[x].municipality
							+ "</td><td>"
							+ content.schoolOfFisheries[x].barangay
							+ "</td><td>" + content.schoolOfFisheries[x].name
							+ "</td><td>" + content.schoolOfFisheries[x].lat
							+ "," + content.schoolOfFisheries[x].lon
							+ "</td>" +
							ShowHide +
							//"<td class='map menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].uniqueKey + ">MAP/UPDATE</td>" +
						//	"<td class='del menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].id + ">ACTIVE</td>" +
							//"<td class='view menu_links btn-primary' data-href="+ content.schoolOfFisheries[x].uniqueKey + ">View</td>" +
									
							"</tr>";
				}
				$("#export-buttons-table").find('tbody').empty();
				$("#export-buttons-table").find('tbody').append(markup);
				$("#paging").empty();
				$("#paging").append(content.pageNumber);
			});
	});
}

function getSchoolInfoById(data, status ){
	
	//var content = ("#pdfSCHOOLOFFISHERIES").find('tbody').empty();
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		 getProvinceMunicipalityBarangay(obj); 
		 
		$(".id").val(obj.id);
	    $(".user").val(obj.user);
	    $(".uniqueKey").val(obj.uniqueKey);
	    $(".dateEncoded").val(obj.dateEncoded);
	    $(".encodedBy").val(obj.encodedBy);	 	 
    $(".schoolname").val(obj.name); 
    $(".dateEstablished").val(obj.dateEstablished);
    $(".numberStudentsEnrolled").val(obj.numberStudentsEnrolled); 
    $(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image_src);
	$(".image").val(obj.image);
	$(".preview").attr("src",obj.image);


    var markup = 
    "<tr><th scope=\"col\">FISHERIES LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
    "<tr><th scope=\"col\">FISHERIES INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">Name of School:</th><td>"+obj.name+"</td></tr>"+
   "<tr><th scope=\"row\">Date Established:</th><td>" + obj.dateEstablished + "</td></tr>"+ 
   "<tr><th scope=\"row\">Number of Student Enrolled:</th><td>" + obj.numberStudentsEnrolled + "</td></tr>"+
 "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
 
  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
  
  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
  "<tr><td  scope=\"col\">IMAGE</td><td scope=\"col\">&nbsp;</td></tr>"+
  "<tr><td  scope=\"col\"></td><td scope=\"col\">&nbsp;</td></tr>"+
  "<tr><td   scope=\"col\"><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td></tr>"; 
 
    $("#pdfSCHOOLOFFISHERIES").find('tbody').append(markup);
   //content.append(markup);
    
   // reset();
    markup = "";
    $('#img_preview').show();
    $('#pdf').show();
    $('#table').hide();		
    $('.file').prop('required',false);
}

	
}

