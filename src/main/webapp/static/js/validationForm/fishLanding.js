
var save = false;


jQuery(document).ready(function($) {
	
	$('.file').prop('required',true);
	$(".exportToExcel").hide();
	$("#cflc_status").hide();
			//refreshLocation();
			getPage("fishlanding");			
			getFishLandingList();
			getFishlandingByPage();
			getPerPage();
});
			var forms = document.getElementsByClassName('needs-validation');
		    // Loop over them and prevent submission
		    var validation = Array.prototype.filter.call(forms, function(form) {
			$(".submitFishLanding").click(function(event) {

		        if (form.checkValidity() === false) {
		          event.preventDefault();
		          event.stopPropagation();
		
		          form.classList.add('was-validated'); 
		        }
		        if (form.checkValidity() === true) {
		            event.preventDefault();
		            document.getElementById("submitFishLanding").disabled = true;
		            var array = jQuery(form).serializeArray();
		        	var json = {};

		        	jQuery.each(array, function() {

		        		json[this.name] = this.value || '';
		        	});
		        	
		        	var formData = JSON.stringify(json);
		        	
		        	sendingForm(formData);
		        	form.classList.remove('was-validated'); 
		          }
		        
				});
				});

		    var validations = Array.prototype.filter.call(forms, function(form) {
		    $(".cancel").click(function(event) {
		    	document.getElementById("submitFishLanding").disabled = false;
		    	form.classList.remove('was-validated'); 
		    	window.scrollTo(0, 0);
		    	 $('.form_input').hide();
            	 $('#mapview').hide();
            	 $('.container').show();
				  
			  });
		    });
		    $(".create").click(function(event) {
				  
		    	document.querySelector('#submitFishLanding').innerHTML = 'SAVE'; 
		    	document.getElementById("myform").reset();
				document.getElementById("preview").src = "";
				var img_preview = document.getElementById("img_preview");
				var pdf_view = document.getElementById("pdf");
				img_preview.style.display = "none";
				pdf_view.style.display = "none";
				save = true;
				$('.province').find('option').remove().end();
				
				refreshLocation();
				 $('.form_input').show();
				 $('#mapview').hide();
				 $("#cflc_status").hide();
				 $('.container').hide();
				 window.scrollTo(0, 0);
			  });

		    
$("#type").change(function(){
	var value = $("#type").val();
	
	if(value == "CFLC"){
		$("#cflc_status").show();
	}
	
});
		    
function getFishlandingByPage(){


	$.get("/create/fishlandings/" + 0, function(content, status) {

		var markup = "";
		for (var x = 0; x < content.fishLanding.length; x++) {
			var active = content.fishLanding[x].enabled;
			if(active){
				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.fishLanding[x].id + ">ACTIVE</td>"+
				"<td class='map menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">MAP/UPDATE</td>";
			}else{
				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
				"<td class='menu_links btn-primary'>Not applicable</td>";
			}
			markup += "<tr>" +
					"<td><span>" + content.fishLanding[x].region + "</span><td>"
					+ content.fishLanding[x].province + "</td><td>"
					+ content.fishLanding[x].municipality + "</td><td>"
					+ content.fishLanding[x].barangay + "</td><td>"
					+ content.fishLanding[x].nameOfLanding
					+ "</td><td>" + content.fishLanding[x].lat + ","
					+ content.fishLanding[x].lon + "</td>" +
					ShowHide +
					//"<td class='map menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">MAP/UPDATE</td>" +
					//"<td class='del menu_links btn-primary' data-href="+ content.fishLanding[x].id + ">ACTIVE</td>" +
					//	"<td class='view menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">View</td>" +
					"</tr>";
		}
		$("#export-buttons-table").find('tbody').empty();
		$("#export-buttons-table").find('tbody').append(markup);
		$("#paging").empty();
		$("#paging").append(content.pageNumber);
	});
}
//var token = $("meta[name='_csrf']").attr("content");
//var header = $("meta[name='_csrf_header']").attr("content");
//
//$(document).ajaxSend(function(e, xhr, options) {
//	xhr.setRequestHeader(header, token);
//});



function sendingForm(form) {
	var json = form;

	$.ajax({
		url : "/create/saveFishLanding",
		type : "POST",
		contentType : "application/json",
		dataType : 'json',
		data : json,

		success : function(content) {

			getFishlandingByPage();
			

			document.getElementById("myform").reset();
			document.getElementById("preview").src = "";
			var img_preview = document.getElementById("img_preview");
			var pdf_view = document.getElementById("pdf");
			img_preview.style.display = "none";
			pdf_view.style.display = "none";
			save = true;
			$('.province').find('option').remove().end();
			
			refreshLocation();

			$(".se-pre-con").hide();
			alert("SUCCESSFULLY SAVE");
			window.location.replace("/create/fishLandingMap/" + content.uniqueKey);
		},
		error : function(data) {
			console.log("error", data);
			$(".se-pre-con").hide();
		}
	});
}


//$('#export-buttons-table').delegate('tr', 'click', function() {
//	$("#tbodyID").empty();
//	var uniqueKey = $(this).data('href');
//	save = false;
//	$.get("/create/getFishLanding/" + uniqueKey, function(data, status) {
//
//		getFishLandingInfoById(data, status);
//	});
//	document.querySelector('#submitFishLanding').innerHTML = 'UPDATE';
//});

//$('#export-buttons-table').delegate('td.view', 'click', function() {
//	$("#tbodyID").empty();
//	var uniqueKey = $(this).data('href');
//	save = false;
//
//	$.get("/create/getFishLanding/" + uniqueKey, function(data, status) {
//
//		$('.create').show();
//		getFishLandingInfoById(data, status);
//	});
//	 document.querySelector('#submitFishLanding').innerHTML = 'UPDATE'; 
//});

$('#export-buttons-table').delegate('td.map', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	
	save = false;
	$('#mapview').show();
	window.location.replace("/create/fishLandingMap/" + uniqueKey);

});

$('#export-buttons-table').delegate('td.del', 'click', function() {
	$("#tbodyID").empty();
	var id = $(this).data('href');
	save = false;
	/* let text = "are you sure you want to delete";
	  if (confirm(text) == true) {
			window.location.replace("/create/fishLandingDeleteById/" + id);
	  } else {
	    text = "You canceled!";
	  }*/
	
	let text;
	  let reason = prompt("Please enter your reason:","");
	  if (reason == null || reason == "") {
	    text = "User cancelled the prompt.";
	  } else {
		  window.location.replace("/create/fishLandingDeleteById/" + id +"/" + reason);
		  console.log(reason);

	  }
});


$(function() {
	$(".exportToPDF").click(function(e){
	var doc = new jsPDF('p', 'pt');

	var elem = document.getElementById('pdfFISHLANDING');
	var imgElements = document.querySelectorAll('#pdfFISHLANDING tbody img');
	var data = doc.autoTableHtmlToJson(elem);
	var images = [];
	var i = 0;

	var img = imgElements[i].src;
	doc.autoTable(data.columns, data.rows, {
		// bodyStyles: {rowHeight: 30},
		drawCell : function(cell, opts) {
			if (opts.column.dataKey === 1) {
				images.push({
					url : img,
					x : cell.textPos.x,
					y : cell.textPos.y
				});
				i++;
			}
		},
		addPageContent : function() {

			for (var i = 0; i < images.length; i++) {
				if (i == images.length - 1) {
					doc.addImage(images[i].url, 100, images[i].y, 200, 200);
				}
			}
		}
	});

	doc.save("fishLanding.pdf");
	window.location.reload();
	});
});


//$(document).ready(function() {
//	$('.fishLandingarea').change(function() {
//		var areaValue = $(this).val();
//		var regexp = /^\d+(\.\d{1,4})?$/;
//		if (areaValue == '') {
//			return;
//		}
//		regexp.test('10.5');
//		console.log(areaValue + " returns " + regexp.test(areaValue));
//		if (!regexp.test(areaValue)) {
//			alert("INVALID AREA");
//			$(".fishLandingarea").val("");
//		}
//
//	});
//
//});

$(function() {
	$(".exportToExcel")
			.click(
					function(e) {
						var table = $(this).prev('.table2excel');
						if (table && table.length) {
							var preserveColors = (table
									.hasClass('table2excel_with_colors') ? true
									: false);
							$(table).table2excel({
								exclude : ".noExl",
								name : "Excel Document Name",
								filename : "FishLanding.xls",
								fileext : ".xls",
								exclude_img : true,
								exclude_links : true,
								exclude_inputs : true,
								preserveColors : preserveColors
							});
						}
					});

});

function getFishLandingList(){
$.get("/create/fishlandingsList", function(content, status) {

	var markup = "";
	var tr;

	for (var x = 0; x < content.length; x++) {
		region = content[x].region;
		
		 if(content.length - 1 === x) {
		        $(".exportToExcel").show();
		    }
		
		
		tr = $('<tr/>');

		tr.append("<td>" + content[x].province + "</td>");
		tr.append("<td>" + content[x].municipality + "</td>");
		tr.append("<td>" + content[x].barangay + "</td>");
		tr.append("<td>" + content[x].nameOfLanding + "</td>");
		tr.append("<td>" + content[x].code + "</td>");
		tr.append("<td>" + content[x].area + "</td>");
		tr.append("<td>" + content[x].classification + "</td>");
		tr.append("<td>" + content[x].volumeOfUnloadingMT + "</td>");
		tr.append("<td>" + content[x].type + "</td>");
		tr.append("<td>" + content[x].cflc_status + "</td>");
		tr.append("<td>" + content[x].dataSource + "</td>");
		tr.append("<td>" + content[x].lat + "</td>");
		tr.append("<td>" + content[x].lon + "</td>");
		tr.append("<td>" + content[x].remarks + "</td>");
		$('#emp_body').append(tr);
	}
});
}
function getPerPage() {
	 $('#paging').delegate('a','click',function() {
			var $this = $(this), 
			target = $this.data('target');
			
			$.get("/create/fishlandings/" + target, function(content, status) {

				var markup = "";
				for (var x = 0; x < content.fishLanding.length; x++) {
					var active = content.fishLanding[x].enabled;
					if(active){
						ShowHide = "<td class='del menu_links btn-primary'  data-href="+ content.fishLanding[x].id + ">ACTIVE</td>"+
						"<td class='map menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">MAP/UPDATE</td>";
					}else{
						ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
						"<td class='menu_links btn-primary'>Not applicable</td>";
					}	
						markup += "<tr>" +
						"<td><span>" + content.fishLanding[x].region + "</span><td>"
						+ content.fishLanding[x].province + "</td><td>"
						+ content.fishLanding[x].municipality + "</td><td>"
						+ content.fishLanding[x].barangay + "</td><td>"
						+ content.fishLanding[x].nameOfLanding
						+ "</td><td>" + content.fishLanding[x].lat + ","
						+ content.fishLanding[x].lon + "</td>" +
						ShowHide +
						//"<td class='map menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">MAP/UPDATE</td>" +
						//"<td class='del menu_links btn-primary' data-href="+ content.fishLanding[x].id + ">ACTIVE</td>" +
						//"<td class='view menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">View</td>" +
						"</tr>";
			}
			$("#export-buttons-table").find('tbody').empty();
			$("#export-buttons-table").find('tbody').append(markup);
			$("#paging").empty();
			$("#paging").append(content.pageNumber);
			
//					markup += "<tr><td>"
//							"<td><span>" + content.fishLanding[x].region + "</span></td>" +									
//							"<td>"+ content.fishLanding[x].province + "</td><td>"
//							+ content.fishLanding[x].municipality + "</td><td>"
//							+ content.fishLanding[x].barangay + "</td><td>"
//							+ content.fishLanding[x].nameOfLanding
//							+ "</td><td>" + content.fishLanding[x].lat + ","
//							+ content.fishLanding[x].lon + "</td>" +
//							"<td class='map menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">MAP</td>" +
//							"<td class='view menu_links btn-primary' data-href="+ content.fishLanding[x].uniqueKey + ">View</td>" +
//							"</tr>";
//				}
//				$("#export-buttons-table").find('tbody').empty();
//				$("#export-buttons-table").find('tbody').append(markup);
//				$("#paging").empty();
//				$("#paging").append(content.pageNumber);
			});
	 });
}

function getFishLandingInfoById(data, status) {

	var content = $("#pdfFISHLANDING").find('tbody').empty();

	for (var x = 0; x < data.length; x++) {
		var obj = data[x];

		$(".id").val(obj.id);
		$(".user").val(obj.user);
		$(".uniqueKey").val(obj.uniqueKey);
		$(".dateEncoded").val(obj.dateEncoded);
		$(".encodedBy").val(obj.encodedBy);
		
		 getProvinceMunicipalityBarangay(obj); 
		 
		$(".nameOfLanding").val(obj.nameOfLanding);
		$(".classification").val(obj.classification);
		$(".volumeOfUnloadingMT").val(obj.volumeOfUnloadingMT);
		$(".fishLandingType").val(obj.type);
		$(".cflcStatus").val(obj.cflc_status);
		$(".croppingEnd").val(obj.croppingEnd);
		if(obj.type == "CFLC"){
			var slctProvinces=$('.cflcStatus').find('option').remove().end();
			  var option="";
			  slctProvinces.empty();
			  option = "<option value="+obj.cflc_status+">"+obj.cflc_status+"</option>";
			  option = option + "<option value='Operational'>Operational</option>"
				+ "<option value='For Operation'>For Operation</option>"
				+ "<option value='For Completion'>For Completion</option>"
				+ "<option value='For Transfer'>For Transfer</option>"
				+ "<option value='Damaged'>Damaged</option>"
				+"<option value=''>-- PLS SELECT--</option>";
				
			  slctProvinces.append(option);	
			
		$("#cflc_status").show();	
			
		}
		$(".dataSource").val(obj.dataSource);
		$(".area").val(obj.area);
		$(".code").val(obj.code);
		$(".dateAsOf").val(obj.dateAsOf);
		$(".remarks").val(obj.remarks);
		$(".lat").val(obj.lat);
		$(".lon").val(obj.lon);
		$(".image_src").val(obj.image_src);
		$(".image").val(obj.image);
		$(".preview").attr("src", obj.image);

		var markup = "<tr><th scope=\"col\">FISH LANDING LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"
				+ "<tr><th scope=\"row\">Region</th><td>"
				+ obj.region
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Province</th><td>"
				+ obj.province
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Municipality</th><td>"
				+ obj.municipality
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Barangay</th><td>"
				+ obj.barangay
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Code:</th><td>"
				+ obj.code
				+ "</td></tr>"
				+ "<tr><th scope=\"col\">FISH LANDING INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"
				+ "<tr><th scope=\"row\">Name of Lading:</th><td>"
				+ obj.nameOfLanding
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Area:</th><td>"
				+ obj.area
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Volume of Unloading MT:</th><td>"
				+ obj.volumeOfUnloadingMT
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Classification:</th><td>"
				+ obj.classification
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Type:</th><td>"
				+ obj.type
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">CFLC Status:</th><td>"
				+ obj.cflc_status
				+ "</td></tr>"
				+

				"<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"
				+ "<tr><th scope=\"row\">Date as of:</th><td>"
				+ obj.dateAsOf
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Data Sources:</th><td>"
				+ obj.dataSource
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Remarks:</th><td>"
				+ obj.remarks
				+ "</td></tr>"
				+ "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"
				+ "<tr><th scope=\"row\">Latitude:</th><td>"
				+ obj.lat
				+ "</td></tr>"
				+ "<tr><th scope=\"row\">Longitude:</th><td>"
				+ obj.lon
				+ "</td></tr>"
				+ "<tr align=\"left\"><td><img id=\"imgview\" src=\""
				+ obj.image_src + "\"></img></td>&nbsp;<td></td></tr>";

		content.append(markup);

		//reset();

		markup = "";
		$('#img_preview').show();
		$('#pdf').show();
		$('#table').hide();
		$('.file').prop('required',false);
	}

}
