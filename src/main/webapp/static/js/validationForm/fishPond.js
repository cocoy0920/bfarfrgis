
var save=false;

	jQuery(document).ready(function($) {
		$('.file').prop('required',true);
		$(".exportToExcel").hide();

					getPage("fishpond");
					getAllFishPondPageUser();
					getFishPondList();
					getPerPage();
	});	 					
					var forms = document.getElementsByClassName('needs-validation');
				    // Loop over them and prevent submission
				    var validation = Array.prototype.filter.call(forms, function(form) {
					$(".submitFishPond").click(function(event) {

				        if (form.checkValidity() === false) {
				          event.preventDefault();
				          event.stopPropagation();
				          
				          form.classList.add('was-validated'); 
				        }
				        if (form.checkValidity() === true) {
				            event.preventDefault();
				            
				            document.getElementById("submitFishPond").disabled = true;
				            
				            var array = jQuery(form).serializeArray();
				        	var json = {};

				        	jQuery.each(array, function() {

				        		json[this.name] = this.value || '';
				        	});
				        	
				        	var formData = JSON.stringify(json);
				        	
				        	sendingForm(formData);
				        	form.classList.remove('was-validated'); 
				        	
				          }
				        
				      
						});
					});
				    
				    var validations = Array.prototype.filter.call(forms, function(form) {
				    $(".cancel").click(function(event) {
				    	document.getElementById("submitFishPond").disabled = false;
				    	form.classList.remove('was-validated'); 
				    	window.scrollTo(0, 0); 
				    	 $('.form_input').hide();
	                	 $('#mapview').hide();
	                	 $('.container').show();
					  });
				    });
				    
				    $(".create").click(function(event) {
				    	document.querySelector('#submitFishPond').innerHTML = 'SAVE';
				    	
						 
						 	document.getElementById("myform").reset();
						  	document.getElementById("preview").src = "";
						  	var img_preview = document.getElementById("img_preview");
						  	var pdf_view = document.getElementById("pdf");
						   	img_preview.style.display = "none";
						   	pdf_view.style.display = "none";
						   	save = true;
							$('.province').find('option').remove().end();
		    		  		refreshLocation();
		    		  		$('.container').hide();		 
							 $('.form_input').show();
							 $('#mapview').hide();
							 $('#nonActiveData').hide();
						 
						 
					  });
				    

//                 	$(".nameOfFishPondOperator").val("");             
//                 	$(".nameOfCaretaker").val("");
//                 	$(".typeOfWater").val("");
//                 	$(".fishPond").val("");
//                 	$(".fishPondType").val("");
//                 	$(".speciesCultured").val("");
//                 	$(".status").val("");
//                 	$(".kind").val("");
//                 	$(".hatchery").val("");
//                 	$(".productPer").val("");
//                 	$(".dataSource").val("");
//                 	
//            	$(".area").val("");
//            	$(".code").val("");
//            	$(".dateAsOf").val("");
//            	$(".remarks").val("");
//            	$(".lat").val("");
//            	$(".lon").val("");
//            	$(".image_src").val("0");
//            	$(".image").val("");
//            	$(".preview").attr("src", "");
//            	save=true;
//   
				    
function getAllFishPondPageUser(){			    
      		 $.get("/create/fishpond/" + "0", function(content, status){
        	  
        	 var markup = "";
         			for ( var x = 0; x < content.fishPond.length; x++) {
         				var active = content.fishPond[x].enabled;
            			if(active){
            				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.fishPond[x].id + ">ACTIVE</td>"+
            				"<td class='map menu_links btn-primary' data-href="+ content.fishPond[x].uniqueKey + ">MAP/UPDATE</td>";
            			}else{
            				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
            				"<td class='menu_links btn-primary'>Not applicable</td>";
            			}
         			   markup +="<tr>" +
         			   		"<td><span>"+content.fishPond[x].region+"</span></td>" +
         			   		"<td>"+content.fishPond[x].province+"</td>" +
         			   		"<td>"+content.fishPond[x].municipality+"</td>" +
         			   		"<td>"+content.fishPond[x].barangay+"</td>" +
         			   		"<td>"+content.fishPond[x].nameoffishpondoperator+"</td>" +
         			   		"<td>"+content.fishPond[x].lat+","+content.fishPond[x].lon+"</td>" +
         			   		ShowHide +
         			   		//"<td class='map menu_links btn-primary' data-href="+ content.fishPond[x].uniqueKey + ">MAP/UPDATE</td>" +
         			   		//"<td class='del menu_links btn-primary' data-href="+ content.fishPond[x].id + ">ACTIVE</td>" +
         			   		//"<td class='view menu_links btn-primary' data-href="+ content.fishPond[x].uniqueKey + ">View</td>" +
         			   		"</tr>";
					}
    				$("#export-buttons-table").find('tbody').empty();
    				$("#export-buttons-table").find('tbody').append(markup);
    			$("#paging").empty();		
    			$("#paging").append(content.pageNumber);	
    		  });
 	  
	}
function getPerPage(){	
	  $('#paging').delegate('a', 'click' , function(){
		  var $this = $(this),
           target = $this.data('target');
		 $.get("/create/fishpond/" + "0", function(content, status){
	  
	 var markup = "";
			for ( var x = 0; x < content.fishPond.length; x++) {
				var active = content.fishPond[x].enabled;
    			if(active){
    				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.fishPond[x].id + ">ACTIVE</td>"+
    				"<td class='map menu_links btn-primary' data-href="+ content.fishPond[x].uniqueKey + ">MAP/UPDATE</td>";
    			}else{
    				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
    				"<td class='menu_links btn-primary'>Not applicable</td>";
    			}
			   markup +="<tr>" +
			   		"<td><span>"+content.fishPond[x].region+"</span></td>" +
			   		"<td>"+content.fishPond[x].province+"</td>" +
			   		"<td>"+content.fishPond[x].municipality+"</td>" +
			   		"<td>"+content.fishPond[x].barangay+"</td>" +
			   		"<td>"+content.fishPond[x].nameoffishpondoperator+"</td>" +
			   		"<td>"+content.fishPond[x].lat+","+content.fishPond[x].lon+"</td>" +
			   		ShowHide +
			   		//"<td class='map menu_links btn-primary' data-href="+ content.fishPond[x].uniqueKey + ">MAP/UPDATE</td>" +
			   		//"<td class='del menu_links btn-primary' data-href="+ content.fishPond[x].id + ">ACTIVE</td>" +
			   		//"<td class='view menu_links btn-primary' data-href="+ content.fishPond[x].uniqueKey + ">View</td>" +
			   		"</tr>";
			}
			$("#export-buttons-table").find('tbody').empty();
			$("#export-buttons-table").find('tbody').append(markup);
		$("#paging").empty();		
		$("#paging").append(content.pageNumber);	
	  });
	  });
}

//	var token = $("meta[name='_csrf']").attr("content");
//	var header = $("meta[name='_csrf_header']").attr("content");

	
			function sendingForm(form){
		        	var json = form;
					$.ajax({						
						url : "/create/saveFishPond",
						type : "POST",
						contentType : "application/json",						
						data : json,
						dataType : 'json',				
						success : function(content) {
							getAllFishPondPageUser();
	    		  		
	    		  		
						document.getElementById("myform").reset();
					  	document.getElementById("preview").src = "";
					  	var img_preview = document.getElementById("img_preview");
					  	var pdf_view = document.getElementById("pdf");
					  	//document.getElementById("file").disabled = false;
					   	img_preview.style.display = "none";
					   	pdf_view.style.display = "none";
						save = true;
						$('.province').find('option').remove().end();
	    		  		refreshLocation();
							$(".se-pre-con").hide();
							alert("SUCCESSFULLY SAVE");
							window.location.replace("/create/fishPondMap/" + content.uniqueKey);
			    			
							// window.location.reload(true); 
							 
						},
						error: function(data){
						
						$(".se-pre-con").hide();
						}
					});
		       }


//$('#export-buttons-table').delegate('td.view', 'click' , function(){
//$("#tbodyID").empty();
//  var uniqueKey = $(this).data('href');
//  //console.log("*[data-href]" + uniqueKey);
//  save=false;
//  $.get("/create/getFishPond/" + uniqueKey, function(data, status){
//         for ( var x = 0; x < data.length; x++) {
//						var obj = data[x];
//						var fishPondType = obj.fishPondType;
//						var status = obj.status;
//	            		
//	            		if(status == 'Non-Active'){
//	            			$('#nonActiveData').show();
//	            		}else{
//	            			$('#nonActiveData').hide();
//	            		}
//	            		  
//	            	}
//         	$('.form_input').show();
//			getFishPondInfoById(data, status);
//    		  });
//  document.querySelector('#submitFishPond').innerHTML = 'UPDATE'; 
//});

$('#export-buttons-table').delegate('td.map', 'click', function() {
	$("#tbodyID").empty();
	var id = $(this).data('href');
	save = false;
	$('#mapview').show();
	window.location.replace("/create/fishPondMap/" + id);

});
$('#export-buttons-table').delegate('td.del', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;
/*	 let text = "are you sure you want to delete";
	  if (confirm(text) == true) {
			window.location.replace("/create/fishPondDeleteById/" + uniqueKey);
	  } else {
	    text = "You canceled!";
	  }*/
	
	let text;
	  let reason = prompt("Please enter your reason:","");
	  if (reason == null || reason == "") {
	    text = "User cancelled the prompt.";
	  } else {
		  window.location.replace("/create/fishPondDeleteById/" + id +"/" + reason);
		  console.log(reason);

	  }
});

$(function() {
	$(".exportToPDF").click(function(e){
      var doc = new jsPDF('p', 'pt');

      var elem = document.getElementById('pdfFISHPOND');
      var imgElements = document.querySelectorAll('#pdfFISHPOND tbody img');
      var data = doc.autoTableHtmlToJson(elem);
      var images = [];
      var i = 0;

      var img = imgElements[i].src;
      doc.autoTable(data.columns, data.rows, {
       // bodyStyles: {rowHeight: 30},
        drawCell: function(cell, opts) {
          if (opts.column.dataKey === 1) {
            images.push({
              url: img,
              x: cell.textPos.x,
              y: cell.textPos.y
            });
            i++;
          }
        },
        addPageContent: function() {
          for (var i = 0; i < images.length; i++) {
          	if(i == images.length-1){
            doc.addImage(images[i].url, 100, images[i].y, 200, 200);
            }
          }
        }
      });

      doc.save("fishPond.pdf");
       window.location.reload();
	});
});

			$(function() {
				$(".exportToExcel").click(function(e){

					var table = $(this).prev('.table2excel');
					if(table && table.length){
						var preserveColors = (table.hasClass('table2excel_with_colors') ? true : false);
						$(table).table2excel({
							exclude: ".noExl",
							name: "Excel Document Name",
							filename: "FishPond.xls",
							fileext: ".xls",
							exclude_img: true,
							exclude_links: true,
							exclude_inputs: true,
							preserveColors: preserveColors
						});
					}
				});
				
			});
function getFishPondList(){
$.get("/create/fishpondList", function(content, status){
        	console.log(content);
        	 var markup = "";
        	 var tr;
       
         			for ( var x = 0; x < content.length; x++) {
         			region = content[x].region;
         			$(".exportToExcel").show();
					tr = $('<tr/>');
					
					tr.append("<td>" + content[x].province + "</td>");
					tr.append("<td>" + content[x].municipality + "</td>");
					tr.append("<td>" + content[x].barangay + "</td>");
					tr.append("<td>" + content[x].nameoffishpondoperator + "</td>");
					tr.append("<td>" + content[x].nameofactualoccupant + "</td>");
					tr.append("<td>" + content[x].code + "</td>");
					tr.append("<td>" + content[x].area + "</td>");
					tr.append("<td>" + content[x].fishpondtype + "</td>");
//					tr.append("<td>" + content.fishPond[x].fishPondCondition + "</td>");
//					tr.append("<td>" + content.fishPond[x].typeOfWater + "</td>");
					tr.append("<td>" + content[x].status + "</td>");
					tr.append("<td>" + content[x].speciescultured + "</td>");
					tr.append("<td>" + content[x].kind + "</td>");
					tr.append("<td>" + content[x].hatchery + "</td>");
//					tr.append("<td>" + content.fishPond[x].productPer + "</td>");
					tr.append("<td>" + content[x].datasource + "</td>");
					tr.append("<td>" + content[x].lat + "</td>");
					tr.append("<td>" + content[x].lon + "</td>");
					tr.append("<td>" + content[x].remarks + "</td>");
					$('#emp_body').append(tr);	
					}    		
    		  });
	}

//	 $(document).ready(function () {
//	 	  $('.fishpondarea').change(function(){
//		    var areaValue = $(this).val();
//		    var regexp = /^\d+(\.\d{1,4})?$/;
//		    if(areaValue == ''){
//		    	return;
//		    }
//		    regexp.test('10.5');
//		    console.log( areaValue + " returns " + regexp.test(areaValue));
//		  if(!regexp.test(areaValue)){
//			  alert("INVALID AREA");
//			  $(".fishpondarea").val("");
//		  }
//
//		});
//	  
//	  
//	 });
//
//	 
	 function getFishPondInfoById(data, status,resProvince ){
			
			var content =  $("#pdfFISHPOND").find('tbody').empty();
			
		    for ( var x = 0; x < data.length; x++) {
				var obj = data[x];
		
				$(".id").val(obj.id);
			    $(".user").val(obj.user);
			    $(".uniqueKey").val(obj.uniqueKey);
			    $(".dateencoded").val(obj.dateencoded);
			    $(".encodedby").val(obj.encodedby);
			 	//$(".region").val(obj.region);
			    getProvinceMunicipalityBarangay(obj); 
			    
		    $(".nameoffishpondoperator").val(obj.nameoffishpondoperator);            
		    $(".nameofactualoccupant").val(obj.nameofactualoccupant);
		   // $(".typeOfWater").val(obj.typeOfWater);
		    $(".fishpond").val(obj.fishpond);
		    $(".fishpondtype").val(obj.fishpondtype);
		   // $(".yearIssued").val(obj.yearIssued);
		   // $(".yearExpired").val(obj.yearExpired);
		  //  $(".fishPondCondition").val(obj.fishPondCondition);
		    $(".nonactive").val(obj.nonactive);
		    $(".speciescultured").val(obj.speciescultured);
		    $(".status").val(obj.status);
		    $(".kind").val(obj.kind);
		    $(".hatchery").val(obj.hatchery);
		   // $(".productPer").val(obj.productPer);
		    $(".datasource").val(obj.datasource);
			$(".area").val(obj.area);
			$(".code").val(obj.code);
			$(".dataasof").val(obj.dataasof);
			$(".remarks").val(obj.remarks);
			$(".lat").val(obj.lat);
			$(".lon").val(obj.lon);
			$(".image_src").val(obj.image_src);
			$(".image").val(obj.image);
			$(".preview").attr("src", obj.image);


		    var markup = 
		    "<tr><th scope=\"col\">FISHPOND LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
		    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
		    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
		    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
		    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
		    "<tr><th scope=\"col\">FISH POND INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		    "<tr><th scope=\"row\">Name of Fishpond Operator:</th><td>"+obj.nameoffishpondoperator+"</td></tr>"+
		    "<tr><th scope=\"row\">Name of Actual Occupant:</th><td>" + obj.nameoffishpondoperator + "</td></tr>"+
		   "<tr><th scope=\"row\">FishPond Type:</th><td>" + obj.fishpondtype + "</td></tr>"+
		   
		 //  "<tr><th scope=\"row\">Year Issued:</th><td>" + obj.yearIssued + "</td></tr>"+
		//   "<tr><th scope=\"row\">Year Expired:</th><td>" + obj.yearExpired + "</td></tr>"+
		  "<tr><th scope=\"row\">FishPond Status:</th><td>" + obj.status + "</td></tr>"+
		  "<tr><th scope=\"row\">if Non-Active:</th><td>" + obj.nonactive + "</td></tr>"+
		  "<tr><th scope=\"row\">Area (sqm.):</th><td>" + obj.area + "</td></tr>"+
		  
		 // "<tr><th scope=\"row\">Type of Water:</th><td>" + obj.typeOfWater + "</td></tr>"+
		 // "<tr><th scope=\"row\">Status:</th><td>" + obj.status + "</td></tr>"+
		  "<tr><th scope=\"row\">Species Cultured:</th><td>" + obj.speciescultured + "</td></tr>"+
		  "<tr><th scope=\"row\">Kind:</th><td>" + obj.kind + "</td></tr>"+
		  "<tr><th scope=\"row\">Hatchery:</th><td>" + obj.hatchery + "</td></tr>"+
		 // "<tr><th scope=\"row\">Product Per Cropping:</th><td>" + obj.productPer + "</td></tr>"+
		  
		  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		  "<tr><th scope=\"row\">Data as of:</th><td>" + obj.dataasof + "</td></tr>"+
		  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.datasource + "</td></tr>"+
		  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
		  
		  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
		  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
		  "<tr><td  scope=\"col\">IMAGE</td><td scope=\"col\">&nbsp;</td></tr>"+
		  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td></tr>"; 
		 
		    $("#pdfFISHPOND").find('tbody').append(markup);
		    
		    
		 //   content.append(markup);
		    
		//    reset();
		    markup = "";
		    $('#img_preview').show();
		    $('#pdf').show();
		    $('#table').hide();	
			$('.file').prop('required',false);
		}

			
		}	 

	 
	 