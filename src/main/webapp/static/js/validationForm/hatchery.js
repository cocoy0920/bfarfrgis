

	jQuery(document).ready(function($) {
		
		$(".exportToExcel").hide();
		//$('.file').prop('required',true);

		var save=false;
	//	refreshLocation();
		getPage("hatchery");
        getHatcheryList();
         getHacheryPerPage();
         getPerPage();
	});	  
     	var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
        	
     	$(".submitHatchery").click(function(event){

            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();

              form.classList.add('was-validated'); 
            }
            if (form.checkValidity() === true) {
                event.preventDefault();
                document.getElementById("submitHatchery").disabled = true;
                var array = jQuery(form).serializeArray();
            	var json = {};

            	jQuery.each(array, function() {

            		json[this.name] = this.value || '';
            	});
            	
            	var formData = JSON.stringify(json);
            	
            	sendingForm(formData);
            	form.classList.remove('was-validated'); 
              }
            
     		});
    		});
    	
        var validations = Array.prototype.filter.call(forms, function(form) {
        $(".cancel").click(function(event) {
        	 document.getElementById("submitHatchery").disabled = false;
        	 form.classList.remove('was-validated'); 
        	window.scrollTo(0, 0);  
        	 $('.form_input').hide();
        	 $('#mapview').hide();
        	 $('.container').show();
			  
		  });
        });
        
        $(".create").click(function(event) {
        	 $('#mapview').hide();
        	 $('.container').hide();
        	 document.querySelector('#submitHatchery').innerHTML = 'SAVE'; 
        	document.getElementById("myform").reset();
			document.getElementById("preview").src = "";
			var img_preview = document.getElementById("img_preview");
			var pdf_view = document.getElementById("pdf");
			img_preview.style.display = "none";
		  	pdf_view.style.display = "none";
			save = true;
			$('.province').find('option').remove().end();
			refreshLocation();
  		
    		 $('.form_input').show();
    		  
    	  });

	function getHacheryPerPage(){
	
 		 $.get("/create/hatchery/" + "0", function(content, status){
        	 
         	
        	 var markup = "";
         			for ( var x = 0; x < content.hatcheries.length; x++) {
         				var active = content.hatcheries[x].enabled;
            			if(active){
            				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.hatcheries[x].id + ">ACTIVE</td>"+
            				"<td class='map menu_links btn-primary' data-href="+ content.hatcheries[x].uniqueKey + ">MAP/UPDATE</td>";
            			}else{
            				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
            				"<td class='menu_links btn-primary'>Not applicable</td>";
            			}
         			   markup +="<tr>" +
         			   			"<td><span>" + content.hatcheries[x].region + "</span></td>" +
         			   			"<td>"+content.hatcheries[x].province+"</td>" +
         			   			"<td>"+content.hatcheries[x].municipality+"</td>" +
         			   			"<td>"+content.hatcheries[x].barangay+"</td>" +
         			   			"<td>"+content.hatcheries[x].nameOfHatchery+"</td>" +
         			   			"<td>"+content.hatcheries[x].lat+","+content.hatcheries[x].lon+"</td>" +
         			   			ShowHide +
         			   			//"<td class='map menu_links btn-primary' data-href="+ content.hatcheries[x].uniqueKey + ">MAP/UPDATE</td>" +
         			   			//"<td class='del menu_links btn-primary' data-href="+ content.hatcheries[x].id + ">ACTIVE</td>" +
         			   			//"<td class='view menu_links btn-primary' data-href="+ content.hatcheries[x].uniqueKey + ">View</td>" +			
         			   			"</tr>";
					}
    				$("#export-buttons-table").find('tbody').empty();
    				$("#export-buttons-table").find('tbody').append(markup);
    			$("#paging").empty();		
    			$("#paging").append(content.pageNumber);	
    		  });
	}

//    var token = $("meta[name='_csrf']").attr("content");
//
//            var header = $("meta[name='_csrf_header']").attr("content");
//
//            
//
//            $(document).ajaxSend(function(e, xhr, options) {
//
//                xhr.setRequestHeader(header, token);
//
//              });

	function sendingForm(form){
        	var json = form;
			$.ajax({							
				url : "/create/saveHatchery",
				type : "POST",
				contentType : "application/json",
				data : json,
				dataType : 'json',				
				success : function(content) {
					
					getHacheryPerPage();
				document.getElementById("myform").reset();
				document.getElementById("preview").src = "";
				var img_preview = document.getElementById("img_preview");
				var pdf_view = document.getElementById("pdf");
				img_preview.style.display = "none";
			  	pdf_view.style.display = "none";
				save = true;
				$('.province').find('option').remove().end();
				refreshLocation();
	  		
		  		
					$(".se-pre-con").hide();
					alert("SUCCESSFULLY SAVE");
					window.location.replace("/create/hatcheryMap/" + content.uniqueKey);
					//window.location.reload(true);
				},
				error: function(data){
				$(".se-pre-con").hide();
				}
			});
		}

//
//$('#export-buttons-table').delegate('tr', 'click' , function(){
//$("#tbodyID").empty();
//  var uniqueKey = $(this).data('href');
//  console.log("*[data-href]" + uniqueKey);
//	save = false;
//		 
// $.get("/create/getHatchery/" + uniqueKey, function(data, status){
//        getHatcheryInfoById(data, status);
//    	});
// document.querySelector('#submitHatchery').innerHTML = 'UPDATE'; 
//});

//$('#export-buttons-table').delegate('td.view', 'click', function() {
//	$("#tbodyID").empty();
//	var uniqueKey = $(this).data('href');
//	save = false;
//
//	$.get("/create/getHatchery/" + uniqueKey, function(data, status) {
//
//		$('.form_input').show();
//		getHatcheryInfoById(data, status);
//	});
//	 document.querySelector('#submitHatchery').innerHTML = 'UPDATE'; 
//});

$('#export-buttons-table').delegate('td.map', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;
	$('#mapview').show();
	window.location.replace("/create/hatcheryMap/" + uniqueKey);

});
$('#export-buttons-table').delegate('td.del', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;
	/* let text = "are you sure you want to delete";
	  if (confirm(text) == true) {
			window.location.replace("/create/hatcheryDeleteById/" + uniqueKey);
	  } else {
	    text = "You canceled!";
	  }*/
	let text;
	  let reason = prompt("Please enter your reason:","");
	  if (reason == null || reason == "") {
	    text = "User cancelled the prompt.";
	  } else {
		  window.location.replace("/create/hatcheryDeleteById/" + uniqueKey +"/" + reason);
		  console.log(reason);

	  }
});


$(function() {
	$(".exportToPDF").click(function(e){
      var doc = new jsPDF('p', 'pt');

      var elem = document.getElementById('pdfHATCHERY');
      var imgElements = document.querySelectorAll('#pdfHATCHERY tbody img');
      var data = doc.autoTableHtmlToJson(elem);
      var images = [];
      var i = 0;
     // console.log(imgElements[i].src);
      var img = imgElements[i].src;
      doc.autoTable(data.columns, data.rows, {
       // bodyStyles: {rowHeight: 30},
        drawCell: function(cell, opts) {
          if (opts.column.dataKey === 1) {
            images.push({
              url: img,
              x: cell.textPos.x,
              y: cell.textPos.y
            });
            i++;
          }
        },
        addPageContent: function() {
       // console.log(images.length);
          for (var i = 0; i < images.length; i++) {
          	if(i == images.length-1){
            doc.addImage(images[i].url, 100, images[i].y, 200, 200);
            }
          }
        }
      });

      doc.save("hatchery.pdf");
       //  window.location.reload();
	});
});


			$(function() {
				$(".exportToExcel").click(function(e){
					var table = $(this).prev('.table2excel');
					if(table && table.length){
						var preserveColors = (table.hasClass('table2excel_with_colors') ? true : false);
						$(table).table2excel({
							exclude: ".noExl",
							name: "Excel Document Name",
							filename: "HATCHERY.xls",
							fileext: ".xls",
							exclude_img: true,
							exclude_links: true,
							exclude_inputs: true,
							preserveColors: preserveColors
						});
					}
				});
				
			});
function getHatcheryList(){
$.get("/create/hatcheryList", function(content, status){
        	
        	 var markup = "";
        	 var tr;
       
         			for ( var x = 0; x < content.length; x++) {
         			region = content[x].region;
         			
         			 if(content.length - 1 === x) {
 				        $(".exportToExcel").show();
 				    }
 				
         			
					tr = $('<tr/>');
					
					tr.append("<td>" + content[x].province + "</td>");
					tr.append("<td>" + content[x].municipality + "</td>");
					tr.append("<td>" + content[x].barangay + "</td>");
					tr.append("<td>" + content[x].code + "</td>");
					tr.append("<td>" + content[x].legislated + "</td>");
					tr.append("<td>" + content[x].status + "</td>");
					tr.append("<td>" + content[x].ra + "</td>");
					tr.append("<td>" + content[x].title + "</td>");
					tr.append("<td>" + content[x].area + "</td>");
					tr.append("<td>" + content[x].nameOfHatchery + "</td>");
					tr.append("<td>" + content[x].nameOfOperator + "</td>");
					tr.append("<td>" + content[x].addressOfOperator + "</td>");
					tr.append("<td>" + content[x].operatorClassification + "</td>");					
					tr.append("<td>" + content[x].publicprivate + "</td>");
					tr.append("<td>" + content[x].indicateSpecies + "</td>");
					tr.append("<td>" + content[x].prodStocking + "</td>");
					tr.append("<td>" + content[x].prodCycle + "</td>");
					tr.append("<td>" + content[x].actualProdForTheYear + "</td>");
					tr.append("<td>" + content[x].dataSource + "</td>");
					tr.append("<td>" + content[x].lat + "</td>");
					tr.append("<td>" + content[x].lon + "</td>");
					tr.append("<td>" + content[x].remarks + "</td>");
					
					$('#emp_body').append(tr);	
					}    		
    		  });
			}
function getPerPage(){   

	  $('#paging').delegate('a', 'click' , function(){
		  var $this = $(this),
           target = $this.data('target');    

		  $.get("/create/hatchery/" + target, function(content, status){
	        	 
	         	
	        	 var markup = "";
	         			for ( var x = 0; x < content.hatcheries.length; x++) {
	         				var active = content.hatcheries[x].enabled;
	            			if(active){
	            				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.hatcheries[x].id + ">ACTIVE</td>"+
	            				"<td class='map menu_links btn-primary' data-href="+ content.hatcheries[x].uniqueKey + ">MAP/UPDATE</td>";
	            			}else{
	            				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
	            				"<td class='menu_links btn-primary'>Not applicable</td>";
	            			}
	         			   markup +="<tr>" +
	         			  "<td><span>" + content.hatcheries[x].region + "</span></td>" +
	         			   				"<td>"+content.hatcheries[x].province+"</td>" +
	         			   				"<td>"+content.hatcheries[x].municipality+"</td>" +
	         			   				"<td>"+content.hatcheries[x].barangay+"</td>" +
	         			   				"<td>"+content.hatcheries[x].nameOfHatchery+"</td>" +
	         			   				"<td>"+content.hatcheries[x].lat+","+content.hatcheries[x].lon+"</td>" +
	         			   				ShowHide +
	         			   				//"<td class='map menu_links btn-primary' data-href="+ content.hatcheries[x].uniqueKey + ">MAP/UPDATE</td>" +
	         			   				//"<td class='del menu_links btn-primary' data-href="+ content.hatcheries[x].id + ">ACTIVE</td>" +
	         			   				//"<td class='view menu_links btn-primary' data-href="+ content.hatcheries[x].uniqueKey + ">View</td>" +		
	         			   				"</tr>";
						}
	    				$("#export-buttons-table").find('tbody').empty();
	    				$("#export-buttons-table").find('tbody').append(markup);
	    			$("#paging").empty();		
	    			$("#paging").append(content.pageNumber);	
	    		  });
      });
      

   }
      function getHatcheryInfoById(data, status){
    		
    		var content = $("#pdfHATCHERY").find('tbody').empty();
    	    for ( var x = 0; x < data.length; x++) {
    			var obj = data[x];
    			
    			getProvinceMunicipalityBarangay(obj); 
    			
    			$(".id").val(obj.id);
    		    $(".user").val(obj.user);
    		    $(".uniqueKey").val(obj.uniqueKey);
    		    $(".dateEncoded").val(obj.dateEncoded);
    		    $(".encodedBy").val(obj.encodedBy);
    		 	
    	    $(".nameOfHatchery").val(obj.nameOfHatchery);           
    	    $(".nameOfOperator").val(obj.nameOfOperator);
    	    $(".addressOfOperator").val(obj.addressOfOperator);
    	    
    	    if(obj.operatorClassification === 'BFAR' ){
    	    	   $(".operatorClassification").val(obj.operatorClassification);
    	    	    $(".legislated").val(obj.legislated);
    	    	    $(".status").val(obj.status);
    	    	    $('#bfar_id').show();   
    	    }else{
    	    	$(".operatorClassification").val(obj.operatorClassification);
    	    	$('#bfar_id').hide();  
    	    }
    	 
    	    $(".hatcherypublicprivate").val(obj.publicprivate);
    	    $(".hatcheryindicateSpecies").val(obj.indicateSpecies);

    	    $(".ra").val(obj.ra);
    	    $(".title").val(obj.title);
    	    $(".prodStocking").val(obj.prodStocking);
    	    $(".prodCycle").val(obj.prodCycle);
    	    $(".actualProdForTheYear").val(obj.actualProdForTheYear);
//    	    $(".monthStart").val(obj.monthStart);
//    	    $(".monthEnd").val(obj.monthEnd);
    	    $(".dataSource").val(obj.dataSource);
    		$(".area").val(obj.area);
    		$(".code").val(obj.code);
    		$(".dateAsOf").val(obj.dateAsOf);
    		$(".remarks").val(obj.remarks);
    		$(".lat").val(obj.lat);
    		$(".lon").val(obj.lon);
    		$(".image_src").val(obj.image_src);
    		$(".image").val(obj.image);
    		$(".preview").attr("src", obj.image_src);


    	    var markup = 
    	    "<tr><th scope=\"col\">HATCHERY LOCATION</th></tr>"+
    	    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
    	    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
    	    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
    	    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
    	    "<tr><th scope=\"row\">Code</th><td>" + obj.code + "</td></tr>"+
    	    "<tr><th scope=\"col\">HATCHERY INFORMATION</th></tr>"+
    	    "<tr><th scope=\"row\">Legislated:</th><td>"+obj.legislated+"</td></tr>"+
    	    "<tr><th scope=\"row\">Status:</th><td>"+obj.status+"</td></tr>"+
    	    "<tr><th scope=\"row\">RA#:</th><td>"+obj.ra+"</td></tr>"+
    	    "<tr><th scope=\"row\">Title:</th><td>"+obj.title+"</td></tr>"+
     	   
    	    "<tr><th scope=\"row\">Name of Hatchery:</th><td>"+obj.nameOfHatchery+"</td></tr>"+
    	    "<tr><th scope=\"row\">Address of Operator:</th><td>" + obj.addressOfOperator + "</td></tr>"+
    	    "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
    	    "<tr><th scope=\"row\">Indicate Species:</th><td>" + obj.indicateSpecies + "</td></tr>"+
    	    "<tr><th scope=\"row\">Type:</th><td>" + obj.type + "</td></tr>"+
    	    "<tr><th scope=\"row\">Stocking Density/Cycle:</th><td>" + obj.prodStocking + "</td></tr>"+
    	  "<tr><th scope=\"row\">Cycle/Year:</th><td>" + obj.prodCycle + "</td></tr>"+
    	  "<tr><th scope=\"row\">Actual Production for the year:</th><td>" + obj.actualProdForTheYear + "</td></tr>"+
    	  "<tr><th scope=\"col\">Month range of production cycle:</th></tr>"+
    	  
//    	  "<tr><th scope=\"row\">Month Start:</th><td>" + obj.monthStart + "</td></tr>"+
//    	  "<tr><th scope=\"row\">Month End:</th><td>" + obj.monthEnd + "</td></tr>"+
//    	  

    	  
    	  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th></tr>"+
    	  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
    	  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
    	  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
    	  
    	  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th></tr>"+
    	  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
    	  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
    	  "<tr><td  scope=\"col\">IMAGE</td><td scope=\"col\">&nbsp;</td></tr>"+
          
    	  "<tr><td scope=\"row\"><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td></tr>"; 
    	 
    	    content.append(markup);
    	   // reset();
    	    
    	    markup = "";
    	    $('#img_preview').show();
    	    $('#pdf').show();
    	    $('#table').hide();	
    	    $('.file').prop('required',false);
    	}

    		
    	}
      $('.operatorClassification').change(function(){
    	  console.log( $('.operatorClassification').val());
    	  var data =  $('.operatorClassification').val();
    	  if(data === 'BFAR'){
    		  $('#bfar_id').show(); 
    	  }else{
    		  $('#bfar_id').hide();
    	  }
    	  
      });
      