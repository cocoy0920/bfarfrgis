var sav=false;


	jQuery(document).ready(function($) {
		$('.file').prop('required',true);
		$(".exportToExcel").hide();
				//refreshLocation();
				getPage("mariculturezone");
            	getMariculturePerPage();
            	getAllMaricultureList();
            	getPerPage();
      		 
	});

	var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
	$(".submitmariculture").click(function(event){

        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
     
          form.classList.add('was-validated'); 
        }
        if (form.checkValidity() === true) {
            event.preventDefault();
            document.getElementById("submitmariculture").disabled = true;
            var array = jQuery(form).serializeArray();
        	var json = {};

        	jQuery.each(array, function() {

        		json[this.name] = this.value || '';
        	});
        	
        	var formData = JSON.stringify(json);
        	
        	sendingForm(formData);
        	form.classList.remove('was-validated'); 
          }
	});
      
		});	
    
    var validations = Array.prototype.filter.call(forms, function(form) {
    $(".cancel").click(function(event) {
    	document.getElementById("submitmariculture").disabled = false;
    	form.classList.remove('was-validated'); 
    	window.scrollTo(0, 0);  
    	 $('.form_input').hide();
    	 $('#mapview').hide();
    	 $('.container').show();
		  
	  });
    });
    
    $(".create").click(function(event) {
        document.querySelector('#submitmariculture').innerHTML = 'SAVE';  
    	document.getElementById("myform").reset();
	  	document.getElementById("preview").src = "";
	  	var img_preview = document.getElementById("img_preview");
	  	var pdf_view = document.getElementById("pdf");
	   	img_preview.style.display = "none";
	   	pdf_view.style.display = "none";
		save = true;
		$('.province').find('option').remove().end();
		refreshLocation();
		
    	 $('.form_input').show();
    	 $('#mapview').hide();
    	 $('.container').hide();
		  
	  }); 
	function getMariculturePerPage(){
		
 		 $.get("/create/mariculturezone/" + "0", function(content, status){
        	          	       	
       	 var markup = "";
        			for ( var x = 0; x < content.maricultureZone.length; x++) {
        				
        				var active = content.maricultureZone[x].enabled;
            			if(active){
            				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.maricultureZone[x].id + ">ACTIVE</td>"+
            				"<td class='map menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">MAP/UPDATE</td>";
            			}else{
            				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
            				"<td class='menu_links btn-primary'>Not applicable</td>";
            			}
        			   markup +="<tr>" +
        			   				"<td><span>" + content.maricultureZone[x].region + "</span></td>" +
        			   				"<td>"+content.maricultureZone[x].province+"</td><td>"+content.maricultureZone[x].municipality+"</td><td>"+content.maricultureZone[x].barangay+"</td><td>"+content.maricultureZone[x].nameOfMariculture+"</td><td>"+content.maricultureZone[x].lat+","+content.maricultureZone[x].lat+"</td>" +
        			   				ShowHide +
        			   				//"<td class='map menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">MAP/UPDATE</td>" +
        			   				//"<td class='del menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">ACTIVE</td>" +
        			   				//"<td class='view menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">View</td>" +
        			   				"</tr>";
					}
   				$("#export-buttons-table").find('tbody').empty();
   				$("#export-buttons-table").find('tbody').append(markup);
   			$("#paging").empty();		
   			$("#paging").append(content.pageNumber);	
   		  });
	}
//    var token = $("meta[name='_csrf']").attr("content");
//
//    var header = $("meta[name='_csrf_header']").attr("content");
//
//    
//
//    $(document).ajaxSend(function(e, xhr, options) {
//
//        xhr.setRequestHeader(header, token);
//
//      });
//    


	function sendingForm(form){
	var json = form;	
	
	$.ajax({
				
		url : "/create/saveMaricultureZone",
		type : "POST",
		contentType : "application/json",
		data : json,
		dataType : 'json',				
		success : function(content) {
			getMariculturePerPage();

		document.getElementById("myform").reset();
	  	document.getElementById("preview").src = "";
	  	var img_preview = document.getElementById("img_preview");
	  	var pdf_view = document.getElementById("pdf");
	   	img_preview.style.display = "none";
	   	pdf_view.style.display = "none";
		save = true;
		$('.province').find('option').remove().end();
		refreshLocation();
			$(".se-pre-con").hide();
			alert("SUCCESSFULLY SAVE");
			window.location.replace("/create/zoneMap/" + content.uniqueKey); 
		},
		error: function(data){
		console.log("error", data);
		$(".se-pre-con").hide();
		return;
		}
	});
}
	
//$('#export-buttons-table').delegate('tr', 'click' , function(){
//$("#tbodyID").empty();
//  var uniqueKey = $(this).data('href');
//  save = false;
//     $.get("/create/getMaricultureZone/" + uniqueKey, function(data, status){
//       
//			getMaricultureZoneInfoById(data, status);
//    		  });
//     document.querySelector('#submitmariculture').innerHTML = 'UPDATE';    
//});
//$('#export-buttons-table').delegate('td.view', 'click', function() {
//	$("#tbodyID").empty();
//	var uniqueKey = $(this).data('href');
//	save = false;
//
//	$.get("/create/getMaricultureZone/" + uniqueKey, function(data, status) {
//
//		$('.form_input').show();
//		getMaricultureZoneInfoById(data, status);
//	});
//	 document.querySelector('#submitmariculture').innerHTML = 'UPDATE'; 
//});

$('#export-buttons-table').delegate('td.map', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;
	$('#mapview').show();
	window.location.replace("/create/zoneMap/" + uniqueKey);

});
$('#export-buttons-table').delegate('td.del', 'click', function() {
	$("#tbodyID").empty();
	var id = $(this).data('href');
	save = false;
/*	 let text = "are you sure you want to delete";
	  if (confirm(text) == true) {
			window.location.replace("/create/zoneMapDeleteById/" + id);
	  } else {
	    text = "You canceled!";
	  }*/
	
	let text;
	  let reason = prompt("Please enter your reason:","");
	  if (reason == null || reason == "") {
	    text = "User cancelled the prompt.";
	  } else {
		  window.location.replace("/create/zoneMapDeleteById/" + id +"/" + reason);
		  console.log(reason);

	  }
});

$(function() {
	$(".exportToPDF").click(function(e){
      var doc = new jsPDF('p', 'pt');

      var elem = document.getElementById('pdftableMARICULTURE');
      var imgElements = document.querySelectorAll('#pdftableMARICULTURE tbody img');
      var data = doc.autoTableHtmlToJson(elem);
      var images = [];
      var i = 0;
     
      var img = imgElements[i].src;
      doc.autoTable(data.columns, data.rows, {
       // bodyStyles: {rowHeight: 30},
        drawCell: function(cell, opts) {
          if (opts.column.dataKey === 1) {
            images.push({
              url: img,
              x: cell.textPos.x,
              y: cell.textPos.y
            });
            i++;
          }
        },
        addPageContent: function() {
       // console.log(images.length);
          for (var i = 0; i < images.length; i++) {
          	if(i == images.length-1){
            doc.addImage(images[i].url, 100, images[i].y, 200, 200);
            }
          }
        }
      });

      doc.save("mariculture.pdf");
      window.location.reload();
	});
});


/*	 $(document).ready(function () {
	 	  $('.mariculturearea').change(function(){
		    var areaValue = $(this).val();
		    var regexp = /^\d+(\.\d{1,4})?$/;
		    if(areaValue == ''){
		    	return;
		    }
		    regexp.test('10.5');
		    console.log( areaValue + " returns " + regexp.test(areaValue));
		  if(!regexp.test(areaValue)){
			  $(".mariculturearea").val("");
		  }

		});
	  
	  
	 });*/

			$(function() {
				$(".exportToExcel").click(function(e){
					var table = $(this).prev('.table2excel');
					if(table && table.length){
						var preserveColors = (table.hasClass('table2excel_with_colors') ? true : false);
						$(table).table2excel({
							exclude: ".noExl",
							name: "Excel Document Name",
							filename: "MARICULTUREZONE.xls",
							fileext: ".xls",
							exclude_img: true,
							exclude_links: true,
							exclude_inputs: true,
							preserveColors: preserveColors
						});
					}
				});
				
			});
			
function getAllMaricultureList(){			
$.get("/create/maricultureList", function(content, status){
        	
        	 var markup = "";
        	 var tr;
       
         			for ( var x = 0; x < content.length; x++) {
         			region = content[x].region;
					
         			 if(content.length - 1 === x) {
 				        $(".exportToExcel").show();
 				    }
 				
         			
         			tr = $('<tr/>');
					
					tr.append("<td>" + content[x].province + "</td>");
					tr.append("<td>" + content[x].municipality + "</td>");
					tr.append("<td>" + content[x].barangay + "</td>");
					tr.append("<td>" + content[x].nameOfMariculture + "</td>");
					tr.append("<td>" + content[x].code + "</td>");
					tr.append("<td>" + content[x].area + "</td>");
					tr.append("<td>" + content[x].dateLaunched + "</td>");
					tr.append("<td>" + content[x].dateInacted + "</td>");
					tr.append("<td>" + content[x].eccNumber + "</td>");
					tr.append("<td>" + content[x].maricultureType + "</td>");
					tr.append("<td>" + content[x].municipalOrdinanceNo + "</td>");
					tr.append("<td>" + content[x].dateApproved + "</td>");
					/*
					tr.append("<td>" + content.maricultureZone[x].individual + "</td>");
					tr.append("<td>" + content.maricultureZone[x].partnership + "</td>");
					tr.append("<td>" + content.maricultureZone[x].cooperativeAssociation + "</td>");
					tr.append("<td>" + content.maricultureZone[x].cageType + "</td>");
					tr.append("<td>" + content.maricultureZone[x].cageSize + "</td>");
					tr.append("<td>" + content.maricultureZone[x].cageQuantity + "</td>");
					tr.append("<td>" + content.maricultureZone[x].speciesCultured + "</td>");
					tr.append("<td>" + content.maricultureZone[x].quantityMt + "</td>");
					tr.append("<td>" + content.maricultureZone[x].kind + "</td>");
					tr.append("<td>" + content.maricultureZone[x].productionPerCropping + "</td>");
					tr.append("<td>" + content.maricultureZone[x].numberofCropping + "</td>");
					tr.append("<td>" + content.maricultureZone[x].species + "</td>");
					tr.append("<td>" + content.maricultureZone[x].farmProduce + "</td>");
					tr.append("<td>" + content.maricultureZone[x].productDestination + "</td>");
					tr.append("<td>" + content.maricultureZone[x].aquacultureProduction + "</td>");
					*/
					tr.append("<td>" + content[x].dataSource + "</td>");
					tr.append("<td>" + content[x].lat + "</td>");
					tr.append("<td>" + content[x].lon + "</td>");
					tr.append("<td>" + content[x].remarks + "</td>");
					
					$('#emp_body').append(tr);	
					//ExportTable();
					}    		
    		  });
}
function getPerPage(){   

	  $('#paging').delegate('a', 'click' , function(){
		  var $this = $(this),
           target = $this.data('target');    
          
		  $.get("/create/mariculturezone/" + target, function(content, status){
    	       	
		       	 var markup = "";
		        			for ( var x = 0; x < content.maricultureZone.length; x++) {
		        				var active = content.maricultureZone[x].enabled;
		            			if(active){
		            				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.maricultureZone[x].id + ">ACTIVE</td>"+
		            				"<td class='map menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">MAP/UPDATE</td>";
		            			}else{
		            				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
		            				"<td class='menu_links btn-primary'>Not applicable</td>";
		            			}
		        			   markup +="<tr>" +
		        			   "<td><span>" + content.maricultureZone[x].region + "</span></td>" +
		        			   				"<td>"+content.maricultureZone[x].province+"</td><td>"+content.maricultureZone[x].municipality+"</td><td>"+content.maricultureZone[x].barangay+"</td><td>"+content.maricultureZone[x].nameOfMariculture+"</td><td>"+content.maricultureZone[x].lat+","+content.maricultureZone[x].lat+"</td>" +
		        			   				ShowHide +
		        			   				//"<td class='map menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">MAP/UPDATE</td>" +
		        			   				//"<td class='del menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">ACTIVE</td>" +
		        			   				//"<td class='view menu_links btn-primary' data-href="+ content.maricultureZone[x].uniqueKey + ">View</td>" +
		        			   				"</tr>";
							}
		   				$("#export-buttons-table").find('tbody').empty();
		   				$("#export-buttons-table").find('tbody').append(markup);
		   			$("#paging").empty();		
		   			$("#paging").append(content.pageNumber);	
		   		  });
      });
      

   }

function getMaricultureZoneInfoById(data, status,resProvince ){
	var content =   $("#pdftableMARICULTURE").find('tbody').empty();
    for ( var x = 0; x < data.length; x++) {
		var obj = data[x];
		getProvinceMunicipalityBarangay(obj);
		$(".id").val(obj.id);
	    $(".user").val(obj.user);
	    $(".uniqueKey").val(obj.uniqueKey);
	    $(".dateEncoded").val(obj.dateEncoded);
	    $(".encodedBy").val(obj.encodedBy);
	 	
    $(".nameOfMariculture").val(obj.nameOfMariculture);
	$(".maricultureType").val(obj.maricultureType);
	$(".eccNumber").val(obj.eccNumber);
	$(".individual").val(obj.individual);
	$(".partnership").val(obj.partnership);
	$(".cooperativeAssociation").val(obj.cooperativeAssociation);
	$(".zonecageType").val(obj.cageType);
	$(".cageSize").val(obj.cageSize);
	$(".cageQuantity").val(obj.cageQuantity);
	$(".mariculturespeciesCultured").val(obj.speciesCultured);
	$(".quantityMt").val(obj.quantityMt);
	$(".zonekind").val(obj.kind);
	$(".productionPerCropping").val(obj.productionPerCropping);
	$(".numberofCropping").val(obj.numberofCropping);
    $(".species").val(obj.species);
    $(".farmProduce").val(obj.farmProduce);
    $(".productDestination").val(obj.productDestination);
    $(".aquacultureProduction").val(obj.aquacultureProduction);
    $(".dateLaunched").val(obj.dateLaunched);
    $(".dateInacted").val(obj.dateInacted);
    $(".dateApproved").val(obj.dateApproved);
    $(".municipalOrdinanceNo").val(obj.municipalOrdinanceNo);
    $(".dataSource").val(obj.dataSource);
	$(".area").val(obj.area);
	$(".code").val(obj.code);
	$(".dateAsOf").val(obj.dateAsOf);
	$(".remarks").val(obj.remarks);
	$(".lat").val(obj.lat);
	$(".lon").val(obj.lon);
	$(".image_src").val(obj.image_src);
	$(".image").val(obj.image);
	$(".preview").attr("src", obj.image);


    var markup = 
    "<tr><th scope=\"col\">MARICULTURE ZONE/PARK LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
    "<tr><th scope=\"col\">MARICULTURE ZONE/PARK INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
    "<tr><th scope=\"row\">Name of Mariculture:</th><td>"+obj.nameOfMariculture+"</td></tr>"+
    "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
   "<tr><th scope=\"row\">Date Launched:</th><td>" + obj.dateLaunched + "</td></tr>"+
   "<tr><th scope=\"row\">Date Inacted:</th><td>" + obj.dateInacted + "</td></tr>"+
   "<tr><th scope=\"row\">eCcNumber:</th><td>" + obj.eccNumber + "</td></tr>"+
  "<tr><th scope=\"row\">Mariculture Type:</th><td>" + obj.maricultureType + "</td></tr>"+
  "<tr><th scope=\"col\">Municipal Ordinance</th><th scope=\"col\">&nbsp;</th></tr>"+
  
  "<tr><th scope=\"row\">Municipal Ordinance Number:</th><td>" + obj.municipalOrdinanceNo + "</td></tr>"+
  "<tr><th scope=\"row\">Date Approved:</th><td>" + obj.dateApproved + "</td></tr>"+
  /* "<tr><th scope=\"col\">Name of Inventors</th><th scope=\"col\">&nbsp;</th></tr>"+
  
  "<tr><th scope=\"row\">Individual:</th><td>" + obj.individual + "</td></tr>"+
  "<tr><th scope=\"row\">Partnership:</th><td>" + obj.partnership + "</td></tr>"+
  "<tr><th scope=\"row\">Cooperative Association:</th><td>" + obj.cooperativeAssociation + "</td></tr>"+
  
  "<tr><th scope=\"col\">Cage</th><th scope=\"col\">&nbsp;</th></tr>"+
  
  "<tr><th scope=\"row\">Type:</th><td>" + obj.cageType + "</td></tr>"+
  "<tr><th scope=\"row\">Size:</th><td>" + obj.cageSize + "</td></tr>"+
  "<tr><th scope=\"row\">Quality:</th><td>" + obj.cageQuantity + "</td></tr>"+
  "<tr><th scope=\"row\">Species Cultured:</th><td>" + obj.speciesCultured + "</td></tr>"+
  "<tr><th scope=\"row\">Quantity (mt.):</th><td>" + obj.quantityMt + "</td></tr>"+
  "<tr><th scope=\"row\">Kind:</th><td>" + obj.kind + "</td></tr>"+
  "<tr><th scope=\"row\">Production per Cropping (M.t):</th><td>" + obj.productionPerCropping + "</td></tr>"+
  "<tr><th scope=\"row\">No. of Cropping per year:</th><td>" + obj.numberofCropping + "</td></tr>"+
  
  "<tr><th scope=\"col\">Prices and Production</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Species:</th><td>" + obj.species + "</td></tr>"+
  "<tr><th scope=\"row\">Farm Produce/Kg:</th><td>" + obj.farmProduce + "</td></tr>"+
  "<tr><th scope=\"row\">Product destination for marketing monitoring:</th><td>" + obj.productDestination + "</td></tr>"+
  "<tr><th scope=\"row\">Aquaculture production MT for the last 5 years:</th><td>" + obj.aquacultureProduction + "</td></tr>"+
  */
  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
 
  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
  "<tr><td  scope=\"col\">IMAGE</td><td scope=\"col\">&nbsp;</td></tr>"+
  "<tr><td  scope=\"col\"></td><td scope=\"col\">&nbsp;</td></tr>"+
 
  "<tr><td scope=\"row\"><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td></tr>"; 
 
    content.append(markup);
    
   // reset();
    markup = "";
    $('#img_preview').show();
    $('#pdf').show();
    $('#table').hide();	
    $('.file').prop('required',false);
}

	
}

