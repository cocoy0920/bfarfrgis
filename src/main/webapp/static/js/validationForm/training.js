var save=false;

jQuery(document).ready(function($) {
	$('.file').prop('required',true);
	$(".exportToExcel").hide();

			//refreshLocation();
			getPage("trainingcenter");
            getTrainingPerPage();
            getTrainingList();
            getPerPage();
});            
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
        	
            $(".submitTrainingCenter").click(function(event){

                if (form.checkValidity() === false) {
                  event.preventDefault();
                  event.stopPropagation();
             
                  form.classList.add('was-validated'); 
                }
                if (form.checkValidity() === true) {
                    event.preventDefault();
                    document.getElementById("submitTrainingCenter").disabled = true;
                    var array = jQuery(form).serializeArray();
                	var json = {};

                	jQuery.each(array, function() {

                		json[this.name] = this.value || '';
                	});
                	
                	var formData = JSON.stringify(json);
                	
                	sendingForm(formData);
                	form.classList.remove('was-validated'); 
                  }
                
            });
        		});
            
            var validations = Array.prototype.filter.call(forms, function(form) {
            $(".cancel").click(function(event) {
            	document.getElementById("submitTrainingCenter").disabled = false;
            	form.classList.remove('was-validated'); 
            	window.scrollTo(0, 0);  
            	 $('.form_input').hide();
            	 $('#mapview').hide();
            	 $('.container').show();
				  
			  });
            });
            
            $(".create").click(function(event) {
            	document.querySelector('#submitTrainingCenter').innerHTML = 'SAVE';  
            	document.getElementById("myform").reset();
              	document.getElementById("preview").src = "";
              	var img_preview = document.getElementById("img_preview");
              	var pdf_view = document.getElementById("pdf");
               	img_preview.style.display = "none";
               	pdf_view.style.display = "none";
            	save = true;
            	$('.province').find('option').remove().end();
            	refreshLocation();
            	 $('.form_input').show();
            	 $('#mapview').hide();
            	 $('.container').hide();
       		  
       	  });

function getTrainingPerPage(){

		 $.get("/create/trainingcenter/" + "0", function(content, status){
       	  
	 var markup = "";
			for ( var x = 0; x < content.trainingCenter.length; x++) {
				var active = content.trainingCenter[x].enabled;
    			if(active){
    				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.trainingCenter[x].id + ">ACTIVE</td>"+
    				"<td class='map menu_links btn-primary' data-href="+ content.trainingCenter[x].uniqueKey + ">MAP/UPDATE</td>";
    			}else{
    				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
    				"<td class='menu_links btn-primary'>Not applicable</td>";
    			}
			   markup +="<tr>" +
			   		"<td><span>" + content.trainingCenter[x].region + "</span></td>" +
   			   		"<td>"+content.trainingCenter[x].province+"</td>" +
   			 		"<td>"+content.trainingCenter[x].municipality+"</td>" +
   			 		"<td>"+content.trainingCenter[x].barangay+"</td>" +
   			 		"<td>"+content.trainingCenter[x].name+"</td><td>"+content.trainingCenter[x].lat+","+content.trainingCenter[x].lon+"</td>" +
   			 		ShowHide +
   			 		//"<td class='map menu_links btn-primary' data-href="+ content.trainingCenter[x].uniqueKey + ">MAP/UPDATE</td>" +
   			 		//"<td class='del menu_links btn-primary' data-href="+ content.trainingCenter[x].id + ">ACTIVE</td>" +
			 		
   			 		//"<td class='view menu_links btn-primary' data-href="+ content.trainingCenter[x].uniqueKey + ">View</td>" +
   			 		"</tr>";
			}
			$("#tableList").find('tbody').empty();
			$("#tableList").find('tbody').append(markup);
		$("#paging").empty();		
		$("#paging").append(content.pageNumber);	
	  });
}
//var token = $("meta[name='_csrf']").attr("content");
//
//var header = $("meta[name='_csrf_header']").attr("content");
//
//
//
//$(document).ajaxSend(function(e, xhr, options) {
//
//    xhr.setRequestHeader(header, token);
//
//  });



function sendingForm(form){

	var json = form;
	
	$.ajax({		
	url : "/create/getTraining",
	type : "POST",
	contentType : "application/json",
	data : json,
	dataType : 'json',				
	success : function(content) {
		getTrainingPerPage();	

	document.getElementById("myform").reset();
  	document.getElementById("preview").src = "";
  	var img_preview = document.getElementById("img_preview");
  	var pdf_view = document.getElementById("pdf");
   	img_preview.style.display = "none";
   	pdf_view.style.display = "none";
	save = true;
	$('.province').find('option').remove().end();
	refreshLocation();
		
		$(".se-pre-con").hide();
		alert("SUCCESSFULLY SAVE");
		window.location.replace("/create/trainingMap/" + content.uniqueKey);
	},
	error: function(data){
	console.log("error", data);
	$(".se-pre-con").hide();
	return;
	}
});

}

//$('#tableList').delegate('tr', 'click' , function(){
//$("#tbodyID").empty();
//  var uniqueKey = $(this).data('href');
//  save = false;
//    $.get("/create/getTraining/" + uniqueKey, function(data, status){
//        
//			getTrainingInfoById(data, status );
//    		  });
//    document.querySelector('#submitTrainingCenter').innerHTML = 'UPDATE';   
//});
//
//$('#tableList').delegate('td.view', 'click', function() {
//	$("#tbodyID").empty();
//	var uniqueKey = $(this).data('href');
//	save = false;
//
//	$.get("/create/getTraining/" + uniqueKey, function(data, status) {
//
//		$('.form_input').show();
//		getTrainingInfoById(data, status);
//	});
//	 document.querySelector('#submitTrainingCenter').innerHTML = 'UPDATE'; 
//});

$('#tableList').delegate('td.map', 'click', function() {
	$("#tbodyID").empty();
	var uniqueKey = $(this).data('href');
	save = false;
	
	window.location.replace("/create/trainingMap/" + uniqueKey);

});
$('#export-buttons-table').delegate('td.del', 'click', function() {
	$("#tbodyID").empty();
	var id = $(this).data('href');
	save = false;
/*	 let text = "are you sure you want to delete";
	  if (confirm(text) == true) {
			window.location.replace("/create/trainingDeleteById/" + id);
	  } else {
	    text = "You canceled!";
	  }*/
	let text;
	  let reason = prompt("Please enter your reason:","");
	  if (reason == null || reason == "") {
	    text = "User cancelled the prompt.";
	  } else {
		  window.location.replace("/create/trainingDeleteById/" + id +"/" + reason);
		  console.log(reason);

	  }
});

$(function() {
	$(".exportToPDF").click(function(e){
      var doc = new jsPDF('p', 'pt');

      var elem = document.getElementById('pdfTRAINING');
      var imgElements = document.querySelectorAll('#pdfTRAINING tbody img');
      var data = doc.autoTableHtmlToJson(elem);
      var images = [];
      var i = 0;
     
      var img = imgElements[i].src;
      doc.autoTable(data.columns, data.rows, {
       // bodyStyles: {rowHeight: 30},
        drawCell: function(cell, opts) {
          if (opts.column.dataKey === 1) {
            images.push({
              url: img,
              x: cell.textPos.x,
              y: cell.textPos.y
            });
            i++;
          }
        },
        addPageContent: function() {
        console.log(images.length);
          for (var i = 0; i < images.length; i++) {
          	if(i == images.length-1){
            doc.addImage(images[i].url, 100, images[i].y, 200, 200);
            }
          }
        }
      });

      doc.save("Trainings.pdf");
         window.location.reload();
	});
});


			$(function() {
				$(".exportToExcel").click(function(e){
					var table = $(this).prev('.table2excel');
					if(table && table.length){
						var preserveColors = (table.hasClass('table2excel_with_colors') ? true : false);
						$(table).table2excel({
							exclude: ".noExl",
							name: "Excel Document Name",
							filename: "TrainingCenter.xls",
							fileext: ".xls",
							exclude_img: true,
							exclude_links: true,
							exclude_inputs: true,
							preserveColors: preserveColors
						});
					}
				});
				
			});

function getTrainingList(){
$.get("/create/trainingcenterList", function(content, status){
        	 
        	 var markup = "";
        	 var tr;
       
         			for ( var x = 0; x < content.length; x++) {
         			region = content[x].region;
         			
         			 if(content.length - 1 === x) {
 				        $(".exportToExcel").show();
 				    }
 				
         			
					tr = $('<tr/>');
					
					tr.append("<td>" + content[x].province + "</td>");
					tr.append("<td>" + content[x].municipality + "</td>");
					tr.append("<td>" + content[x].barangay + "</td>");
					tr.append("<td>" + content[x].code + "</td>");
					tr.append("<td>" + content[x].name + "</td>");
					tr.append("<td>" + content[x].specialization + "</td>");
					tr.append("<td>" + content[x].facilityWithinTheTrainingCenter + "</td>");
					tr.append("<td>" + content[x].type + "</td>");
					tr.append("<td>" + content[x].dataSource + "</td>");
					tr.append("<td>" + content[x].lat + "</td>");
					tr.append("<td>" + content[x].lon + "</td>");
					tr.append("<td>" + content[x].remarks + "</td>");
					
					$('#emp_body').append(tr);	
					//ExportTable();
					}    		
    		  });
}
function getPerPage(){   
    
	  $('#paging').delegate('a', 'click' , function(){
  	  
        var $this = $(this),
           target = $this.data('target');    
        	
        $.get("/create/trainingcenter/" + target, function(content, status){
         	  
       	 var markup = "";
       			for ( var x = 0; x < content.trainingCenter.length; x++) {
       			  
       				var active = content.trainingCenter[x].enabled;
        			if(active){
        				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.trainingCenter[x].id + ">ACTIVE</td>"+
        				"<td class='map menu_links btn-primary' data-href="+ content.trainingCenter[x].uniqueKey + ">MAP/UPDATE</td>";
        			}else{
        				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
        				"<td class='menu_links btn-primary'>Not applicable</td>";
        			}
       				markup +="<tr>" +
       			   		"<td><span>" + content.trainingCenter[x].region + "</span></td>" +
       			   		"<td>"+content.trainingCenter[x].province+"</td>" +
       			 		"<td>"+content.trainingCenter[x].municipality+"</td>" +
       			 		"<td>"+content.trainingCenter[x].barangay+"</td>" +
       			 		"<td>"+content.trainingCenter[x].name+"</td><td>"+content.trainingCenter[x].lat+","+content.trainingCenter[x].lon+"</td>" +
       			 		ShowHide +
       			 		//"<td class='map menu_links btn-primary' data-href="+ content.trainingCenter[x].uniqueKey + ">MAP/UPDATE</td>" +
       			 		//"<td class='del menu_links btn-primary' data-href="+ content.trainingCenter[x].id + ">ACTIVE</td>" +
       			 		//"<td class='view menu_links btn-primary' data-href="+ content.trainingCenter[x].uniqueKey + ">View</td>" +
       			 		"</tr>";
       			}
       			$("#tableList").find('tbody').empty();
       			$("#tableList").find('tbody').append(markup);
       		$("#paging").empty();		
       		$("#paging").append(content.pageNumber);	
       	  });
      });
   }

		function getTrainingInfoById(data, status,resProvince ){
			
			var content = $("#pdfTRAINING").find('tbody').empty();
		    for ( var x = 0; x < data.length; x++) {
				var obj = data[x];
				
				getProvinceMunicipalityBarangay(obj); 
				$(".id").val(obj.id);
			    $(".user").val(obj.user);
			    $(".uniqueKey").val(obj.uniqueKey);
			    $(".dateEncoded").val(obj.dateEncoded);
			    $(".encodedBy").val(obj.encodedBy);			 	
		    $(".name").val(obj.name);
			$(".specialization").val(obj.specialization);
			$(".facilityWithinTheTrainingCenter").val(obj.facilityWithinTheTrainingCenter);
		    $(".dataSource").val(obj.dataSource);
		    $(".type").val(obj.type);
			//$(".area").val(obj.area);
			$(".code").val(obj.code);
			$(".dateAsOf").val(obj.dateAsOf);
			$(".remarks").val(obj.remarks);
			$(".lat").val(obj.lat);
			$(".lon").val(obj.lon);
			$(".image_src").val(obj.image_src);
			$(".image").val(obj.image);
			$(".preview").attr("src", obj.image);


		    var markup = 
		    "<tr><th scope=\"col\">FFISHERIES TRAINING CENTER LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
		    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
		    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
		    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
		    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
		    
		    "<tr><th scope=\"col\">FISHERIES TRAINING CENTER INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		    "<tr><th scope=\"row\">Name of Training Center:</th><td>"+obj.name+"</td></tr>"+
		   "<tr><th scope=\"row\">Specialization:</th><td>" + obj.specialization + "</td></tr>"+
		   "<tr><th scope=\"row\">Facility Within The Training Center:</th><td>" + obj.facilityWithinTheTrainingCenter + "</td></tr>"+
		   "<tr><th scope=\"row\">Type:</th><td>" + obj.type + "</td></tr>"+
		   
		  
		  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
		  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
		  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
		 
		  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
		  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
		  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
		  "<tr><td  scope=\"col\">IMAGE</td><td scope=\"col\">&nbsp;</td></tr>"+
		  "<tr><td  scope=\"col\"></td><td scope=\"col\">&nbsp;</td></tr>"+
		  
		  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
		 
		    content.append(markup);
		    
		    //reset();
		    markup = "";
		    $('#img_preview').show();
		    $('#pdf').show();
		    $('#table').hide();	
		    $('.file').prop('required',false);
		}

			
		}

