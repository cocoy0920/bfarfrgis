	jQuery(document).ready(function($) {
		refreshAccount();
		getUserList();
//		getRregions();
//		getProvinceForUserRegistration();
		
		
	   	var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
        $(".submitUseInfo").click(function(event){
        	console.log(".submitUseInfo");
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();

              form.classList.add('was-validated'); 
            }
            if (form.checkValidity() === true) {
                event.preventDefault();
                document.getElementById("submitUseInfo").disabled = true;
                var array = jQuery(form).serializeArray();
            	var json = {};

            	jQuery.each(array, function() {

            		json[this.name] = this.value || '';
            	});
            	
            	var formData = JSON.stringify(json);
            	console.log(formData);
            	sendingForm(formData);
            	form.classList.remove('was-validated'); 
              }
            
          
    		});      	
    	
        }); 
        
        $(".canceltUseInfo").click(function(event){
        	console.log(".cancelUseInfo");
        	document.querySelector('#submitUseInfo').innerHTML = 'SAVE'; 
			refreshAccount();
			document.getElementById("myform").reset();
          
    		}); 
        
       	function sendingForm(form){
        	var json = form;
	        	
			$.ajax({				
				url : "/admin/saveRegistration",
				type: "POST",
				contentType : "application/json",
				data : json,
				dataType : 'json',				
				success : function(content,status) {
					document.getElementById("submitUseInfo").disabled = false;
					document.querySelector('#submitUseInfo').innerHTML = 'SAVE'; 
					refreshAccount();
					document.getElementById("myform").reset();

					$(".se-pre-con").hide();
					alert(content);
					
					
				},
				error: function(data){
					document.getElementById("submitUseInfo").disabled = false;
					alert(data);
				return;
				}
			});
		}
    
       	function getUserList(){
       	$.get("/admin/acctList", function(content, status){
       	 var markup = "";
    	 var tr;
    	 var role;
   
     			for ( var x = 0; x < content.length; x++) {
     			
     			
     				
				tr = $('<tr/>');
								
				tr.append("<td>" + content[x].name + "</td>");
				tr.append("<td>" + content[x].lastName + "</td>");
				tr.append("<td>" + content[x].username + "</td>");
				tr.append("<td>" + content[x].email + "</td>");
				tr.append("<td>" + content[x].region + "</td>");
			for ( var y = 0; y < content[x].userProfiles.length; y++) {   				
     			tr.append("<td>" + content[x].userProfiles[y].type + "</td>");
     			role = content[x].userProfiles[y].type;
     		}
			tr.append("<td class='view menu_links btn-primary' data-href="+ content[x].username + ">VIEW</td>");
			if(role  != "SUPERADMIN"){
				if(content[x].enabled){
					tr.append("<td class='inactive menu_links btn-primary' data-href="+ content[x].username + ">DISABLED</td>");	
				}else{
					tr.append("<td class='active menu_links btn-primary' data-href="+ content[x].username + ">ENABLED</td>");
				}
     		}
			$('#emp_body').append(tr);	
				} 
       	});
       	}
       	$('#export-buttons-table').delegate('td.view', 'click', function() {
       		$("#tbodyID").empty();
       		var username = $(this).data('href');
       		save = false;

       		$.get("/admin/getUserAcct/" + username, function(data, status) {

       			//$('.create').show();
       			getuserAcct(data, status);
       		});
       		 document.querySelector('#submitUseInfo').innerHTML = 'UPDATE'; 
       	});
       	
       	$('#export-buttons-table').delegate('td.inactive', 'click', function() {
       		$("#tbodyID").empty();
       		var username = $(this).data('href');
       		save = false;

       		$.get("/admin/updateAcctStatus/" + username, function(data, status) {

       			alert(data);
       			location.reload();
       			
       		});
       		
       	});
       	
       	$('#export-buttons-table').delegate('td.active', 'click', function() {
       		$("#tbodyID").empty();
       		var username = $(this).data('href');
       		save = false;

       		$.get("/admin/enableAcctStatus/" + username, function(data, status) {

       			alert(data);
       			location.reload();
       			
       		});
       		
       	});

       	function getuserAcct(data, status){  
       	 for ( var x = 0; x < data.length; x++) {
 	 		var obj = data[x];
 	 		$(".id").val(obj.id);
 	 		$(".state").val(obj.state);
 	 		$(".enabled").val(obj.enabled);
 	 		console.log(".enabled: " + obj.enabled);
 	 		 $(".enabled").prop("checked");
 	 		$(".firstName").val(obj.name);
 	 		$(".lastName").val(obj.lastName);
 	 		$(".username").val(obj.username);
 	 		$(".password").val(obj.pass);
 	 		$(".email").val(obj.email);
 	 		
 	 		
 	 		getRolesLocation(obj);
 	 		
 	 		
       	 }	
       	}	
       	function refreshAccount(){  
       		var slctRoles=$('.userProfiles').find('option').remove().end();
            var option="";
            option = "<option value=''>-- PLS SELECT--</option>";
            
            $.get("/admin/getAllRoles", function(data, status){     	  			
				for( let prop in data ){						 					
						option = option + "<option value='"+data[prop].id+ ","+data[prop].type+ "'>"+data[prop].type+"</option>";	           
					}
				slctRoles.append(option);
		 	}); 
            
            
            var slctRegions=$('.region').find('option').remove().end();
           
            var option_region="";
            option_region = "<option value=''>-- PLS SELECT--</option>";
            
            $.get("/admin/getAllRegions", function(data, status){     	  			
				for( let prop in data ){						 					
					option_region = option_region + "<option value='"+data[prop].region_id+ "," + data[prop].region +"'>"+data[prop].region+"</option>";	           
					}
				slctRegions.append(option_region);
		 	}); 
      	  
       	}
       	
        function getRolesLocation(obj){
        	var slctRoles=$('.userProfiles'),optionRole = "";
        	slctRoles.empty();
        	 var slctRegions=$('.region'),optionRegion = "";
        	 slctRegions.empty();
        	var roleType ="";
        	 console.log(obj.userProfiles);
        	 for( let prop in obj.userProfiles ){
        		 roleType = obj.userProfiles[prop].type;
        		 optionRole = "<option value='"+obj.userProfiles[prop].id+ ","+obj.userProfiles[prop].type+ "'>"+obj.userProfiles[prop].type+"</option>";
        	 }
        	 
        	 	
             $.get("/admin/getAllRoles", function(data, status){ 
            	 
            	 
 				for( let prop in data ){
 					var getrole = data[prop].type;
 					if(getrole != roleType){
 						optionRole = optionRole + "<option value='"+data[prop].id+","+data[prop].type+ "'>"+data[prop].type+"</option>";	
 					}
 						           
 					}
 				slctRoles.append(optionRole);
 		 	});
             
             optionRegion = "<option value='"+obj.region_id+ "," + obj.region +"'>"+obj.region+"</option>";	           
 			
             $.get("/admin/getAllRegions", function(data, status){     	  			
 				for( let prop in data ){
 					var get_region = data[prop].region_id;
 					console.log("/admin/getAllRegions" + obj.region_id + " : " + obj.region );
 					if(get_region != obj.region_id){
 						optionRegion = optionRegion + "<option value='"+data[prop].region_id+ "," + data[prop].region +"'>"+data[prop].region+"</option>";	           
 	 						
 					}
 					}
 				slctRegions.append(optionRegion);
 		 	});
        	 
        }
       	
//       	$('.userProfiles').change(function(){
//       		let text = this.value;
//       		const myArray = text.split(',');
//       		let word = myArray[1];
//       	  alert("The text has been changed." + word);
//       	  
//       	  if(word === "REGION" ){
//       		$('.regionList').show(); 
//       	  }else{
//       		$('.regionList').hide();  
//       	  }
//       	});
		
	}); 