
var save=false;
	jQuery(document).ready(function($) {
				console.log("province: "+ $(".province").val());
				console.log(".municipality: " +$(".municipality").val());
				console.log(".barangay: " + $(".barangay").val());
				
				refreshLocation();
				getPage("fishcage");
                 	$(".nameOfOperator").val("");             
                 	$(".classificationofOperator").val("");
                 	$(".cageDimension").val("");
                 	//$(".cageTotalArea").val("");
                 	$(".cageType").val("");
                 	$(".cageNoOfCompartments").val("");
                 	$(".indicateSpecies").val("");
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".sourceOfData").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
            	save=true;
            	
            	getFishCageList();
            	getAllFishCageFirstPage();
            	getPerPage();

  	});  	  

function getAllFishCageFirstPage(){
	
	
		 $.get("fishcage/" + "1", function(content, status){
	  
	 var markup = "";
			for ( var x = 0; x < content.fishCage.length; x++) {
			   console.log(content.fishCage[x].id);
			   markup +="<tr data-href="+ content.fishCage[x].uniqueKey+">" +
			   "<td>"+content.fishCage[x].region+"</td>" +
			   "<td>"+content.fishCage[x].province+"</td>" +
			   "<td>"+content.fishCage[x].municipality+"</td>" +
			   "<td>"+content.fishCage[x].barangay+"</td>" +
			   "<td>"+content.fishCage[x].nameOfOperator+"</td>" +
			    "<td>"+content.fishCage[x].lat+","+content.fishCage[x].lon+"</td>" +
			   "</tr>";
			}
			$("#export-buttons-table").find('tbody').empty();
			$("#export-buttons-table").find('tbody').append(markup);
		$("#paging").empty();		
		$("#paging").append(content.pageNumber);	
	  });

}
function getFishCageList(){
	$.get("fishcageList", function(content, status){
	        	
	        	 var markup = "";
	        	 var tr;
	       
	         			for ( var x = 0; x < content.fishCage.length; x++) {
	         			region = content.fishCage[x].region;
						tr = $('<tr/>');
						
						tr.append("<td>" + content.fishCage[x].province + "</td>");
						tr.append("<td>" + content.fishCage[x].municipality + "</td>");
						tr.append("<td>" + content.fishCage[x].barangay + "</td>");
						tr.append("<td>" + content.fishCage[x].nameOfOperator + "</td>");
						tr.append("<td>" + content.fishCage[x].code + "</td>");
						tr.append("<td>" + content.fishCage[x].area + "</td>");
						tr.append("<td>" + content.fishCage[x].classificationofOperator + "</td>");
						tr.append("<td>" + content.fishCage[x].cageDimension + "</td>");
					//	tr.append("<td>" + content.fishCage[x].cageTotalArea + "</td>");
						tr.append("<td>" + content.fishCage[x].cageType + "</td>");
						tr.append("<td>" + content.fishCage[x].cageNoOfCompartments + "</td>");
						tr.append("<td>" + content.fishCage[x].indicateSpecies + "</td>");
						tr.append("<td>" + content.fishCage[x].sourceOfData + "</td>");
						tr.append("<td>" + content.fishCage[x].lat + "</td>");
						tr.append("<td>" + content.fishCage[x].lon + "</td>");
						tr.append("<td>" + content.fishCage[x].remarks + "</td>");
						$('#emp_body').append(tr);	
						}    		
	    		  });
	} 

function getPerPage(){   
    

	 // var trigger = $('ul li a'); 
	  $('#paging').delegate('a', 'click' , function(){
   //  trigger.on('click', function(){
   	  
         var $this = $(this),
            target = $this.data('target');    
         	
     	   $.get("fishcage/" + target, function(content, status){
       	  
       	 var markup = "";
        			for ( var x = 0; x < content.fishCage.length; x++) {
        			 
        			   markup +="<tr data-href="+ content.fishCage[x].uniqueKey+">"+
        			   "<td>"+content.fishCage[x].region+"</td>"+
        			   "<td>"+content.fishCage[x].province+"</td>"+
        			   "<td>"+content.fishCage[x].municipality+"</td>"+
        			   "<td>"+content.fishCage[x].barangay+"</td>"+
        			   "<td>"+content.fishCage[x].nameOfOperator+"</td>"+
        			   "</tr>";
					}
   			
   				$("#export-buttons-table").find('tbody').empty();
   				$("#export-buttons-table").find('tbody').append(markup);
   			$("#paging").empty();		
   			$("#paging").append(content.pageNumber);	
   		  });
   	  
         return false;
       });
       

    }

$('#export-buttons-table').delegate('tr', 'click' , function(){
$("#tbodyID").empty();
  var uniqueKey = $(this).data('href');
	   	save = false;
  $.get("getFishCage/" + uniqueKey, function(data, status){
     
			getFishCageInfoById(data, status);
    		  });
             
});

 function generatepdfFISHCAGE() {
      var doc = new jsPDF('p', 'pt');

      var elem = document.getElementById('pdfFISHCAGE');
      var imgElements = document.querySelectorAll('#pdfFISHCAGE tbody img');
      var data = doc.autoTableHtmlToJson(elem);
      var images = [];
      var i = 0;
      console.log(imgElements[i].src);
      var img = imgElements[i].src;
      doc.autoTable(data.columns, data.rows, {
       // bodyStyles: {rowHeight: 30},
        drawCell: function(cell, opts) {
          if (opts.column.dataKey === 1) {
            images.push({
              url: img,
              x: cell.textPos.x,
              y: cell.textPos.y
            });
            i++;
          }
        },
        addPageContent: function() {
        console.log(images.length);
          for (var i = 0; i < images.length; i++) {
          	if(i == images.length-1){
            doc.addImage(images[i].url, 100, images[i].y, 200, 200);
            }
          }
        }
      });

      doc.save("fishcage.pdf");
         window.location.reload();
    }


			$(function() {
				$(".exportToExcel").click(function(e){
					var table = $(this).prev('.table2excel');
					if(table && table.length){
						var preserveColors = (table.hasClass('table2excel_with_colors') ? true : false);
						$(table).table2excel({
							exclude: ".noExl",
							name: "Excel Document Name",
							filename: "FishCage.xls",
							fileext: ".xls",
							exclude_img: true,
							exclude_links: true,
							exclude_inputs: true,
							preserveColors: preserveColors
						});
					}
				});
				
			});

//
//	 $(document).ready(function () {
//	 	  $('.cageTotalArea').change(function(){
//		    var areaValue = $(this).val();
//		    var regexp = /^\d+(\.\d{1,4})?$/;
//		    if(areaValue == ''){
//		    	return;
//		    }
//		    regexp.test('10.5');
//		    console.log( areaValue + " returns " + regexp.test(areaValue));
//		  if(!regexp.test(areaValue)){
//			  alert("INVALID AREA");
//			  $(".cageTotalArea").val("");
//		  }
//
//		});
//	  
//	  
//	 });
//	 

	 function getFishCageInfoById(data, status){
	 	var content =  $("#pdfFISHCAGE").find('tbody').empty();
	 	
	     for ( var x = 0; x < data.length; x++) {
	 		var obj = data[x];

	 		getProvinceMunicipalityBarangay(obj);
	 		
	 		$(".id").val(obj.id);
	 	    $(".user").val(obj.user);
	 	    $(".uniqueKey").val(obj.uniqueKey);
	 	    $(".dateEncoded").val(obj.dateEncoded);
	 	    $(".encodedBy").val(obj.encodedBy);
	     $(".fishcagenameOfOperator").val(obj.nameOfOperator);           
	     $(".classificationofOperator").val(obj.classificationofOperator);
	     $(".cageDimension").val(obj.cageDimension);
	     $(".area").val(obj.area);
	     $(".cageType").val(obj.cageType);
	     $(".cageNoOfCompartments").val(obj.cageNoOfCompartments);
	     $(".fishcageindicateSpecies").val(obj.indicateSpecies);
	     $(".fishcagesourceOfData").val(obj.sourceOfData);
	 	//$(".area").val(obj.area);
	 	$(".code").val(obj.code);
	 	$(".fishcagedateAsOf").val(obj.dateAsOf);
	 	$(".fishcageremarks").val(obj.remarks);
	 	$(".lat").val(obj.lat);
	 	$(".lon").val(obj.lon);
	 	$(".image_src").val(obj.image_src);
	 	$(".image").val(obj.image);
	 	$(".preview").attr("src", obj.image_src);


	     var markup = 
	     "<tr><th scope=\"col\">FISH CAGE LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
	     "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
	     "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
	     "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
	     "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
	     
	     "<tr><th scope=\"col\">FISH CAGE INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
	     "<tr><th scope=\"row\">Name of Operator:</th><td>"+obj.nameOfOperator+"</td></tr>"+
	    "<tr><th scope=\"row\">Classification of Operator:</th><td>" + obj.classificationofOperator + "</td></tr>"+

	    "<tr><th scope=\"row\">Cage Dimension:</th><td>" + obj.cageDimension + "</td></tr>"+
	    "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
	   "<tr><th scope=\"row\">Cage Type:</th><td>" + obj.cageType + "</td></tr>"+
	   "<tr><th scope=\"row\">Cage No. of Compartments:</th><td>" + obj.cageNoOfCompartments + "</td></tr>"+
	   "<tr><th scope=\"row\">Indicate Species:</th><td>" + obj.indicateSpecies + "</td></tr>"+

	    "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
	   "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
	   "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.sourceOfData + "</td></tr>"+
	   "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
	   
	   "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
	   "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
	   "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
	   "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
	  
	     content.append(markup);
	     
	    // reset();
	     markup = "";
	     $('#img_preview').show();
	     $('#pdf').show();
	     $('#table').hide();			
	 }

	 	
	 }

	