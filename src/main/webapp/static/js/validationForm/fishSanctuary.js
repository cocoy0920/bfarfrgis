	var save = false;
jQuery(document).ready(function($) {
	

	$('.file').prop('required',true);
	$(".exportToExcel").hide();

//	 $('.container').show();
//	 $('#mapview').hide();

	//getImplementor();
	//refreshLocation();
	getPage("fishsanctuaries");
	getFishSanctuaryList();
	getAllFishSanctuaryPageUser();
	getPerPage();
});	
	var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
	$(".submitFishSanctuaries").click(function(event) {

        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
          
          form.classList.add('was-validated'); 
        }
        if (form.checkValidity() === true) {
            event.preventDefault();
            
            document.getElementById("submitFishSanctuaries").disabled = true;
            
            var array = jQuery(form).serializeArray();
        	var json = {};

        	jQuery.each(array, function() {

        		json[this.name] = this.value || '';
        	});
        	
        	var formData = JSON.stringify(json);
        	
        	sendingForm(formData);
        	form.classList.remove('was-validated'); 
        	
          }
        
      
		});
	});
    
    var validations = Array.prototype.filter.call(forms, function(form) {
    $(".cancel").click(function(event) {
    	  document.getElementById("submitFishSanctuaries").disabled = false;
    	  form.classList.remove('was-validated'); 
    	window.scrollTo(0, 0); 
    	 $('.form_input').hide();
    	 $('.container').show();
    	 $('#mapview').hide();
		  
	  });
    });
    
    $(".create").click(function(event) {
    	$('#mapview').hide();
    	 $('.container').hide();
    	 document.getElementById("myform").reset();		
    	 document.getElementById("preview").src = "";
			var img_preview = document.getElementById("img_preview");
			var pdf_view = document.getElementById("pdf");
			img_preview.style.display = "none";
			pdf_view.style.display = "none";
			document.querySelector('#submitFishSanctuaries').innerHTML = 'SAVE'; 
			save = true;
		
			getImplementor();
			$('#bfarData').hide();
			$('#denrData').hide();
//			var bfarData = document.getElementById("bfarData");
//			bfarData.style.display = "none";
//			var denrData = document.getElementById("denrData");
//			denrData.style.display = "none";
			$('.province').find('option').remove().end();
			refreshLocation();
		 $('.form_input').show();
		 $('.container').hide();
    	 $('#mapview').hide();

	  });
    

    
    function getAllFishSanctuaryPageUser(){

    	$.get("/create/fishsanctuaries/"+ "0", function(content, status) {

    		var markup = "";
    		//var ShowHide ="";
    		for (var x = 0; x < content.fishSanctuaries.length; x++) {

    			var active = content.fishSanctuaries[x].enabled;
    			if(active){
    				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.fishSanctuaries[x].id + ">ACTIVE</td>"+
    				"<td class='map menu_links btn-primary' data-href="+ content.fishSanctuaries[x].uniqueKey + ">MAP/UPDATE</td>";
    			}else{
    				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
    				"<td class='menu_links btn-primary'>Not applicable</td>";
    			}
    			markup += "<tr>" +
            
    						"<td><span>" + content.fishSanctuaries[x].region + "</span></td>" +
//    						"<span class='spnTooltip'>" +
//    						"<strong>CLICK FOR VIEW/EDIT</strong><br/></span>" +"</td>" +
    						"<td>" + content.fishSanctuaries[x].province + "</td>" +
    						"<td>" + content.fishSanctuaries[x].municipality + "</td>" +
    						"<td>" + content.fishSanctuaries[x].barangay + "</td>" +
    						"<td>" + content.fishSanctuaries[x].nameOfFishSanctuary + "</td>" +
    						"<td>" + content.fishSanctuaries[x].lat + "," + content.fishSanctuaries[x].lon + "</td>" +
    						/*"<td class='map menu_links btn-primary' data-href="+ content.fishSanctuaries[x].uniqueKey + ">MAP/UPDATE</td>" +
    						"<td class='del menu_links btn-primary' data-href="+ content.fishSanctuaries[x].id + ">ACTIVE</td>" +*/

    						ShowHide + 
    						"</tr>";
    			
    		}
    		$("#export-buttons-table").find('tbody').empty();
    		$("#export-buttons-table").find('tbody').append(markup);
    		$("#paging").empty();
    		$("#paging").append(content.pageNumber);
    	});

    }

    function sendingForm(form) {
    	
    	var json = form;
    	$.ajax({
    		url : "/create/saveFishSanctuary",
    		type : "POST",
    		contentType : "application/json",
    		data : json,
    		dataType : 'json',
    		success : function(content) {
    			getAllFishSanctuaryPageUser();

    			document.getElementById("myform").reset();
    			document.getElementById("preview").src = "";
    			var img_preview = document.getElementById("img_preview");
    			var pdf_view = document.getElementById("pdf");
    			img_preview.style.display = "none";
    			pdf_view.style.display = "none";
    			save = true;
    			$('.province').find('option').remove().end();
    			
    			refreshLocation();
    			
    			$(".se-pre-con").hide();
    			alert("SUCCESSFULLY SAVE");
    			window.location.replace("/create/fishSanctuaryMap/" + content.uniqueKey);
    			$('#bfarData').hide();
    			$('#denrData').hide();
    			$('#pdftable').hide();

    		},
    		error : function(data) {
    			console.log("error", data);
    			$(".se-pre-con").hide();
    			return;

    		}
    	});
    }

//var token = $("meta[name='_csrf']").attr("content");
//
//var header = $("meta[name='_csrf_header']").attr("content");
//
//$(document).ajaxSend(function(e, xhr, options) {
//
//	xhr.setRequestHeader(header, token);
//
//});




//viewing of DATA PER ID AND PDF FILE CREATION


function myFunction(value) {
  txt += value + "<br>";
}

//clicking on table row for viewing of data to form
//$('#export-buttons-table').delegate('tr', 'click', function() {
//	$("#tbodyID").empty();
//	var uniqueKey = $(this).data('href');
//	console.log("*[data-href]" + uniqueKey);
//	save = false;
//
//	$.get("/create/info/" + uniqueKey, function(data, status) {
//
//		for (var x = 0; x < data.length; x++) {
//			var obj = data[x];
//			var bfardenr = obj.bfardenr;
//			console.log(bfardenr);
//			if (bfardenr == 'BFAR') {
//				$('#bfarData').show();
//				$('#denrData').hide();
//			}
//			if (bfardenr == 'DENR') {
//				$('#bfarData').hide();
//				$('#denrData').show();
//			}
//
//		}
//		$('.create').show();
//		getFishSanctuaryInfoById(data, status);
//	});
//	 document.querySelector('#submitFishSanctuaries').innerHTML = 'UPDATE'; 
//});

//$('#export-buttons-table').delegate('td.view', 'click', function() {
//	$("#tbodyID").empty();
//	uniqueKey = $(this).data('href');
//	save = false;
//
//	$.get("/create/info/" + uniqueKey, function(data, status) {
//
//		for (var x = 0; x < data.length; x++) {
//			var obj = data[x];
//			var bfardenr = obj.bfardenr;
//			console.log(bfardenr);
//			if (bfardenr == 'BFAR') {
//				$('#bfarData').show();
//				$('#denrData').hide();
//			}
//			if (bfardenr == 'DENR') {
//				$('#bfarData').hide();
//				$('#denrData').show();
//			}
//
//		}
//		$('.create').show();
//		getFishSanctuaryInfoById(data, status);
//
//	});	
//	 document.querySelector('#submitFishSanctuaries').innerHTML = 'UPDATE'; 
//});
//

$('#export-buttons-table').delegate('td.map', 'click', function() {
	$("#tbodyID").empty();
	var id = $(this).data('href');
	save = false;
	$('#mapview').show();
	window.location.replace("/create/fishSanctuaryMap/" + id);

});
$('#export-buttons-table').delegate('td.del', 'click', function() {
	$("#tbodyID").empty();
	var id = $(this).data('href');
	save = false;
//	 let text = "are you sure you want to delete";
//	 
//	  if (confirm(text) == true) {
//			window.location.replace("/create/fishSanctuaryDeleteById/" + id);
//	  } else {
//	    text = "You canceled!";
//	  }

	let text;
	  let reason = prompt("Please enter your reason:","");
	  if (reason == null || reason == "") {
	    text = "User cancelled the prompt.";
	  } else {
		  window.location.replace("/create/fishSanctuaryDeleteById/" + id +"/" + reason);
		  console.log(reason);

	  }

});


$(function() {
	$(".exportToPDF").click(function(e){

//	var element = document.getElementById("myprogressBar");
//	var width = 1;
//	var identity = setInterval(scene, 10);
//	function scene() {
//		if (width >= 100) {
//			clearInterval(identity);
//		} else {
//			width++;
//			//element.style.width = width + '%'; 
//			//element.innerHTML = width * 1 + '%'; 
//		}
//	}

	var doc = new jsPDF('p', 'pt');

	var elem = document.getElementById('pdfFISHSANCTUARY');
	var imgElements = document.querySelectorAll('#pdfFISHSANCTUARY tbody img');
	
	var data = doc.autoTableHtmlToJson(elem);
	var images = [];
	var i = 0;
	var img = imgElements[i].src;
	
	doc.autoTable(data.columns, data.rows, {
		// bodyStyles: {rowHeight: 30},
		drawCell : function(cell, opts) {
			if (opts.column.dataKey === 1) {
				images.push({
					url : img,
					url : img,
					x : cell.textPos.x,
					y : cell.textPos.y
				});
				
				i++;
			}
		},
		addPageContent : function() {
			
			for (var i = 0; i < images.length; i++) {
				if (i == images.length - 1) {
					doc.addImage(images[i].url, 100, images[i].y, 200, 200);
				}
			}
			
			
		}
	});

	doc.save("fishSanctuaries.pdf");
	window.location.reload();
	});
});





function getImplementor(){
	var slctImplementer=$('.bfardenr').find('option').remove().end();
	option ="";
	slctImplementer.empty();
	
	var implementer = ["BFAR","DENR","LGU","NGO","SCHOOL"];
	var implementerLen = implementer.length;

	option = "<option value=''>-- pls select--</option>";
	for (let i = 0; i < implementerLen; i++) {

		option += "<option value='"+implementer[i]+ "'>"+implementer[i]+"</option>";
		
		
	}
	
	slctImplementer.append(option);
	
}

$(function() {
	$(".exportToExcel")
			.click(
					function(e) {
						var table = $(this).prev('.table2excel');
						if (table && table.length) {
							var preserveColors = (table
									.hasClass('table2excel_with_colors') ? true
									: false);
							$(table).table2excel({
								exclude : ".noExl",
								name : "Excel Document Name",
								filename : "FishSanctuary.xls",
								fileext : ".xls",
								exclude_img : true,
								exclude_links : true,
								exclude_inputs : true,
								preserveColors : preserveColors
							});
						}
					});

});

function getFishSanctuaryList(){
	$.get("/create/fishsanctuariesList",function(content, status) {
		
				var markup = "";
				var tr;
				var region="";
				var dataAsOf="";
				
				var excelHead="";
			
				for (var x = 0; x < content.length; x++) {
					
					 if(content.length - 1 === x) {
						 region = content[x].region;
						 dataAsOf = content[x].dateAsOf;
					        $(".exportToExcel").show();
					    }
					
					tr = $('<tr/>');
					
					tr.append("<td>" + content[x].province+ "</td>");
					tr.append("<td>" + content[x].municipality+ "</td>");
					tr.append("<td>" + content[x].barangay+ "</td>");
					tr.append("<td>"+ content[x].nameOfFishSanctuary+ "</td>");
					tr.append("<td>" + content[x].code + "</td>");
					tr.append("<td>" + content[x].area + "</td>");
					tr.append("<td>" + content[x].type + "</td>");
					tr.append("<td>"+ content[x].fishSanctuarySource+ "</td>");
					tr.append("<td>" + content[x].bfardenr+ "</td>");
					tr.append("<td>" + content[x].sheltered+ "</td>");
					tr.append("<td>"+ content[x].nameOfSensitiveHabitatMPA+ "</td>");
					tr.append("<td>" + content[x].ordinanceNo+ "</td>");
					tr.append("<td>" + content[x].ordinanceTitle+ "</td>");
					tr.append("<td>" + content[x].dateEstablished+ "</td>");
					tr.append("<td>" + content[x].lat + "</td>");
					tr.append("<td>" + content[x].lon + "</td>");
					tr.append("<td>" + content[x].remarks+ "</td>");

					$('#emp_body').append(tr);
					//ExportTable();
				}
//				excelHead +=  "<tr><th align='center' colspan='17'>FISH SANCTUARY</th></tr>"			                
//		            + "<tr><th>" + region +"</th>"
//		            + "<th></th>"
//		            + "<th></th>"
//		            + "<th>Data as of:" +dataAsOf+ "</th>"
//		            + "</tr>"             
//		            + "<tr><th>Province</th>"
//		            + "<th>Municipality</th>"
//		            + "<th>Barangay</th>"
//		            + "<th>Name of FishSanctuary</th>"
//		            + "<th>Code</th>"
//		            + "<th>Area</th>"
//		            + "<th>Type</th>"
//		            + "<th>Data Source</th>"
//		            + "<th>Implementer</th>"
//		            + "<th>Sheltered</th>"
//		            + "<th>Name Of Sensitive Habitat MPA</th>"
//		            + "<th>Ordinance No.</th>"
//		            + "<th>Ordinance Title</th>"
//		            + "<th>Date Established</th>"
//		            + "<th>Latitude</th>"
//		            + "<th>Longitude</th>"
//		            + "<th>Remarks</th></tr>";	
//				
//				
//				$('#thead_body').append(excelHead);
			});

	}

function getPerPage() {
	$('#paging').delegate('a','click',function() {
					var $this = $(this), 
					target = $this.data('target');
					
					$.get("/create/fishsanctuaries/" + target, function(content, status) {

						var markup = "";

						for (var x = 0; x < content.fishSanctuaries.length; x++) {
							var active = content.fishSanctuaries[x].enabled;
			    			if(active){
			    				ShowHide = "<td class='del menu_links btn-primary' data-href="+ content.fishSanctuaries[x].id + ">ACTIVE</td>"+
			    				"<td class='map menu_links btn-primary' data-href="+ content.fishSanctuaries[x].uniqueKey + ">MAP/UPDATE</td>";
			    			}else{
			    				ShowHide = "<td class='menu_links btn-primary'>INACTIVE</td>"+
			    				"<td class='menu_links btn-primary'>Not applicable</td>";
			    			}
							markup += "<tr>" + 
									"<td><span>" + content.fishSanctuaries[x].region + "</span> +</td>" +
									"<td>"+ content.fishSanctuaries[x].province + "</td>" +
									"<td>"+ content.fishSanctuaries[x].municipality+ "</td>" +
									"<td>" + content.fishSanctuaries[x].barangay + "</td>" +
									"<td>"+ content.fishSanctuaries[x].nameOfFishSanctuary+ "</td>" +
									"<td>" + content.fishSanctuaries[x].lat + "," + content.fishSanctuaries[x].lon+ "</td>" +
									ShowHide + 
									//"<td class='map menu_links btn-primary' data-href="+ content.fishSanctuaries[x].uniqueKey + ">MAP/UPDATE</td>" +
									//"<td class='del menu_links btn-primary' data-href="+ content.fishSanctuaries[x].id + ">ACTIVE</td>" +
									//"<td class='view menu_links btn-primary' data-href="+ content.fishSanctuaries[x].uniqueKey + ">View</td>" +
									"</tr>";
						}
						$("#export-buttons-table").find('tbody').empty();
						$("#export-buttons-table").find('tbody').append(markup);
						$("#paging").empty();
						$("#paging").append(content.pageNumber);
					});
					//return target;
	});

}

function getFishSanctuaryInfoById(data, status) {

	var content = $("#pdfFISHSANCTUARY").find('tbody').empty();
	var markup = null
	//$('#customers').hide();

	for (var x = 0; x < data.length; x++) {
		var obj = data[x];

		$(".id").val(obj.id);
		$(".user").val(obj.user);
		$(".uniqueKey").val(obj.uniqueKey);
		$(".dateEncoded").val(obj.dateEncoded);
		$(".encodedBy").val(obj.encodedBy);
		
		getProvinceMunicipalityBarangay(obj); 
		
		$(".nameOfFishSanctuary").val(obj.nameOfFishSanctuary);
		$(".area").val(obj.area);
		

		
		var slctImplementer=$('.bfardenr').find('option').remove().end();
		option ="";
		slctImplementer.empty();
		
		var implementer = ["BFAR","DENR","LGU","NGO","SCHOOL"];
		var implementerLen = implementer.length;

		option = "<option value='"+obj.bfardenr+ "'>"+obj.bfardenr+"</option>";
		for (let i = 0; i < implementerLen; i++) {
			if(implementer[i] == obj.bfardenr){
				
			}else{
			option += "<option value='"+implementer[i]+ "'>"+implementer[i]+"</option>";
			}
			
		}
		option += "<option value=''>-- pls select--</option>";
		
		slctImplementer.append(option);
		
		if(obj.bfardenr === 'BFAR'){
			
		$(".bfardenr").val(obj.bfardenr);
		$(".sheltered").val(obj.sheltered);
		
		}
		else if(obj.bfardenr === 'DENR'){

		$(".bfardenr").val(obj.bfardenr);
		$(".nameOfSensitiveHabitatMPA").val(obj.nameOfSensitiveHabitatMPA);
		$(".ordinanceNo").val(obj.ordinanceNo);
		$(".ordinanceTitle").val(obj.ordinanceTitle);
		$(".dateEstablished").val(obj.dateEstablished);
		
		}else{
			$(".bfardenr").val(obj.bfardenr);
			$(".sheltered").val(obj.sheltered);
		}
		
		$(".type").val(obj.type);
		$(".code").val(obj.code);
		$(".dateAsOf").val(obj.dateAsOf);
		$(".fishSanctuarySource").val(obj.fishSanctuarySource);
		$(".remarks").val(obj.remarks);
		$(".lat").val(obj.lat);
		$(".lon").val(obj.lon);
		
		$(".image_src").val(obj.image_src);
		$(".image").val(obj.image);
		$(".preview").attr("src", obj.image);
		
		var implementer = null;
		var last = null;

		markup = "<tr><td scope=\"col\">FISH SANCTUARY LOCATION</td><td scope=\"col\">&nbsp;</td></tr>"
				+ "<tr><td scope=\"row\">Region</td><td>"
				+ obj.region
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Province</td><td>"
				+ obj.province
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Municipality</td><td>"
				+ obj.municipality
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Barangay</td><td>"
				+ obj.barangay
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Code</td><td>"
				+ obj.code
				+ "</td></tr>"
				+ "<tr><td scope=\"col\">FISH SANCTUARY INFORMATION</td><td scope=\"col\">&nbsp;</td></tr>"
				+ "<tr><td scope=\"row\">Name of Sanctuary/Protected Area:</td><td>"
				+ obj.nameOfFishSanctuary
				+ "</td></tr>";

				if(obj.bfardenr === 'BFAR'){		
				implementer = "<tr><td scope=\"row\">Implementer:</td><td>"
				+ obj.bfardenr 
				+"</td></tr>"
				+ "<tr><td scope=\"row\">Name of Sheltered Fisheries/Species:</td><td>"
				+ obj.sheltered + "</td></tr>";
				}
				else if(obj.bfardenr === 'DENR'){
					implementer =  "<tr><td scope=\"row\">Implementer:</td><td>"+obj.bfardenr+"</td></tr>"
				+ "<tr><td scope=\"row\">Name of Sensitive Habitat with MPA:</td><td>"
				+ obj.nameOfSensitiveHabitatMPA
				+ "</td></tr>"				
				+ "<tr><td scope=\"row\">Ordinance No:</td><td>"
				+ obj.ordinanceNo
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Ordinance Title:</td><td>"
				+ obj.ordinanceTitle
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Data Established:</td><td>"
				+ obj.dateEstablished
				+ "</td></tr>"
				}else{
					implementer =  "<tr><td scope=\"row\">Implementer:</td><td>"+obj.bfardenr+"</td></tr>"
				}
				last = "<tr><td scope=\"row\">Area:</td><td>"
				+ obj.area
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Type:</td><td>"
				+ obj.type
				+ "</td></tr>"
				+ "<tr><td scope=\"col\">ADDITIONAL INFORMATION</td><td scope=\"col\">&nbsp;</td></tr>"
				+ "<tr><td scope=\"row\">Date as of:</td><td>"
				+ obj.dateAsOf
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Data Sources:</td><td>"
				+ obj.fishSanctuarySource
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Remarks:</td><td>"
				+ obj.remarks
				+ "</td></tr>"
				+ "<tr><td  scope=\"col\">GEOGRAPHICAL LOCATION</td><td scope=\"col\">&nbsp;</td></tr>"
				+ "<tr><td scope=\"row\">Latitude:</td><td>"
				+ obj.lat
				+ "</td></tr>"
				+ "<tr><td scope=\"row\">Longitude:</td><td>"
				+ obj.lon
				+ "</td></tr>"
				+ "<tr><td  scope=\"col\">IMAGE</td><td scope=\"col\">&nbsp;</td></tr>"
				+ "<tr><td  scope=\"col\"></td><td scope=\"col\">&nbsp;</td></tr>"
				+ "<tr align=\"left\"><td><img id=\"imgview\" src=\""
				+ obj.image_src + "\"></img></td>&nbsp;<td></td></tr>";
				
				$("#pdfFISHSANCTUARY").find('tbody').append(markup + implementer + last);
		//content.append(markup);

		//reset();

		markup = "";
		$('#img_preview').show();
		$('#pdf').show();
		$('#table').hide();
		$('.file').prop('required',false);
		//   $('.provinceForm').text(obj.province);
		//  $('.regionForm').text(obj.region);

	}
}

