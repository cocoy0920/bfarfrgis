	jQuery(document).ready(function($) {

		$("#clearFishProcessingPlants").click(function(e){
				
			        	$(".fishprocessingdateAsOf").val("");
			        	
						$(".fishprocessingplantprovince").empty();
			        	$(".fishprocessingplantmunicipality").empty();
			        	$(".fishprocessingplantbarangay").val("");
			        	$(".nameOfProcessingPlants").val("");
			        	$(".nameOfOperator").val("");
			        	$(".fishprocessingArea").val("");
			        	$(".operatorClassification").val("");
			        	$(".processingTechnique").val("");
			        	$(".processingEnvironmentClassification").val("");
			        	$(".packagingType").val("");
			        	$(".businessPermitsAndCertificateObtained").val("");
			        	$(".bfarRegistered").val("");
			        	$(".plantRegistered").val("");
			        	$(".marketReach").val("");
			        	$(".indicateSpecies").val("");
			        	$(".code").val("");
			        	$(".sourceOfData").val("");
			        	$(".fishProcessingPlantRemarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".file").val("");
			        	$(".image").val("");
			        	$(".image_src").val("");
			        	$('.preview').attr('src', '');
			        	$('#plantData').hide(); 
					
			});
	});