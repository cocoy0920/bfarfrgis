
function clickZoom(e) {
	   map.setView(e.target.getLatLng(),17);
}

function clickZoomOut(e) {
	   map.setView(e.target.getLatLng(),10);
}


var LeafIconMarker = L.Icon.extend({
		options: {
			iconSize:[20,25]
		}
});

var geoJSON= new L.LayerGroup();

var sanctuaryicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/FishSanctuary.png"});
var FS = new L.LayerGroup();

var processingicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/Fish_Processing.png"});
var FP = new L.LayerGroup();

var FP_BFAR = new L.LayerGroup();
var FP_LGU = new L.LayerGroup();
var FP_PFDA = new L.LayerGroup();
var FP_PRIVATE = new L.LayerGroup();
var FP_COOPERATIVE = new L.LayerGroup();

var FP_BFAR_ICON = new LeafIconMarker({iconUrl:"/static/images/pin2022/Fish_Processing.png"});
var FP_LGU_ICON  = new LeafIconMarker({iconUrl:"/static/images/pin2022/Fish_Processing_LGU.png"});
var FP_PRIVATE_ICON  = new LeafIconMarker({iconUrl:"/static/images/pin2022/Fish_Processing_PrivateSector.png"});
var FP_COOPERATIVE_ICON  = new LeafIconMarker({iconUrl:"/static/images/pin2022/Fish_Processing_Cooperative.png"});

var ncentericon = new LeafIconMarker({iconUrl:"/static/images/pin2022/ProvincialFisheriesOffice.png"});
var NCENTER = new L.LayerGroup();

var rofficeicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/RegionalFisheriesOffice.png"});
var ROFFICE = new L.LayerGroup();

var toscon = new LeafIconMarker({iconUrl:"/static/images/pin2022/TechnologyOutreachStation.png"});
var TOS = new L.LayerGroup();

var landingicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/FishLanding.png"});
var FL = new L.LayerGroup();

var FLOperationalIcon = new LeafIconMarker({iconUrl:"/static/images/pin2022/CFLC_operational.png"});
var FL_operational = new L.LayerGroup();

var FLForOperationalIcon = new LeafIconMarker({iconUrl:"/static/images/pin2022/CFLC_foroperational.png"});
var FL_for_operation = new L.LayerGroup();

var FLForCompletionIcon = new LeafIconMarker({iconUrl:"/static/images/pin2022/CFLC_completion.png"});
var FL_for_completion = new L.LayerGroup();

var FLForTransferIcon = new LeafIconMarker({iconUrl:"/static/images/pin2022/CFLC_transfer.png"});
var FL_for_transfer = new L.LayerGroup();

var FLDamagedIcon = new LeafIconMarker({iconUrl:"/static/images/pin2022/CFLC_damaged.png"});
var FL_damaged = new L.LayerGroup();

var FLNontraditionalIcon = new LeafIconMarker({iconUrl:"/static/images/pin2022/non-traditional.png"});
var FL_noncflc = new L.LayerGroup();

var FLTraditionalIcon = new LeafIconMarker({iconUrl:"/static/images/pin2022/traditional.png"});
var FL_traditional = new L.LayerGroup();

var porticon = new LeafIconMarker({iconUrl:"/static/images/pin2022/FishPort.png"});
var FPT = new L.LayerGroup();

var penicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/FishPen.png"});
var FPEN = new L.LayerGroup();

var cageicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/FishCage.png"});
var FCage = new L.LayerGroup();

var pondicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/FishPond.png"});
var FPond = new L.LayerGroup();

var ipcsicon = new LeafIconMarker({iconUrl:"/static/images/pin/new/IcePlant.png"});
var IPCS = new L.LayerGroup();

var ipcsbfaricon = new LeafIconMarker({iconUrl:"/static/images/pin2022/IcePlant_BFAR.png"});
var IPCS_PFDA = new L.LayerGroup();

var ipcscoopicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/IcePlant_Cooperative.png"});
var IPCS_COOPERATIVE = new L.LayerGroup();

var ipcsprivateicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/IcePlant_PrivateSector.png"});
var IPCS_PRIVATEORGANIZATION = new L.LayerGroup();

var marketicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/Market.png"});
var Market = new L.LayerGroup();

var schoolicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/FisheriesSchool.png"});
var School = new L.LayerGroup();

var fishcoralsicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/FishCorral.png"});
var Fishcorals = new L.LayerGroup();

var seagrassicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/Seagrass.png"});
var Seagrass = new L.LayerGroup();

var seaweedsicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/Seaweeds(postharvest).png"});
var Seaweeds = new L.LayerGroup();

var seaweedsLaboratoryicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/Seaweeds_laboratory.png"});
var Seaweed_laboratory = new L.LayerGroup();

var seaweedsNurseryicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/Seaweeds_Nursery.png"});
var Seaweed_nursery = new L.LayerGroup();
var seaweedsWarehouseicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/seaweeds_warehouse.png"});
var Seaweed_warehouse = new L.LayerGroup();
var seaweedsDryericon = new LeafIconMarker({iconUrl:"/static/images/pin2022/seaweeds_dryer.png"});
var Seaweed_dryer = new L.LayerGroup();

var mangroveicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/Mangrove.png"});
var Mangrove = new L.LayerGroup();

var coralreefsicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/Coral-Reefs.png"});
var CORALREEFS = new L.LayerGroup();

var lguicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/ProvincialFisheriesOffice.png"});
var LGU = new L.LayerGroup();

var maricultureicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/Mariculture_BFAR.png"});
var Mariculture = new L.LayerGroup();

var Mariculture_BFAR = new L.LayerGroup();

var mariculturePrivateicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/Mariculture_PrivateSector.png"});
var Mariculture_PRIVATE = new L.LayerGroup();

var maricultureLGUicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/Mariculture_LGU.png"});
var Mariculture_LGU = new L.LayerGroup();


var trainingcentericon = new LeafIconMarker({iconUrl:"/static/images/pin2022/BIRDTC.png"});
var Trainingcenter = new L.LayerGroup();

var Hatchery = new L.LayerGroup();
var HatcheryComplete = new L.LayerGroup();
var HatcheryinComplete = new L.LayerGroup();
var Hatchery_LGU = new L.LayerGroup();
var Hatchery_PRIVATE = new L.LayerGroup();

var iconComplete = new LeafIconMarker({iconUrl:"/static/images/pin2022/Hatchery.png"});
var iconNComplete = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/hatcheries.png"});
var hatchLGU = new LeafIconMarker({iconUrl:"/static/images/pin2022/Hatchery_LGU.png"});
var hatchPrivate = new LeafIconMarker({iconUrl:"/static/images/pin2022/Hatchery_PrivateSector.png"});
var payaoicon = new LeafIconMarker({iconUrl:"/static/images/pin2022/Payao.png"});
var corals = new LeafIconMarker({iconUrl:"/static/images/pin2022/Lambaklad.png"});
var depth_1000 = new L.LayerGroup();
var depth_2000 = new L.LayerGroup();
var depth_3000 = new L.LayerGroup();
var depth_5000 = new L.LayerGroup();
var depth_10000 = new L.LayerGroup();
var LAMBAKLAD = new L.LayerGroup();
var PAYAO = new L.LayerGroup();

function payaoToLayer(feature, latlng){
	 if(feature.properties.resources == "payao"){
		  return L.marker(latlng,{icon: trainingcentericon});
	  }
}
function PenToLayer(feature, latlng){
	 if(feature.properties.resources == "fishpen"){
		  return L.marker(latlng,{icon: penicon});
	  }
}
function pointToLayer(feature, latlng){

//		  if(feature.properties.resources == "fishsanctuaries"){
//			  return L.marker(latlng,{icon: sanctuaryicon});
//		  }
		  if(feature.properties.resources == "fishprocessingplants"){		
			  return L.marker(latlng,{icon: processingicon});
			 
		  }
		  if(feature.properties.resources == "fishlanding"){		
			  return L.marker(latlng,{icon: landingicon});
		  }

		  if(feature.properties.resources == "FISHPORT"){		
		  return L.marker(latlng,{icon: porticon});
		  }

		  if(feature.properties.resources == "fishpen"){		
		  return L.marker(latlng,{icon: penicon});
		  }

		  if(feature.properties.resources == "fishcage"){		
		  return L.marker(latlng,{icon: cageicon});
		  }
		  if(feature.properties.resources == "fishpond"){		
			  return L.marker(latlng,{icon: pondicon});
			  }

		  if(feature.properties.resources == "coldstorage"){	
			  
			  if(feature.properties.Operator === "BFAR"){
				  return L.marker(latlng,{icon: ipcsbfaricon}); 
			  }
			  if(feature.properties.Operator === "COOPERATIVE"){
				  return L.marker(latlng,{icon: ipcscoopicon}); 
			  }
			  if(feature.properties.Operator === "PRIVATE ORGANIZATION"){
				  return L.marker(latlng,{icon: ipcsprivateicon}); 
			  }

		  }

		  if(feature.properties.resources == "market"){		
		  return L.marker(latlng,{icon: marketicon});
		  }
		  
		  if(feature.properties.resources == "schoolOfFisheries"){		
		  return L.marker(latlng,{icon: schoolicon});
		  }

		  if(feature.properties.resources == "fishcoral"){		
		  return L.marker(latlng,{icon: fishcoralsicon});
		  }

		  if(feature.properties.resources == "seagrass"){		
		  return L.marker(latlng,{icon: seagrassicon});
		  }

		  if(feature.properties.resources == "seaweeds"){
			  if(feature.properties.Type === "SEAWEED LABORATORY"){
				  return L.marker(latlng,{icon: seaweedsLaboratoryicon});
				}
				if(feature.properties.Type === "SEAWEED NURSERY"){
					 return L.marker(latlng,{icon: seaweedsNurseryicon});
				}
				if(feature.properties.Type === "SEAWEED WAREHOUSE"){
					 return L.marker(latlng,{icon: seaweedsWarehouseicon});
				}
				if(feature.properties.Type === "SEAWEED DRYER"){
					 return L.marker(latlng,{icon: seaweedsDryericon});
				}  
		 
		  }
		  
		  if(feature.properties.resources == "mangrove"){		
		  return L.marker(latlng,{icon: mangroveicon});
		  }

		  if(feature.properties.resources == "lgu"){		
		  return L.marker(latlng,{icon: lguicon});
		  }

		  if(feature.properties.resources == "mariculturezone"){	
			  if(feature.properties.Type === "BFAR"){
				  return L.marker(latlng,{icon: maricultureicon});
				}
				if(feature.properties.Type === "PRIVATE"){
					 return L.marker(latlng,{icon: mariculturePrivateicon});
					 }
				if(feature.properties.Type === "LGU"){
					 return L.marker(latlng,{icon: maricultureLGUicon});
				}
		 
		  }

		  if(feature.properties.resources == "trainingcenter"){		
		  return L.marker(latlng,{icon: trainingcentericon});
		  }
		  
		  if(feature.properties.resources == "hatchery"){	
			  if(feature.properties.Legislative === "legislated"){
				  return L.marker(latlng,{icon: iconComplete});
			  }
			  if(feature.properties.Legislative == "nonLegislated"){
				  return L.marker(latlng,{icon: iconNComplete});
			  }
			  if(feature.properties.Type == "LGU"){
				  return L.marker(latlng,{icon: hatchLGU});
			  }
			  if(feature.properties.Type == "Private"){
				  return L.marker(latlng,{icon: hatchPrivate});
			  }
			  
			  }
		  if(feature.properties.resources == "nationalcenter"){	

			  //return L.marker(latlng,{icon: ncentericon});
			 return L.marker(latlng, {icon: ncentericon}).bindTooltip(feature.properties.Name, {permanent: true, direction: feature.properties.Direction});
//			  return L.marker(latlng, {
//				    icon: L.divIcon({
//				    className: 'my-div-icon',
//				      iconSize:null,
//				      html: '<div><div class="map-label-content">'+feature.properties.Name+'</div><div class="map-label-arrow"></div></div>'
//				    })
//		  });
			  }
		  if(feature.properties.resources == "regionaloffice"){	

			  //return L.marker(latlng,{icon: rofficeicon});
			  return L.marker(latlng, {
				    icon: L.divIcon({
				      iconSize:[100,20],
				      html: "<img src=\"/static/images/pin/new/bfar-infrastructure_RegionalFisheriesOffice.png\" style=\"width: 20px; height: 20px;\">" + "<b>" +feature.properties.Region + "</b>"
				    })
		  });
			  }
		  if(feature.properties.resources == "tos"){	

			  return L.marker(latlng,{icon: toscon});
			  }
		  if(feature.properties.resources == "payao"){	

			  return L.marker(latlng,{icon: payaoicon});
			  }
	
}

function pointToLayerAll(feature, latlng){

//	  if(feature.properties.resources == "fishsanctuaries"){
//		  return L.marker(latlng,{icon: sanctuaryicon});
//	  }
//	  if(feature.properties.resources == "fishprocessingplants"){
//	  if(feature.properties.Operator === "BFARMANAGED"){
//		  return L.marker(latlng,{icon: FP_BFAR_ICON});
//
//		 }
//		 if(feature.properties.Operator === "LGUMANAGED"){
//			 return L.marker(latlng,{icon: FP_LGU_ICON});
//		 }
//
//		 if(feature.properties.Operator === "PRIVATESECTOR"){
//			 return L.marker(latlng,{icon: FP_PRIVATE_ICON});
//		 }
//		 if(feature.properties.Operator === "COOPERATIVE"){
//			 return L.marker(latlng,{icon: FP_COOPERATIVE_ICON}); 
//		 }
//	  }
	  if(feature.properties.resources == "fishlanding"){	

		  if(feature.properties.CFLCSTATUS === "Operational"){
			  return L.marker(latlng,{icon: FLOperationalIcon});
			}
			if(feature.properties.CFLCSTATUS === "ForOperation"){
				 return L.marker(latlng,{icon: FLForOperationalIcon});
			}
			if(feature.properties.CFLCSTATUS === "ForCompletion"){
				 return L.marker(latlng,{icon: FLForCompletionIcon});
			}
			if(feature.properties.CFLCSTATUS === "ForTransfer"){
				 return L.marker(latlng,{icon: FLForTransferIcon});
			}
			if(feature.properties.CFLCSTATUS === "Damaged"){
				 return L.marker(latlng,{icon: FLDamagedIcon});
			}
			if(feature.properties.Type === "Non-Traditional"){
				 return L.marker(latlng,{icon: FLNontraditionalIcon});
			}
			if(feature.properties.Type === "Traditional"){
				 return L.marker(latlng,{icon: FLTraditionalIcon});
			}
	  }

	  if(feature.properties.resources == "FISHPORT"){		
	  return L.marker(latlng,{icon: porticon});
	  }

	  if(feature.properties.resources == "fishpen"){		
	  return L.marker(latlng,{icon: penicon});
	  }

	  if(feature.properties.resources == "fishcage"){		
	  return L.marker(latlng,{icon: cageicon});
	  }
	  if(feature.properties.resources == "fishpond"){		
		  return L.marker(latlng,{icon: pondicon});
		  }

	  if(feature.properties.resources == "coldstorage"){	
		  
		  if(feature.properties.Operator === "BFAR"){
			  return L.marker(latlng,{icon: ipcsbfaricon}); 
		  }
		  if(feature.properties.Operator === "COOPERATIVE"){
			  return L.marker(latlng,{icon: ipcscoopicon}); 
		  }
		  if(feature.properties.Operator === "PRIVATEORGANIZATION"){
			  return L.marker(latlng,{icon: ipcsprivateicon}); 
		  }

	  }

	  if(feature.properties.resources == "market"){		
	  return L.marker(latlng,{icon: marketicon});
	  }
	  
	  if(feature.properties.resources == "schoolOfFisheries"){		
	  return L.marker(latlng,{icon: schoolicon});
	  }

	  if(feature.properties.resources == "fishcoral"){		
	  return L.marker(latlng,{icon: fishcoralsicon});
	  }

	  if(feature.properties.resources == "seagrass"){		
	  return L.marker(latlng,{icon: seagrassicon});
	  }

	  if(feature.properties.resources == "seaweeds"){
		  if(feature.properties.Type === "SEAWEEDLABORATORY"){
			  return L.marker(latlng,{icon: seaweedsLaboratoryicon});
			}
			if(feature.properties.Type === "SEAWEEDNURSERY"){
				 return L.marker(latlng,{icon: seaweedsNurseryicon});
			}
			if(feature.properties.Type === "SEAWEEDWAREHOUSE"){
				 return L.marker(latlng,{icon: seaweedsWarehouseicon});
			}
			if(feature.properties.Type === "SEAWEEDDRYER"){
				 return L.marker(latlng,{icon: seaweedsDryericon});
			}  
	 
	  }
	  
	  if(feature.properties.resources == "mangrove"){		
	  return L.marker(latlng,{icon: mangroveicon});
	  }

	  if(feature.properties.resources == "lgu"){		
	  return L.marker(latlng,{icon: lguicon});
	  }

	  if(feature.properties.resources == "mariculturezone"){	
		  if(feature.properties.Type === "BFAR"){
			  return L.marker(latlng,{icon: maricultureicon});
			}
			if(feature.properties.Type === "PRIVATE"){
				 return L.marker(latlng,{icon: mariculturePrivateicon});
				 }
			if(feature.properties.Type === "LGU"){
				 return L.marker(latlng,{icon: maricultureLGUicon});
			}
	 
	  }

	  if(feature.properties.resources == "trainingcenter"){		
	  return L.marker(latlng,{icon: trainingcentericon});
	  }
	  
	  if(feature.properties.resources == "hatchery"){	
		  if(feature.properties.Operator === "BFAR" && feature.properties.Legislative === "legislated"){
			  
				  return L.marker(latlng,{icon: iconComplete});
		  }		  
		  if(feature.properties.Operator === "BFAR" && feature.properties.Legislative === "nonLegislated"){
				  return L.marker(latlng,{icon: iconNComplete});
		  }
		  
		  if(feature.properties.Operator === "LGU"){
			  return L.marker(latlng,{icon: hatchLGU});
		  }
		  if(feature.properties.Operator === "PRIVATE"){
			  return L.marker(latlng,{icon: hatchPrivate});
		  }
		  
		  }
	  if(feature.properties.resources == "nationalcenter"){	

		  //return L.marker(latlng,{icon: ncentericon});
		 return L.marker(latlng, {icon: ncentericon}).bindTooltip(feature.properties.Name, {permanent: true, direction: feature.properties.Direction});
//		  return L.marker(latlng, {
//			    icon: L.divIcon({
//			    className: 'my-div-icon',
//			      iconSize:null,
//			      html: '<div><div class="map-label-content">'+feature.properties.Name+'</div><div class="map-label-arrow"></div></div>'
//			    })
//	  });
		  }
	  if(feature.properties.resources == "regionaloffice"){	

		  //return L.marker(latlng,{icon: rofficeicon});
		  return L.marker(latlng, {
			    icon: L.divIcon({
			      iconSize:[100,20],
			      html: "<img src=\"/static/images/pin/new/bfar-infrastructure_RegionalFisheriesOffice.png\" style=\"width: 20px; height: 20px;\">" + "<b>" +feature.properties.Region + "</b>"
			    })
	  });
		  }
	  if(feature.properties.resources == "tos"){	

		  return L.marker(latlng,{icon: toscon});
		  }
	  if(feature.properties.resources == "payao"){	

		  return L.marker(latlng,{icon: payaoicon});
		  }

}



function feature(feature, layer){
	console.log(region_id);
	console.log(feature.properties.Region);
	console.log(region_id == feature.properties.Region);
	if(region_id == feature.properties.Region){
		
		if(feature.properties.resources == "fishsanctuaries"){
		//  console.log(feature.properties.Image);
		 FS.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
		  
	  }
	 if(feature.properties.resources == "fishprocessingplants"){
		 if(feature.properties.Operator === "BFAR_MANAGED"){
			 FP_BFAR.addLayer(layer.bindPopup(feature.properties.Information)); 
		 }
		 if(feature.properties.Operator === "LGU_MANAGED"){
			 FP_LGU.addLayer(layer.bindPopup( feature.properties.Information)); 
		 }
		 if(feature.properties.Operator === "COOPERATIVE"){
			 FP_PFDA.addLayer(layer.bindPopup( feature.properties.Information)); 
		 }
		 if(feature.properties.Operator === "PRIVATE_SECTOR"){
			 FP_PRIVATE.addLayer(layer.bindPopup( feature.properties.Information)); 
		 }
	  }
	if(feature.properties.resources == "fishlanding"){
		
		if(feature.properties.CFLCSTATUS === "Operational"){
			FL_operational.addLayer(layer.bindPopup( feature.properties.Information));
		}
		if(feature.properties.CFLCSTATUS === "ForOperation"){
			FL_for_operation.addLayer(layer.bindPopup(feature.properties.Information));
		}
		if(feature.properties.CFLCSTATUS === "ForCompletion"){
			FL_for_completion.addLayer(layer.bindPopup(feature.properties.Information));
		}
		if(feature.properties.CFLCSTATUS === "ForTransfer"){
			FL_for_transfer.addLayer(layer.bindPopup(feature.properties.Information));
		}
		if(feature.properties.CFLCSTATUS === "Damaged"){
			FL_damaged.addLayer(layer.bindPopup(feature.properties.Information));
		}
		if(feature.properties.Type === "Non-Traditionanal"){
			FL_noncflc.addLayer(layer.bindPopup(feature.properties.Information));
		}
		if(feature.properties.Type === "Traditional"){
			FL_traditional.addLayer(layer.bindPopup(feature.properties.Information));
		}
	  }
	if(feature.properties.resources == "FISHPORT"){
		 FPT.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "fishpen"){
		 FPEN.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "fishcage"){
		 FCage.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "coldstorage"){
		 IPCS.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "market"){
		 Market.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "schoolOfFisheries"){
		 School.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "fishcoral"){
		 Fishcorals.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "seagrass"){
		 Seagrass.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "seaweeds"){
		console.log("TYPE: " + feature.properties.Type);
		if(feature.properties.Type === "SEAWEEDLABORATORY"){
			Seaweed_laboratory.addLayer(layer.bindPopup(feature.properties.Information));
		}
		if(feature.properties.Type === "SEAWEEDNURSERY"){
			Seaweed_nursery.addLayer(layer.bindPopup(feature.properties.Information));
		}
		if(feature.properties.Type === "SEAWEEDWAREHOUSE"){
			Seaweed_warehouse.addLayer(layer.bindPopup(feature.properties.Information));
		}
		if(feature.properties.Type === "SEAWEEDDRYER"){
			Seaweed_dryer.addLayer(layer.bindPopup(feature.properties.Information));
		}

		 Seaweeds.addLayer(layer.bindPopup( feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "mangrove"){
		 Mangrove.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "lgu"){
		 LGU.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "mariculturezone"){
		 Mariculture.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "trainingcenter"){
		 Trainingcenter.addLayer(layer.bindPopup( feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "hatchery"){

			if(feature.properties.Legislative == "legislated"){
				console.log(feature.properties.Legislative);
				Hatchery.addLayer(layer.bindPopup(feature.properties.Information));
			}
			if(feature.properties.Legislative == "nonLegislated"){
				HatcheryinComplete.addLayer(layer.bindPopup(feature.properties.Information));
			}

			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	
	if(feature.properties.resources == "nationalcenter"){
	
		NCENTER.addLayer(layer.bindPopup( feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "regionaloffice"){
			ROFFICE.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
	if(feature.properties.resources == "tos"){
		TOS.addLayer(layer.bindPopup(feature.properties.Information));
		//layer.getPopup().on('remove',clickZoomOut);
		//console.log("fishprocessingplants" + feature.properties.Region);
  }
	
	}

//	if(region_id == feature.properties.Region){
//		  if(feature.properties.resources == "fishsanctuaries"){
//			 FS.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH SANCTUARY' + '</p>'));
//				layer.getPopup().on('remove',clickZoomOut);
//
//		  }
//	   }
//	  if(region_id == feature.properties.Region){
//		  if(feature.properties.resources == "fishprocessingplants"){
//			 FP.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH PROCESSING' + '</p>'));
//				layer.getPopup().on('remove',clickZoomOut);
//				
//		  }
//	   }
//	 if(region_id == feature.properties.Region){
//		  if(feature.properties.resources == "fishlanding"){
//			 FL.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH LANDING' + '</p>'));
//				layer.getPopup().on('remove',clickZoomOut);
//				
//		  }
//	   }
//	if(region_id == feature.properties.Region){
//	if(feature.properties.resources == "fishpen"){
//		 FPEN.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH PEN' + '</p>'));
//			layer.getPopup().on('remove',clickZoomOut);
//			//console.log("fishprocessingplants" + feature.properties.Region);
//	  }
//	}
//	if(region_id == feature.properties.Region){
//	if(feature.properties.resources == "fishcage"){
//		 FCage.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH CAGE' + '</p>'));
//			layer.getPopup().on('remove',clickZoomOut);
//			//console.log("fishprocessingplants" + feature.properties.Region);
//	  }
//	if(feature.properties.resources == "coldstorage"){
//		 IPCS.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: COLD STORAGE' + '</p>'));
//			layer.getPopup().on('remove',clickZoomOut);
//			//console.log("fishprocessingplants" + feature.properties.Region);
//	  }
//	}
//	if(region_id == feature.properties.Region){
//	if(feature.properties.resources == "market"){
//		 Market.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: MARKET' + '</p>'));
//			layer.getPopup().on('remove',clickZoomOut);
//			//console.log("fishprocessingplants" + feature.properties.Region);
//	  }
//	}
//	if(region_id == feature.properties.Region){
//	if(feature.properties.resources == "schoolOfFisheries"){
//		 School.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: SCHOOL OF FISHERIES' + '</p>'));
//			layer.getPopup().on('remove',clickZoomOut);
//			//console.log("fishprocessingplants" + feature.properties.Region);
//	  }
//	}
//	if(region_id == feature.properties.Region){
//	if(feature.properties.resources == "fishcoral"){
//		 Fishcorals.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: FISH CORALS' + '</p>'));
//			layer.getPopup().on('remove',clickZoomOut);
//			//console.log("fishprocessingplants" + feature.properties.Region);
//	  }
//	}
//	if(region_id == feature.properties.Region){
//	if(feature.properties.resources == "seagrass"){
//		 Seagrass.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: SEAGRASS' + '</p>'));
//			layer.getPopup().on('remove',clickZoomOut);
//			//console.log("fishprocessingplants" + feature.properties.Region);
//	  }
//	}
//	if(region_id == feature.properties.Region){
//	if(feature.properties.resources == "seaweeds"){
//		 Seaweeds.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: SEAWEEDS' + '</p>'));
//			layer.getPopup().on('remove',clickZoomOut);
//			//console.log("fishprocessingplants" + feature.properties.Region);
//	  }
//	}
//	if(region_id == feature.properties.Region){
//
//	if(feature.properties.resources == "mangrove"){
//		 Mangrove.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: MANGROVE' + '</p>'));
//			layer.getPopup().on('remove',clickZoomOut);
//			//console.log("fishprocessingplants" + feature.properties.Region);
//	  }
//	}
//	if(region_id == feature.properties.Region){
//	if(feature.properties.resources == "lgu"){
//		 LGU.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: PFO' + '</p>'));
//			layer.getPopup().on('remove',clickZoomOut);
//			//console.log("fishprocessingplants" + feature.properties.Region);
//	  }
//	}
//	if(region_id == feature.properties.Region){
//	if(feature.properties.resources == "mariculturezone"){
//		 Mariculture.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: MARICULTURE ZONE' + '</p>'));
//			layer.getPopup().on('remove',clickZoomOut);
//			//console.log("fishprocessingplants" + feature.properties.Region);
//	  }
//	}
//	if(region_id == feature.properties.Region){
//	if(feature.properties.resources == "trainingcenter"){
//		 Trainingcenter.addLayer(layer.bindPopup('<p>' +'Location: '+feature.properties.Location+'<br>Resources: TRAINING CENTER' + '</p>'));
//			layer.getPopup().on('remove',clickZoomOut);
//			//console.log("fishprocessingplants" + feature.properties.Region);
//	  }
//	}
}

function featuresByProvince(feature, layer){
		
		if(province_id == feature.properties.Province){		  
			
			if(feature.properties.resources == "fishsanctuaries"){
			//  console.log(feature.properties.Image);
			 FS.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
			  
		  }
		 if(feature.properties.resources == "fishprocessingplants"){
			 if(feature.properties.Operator === "BFAR_MANAGED"){
				 FP_BFAR.addLayer(layer.bindPopup(feature.properties.Information)); 
			 }
			 if(feature.properties.Operator === "LGU_MANAGED"){
				 FP_LGU.addLayer(layer.bindPopup( feature.properties.Information)); 
			 }
			 if(feature.properties.Operator === "COOPERATIVE"){
				 FP_PFDA.addLayer(layer.bindPopup( feature.properties.Information)); 
			 }
			 if(feature.properties.Operator === "PRIVATE_SECTOR"){
				 FP_PRIVATE.addLayer(layer.bindPopup( feature.properties.Information)); 
			 }
		  }
		if(feature.properties.resources == "fishlanding"){
			
			if(feature.properties.CFLCSTATUS === "Operational"){
				FL_operational.addLayer(layer.bindPopup( feature.properties.Information));
			}
			if(feature.properties.CFLCSTATUS === "For Operation"){
				FL_for_operation.addLayer(layer.bindPopup(feature.properties.Information));
			}
			if(feature.properties.CFLCSTATUS === "For Completion"){
				FL_for_completion.addLayer(layer.bindPopup(feature.properties.Information));
			}
			if(feature.properties.CFLCSTATUS === "For Transfer"){
				FL_for_transfer.addLayer(layer.bindPopup(feature.properties.Information));
			}
			if(feature.properties.CFLCSTATUS === "Damaged"){
				FL_damaged.addLayer(layer.bindPopup(feature.properties.Information));
			}
			if(feature.properties.Type === "Non-CFLC"){
				FL_noncflc.addLayer(layer.bindPopup(feature.properties.Information));
			}
			if(feature.properties.Type === "Traditional"){
				FL_traditional.addLayer(layer.bindPopup(feature.properties.Information));
			}
		  }
		if(feature.properties.resources == "FISHPORT"){
			 FPT.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "fishpen"){
			 FPEN.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "fishcage"){
			 FCage.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "coldstorage"){
			 IPCS.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "market"){
			 Market.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "schoolOfFisheries"){
			 School.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "fishcoral"){
			 Fishcorals.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "seagrass"){
			 Seagrass.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "seaweeds"){
			if(feature.properties.Type === "SEAWEEDLABORATORY"){
				Seaweed_laboratory.addLayer(layer.bindPopup(feature.properties.Information));
			}
			if(feature.properties.Type === "SEAWEEDNURSERY"){
				Seaweed_nursery.addLayer(layer.bindPopup(feature.properties.Information));
			}
			if(feature.properties.Type === "SEAWEEDWAREHOUSE"){
				Seaweed_warehouse.addLayer(layer.bindPopup(feature.properties.Information));
			}
			if(feature.properties.Type === "SEAWEEDDRYER"){
				Seaweed_dryer.addLayer(layer.bindPopup(feature.properties.Information));
			}

			 Seaweeds.addLayer(layer.bindPopup( feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "mangrove"){
			 Mangrove.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "lgu"){
			 LGU.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "mariculturezone"){
			 Mariculture.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "trainingcenter"){
			 Trainingcenter.addLayer(layer.bindPopup( feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "hatchery"){

				if(feature.properties.Legislative == "legislated"){
					console.log(feature.properties.Legislative);
					Hatchery.addLayer(layer.bindPopup(feature.properties.Information));
				}
				if(feature.properties.Legislative == "nonLegislated"){
					HatcheryinComplete.addLayer(layer.bindPopup(feature.properties.Information));
				}

				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		
		if(feature.properties.resources == "nationalcenter"){
		
			NCENTER.addLayer(layer.bindPopup( feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "regionaloffice"){
				ROFFICE.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
				//console.log("fishprocessingplants" + feature.properties.Region);
		  }
		if(feature.properties.resources == "tos"){
			TOS.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
	  }
		}
	
}

function featuresByResources(feature, layer){

	   
		  if(feature.properties.resources == "fishsanctuaries"){
			//  console.log(feature.properties.Image);
			 FS.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			  
		  }
		 if(feature.properties.resources == "fishprocessingplants"){
			 if(feature.properties.Operator === "BFAR_MANAGED"){
				 FP_BFAR.addLayer(layer.bindPopup(feature.properties.Information));
				// FP_BFAR.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
				//layer.getPopup().on('remove',clickZoomOut); 
			 }
			 if(feature.properties.Operator === "LGU_MANAGED"){
				 FP_LGU.addLayer(layer.bindPopup( feature.properties.Information));
					//layer.getPopup().on('remove',clickZoomOut);
			 }

			 if(feature.properties.Operator === "PRIVATE_SECTOR"){
				 FP_PRIVATE.addLayer(layer.bindPopup( feature.properties.Information));
					//layer.getPopup().on('remove',clickZoomOut); 
			 }
			 if(feature.properties.Operator === "COOPERATIVE"){
				 FP_COOPERATIVE.addLayer(layer.bindPopup( feature.properties.Information));
					//layer.getPopup().on('remove',clickZoomOut); 
			 }
		  }
		if(feature.properties.resources == "fishlanding"){
			
			if(feature.properties.CFLCSTATUS === "Operational"){
				FL_operational.addLayer(layer.bindPopup( feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
			}
			else if(feature.properties.CFLCSTATUS === "ForOperation"){
				
				FL_for_operation.addLayer(layer.bindPopup(feature.properties.Information));
				//.getPopup().on('remove',clickZoomOut);
			}else if(feature.properties.CFLCSTATUS === "ForCompletion"){
				FL_for_completion.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
			}else if(feature.properties.CFLCSTATUS === "ForTransfer"){
				FL_for_transfer.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
			}else if(feature.properties.CFLCSTATUS === "Damaged"){
				FL_damaged.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
			}else if(feature.properties.Type === "Non-Traditional"){
				FL_noncflc.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
			}else if(feature.properties.Type === "Traditional"){
				FL_traditional.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
			}
		  }
		if(feature.properties.resources == "FISHPORT"){
			 FPT.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "fishpen"){
			 FPEN.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "fishcage"){
			 FCage.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "fishpond"){
			 FPond.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "coldstorage"){
			if(feature.properties.Operator === "BFAR"){
					IPCS_PFDA.addLayer(layer.bindPopup(feature.properties.Information));
					//layer.getPopup().on('remove',clickZoomOut);
				 }
				if(feature.properties.Operator === "COOPERATIVE"){
					IPCS_COOPERATIVE.addLayer(layer.bindPopup(feature.properties.Information));
					//layer.getPopup().on('remove',clickZoomOut);
				 }
				if(feature.properties.Operator === "PRIVATEORGANIZATION"){
					IPCS_PRIVATEORGANIZATION.addLayer(layer.bindPopup(feature.properties.Information));
					//layer.getPopup().on('remove',clickZoomOut); 
				 }
			// IPCS.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
			//	layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "market"){
			 Market.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "schoolOfFisheries"){
			 School.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "fishcoral"){
			 Fishcorals.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "seagrass"){
			 Seagrass.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "seaweeds"){
			if(feature.properties.Type === "SEAWEEDLABORATORY"){
				Seaweed_laboratory.addLayer(layer.bindPopup(feature.properties.Information));
			}
			if(feature.properties.Type === "SEAWEEDNURSERY"){
				Seaweed_nursery.addLayer(layer.bindPopup(feature.properties.Information));
			}
			if(feature.properties.Type === "SEAWEEDWAREHOUSE"){
				Seaweed_warehouse.addLayer(layer.bindPopup(feature.properties.Information));
			}
			if(feature.properties.Type === "SEAWEEDDRYER"){
				Seaweed_dryer.addLayer(layer.bindPopup(feature.properties.Information));
			}

			 Seaweeds.addLayer(layer.bindPopup( feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "mangrove"){
			 Mangrove.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "lgu"){
			 LGU.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "mariculturezone"){
			if(feature.properties.Type === "BFAR"){
				Mariculture_BFAR.addLayer(layer.bindPopup(feature.properties.Information));
			}
			if(feature.properties.Type === "PRIVATE"){
				Mariculture_PRIVATE.addLayer(layer.bindPopup(feature.properties.Information));
			}
			if(feature.properties.Type === "LGU"){
				Mariculture_LGU.addLayer(layer.bindPopup(feature.properties.Information));
			}
//			 Mariculture.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
//				layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "trainingcenter"){
			 Trainingcenter.addLayer(layer.bindPopup( feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "hatchery"){

				if(feature.properties.Operator === "BFAR" && feature.properties.Legislative === "legislated"){
					Hatchery.addLayer(layer.bindPopup(feature.properties.Information));
					//layer.getPopup().on('remove',clickZoomOut);
				}
				if(feature.properties.Operator === "BFAR" &&  feature.properties.Legislative === "nonLegislated"){
					HatcheryinComplete.addLayer(layer.bindPopup(feature.properties.Information));
					//layer.getPopup().on('remove',clickZoomOut);
				}
				if(feature.properties.Operator === "LGU"){
					Hatchery_LGU.addLayer(layer.bindPopup(feature.properties.Information));
					//layer.getPopup().on('remove',clickZoomOut);
				}
				if(feature.properties.Operator === "PRIVATE"){
					Hatchery_PRIVATE.addLayer(layer.bindPopup(feature.properties.Information));
					//layer.getPopup().on('remove',clickZoomOut);
				}

		  }
		
		if(feature.properties.resources == "nationalcenter"){
		
			NCENTER.addLayer(layer.bindPopup( feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "regionaloffice"){
				ROFFICE.addLayer(layer.bindPopup(feature.properties.Information));
				//layer.getPopup().on('remove',clickZoomOut);
		  }
		if(feature.properties.resources == "tos"){
			
			TOS.addLayer(layer.bindPopup(feature.properties.Information));
			//layer.getPopup().on('remove',clickZoomOut);
			//layer.getPopup().on('remove',clickZoomOut);
			//console.log("fishprocessingplants" + feature.properties.Region);
		}
		
//		if(feature.properties.resources == "payao"){
//			PAYAO.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
//			layer.getPopup().on('remove',clickZoomOut);
//		}
		
	  
}

function payaoResources(feature, layer){
	if(feature.properties.resources == "payao"){
		console.log("PAYAO: " + feature.properties.Information);
		PAYAO.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		layer.getPopup().on('remove',clickZoomOut);
		//layer.getPopup().on('remove',clickZoomOut);
		//console.log("fishprocessingplants" + feature.properties.Region);
  }
}
function penResources(feature, layer){
	if(feature.properties.resources == "fishpen"){

		FPEN.addLayer(layer.bindPopup(feature.properties.Information).on('click', clickZoom));
		layer.getPopup().on('remove',clickZoomOut);
		//layer.getPopup().on('remove',clickZoomOut);
		//console.log("fishprocessingplants" + feature.properties.Region);
  }
}


function getPAYAOBYRegion(){
	

	map.spin(true);
	  $.getJSON("/create/getPAYAOGeoJsonByRegion",function(data){
		
		  setTimeout(function () {
		  L.geoJSON(data, {
			  pointToLayer: payaoToLayer,
			  	onEachFeature: payaoResources
		})
map.spin(false);
}, 3000);
});

	}


