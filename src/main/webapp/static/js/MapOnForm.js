
//var center = ${set_center}; 
//var map_data = ${mapsData};	
//console.log("map_data: " + map_data);
var map =L.map('map',{scrollWheelZoom:false}).setView([16.913544, 120.879809],6);

  var OpenTopoMap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)',
    opacity: 0.90
  });
  
 // OpenTopoMap.addTo(map);

	map.zoomControl.setPosition('topright');
	var osm=new L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png',{ 
				maxZoom: 18,
				attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'});
	
	
	

	//osm.addTo(map);
	// https: also suppported.
	 var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
	});
	 Esri_WorldImagery.addTo(map);
	 
	// https: also suppported.
	var Esri_WorldGrayCanvas = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {		
		attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'});
		
	var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {	
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
	});
	
	
/* 	var wmsLayer = L.tileLayer.wms('http://geo.bfar.da.gov.ph/gwc/service/wms', {
    layers: 'region:Region_I'
	});
	 */
	
L.control.scale().addTo(map);

function style(feature) {
    return {
        fillColor: 'green', 
        fillOpacity: 0.5,  
        weight: 2,
        opacity: 1,
        color: '#ffffff',
        dashArray: '3'
    };
}

	var highlight = {
		'fillColor': 'yellow',
		'weight': 2,
		'opacity': 1
	};


		
  		function polySelect(a){
  		console.log("polySelect(a): " + a);
			map._layers[a].fire('click'); 
			var layer = map._layers[a];
			map.fitBounds(layer.getBounds());
        }
  //    var geojsonLayer = new L.GeoJSON.AJAX(map_data);

     
//geojsonLayer.addTo(map);       
 var fishSancturiesGroup = [];

			$(".se-pre-con").show();
			
			var LamMarkerList = [];
			var group;
			var layerGroup;

	        	  function clickZoom(e) {
	                   map.setView(e.target.getLatLng(),17);
	               }
	        	  function clickZoomOut(e) {
	                   map.setView(e.target.getLatLng(),6);
	               }
       
			var LeafIconMarker = L.Icon.extend({
				 options: {
						iconSize:     [30, 48]
						}
				});
		    
	        var geojsonMarkerOptions = {
					radius: 8,
					fillColor: "#ff7800",
					color: "#000",
					weight: 1,
					opacity: 1,
					fillOpacity: 0.8
					};
	        
	        var HatcheryComplete = new L.LayerGroup();
	        var HatcheryinComplete = new L.LayerGroup();
	        var iconComplete = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/hatcheries.png"});
	        var iconNComplete = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/mpa.png"});
		       		$.getJSON("/static/geojson/StatusofLegislatedHatcheries.geojson",function(data){
		       			console.log("/static/geojson/StatusofLegislatedHatcheries.geojson" + data);
		       		L.geoJSON(data, {
					pointToLayer: function (feature, latlng) {
						if(feature.properties.Status == "Completed"){
							return L.marker(latlng,{icon: iconComplete});
						}else{
							return L.marker(latlng,{icon: iconNComplete});
						}
    				
    				},
						onEachFeature: function (feature, layer) {
							if(feature.properties.Status == "Completed"){
							HatcheryComplete.addLayer(layer.bindPopup('<p>Location:'+feature.properties.Location+'<br>RANo: '+feature.properties.RANo+'<br>Status: '+feature.properties.Status+'<br>Coordinates: '+feature.geometry.coordinates+'</p>').on('click', clickZoom));
							layer.getPopup().on('remove',clickZoomOut);
							}else{
							HatcheryinComplete.addLayer(layer.bindPopup('<p>Location:'+feature.properties.Location+'<br>RANo: '+feature.properties.RANo+'<br>Status: '+feature.properties.Status+ '<br>Coordinates: '+feature.geometry.coordinates+'</p>').on('click', clickZoom));
							layer.getPopup().on('remove',clickZoomOut);	
							}
							}
				})//.addTo(map);
		       		
		       	}); 
		       	 var fsicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishsanctuary.png"});
		       		var FS = new L.LayerGroup();
		       		
		       		$.getJSON("/home/getFSGeoJson",function(data){
		       			
		       			L.geoJSON(data, {
		       				
							pointToLayer: function (feature, latlng) {	
								
									return L.marker(latlng,{icon: fsicon});
										    				
		    				},
								onEachFeature: function (feature, layer) {
									FS.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: Fish Sanctuary' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
									layer.getPopup().on('remove',clickZoomOut);
									}
						})
		       		}); 
		       		FS.addTo(map);
		       		
		       	 var fpicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishprocessing.png"});
		    		var FP = new L.LayerGroup();
		    		
		       		$.getJSON("/home/getFPGeoJson",function(data){
		       			
		       			L.geoJSON(data, {
		       				
							pointToLayer: function (feature, latlng) {	
								
									return L.marker(latlng,{icon: fpicon});
										    				
		    				},
								onEachFeature: function (feature, layer) {
									FP.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: Fish Processing' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
									layer.getPopup().on('remove',clickZoomOut);
									}
						})
		       		});
		       		
		       	 var flicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fish-landing.png"});
		    		var FL = new L.LayerGroup();
		    		
		       		$.getJSON("/home/getFLGeoJson",function(data){
		       			
		       			L.geoJSON(data, {
		       				
							pointToLayer: function (feature, latlng) {	
								
									return L.marker(latlng,{icon: flicon});
										    				
		    				},
								onEachFeature: function (feature, layer) {
									FL.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: Fish Landing' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
									layer.getPopup().on('remove',clickZoomOut);
									}
						})
		       		});
		       		
		       		
			       	 var fpticon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishport.png"});
			    		var FPT = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getFPTGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: fpticon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										FPT.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: Fish Port' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var fpenicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishpen.png"});
			    		var FPEN = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getFPenGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: fpenicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										FPEN.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: Fish Pen' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var fcageicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishcage.png"});
			    		var FCage = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getFCageGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: fcageicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										FCage.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: Fish Cages' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var ipcsicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/iceplant.png"});
			    		var IPCS = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getIPCSGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: ipcsicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										IPCS.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: Cold Storage' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var marketicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/market.png"});
			    		var Market = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getMarketGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: marketicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										Market.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: Market' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var schoolicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/schooloffisheries.png"});
			    		var School = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getSchoolGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: schoolicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										School.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: School of Fisheries' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		var fishcoralsicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fishcorral.png"});
			    		var Fishcorals = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getFishCoralsGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: fishcoralsicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										Fishcorals.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: Fish Corals' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var seagrassicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/seagrass.png"});
			    		var Seagrass = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getSeagrassGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: seagrassicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										Seagrass.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: Seagrass' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var seaweedsicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/seaweeds.png"});
			    		var Seaweeds = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getSeaweedsGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: seaweedsicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										Seaweeds.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: Seaweeds' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		var mangroveicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/mangrove.png"});
			    		var Mangrove = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getMangroveGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: mangroveicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										Mangrove.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: Mangrove' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		var lguicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/lgu.png"});
			    		var LGU = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getLGUGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: lguicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										LGU.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: PFO' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var maricultureicon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/mariculture.png"});
			    		var Mariculture = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getMaricultureGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: maricultureicon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										Mariculture.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: Mariculture Zone' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
			       		
			       		var trainingcentericon = new LeafIconMarker({iconUrl:"/static/images/pin/20x34/fisheriestraining.png"});
			    		var Trainingcenter = new L.LayerGroup();
			    		
			       		$.getJSON("/home/getTrainingcenterGeoJson",function(data){
			       			
			       			L.geoJSON(data, {
			       				
								pointToLayer: function (feature, latlng) {	
									
										return L.marker(latlng,{icon: trainingcentericon});
											    				
			    				},
									onEachFeature: function (feature, layer) {
										Trainingcenter.addLayer(layer.bindPopup('<p>' + '<img src=' +feature.properties.Image+'style="width: 100px; height: 100px;"/>'+'<br>Location: '+feature.properties.Location+'<br>Name: '+feature.properties.Name+'<br>Resources: Training Center' + '<br>Coordinates: '+feature.properties.Latitude+','+feature.properties.Longitude+'</p>').on('click', clickZoom));
										layer.getPopup().on('remove',clickZoomOut);
										}
							})
			       		});
		
			       		fetch("/static/geojson/Ilocos.geojson").then(res => res.json()).then(data => {
			       		    // add GeoJSON layer to the map once the file is loaded
			       			geoJSON = L.geoJson(data).addTo(map);
			       		  });

        
var baseMaps = {
    /* "Open Street Map": osm,
   	"OSM B&W":OpenStreetMap_BlackAndWhite,
   	"Esri WorldI magery":Esri_WorldImagery,
   	"Esri World Gray Canvas":Esri_WorldGrayCanvas, */
		
//    "<img src='/static/images/iconCircle/hatcheries.png' style='width: 25px; height: 25px;'/>  Hatchery Complete":HatcheryComplete,
//    "<img src='/static/images/iconCircle/hatcheries.png' style='width: 25px; height: 25px;'/> Hatchery Incomplete ":HatcheryinComplete,
//    "<img src='/static/images/iconCircle/fishsanctuary.png' style='width: 25px; height: 25px;'/> Fish Sanctuary":FS,
//    "<img src='/static/images/iconCircle/fishprocessing.png' style='width: 25px; height: 25px;'/> Fish Processing":FP,
//    "<img src='/static/images/iconCircle/fish-landing.png' style='width: 25px; height: 25px;'/> Fish Landing":FL,
//    "<img src='/static/images/iconCircle/fishport.png' style='width: 25px; height: 25px;'/> Fish Port":FPT,
//    "<img src='/static/images/iconCircle/fishpen.png' style='width: 25px; height: 25px;'/> Fish Pen":FPEN,
//    "<img src='/static/images/iconCircle/fishcage.png' style='width: 25px; height: 25px;'/> Fish Cage":FCage,
//    "<img src='/static/images/iconCircle/iceplant.png' style='width: 25px; height: 25px;'/> Cold Storage":IPCS,
//    "<img src='/static/images/iconCircle/market.png' style='width: 25px; height: 25px;'/> Market":Market,
//    "<img src='/static/images/iconCircle/schoolof-fisheries.png' style='width: 25px; height: 25px;'/> School of Fisheries":School,
//    "<img src='/static/images/iconCircle/fishcorral.png' style='width: 25px; height: 25px;'/> Fish Corals":Fishcorals,
//    "<img src='/static/images/iconCircle/seagrass.png' style='width: 25px; height: 25px;'/> Sea Grass":Seagrass,
//    "<img src='/static/images/iconCircle/seaweeds.png' style='width: 25px; height: 25px;'/> Sea Weeds":Seaweeds,
//    "<img src='/static/images/iconCircle/mangrove.png' style='width: 25px; height: 25px;'/> Mangrove":Mangrove,
//    "<img src='/static/images/iconCircle/lgu.png' style='width: 25px; height: 25px;'/> LGU":LGU,
//    "<img src='/static/images/iconCircle/mariculturezone.png' style='width: 25px; height: 25px;'/> Mariculture":Mariculture,
//    "<img src='/static/images/iconCircle/fisheriestraining.png' style='width: 25px; height: 25px;'/> Trainingcenter":Trainingcenter
};


L.control.layers(null,baseMaps,{collapsed:false}).addTo(map);
//L.control.layers(baseMaps).addTo(map);	
		var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});

		/*  function getFisheriesData(){
			getAllResources(map_location);
		} */
 