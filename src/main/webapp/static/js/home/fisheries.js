

        var LamMarker;
        var fishSancturiesGroup = [];
        var lon
        var lat;
        var icon;
        var html;
	    var edit;
	    var greenIcon;
	    var latlngData;

        
        function setMarkers(data) {
        	
        	
        	  data.forEach(function (obj) {
        		  LamMarker = new L.marker([obj.lat,obj.lon],
       				//  {icon: obj.greenIcon}).addTo(map).bindPopup(obj.html + '<img class="imgicon" id="myImg" src="'+obj.image+'" style="width:300px;max-width:250px;height:250px"></img>');
        				//  {icon: obj.greenIcon}).addTo(map).bindPopup(obj.html + '<img class="imgicon" id="myImg" src="'+obj.image+'" style="width:300px;max-width:250px;height:250px"></img>' + "<br>").on('click', clickZoom);
        		  	//	  {icon: obj.greenIcon}).addTo(map).bindPopup(obj.html).on('click', clickZoom);		  
         		  {icon: obj.greenIcon}).addTo(map).bindPopup(obj.html);		  
          		
        		  //{icon: obj.greenIcon}).addTo(map).bindPopup(obj.edit).on('click', function(e) {
								//onMapClick(this.getLatLng());
						//legendData();
					//});
//        		  LamMarker = new L.marker([obj.lat,obj.lon],
//        				  {icon: obj.greenIcon}).addTo(map).bindPopup(obj.edit).on('click', markerOnClick);
				//	LamMarker.getPopup().on('remove', clickZoomOut);
					
					//allParks.addLayer(LamMarker);
        		  latlngData = obj.unique;
        	  });
        	}
     
        function getLatLng(){
        //	for ( var i = 0; i < LamMarker.length; i++) {
        		
        		console.log(LamMarker);
        		
        	//}
        }
        function delMarker(){
        	map.removeLayer(LamMarker);
        }
        function clickZoom(e) {
            map.setView(e.target.getLatLng(),17);
        }
        function clickZoomOut(e) {
     	   map.setView(e.target.getLatLng(),8);
     }

        function legendData(){
        	
    		
    		function getColor(d) {
    		    return d > 1000 ? '#800026' :
    		           d > 500  ? '#BD0026' :
    		           d > 200  ? '#E31A1C' :
    		           d > 100  ? '#FC4E2A' :
    		           d > 50   ? '#FD8D3C' :
    		           d > 20   ? '#FEB24C' :
    		           d > 10   ? '#FED976' :
    		                      '#FFEDA0';
    		}	
    		
    		var legend = L.control({position: 'bottomleft'});
    		legend.onAdd = function (map) {

    		var div = L.DomUtil.create('div', 'info legend');
    		labels = ['<strong>Categories</strong>'],
    		categories = ['Road Surface','Signage','Line Markings','Roadside Hazards','Other'];

    		for (var i = 0; i < categories.length; i++) {

    		        div.innerHTML += 
    		        labels.push(
    		            '<a href="/frgis/usermap/fishsanctuaries">' +
    		        (categories[i] ? categories[i] : '+') +'</a> ');
    					
    		    }
    		    div.innerHTML = labels.join('<br>');
    		return div;
    		};
    		legend.addTo(map);

        	
        }
        
        
        
        
        function getFishSanctuaries(map_location) {
     		
        	var LeafIcon = L.Icon.extend({
  				 options: {
  					 iconSize:     [30, 48]
  				 		}
			});
        				
        				for ( var i = 0; i < map_location.length; i++) {
        						var obj = map_location[i];
        						
        						fishSancturiesGroup.push({
        					        lon:obj.lon,
        					        lat:obj.lat,
        					        icon:obj.icon,
        					        html:obj.html,
        					     //   image:obj.image,
        						 //   edit:'<div class="fishsanctuaries_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
        							greenIcon:new LeafIcon({iconUrl: obj.icon})
        					    });
        			
        	    				$.get("/create/info/" +  obj.uniqueKey, function(data, status) {

        	    					for (var x = 0; x < data.length; x++) {
        	    						var obj = data[x];
        	    						var bfardenr = obj.bfardenr;
        	    						
        	    						if (bfardenr == 'BFAR') {
        	    							$('#bfarData').show();
        	    							$('#denrData').hide();
        	    						}
        	    						if (bfardenr == 'DENR') {
        	    							$('#bfarData').hide();
        	    							$('#denrData').show();
        	    						}

        	    					}
        	    					$('.form_input').show();
        	    					getFishSanctuaryInfoById(data, status);

        	    				});
        	    				document.querySelector('#submitFishSanctuaries').innerHTML = 'UPDATE'; 
        			//	}
        			}
        				
        				setMarkers(fishSancturiesGroup);
        		
        								$('#details').empty();
        								clearClickMark();
        								$(".se-pre-con").hide();

        	}
    	function getFishLanding(map_location) {

    		var LeafIcon = L.Icon.extend({
  				 options: {
       		iconSize:     [30, 48]
   		}
			});


    		$(".se-pre-con").show();
    		for (var i = 0; i < map_location.length; i++) {
    			var obj = map_location[i];
    	
    			//var _uniqueKey=null;
    			//if (obj.page == "fishlanding") {
    				fishSancturiesGroup.push({
    					
				        lon:obj.lon,
				        lat:obj.lat,
				        icon:obj.icon,
				        html:obj.html,
				        image:obj.image,
					   // edit:'<div class="fishlanding_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon:new LeafIcon({iconUrl: obj.icon})
   				
				    });
    				
    				$.get("/create/getFishLanding/" + obj.uniqueKey, function(data, status) {

    					$('.form_input').show();
    					getFishLandingInfoById(data, status);
    				});
    				 document.querySelector('#submitFishLanding').innerHTML = 'UPDATE'; 
    				

    			//}
    		}

    		setMarkers(fishSancturiesGroup);
    		$('#details').empty();
    		clearClickMark();
    		$(".se-pre-con").hide();

    		}
           
    	function getFishCage(map_location) {
    		
    		var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

    		$(".se-pre-con").show();
    		for (var i = 0; i < map_location.length; i++) {
    			var obj = map_location[i];
    			//if (obj.page == "fishcage") {
    				fishSancturiesGroup
    				.push({
    					lon : obj.lon,
    					lat : obj.lat,
    					icon : obj.icon,
    					html : obj.html,
    					image : obj.image,
    					edit : '<div class="fishcage_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
    					greenIcon : new LeafIcon({
    						iconUrl : obj.icon
    					})
    				});
    				  $.get("/create/getFishCage/" + obj.uniqueKey, function(data, status){
    					  $('.form_input').show();
    						getFishCageInfoById(data, status);
    			    		  });
    			  
    			  document.querySelector('#submitFishCage').innerHTML = 'UPDATE';
    			   
    				//}
    		}
    		
    		setMarkers(fishSancturiesGroup);
    		$('#details').empty();
    		clearClickMark();
    		$(".se-pre-con").hide();
    	}
    	
        
    	function getFishCorals(map_location) {

    		var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});
    		
    		$(".se-pre-con").show();
    		for (var i = 0; i < map_location.length; i++) {
    			var obj = map_location[i];
    			//if (obj.page == "fishcorals") {
    				fishSancturiesGroup
    				.push({
    					lon : obj.lon,
    					lat : obj.lat,
    					icon : obj.icon,
    					html : obj.html,
    					 image:obj.image,
    					edit : '<div class="fishcorals_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
    					greenIcon : new LeafIcon({
    						iconUrl : obj.icon
    					})
    				});
    				
    				$.get("/create/getFishCoral/" + obj.uniqueKey, function(data, status) {

    					$('.form_input').show();
    					getFishCoralInfoById(data, status);
    				});
    				 document.querySelector('#submitFishCoral').innerHTML = 'UPDATE'; 
    			//	}
    		}
    		
    		setMarkers(fishSancturiesGroup);
    		$('#details').empty();
    		clearClickMark();
    		$(".se-pre-con").hide();
    	}

    	function getHatchery(map_location) {
    		var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

    		$(".se-pre-con").show();
    		for (var i = 0; i < map_location.length; i++) {
    			var obj = map_location[i];
    		//	if (obj.page == "hatchery") {
    				fishSancturiesGroup
    				.push({
    					lon : obj.lon,
    					lat : obj.lat,
    					icon : obj.icon,
    					html : obj.html,
    					image:obj.image,
    					edit : '<div class="hatchery_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
    					greenIcon : new LeafIcon({
    						iconUrl : obj.icon
    					})
    				});
    				$.get("/create/getHatchery/" + obj.uniqueKey, function(data, status) {

    					$('.form_input').show();
    					getHatcheryInfoById(data, status);
    				});
    				 document.querySelector('#submitHatchery').innerHTML = 'UPDATE';
    				//}
    		}
    		
    		setMarkers(fishSancturiesGroup);
    		$('#details').empty();
    		clearClickMark();
    		$(".se-pre-con").hide();
    	}
    	function getFishPen(map_location) {
    		
    		var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

    		$(".se-pre-con").show();
    		for (var i = 0; i < map_location.length; i++) {
    			var obj = map_location[i];
    			//if (obj.page == "fishpen") {
    				fishSancturiesGroup
    				.push({
    					lon : obj.lon,
    					lat : obj.lat,
    					icon : obj.icon,
    					html : obj.html,
    					image:obj.image,
    					edit : '<div class="fishpen_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
    					greenIcon : new LeafIcon({
    						iconUrl : obj.icon
    					})
    				});
    				$.get("/create/getFishPen/" +obj.uniqueKey, function(data, status) {

    					$('.form_input').show();
    					getFishPenInfoById(data, status);
    				});
    				 document.querySelector('#submitFishPen').innerHTML = 'UPDATE'; 
    				//}
    		}
    		
    		setMarkers(fishSancturiesGroup);
    		$('#details').empty();
    		clearClickMark();
    		$(".se-pre-con").hide();
    	}
    	function getFishPond(map_location) {
    		
    		var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

    		$(".se-pre-con").show();
    		for (var i = 0; i < map_location.length; i++) {
    			var obj = map_location[i];
    			//if (obj.page == "fishpen") {
    				fishSancturiesGroup
    				.push({
    					lon : obj.lon,
    					lat : obj.lat,
    					icon : obj.icon,
    					html : obj.html,
    					image:obj.image,
    				//	edit : '<div class="fishpen_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
    					greenIcon : new LeafIcon({
    						iconUrl : obj.icon
    					})
    				});
    				  $.get("/create/getFishPond/" + obj.uniqueKey, function(data, status){
    				         for ( var x = 0; x < data.length; x++) {
    										var obj = data[x];
    										var fishPondType = obj.fishPondType;
    										var status = obj.status;
    					            		
    					            		if(status == 'Non-Active'){
    					            			$('#nonActiveData').show();
    					            		}else{
    					            			$('#nonActiveData').hide();
    					            		}
    					            		  
    					            	}
    				         	$('.form_input').show();
    							getFishPondInfoById(data, status);
    				    		  });
    				  document.querySelector('#submitFishPond').innerHTML = 'UPDATE'; 
    				//}
    		}
    		
    		setMarkers(fishSancturiesGroup);
    		$('#details').empty();
    		clearClickMark();
    		$(".se-pre-con").hide();
    	}

    	function getFishPort(map_location) {
    		
    		var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

    		$(".se-pre-con").show();
    		for (var i = 0; i < map_location.length; i++) {
    			var obj = map_location[i];
    			//if (obj.page == "FISHPORT") {
    				fishSancturiesGroup
    				.push({
    					lon : obj.lon,
    					lat : obj.lat,
    					icon : obj.icon,
    					html : obj.html,
    					image:obj.image,
    					edit : '<div class="FISHPORT_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
    					greenIcon : new LeafIcon({
    						iconUrl : obj.icon
    					})
    				});

    				$.get("/create/getFishPort/" + obj.uniqueKey, function(data, status) {


    					$('.form_input').show();
    					getFishPortInfoById(data, status);
    				});
    				document.querySelector('#submitFishport').innerHTML = 'UPDATE'; 
    				//}
    		}

    		setMarkers(fishSancturiesGroup);
    		$('#details').empty();
    		clearClickMark();
    		$(".se-pre-con").hide();
    	}


		function getFishProcessingPlants(map_location) {
			
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});
			$(".se-pre-con").show();
			
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				//if (obj.page === "fishprocessingplants") {

					fishSancturiesGroup
							.push({
								lon : obj.lon,
								lat : obj.lat,
								icon : obj.icon,
								html : obj.html,
								image:obj.image,
								edit : '<div class="fishprocessingplants_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
								greenIcon : new LeafIcon({
									iconUrl : obj.icon
								})
							});

					$.get("/create/getFishProcessingplants/" + obj.uniqueKey, function(data, status) {

						for (var x = 0; x < data.length; x++) {
							var obj = data[x];
							var plantRegistered = obj.processingEnvironmentClassification;

							if (plantRegistered == 'Plant') {
								$('#plantData').show();
							}
							if (plantRegistered == 'Backyard') {
								$('#plantData').hide();
							}

						}
						$('.form_input').show();
						getFishProcessingInfoById(data, status);
					});
					 document.querySelector('#submitFishProcessingPlant').innerHTML = 'UPDATE'; 
				//}
			}
			//15.452077, 129.349663
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();

		}

		function getIPCS(map_location) {
			
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

			$(".se-pre-con").show();
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				console.log(obj);
				//if (obj.page == "iceplantorcoldstorage") {
					fishSancturiesGroup
					.push({
						lon : obj.lon,
						lat : obj.lat,
						icon : obj.icon,
						html : obj.html,
						image:obj.image,
						uniqueKey:obj.uniqueKey,
						edit : '<div class="iceplantorcoldstorage_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon : new LeafIcon({
							iconUrl : obj.icon
						})
					});
					   $.get("/create/getIce/" + obj.uniqueKey, function(data, status){
						   getIceInfoById(data, status);
						   $('.form_input').show();
			    	});
			   document.querySelector('#submitIcePlantColdStorage').innerHTML = 'UPDATE';  
					//}
			}
			
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();
		}

		
		function getMangrove(map_location) {
			
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

			$(".se-pre-con").show();
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				//if (obj.page == "mangrove") {
					fishSancturiesGroup
					.push({
						lon : obj.lon,
						lat : obj.lat,
						icon : obj.icon,
						html : obj.html,
						image:obj.image,
						edit : '<div class="mangrove_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon : new LeafIcon({
							iconUrl : obj.icon
						})
					});
					$.get("/create/getMangrove/" + obj.uniqueKey, function(data, status) {

						$('.form_input').show();
						getMangroveInfoById(data, status);
					});
					 document.querySelector('#submitMangrove').innerHTML = 'UPDATE'; 
					//}
			}
			
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();
		}


		function getMaricultureZone(map_location) {
			
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

			$(".se-pre-con").show();
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				//if (obj.page == "mariculturezone") {
					fishSancturiesGroup
					.push({
						lon : obj.lon,
						lat : obj.lat,
						icon : obj.icon,
						html : obj.html,
						image:obj.image,
						edit : '<div class="mariculturezone_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon : new LeafIcon({
							iconUrl : obj.icon
						})
					});
					$.get("/create/getMaricultureZone/" + obj.uniqueKey, function(data, status) {

						$('.form_input').show();
						getMaricultureZoneInfoById(data, status);
					});
					 document.querySelector('#submitmariculture').innerHTML = 'UPDATE'; 
				

					//}
			}
			
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();
		}

		function getMarket(map_location) {
			
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

			$(".se-pre-con").show();
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				//if (obj.page == "market") {
					fishSancturiesGroup
					.push({
						lon : obj.lon,
						lat : obj.lat,
						icon : obj.icon,
						html : obj.html,
						image:obj.image,
						edit : '<div class="market_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon : new LeafIcon({
							iconUrl : obj.icon
						})
					});
					$.get("/create/getMarket/" + obj.uniqueKey, function(data, status) {

						$('.form_input').show();
						getMarketInfoById(data, status);
					});
					 document.querySelector('#submitMarket').innerHTML = 'UPDATE';
					//}
			}
			
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();
		}

		function getLGU(map_location) {
			
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

			$(".se-pre-con").show();
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				//if (obj.page == "lgu") {
					fishSancturiesGroup
					.push({
						lon : obj.lon,
						lat : obj.lat,
						icon : obj.icon,
						html : obj.html,
						image:obj.image,
						edit : '<div class="lgu_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon : new LeafIcon({
							iconUrl : obj.icon
						})
					});
					$.get("/create/getLGU/" + obj.uniqueKey, function(data, status) {

						$('.form_input').show();
						getLGUInfoById(data, status);
					});
					 document.querySelector('#submitLGU').innerHTML = 'UPDATE'; 
					//}
			}
			
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();
		}

		function getSchoolOfFisheries(map_location) {
			
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

			$(".se-pre-con").show();
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				//if (obj.page == "schooloffisheries") {
					fishSancturiesGroup
					.push({
						lon : obj.lon,
						lat : obj.lat,
						icon : obj.icon,
						html : obj.html,
						image:obj.image,
						edit : '<div class="schooloffisheries_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon : new LeafIcon({
							iconUrl : obj.icon
						})
					});
					$.get("/create/getSchool/" + obj.uniqueKey, function(data, status) {

						$('.form_input').show();
						getSchoolInfoById(data, status);
					});
					 document.querySelector('#submitSchoolOfFisheries').innerHTML = 'UPDATE'; 
					//}
			}
			
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();
		}

		
		function seaGrass(map_location) {

			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});
			
			$(".se-pre-con").show();
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				//if (obj.page == "seagrass") {
					fishSancturiesGroup
					.push({
						lon : obj.lon,
						lat : obj.lat,
						icon : obj.icon,
						html : obj.html,
						image:obj.image,
						edit : '<div class="seagrass_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon : new LeafIcon({
							iconUrl : obj.icon
						})
					});
					$.get("/create/getSeagrass/" + obj.uniqueKey, function(data, status) {

						$('.form_input').show();
						getSeagrassInfoById(data, status);
					});
					 document.querySelector('#submitSeaGrass').innerHTML = 'UPDATE'; 
					//}
			}
			
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();
		}

		
		function getSeaweeds(map_location) {
			
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

			$(".se-pre-con").show();
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				//if (obj.page == "seaweeds") {
					fishSancturiesGroup
					.push({
						lon : obj.lon,
						lat : obj.lat,
						icon : obj.icon,
						html : obj.html,
						image:obj.image,
						edit : '<div class="seaweeds_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon : new LeafIcon({
							iconUrl : obj.icon
						})
					});
					$.get("/create/getSeaweeds/" + obj.uniqueKey, function(data, status) {

						$('.form_input').show();
						getSeaweedsInfoById(data, status);
					});
					 document.querySelector('#submitSeaWeeds').innerHTML = 'UPDATE';
					///}
			}
			
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();
		}

		function getTrainingCenter(map_location) {
			
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

			$(".se-pre-con").show();
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				//if (obj.page == "trainingcenter") {
					fishSancturiesGroup
					.push({
						lon : obj.lon,
						lat : obj.lat,
						icon : obj.icon,
						html : obj.html,
						image:obj.image,
						edit : '<div class="trainingcenter_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon : new LeafIcon({
							iconUrl : obj.icon
						})
					});
					$.get("/create/getTraining/" + obj.uniqueKey, function(data, status) {

						$('.form_input').show();
						getTrainingInfoById(data, status);
					});
					 document.querySelector('#submitTrainingCenter').innerHTML = 'UPDATE'; 
				//	}
			}
			
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();
		}
		function getNationalCenter(map_location) {
			
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

			$(".se-pre-con").show();
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				//if (obj.page == "trainingcenter") {
					fishSancturiesGroup
					.push({
						lon : obj.lon,
						lat : obj.lat,
						icon : obj.icon,
						html : obj.html,
						image:obj.image,
						//edit : '<div class="trainingcenter_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon : new LeafIcon({
							iconUrl : obj.icon
						})
					});

					$.get("/create/getNationalCenter/" + obj.uniqueKey, function(data, status) {

						$('.form_input').show();
						getNationalCenterInfoById(data, status);
					});
					 document.querySelector('#submitNationalCenter').innerHTML = 'UPDATE'; 
				//	}
			}
			
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();
		}
		function getPayao(map_location) {
			
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

			$(".se-pre-con").show();
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				//if (obj.page == "trainingcenter") {
					fishSancturiesGroup
					.push({
						lon : obj.lon,
						lat : obj.lat,
						icon : obj.icon,
						html : obj.html,
						image:obj.image,
						//edit : '<div class="trainingcenter_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon : new LeafIcon({
							iconUrl : obj.icon
						})
					});
					$.get("/create/getPayao/" + obj.uniqueKey, function(data, status) {
					
							$('.form_input').show();
						getFishCageInfoById(data, status);
						});
						 document.querySelector('#submitPayao').innerHTML = 'UPDATE'; 
				//	}
			}
			
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();
		}
		function getTOS(map_location) {
			
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

			$(".se-pre-con").show();
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				//if (obj.page == "trainingcenter") {
					fishSancturiesGroup
					.push({
						lon : obj.lon,
						lat : obj.lat,
						icon : obj.icon,
						html : obj.html,
						image:obj.image,
						//edit : '<div class="trainingcenter_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon : new LeafIcon({
							iconUrl : obj.icon
						})
					});
					$.get("/create/getTOS/" + obj.uniqueKey, function(data, status) {
					
						$('.form_input').show();
							getRegionalOfficeInfoById(data, status);
						});
						 document.querySelector('#submitTOS').innerHTML = 'UPDATE';
				//	}
			}
			
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();
		}
		function getRegionalOffice(map_location) {
			
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

			$(".se-pre-con").show();
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				//if (obj.page == "trainingcenter") {
					fishSancturiesGroup
					.push({
						lon : obj.lon,
						lat : obj.lat,
						icon : obj.icon,
						html : obj.html,
						image:obj.image,
						//edit : '<div class="trainingcenter_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon : new LeafIcon({
							iconUrl : obj.icon
						})
					});
					$.get("/create/getRegionalOffice/" + obj.uniqueKey, function(data, status) {

						$('.form_input').show();
						getRegionalOfficeInfoById(data, status);
					});
					 document.querySelector('#submitRegionalOffice').innerHTML = 'UPDATE'; 
				//	}
			}
			
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();
		}
		function getLambaklad(map_location) {
			console.log(map_location);
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});

			$(".se-pre-con").show();
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				//if (obj.page == "trainingcenter") {
					fishSancturiesGroup
					.push({
						lon : obj.lon,
						lat : obj.lat,
						icon : obj.icon,
						html : obj.html,
						image:obj.image,
						unique:obj.uniqueKey,
						//edit : '<div class="trainingcenter_id"><a href="#" id="' + obj.uniqueKey + '"> <img class="imgicon" id="myImg" src="'+obj.image+'" alt="'+obj.province + ',' + obj.municipality + ',' + obj.barangay+'" style="width:100px;max-width:300px"></img></a></div>',
						greenIcon : new LeafIcon({
							iconUrl : obj.icon
						})
					});
					$.get("/create/getLamabaklad/" + obj.uniqueKey, function(data, status) {

						$('.form_input').show();
						getLambakladInfoById(data, status);
					});
					 document.querySelector('#submitLambaklad').innerHTML = 'UPDATE'; 
				//	}
			}
			
			setMarkers(fishSancturiesGroup);
			$('#details').empty();
			clearClickMark();
			$(".se-pre-con").hide();
		}
		function getAllResources(map_location) {
			
			var LeafIcon = L.Icon.extend({
 				 options: {
      		iconSize:     [30, 48]
  		}
			});
			for (var i = 0; i < map_location.length; i++) {
				var obj = map_location[i];
				if (obj.page == "trainingcenter") {
					getTrainingCenter(map_location);
				}
					
			}
		}		
//        function markerOnClick(e) {
//      	  var id = this.options.id;
//      	  $(".modal-content").html('This is marker ' + id);
//      	  $('#emptymodal').modal('show');
//      	  map.setView(e.target.getLatLng());
//      	  e.preventDefault();
//      	}
//      	/*Close modal on map click */
//      	map.on('click', function(e) {
//      	  $('.modal').modal('hide');
//      	});
//		
    	function clearClickMark() {

    		if (clickmark != undefined) {
    			map.removeLayer(clickmark);
    		}
    		;

    	}

    	var clickmark;

    	// When you click on a circle, it calls the onMapClick function and passes the layers coordinates.
    	// I grab the coords which are X,Y, and I need to flip them to latLng for a marker,  
    	function onMapClick(coords) {
    		console.log(coords + " " + coords.lat);
    		//if prior marker exists, remove it.
    		if (clickmark != undefined) {
    			map.removeLayer(clickmark);
    		}
    		;

    		for (var i = 0; i < map_location.length; i++) {
    			var obj = map_location[i];
    			if (coords.lat == obj.lat && coords.lng == obj.lon) {
    				console.log("true");
    				$('#details').html(obj.html);
    				$('#infoByID').html(obj.html);
    				$('#clockToronto').html(obj.html);
//    				$('#content').html(obj.html);
//    				$('#content').show();
    				addContenToMap(obj.html);
    			}
    		}

    		clickmark = L.circleMarker([ coords.lat, coords.lng ], {
    			radius : 20,
    			color : "yellow",
    			fillColor : "yellow",
    			fillOpacity : 0.8
    		}).addTo(map);
    	}

    	function addContenToMap(html) {
    		console.log("html: " + html);
    	}
  function getResources(page){
	  console.log("fisheries: " + page);
	  createListPerPage(page);
   } 	

