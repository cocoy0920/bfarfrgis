	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
	        
	            
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
	            
	       $(".submitFishCage").click(function(e){
			e.preventDefault();
			$(".se-pre-con").show();
			
			/*
				var developerData = {};
				
				developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
				developerData["dateAsOf"] = $(".fishcagedateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fishcageprovince").val();
	        	developerData["municipality"] =	$(".fishcagemunicipality").val();
	        	developerData["barangay"] = $(".fishcagebarangay").val();
	        	developerData["nameOfOperator"] = $(".fishcagenameOfOperator").val();
	        	developerData["classificationofOperator"] = $(".classificationofOperator").val();
	        	developerData["cageDimension"] = $(".cageDimension").val();
	        	developerData["cageTotalArea"] = $(".cageTotalArea").val();
	        	developerData["cageType"] = $(".cageType").val();
	        	developerData["cageNoOfCompartments"] = $(".cageNoOfCompartments").val();
	        	developerData["indicateSpecies"] = $(".fishcageindicateSpecies").val();
	        	developerData["sourceOfData"] = $(".fishcagesourceOfData").val();
	        	developerData["area"] = $(".cageTotalArea").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["remarks"] = $(".fishcageremarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);
	        	*/
	        	//add//
	        	
				if($(".fishcagedateAsOf").val().replace(/\s/g,"") == "" ||
	        	$(".region").val().replace(/\s/g,"") == "" ||
				$(".fishcageprovince").val().replace(/\s/g,"") == "" ||
	        	$(".fishcagemunicipality").val().replace(/\s/g,"") == "" ||
	        	$(".fishcagebarangay").val().replace(/\s/g,"") == "" ||
	        	$(".fishcagenameOfOperator").val().replace(/\s/g,"") == "" ||
	        	$(".classificationofOperator").val().replace(/\s/g,"") == "" ||
	        	$(".cageDimension").val().replace(/\s/g,"") == "" ||
	        	$(".cageTotalArea").val().replace(/\s/g,"") == "" ||
	        	$(".cageType").val().replace(/\s/g,"") == "" ||
	        	$(".cageNoOfCompartments").val().replace(/\s/g,"") == "" ||
	        	$(".fishcageindicateSpecies").val().replace(/\s/g,"") == "" ||
	        	$(".fishcagesourceOfData").val().replace(/\s/g,"") == "" ||
	        	$(".cageTotalArea").val().replace(/\s/g,"") == "" ||
	        	$(".code").val().replace(/\s/g,"") == "" ||
	        	 $(".lat").val().replace(/\s/g,"") == "" ||
	        	 $(".lon").val().replace(/\s/g,"") == "" ||
	        	// $(".image").val().replace(/\s/g,"") == "" ||
	        	 $(".image_src").val().replace(/\s/g,"") == ""){
					$(".se-pre-con").hide();
	        		alert("ALL FIELDS ARE REQUIRED");
	        		return false;
	        	}
	            
	            //end//
	        			        	
		        	/* CHANGE END */
	            
				var form = document.getElementById( "myform" );
	        	var json = ConvertFormToJSON(form);
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "savingFishCage",
					data : json,
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						
						 var lastPage;
						 $.get("getLastPageFC/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        	
				        	 $.get("fishcage/" + lastPage, function(content, status){
				           	  
				            	 var markup = "";
				             			for ( var x = 0; x < content.fishCage.length; x++) {
				             			 
				             			   markup +="<tr data-href="+ content.fishCage[x].uniqueKey+"><td>"+content.fishCage[x].region+"</td><td>"+content.fishCage[x].province+"</td><td>"+content.fishCage[x].municipality+"</td><td>"+content.fishCage[x].barangay+"</td><td>"+content.fishCage[x].nameOfOperator+"</td></tr>";
				    					}
				        			
				        				$("#export-buttons-table").find('tbody').empty();
				        				$("#export-buttons-table").find('tbody').append(markup);
				        			$("#paging").empty();		
				        			$("#paging").append(content.pageNumber);	
				        		  });
				        	  
				     		  });
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
				//refreshRecord(data,"fishcage","old");							
						
			        	/*$(".fishcagedateAsOf").val("");			        	
						$(".fishcageprovince").val("");
			        	$(".fishcagemunicipality").empty();
			        	$(".fishcagebarangay").empty();
			        	$(".nameOfOperator").val("");
			        	$(".classificationofOperator").val("");
			        	$(".cageDimension").val("");
			        	$(".cageTotalArea").val("");
			        	$(".cageType").val("");
			        	$(".cageNoOfCompartments").val("");
			        	$(".indicateSpecies").val("");
			        	$(".sourceOfData").val("");
			        	$(".cageTotalArea").val("");
			        	$(".code").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						$(".image_src").val("");
						$('.preview').attr('src', '');
						$('.file').val("");*/
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					return;
					}
				});
			});
	});