	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });

	            function toJSONString( form ) {
	        		var obj = {};
	        		var elements = form.querySelectorAll( "input, select, textarea, hidden" );
	        		for( var i = 0; i < elements.length; ++i ) {
	        			var element = elements[i];
	        			var name = element.name;
	        			var value = element.value;

	        			if( name ) {
	        				obj[ name ] = value;
	        			}
	        		}

	        		return JSON.stringify( obj );
	        	}
	            
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
	            
		$(".submitFishSanctuaries").click(function(e){
			e.preventDefault();
			
			$(".se-pre-con").show();
			
			if($(".dateAsOf").val().replace(/\s/g,"") == "" ||
		        	  $(".fishsanctuaryprovince").val().replace(/\s/g,"") == "" ||
		        	 $(".fishsanctuarymunicipality").val().replace(/\s/g,"") == "" ||
		        	$(".fishsanctuarybarangay").val().replace(/\s/g,"") == "" ||
		        	 $(".nameOfFishSanctuary").val().replace(/\s/g,"") == "" ||
		        	 $(".code").val().replace(/\s/g,"") == "" ||
		        	 $(".area").val().replace(/\s/g,"") == "" ||
		        	 $(".type").val().replace(/\s/g,"") == "" ||
		        	 $(".fishSanctuarySource").val().replace(/\s/g,"") == "" ||
		        	 $(".remarksContent").val().replace(/\s/g,"") == "" ||
		        	 $(".bfardenr").val().replace(/\s/g,"") == "" ||
		        	 $(".lat").val().replace(/\s/g,"") == "" ||
		        	 $(".lon").val().replace(/\s/g,"") == "" ||
		        	 $(".image_src").val().replace(/\s/g,"") == ""){
					$(".se-pre-con").hide();
		        		alert("ALL FIELDS ARE REQUIRED");
		        		return false;
		        	}
        	
        	var form = document.getElementById( "myform" );
        	var json = ConvertFormToJSON(form);
        	
			$.ajax({
				url : "getFishSanctuary",
				type : "POST",
				contentType : "application/json",
				data : json,
				dataType : 'json',				
				success : function(data) {
				
					$(".se-pre-con").hide();
					alert("SUCCESSFULLY SAVE");
					 var lastPage;
					 $.get("getLastPage/", function(content, status){
			         	 lastPage = content;
			         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
			        	
			         	$.get("fishsanctuaries/" + lastPage, function(content, status){
				         	
			        		 data = content.fishSanctuaries;
				        	 var markup = "";
				         			for ( var x = 0; x < content.fishSanctuaries.length; x++) {
				         			   
				         			   markup +="<tr data-href="+ content.fishSanctuaries[x].uniqueKey+"><td>"+content.fishSanctuaries[x].region+"</td><td>"+content.fishSanctuaries[x].province+"</td><td>"+content.fishSanctuaries[x].municipality+"</td><td>"+content.fishSanctuaries[x].barangay+"</td><td>"+content.fishSanctuaries[x].nameOfFishSanctuary+"</td></tr>";
									}   	
				    				$("#export-buttons-table").find('tbody').empty();
				    				$("#export-buttons-table").find('tbody').append(markup);
				    			$("#paging").empty();		
				    			$("#paging").append(content.pageNumber);	
				     		  });
			     		  });
					 
			      	 // var pro =  document.getElementById("province");
			      	/*var pro = $( "#province option" ).text();
			      	var x = document.getElementById("province");
			      	var txt = "";
			      	var i;
			      	for (i = 0; i < x.length; i++) {
			      	  txt = txt + x.options[i].text + "<br>";
			      	}*/
					 document.getElementById("myform").reset();
					 var slctMunicipality=$('#municipality');
		        	  var selectBarangay=$('#barangay');
		        	  slctMunicipality.empty();
		        	  selectBarangay.empty();
		        	  $('.preview').attr('src', '');
		        	$('#bfarData').hide();
		            $('#denrData').hide();
		            $('#pdftable').hide();
				},
				error: function(data){
				console.log("error", data);
				$(".se-pre-con").hide();
				return;
				
				}
			});
		});
	});	