	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
	      $(".submitMarineProtectedArea").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
				developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".dateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".marineprotectedareaprovince").val();
	        	developerData["municipality"] =	$(".marineprotectedareamunicipality").val();
	        	developerData["barangay"] = $(".marineprotectedareabarangay").val();
	        	developerData["area"] = $(".marineprotectedarea").val();
	        	developerData["nameOfMarineProtectedArea"] = $(".nameOfMarineProtectedArea").val();
	        	developerData["type"] = $(".type").val();
	        	developerData["sensitiveHabitatWithMPA"] = $(".sensitiveHabitatWithMPA").val();
	        	developerData["dateEstablishment"] = $(".dateEstablishment").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);
	        	
	        	//add //
	        	
			
				
	        	if($(".dateAsOf").val().replace(/\s/g,"") == "" ||
				$(".marineprotectedareaprovince").val().replace(/\s/g,"") == "" ||
	        	$(".marineprotectedareamunicipality").val().replace(/\s/g,"") == "" ||
	        	 $(".marineprotectedareabarangay").val().replace(/\s/g,"") == "" ||
	        	 $(".marineprotectedarea").val().replace(/\s/g,"") == "" ||
	        	 $(".nameOfMarineProtectedArea").val().replace(/\s/g,"") == "" ||
	        	 $(".type").val().replace(/\s/g,"") == "" ||
	        	 $(".sensitiveHabitatWithMPA").val().replace(/\s/g,"") == "" ||
	        	 $(".dateEstablishment").val().replace(/\s/g,"") == "" ||
	        	 $(".code").val().replace(/\s/g,"") == "" ||
	        	 $(".dataSource").val().replace(/\s/g,"") == "" ||
	        	// $(".remarks").val().replace(/\s/g,"") == "" ||
	        	 $(".lat").val().replace(/\s/g,"") == "" ||
	        	 $(".lon").val().replace(/\s/g,"") == "" ||
	        	//developerData["image"] = $(".image").val();
	        	 $(".image_src").val().replace(/\s/g,"") == ""){
	        		alert("ALL FIELDS ARE REQUIRED");
	        		return false;
	        	}
	            
	        	//end //
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getMarineProtectedArea",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						MarineProtectedAreaList(true);
						console.log("SUCCESS", data);
						alert("SUCCESSFULLY SAVE");
						
				refreshRecord(data,"marineprotected","old");
						
			        	$(".dateAsOf").val("");
			        	
						$(".marineprotectedareaprovince").val("");
			        	$(".marineprotectedareamunicipality").empty();
			        	$(".marineprotectedareabarangay").empty();
			        	$(".marineprotectedarea").val("");
			        	$(".nameOfMarineProtectedArea").val("");
			        	$(".type").val("");
			        	$(".sensitiveHabitatWithMPA").val("");
			        	$(".dateEstablishment").val();
			        	$(".code").val("");
			        	$(".dataSource").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".image_src").val();
			        	$('.preview').attr('src', '');
			        	$('.file').val("");
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
	});