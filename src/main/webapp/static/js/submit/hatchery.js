	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });

	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
	            
		$(".submitHatchery").click(function(e){
			e.preventDefault();
			//	var developerData = {};
			$(".se-pre-con").show();	
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".hatcherydateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".hatcheryprovince").val();
	        	developerData["municipality"] =	$(".hatcherymunicipality").val();
	        	developerData["barangay"] = $(".hatcherybarangay").val();
	        	developerData["nameOfHatchery"] = $(".nameOfHatchery").val();
	        	developerData["nameOfOperator"] = $(".hatcherynameOfOperator").val();
	        	developerData["addressOfOperator"] = $(".hatcheryaddressOfOperator").val();
	        	developerData["operatorClassification"] = $(".hatcheryoperatorClassification").val();
	        	developerData["area"] = $(".hatcheryarea").val();
	        	developerData["publicprivate"] = $(".hatcherypublicprivate").val();
	        	developerData["indicateSpecies"] = $(".hatcheryindicateSpecies").val();
	        	developerData["prodStocking"] = $(".prodStocking").val();
	        	developerData["prodCycle"] = $(".prodCycle").val();
	        	developerData["actualProdForTheYear"] = $(".actualProdForTheYear").val();
	        	developerData["monthStart"] = $(".monthStart").val();
	        	developerData["monthEnd"] = $(".monthEnd").val();
	        	developerData["dataSource"] = $(".hatcherydataSource").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["remarks"] = $(".hatcheryremarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);*/
				
	        	//add //
	        	
				if($(".hatcherydateAsOf").val().replace(/\s/g,"") == "" ||
				$(".region").val().replace(/\s/g,"") == "" ||
				$(".hatcheryprovince").val().replace(/\s/g,"") == "" ||
	        	$(".hatcherymunicipality").val().replace(/\s/g,"") == "" ||
	        	$(".hatcherybarangay").val().replace(/\s/g,"") == "" ||
	        	$(".nameOfHatchery").val().replace(/\s/g,"") == "" ||
	        	$(".hatcherynameOfOperator").val().replace(/\s/g,"") == "" ||
	        	$(".hatcheryaddressOfOperator").val().replace(/\s/g,"") == "" ||
	        	$(".hatcheryoperatorClassification").val().replace(/\s/g,"") == "" ||
	        	$(".hatcheryarea").val().replace(/\s/g,"") == "" ||
	        	$(".hatcherypublicprivate").val().replace(/\s/g,"") == "" ||
	        	$(".hatcheryindicateSpecies").val().replace(/\s/g,"") == "" ||
	        	$(".prodStocking").val().replace(/\s/g,"") == "" ||
	        	$(".prodCycle").val().replace(/\s/g,"") == "" ||
	        	$(".actualProdForTheYear").val().replace(/\s/g,"") == "" ||
	        	$(".monthStart").val().replace(/\s/g,"") == "" ||
	        	$(".monthEnd").val().replace(/\s/g,"") == "" ||
	        	$(".hatcherydataSource").val().replace(/\s/g,"") == "" ||
	        	$(".code").val().replace(/\s/g,"") == "" ||
	        	//$(".hatcheryremarks").val().replace(/\s/g,"") == "" ||
	        	$(".lat").val().replace(/\s/g,"") == "" ||
	        	 $(".lon").val().replace(/\s/g,"") == "" ||
	        	// $(".image").val().replace(/\s/g,"") == "" ||
	        	 $(".image_src").val().replace(/\s/g,"") == ""){
					$(".se-pre-con").hide();
	        		alert("ALL FIELDS ARE REQUIRED");
	        		return false;
	        	}

	            
	        	//end //
		        	
		        	/* CHANGE END */
	        	var form = document.getElementById( "myform" );
	        	var json = ConvertFormToJSON(form);
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getHatchery",
					/* CHANGE END */
					
					data : json,
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						
						 var lastPage;
						 $.get("getLastPageHatchery/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        	
				        	 $.get("hatchery/" + lastPage, function(content, status){
				            	 
				            	 var markup = "";
				             			for ( var x = 0; x < content.hatcheries.length; x++) {
				             			   
				             			   markup +="<tr data-href="+ content.hatcheries[x].uniqueKey+"><td>"+content.hatcheries[x].region+"</td><td>"+content.hatcheries[x].province+"</td><td>"+content.hatcheries[x].municipality+"</td><td>"+content.hatcheries[x].barangay+"</td><td>"+content.hatcheries[x].nameOfHatchery+"</td></tr>";
				    					}
				        				
				        				$("#export-buttons-table").find('tbody').empty();
				        				$("#export-buttons-table").find('tbody').append(markup);
				        			$("#paging").empty();		
				        			$("#paging").append(content.pageNumber);	
				        		  });
				     		  });
						 
				      	   
						
			//	refreshRecord(data,"hatchery","old");	
						/*
			        	$(".hatcherydateAsOf").val("");
			        	
						$(".hatcheryprovince").val("");
			        	$(".hatcherymunicipality").empty();
			        	$(".hatcherybarangay").empty();
			        	$(".nameOfHatchery").val("");
			        	$(".hatcherynameOfOperator").val("");
			        	$(".hatcheryaddressOfOperator").val("");
			        	$(".hatcheryoperatorClassification").val("");
			        	$(".hatcheryarea").val("");
			        	$(".hatcherypublicprivate").val("");
			        	$(".hatcheryindicateSpecies").val("");
			        	$(".prodStocking").val("");
			        	$(".prodCycle").val("");
			        	$(".actualProdForTheYear").val("");
			        	$(".monthStart").val("");
			        	$(".monthEnd").val("");
			        	$(".hatcherydataSource").val("");
			        	$(".code").val("");
			        	$(".hatcheryremarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
						$(".image_src").val("");
						$('.preview').attr('src', '');
						$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					}
				});
			});
	});