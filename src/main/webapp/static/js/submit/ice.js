	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
		
	            
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
	            
		$(".submitIcePlantColdStorage").click(function(e){
			e.preventDefault();
				//var developerData = {};
				$(".se-pre-con").show();
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".iceplantdateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".iceplantorcoldstorageprovince").val();
	        	developerData["municipality"] =	$(".iceplantorcoldstoragemunicipality").val();
	        	developerData["barangay"] = $(".iceplantorcoldstoragebarangay").val();
	        	developerData["nameOfIcePlant"] = $(".nameOfIcePlant").val();
	        	developerData["sanitaryPermit"] = $(".sanitaryPermit").val();
	        	developerData["operator"] = $(".operator").val();
	        	developerData["typeOfFacility"] = $(".typeOfFacility").val();
	        	developerData["structureType"] = $(".structureType").val();
	        	developerData["capacity"] = $(".capacity").val();
	        	developerData["withValidLicenseToOperate"] = $(".withValidLicenseToOperate").val();
	        	developerData["withValidSanitaryPermit"] = $(".withValidSanitaryPermit").val();
	        	developerData["withValidSanitaryPermitYes"] = $(".withValidSanitaryPermitYes").val();
	        	developerData["withValidSanitaryPermitOthers"] = $(".withValidSanitaryPermitOthers").val();
	        	developerData["businessPermitsAndCertificateObtained"] = $(".iceplantbusinessPermitsAndCertificateObtained").val();
	        	developerData["typeOfIceMaker"] = $(".typeOfIceMaker").val();
	        	developerData["foodGradule"] = $(".foodGradule").val();
	        	developerData["area"] = $(".icearea").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".iceplantdataSources").val();
	        	developerData["remarks"] = $(".iceplantremarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);*/
				
	        	//add //
	        	
				if($(".iceplantdateAsOf").val().replace(/\s/g,"") == "" ||
				$(".region").val().replace(/\s/g,"") == "" ||
				$(".iceplantorcoldstorageprovince").val().replace(/\s/g,"") == "" ||
	        	$(".iceplantorcoldstoragemunicipality").val().replace(/\s/g,"") == "" ||
	        	$(".iceplantorcoldstoragebarangay").val().replace(/\s/g,"") == "" ||
	        	$(".nameOfIcePlant").val().replace(/\s/g,"") == "" ||
	        	$(".sanitaryPermit").val().replace(/\s/g,"") == "" ||
	        	$(".operator").val().replace(/\s/g,"") == "" ||
	        	$(".typeOfFacility").val().replace(/\s/g,"") == "" ||
	        	$(".structureType").val().replace(/\s/g,"") == "" ||
	        	$(".capacity").val().replace(/\s/g,"") == "" ||
	        	$(".withValidLicenseToOperate").val().replace(/\s/g,"") == "" ||
	        	$(".withValidSanitaryPermit").val().replace(/\s/g,"") == "" ||
	        	$(".iceplantbusinessPermitsAndCertificateObtained").val().replace(/\s/g,"") == "" ||
	        	$(".typeOfIceMaker").val().replace(/\s/g,"") == "" ||
	        	$(".foodGradule").val().replace(/\s/g,"") == "" ||
	        	$(".icearea").val().replace(/\s/g,"") == "" ||
	        	$(".code").val().replace(/\s/g,"") == "" ||
	        	$(".iceplantdataSources").val().replace(/\s/g,"") == "" ||
	        	$(".lat").val().replace(/\s/g,"") == "" ||
	        	 $(".lon").val().replace(/\s/g,"") == "" ||
	        	 $(".image_src").val().replace(/\s/g,"") == ""){
					$(".se-pre-con").hide();
	        		alert("ALL FIELDS ARE REQUIRED");
	        		return false;
	        	}

	            
	            //end //
	        			        	
		        	/* CHANGE END */
				var form = document.getElementById( "myform" );
	        	var json = ConvertFormToJSON(form);	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getIcePlant",
					/* CHANGE END */
					
					data : json,
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						
						 var lastPage;
						 $.get("getLastPageIcePlant/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        	
				         	 $.get("iceplantorcoldstorage/" + lastPage, function(content, status){
				             	
				            	 var markup = "";
				             			for ( var x = 0; x < content.icePlantColdStorage.length; x++) {
				             			   markup +="<tr data-href="+ content.icePlantColdStorage[x].uniqueKey+"><td>"+content.icePlantColdStorage[x].region+"</td><td>"+content.icePlantColdStorage[x].province+"</td><td>"+content.icePlantColdStorage[x].municipality+"</td><td>"+content.icePlantColdStorage[x].barangay+"</td><td>"+content.icePlantColdStorage[x].nameOfIcePlant+"</td></tr>";
				    					}
				        				$("#export-buttons-table").find('tbody').empty();
				        				$("#export-buttons-table").find('tbody').append(markup);
				        			$("#paging").empty();		
				        			$("#paging").append(content.pageNumber);	
				        		  });
				     		  });
						 
				      	   
			//	refreshRecord(data,"iceplantorcoldstorage","old");	
						
				/*
						$(".iceplantdateAsOf").val("");
						$(".iceplantorcoldstorageprovince").val("");
			        	$(".iceplantorcoldstoragemunicipality").empty();
			        	$(".iceplantorcoldstoragebarangay").empty();
			        	$(".nameOfIcePlant").val("");
			        	$(".sanitaryPermit").val("");
			        	$(".operator").val("");
			        	$(".typeOfFacility").val("");
			        	$(".structureType").val("");
			        	$(".capacity").val("");
			        	$(".withValidLicenseToOperate").val("");
			        	$(".withValidSanitaryPermit").val("");
			        	$(".withValidSanitaryPermitYes").val("");
			        	$(".withValidSanitaryPermitOthers").val("");
			        	$(".businessPermitsAndCertificateObtained").val("");
			        	$(".typeOfIceMaker").val("");
			        	$(".foodGradule").val("");
			        	$(".icearea").val("");
			        	$(".code").val("");
			        	$(".iceplantdataSources").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".image_src").val("");
			        	$('.preview').attr('src', '');
			        	$('#yesData').hide();
			        	$('#othersData').hide(); 
			        	$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', ''); 
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					}
				});
			});
	  });
		