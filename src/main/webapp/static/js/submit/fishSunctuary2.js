	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });

		$(".submitFishSanctuaries").click(function(e){
			e.preventDefault();
			var developerData = {};
			
			$(".se-pre-con").show();
			/* CHANGE ONLY */
			developerData["id"] = $(".id").val();
			developerData["user"] = $(".user").val();
			developerData["uniqueKey"] = $(".uniqueKey").val();
			developerData["encodedBy"] = $(".encodedBy").val();
			developerData["editedBy"] = $(".editedBy").val();
			developerData["dateEncoded"] = $(".dateEncoded").val();
			developerData["dateEdited"] = $(".dateEdited").val();
			developerData["dateAsOf"] = $(".dateAsOf").val();
        	developerData["region"] = $(".region").val();
			developerData["province"] = $(".fishsanctuaryprovince").val();
        	developerData["municipality"] =	$(".fishsanctuarymunicipality").val();
        	developerData["barangay"] = $(".fishsanctuarybarangay").val();
        	developerData["nameOfFishSanctuary"] = $(".nameOfFishSanctuary").val();
        	developerData["code"] = $(".code").val();
        	developerData["area"] = $(".area").val();
        	developerData["type"] = $(".type").val();
        	developerData["fishSanctuarySource"] = $(".fishSanctuarySource").val();
        	developerData["remarks"] = $(".remarksContent").val();
        	developerData["bfardenr"] = $(".bfardenr").val();
        	developerData["sheltered"] = $(".sheltered").val();
        	developerData["nameOfSensitiveHabitatMPA"] = $(".nameOfSensitiveHabitatMPA").val();
        	developerData["OrdinanceNo"] = $(".OrdinanceNo").val();
        	developerData["OrdinanceTitle"] = $(".OrdinanceTitle").val();
        	developerData["DateEstablished"] = $(".DateEstablished").val();
        	developerData["lat"] = $(".lat").val();
        	developerData["lon"] = $(".lon").val();
        	developerData["image"] = $(".image").val();
        	developerData["image_src"] = $(".image_src").val();
        	//console.log(developerData);
        	
        	if($(".dateAsOf").val().replace(/\s/g,"") == "" ||
        	  $(".fishsanctuaryprovince").val().replace(/\s/g,"") == "" ||
        	 $(".fishsanctuarymunicipality").val().replace(/\s/g,"") == "" ||
        	$(".fishsanctuarybarangay").val().replace(/\s/g,"") == "" ||
        	 $(".nameOfFishSanctuary").val().replace(/\s/g,"") == "" ||
        	 $(".code").val().replace(/\s/g,"") == "" ||
        	 $(".area").val().replace(/\s/g,"") == "" ||
        	 $(".type").val().replace(/\s/g,"") == "" ||
        	 $(".fishSanctuarySource").val().replace(/\s/g,"") == "" ||
        	 $(".remarksContent").val().replace(/\s/g,"") == "" ||
        	 $(".bfardenr").val().replace(/\s/g,"") == "" ||
        	 $(".lat").val().replace(/\s/g,"") == "" ||
        	 $(".lon").val().replace(/\s/g,"") == "" ||
        	 $(".image_src").val().replace(/\s/g,"") == ""){
        		alert("ALL FIELDS ARE REQUIRED");
        		return false;
        	}
        	
			$.ajax({
				url : "getFishSanctuary",
				type : "POST",
				contentType : "application/json",
				data : formData,
				dataType : 'json',				
				success : function(data) {
					//("#loading").hide();
					$(".se-pre-con").hide();
				//	FishSanctuaryList(true);
				//	console.log("SUCCESS", data);
					// var container = $('#myModalList .modal-dialog .modal-content .modal-body');
					 //container.load("fishsanctuariesList");	
					alert("SUCCESSFULLY SAVE");
					 var lastPage;
					 $.get("getLastPage/", function(content, status){
			         	 lastPage = content;
			         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
			        	
			         	$.get("fishsanctuaries/" + lastPage, function(content, status){
				         	
			        		 data = content.fishSanctuaries;
				        	 var markup = "";
				         			for ( var x = 0; x < content.fishSanctuaries.length; x++) {
				         			   
				         			   markup +="<tr data-href="+ content.fishSanctuaries[x].uniqueKey+"><td>"+content.fishSanctuaries[x].region+"</td><td>"+content.fishSanctuaries[x].province+"</td><td>"+content.fishSanctuaries[x].municipality+"</td><td>"+content.fishSanctuaries[x].barangay+"</td><td>"+content.fishSanctuaries[x].nameOfFishSanctuary+"</td></tr>";
									}   	
				    				$("#export-buttons-table").find('tbody').empty();
				    				$("#export-buttons-table").find('tbody').append(markup);
				    			$("#paging").empty();		
				    			$("#paging").append(content.pageNumber);	
				     		  });
			     		  });
					 
			      	   
			      	

					 
				//refreshRecord(data,"fishsanctuaries","new");
					//window.location.reload();
		        	$(".dateAsOf").val("");
		
					$(".fishsanctuaryprovince").val("");
		        	$(".fishsanctuarymunicipality").empty();
		        	$(".fishsanctuarybarangay").empty();
		        	$(".nameOfFishSanctuary").val("");
		        	$(".code").val("");
		        	$(".area").val("");
		        	$(".type").val("");
		        	$(".fishSanctuarySource").val("");
		        	$(".remarksContent").val("");
		        	$(".bfardenr").val("");
		        	$(".sheltered").val("");
		        	$(".lat").val("");
		        	$(".lon").val("");
		        	$(".image").val("");
		        	$(".image_src").val("");
		        	$('.preview').attr('src', "");
		        	$('.file').val("");
		        	$('#bfarData').hide();
		            $('#denrData').hide();
		            $('#pdftable').hide();
				},
				error: function(data){
				console.log("error", data);
				}
			});
		});
	});	