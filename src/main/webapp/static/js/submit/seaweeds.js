	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
	           
		$(".submitSeaWeeds").click(function(e){
			e.preventDefault();
				var developerData = {};
				$(".se-pre-con").show();
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".seaweedsdateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".seaweedsprovince").val();
	        	developerData["municipality"] =	$(".seaweedsmunicipality").val();
	        	developerData["barangay"] = $(".seaweedsbarangay").val();
	        	developerData["area"] = $(".seaweedsarea").val();
	        	developerData["indicateGenus"] = $(".indicateGenus").val();
	        	developerData["culturedMethodUsed"] = $(".culturedMethodUsed").val();
	        	developerData["culturedPeriod"] = $(".culturedPeriod").val();
	        	developerData["productionKgPerCropping"] = $(".productionKgPerCropping").val();
	        	developerData["productionNoOfCroppingPerYear"] = $(".productionNoOfCroppingPerYear").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".seaweedsdataSource").val();
	        	developerData["remarks"] = $(".seaweedsremarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);*/
	        	
	        	//add //
				
	        	if( $(".seaweedsdateAsOf").val().replace(/\s/g,"") == "" ||
				 $(".seaweedsprovince").val().replace(/\s/g,"") == "" ||
	        	 $(".seaweedsmunicipality").val().replace(/\s/g,"") == "" ||
	        	 $(".seaweedsbarangay").val().replace(/\s/g,"") == "" ||
	        	 $(".seaweedsarea").val().replace(/\s/g,"") == "" ||
	        	 $(".indicateGenus").val().replace(/\s/g,"") == "" ||
	        	 $(".culturedMethodUsed").val().replace(/\s/g,"") == "" ||
	        	 $(".culturedPeriod").val().replace(/\s/g,"") == "" ||
	        	 $(".productionKgPerCropping").val().replace(/\s/g,"") == "" ||
	        	 $(".productionNoOfCroppingPerYear").val().replace(/\s/g,"") == "" ||
	        	 $(".code").val().replace(/\s/g,"") == "" ||
	        	 $(".seaweedsdataSource").val().replace(/\s/g,"") == "" ||
	        	// $(".seaweedsremarks").val().replace(/\s/g,"") == "" ||
	        	 $(".lat").val().replace(/\s/g,"") == "" ||
	        	 $(".lon").val().replace(/\s/g,"") == ""){
	        		$(".se-pre-con").hide();
	        		alert("ALL FIELDS ARE REQUIRED");
	        		return false;
	        	}
	        	//developerData["image"] = $(".image").val();
	        	//developerData["image_src"] = $(".image_src").val();	            
	        	//end //
	        	
		        	
		        	/* CHANGE END */
	        	var form = document.getElementById( "myform" );
	        	var json = ConvertFormToJSON(form);
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getSeaWeeds",
					/* CHANGE END */
					
					data : json,
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						
						 var lastPage;
						 $.get("getLastPageSeaweeds/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        	
				         	$.get("seaweeds/" + lastPage, function(content, status){
				          	  
				           	 var markup = "";
				            			for ( var x = 0; x < content.seaweeds.length; x++) {
				            			   markup +="<tr data-href="+ content.seaweeds[x].uniqueKey+"><td>"+content.seaweeds[x].region+"</td><td>"+content.seaweeds[x].province+"</td><td>"+content.seaweeds[x].municipality+"</td><td>"+content.seaweeds[x].barangay+"</td><td>"+content.seaweeds[x].culturedMethodUsed+"</td></tr>";
				   					}
				       				$("#export-buttons-table").find('tbody').empty();
				       				$("#export-buttons-table").find('tbody').append(markup);
				       			$("#paging").empty();		
				       			$("#paging").append(content.pageNumber);	
				       		  });
				     		  });
						 
				      	   	
				//refreshRecord(data,"seaweeds","old");
						/*
			        	$(".seaweedsdateAsOf").val("");
			        	
						$(".seaweedsprovince").val("");
			        	$(".seaweedsmunicipality").empty();
			        	$(".seaweedsbarangay").empty();
			        	$(".seaweedsarea").val("");
			        	$(".indicateGenus").val("");
			        	$(".culturedMethodUsed").val("");
			        	$(".culturedPeriod").val("");
			        	$(".productionKgPerCropping").val("");
			        	$(".productionNoOfCroppingPerYear").val("");
			        	$(".code").val("");
			        	$(".seaweedsdataSource").val("");
			        	$(".seaweedsremarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".image_src").val();
			        	$('.preview').attr('src', '');
			        	$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					}
				});
			});
		
	});