	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
	         
		
		$(".submitFishport").click(function(e){
			e.preventDefault();
				//var developerData = {};
				$(".se-pre-con").show();
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".fishportdateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fishportprovince").val();
	        	developerData["municipality"] =	$(".fishportmunicipality").val();
	        	developerData["barangay"] = $(".fishportbarangay").val();
	        	developerData["nameOfFishport"] = $(".nameOfFishport").val();
	        	developerData["nameOfCaretaker"] = $(".fishportnameOfCaretaker").val();
	        	developerData["type"] = $(".fishPortType").val();
	        	developerData["volumeOfUnloading"] = $(".volumeOfUnloading").val();
	        	developerData["classification"] = $(".fishportclassification").val();
	        	developerData["area"] = $(".fishportarea").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".fishportdatasource").val();
	        	developerData["remarks"] = $(".fishportremarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);*/
	        	
	        	//add //

				if($(".fishportdateAsOf").val().replace(/\s/g,"") == "" ||
	        	$(".region").val().replace(/\s/g,"") == "" ||
				$(".fishportprovince").val().replace(/\s/g,"") == "" ||
	        	$(".fishportmunicipality").val().replace(/\s/g,"") == "" ||
	        	$(".fishportbarangay").val().replace(/\s/g,"") == "" ||
	        	$(".nameOfFishport").val().replace(/\s/g,"") == "" ||
	        	$(".fishportnameOfCaretaker").val().replace(/\s/g,"") == "" ||
	        	$(".fishPortType").val().replace(/\s/g,"") == "" ||
	        	$(".volumeOfUnloading").val().replace(/\s/g,"") == "" ||
	        	$(".fishportclassification").val().replace(/\s/g,"") == "" ||
	        	$(".fishportarea").val().replace(/\s/g,"") == "" ||
	        	$(".code").val().replace(/\s/g,"") == "" ||
	        	$(".fishportdatasource").val().replace(/\s/g,"") == "" ||
	        	$(".lat").val().replace(/\s/g,"") == "" ||
	        	$(".lon").val().replace(/\s/g,"") == "" ||
	        	$(".image").val().replace(/\s/g,"") == "" ||
	        	$(".image_src").val().replace(/\s/g,"") == "" ){
				$(".se-pre-con").hide();	
			        		alert("ALL FIELDS ARE REQUIRED");
			        		return false;
			        	}

	            //end//
	        	
		        	
		        	/* CHANGE END */
				var form = document.getElementById( "myform" );
	        	var json = ConvertFormToJSON(form);	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getFishPort",
					/* CHANGE END */
					
					data : json,
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						
						 var lastPage;
						 $.get("getLastPageFPport/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        	
				         	 $.get("FISHPORT/" + lastPage, function(content, status){
				           	  
				            	 var markup = "";
				             			for ( var x = 0; x < content.fishPort.length; x++) {
				             			   markup +="<tr data-href="+ content.fishPort[x].uniqueKey+"><td>"+content.fishPort[x].region+"</td><td>"+content.fishPort[x].province+"</td><td>"+content.fishPort[x].municipality+"</td><td>"+content.fishPort[x].barangay+"</td><td>"+content.fishPort[x].nameOfFishport+"</td></tr>";
				    					}
				        				$("#export-buttons-table").find('tbody').empty();
				        				$("#export-buttons-table").find('tbody').append(markup);
				        			$("#paging").empty();		
				        			$("#paging").append(content.pageNumber);	
				        		  });
				     		  });
						 
				      	   
				//refreshRecord(data,"FISHPORT","old");		
						/*
			        	$(".fishportdateAsOf").val("");
			        	
						$(".fishportprovince").val("");
			        	$(".fishportmunicipality").empty();
			        	$(".fishportbarangay").empty();
			        	$(".nameOfFishport").val("");
			        	$(".fishportnameOfCaretaker").val("");
			        	$(".fishportoperatorClassification").val("");
			        	$(".fishPortType").val("");
			        	$(".volumeOfUnloading").val("");
			        	$(".fishportclassification").val("");
			        	$(".fishportarea").val("");
			        	$(".code").val("");
			        	$(".fishportdatasource").val("");
			        	$(".fishportremarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".image_src").val("");
			        	$('.preview').attr('src', '');	
			        	$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					}
				});
			});
	 });	
		