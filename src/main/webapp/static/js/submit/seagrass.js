		
	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
	           
		
		$(".submitSeaGrass").click(function(e){
			e.preventDefault();
				var developerData = {};
				$(".se-pre-con").show();
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".seagrassdateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".seagrassprovince").val();
	        	developerData["municipality"] =	$(".seagrassmunicipality").val();
	        	developerData["barangay"] = $(".seagrassbarangay").val();
	        	developerData["indicateGenus"] = $(".seagrassindicateGenus").val();
	        	developerData["culturedMethodUsed"] = $(".seagrassculturedMethodUsed").val();
	        	developerData["culturedPeriod"] = $(".seagrassculturedPeriod").val();
	        	developerData["volumePerCropping"] = $(".seagrassvolumePerCropping").val();
	        	developerData["noOfCroppingPerYear"] = $(".noOfCroppingPerYear").val();
	        	developerData["area"] = $(".seagrassarea").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".seagrassdataSource").val();
	        	developerData["remarks"] = $(".seagrassremarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);*/
	        	
	        	//add //
				
	        	if($(".seagrassdateAsOf").val().replace(/\s/g,"") == "" ||
				$(".seagrassprovince").val().replace(/\s/g,"") == "" ||
	        	$(".seagrassmunicipality").val().replace(/\s/g,"") == "" ||
	        	$(".seagrassbarangay").val().replace(/\s/g,"") == "" ||
	        	$(".seagrassindicateGenus").val().replace(/\s/g,"") == "" ||
	        	$(".seagrassculturedMethodUsed").val().replace(/\s/g,"") == "" ||
	        	$(".seagrassculturedPeriod").val().replace(/\s/g,"") == "" ||
	        	$(".seagrassvolumePerCropping").val().replace(/\s/g,"") == "" ||
	        	$(".noOfCroppingPerYear").val().replace(/\s/g,"") == "" ||	        	
	        	$(".seagrassarea").val().replace(/\s/g,"") == "" ||
	        	$(".code").val().replace(/\s/g,"") == "" ||
	        	$(".seagrassdataSource").val().replace(/\s/g,"") == "" ||
	        	$(".lat").val().replace(/\s/g,"") == "" ||
	        	$(".lon").val().replace(/\s/g,"") == ""){
	        		$(".se-pre-con").hide();
	        		alert("ALL FIELDS ARE REQUIRED");
	        		return false;
				}
	        	//developerData["image"] = $(".image").val();
	        	//developerData["image_src"] = $(".image_src").val();
	            
	        	//end //
	        	
		        	
		        	/* CHANGE END */
	        	var form = document.getElementById( "myform" );
	        	var json = ConvertFormToJSON(form);
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getSeaGrass",
					/* CHANGE END */
					
					data : json,
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						
						 var lastPage;
						 $.get("getLastPageSeagrass/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        	
				         	 $.get("seagrass/" + lastPage, function(content, status){
				           	  
				            	 var markup = "";
				             			for ( var x = 0; x < content.seaGrass.length; x++) {
				             			   markup +="<tr data-href="+ content.seaGrass[x].uniqueKey+"><td>"+content.seaGrass[x].region+"</td><td>"+content.seaGrass[x].province+"</td><td>"+content.seaGrass[x].municipality+"</td><td>"+content.seaGrass[x].barangay+"</td><td>"+content.seaGrass[x].culturedMethodUsed+"</td></tr>";
				    					}
				        				
				        				$("#export-buttons-table").find('tbody').empty();
				        				$("#export-buttons-table").find('tbody').append(markup);
				        			$("#paging").empty();		
				        			$("#paging").append(content.pageNumber);	
				        		  });
				        	  
				     		  });
						 
				      	   
				//		refreshRecord(data,"seagrass","old");
						/*
			        	$(".seagrassdateAsOf").val("");
			        	
						$(".seagrassprovince").val("");
			        	$(".seagrassmunicipality").empty();
			        	$(".seagrassbarangay").empty();
			        	$(".indicateGenus").val("")
			        	$(".culturedMethodUsed").val("");
			        	$(".culturedPeriod").val("");
			        	$(".volumePerCropping").val("");
			        	$(".noOfCroppingPerYear").val("");
			        	$(".seagrassarea").val("");
			        	$(".code").val("");
			        	$(".seagrassdataSource").val("");
			        	$(".seagrassremarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".image_src").val();
			        	$('.preview').attr('src', '');
			        	$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					}
				});
			});
		
	});	