	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
	            
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
	
		$(".submitMaricultureZone").click(function(e){
			e.preventDefault();
				var developerData = {};
				$(".se-pre-con").show();
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".zonedateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".mariculturezoneprovince").val();
	        	developerData["municipality"] =	$(".mariculturezonemunicipality").val();
	        	developerData["barangay"] = $(".mariculturezonebarangay").val();
	        	
	        	developerData["nameOfMariculture"] = $(".nameOfMariculture").val();
	        	developerData["area"] = $(".mariculturearea").val();
	        	developerData["dateLaunched"] = $(".dateLaunched").val();
	        	developerData["dateInacted"] = $(".dateInacted").val();
	        	developerData["eCcNumber"] = $(".eCcNumber").val();
	        	developerData["maricultureType"] = $(".maricultureType").val();
	        	developerData["municipalOrdinanceNo"] = $(".municipalOrdinanceNo").val();
	        	developerData["dateApproved"] = $(".dateApproved").val();
	        	
	        	developerData["individual"] = $(".individual").val();
	        	developerData["partnership"] = $(".partnership").val();
	        	developerData["cooperativeAssociation"] = $(".cooperativeAssociation").val();
	        	
	        	developerData["cageType"] = $(".zonecageType").val();
	        	developerData["cageSize"] = $(".cageSize").val();
	        	developerData["cageQuantity"] = $(".cageQuantity").val();
	        	developerData["speciesCultured"] = $(".mariculturespeciesCultured").val();
	        	developerData["quantityMt"] = $(".quantityMt").val();
	        	developerData["kind"] = $(".zonekind").val();
	        	
	        	developerData["productionPerCropping"] = $(".productionPerCropping").val();
	        	developerData["numberofCropping"] = $(".numberofCropping").val();
	        	developerData["species"] = $(".species").val();
	        	developerData["farmProduce"] = $(".farmProduce").val();
	        	developerData["productDestination"] = $(".productDestination").val();
	        	developerData["aquacultureProduction"] = $(".aquacultureProduction").val();
	        	developerData["code"] = $(".code").val();
	              	
	        	developerData["remarks"] = $(".maricultureremarks").val();
	        	developerData["dataSource"] = $(".mariculturedataSource").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);
				*/
	        	//add //
	        	
				if($(".zonedateAsOf").val().replace(/\s/g,"") == "" ||
				$(".mariculturezoneprovince").val().replace(/\s/g,"") == "" ||
	        	$(".mariculturezonemunicipality").val().replace(/\s/g,"") == "" ||
	        	$(".mariculturezonebarangay").val().replace(/\s/g,"") == "" ||
	        	$(".nameOfMariculture").val().replace(/\s/g,"") == "" ||
	        	$(".mariculturearea").val().replace(/\s/g,"") == "" ||
	        	$(".dateLaunched").val().replace(/\s/g,"") == "" ||
	        	$(".dateInacted").val().replace(/\s/g,"") == "" ||
	        	$(".eCcNumber").val().replace(/\s/g,"") == "" ||	        	
	        	$(".maricultureType").val().replace(/\s/g,"") == "" ||
	        	$(".municipalOrdinanceNo").val().replace(/\s/g,"") == "" ||
				$(".dateApproved").val().replace(/\s/g,"") == "" ||		        		
	        	$(".individual").val().replace(/\s/g,"") == "" ||
	        	$(".partnership").val().replace(/\s/g,"") == "" ||
	        	$(".cooperativeAssociation").val().replace(/\s/g,"") == "" ||
	        	$(".zonecageType").val().replace(/\s/g,"") == "" ||
	        	$(".cageSize").val().replace(/\s/g,"") == "" ||
	        	$(".cageQuantity").val().replace(/\s/g,"") == "" ||
	        	$(".mariculturespeciesCultured").val().replace(/\s/g,"") == "" ||
	        	$(".quantityMt").val().replace(/\s/g,"") == "" ||
	        	$(".zonekind").val().replace(/\s/g,"") == "" ||
	        	$(".productionPerCropping").val().replace(/\s/g,"") == "" ||
	        	$(".numberofCropping").val().replace(/\s/g,"") == "" ||
	        	$(".species").val().replace(/\s/g,"") == "" ||
	        	$(".farmProduce").val().replace(/\s/g,"") == "" ||
	        	$(".productDestination").val().replace(/\s/g,"") == "" ||
	        	$(".aquacultureProduction").val().replace(/\s/g,"") == "" ||
	        	$(".code").val().replace(/\s/g,"") == "" ||
	        	$(".mariculturedataSource").val().replace(/\s/g,"") == "" ||
	        	$(".lat").val().replace(/\s/g,"") == "" ||
	        	$(".lon").val().replace(/\s/g,"") == "" ){
					$(".se-pre-con").hide();
	        		alert("ALL FIELDS ARE REQUIRED");
	        		return false;
	        	}

	            
	        	//end //
		        	
		        	/* CHANGE END */
				var form = document.getElementById( "myform" );
	        	var json = ConvertFormToJSON(form);	
	        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getMaricultureZone",
					/* CHANGE END */
					
					data : json,
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						
						 var lastPage;
						 $.get("getLastPageZone/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        	
				         	 $.get("mariculturezone/" + lastPage, function(content, status){
				            	 var markup = "";
				             			for ( var x = 0; x < content.maricultureZone.length; x++) {
				             			
				             			   markup +="<tr data-href="+ content.maricultureZone[x].uniqueKey+"><td>"+content.maricultureZone[x].region+"</td><td>"+content.maricultureZone[x].province+"</td><td>"+content.maricultureZone[x].municipality+"</td><td>"+content.maricultureZone[x].barangay+"</td><td>"+content.maricultureZone[x].nameOfMariculture+"</td></tr>";
				    					}
				        				
				        				$("#export-buttons-table").find('tbody').empty();
				        				$("#export-buttons-table").find('tbody').append(markup);
				        			$("#paging").empty();		
				        			$("#paging").append(content.pageNumber);	
				        		  });
				     		  });
						 
				//refreshRecord(data,"mariculturezone","old");
						/*
			        	$(".zonedateAsOf").val("");
			  
						$(".mariculturezoneprovince").val("");
			        	$(".mariculturezonemunicipality").empty();
			        	$(".mariculturezonebarangay").empty();
			        	$(".nameOfMariculture").val("");
			        	$(".maricultureType").val("");
			        	$(".eCcNumber").val("");
			        	$(".individual").val("");
			        	$(".partnership").val("");
			        	$(".cooperativeAssociation").val("");
			        	$(".cageType").val("");
			        	$(".cageSize").val("");
			        	$(".cageQuantity").val("");
			        	$(".speciesCultured").val("");
			        	$(".quantityMt").val("");
			        	$(".kind").val("");
			        	$(".productionPerCropping").val("");
			        	$(".numberofCropping").val("");
			        	$(".species").val("");
			        	$(".farmProduce").val("");
			        	$(".productDestination").val("");
			        	$(".aquacultureProduction").val("");
			        	$(".code").val("");
			        	$(".mariculturearea").val("");
			        	$(".dateLaunched").val("");
			        	$(".dateInacted").val("");
			        	$(".dateApproved").val("");
			        	$(".remarks").val("");
			        	$(".dataSource").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".municipalOrdinanceNo").val("");
			        	$(".image_src").val("");
			        	$('.preview').attr('src', '');
			        	$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					return;
					}
				});
			});
	});		
		
	