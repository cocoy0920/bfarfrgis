	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
	            
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
		$(".submitFishProcessingPlant").click(function(e){
			e.preventDefault();
			$(".se-pre-con").show();
				var developerData = {};
				//var comment = $.trim($("#remarks").val());
	            //if(comment != ""){
	                // Show alert dialog if value is not blank
	             //   alert(comment);
	            //}
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".fishprocessingdateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fishprocessingplantprovince").val();
	        	developerData["municipality"] =	$(".fishprocessingplantmunicipality").val();
	        	developerData["barangay"] = $(".fishprocessingplantbarangay").val();
	        	developerData["nameOfProcessingPlants"] = $(".nameOfProcessingPlants").val();
	        	developerData["nameOfOperator"] = $(".nameOfOperator").val();
	        	developerData["area"] = $(".fishprocessingArea").val();
	        	developerData["operatorClassification"] = $(".operatorClassification").val();
	        	developerData["processingTechnique"] = $(".processingTechnique").val();
	        	developerData["processingEnvironmentClassification"] = $(".processingEnvironmentClassification").val();
	        	developerData["packagingType"] = $(".packagingType").val();
	        	developerData["businessPermitsAndCertificateObtained"] = $(".businessPermitsAndCertificateObtained").val();
	        	developerData["bfarRegistered"] = $(".bfarRegistered").val();
	        	developerData["plantRegistered"] = $(".plantRegistered").val();
	        	developerData["marketReach"] = $(".marketReach").val();
	        	developerData["indicateSpecies"] = $(".indicateSpecies").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["sourceOfData"] = $(".sourceOfData").val();
	        	developerData["remarks"] = $(".fishProcessingPlantRemarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);*/
	        	
	        	//add //

				if($(".fishprocessingdateAsOf").val().replace(/\s/g,"") == "" ||
			        	  $(".fishprocessingplantprovince").val().replace(/\s/g,"") == "" ||
			        	 $(".fishprocessingplantmunicipality").val().replace(/\s/g,"") == "" ||
			        	$(".fishprocessingplantbarangay").val().replace(/\s/g,"") == "" ||
			        	$(".code").val().replace(/\s/g,"") == "" ||
			        	 $(".nameOfProcessingPlants").val().replace(/\s/g,"") == "" ||
			        	 $(".nameOfOperator").val().replace(/\s/g,"") == "" ||
			        	 $(".fishprocessingArea").val().replace(/\s/g,"") == "" ||
			        	 $(".operatorClassification").val().replace(/\s/g,"") == "" ||
			        	 $(".processingTechnique").val().replace(/\s/g,"") == "" ||
			        	 $(".processingEnvironmentClassification").val().replace(/\s/g,"") == "" ||
			        	 $(".bfarRegistered").val().replace(/\s/g,"") == "" ||
			        	 $(".packagingType").val().replace(/\s/g,"") == "" ||
			        	 $(".marketReach").val().replace(/\s/g,"") == "" ||
			        	 $(".indicateSpecies").val().replace(/\s/g,"") == "" ||
			        	 $(".businessPermitsAndCertificateObtained").val().replace(/\s/g,"") == "" ||
			        	// $(".fishProcessingPlantRemarks").val().replace(/\s/g,"") == "" ||
			        	 $(".sourceOfData").val().replace(/\s/g,"") == "" ||
			        	 $(".lat").val().replace(/\s/g,"") == "" ||
			        	 $(".lon").val().replace(/\s/g,"") == ""){
			        	// $(".image").val().replace(/\s/g,"") == "" ||
			        	// $(".image_src").val().replace(/\s/g,"") == ""){
					$(".se-pre-con").hide();
			        		alert("ALL FIELDS ARE REQUIRED");
			        		return false;
			        	}

	            //end//
	        	
		        	
		        	/* CHANGE END */
		        
				var form = document.getElementById( "myform" );
	        	var json = ConvertFormToJSON(form);
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "getFishProcessingPlant",
					data : json,
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						 var lastPage;
						 $.get("getLastPageFP/", function(content, status){
				         	  
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        
				        	 $.get("fishprocessingplants/" + lastPage, function(content, status){
					         	 var markup = "";
					         			for ( var x = 0; x < content.fishProcessingPlant.length; x++) {
					         			   
					         			   markup +="<tr data-href="+ content.fishProcessingPlant[x].uniqueKey+"><td>"+content.fishProcessingPlant[x].region+"</td><td>"+content.fishProcessingPlant[x].province+"</td><td>"+content.fishProcessingPlant[x].municipality+"</td><td>"+content.fishProcessingPlant[x].barangay+"</td><td>"+content.fishProcessingPlant[x].nameOfProcessingPlants+"</td></tr>";
					   					}   	
					    				$("#export-buttons-table").find('tbody').empty();
					    				$("#export-buttons-table").find('tbody').append(markup);
					    			$("#paging").empty();		
					    			$("#paging").append(content.pageNumber);	
					     		  });
				     		  });
						 
				      	   	
				//	refreshRecord(data,"fishprocessingplants","new");	
						
			        	/*$(".fishprocessingdateAsOf").val("");
			        	
						$(".fishprocessingplantprovince").empty();
			        	$(".fishprocessingplantmunicipality").empty();
			        	$(".fishprocessingplantbarangay").val("");
			        	$(".nameOfProcessingPlants").val("");
			        	$(".nameOfOperator").val("");
			        	$(".fishprocessingArea").val("");
			        	$(".operatorClassification").val("");
			        	$(".processingTechnique").val("");
			        	$(".processingEnvironmentClassification").val("");
			        	$(".packagingType").val("");
			        	$(".businessPermitsAndCertificateObtained").val("");
			        	$(".bfarRegistered").val("");
			        	$(".plantRegistered").val("");
			        	$(".marketReach").val("");
			        	$(".indicateSpecies").val("");
			        	$(".code").val("");
			        	$(".sourceOfData").val("");
			        	$(".fishProcessingPlantRemarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".file").val("");
			        	$(".image").val("");
			        	$(".image_src").val("");
			        	$('.preview').attr('src', '');
			        	$('#plantData').hide(); 
			        	$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					return;
					}
				});
			});
	});