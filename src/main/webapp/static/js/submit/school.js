	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
	            
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
	            
		$(".submitSchoolOfFisheries").click(function(e){
			e.preventDefault();
				//var developerData = {};
				$(".se-pre-con").show();
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".schooldateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".schooloffisheriesprovince").val();
	        	developerData["municipality"] =	$(".schooloffisheriesmunicipality").val();
	        	developerData["barangay"] = $(".schooloffisheriesbarangay").val();
	        	developerData["name"] = $(".schoolname").val();
	        	developerData["dateEstablished"] = $(".dateEstablished").val();
	        	developerData["numberStudentsEnrolled"] = $(".numberStudentsEnrolled").val();
	        	developerData["area"] = $(".schoolarea").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".schooldataSource").val();
	        	developerData["remarks"] = $(".schoolremarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);*/
	        	
	        	//add //
				
	        	 if($(".schooldateAsOf").val().replace(/\s/g,"") == "" ||
				 $(".schooloffisheriesprovince").val().replace(/\s/g,"") == "" ||
	        	 $(".schooloffisheriesmunicipality").val().replace(/\s/g,"") == "" ||
	        	 $(".schooloffisheriesbarangay").val().replace(/\s/g,"") == "" ||
	        	 $(".schoolname").val().replace(/\s/g,"") == "" ||
	        	 $(".dateEstablished").val().replace(/\s/g,"") == "" ||
		         $(".numberStudentsEnrolled").val().replace(/\s/g,"") == "" ||
		         $(".schoolarea").val().replace(/\s/g,"") == "" ||
	        	 $(".code").val().replace(/\s/g,"") == "" ||
	        	 $(".schooldataSource").val().replace(/\s/g,"") == "" ||
	        	 $(".lat").val().replace(/\s/g,"") == "" ||
	        	 $(".lon").val().replace(/\s/g,"") == "" ){
	        		 $(".se-pre-con").hide(); 
	        		 alert("ALL FIELDS ARE REQUIRED");
		        		return false;
	        	 }
	        	//developerData["image"] = $(".image").val();
	        	//developerData["image_src"] = $(".image_src").val();
	        	//end //
	        	
		        	
		        	/* CHANGE END */
	        	 var form = document.getElementById( "myform" );
	         	var json = ConvertFormToJSON(form);	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getSchool",
					/* CHANGE END */
					
					data : json,
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						
						 var lastPage;
						 $.get("getLastPageSchool/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        	
				         	$.get("schooloffisheries/" + lastPage, function(content, status){
				           	 
				           	 var markup = "";
				            			for ( var x = 0; x < content.schoolOfFisheries.length; x++) {
				            			   markup +="<tr data-href="+ content.schoolOfFisheries[x].uniqueKey+"><td>"+content.schoolOfFisheries[x].region+"</td><td>"+content.schoolOfFisheries[x].province+"</td><td>"+content.schoolOfFisheries[x].municipality+"</td><td>"+content.schoolOfFisheries[x].barangay+"</td><td>"+content.schoolOfFisheries[x].name+"</td></tr>";
				   					}
				       				
				       				$("#export-buttons-table").find('tbody').empty();
				       				$("#export-buttons-table").find('tbody').append(markup);
				       			$("#paging").empty();		
				       			$("#paging").append(content.pageNumber);	
				       		  });
				     		  });
						 
				      	   
				//refreshRecord(data,"schooloffisheries","old");
						/*
			        	$(".schooldateAsOf").val("");
			        	
						$(".schooloffisheriesprovince").val("");
			        	$(".schooloffisheriesmunicipality").empty();
			        	$(".schooloffisheriesbarangay").empty();
			        	$(".schoolname").val("");
			        	$(".dateEstablished").val("");
				        $(".numberStudentsEnrolled").val(""); 
			        	$(".schoolarea").val("");
			        	$(".code").val("");
			        	$(".schooldataSource").val("");
			        	$(".schoolremarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".image_src").val();
			        	$('.preview').attr('src', '');
			        	$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					}
				});
			});
		
	});	