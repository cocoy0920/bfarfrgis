	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });

	            
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    				
		$(".submitFishLanding").click(function(e){
			e.preventDefault();
			
			$(".se-pre-con").show();
				//var developerData = {};
				
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".fishlandingdateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fishlandingprovince").val();
	        	developerData["municipality"] =	$(".fishlandingmunicipality").val();
	        	developerData["barangay"] = $(".fishlandingbarangay").val();
	        	developerData["nameOfLanding"] = $(".nameOfLanding").val();
	        	developerData["classification"] = $(".classification").val();
	        	developerData["volumeOfUnloadingMT"] = $(".volumeOfUnloadingMT").val();
	        	developerData["type"] = $(".fishLandingType").val();
	        	developerData["area"] = $(".fishLandingarea").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".dataSource").val();
	        	developerData["remarks"] = $(".remarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);
	        	*/
	        	//add //

				if($(".fishlandingdateAsOf").val().replace(/\s/g,"") == "" ||
	        	$(".region").val().replace(/\s/g,"") == "" ||
				$(".fishlandingprovince").val().replace(/\s/g,"") == "" ||
	        	$(".fishlandingmunicipality").val().replace(/\s/g,"") == "" ||
	        	$(".fishlandingbarangay").val().replace(/\s/g,"") == "" ||
	        	$(".nameOfLanding").val().replace(/\s/g,"") == "" ||
	        	$(".classification").val().replace(/\s/g,"") == "" ||
	        	$(".volumeOfUnloadingMT").val().replace(/\s/g,"") == "" ||
	        	$(".fishLandingType").val().replace(/\s/g,"") == "" ||
	        	$(".fishLandingarea").val().replace(/\s/g,"") == "" ||
	        	$(".code").val().replace(/\s/g,"") == "" ||
	        	$(".dataSource").val().replace(/\s/g,"") == "" ||
	        	//$(".remarks").val().replace(/\s/g,"") == "" ||
	        	$(".lat").val().replace(/\s/g,"") == "" ||
	        	$(".lon").val().replace(/\s/g,"") == "" ||
	        	$(".image_src").val().replace(/\s/g,"") == ""){
					$(".se-pre-con").hide();
	        		alert("ALL FIELDS ARE REQUIRED");
	        		return false;
	        	}

	            //end//
	        	
		        	
		        	/* CHANGE END */
				var form = document.getElementById( "myform" );
	        	var json = ConvertFormToJSON(form);
	        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "getFishLanding",
					data : json,
					dataType : 'json',				
					success : function(data) {
						 // FishLandingList(true); 
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						
						 var lastPage;
						 $.get("getLastPageFL/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        
				        	 $.get("fishlanding/" + lastPage, function(content, status){
					         		 
					        	 var markup = "";
					         			for ( var x = 0; x < content.fishLanding.length; x++) {
					         			   
					         			   markup +="<tr data-href="+ content.fishLanding[x].uniqueKey+"><td>"+content.fishLanding[x].region+"</td><td>"+content.fishLanding[x].province+"</td><td>"+content.fishLanding[x].municipality+"</td><td>"+content.fishLanding[x].barangay+"</td><td>"+content.fishLanding[x].nameOfLanding+"</td></tr>";
					   					}   	
					    				$("#export-buttons-table").find('tbody').empty();
					    				$("#export-buttons-table").find('tbody').append(markup);
					    			$("#paging").empty();		
					    			$("#paging").append(content.pageNumber);	
					     		  });
				     		  });
						 
				      	   
						
				//refreshRecord(data,"fishlanding","old");	
					/*	
			        	$(".fishlandingdateAsOf").val("");

						$(".fishlandingprovince").val("");
			        	$(".fishlandingmunicipality").empty();
			        	$(".fishlandingbarangay").empty();
			        	$("nameOfLanding").val("");
			        	$(".classification").val("");
			        	$(".volumeOfUnloadingMT").val("");
			        	$(".type").val("");
			        	$(".area").val("");
			        	$(".code").val("");
			        	$(".dataSource").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".image_src").val("");
			        	$('.preview').attr('src', '');
			        	$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					}
				});
			});
	});

	