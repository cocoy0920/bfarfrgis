	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });


	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
	            

		$(".submitFishPond").click(function(e){
			e.preventDefault();
			//	var developerData = {};
			$(".se-pre-con").show();	
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".fishponddateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fishpondprovince").val();
	        	developerData["municipality"] =	$(".fishpondmunicipality").val();
	        	developerData["barangay"] = $(".fishpondbarangay").val();
	        	developerData["area"] = $(".fishpondarea").val();
	        	developerData["nameOfFishPondOperator"] = $(".nameOfFishPondOperator").val();
	        	developerData["nameOfActualOccupant"] = $(".nameOfActualOccupant").val();
	        	developerData["fishPondType"] = $(".fishPondType").val();
	        	developerData["yearIssued"] = $(".yearIssued").val();
	        	developerData["yearExpired"] = $(".yearExpired").val();
	        	developerData["fishPondCondition"] = $(".fishPondCondition").val();
	        	developerData["nonActive"] = $(".nonActive").val();
	        	developerData["typeOfWater"] = $(".typeOfWater").val();
	        	//developerData["fishPond"] = $(".fishPond").val();
	        	developerData["speciesCultured"] = $(".fishpondspeciesCultured").val();
	        	developerData["status"] = $(".status").val();
	        	developerData["kind"] = $(".kind").val();
	        	developerData["hatchery"] = $(".hatchery").val();
	        	developerData["productPer"] = $(".productPer").val();
	        	developerData["dataSource"] = $(".fishponddataSource").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["remarks"] = $(".fishpondremarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);*/	        	
	        	
	        	//add //

				if($(".fishponddateAsOf").val().replace(/\s/g,"") == "" ||
			        	  $(".fishpondprovince").val().replace(/\s/g,"") == "" ||
			        	 $(".fishpondmunicipality").val().replace(/\s/g,"") == "" ||
			        	$(".fishpondbarangay").val().replace(/\s/g,"") == "" ||
			        	$(".code").val().replace(/\s/g,"") == "" ||
			        	 $(".nameOfFishPondOperator").val().replace(/\s/g,"") == "" ||
			        	 $(".nameOfActualOccupant").val().replace(/\s/g,"") == "" ||
			        	 $(".fishpondarea").val().replace(/\s/g,"") == "" ||
			        	 $(".fishPondType").val().replace(/\s/g,"") == "" ||
			        	 $(".typeOfWater").val().replace(/\s/g,"") == "" ||
			        	 $(".fishpondspeciesCultured").val().replace(/\s/g,"") == "" ||
			        	 $(".status").val().replace(/\s/g,"") == "" ||
			        	 $(".kind").val().replace(/\s/g,"") == "" ||
			        	 $(".hatchery").val().replace(/\s/g,"") == "" ||
			        	 $(".productPer").val().replace(/\s/g,"") == "" ||
			        	 $(".fishponddataSource").val().replace(/\s/g,"") == "" ||
			        	 $(".lat").val().replace(/\s/g,"") == "" ||
			        	 $(".lon").val().replace(/\s/g,"") == "" ){
					     $(".se-pre-con").hide();
			        		alert("ALL FIELDS ARE REQUIRED");
			        		return false;
			        	}

	            //end//
	        		 	        
	        			        	
		        	/* CHANGE END */
				var form = document.getElementById( "myform" );
	        	var json = ConvertFormToJSON(form);
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "getFishPond",
					data : json,
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
					
						 var lastPage;
						 $.get("getLastPageFPOND/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');

				        	 $.get("fishpond/" + lastPage, function(content, status){
				             	
				            	 var markup = "";
				             			for ( var x = 0; x < content.fishPond.length; x++) {
				             			   markup +="<tr data-href="+ content.fishPond[x].uniqueKey+"><td>"+content.fishPond[x].region+"</td><td>"+content.fishPond[x].province+"</td><td>"+content.fishPond[x].municipality+"</td><td>"+content.fishPond[x].barangay+"</td><td>"+content.fishPond[x].nameOfFishPondOperator+"</td></tr>";
				    					}
				        				$("#export-buttons-table").find('tbody').empty();
				        				$("#export-buttons-table").find('tbody').append(markup);
				        			$("#paging").empty();		
				        			$("#paging").append(content.pageNumber);	
				        		  });
				     		  });
						 
				      	   
						
				//refreshRecord(data,"fishpond","old");			
						/*
			        	$(".fishponddateAsOf").val("");
			        	
						$(".fishpondprovince").val("");
			        	$(".fishpondmunicipality").empty();
			        	$(".fishpondbarangay").empty();
			        	$(".fishpondarea").val("");
			        	$(".nameOfFishPondOperator").val("");
			        	$(".nameOfActualOccupant").val("");
			        	$(".typeOfWater").val("");
			        	//$(".fishPond").val("");
			        	$(".fishPondType").val("");
			        	$(".fishpondspeciesCultured").val("");
			        	$(".status").val("");
			        	$(".kind").val("");
			        	$(".hatchery").val("");
			        	$(".productPer").val("");
			        	$(".fishponddataSource").val("");
			        	$(".code").val("");
			        	$(".fishpondremarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".image_src").val("");
			        	$('.preview').attr('src', '');
			        	$('#nonActiveData').hide(); 
			        	$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					}
				});
	       });
	  });
		
	