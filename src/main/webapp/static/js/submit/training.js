	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
	            
		
		$(".submitTrainingCenter").click(function(e){
			e.preventDefault();
				var developerData = {};
				$(".se-pre-con").show();
				
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".trainingdateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fisheriestrainingcenterprovince").val();
	        	developerData["municipality"] =	$(".fisheriestrainingcentermunicipality").val();
	        	developerData["barangay"] = $(".fisheriestrainingcenterbarangay").val();
	        	developerData["name"] = $(".name").val();
	        	developerData["specialization"] = $(".specialization").val();
	        	developerData["facilityWithinTheTrainingCenter"] = $(".facilityWithinTheTrainingCenter").val();
	        	//developerData["area"] = $(".area").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["type"] = $(".fisheriestrainingcentertype").val();
	        	developerData["dataSource"] = $(".fisheriestrainingcenterdataSource").val();
	        	developerData["remarks"] = $(".fisheriestrainingcenterremarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);*/
	        	
	        	//add //
				
	        	 if($(".trainingdateAsOf").val().replace(/\s/g,"") == "" ||
				 $(".fisheriestrainingcenterprovince").val().replace(/\s/g,"") == "" ||
	        	 $(".fisheriestrainingcentermunicipality").val().replace(/\s/g,"") == "" ||
	        	 $(".fisheriestrainingcenterbarangay").val().replace(/\s/g,"") == "" ||
	        	 $(".name").val().replace(/\s/g,"") == "" ||
	        	 $(".specialization").val().replace(/\s/g,"") == "" ||
	        	 $(".facilityWithinTheTrainingCenter").val().replace(/\s/g,"") == "" ||
	        	 //$(".area").val().replace(/\s/g,"") == "" ||
	        	 $(".code").val().replace(/\s/g,"") == "" ||
	        	 $(".fisheriestrainingcentertype").val().replace(/\s/g,"") == "" ||
	        	 $(".fisheriestrainingcenterdataSource").val().replace(/\s/g,"") == "" ||
	        	// $(".fisheriestrainingcenterremarks").val().replace(/\s/g,"") == "" ||
	        	 $(".lat").val().replace(/\s/g,"") == "" ||
	        	 $(".lon").val().replace(/\s/g,"") == ""){
	        		 $(".se-pre-con").hide();
	     			
	        		 alert("ALL FIELDS ARE REQUIRED");
		        		return false;
		}
	        	//developerData["image"] = $(".image").val();
	        	//developerData["image_src"] = $(".image_src").val();
	        	//end //
	        	
		        	
		        	/* CHANGE END */
	        	 var form = document.getElementById( "myform" );
	         	var json = ConvertFormToJSON(form);	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getTraining",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						
						 var lastPage;
						 $.get("getLastPageTC/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        	
				         	$.get("trainingcenter/" + lastPage, function(content, status){
				           	 
				           	 var markup = "";
				            			for ( var x = 0; x < content.trainingCenter.length; x++) {

				            			   markup +="<tr data-href="+ content.trainingCenter[x].uniqueKey+"><td>"+content.trainingCenter[x].region+"</td><td>"+content.trainingCenter[x].province+"</td><td>"+content.trainingCenter[x].municipality+"</td><td>"+content.trainingCenter[x].barangay+"</td><td>"+content.trainingCenter[x].name+"</td></tr>";
				   					}
				       				
				       				$("#tableList").find('tbody').empty();
				       				$("#tableList").find('tbody').append(markup);
				       			$("#paging").empty();		
				       			$("#paging").append(content.pageNumber);	
				       		  });
				     		  });
						 
				//refreshRecord(data,"trainingcenter","old");
						/*
			        	$(".trainingdateAsOf").val("");

						$(".fisheriestrainingcenterprovince").val("");
			        	$(".fisheriestrainingcentermunicipality").empty();
			        	$(".fisheriestrainingcenterbarangay").empty();
			        	$(".name").val("");
			        	$(".specialization").val("");
			        	$(".facilityWithinTheTrainingCenter").val("");
			        	$(".code").val("");
			        	$(".fisheriestrainingcentertype").val("");
			        	$(".fisheriestrainingcenterdataSource").val("");
			        	$(".fisheriestrainingcenterremarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".image_src").val("");
			        	$('.preview').attr('src', '');
			        	$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					return;
					}
				});
			});
	});	
		