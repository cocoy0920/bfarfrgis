	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });

		
		$(".submitLGU").click(function(e){
			e.preventDefault();
				var developerData = {};
				
				/* CHANGE ONLY */
				developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".lgudateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".localgovernmentunitprovince").val();
	        	developerData["municipality"] =	$(".localgovernmentunitmunicipality").val();
	        	developerData["barangay"] = $(".localgovernmentunitbarangay").val();
	        	developerData["lguName"] = $(".lguName").val();
	        	developerData["dataSource"] = $(".lgudataSource").val();
	        	developerData["noOfBarangayCoastal"] = $(".noOfBarangayCoastal").val();
	        	developerData["noOfBarangayInland"] = $(".noOfBarangayInland").val();
	        	developerData["lguCoastalLength"] = $(".lguCoastalLength").val();
	        	//developerData["area"] = $(".area").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["remarks"] = $(".lguremarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);
				
	        	//add //
	        	
				if($(".lgudateAsOf").val().replace(/\s/g,"") == "" ||
				$(".region").val().replace(/\s/g,"") == "" ||
				$(".localgovernmentunitprovince").val().replace(/\s/g,"") == "" ||
	        	$(".localgovernmentunitmunicipality").val().replace(/\s/g,"") == "" ||
	        	$(".localgovernmentunitbarangay").val().replace(/\s/g,"") == "" ||
	        	$(".lguName").val().replace(/\s/g,"") == "" ||
	        	$(".lgudataSource").val().replace(/\s/g,"") == "" ||
	        	$(".noOfBarangayCoastal").val().replace(/\s/g,"") == "" ||
	        	$(".noOfBarangayInland").val().replace(/\s/g,"") == "" ||
	        	$(".lguCoastalLength").val().replace(/\s/g,"") == "" ||
	        	//$(".area").val().replace(/\s/g,"") == "" ||
	        	$(".code").val().replace(/\s/g,"") == "" ||
	        	//$(".lguremarks").val().replace(/\s/g,"") == "" ||
	        	 $(".lat").val().replace(/\s/g,"") == "" ||
	        	 $(".lon").val().replace(/\s/g,"") == "" ||
	        	// $(".image").val().replace(/\s/g,"") == "" ||
	        	 $(".image_src").val().replace(/\s/g,"") == ""){
	        		alert("ALL FIELDS ARE REQUIRED");
	        		return false;
	        	}

	            
	        	//end //
		        	
		        	/* CHANGE END */
		        	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getLGU",
					/* CHANGE END */
					
					data : JSON.stringify(developerData),
					dataType : 'json',				
					success : function(data) {
						
						alert("SUCCESSFULLY SAVE");
						 var lastPage;
						 $.get("getLastPageLGU/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        	
				         	$.get("lgu/" + lastPage, function(content, status){
				           	 
				           	 var markup = "";
				            			for ( var x = 0; x < content.lgu.length; x++) {
				            			   markup +="<tr data-href="+ content.lgu[x].uniqueKey+"><td>"+content.lgu[x].region+"</td><td>"+content.lgu[x].province+"</td><td>"+content.lgu[x].municipality+"</td><td>"+content.lgu[x].barangay+"</td><td>"+content.lgu[x].lguName+"</td></tr>";
				   					}
				       				$("#export-buttons-table").find('tbody').empty();
				       				$("#export-buttons-table").find('tbody').append(markup);
				       			$("#paging").empty();		
				       			$("#paging").append(content.pageNumber);	
				       		  });
				     		  });
						 
				      	   
					//refreshRecord(data,"lgu","old");
						
			        	$(".lgudateAsOf").val("");
			        	
						$(".localgovernmentunitprovince").val("");
			        	$(".localgovernmentunitmunicipality").empty();
			        	$(".localgovernmentunitbarangay").empty();
			        	$(".lgudataSource").val("");
			        	$(".noOfBarangayCoastal").val("");
			        	$(".noOfBarangayInland").val("");
			        	$(".lguCoastalLength").val("");
			        	//$(".area").val("");
			        	$(".code").val("");
			        	$(".lguremarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".image_src").val("");
			        	$('.preview').attr('src', '');
			        	$('.file').val("");
					},
					error: function(data){
					console.log("error", data);
					}
				});
			});
		
	});	