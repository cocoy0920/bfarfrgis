	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
	            
		$(".submitMangrove").click(function(e){
			e.preventDefault();
				var developerData = {};
				$(".se-pre-con").show();
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".mangrovedateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".mangroveprovince").val();
	        	developerData["municipality"] =	$(".mangrovemunicipality").val();
	        	developerData["barangay"] = $(".mangrovebarangay").val();
	        	developerData["indicateSpecies"] = $(".mangroveindicateSpecies").val();
	        	developerData["type"] = $(".mangrovetype").val();
	        	developerData["area"] = $(".mangrovearea").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".mangrovedataSource").val();
	        	developerData["remarks"] = $(".mangroveremarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);*/
				
	        	//add //
	        	
				if($(".mangrovedateAsOf").val().replace(/\s/g,"") == "" ||
				$(".region").val().replace(/\s/g,"") == "" ||
				$(".mangroveprovince").val().replace(/\s/g,"") == "" ||
	        	$(".mangrovemunicipality").val().replace(/\s/g,"") == "" ||
	        	$(".mangrovebarangay").val().replace(/\s/g,"") == "" ||
	        	$(".mangroveindicateSpecies").val().replace(/\s/g,"") == "" ||
	        	$(".mangrovetype").val().replace(/\s/g,"") == "" ||
	        	$(".mangrovearea").val().replace(/\s/g,"") == "" ||
	        	$(".code").val().replace(/\s/g,"") == "" ||
	        	$(".mangrovedataSource").val().replace(/\s/g,"") == "" ||
	        	//$(".mangroveremarks").val().replace(/\s/g,"") == "" ||
	        	$(".lat").val().replace(/\s/g,"") == "" ||
	        	 $(".lon").val().replace(/\s/g,"") == "" ||
	        	// $(".image").val().replace(/\s/g,"") == "" ||
	        	 $(".image_src").val().replace(/\s/g,"") == ""){
					$(".se-pre-con").hide();
	        		alert("ALL FIELDS ARE REQUIRED");
	        		return false;
	        	}

	            
	        	//end //
		        	
		        	/* CHANGE END */
				var form = document.getElementById( "myform" );
	        	var json = ConvertFormToJSON(form);	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getMangrove",
					/* CHANGE END */
					
					data : json,
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						
						 var lastPage;
						 $.get("getLastPageMangrove/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        	
				         	 $.get("mangrove/" + lastPage, function(content, status){
				           	  
				            	 var markup = "";
				             			for ( var x = 0; x < content.mangrove.length; x++) {

				             			   markup +="<tr data-href="+ content.mangrove[x].uniqueKey+"><td>"+content.mangrove[x].region+"</td><td>"+content.mangrove[x].province+"</td><td>"+content.mangrove[x].municipality+"</td><td>"+content.mangrove[x].barangay+"</td><td>"+content.mangrove[x].indicateSpecies+"</td></tr>";
				    					}
				        		
				        				$("#export-buttons-table").find('tbody').empty();
				        				$("#export-buttons-table").find('tbody').append(markup);
				        			$("#paging").empty();		
				        			$("#paging").append(content.pageNumber);	
				        		  });
				     		  });
						 
				      	   
						
				//	refreshRecord(data,"mangrove","old");
						/*
			        	$(".mangrovedateAsOf").val('');

						$(".mangroveprovince").val("");
			        	$(".mangrovemunicipality").empty();
			        	$(".mangrovebarangay").empty();
			        	$(".mangroveindicateSpecies").val("");
			        	$(".mangrovetype").val("");
			        	$(".mangrovearea").val("");
			        	$(".code").val("");
			        	$(".mangrovedataSource").val("");
			        	$(".mangroveremarks").val("");
			        	$(".encodedBy").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".image_src").val("");
			        	$('.preview').attr('src', '');
			        	$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					}
				});
			});
	});
		
		
	