	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
	            
		$(".submitMarket").click(function(e){
			e.preventDefault();
				//var developerData = {};
				$(".se-pre-con").show();
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".marketdateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".marketprovince").val();
	        	developerData["municipality"] =	$(".marketmunicipality").val();
	        	developerData["barangay"] = $(".marketbarangay").val();
	        	developerData["area"] = $(".marketarea").val();
	        	developerData["nameOfMarket"] = $(".nameOfMarket").val();
	        	developerData["publicprivate"] = $(".publicprivate").val();
	        	developerData["majorMinorMarket"] = $(".majorMinorMarket").val();
	        	developerData["volumeTraded"] = $(".volumeTraded").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["dataSource"] = $(".marketdataSource").val();
	        	developerData["remarks"] = $(".marketremarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);*/
	        	
	        	//add //
				if(
	        	 $(".marketdateAsOf").val().replace(/\s/g,"") == "" ||
				 $(".marketprovince").val().replace(/\s/g,"") == "" ||
	        	 $(".marketmunicipality").val().replace(/\s/g,"") == "" ||
	        	 $(".marketbarangay").val().replace(/\s/g,"") == "" ||
	        	 $(".marketarea").val().replace(/\s/g,"") == "" ||
	        	 $(".nameOfMarket").val().replace(/\s/g,"") == "" ||
	        	 $(".publicprivate").val().replace(/\s/g,"") == "" ||
	        	 $(".majorMinorMarket").val().replace(/\s/g,"") == "" ||
	        	 $(".volumeTraded").val().replace(/\s/g,"") == "" ||
	        	 $(".code").val().replace(/\s/g,"") == "" ||
	        	 $(".marketdataSource").val().replace(/\s/g,"") == "" ||
	        	// $(".marketremarks").val().replace(/\s/g,"") == "" ||
	        	 $(".lat").val().replace(/\s/g,"") == "" ||
	        	 $(".lon").val().replace(/\s/g,"") == "" ||
	        	 $(".image").val().replace(/\s/g,"") == "" ||
	        	 $(".image_src").val().replace(/\s/g,"") == ""){
					$(".se-pre-con").hide();
					alert("ALL FIELDS ARE REQUIRED");
	        		return false;
				}
	            
	        	//end //
	        	
		        	
		        	/* CHANGE END */
				var form = document.getElementById( "myform" );
	        	var json = ConvertFormToJSON(form);
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getMarket",
					/* CHANGE END */
					
					data : json,
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						 var lastPage;
						 $.get("getLastPageMarket/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        	
				         	 $.get("market/" + lastPage, function(content, status){
				             	
				            	 var markup = "";
				             			for ( var x = 0; x < content.market.length; x++) {				             			 
				             			   markup +="<tr data-href="+ content.market[x].uniqueKey+"><td>"+content.market[x].region+"</td><td>"+content.market[x].province+"</td><td>"+content.market[x].barangay+"</td><td>"+content.market[x].nameOfMarket+"</td></tr>";
				    					}
				             			
				        				$("#export-buttons-table").find('tbody').empty();
				        				$("#export-buttons-table").find('tbody').append(markup);
				        			$("#paging").empty();		
				        			$("#paging").append(content.pageNumber);	
				        		  });
				     		  });
						 
				      	   
				//refreshRecord(data,"market","old");
					/*	
			        	$(".marketdateAsOf").val("");

						$(".marketprovince").val("");
			        	$(".marketmunicipality").empty();
			        	$(".marketbarangay").empty();
			        	$(".marketarea").val("");
			        	$(".nameOfMarket").val("");
			        	$(".publicprivate").val("");
			        	$(".majorMinorMarket").val("");
			        	$(".volumeTraded").val("");
			        	$(".code").val("");
			        	$(".marketdataSource").val("");
			        	$(".marketremarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".image_src").val();
			        	$('.preview').attr('src', '');
			        	$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					}
				});
			});
	});	

