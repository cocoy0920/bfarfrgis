	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
		
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
	            
		$(".submitFishCoral").click(function(e){
			e.preventDefault();
				var developerData = {};
				$(".se-pre-con").show();
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".fishcoraldateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fishcoralprovince").val();
	        	developerData["municipality"] =	$(".fishcoralmunicipality").val();
	        	developerData["barangay"] = $(".fishcoralbarangay").val();
	        	developerData["area"] = $(".fishcoralarea").val();
	        	developerData["nameOfOperator"] = $(".fishcoralnameOfOperator").val();
	        	developerData["operatorClassification"] = $(".fishcoraloperatorClassification").val();
	        	developerData["nameOfStationGearUse"] = $(".nameOfStationGearUse").val();
	        	developerData["noOfUnitUse"] = $(".noOfUnitUse").val();
	        	developerData["fishesCaught"] = $(".fishesCaught").val();
	        	developerData["dataSource"] = $(".fishcoraldataSource").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["remarks"] = $(".fishcoralremarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);
	        	*/
	        	//add //

				if($(".fishcoraldateAsOf").val().replace(/\s/g,"") == "" ||
	        	$(".region").val().replace(/\s/g,"") == "" ||
				$(".fishcoralprovince").val().replace(/\s/g,"") == "" ||
	        	$(".fishcoralmunicipality").val().replace(/\s/g,"") == "" ||
	        	$(".fishcoralbarangay").val().replace(/\s/g,"") == "" ||
	        	$(".fishcoralarea").val().replace(/\s/g,"") == "" ||
	        	$(".fishcoralnameOfOperator").val().replace(/\s/g,"") == "" ||
	        	$(".fishcoraloperatorClassification").val().replace(/\s/g,"") == "" ||
	        	$(".nameOfStationGearUse").val().replace(/\s/g,"") == "" ||
	        	$(".noOfUnitUse").val().replace(/\s/g,"") == "" ||
	        	$(".fishesCaught").val().replace(/\s/g,"") == "" ||
	        	$(".fishcoraldataSource").val().replace(/\s/g,"") == "" ||
	        	$(".code").val().replace(/\s/g,"") == "" ||
	        	//$(".fishcoralremarks").val().replace(/\s/g,"") == "" ||
	        	$(".lat").val().replace(/\s/g,"") == "" ||
	        	 $(".lon").val().replace(/\s/g,"") == "" ||
	        	// $(".image").val().replace(/\s/g,"") == "" ||
	        	 $(".image_src").val().replace(/\s/g,"") == ""){
					$(".se-pre-con").hide();
	        		alert("ALL FIELDS ARE REQUIRED");
	        		return false;
	        	}
	            //end//
	        	
		        	
		        	/* CHANGE END */
				var form = document.getElementById( "myform" );
	        	var json = ConvertFormToJSON(form);	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "getFishCoral",
					data : json,
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						
						 var lastPage;
						 $.get("getLastPageFCoral/", function(content, status){
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
				        	
				         	$.get("fishcorals/" + lastPage, function(content, status){
				         		  
				           	 var markup = "";
				            			for ( var x = 0; x < content.fishCoral.length; x++) {
				            			
				            			   markup +="<tr data-href="+ content.fishCoral[x].uniqueKey+"><td>"+content.fishCoral[x].region+"</td><td>"+content.fishCoral[x].province+"</td><td>"+content.fishCoral[x].municipality+"</td><td>"+content.fishCoral[x].barangay+"</td><td>"+content.fishCoral[x].nameOfOperator+"</td></tr>";
				   					}
				       			
				       				$("#export-buttons-table").find('tbody').empty();
				       				$("#export-buttons-table").find('tbody').append(markup);
				       			$("#paging").empty();		
				       			$("#paging").append(content.pageNumber);	
				       		  });
				     		  });
						 
				      	   
				//refreshRecord(data,"fishcorals","old");	
						
			        	/*$(".fishcoraldateAsOf").val("");
			        	
						$(".fishcoralprovince").val("");
			        	$(".fishcoralmunicipality").empty();
			        	$(".fishcoralbarangay").empty();
			        	$(".fishcoralarea").val("");
			        	$(".fishcoralnameOfOperator").val("");
			        	$(".fishcoraloperatorClassification").val("");
			        	$(".nameOfStationGearUse").val("");
			        	$(".noOfUnitUse").val("");
			        	$(".fishesCaught").val("");
			        	$(".fishcoraldataSource").val("");
			        	$(".code").val("");
			        	$(".fishcoralremarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".image_src").val("");
			        	$('.preview').attr('src', '');
			        	$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					}
				});
			});
	
	});		