	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
	            
	            function ConvertFormToJSON(form){
	    			var array = jQuery(form).serializeArray();
	    			var json = {};
	    			
	    			jQuery.each(array, function() {
	    			
	    				json[this.name] = this.value || '';
	    			});
	    			
	    			return JSON.stringify(json);
	    		}
	    		
		$(".submitFishPen").click(function(e){
			e.preventDefault();
				//var developerData = {};
				$(".se-pre-con").show();
				/* CHANGE ONLY */
				/*developerData["id"] = $(".id").val();
				developerData["user"] = $(".user").val();
				developerData["uniqueKey"] = $(".uniqueKey").val();
				developerData["encodedBy"] = $(".encodedBy").val();
				developerData["editedBy"] = $(".editedBy").val();
				developerData["dateEncoded"] = $(".dateEncoded").val();
				developerData["dateEdited"] = $(".dateEdited").val();
	        	developerData["dateAsOf"] = $(".fishpendateAsOf").val();
	        	developerData["region"] = $(".region").val();
				developerData["province"] = $(".fishpenprovince").val();
	        	developerData["municipality"] =	$(".fishpenmunicipality").val();
	        	developerData["barangay"] = $(".fishpenbarangay").val();
	        	developerData["nameOfOperator"] = $(".fishPenNameOfOperator").val();
	        	developerData["area"] = $(".fishpenarea").val();
	        	developerData["noOfFishPen"] = $(".noOfFishPen").val();
	        	developerData["speciesCultured"] = $(".speciesCultured").val();
	        	developerData["code"] = $(".code").val();
	        	developerData["croppingStart"] = $(".croppingStart").val();
	        	developerData["croppingEnd"] = $(".croppingEnd").val();
	        	developerData["sourceOfData"] = $(".fishPenSourceOfData").val();
	        	developerData["remarks"] = $(".fishPenRemarks").val();
	        	developerData["lat"] = $(".lat").val();
	        	developerData["lon"] = $(".lon").val();
	        	developerData["image"] = $(".image").val();
	        	developerData["image_src"] = $(".image_src").val();
				console.log(developerData);*/	        	
	        	
	        	//add //

				if($(".fishpendateAsOf").val().replace(/\s/g,"") == "" ||
				$(".fishpenprovince").val().replace(/\s/g,"") == "" ||
	        	$(".fishpenmunicipality").val().replace(/\s/g,"") == "" ||
	        	$(".fishpenbarangay").val().replace(/\s/g,"") == "" ||
	        	$(".fishPenNameOfOperator").val().replace(/\s/g,"") == "" ||
	        	$(".fishpenarea").val().replace(/\s/g,"") == "" ||
	        	$(".noOfFishPen").val().replace(/\s/g,"") == "" ||
	        	$(".speciesCultured").val().replace(/\s/g,"") == "" ||
	        	$(".code").val().replace(/\s/g,"") == "" ||
	        	$(".croppingStart").val().replace(/\s/g,"") == "" ||
	        	$(".croppingEnd").val().replace(/\s/g,"") == "" ||
	        	$(".fishPenSourceOfData").val().replace(/\s/g,"") == "" ||
	        	$(".lat").val().replace(/\s/g,"") == "" ||
	        	$(".lon").val().replace(/\s/g,"") == ""){
					$(".se-pre-con").hide();
	        		alert("ALL FIELDS ARE REQUIRED");
	        		return false;
	        	}
		
	            //end//
	        	
		        	
		        	/* CHANGE END */
				var form = document.getElementById( "myform" );
	        	var json = ConvertFormToJSON(form);	
				$.ajax({
					type : "POST",
					contentType : "application/json",
					
					/* CHANGE DEFEND ON CLASS NAME */
					url : "getFishPen",
					/* CHANGE END */
					
					data : json,
					dataType : 'json',				
					success : function(data) {
						$(".se-pre-con").hide();
						alert("SUCCESSFULLY SAVE");
						
						 var lastPage;
						 $.get("getLastPageFPen", function(content, status){
				         	  
				         	 lastPage = content;
				         	lastPage = lastPage.replace(/^"(.*)"$/, '$1');
			
				        	 $.get("fishpen/" + lastPage, function(content, status){
				           	  
				            	 var markup = "";
				             			for ( var x = 0; x < content.fishPen.length; x++) {
				             			  
				             			   markup +="<tr data-href="+ content.fishPen[x].uniqueKey+"><td>"+content.fishPen[x].region+"</td><td>"+content.fishPen[x].province+"</td><td>"+content.fishPen[x].municipality+"</td><td>"+content.fishPen[x].barangay+"</td><td>"+content.fishPen[x].nameOfOperator+"</td></tr>";
				    					}
				        				
				        				$("#export-buttons-table").find('tbody').empty();
				        				$("#export-buttons-table").find('tbody').append(markup);
				        			$("#paging").empty();		
				        			$("#paging").append(content.pageNumber);	
				        		  });
				     		  });
						 
				      	   
				//refreshRecord(data,"fishpen","old");	
						
			       /* 	$(".fishpendateAsOf").val("");
			        	
						$(".fishpenprovince").val("");
			        	$(".fishpenmunicipality").empty();
			        	$(".fishpenbarangay").empty();
			        	$(".nameOfOperator").val("");
			        	$(".fishpenarea").val("");
			        	$(".noOfFishPen").val("");
			        	$(".speciesCultured").val("");
			        	$(".code").val("");
			        	$(".croppingStart").val("");
			        	$(".croppingEnd").val("");
			        	$(".sourceOfData").val("");
			        	$(".remarks").val("");
			        	$(".lat").val("");
			        	$(".lon").val("");
			        	$(".image").val("");
			        	$(".image_src").val("");
			        	$('.preview').attr('src', '');
			        	$('.file').val("");*/
						 document.getElementById("myform").reset();
						 var slctMunicipality=$('#municipality');
			        	  var selectBarangay=$('#barangay');
			        	  slctMunicipality.empty();
			        	  selectBarangay.empty();
			        	  $('.preview').attr('src', '');
					},
					error: function(data){
					console.log("error", data);
					$(".se-pre-con").hide();
					}
				});
			});
	});	
		