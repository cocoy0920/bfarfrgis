
      $(document).ready(function(){   
      
    var container = $('#myModalList .modal-dialog .modal-content .modal-body');
	  var trigger = $('.nav ul li a'); 
      trigger.on('click', function(){
    	  
          var $this = $(this),
            target = $this.data('target');       
          	 	 
        		 
       		   $.get("map/" + target, function(content, status){
        	 
         			refreshRecord(content,target,"new");
    	
    		  });
    	  
      	

          return false;
        });
        

 
///////////////////////////////////////////////////////////////////// 
    $(".fishsanctuaries_id a").live('click',function(e){   
     		id =  $(this).attr('id');    
     		console.log("ID: " + id);	
     	$.get("info/" + id, function(data, status){ 
     				//getdata.js      
     				
     				for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
					
						console.log("ID: " + obj.id);	
						 var markup = 
		        "<tr><th class=\"rowhead\" colspan=\"2\">FISH SANCTUARY LOCATION</th></tr>"+
	            "<tr><th>Region</th><td>" + obj.region + "</td></tr>"+ 
	            "<tr><th>Province</th><td>" + obj.province + "</td></tr>"+ 
	            "<tr><th>Municipality</th><td>" + obj.municipality + "</td></tr>"+
	            "<tr><th>Barangay</th><td>" + obj.barangay + "</td></tr>"+
	            "<tr><th class=\"rowhead\" colspan=\"2\">FISH SANCTUARY INFORMATION</th></tr>"+
	            "<tr><th>Name of Sanctuary/Protected Area:</th><td>"+obj.nameOfFishSanctuary+"</td></tr>"+
	            "<tr><th>Implementer:</th><td>&nbsp;</tdd></tr>"+
	            "<tr><th>BFAR</th><td>&nbsp;</td></tr>"+
	           "<tr><th>Name of Sheltered Fisheries/Species:</th><td>" + obj.sheltered + "</td></tr>"+
	           "<tr><th>DENR</th><td>&nbsp;</td></tr>"+
	           "<tr><th>Name of Sensitive Habitat with MPA:</th><td>" + obj.nameOfSensitiveHabitatMPA + "</td></tr>"+
	           "<tr><th>Ordinance No:</th><td>" + obj.OrdinanceNo + "</td></tr>"+
	          "<tr><th>Ordinance Title:</th><td>" + obj.OrdinanceTitle + "</td></tr>"+
	          "<tr><th>Data Established:</th><td>" + obj.DateEstablished + "</td></tr>"+
	          "<tr><th>Area:</th><td>" + obj.area + "</td></tr>"+
	          "<tr><th>Type:</th><td>" + obj.type + "</td></tr>"+
	           "<tr><th class=\"rowhead\" colspan=\"2\">ADDITIONAL INFORMATION</th></tr>"+
	          "<tr><th>Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
	          "<tr><th>Data Sources:</th><td>" + obj.fishSanctuarySource + "</td></tr>"+
	          "<tr><th>Remarks:</th><td>" + obj.remarks + "</td></tr>"+
	          "<tr><th class=\"rowhead\" colspan=\"2\">GEOGRAPHICAL LOCATION</th><td>&nbsp;</td></tr>"+
	          "<tr><th>Latitude:</th><td>" + obj.lat + "</td></tr>"+
	          "<tr><th>Longitude:</th><td>" + obj.lon + "</td></tr>"+
	          "<tr><td colspan=\"2\"><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td></tr>"; 
	         
	          var div = document.getElementById('info');
	            var tb = "<table id=\"example\">" + markup + "</table>"
	            div.innerHTML = tb;         
	            	}
     			
     	});
        	
    });
///////////////////////////////////////////////////////////////////// 
    
    
    $(".fishprocessingplants_id a").live('click',function(e){   
	id =  $(this).attr('id');
	
$.get("getFishProcessingplants/" + id, function(data, status){
  
  for ( var x = 0; x < data.length; x++) {
					var obj = data[x];

					var markup ="";
				    markup = 
				    "<tr><th scope=\"col\">FISH PROCESSING PLANT LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
				    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
				    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
				    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
				    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
				    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
				    "<tr><th scope=\"col\">FISH PROCESSING PLANT INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
				    "<tr><th scope=\"row\">Name of Processing Plant:</th><td>"+obj.nameOfProcessingPlants+"</td></tr>"+
				   "<tr><th scope=\"row\">Name of Operator:</th><td>" + obj.nameOfOperator + "</td></tr>"+
				   "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
				   "<tr><th scope=\"row\">Operator Classification:</th><td>" + obj.operatorClassification + "</td></tr>"+
				   "<tr><th scope=\"row\">Processing Technique:</th><td>" + obj.processingTechnique + "</td></tr>"+
				  "<tr><th scope=\"row\">Processing Environment Classification:</th><td>" + obj.processingEnvironmentClassification + "</td></tr>"+
				  "<tr><th scope=\"row\">BFAR Plant Registered:</th><td>" + obj.plantRegistered + "</td></tr>"+  
				  "<tr><th scope=\"row\">BFAR Registered:</th><td>" + obj.bfarRegistered + "</td></tr>"+
				  "<tr><th scope=\"row\">Packaging Type:</th><td>" + obj.packagingType + "</td></tr>"+
				  "<tr><th scope=\"row\">Market Reach:</th><td>" + obj.marketReach + "</td></tr>"+
				  "<tr><th scope=\"row\">Indicate Species:</th><td>" + obj.indicateSpecies + "</td></tr>"+
				  
				  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
				  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
				  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.sourceOfData + "</td></tr>"+
				  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
				 
				  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
				  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
				  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
				  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td><td>&nbsp;</td></tr>"; 
				 
					 var div = document.getElementById('info');
			            var tb = "<table id=\"example\">" + markup + "</table>"
			            div.innerHTML = tb;  
          	}
  

		  });
  });
///////////////////////////////////////////////////////////////////// 
    $(".fishlanding_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	 var markup = 
     $.get("getFishLanding/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						 markup = 
							    "<tr><th scope=\"col\">FISH LANDING LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
							    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
							    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
							    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
							    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
							    "<tr><th scope=\"col\">FISH LANDING INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							    "<tr><th scope=\"row\">Name of Lading:</th><td>"+obj.nameOfLanding+"</td></tr>"+
							   "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
							   "<tr><th scope=\"row\">Volume of Unloading MT:</th><td>" + obj.volumeOfUnloadingMT + "</td></tr>"+
							   "<tr><th scope=\"row\">Classification:</th><td>" + obj.classification + "</td></tr>"+
							   "<tr><th scope=\"row\">Type:</th><td>" + obj.type + "</td></tr>"+
							   
							   "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
							  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
							  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
							  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
							  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
							  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
							 
	            	}
        var div = document.getElementById('info');
        var tb = "<table id=\"example\">" + markup + "</table>"
        div.innerHTML = tb;  

    		  });
        }); 
///////////////////////////////////////////////////////////////////// 
    $(".fishpen_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getFishPen/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						 var markup = 
							    "<tr><th scope=\"col\">FISH PEN LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
							    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
							    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
							    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
							    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
							    "<tr><th scope=\"col\">FISH PEN INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							    "<tr><th scope=\"row\">Name of Operator:</th><td>"+obj.nameOfOperator+"</td></tr>"+
							   "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
							   "<tr><th scope=\"row\">No. of Fish Pen:</th><td>" + obj.noOfFishPen + "</td></tr>"+
							   "<tr><th scope=\"row\">Species Cultured:</th><td>" + obj.speciesCultured + "</td></tr>"+
							   "<tr><th scope=\"col\">Cropping</th><th scope=\"col\">&nbsp;</th></tr>"+
							   "<tr><th scope=\"row\">Cropping Start:</th><td>" + obj.croppingStart + "</td></tr>"+
							  "<tr><th scope=\"row\">Cropping End:</th><td>" + obj.croppingEnd + "</td></tr>"+
							  "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
							   "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
							  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.sourceOfData + "</td></tr>"+
							  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
							  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
							  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
							  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
							 
	            	}
        var div = document.getElementById('info');
        var tb = "<table id=\"example\">" + markup + "</table>"
        div.innerHTML = tb; 
    		  });
          
        });
///////////////////////////////////////////////////////////////////// 
    $(".fishcage_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	console.log("fishcage_id a: " + id);
     $.get("getFishCage/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						var markup = 
						    "<tr><th scope=\"col\">FISH CAGE LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
						    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
						    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
						    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
						    
						    "<tr><th scope=\"col\">FISH CAGE INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						    "<tr><th scope=\"row\">Name of Operator:</th><td>"+obj.nameOfOperator+"</td></tr>"+
						   "<tr><th scope=\"row\">Classification of Operator:</th><td>" + obj.classificationofOperator + "</td></tr>"+

						   "<tr><th scope=\"row\">Cage Dimension:</th><td>" + obj.cageDimension + "</td></tr>"+
						   "<tr><th scope=\"row\">Area:</th><td>" + obj.cageTotalArea + "</td></tr>"+
						  "<tr><th scope=\"row\">Cage Type:</th><td>" + obj.cageType + "</td></tr>"+
						  "<tr><th scope=\"row\">Cage No. of Compartments:</th><td>" + obj.cageNoOfCompartments + "</td></tr>"+
						  "<tr><th scope=\"row\">Indicate Species:</th><td>" + obj.indicateSpecies + "</td></tr>"+

						   "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
						  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.sourceOfData + "</td></tr>"+
						  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
						  
						  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
						  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
						  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 

	            	}
        var div = document.getElementById('info');
        var tb = "<table id=\"example\">" + markup + "</table>"
        div.innerHTML = tb;
    		  });
        });
///////////////////////////////////////////////////////////////////// 
    $(".fishpond_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getFishPond/" + id, function(data, status){
         for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						   var markup = 
						 "<tr><th scope=\"col\">FISH POND LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
						    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
						    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
						    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
						    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
						    "<tr><th scope=\"col\">FISH POND INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						    "<tr><th scope=\"row\">Name of Fishpond Lessee:</th><td>"+obj.nameOfFishPondOperator+"</td></tr>"+
						    "<tr><th scope=\"row\">Name of Actual Occupant:</th><td>" + obj.nameOfActualOccupant + "</td></tr>"+
						   "<tr><th scope=\"row\">Fish Pond Type:</th><td>" + obj.fishPondType + "</td></tr>"+
						   
						   "<tr><th scope=\"row\">Year Issued:</th><td>" + obj.yearIssued + "</td></tr>"+
						   "<tr><th scope=\"row\">Year Expired:</th><td>" + obj.yearExpired + "</td></tr>"+
						  "<tr><th scope=\"row\">Fish Pond Condition:</th><td>" + obj.fishPondCondition + "</td></tr>"+
						  "<tr><th scope=\"row\">if Non-Active:</th><td>" + obj.nonActive + "</td></tr>"+
						  "<tr><th scope=\"row\">Area (sqm.):</th><td>" + obj.area + "</td></tr>"+
						  
						  "<tr><th scope=\"row\">Type of Water:</th><td>" + obj.typeOfWater + "</td></tr>"+
						  "<tr><th scope=\"row\">Status:</th><td>" + obj.status + "</td></tr>"+
						  "<tr><th scope=\"row\">Species Commodities:</th><td>" + obj.speciesCultured + "</td></tr>"+
						  "<tr><th scope=\"row\">Kind:</th><td>" + obj.kind + "</td></tr>"+
						  "<tr><th scope=\"row\">Hatchery:</th><td>" + obj.hatchery + "</td></tr>"+
						  "<tr><th scope=\"row\">Product Per Cropping:</th><td>" + obj.productPer + "</td></tr>"+
						  
						  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
						  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
						  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
						  
						  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
						  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
						  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
						 
	            	}
         var div = document.getElementById('info');
         var tb = "<table id=\"example\">" + markup + "</table>"
         div.innerHTML = tb;
    		  });
        });
///////////////////////////////////////////////////////////////////// 
    $(".hatchery_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getHatchery/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						var markup = 
						    "<tr><th scope=\"col\">HATCHERY LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
						    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
						    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
						    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
						    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
						    "<tr><th scope=\"col\">HATCHERY INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						    "<tr><th scope=\"row\">Name of Hatchery:</th><td>"+obj.nameOfHatchery+"</td></tr>"+
						    "<tr><th scope=\"row\">Address of Operator:</th><td>" + obj.addressOfOperator + "</td></tr>"+
						    "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
						    "<tr><th scope=\"row\">Indicate Species:</th><td>" + obj.indicateSpecies + "</td></tr>"+
						    "<tr><th scope=\"row\">Type:</th><td>" + obj.type + "</td></tr>"+
						    "<tr><th scope=\"row\">Stocking Density/Cycle:</th><td>" + obj.prodStocking + "</td></tr>"+
						  "<tr><th scope=\"row\">Cycle/Year:</th><td>" + obj.prodCycle + "</td></tr>"+
						  "<tr><th scope=\"row\">Actual Production for the year:</th><td>" + obj.actualProdForTheYear + "</td></tr>"+
						  "<tr><th scope=\"col\">Month range of production cycle:</th><th scope=\"col\">&nbsp;</th></tr>"+
						  
						  "<tr><th scope=\"row\">Month Start:</th><td>" + obj.monthStart + "</td></tr>"+
						  "<tr><th scope=\"row\">Month End:</th><td>" + obj.monthEnd + "</td></tr>"+
						  

						  
						  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
						  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
						  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
						  
						  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
						  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
						  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
						 
	            	}
        var div = document.getElementById('info');
        var tb = "<table id=\"example\">" + markup + "</table>"
        div.innerHTML = tb;
    		  });
        });
///////////////////////////////////////////////////////////////////// 
    $(".iceplantorcoldstorage_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getIce/" + id, function(data, status){
                for ( var x = 0; x < data.length; x++) {
                	var obj = data[x];
                	var markup = 
                	    "<tr><th scope=\"col\">ICE PLANT/COLD STORAGE LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
                	    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
                	    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
                	    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
                	    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
                	    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
                	    "<tr><th scope=\"col\">ICE PLANT/COLD STORAGE INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
                	    "<tr><th scope=\"row\">Name of Ice Plant or Cold Storage:</th><td>"+obj.nameOfIcePlant+"</td></tr>"+
                	   "<tr><th scope=\"row\">Valid Sanitary Permit:</th><td>" + obj.sanitaryPermit + "</td></tr>"+
                	   "<tr><th scope=\"row\">Operator:</th><td>" + obj.operator + "</td></tr>"+
                	   "<tr><th scope=\"row\">Type of Facility:</th><td>" + obj.typeOfFacility + "</td></tr>"+
                	  "<tr><th scope=\"row\">Structure Type:</th><td>" + obj.structureType + "</td></tr>"+
                	  "<tr><th scope=\"row\">Capacity:</th><td>" + obj.capacity + "</td></tr>"+
                	  "<tr><th scope=\"row\">With Valid License to Operate:</th><td>" + obj.withValidLicenseToOperate + "</td></tr>"+
                	  "<tr><th scope=\"row\">With Valid Sanitary Permit:</th><td>" + obj.withValidSanitaryPermit + "</td></tr>"+
                	  "<tr><th scope=\"row\">Type Sanitary Permit:</th><td>" + obj.withValidSanitaryPermitYes + "</td></tr>"+
                	  "<tr><th scope=\"row\">Others:</th><td>" + obj.withValidSanitaryPermitOthers + "</td></tr>"+
                	  "<tr><th scope=\"row\">Business Permits and Certificate Obtained:</th><td>" + obj.businessPermitsAndCertificateObtained + "</td></tr>"+
                	  "<tr><th scope=\"row\">Type of Ice Maker:</th><td>" + obj.typeOfIceMaker + "</td></tr>"+
                	  "<tr><th scope=\"row\">Food Gradule:</th><td>" + obj.foodGradule + "</td></tr>"+
                	  "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
                	  
                	  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
                	  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
                	  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
                	  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
                	 
                	  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
                	  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
                	  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
                	  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
                	 
	            	}
                var div = document.getElementById('info');
                var tb = "<table id=\"example\">" + markup + "</table>"
                div.innerHTML = tb;
    		  });

        });
///////////////////////////////////////////////////////////////////// 
    $(".fishcorals_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getFishCoral/" + id, function(data, status){
        	 for ( var x = 0; x < data.length; x++) {
        		 var obj = data[x]; 
        		 var markup = 
        			    "<tr><th scope=\"col\">FISH CORAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        			    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
        			    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
        			    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
        			    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
        			    
        			    "<tr><th scope=\"col\">FISH CORAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        			    "<tr><th scope=\"row\">Name of Operator:</th><td>"+obj.nameOfOperator+"</td></tr>"+
        			   
        			   "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
        			   "<tr><th scope=\"row\">Operator Classification:</th><td>" + obj.operatorClassification + "</td></tr>"+
        			   "<tr><th scope=\"row\">Name of Station Gear Use:</th><td>" + obj.nameOfStationGearUse + "</td></tr>"+
        			  "<tr><th scope=\"row\">Name of Unite Use:</th><td>" + obj.noOfUnitUse + "</td></tr>"+
        			  "<tr><th scope=\"row\">Fishes Caught:</th><td>" + obj.fishesCaught + "</td></tr>"+
        			  
        			   "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        			  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
        			  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
        			  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
        			  
        			  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        			  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
        			  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
        			  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
        			 
        	 }
        	 var div = document.getElementById('info');
             var tb = "<table id=\"example\">" + markup + "</table>"
             div.innerHTML = tb;
    		  });
        });
/////////////////////////////////////////////////////////////////////         
    $(".FISHPORT_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getFishPort/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
        	var obj = data[x];
        	 var markup = 
        		    "<tr><th scope=\"col\">FISH PORT LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
        		    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
        		    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
        		    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
        		    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
        		    "<tr><th scope=\"col\">FISH PORT INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		    "<tr><th scope=\"row\">Name of Fish Port:</th><td>"+obj.nameOfFishport+"</td></tr>"+
        		  "<tr><th scope=\"row\">Name of Caretaker:</th><td>" + obj.nameOfCaretaker + "</td></tr>"+
        		   "<tr><th scope=\"row\">Type:</th><td>" + obj.type + "</td></tr>"+
        		   "<tr><th scope=\"row\">Classification:</th><td>" + obj.operatorClassification + "</td></tr>"+
        		  "<tr><th scope=\"row\">Area (sqm.):</th><td>" + obj.area + "</td></tr>"+
        		  "<tr><th scope=\"row\">Volume of Unloading:</th><td>" + obj.volumeOfUnloading + "</td></tr>"+

        		 
        		  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
        		  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
        		  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
        		  
        		  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
        		  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
        		  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
        		 
        }
        var div = document.getElementById('info');
        var tb = "<table id=\"example\">" + markup + "</table>"
        div.innerHTML = tb;
    		  });
        });
/////////////////////////////////////////////////////////////////////         
        
    $(".marineprotected_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getMarine/" + id, function(data, status){
    	 for ( var x = 0; x < data.length; x++) {
				var obj = data[x];

			    var markup = 
			    "<tr><th scope=\"col\">FISH SANCTUARY LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
			    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
			    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
			    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
			    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
			    "<tr><th scope=\"col\">FISH SANCTUARY INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
			    "<tr><th scope=\"row\">Name of Sanctuary/Protected Area:</th><td>"+obj.nameOfFishSanctuary+"</td></tr>"+
			    "<tr><th scope=\"row\">Implementer:</th><th scope=\"row\">&nbsp;</th></tr>"+
			    "<tr><th scope=\"col\">BFAR</th><th scope=\"col\">&nbsp;</th></tr>"+
			   "<tr><th scope=\"row\">Name of Sheltered Fisheries/Species:</th><td>" + obj.sheltered + "</td></tr>"+
			   "<tr><th scope=\"col\">DENR</th><th scope=\"col\">&nbsp;</th></tr>"+
			   "<tr><th scope=\"row\">Name of Sensitive Habitat with MPA:</th><td>" + obj.nameOfSensitiveHabitatMPA + "</td></tr>"+
			   "<tr><th scope=\"row\">Ordinance No:</th><td>" + obj.OrdinanceNo + "</td></tr>"+
			  "<tr><th scope=\"row\">Ordinance Title:</th><td>" + obj.OrdinanceTitle + "</td></tr>"+
			  "<tr><th scope=\"row\">Data Established:</th><td>" + obj.DateEstablished + "</td></tr>"+
			  "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
			  "<tr><th scope=\"row\">Type:</th><td>" + obj.type + "</td></tr>"+
			   "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
			  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
			  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.fishSanctuarySource + "</td></tr>"+
			  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
			  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
			  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
			  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
			  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
			 
    	 }
    	 var div = document.getElementById('info');
         var tb = "<table id=\"example\">" + markup + "</table>"
         div.innerHTML = tb;
     });

        });
/////////////////////////////////////////////////////////////////////         
         
    $(".market_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getMarket/" + id, function(data, status){
         for ( var x = 0; x < data.length; x++) {
        	 var obj = data[x]; 
        	 var markup = 
        		    "<tr><th scope=\"col\">MARKET LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
        		    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
        		    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
        		    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
        		    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
        		    "<tr><th scope=\"col\">MARKET INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		    "<tr><th scope=\"row\">Name of Market:</th><td>"+obj.nameOfMarket+"</td></tr>"+
        		    "<tr><th scope=\"row\">Type:</th><td>" + obj.type + "</td></tr>"+
        		   "<tr><th scope=\"row\">Classification of Market:</th><td>" + obj.majorMinorMarket + "</td></tr>"+
        		   "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
        		   "<tr><th scope=\"row\">Volume Traded:</th><td>" + obj.volumeTraded + "</td></tr>"+
        		   
        		  
        		  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
        		  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
        		  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
        		  
        		  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
        		  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
        		  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
        		 
         }	
         var div = document.getElementById('info');
         var tb = "<table id=\"example\">" + markup + "</table>"
         div.innerHTML = tb;
    		  });
        });
/////////////////////////////////////////////////////////////////////                 
          
    $(".schooloffisheries_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getSchool/" + id, function(data, status){
         for ( var x = 0; x < data.length; x++) {
        	 var obj = data[x]; 
        	 var markup = 
        		    "<tr><th scope=\"col\">FISHERIES LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
        		    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
        		    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
        		    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
        		    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
        		    "<tr><th scope=\"col\">FISHERIES INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		    "<tr><th scope=\"row\">Name of School:</th><td>"+obj.name+"</td></tr>"+
        		   "<tr><th scope=\"row\">Date Established:</th><td>" + obj.dateEstablished + "</td></tr>"+ 
        		   "<tr><th scope=\"row\">Number of Student Enrolled:</th><td>" + obj.numberStudentsEnrolled + "</td></tr>"+
        		 "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
        		 
        		  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
        		  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
        		  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
        		  
        		  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
        		  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
        		  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
        		 
         }
         var div = document.getElementById('info');
         var tb = "<<table id=\"example\">" + markup + "</table>"
         div.innerHTML = tb;
    		  });
        });
/////////////////////////////////////////////////////////////////////                 
                      
          
    $(".seagrass_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getSeagrass/" + id, function(data, status){
         for ( var x = 0; x < data.length; x++) {
        	 var obj = data[x];
        	 var markup = 
        		    "<tr><th scope=\"col\">SEAGRASS LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
        		    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
        		    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
        		    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
        		    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
        		    
        		    "<tr><th scope=\"col\">SEAGRASS INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		    "<tr><th scope=\"row\">Indicate Genus:</th><td>"+obj.indicateGenus+"</td></tr>"+
        		    
        		   "<tr><th scope=\"row\">Cultured Method Used:</th><td>" + obj.culturedMethodUsed + "</td></tr>"+

        		   "<tr><th scope=\"row\">Cultured Period:</th><td>" + obj.culturedPeriod + "</td></tr>"+
        		   "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
        		   "<tr><th scope=\"col\">PRODUCTION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		   
        		   "<tr><th scope=\"row\">Volume per Cropping (Kgs.):</th><td>" + obj.volumePerCropping + "</td></tr>"+
        		  "<tr><th scope=\"row\">No. of Cropping per Year:</th><td>" + obj.noOfCroppingPerYear + "</td></tr>"+
        		 
        		  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
        		  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.fishSanctuarySource + "</td></tr>"+
        		  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
        		 
        		  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
        		  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
        		  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
        		  
         }
         var div = document.getElementById('info');
         var tb = "<table id=\"example\">" + markup + "</table>"
         div.innerHTML = tb;
    		  });
        });
/////////////////////////////////////////////////////////////////////                 
                      
          
    $(".seaweeds_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getSeaweeds/" + id, function(data, status){
         for ( var x = 0; x < data.length; x++) {
        	 var obj = data[x];
        	 var markup = 
        		    "<tr><th scope=\"col\">SEAWEEDS LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
        		    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
        		    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
        		    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
        		    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
        		    
        		    "<tr><th scope=\"col\">SEAWEEDS INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		    "<tr><th scope=\"row\">Indicate Genus:</th><td>"+obj.indicateGenus+"</td></tr>"+
        		    
        		   "<tr><th scope=\"row\">Cultured Method Used:</th><td>" + obj.culturedMethodUsed + "</td></tr>"+

        		   "<tr><th scope=\"row\">Cultured Period:</th><td>" + obj.culturedPeriod + "</td></tr>"+
        		   "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
        		   "<tr><th scope=\"col\">Production</th><th scope=\"col\">&nbsp;</th></tr>"+
        		   
        		   "<tr><th scope=\"row\">Volume per Cropping (Kgs.):</th><td>" + obj.productionKgPerCropping + "</td></tr>"+
        		  "<tr><th scope=\"row\">No. of Cropping per Year:</th><td>" + obj.productionNoOfCroppingPerYear + "</td></tr>"+

        		   "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
        		  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
        		  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
        		 
        		  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
        		  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
        		  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
        		  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
        		 
         }
         var div = document.getElementById('info');
         var tb = "<table id=\"example\">" + markup + "</table>"
         div.innerHTML = tb;
    		  });
        });
/////////////////////////////////////////////////////////////////////                 
                      
           
    $(".mangrove_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getMangrove/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						 var markup = 
							    "<tr><th scope=\"col\">MANGROVE LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
							    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
							    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
							    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
							    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
							    "<tr><th scope=\"col\">MANGROVE INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							    "<tr><th scope=\"row\">Indicate Species:</th><td>"+obj.indicateSpecies+"</td></tr>"+
							   "<tr><th scope=\"row\">Type:</th><td>" + obj.type + "</td></tr>"+
							  "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
							  
							  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
							  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
							  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
							  
							  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
							  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
							  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
							 
	            }
        var div = document.getElementById('info');
        var tb = "<table id=\"example\">" + markup + "</table>"
        div.innerHTML = tb;
    		  });
        });
/////////////////////////////////////////////////////////////////////                 
                      
           
    $(".lgu_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getLGU/" + id, function(data, status){
         for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						 var markup = 
							    "<tr><th scope=\"col\">PROVINCIAL FISHERIES OFFICE LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
							    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
							    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
							    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
							    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
							    "<tr><th scope=\"col\">PROVINCIAL FISHERIES OFFICEINFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							    "<tr><th scope=\"row\">PFO Name:</th><td>"+obj.lguName+"</td></tr>"+
							    "<tr><th scope=\"col\">No. of Barangay</th><th scope=\"col\">&nbsp;</th></tr>"+
							    "<tr><th scope=\"row\">Coastal:</th><td>" + obj.noOfBarangayCoastal + "</td></tr>"+  
							   "<tr><th scope=\"row\">Inland:</th><td>" + obj.noOfBarangayInland + "</td></tr>"+
							   "<tr><th scope=\"row\">LGU Coastal Length:</th><td>" + obj.lguCoastalLength + "</td></tr>"+

							  
							  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
							  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
							  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
							 
							  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
							  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
							  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
							 
	            }
         var div = document.getElementById('info');
         var tb = "<table id=\"example\">" + markup + "</table>"
         div.innerHTML = tb;
    		  });

        });
/////////////////////////////////////////////////////////////////////                 
                      
           
    $(".trainingcenter_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getTraining/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						 var markup = 
							    "<tr><th scope=\"col\">FFISHERIES TRAINING CENTER LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
							    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
							    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
							    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
							    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
							    
							    "<tr><th scope=\"col\">FISHERIES TRAINING CENTER INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							    "<tr><th scope=\"row\">Name of Training Center:</th><td>"+obj.name+"</td></tr>"+
							   "<tr><th scope=\"row\">Specialization:</th><td>" + obj.specialization + "</td></tr>"+
							   "<tr><th scope=\"row\">Facility Within The Training Center:</th><td>" + obj.facilityWithinTheTrainingCenter + "</td></tr>"+
							   "<tr><th scope=\"row\">Type:</th><td>" + obj.type + "</td></tr>"+
							   
							  
							  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
							  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
							  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
							 
							  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
							  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
							  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
							  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td>&nbsp;<td></td></tr>"; 
							 
	            }
        var div = document.getElementById('info');
        var tb = "<table id=\"example\">" + markup + "</table>"
        div.innerHTML = tb;
    		  });

        });
/////////////////////////////////////////////////////////////////////                 
                      
           
    $(".mariculturezone_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getMaricultureZone/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						var markup = 
						    "<tr><th scope=\"col\">MARICULTURE ZONE/PARK LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						    "<tr><th scope=\"row\">Region</th><td>" + obj.region + "</td></tr>"+ 
						    "<tr><th scope=\"row\">Province</th><td>" + obj.province + "</td></tr>"+ 
						    "<tr><th scope=\"row\">Municipality</th><td>" + obj.municipality + "</td></tr>"+
						    "<tr><th scope=\"row\">Barangay</th><td>" + obj.barangay + "</td></tr>"+
						    "<tr><th scope=\"row\">Code:</th><td>"+obj.code+"</td></tr>"+
						    "<tr><th scope=\"col\">MARICULTURE ZONE/PARK INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						    "<tr><th scope=\"row\">Name of Mariculture:</th><td>"+obj.nameOfMariculture+"</td></tr>"+
						    "<tr><th scope=\"row\">Area:</th><td>" + obj.area + "</td></tr>"+
						   "<tr><th scope=\"row\">Date Launched:</th><td>" + obj.dateLaunched + "</td></tr>"+
						   "<tr><th scope=\"row\">Date Inacted:</th><td>" + obj.dateInacted + "</td></tr>"+
						   "<tr><th scope=\"row\">eCcNumber:</th><td>" + obj.eCcNumber + "</td></tr>"+
						  "<tr><th scope=\"row\">Mariculture Type:</th><td>" + obj.maricultureType + "</td></tr>"+
						  "<tr><th scope=\"col\">Municipal Ordinance</th><th scope=\"col\">&nbsp;</th></tr>"+
						  
						  "<tr><th scope=\"row\">Municipal Ordinance Number:</th><td>" + obj.municipalOrdinanceNo + "</td></tr>"+
						  "<tr><th scope=\"row\">Date Approved:</th><td>" + obj.dateApproved + "</td></tr>"+
						  "<tr><th scope=\"col\">Name of Inventors</th><th scope=\"col\">&nbsp;</th></tr>"+
						  
						  "<tr><th scope=\"row\">Individual:</th><td>" + obj.individual + "</td></tr>"+
						  "<tr><th scope=\"row\">Partnership:</th><td>" + obj.partnership + "</td></tr>"+
						  "<tr><th scope=\"row\">Cooperative Association:</th><td>" + obj.cooperativeAssociation + "</td></tr>"+
						  
						  "<tr><th scope=\"col\">Cage</th><th scope=\"col\">&nbsp;</th></tr>"+
						  
						  "<tr><th scope=\"row\">Type:</th><td>" + obj.cageType + "</td></tr>"+
						  "<tr><th scope=\"row\">Size:</th><td>" + obj.cageSize + "</td></tr>"+
						  "<tr><th scope=\"row\">Quality:</th><td>" + obj.cageQuantity + "</td></tr>"+
						  "<tr><th scope=\"row\">Species Cultured:</th><td>" + obj.speciesCultured + "</td></tr>"+
						  "<tr><th scope=\"row\">Quantity (mt.):</th><td>" + obj.quantityMt + "</td></tr>"+
						  "<tr><th scope=\"row\">Kind:</th><td>" + obj.kind + "</td></tr>"+
						  "<tr><th scope=\"row\">Production per Cropping (M.t):</th><td>" + obj.productionPerCropping + "</td></tr>"+
						  "<tr><th scope=\"row\">No. of Cropping per year:</th><td>" + obj.numberofCropping + "</td></tr>"+
						  
						  "<tr><th scope=\"col\">Prices and Production</th><th scope=\"col\">&nbsp;</th></tr>"+
						  "<tr><th scope=\"row\">Species:</th><td>" + obj.species + "</td></tr>"+
						  "<tr><th scope=\"row\">Farm Produce/Kg:</th><td>" + obj.farmProduce + "</td></tr>"+
						  "<tr><th scope=\"row\">Product destination for marketing monitoring:</th><td>" + obj.productDestination + "</td></tr>"+
						  "<tr><th scope=\"row\">Aquaculture production MT for the last 5 years:</th><td>" + obj.aquacultureProduction + "</td></tr>"+
						  
						  "<tr><th scope=\"col\">ADDITIONAL INFORMATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						  "<tr><th scope=\"row\">Date as of:</th><td>" + obj.dateAsOf + "</td></tr>"+
						  "<tr><th scope=\"row\">Data Sources:</th><td>" + obj.dataSource + "</td></tr>"+
						  "<tr><th scope=\"row\">Remarks:</th><td>" + obj.remarks + "</td></tr>"+
						 
						  "<tr><th  scope=\"col\">GEOGRAPHICAL LOCATION</th><th scope=\"col\">&nbsp;</th></tr>"+
						  "<tr><th scope=\"row\">Latitude:</th><td>" + obj.lat + "</td></tr>"+
						  "<tr><th scope=\"row\">Longitude:</th><td>" + obj.lon + "</td></tr>"+
						  "<tr align=\"left\"><td><img id=\"imgview\" src=\""+obj.image_src+"\"></img></td></tr>"; 

	            }
        var div = document.getElementById('info');
        var tb = "<table id=\"example\">" + markup + "</table>"
        div.innerHTML = tb;
    		  });

        });
/////////////////////////////////////////////////////////////////////                 
                    

      });