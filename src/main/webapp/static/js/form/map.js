   
    
   // var data =  ${maps};

    	var getMap = 'https://geo.bfar.da.gov.ph/wms';
    	var mapLayer = '${region_map}';
     	var map = L.map('mapContainer').setView(['${set_center}'], 7),
		
    	vector = L.geoJson().addTo(map);
    	 var wmsLayer = L.tileLayer.wms(getMap, {
    		layers: mapLayer,
    		attribution: "FIMC",
    		transparent: true,
    		maxZoom: 15
		}).addTo(map); 
         
      
          var legend = L.control({position: 'bottomright'});

         legend.onAdd = function (map) {

             var div = L.DomUtil.create('div', 'info legend'),
                 grades = [0, 10, 20, 50, 100, 200, 500, 1000],
                 labels = [];
                 div.innerHTML += " <img src='/frgis/static/images/nswe.jpeg' height='50' width='50'>";

             return div;
         };

         legend.addTo(map);
         
           var regionLegend = L.control({position: 'bottomleft'});

         regionLegend.onAdd = function (map) {

             var div = L.DomUtil.create('div', 'info legend'),
                 grades = [0, 10, 20, 50, 100, 200, 500, 1000],
                 labels = [];
                 div.innerHTML += '${region_name}' ;

             return div;
         };

         regionLegend.addTo(map);
  
            
                    	var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});
            
  
          var theMarker = new Array();
          var fishSanctuaryLayerGroup = false; 
          var fishprocessingplantsLayerGroup  = new Array();
          var getAllMaps = new Array();
       	var layerControl = false;
       	
 function refreshRecord(value,link)
    {

       data = value;
		
      
	for ( var i = 0; i < data.length; i++) {
		var obj = data[i];
		 
		
		var lat;
        var lon ;
        var icon;
        var html;
  		var role = '${role}';
  		var uniqueKey;
  		var page;
		
		page = obj.page;
		
			if(page == "fishsanctuaries"){
			
			
		
		markerFishProcessing();
		
			lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="fishsanctuaries_id"><a href="#" id="'+uniqueKey+'" class="btn btn-primary" >EDIT</a></div>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    var LamMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
    				theMarker.push(LamMarker);
    	
 	 	}
 	 	if(page == "fishprocessingplants"){
 	 	markerDelAgain();
 	
 	 	lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		    if (role == 'ROLE_USER') {
  							var edit = '<a href="edit/' + uniqueKey + '" class="btn btn-primary" >EDIT</a>';
					} else {
  							var edit = '';
					}
					
        		   
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		var fishprocessingplantsMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        		fishprocessingplantsLayerGroup.push(fishprocessingplantsMarker);
        		
        		
 	 	}
		    

  
	}

       	 
    }
    
    	function markerDelAgain() {
		for(i=0;i<theMarker.length;i++) {
    		map.removeLayer(theMarker[i]);
    }  
    }
    function markerFishProcessing() {
		for(i=0;i<fishprocessingplantsLayerGroup.length;i++) {
    		map.removeLayer(fishprocessingplantsLayerGroup[i]);
    }
	}
	
	 function getRecord(value,link)
    {

       data = value;
		
      
/*	for ( var i = 0; i < data.length; i++) {
		var obj = data[i];
		
		console.log("id" + obj.id);
    		  	
	}*/
}		