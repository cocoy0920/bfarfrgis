
const hiddenElements = `<input type="hidden" name="id" id="id" class="id">
				<input type="hidden" name="user" id="user" class="user"/>
				<input type="hidden" name="uniqueKey" id="uniqueKey" class="uniqueKey"/>
				<input type="hidden" name="dateEncoded" id="dateEncoded" class="dateEncoded"/>
				<input type="hidden" name="dateEdited" id="dateEdited" class="dateEdited" />
				<input type="hidden" name="encodedBy" id="encodedBy" class="encodedBy"/>
				<input type="hidden" name="editedBy" id="editedBy" class="editedBy" />
				<input type="hidden" name="region" id="region" class="region"/>	
				<input type="hidden" name="page" id="page" class="page"/>	
				
				<br>`
		
const locationElement = "<fieldset class='form-group border border-dark rounded p-4'>" +
			"<legend class='w-auto px-2 font-weight-bold bg-secondary text-white rounded'>FISH CAGE LOCATION</legend>" +
				"<div class='form-row'>"+
    			"<div class='col-md-4 mb-3'>" +
					"<label  for='province'>Province</label>" +	
					"<select name='province' id='province' class='form-control province' required></select>" +																
					"<div class='valid-feedback'>ok.</div>" +	
      				"<div class='invalid-feedback'>Please select a valid province.</div></div>" +	


    			"<div class='col-md-4 mb-3'>" +	
				"<label  for='municipality'>Municipality/City </label>" +	 		
				  "<select name='municipality' id='municipality' class='form-control municipality' required></select>" +		
				 "<div class='valid-feedback'>ok.</div>" +	
      				"<div class='invalid-feedback'>Please select a valid Municipality/City </div></div>" +	

    			"<div class='col-md-4 mb-3'>" +																
				"<label  for='barangay'>Barangay</label>" +	
					"<select name='barangay' id='barangay' class='form-control barangay' required></select>" +	
					"<div class='valid-feedback'>ok.</div>" +	
      				"<div class='invalid-feedback'>Please select a valid Barangay</div></div></div>" +	
			
			"<div class='form-row'><div class='col-md-12 mb-3'>" +			
			"<label for='code'>Code:</label><input type='text' name='code' id='code'  class='code' placeholder='Code' readonly='readonly'/></div></div></fieldset>";
			
const fishCageElements =`<fieldset class="form-group border border-dark rounded p-4">
			<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">FISH CAGE INFORMATION</legend>
			<div class="form-row">
    			<div class="col-md-4 mb-3">
			 <label  for="nameOfOperator"><span>Name of Operator:</span></label>
			 <input type="text" name="nameOfOperator"  id="nameOfOperator"  class="fishcagenameOfOperator form-control" required/>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-4 mb-3">
				<label  for="classificationofOperator"><span>Classification of Operator</span></label>				
				<select name="classificationofOperator"  id="classificationofOperator"  class="classificationofOperator form-control" required>
					
										<option value=""></option>
										<option value="Individual">Individual</option>
										<option value="Partnership">Partnership</option>
										<option value="Corporation">Corporation</option>
										<option value="Association">Association</option>
				</select>
			<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
			<div class="col-md-4 mb-3">
		<label  for="cageDimension"><span>Cage Dimension(LxWxH):</span></label>
					<input type="text" name="cageDimension"  id="cageDimension"  class="cageDimension form-control" required/>
		<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>	
		</div>
		</div>	
		<div class="form-row">
    			<div class="col-md-4 mb-3">	
				<label  for="area"><span>Area</span></label>
				<input type="text" name="area"  id="area"  class="area form-control" required/>	
				<!-- <input type="number" name="area" min="0" value="0" step="any"  id="area"  class="area form-control" required/> -->																
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
				<div class="col-md-4 mb-3">	
					<label  for="cageNoOfCompartments"><span>No. of Compartments:</span></label>
					<input type="number" name="cageNoOfCompartments"  id="cageNoOfCompartments"  class="cageNoOfCompartments form-control" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>
		</div>
		<div class="form-row">
				<div class="col-md-4 mb-3">	
				<label  for="cageType"><span>Cage Type:</span></label>
					<select name="cageType"  id="cageType"  class="cageType form-control" required>		
							<option value=""></option>
							<option value="Round/Circular">Round/Circular</option>
							<option value="Square">Square</option>
							<option value="Long/Rectangle">Long/Rectangle</option>										
					</select>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
				<div class="col-md-4 mb-3">	
				<label  for="cageType"><span>Cage Design:</span></label>
					<input type="text" name="cageDesign"  id="cageDesign"  class="cageDesign form-control" required>		
					<!-- 		<option value=""></option>
							<option value="Bamboo">Bamboo</option>
							<option value="High-density Polyetylene/HDPE">High-density Polyetylene/HDPE</option>
							<option value="Galvanized">Galvanized</option>										
					</select> -->
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
		</div>
		<div class="form-row">
    			<div class="col-md-12 mb-3">	
		<label  for="indicateSpecies"><span>Indicate Species:</span></label>
					<input type="text" name="indicateSpecies"  id="indicateSpecies"  class="fishcageindicateSpecies form-control" required/>
		<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
		</div>
		</div>
		</fieldset>`;
		
const additionalElement = `<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">ADDITIONAL INFORMATION</legend>
						<div class="form-row">

    			<div class="col-md-6 mb-3">	
					<label  for="dateAsOf">Data as of</label>
					<input type="date" name="dateAsOf" id="dateAsOf" class="form-control dateAsOf"  required>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
		<!-- 	<div class="col-md-6 mb-3">	
					<input type="text" name="startDate" id="startDate" class="date-picker"  data-date="" value="" />
			</div>-->

    			<div class="col-md-6 mb-3">	
				<label  for="validationCustom09">Data Source/s</label>
					<input type="text" name="sourceOfData"  id="validationCustom09"  class="form-control sourceOfData" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>

			</div>
		<div class="form-row">
    		<div class="col-md-12 mb-3">					
				<label  for="validationCustom10">Remarks</label>
					<textarea name="remarks" id="validationCustom10"  class="form-control remarks" required></textarea>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

		</div>
		</fieldset>`;
		
const geolocationElement = `<fieldset class="form-group border border-dark rounded p-4">
		<legend class="w-auto px-2 font-weight-bold bg-secondary text-white rounded">GEOGRAPHICAL LOCATION</legend>
			<div class="form-row">

    		<div class="col-md-6 mb-3">	
				<label  for="lat">Latitude</label>			
					<input type="text" name="lat"  readonly="readonly"   id="lat" class="form-control lat" required/>
					<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

    		<div class="col-md-6 mb-3">	
				<label  for="lon">Longitude</label>
				<input type="text" name="lon"  readonly="readonly"  id="lon" class="form-control lon" required/>
				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
			</div>

		</div>	
			<div class="form-row" id="image_form">
    			<div class="col-md-12 mb-3">				
			  		<label  for="photos">Captured Image</label>		
					<i class="fas fa-upload"></i><input type="file" id="photos" class="form-control file" accept="image/*"/>
    				<div class="valid-feedback">
        				ok.
      				</div>
      				<div class="invalid-feedback">
        				Required
      				</div>
				</div>
			</div>
     			</fieldset>`;
     			
  const saveElement = `<div id="img_preview" style="display: none">
     						<img id="preview" name="preview" class="preview" style="width: 450px; height: 200px"></img>
     				</div>
     						<input type="hidden" name="image_src" id="image_src" class="image_src" value="" /><br />
							<input type="hidden" name="image"  id="image" class="image"/>			
		
		 			`;
/*		 			<div class="btn-group" role="group" aria-label="Basic example">
					 <button type="button" id="submitFishCage" class="submitFishCage btn btn-primary">SUBMIT</button>
 					<button type="button" id="cancel" class="cancel btn btn-secondary">CANCEL</button>
					  </div>*/
	var buttonSave = document.createElement("button");
				buttonSave.innerHTML = "SUBMIT";				  
				buttonSave.className = "submitFishCage";	
	var buttonCancel = document.createElement("button");
				buttonCancel.innerHTML = "CANCEL";				  
				buttonCancel.className = "cancel";		
var forms = document.getElementsByClassName('needs-validation');				
var validation = Array.prototype.filter.call(forms, function(form) {
				
	buttonSave.addEventListener ("click", function() {
  	                    if (form.checkValidity() === false) {
                      event.preventDefault();
                      event.stopPropagation();
        
                      form.classList.add('was-validated'); 
                    }
                    if (form.checkValidity() === true) {
                        event.preventDefault();
                       buttonSave.disabled = true;
                        var array = jQuery(form).serializeArray();
                    	var json = {};

                    	jQuery.each(array, function() {

                    		json[this.name] = this.value || '';
                    	});
                    	
                    	var formData = JSON.stringify(json);
                    	
                    	sendingForm(formData);
                    	form.classList.remove('was-validated'); 
                      }
                    	
});	
});						  
function getFormsByResources(resource){
	console.log('getFormsByResources' + resource);
	switch (resource) {
		
	  case "fishcage":
	    document.getElementById("myform").innerHTML = hiddenElements + locationElement + fishCageElements + additionalElement + geolocationElement + saveElement;
	    document.getElementById("myform").appendChild(buttonSave);
	    document.getElementById("myform").appendChild(buttonCancel);
	    break;
	}
}


