  $(document).ready(function(){

    $('.bfardenr').change(function(){
        var bfardenr = $(this).val();
        html="";
        console.log(bfardenr);
   
   	 
        if(bfardenr == 'BFAR'){
        	 
        	$('#bfarData').show();
            $('#denrData').hide();
        	        	
        }
        if(bfardenr == 'DENR'){
        	$('#bfarData').hide();
            $('#denrData').show();
        }
    });
  
    $('.processingEnvironmentClassification').change(function(){
        var bfardenr = $(this).val();
        html="";
        console.log(bfardenr);
    	 
        if(bfardenr == 'Plant'){        	 
        	$('#plantData').show();        	        	
        }else{
        	$('#plantData').hide();  
        }

    });
  
    $('.fishPondType').change(function(){
        var bfardenr = $(this).val();
        html="";
        console.log(bfardenr);
    	 
        if(bfardenr == 'FLA'){        	 
        	$('#FLAData').show();        	        	
        }else{
        	$('#FLAData').hide(); 
        }

    });
  
    $('.fishPondCondition').change(function(){
        var bfardenr = $(this).val();
        html="";
        console.log(bfardenr);
    	 
        if(bfardenr == 'Non-Active'){        	 
        	$('#nonActiveData').show();        	        	
        }else{
        	$('#nonActiveData').hide(); 
        }

    });
  
    $('.withValidSanitaryPermit').change(function(){
        var bfardenr = $(this).val();
        html="";
        console.log(bfardenr);
    	 
        if(bfardenr == 'Yes'){        	 
        	$('#yesData').show(); 
        	$('#othersData').hide(); 
        }else{
        	$('#yesData').hide();
        	$('#othersData').hide(); 
        }

    });
    $('.withValidSanitaryPermitYes').change(function(){
        var bfardenr = $(this).val();
        html="";
        console.log(bfardenr);
    	 
        if(bfardenr == 'Others'){        	 
        	$('#othersData').show();        	        	
        }else{
        	$('#othersData').hide(); 
        }

    }); 
    
    $('.hatcheryoperatorClassification').change(function(){
        var operator = $(this).val();
        html="";
        console.log(operator);
    	 
        if(operator == 'BFAR'){        	 
        	$('#bfar_id').show();        	        	
        }else{
        	$('#bfar_id').hide(); 
        }

    }); 
});

