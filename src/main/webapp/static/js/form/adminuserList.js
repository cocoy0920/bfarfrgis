
 $(document).ready(function(){
	 var table = "<table class=\"table table-hover table-striped table-dark table-bordered\">" +   		
		"<thead><tr>" +
      	"<th>Firstname</th>" +
		"<th>Lastname</th>" +
		"<th>Email</th>" +
		"<th>Active/Inactive</th>" +
		"<th width=\"100\"></th>" +
		"</tr>" +
    	"</thead>" +
		"<tbody>"+
    	"</tbody></table>";

$.get("getAllUsers", function(data, status){
	
	html="";
	
	 for(var i=0; i<data.length; i++){
		 var obj = data[i];
		$("#view_alluser").empty();
		html = html+ "<tr>" +
				"<td>" + obj.firstName +"</td>" +
				"<td>" + obj.lastName +"</td>" +
				"<td>" + obj.email +"</td>" +
				"<td>" + obj.state +"</td>" +
				"<td><div class=\"userreg_id\">" +
				"<a href=\"#\" id='" +obj.ssoId+"' class=\"btn btn-primary\">EDIT</a>" +
				"</div>" +
				"</td>" +
				"</tr>";			
	}	
	 $('#view_alluser').append(table); 
	$('#view_alluser table tbody').append(html);
	console.log("content: " + content);
	});

$('.submitSearchAll').click(function(){
	  
   	var search = $('.search').val();
    
    
    if(search === ""){
    	alert("EMPTY");
    	return false;
    }else{	
    	console.log("search: " + search);
    	$.get("getSearch/" + search, function(data, status){
    		
    		html="";
        	
       	 for(var i=0; i<data.length; i++){
       		 var obj = data[i];
				$("#view_alluser").empty();
				html = html+ "<tr>" +
						"<td>" + obj.firstName +"</td>" +
						"<td>" + obj.lastName +"</td>" +
						"<td>" + obj.email +"</td>" +
						"<td>" + obj.state +"</td>" +
						"<td><div class=\"userreg_id\">" +
						"<a href=\"#\" id='" +obj.ssoId+"' class=\"btn btn-primary\">EDIT</a>" +
						"</div>" +
						"</td>" +
						"</tr>";			
   		}	
       	 $('#view_alluser').append(table); 
       	$('#view_alluser table tbody').append(html);	
   	 });
    }
});

$('.closeAdminList').click(function(){
	$('.search').val("");
	$.get("getAllUsers", function(data, status){
		
    	html="";
    	
    	 for(var i=0; i<data.length; i++){
    		 var obj = data[i];
			$("#view_alluser").empty();
			html = html+ "<tr>" +
					"<td>" + obj.firstName +"</td>" +
					"<td>" + obj.lastName +"</td>" +
					"<td>" + obj.email +"</td>" +
					"<td>" + obj.state +"</td>" +
					"<td><div class=\"userreg_id\">" +
					"<a href=\"#\" id='" +obj.ssoId+"' class=\"btn btn-primary\">EDIT</a>" +
					"</div>" +
					"</td>" +
					"</tr>";			
		}	
    	 $('#view_alluser').append(table); 
    	$('#view_alluser table tbody').append(html);
    	
	 	});

});
 });