
   			
      
    	  function newFishSanctuary(){
    		  var slctProvinces=$('.fishsanctuaryprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.fishsanctuarymunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.fishsanctuarybarangay');
        	  var BarangayOption="";
        	  
      		 $(".id").val("0");
		    $(".user").val("0");
		    $(".uniqueKey").val("0");
		    $(".dateEncoded").val("0");
		    $(".encodedBy").val("0");
		    
      		
      		slctProvinces.empty();
      		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
      		ProvinceOptions = ProvinceOptions + optionValue;         
      		slctProvinces.append(ProvinceOptions); 
	  		
	  		slctMunicipality.empty();
         			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
         			slctMunicipality.append(MunicipalityOption);
	            
			selectBarangay.empty();
				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
				selectBarangay.append(BarangayOption);

     
        	$(".nameOfFishSanctuary").val("");
        	$(".area").val("");
        	$(".bfardenr").val("");
        	$(".sheltered").val("");
        	$(".type").val("");
        	$(".code").val("");
        	$(".dateAsOf").val("");
        	$(".fishSanctuarySource").val("");
        	$(".remarks").val("");
        	$(".lat").val("");
        	$(".lon").val("");
        	$(".image_src").val("0");
        	$(".image").val("");
        	$(".preview").attr("src", "");
      		 
      		 
      		 $("#contentR").attr("data-target","#myFishSanctuaryModal");
      		 }
    	  
    	  function newFishprocessingplants(){
    		  var slctProvinces=$('.fishprocessingplantprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.fishprocessingplantmunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.fishprocessingplantbarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);


         
            	$(".nameOfProcessingPlants").val("");
            	$(".nameOfOperator").val("");
            	$(".operatorClassification").val("");
            	$(".processingTechnique").val("");
            	$(".processingEnvironmentClassification").val("");
            	$(".bfarRegistered").val("");          	
            	$(".packagingType").val("");
            	$(".marketReach").val("");
            	$(".indicateSpecies").val("");
            	$(".businessPermitsAndCertificateObtained").val("");            	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".sourceOfData").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	 $("#contentR").attr("data-target","#myFishProcessingPlants");
       
    	  }
    	  
    	  function newFishPen(){
    		  var slctProvinces=$('.fishpenprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.fishpenmunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.fishpenbarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfOperator").val("");             
                 	$(".noOfFishPen").val("");
                 	$(".speciesCultured").val("");
                 	$(".croppingStart").val("");
                 	$(".croppingEnd").val("");
                 	
            	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".sourceOfData").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	 $("#contentR").attr("data-target","#myFishPen");
          		 
    	  }
    	  
    	  function newFishLanding(){
    		  var slctProvinces=$('.fishlandingprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.fishlandingmunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.fishlandingbarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfLanding").val("");             
                 	$(".classification").val("");
                 	$(".volumeOfUnloadingMT").val("");
                 	$(".type").val("");
                 	$(".croppingEnd").val("");
                 	
            	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".dataSource").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myFishLanding");
          		 
    	  }
    	  function newFishCage(){
    		  var slctProvinces=$('.fishcageprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.fishcagemunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.fishcagebarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfOperator").val("");             
                 	$(".classificationofOperator").val("");
                 	$(".cageDimension").val("");
                 	$(".cageTotalArea").val("");
                 	$(".cageType").val("");
                 	$(".cageNoOfCompartments").val("");
                 	$(".indicateSpecies").val("");
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".sourceOfData").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myFishCage");    		 
          		 
    	  }
    	  
    	  function newFishPond(){
    		  var slctProvinces=$('.fishpondprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.fishpondmunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.fishpondbarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfFishPondOperator").val("");             
                 	$(".nameOfCaretaker").val("");
                 	$(".typeOfWater").val("");
                 	$(".fishPond").val("");
                 	$(".fishPondType").val("");
                 	$(".speciesCultured").val("");
                 	$(".status").val("");
                 	$(".kind").val("");
                 	$(".hatchery").val("");
                 	$(".productPer").val("");
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myFishPond");     		 
          		 
    	  }
    	  
    	  function newHatchery(){
    		  var slctProvinces=$('.hatcheryprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.hatcherymunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.hatcherybarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfHatchery").val("");             
                 	$(".nameOfOperator").val("");
                 	$(".addressOfOperator").val("");
                 	$(".operatorClassification").val("");
                 	$(".publicprivate").val("");
                 	$(".indicateSpecies").val("");
                 	$(".prodStocking").val("");
                 	$(".prodCycle").val("");
                 	$(".actualProdForTheYear").val("");
                 	$(".monthStart").val("");
                 	$(".monthEnd").val("");
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myHatchery");
      		 
          		 
    	  }
    	  
    	  function newIceplantorcoldstorage(){
    		  var slctProvinces=$('.iceplantorcoldstorageprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.iceplantorcoldstoragemunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.iceplantorcoldstoragebarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfIcePlant").val("");             
                 	$(".sanitaryPermit").val("");
                 	$(".operator").val("");
                 	$(".typeOfFacility").val("");
                 	$(".structureType").val("");
                 	$(".capacity").val("");
                 	$(".withValidLicenseToOperate").val("");
                 	$(".withValidSanitaryPermit").val("");
                 	$(".businessPermitsAndCertificateObtained").val("");
                 	$(".typeOfIceMaker").val("");
                 	$(".foodGradule").val("");
                 	$(".sourceOfEquipment").val("");
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myIcePlantorColdStorage");       		 
      		 
          		 
    	  }
    	  
    	  
    	  function newFishCoral(){
    		  var slctProvinces=$('.fishcoralprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.fishcoralmunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.fishcoralbarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".fishcoralnameOfOperator").val("");             
                 	$(".fishcoraloperatorClassification").val("");
                 	$(".nameOfStationGearUse").val("");
                 	$(".noOfUnitUse").val("");
                 	$(".fishesCaught").val("");
                 	$(".fishcoraldataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".fishcoralremarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myFishCorals");
    		 
          		 
    	  }
    	  
    	  function newFishPort(){
    		  var slctProvinces=$('.fishportprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.fishportmunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.fishportbarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfFishport").val("");             
                 	$(".nameOfCaretaker").val("");
                 	$(".operatorClassification").val("");
                 	$(".pfdaPrivateMunicipal").val("");
                 	$(".volumeOfUnloading").val("");
                 	$(".classification").val("");
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myFISHPORT");
     		 
          		 
    	  }
    	  function newMarineProtected(){
    		  var slctProvinces=$('.marineprotectedareaprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.marineprotectedareamunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.marineprotectedareabarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfMarineProtectedArea").val("");             
                 	$(".type").val("");
                 	$(".sensitiveHabitatWithMPA").val("");
                 	$(".dateEstablish").val("");
                 	$(".volumeOfUnloading").val("");
                 	$(".dataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".remarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myMarineProtected");
      		 
          		 
    	  }
    	  function newMarket(){
    		  var slctProvinces=$('.marketprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.marketmunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.marketbarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".nameOfMarket").val("");             
                 	$(".publicprivate").val("");
                 	$(".majorMinorMarket").val("");
                 	$(".volumeTraded").val("");
                 	$(".volumeOfUnloading").val("");
                 	$(".marketdataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".marketremarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#myMarket");
       		 
    	  }
    	  function newSchoolOfFisheries(){
    		  var slctProvinces=$('.schooloffisheriesprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.schooloffisheriesmunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.schooloffisheriesbarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				 
                 	$(".schoolname").val("");             
                 	$(".schooldataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".schoolremarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#mySchool");
       		 
    	  }
     	  
    	  function newSeagrass(){
    		  var slctProvinces=$('.seagrassprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.seagrassmunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.seagrassbarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

                 	$(".seagrassdataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".seagrassremarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#mySeagrass");
    		 
       		 
    	  }
    	  
    	  
    	  function newSeaweeds(){
    		  var slctProvinces=$('.seaweedsprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.seaweedsmunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.seaweedsbarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				$(".indicateGenus").val("");
    				$(".culturedMethodUsed").val("");
    				$(".culturedPeriod").val("");
    				$(".productionKgPerCropping").val("");
    				$(".productionNoOfCroppingPerYear").val("");
    				
                 	$(".seaweedsdataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".seaweedsremarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	$("#contentR").attr("data-target","#mySeaweeds");
       		 
    	  }
    	  
    	  
    	  function newMangrove(){
    		  var slctProvinces=$('.mangroveprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.mangrovemunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.mangrovebarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				$(".mangroveindicateSpecies").val("");
    				$(".mangrovetype").val("");
    				
                 	$(".mangrovedataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".mangroveremarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
    		  
            	 
      		 	$("#contentR").attr("data-target","#myMangrove");
      		       		 
    	  }
    	  
    	  function newLGU(){
    		  var slctProvinces=$('.localgovernmentunitprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.localgovernmentunitmunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.localgovernmentunitbarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				$(".lguName").val("");
    				$(".noOfBarangayCoastal").val("");
    				$(".noOfBarangayInland").val("");
    				$(".lguCoastalLength").val("");
                 	$(".lgudataSource").val("");
                 	
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".lguremarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
            	
            	$("#contentR").attr("data-target","#myLGU");
       		 
      		       		 
    	  }
    	  function newTrainingCenter(){
    		  var slctProvinces=$('.fisheriestrainingcenterprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.fisheriestrainingcentermunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.fisheriestrainingcenterbarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				$(".name").val("");
    				$(".specialization").val("");
    				$(".facilityWithinTheTrainingCenter").val("");
                 	$(".fisheriestrainingcenterdataSource").val("");
                 	$(".fisheriestrainingcentertype").val("");
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".fisheriestrainingcenterremarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
            	
            	$("#contentR").attr("data-target","#myTrainingCenter");
      		       		 
    	  }
    	  function newMaricultureZone(){
    		  var slctProvinces=$('.mariculturezoneprovince');
              var ProvinceOptions="";
        	  var slctMunicipality=$('.mariculturezonemunicipality');
        	  var MunicipalityOption="";
        	  var selectBarangay=$('.mariculturezonebarangay');
        	  var BarangayOption="";
    		  
    			$(".id").val("0");
    		    $(".user").val("0");
    		    $(".uniqueKey").val("0");
    		    $(".dateEncoded").val("0");
    		    $(".encodedBy").val("0");
    		    
    		    slctProvinces.empty();
          		ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
          		ProvinceOptions = ProvinceOptions + optionValue;         
          		slctProvinces.append(ProvinceOptions); 
    	  		
    	  		slctMunicipality.empty();
             			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
             			slctMunicipality.append(MunicipalityOption);
    	            
    			selectBarangay.empty();
    				BarangayOption = "<option value=''>-- PLS SELECT --</option>";
    				selectBarangay.append(BarangayOption);

    				$(".nameOfMariculture").val("");
    				$(".maricultureType").val("");
    				$(".eCcNumber").val("");
    				$(".individual").val("");
    				$(".partnership").val("");
    				$(".cooperativeAssociation").val("");
    				$(".cageType").val("");
    				$(".cageSize").val("");
    				$(".cageQuantity").val("");
    				$(".mariculturespeciesCultured").val("");
    				$(".quantityMt").val("");
    				$(".kind").val("");
    				$(".productionPerCropping").val("");
    				$(".numberofCropping").val("");
                 	$(".species").val("");
                 	$(".farmProduce").val("");
                 	$(".productDestination").val("");
                 	$(".aquacultureProduction").val("");
                 	$(".dateLaunched").val("");
                 	$(".dateInacted").val("");
                 	$(".dateApproved").val("");
                 	$(".mariculturepublicprivate").val("");
                 	$(".volumeTraded").val("");
                 	$(".accreditations").val("");
                 	$(".municipalOrdinanceNo").val("");
                 	$(".mariculturedataSource").val("");
            	$(".area").val("");
            	$(".code").val("");
            	$(".dateAsOf").val("");
            	$(".maricultureremarks").val("");
            	$(".lat").val("");
            	$(".lon").val("");
            	$(".image_src").val("0");
            	$(".image").val("");
            	$(".preview").attr("src", "");
            	
            	$("#contentR").attr("data-target","#myMaricultureZone");
     		 
    	  }
