  $(document).ready(function(){
  

	
	  var resources = null;
    var province = null;
    var municipal = null;
    var slctSubcat=null;
    var optionChanges=null;
    
    $('.region').change(function(){
        var region = $(this).val();
        console.log(province);
        if(region === ''){
        	     slctSubcat=$('.province'), optionChanges="";
                slctSubcat.empty();
                optionChanges = null;
                slctSubcat.append(optionChanges);
                
                slctSubcat=$('.municipality'), optionChanges="";
                slctSubcat.empty();
                optionChanges = null;
                slctSubcat.append(optionChanges);
        	return false;
        }
        $.ajax({
            type: 'GET',
            url: "getProvince/" + region,
            success: function(data){
            console.log(data);
               	slctSubcat=$('.province'), optionChanges="";
                slctSubcat.empty();
                optionChanges = "<option value=''>-- PLS SELECT --</option>";
                
                for(var i=0; i<data.length; i++){
                	optionChanges = optionChanges + "<option value='"+data[i].province_id + ","+data[i].province+"'>"+data[i].province + "</option>";
                }
                slctSubcat.append(optionChanges);
            },
            error:function(){
                alert("error");
            }

        });
    });
  
  $('.province').change(function(){
    var province = $(this).val();
    console.log(province);
    if(province === ''){
    	     slctSubcat=$('.municipality'), optionChanges="";
            slctSubcat.empty();
            optionChanges = null;
            slctSubcat.append(optionChanges);
    	return false;
    }
    $.ajax({
        type: 'GET',
        url: "getMunicipality/" + province,
        success: function(data){
        console.log(data);
           	slctSubcat=$('.municipality'), optionChanges="";
            slctSubcat.empty();
            optionChanges = "<option value=''>-- PLS SELECT --</option>";
            
            for(var i=0; i<data.length; i++){
            	optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
            }
            slctSubcat.append(optionChanges);
        },
        error:function(){
            alert("error");
        }

    });
});

  
  
  $('.create').click(function(){
  
   	var resources = $('.resources').val();
	var	region = $('.region').val();
   	var	province = $('.province').val();
   	var municipal = $('.municipality').val();
    console.log(province + '/' + municipal +'/' + resources);
    
    if(region === "" && province === null && municipal === null && resources === "all" ){
    	console.log("ALL EMPTY");
    	$.get("getAllResources", function(content, status){
        	console.log("getResources/"  + content);
         		filterRecord(content,"","new");	
         		printRecord(content,status);	
   	 });
    }else if(region === "" && province === null && municipal === null && resources != "all" ){
    console.log("RESOURCES ONLY WITH VALUE");
     	$.get("getAllbyResources/"+ resources, function(content, status){
        	console.log("getAllbyResources/"  + content);
        	filterRecord(content,resources,status);	
         		printRecord(content,status);	
   	 	});
    }else if(region != "" && province === "" && municipal === null && resources === "all" ){
    console.log("REGION ONLY WITH VALUE");
     	$.get("getResourcePerRegion/"+ region, function(content, status){
        	console.log("getResourcePerRegion/"  + content);
        	filterRecord(content,resources,status);	
         		printRecord(content,status);	
   	 	});
    }else if(region != "" && province === "" && municipal === null && resources != "all" ){
    console.log("RESOURCES ONLY WITH VALUE");
     	$.get("getResourceRegionPerPage/"+ region + '/' + resources, function(content, status){
        	console.log("getAllbyResources/"  + content);
        	filterRecord(content,resources,status);	
         		printRecord(content,status);	
   	 	});
    }else if(region != "" && province != "" && municipal === null || municipal === "" && resources === "all"){
    	console.log("MUNICIPALITY IS EMPTY");
    	$.get("getResourcesRegionPerProvince/" + region + '/' + province + '/'+ resources , function(content, status){
        	console.log("getResourcesRegionPerProvince/"  + "null" + region + "/" + province + '/' + resources);
        		filterRecord(content,resources,"new");	
         		printRecord(content,status);	
   	 	});
    }else if(municipal === "" && resources === "all" ){
    	console.log("MUNICIPALITY and Resources IS EMPTY");
    	$.get("getResources/" + province , function(content, status){
        	console.log("getResources/"  + content);
        	filterRecord(content,"","new");	
         		printRecord(content,status);	
   	 	});
    }else if(resources === "all" ){
    	console.log("RESOURCES IS EMPTY");
    	$.get("getResourcesRegionPerProvinceMunicipalResources/"+ region + '/' + province +'/' + municipal + '/' + resources  , function(content, status){
        	console.log("getResourcesRegionPerProvinceMunicipalResources/"  + content);
        		filterRecord(content,status);	
         		printRecord(content,"","new");	
   	 	});
    }else if(province !="" && municipal !="" && resources != "all" ){
    console.log("ALL  IS NOT EMPTY");
     	$.get("getResourcesRegionPerProvinceMunicipalResources/"+ region + '/' + province +'/' + municipal +'/' + resources, function(content, status){
        	console.log("getResourcesRegionPerProvinceMunicipalResources/"  + content);
        	filterRecord(content,resources,status);	
         		printRecord(content,status);	
   	 	});
    }
});
 
  $('a[data-jumpslide]').each(function(idx, ele){
    $(ele).on('click', function(){
          alert($(this).data("goto-page"));
       
    });
});


    $(".addUser").click(function(e){
          $('#myUserListModal').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
				
			$('#myUserFormModal').modal('show');
       
    });


   			
});

