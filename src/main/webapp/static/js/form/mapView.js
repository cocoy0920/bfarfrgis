    
//
//    	var getMap = 'https://geo.bfar.da.gov.ph/wms';
//    	var mapLayer = ${region_map};
//     	var map = L.map('mapContainer').setView([${set_center}], 7),
//		
    	vector = L.geoJson().addTo(map);
    	 var wmsLayer = L.tileLayer.wms("https://geo.bfar.da.gov.ph/wms", {
    		layers: mapLayer,
    		attribution: "FIMC",
    		transparent: true,
    		maxZoom: 15
		}).addTo(map); 
         
         
                    	var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});

  
  
          
          var fishSanctuaryLayerGroup = false; 
          
          var getAllMaps = new Array();
       	  var layerControl = false;
  
  //     	var role = ${role};		
  		var page;
  		var latestpage;
 function refreshRecord(value,link,status)
    {
console.log("value: " + value);
console.log("link: " + link);
console.log("status: " + status);

       data = value;
		page = link;

			if(page == "fishsanctuaries"){	
			//viewListOnMap.js				
				fishsanctuariesGetListMap(data,role);  	
				
 	 		}
 	 		if(page == "fishprocessingplants"){
 	 			fishprocessingplantsGetListMap(data,role); 	
      				
 	 		}
		 	if(page == "fishlanding"){
				fishlandingGetListMap(data,role);         		
 	 		}   
 	 	 	if(page == "fishpen"){
 	 	 		fishpenGetListMap(data,role);
 	 		} 

			if(page == "fishcage"){
				fishcageGetListMap(data,role);
			} 
 	 	 	if(page == "fishpond"){
 				fishpondGetListMap(data,role);
 			} 	 	        		
        	if(page == "hatchery"){
 	 			hatcheryGetListMap(data,role);
 	 		}
 	 	 	if(page == "iceplantorcoldstorage"){
 	 			iceplantorcoldstorageGetListMap(data,role);
 	 		}
 	 		if(page == "fishcorals"){
 	 			fishcorralsGetListMap(data,role);
 	 		}  
 	 		if(page == "FISHPORT"){
 	 			FISHPORTGetListMap(data,role);
 	 		}   
 	 		if(page == "marineprotected"){
 	 			marineprotectedGetListMap(data,role);
 	 		} 
 	 		if(page == "market"){ 
 	 			marketGetListMap(data,role);
 	 		} 
 	 		if(page == "schooloffisheries"){ 
 	 			schooloffisheriesGetListMap(data,role);
 	 		} 
 	 		if(page == "seagrass"){ 	 	
 	 			seagrassGetListMap(data,role);
 	 		} 
 	 		if(page == "seaweeds"){
        			seaweedsGetListMap(data,role);
 	 		}
 	 		if(page == "mangrove"){      		
 	 			mangroveGetListMap(data,role);
 	 		}
 	 		if(page == "lgu"){        		
        		lguGetListMap(data,role);
 	 		}
 	 		if(page == "trainingcenter"){
        		trainingcenterGetListMap(data,role);
 	 		}
 	 		if(page == "mariculturezone"){
 	 			mariculturezoneGetListMap(data,role);
 	 		}
     	 
    }
