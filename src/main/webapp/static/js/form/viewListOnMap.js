
          var fishSancturiesGroup = new Array();
          var fishprocessingplantsLayerGroup  = new Array();
          var fishlandingLayerGroup  = new Array();
          var fishpenLayerGroup  = new Array();
          var fishcageLayerGroup  = new Array();
          var fishpondLayerGroup  = new Array();
          var hatcheryLayerGroup  = new Array();
          var fishcageLayerGroup  = new Array();
          var iceplantLayerGroup  = new Array();
          var fishcoralLayerGroup  = new Array();
          var fishportLayerGroup  = new Array();
          var marineProtectedLayerGroup  = new Array();
          var marketLayerGroup  = new Array();
          var schoolOfFisheriesLayerGroup  = new Array();
          var seagrassLayerGroup  = new Array();
          var seaweedsLayerGroup  = new Array();
          var mangroveLayerGroup  = new Array();
          var lguLayerGroup  = new Array();
          var fisheriesTrainingLayerGroup  = new Array();
          var maricultureParkLayerGroup  = new Array();
          var AllLayerGroup  = new Array();

function fishsanctuariesGetListMap(data,role){	
	/*
	
	markerFishProcessingDel();
	markerMaricultureParkDel();
	markerFisheriesTrainingDel();
	markerLGUDel();
	markerMangroveDel();
	markerSeaWeedsDel();
	markerSeaGrassDel();
	markerSchoolOfFisheriesDel();
	markerMarketDel();
	markerMarineProtectedDel();
	markerFishPortDel();
	markerFishcorralDel();
	markerIcePlantDel();
	markerFishPondDel();
	markerHatcheryDel();
	markerFishProcessingDel();
	markerFishlandingDel();
	markerFishPenDel();
	markerFishCageDel();*/
	


	for ( var i = 0; i < data.length; i++) {
		var obj = data[i];
		var fishsanctuaries;	
		
			lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			var image = obj.image;
			var latlon = lat + ',' + lon;
			var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
			uniqueKey =  obj.uniqueKey;     
			
		    if (role == 'ROLE_USER') {
				var edit = '<div class="fishsanctuaries_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
				
		    } else {
						var edit = '';
			}
		    
		    var greenIcon = new LeafIcon({iconUrl: icon});
		    var LamMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
		    	
				console.log(this.getLatLng());
				onMapClick(this.getLatLng());
				getCoord(this.getLatLng());
				});
		    fishSancturiesGroup.push(LamMarker);

	}
	$(".se-pre-con").hide();
	
}

 function fishprocessingplantsGetListMap(data,role){
	 	

	 for ( var i = 0; i < data.length; i++) {		 
			var obj = data[i];
			
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;  
			var latlon = lat + ',' + lon;
			image = obj.image;
			//var imageview = "../../../../frgis_captured_images/" + image;
			var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
			if (role == 'ROLE_USER') {
				var edit = '<div class="fishprocessingplants_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
			} else {
						var edit = '';
			}
        		
        		    var greenIcon = new LeafIcon({iconUrl: icon});
        		    
        		//var fishprocessingplantsMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit);
        		var fishprocessingplantsMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    				console.log(this.getLatLng());
    				onMapClick(this.getLatLng());
    				getCoord(this.getLatLng());
    				});
        		fishprocessingplantsLayerGroup.push(fishprocessingplantsMarker);
        		
	 }		
	 $(".se-pre-con").hide();
		
 	 	}
 	 	
 function fishlandingGetListMap(data,role){

				
				for ( var i = 0; i < data.length; i++) {				 
					var obj = data[i];	
					console.log("fishlanding: " + obj.lon + ":" + obj.lat);
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   

        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		//var imageview = "../../../../frgis_captured_images/" + image;
       
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
    			if (role == 'ROLE_USER') {
    				var edit = '<div class="fishlanding_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
				
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		//var fishlandingMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit);
    		var fishlandingMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
		    	
				console.log(this.getLatLng());
				onMapClick(this.getLatLng());
				getCoord(this.getLatLng());
				});
    		fishlandingLayerGroup.push(fishlandingMarker);
          
				}	
				$(".se-pre-con").hide();
				
 	 	} 
 	function fishpenGetListMap(data,role){
 	
		 for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			image = obj.image;
			console.log("image: " + obj.image);
			uniqueKey =  obj.uniqueKey;  
		
        	var imageview = "../../../../frgis_captured_images/" + image;
        		var latlon = lat + ',' + lon;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="fishpen_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			
    		    } else {
							var edit = '';
				}
				
    		   
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		//var fishpenMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit);
    		var fishpenMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
		    	
				console.log(this.getLatLng());
				onMapClick(this.getLatLng());
				getCoord(this.getLatLng());
				});
    		fishpenLayerGroup.push(fishpenMarker);
		 }	
		 $(".se-pre-con").hide();
				
 	 	} 
 	function fishcageGetListMap(data,role){ 

		
		 for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		var imageview = "../../../../frgis_captured_images/" + image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="fishcage_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
				
    		   
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		//var fishcageMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit);
    		var fishcageMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
		    	
				console.log(this.getLatLng());
				onMapClick(this.getLatLng());
				getCoord(this.getLatLng());
				});    		
    		fishcageLayerGroup.push(fishcageMarker);
		 }	
		 $(".se-pre-con").hide();
					
 	 	} 
 	function fishpondGetListMap(data,role){ 
 	 	 
	
		
		for ( var i = 0; i < data.length; i++) {		 
			var obj = data[i];
		
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		//var imageview = "../../../../frgis_captured_images/" + image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="fishpond_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
				
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		//var fishpondMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit);
    		var fishpondMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
		    	
				console.log(this.getLatLng());
				onMapClick(this.getLatLng());
				getCoord(this.getLatLng());
				});
    		fishpondLayerGroup.push(fishpondMarker);
        		
			}	
		$(".se-pre-con").hide();
		
 	 	} 	
 	function hatcheryGetListMap(data,role){ 

			
		for ( var i = 0; i < data.length; i++) {		 
			var obj = data[i];
		
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		var imageview = "../../../../frgis_captured_images/" + image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="hatchery_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
				
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var hatcheryMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    				console.log(this.getLatLng());
    				onMapClick(this.getLatLng());
    				getCoord(this.getLatLng());
    				});
    		hatcheryLayerGroup.push(hatcheryMarker);
        		
		}	
		$(".se-pre-con").hide();
		
 	 	}
	function iceplantorcoldstorageGetListMap(data,role){
 	 
			
		for ( var i = 0; i < data.length; i++) {		 
			var obj = data[i];
		
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		var imageview = "../../../../frgis_captured_images/" + image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="iceplantorcoldstorage_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			
    		   } else {
							var edit = '';
				}
    		   var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var iceplantorcoldstorageMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    				console.log(this.getLatLng());
    				onMapClick(this.getLatLng());
    				getCoord(this.getLatLng());
    				});
    		iceplantLayerGroup.push(iceplantorcoldstorageMarker);
		}	
		$(".se-pre-con").hide();
			
 	 	}
	function fishcorralsGetListMap(data,role){

			
		for ( var i = 0; i < data.length; i++) {		 
			var obj = data[i];
		

				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		var imageview = "../../../../frgis_captured_images/" + image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="fishcorals_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
    		   
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var fishcorralsMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    				console.log(this.getLatLng());
    				onMapClick(this.getLatLng());
    				getCoord(this.getLatLng());
    				});
    		fishcoralLayerGroup.push(fishcorralsMarker);
		}	
		$(".se-pre-con").hide();
			
 	 	} 
	function FISHPORTGetListMap(data,role){

				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
			

 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		var imageview = "../../../../frgis_captured_images/" + image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="FISHPORT_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
    		   
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var FISHPORTMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    				console.log(this.getLatLng());
    				onMapClick(this.getLatLng());
    				getCoord(this.getLatLng());
    				});
    		fishportLayerGroup.push(FISHPORTMarker);
			}	
			$(".se-pre-con").hide();
				
 	 	}
	function marineprotectedGetListMap(data,role){
 	 	
 	 
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
			
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
           		var latlon = lat + ',' + lon;
           		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="marineprotected_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
				
    		   
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var marineprotectedMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    				console.log(this.getLatLng());
    				onMapClick(this.getLatLng());
    				getCoord(this.getLatLng());
    				});
    		marineProtectedLayerGroup.push(marineprotectedMarker);
			}	
			$(".se-pre-con").hide();
				
 	 	}
 	 function marketGetListMap(data,role){
 	 		
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
			
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
           		var latlon = lat + ',' + lon;
           		image = obj.image;
        		var imageview = "../../../../frgis_captured_images/" + image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="market_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
    		    
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var marketMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    				console.log(this.getLatLng());
    				onMapClick(this.getLatLng());
    				getCoord(this.getLatLng());
    				
    				});
    		marketLayerGroup.push(marketMarker);
        		
			}
			$(".se-pre-con").hide();
			
 	 	} 
 	 function schooloffisheriesGetListMap(data,role){
 	 		 
			
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
			
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
           		var latlon = lat + ',' + lon;
           		image = obj.image;
        		var imageview = "../../../../frgis_captured_images/" + image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="schooloffisheries_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			
    		   } else {
							var edit = '';
				}
    		    /*if (role == 'ROLE_USER') {
							var edit = '<div class="schooloffisheries_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >'+latlon+'</a></div>';
				} else {
							var edit = '';
				}*/
				
    		   
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var schooloffisheriesMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    				console.log(this.getLatLng());
    				onMapClick(this.getLatLng());
    				getCoord(this.getLatLng());
    				});
    		schoolOfFisheriesLayerGroup.push(schooloffisheriesMarker);
			}	
			$(".se-pre-con").hide();
				
 	 	} 
 	 function seagrassGetListMap(data,role){
 	 	
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
           		var latlon = lat + ',' + lon;
           		image = obj.image;
        		var imageview = "../../../../frgis_captured_images/" + image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="seagrass_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
    		    
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var seagrassMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    				console.log(this.getLatLng());
    				onMapClick(this.getLatLng());
    				getCoord(this.getLatLng());
    				});
    		seagrassLayerGroup.push(seagrassMarker);
        		
			}	
			$(".se-pre-con").hide();
			
 	 	} 
 	 function seaweedsGetListMap(data,role){

			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
        		var latlon = lat + ',' + lon;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="seaweeds_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
				
    		   
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		var seaweedsMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit);
    		//seaweedsMarker.on('mouseover', function() { seaweedsMarker.openPopup(); });
    		seaweedsLayerGroup.push(seaweedsMarker);
			}	
			$(".se-pre-con").hide();
				
 	 	}
	 function mangroveGetListMap(data,role){

				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		var imageview = "../../../../frgis_captured_images/" + image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="mangrove_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
    		   
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var mangroveMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    				console.log(this.getLatLng());
    				onMapClick(this.getLatLng());
    				getCoord(this.getLatLng());
    				});
    		mangroveLayerGroup.push(mangroveMarker);
			}	
			$(".se-pre-con").hide();
			
 	 	}
	 function lguGetListMap(data,role){

				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
				
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		var imageview = "../../../../frgis_captured_images/" + image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="lgu_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
    		    
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var lguMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    				console.log(this.getLatLng());
    				onMapClick(this.getLatLng());
    				getCoord(this.getLatLng());
    				});
    		lguLayerGroup.push(lguMarker);
			}	
			$(".se-pre-con").hide();
				
 	 	}
	 function trainingcenterGetListMap(data,role){
 	 	
 	
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
			
 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        		 
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		var imageview = "../../../../frgis_captured_images/" + image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="trainingcenter_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
    		    
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var trainingcenterMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    				console.log(this.getLatLng());
    				onMapClick(this.getLatLng());
    				getCoord(this.getLatLng());
    				});
    		fisheriesTrainingLayerGroup.push(trainingcenterMarker);
        		
			}	
			$(".se-pre-con").hide();
			
 	 	}
	 function mariculturezoneGetListMap(data,role){

 
				
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
			

 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			uniqueKey =  obj.uniqueKey;      		   
        	
        		var latlon = lat + ',' + lon;
        		image = obj.image;
        		var imageview = "../../../../frgis_captured_images/" + image;
        		var location = obj.province + ',' + obj.municipality + ',' + obj.barangay;
        		if (role == 'ROLE_USER') {
    				var edit = '<div class="mariculturezone_id"><a href="#" id="' + uniqueKey + '"><img class="imgicon" id="myImg" src="'+image+'" alt="'+location+'" style="width:100px;max-width:300px"></img></a></div>';
    			} else {
							var edit = '';
				}
    		    
    		    var greenIcon = new LeafIcon({iconUrl: icon});
    		    
    		    var mariculturezoneMarker = new L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(edit).on('click', function(e) {
    		    	
    				console.log(this.getLatLng());
    				onMapClick(this.getLatLng());
    				getCoord(this.getLatLng());
    				});
    		maricultureParkLayerGroup.push(mariculturezoneMarker);
			}	
			$(".se-pre-con").hide();
				
 	 	}
	
	function ListAllMap(data,role){
			var LeafIcon = L.Icon.extend({
				options: {
					shadowUrl: '',
					iconSize:     [38, 95],
					shadowSize:   [50, 64],
					iconAnchor:   [22, 94],
					shadowAnchor: [4, 62],
					popupAnchor:  [-3, -76]
				}
			});

			var fishprocessingIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/fishprocessing.png'}),
			fishsanctuaryIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/fishsanctuary.png'}),
			fishlandingIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/fish-landing.png'}),
			fishpenIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/fishpen.png'}),
			fishcageIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/fishcage.png'}),
			fishpondIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/fishpond.png'}),
			hatcheriesIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/hatcheries.png'}),
			fishportIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/fishport.png'}),
			fishcorralIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/fishcorral.png'}),
			marketIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/market.png'}),
			schooloffisheriesIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/schooloffisheries.png'}),
			seaweedsIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/seaweeds.png'}),
			mangroveIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/mangrove.png'}),
			fisheriestrainingIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/fisheriestraining.png'}),
			maricultureIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/mariculture.png'}),
			iceplantIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/iceplant.png'}),
			lguIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/lgu.png'}),
			seagrassIcon = new LeafIcon({iconUrl: '/frgis/static/images/pin/20x34/seagrass.png'});
			var allMarker;	
			for ( var i = 0; i < data.length; i++) {		 
				var obj = data[i];
			

 	 		lon = obj.lon;
			lat = obj.lat;
			icon = obj.icon;
			html = obj.html;
			page = obj.page;
			uniqueKey =  obj.uniqueKey;  
			var latlon = lat + ',' + lon;
        		    if (role == 'ROLE_USER') {
  							var edit = '<div class="mariculturezone_id"><a href="#" id="' + uniqueKey + '" class="btn btn-primary" >'+latlon+'</a></div>';
					} else {
  							var edit = '';
					}
					
        		    if(page == "fishsanctuaries"){	
        		    	
            		    
                		allMarker = L.marker([lat,lon], {icon: fishsanctuaryIcon}).addTo(map).bindPopup(edit);
                		allMarker.on('mouseover', function() { allMarker.openPopup(); });
                		AllLayerGroup.push(allMarker); 	
        					
        	 	 		}
        	 	 		if(page == "fishprocessingplants"){
        	 	 			
                		    
        	        		allMarker = L.marker([lat,lon], {icon: fishprocessingIcon}).addTo(map).bindPopup(edit);
        	        		AllLayerGroup.push(allMarker);	
        	      				
        	 	 		}
        			 	if(page == "fishlanding"){
        			 		allMarker = L.marker([lat,lon], {icon: fishlandingIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);    		
        	 	 		}   
        	 	 	 	if(page == "fishpen"){
        	 	 	 		
                		    
        	        		allMarker = L.marker([lat,lon], {icon: fishpenIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	 	 		} 

        				/*if(page == "fishcage"){
        					 allMarker = L.marker([lat,lon], {icon: fishcageIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        				} */
        	 	 	 	if(page == "fishpond"){
        	 	 	 		 allMarker = L.marker([lat,lon], {icon: fishpondIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	 			} 	 	        		
        	        	if(page == "hatchery"){
        	        		allMarker = L.marker([lat,lon], {icon: hatcheriesIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	 	 		}
        	 	 	 	if(page == "iceplantorcoldstorage"){
        	 	 	 		allMarker = L.marker([lat,lon], {icon: iceplantIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	 	 		}
        	 	 		if(page == "fishcorals"){
        	 	 			allMarker = L.marker([lat,lon], {icon: fishcorralIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	 	 		}  
        	 	 		if(page == "FISHPORT"){
        	 	 			allMarker = L.marker([lat,lon], {icon: fishportIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	 	 		}   
        	 	 		if(page == "marineprotected"){
        	 	 			allMarker = L.marker([lat,lon], {icon: greenIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	 	 		} 
        	 	 		if(page == "market"){ 
        	 	 			allMarker = L.marker([lat,lon], {icon: marketIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	 	 		} 
        	 	 		if(page == "schooloffisheries"){ 
        	 	 			allMarker = L.marker([lat,lon], {icon: schooloffisheriesIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	 	 		} 
        	 	 		if(page == "seagrass"){ 	 	
        	 	 			allMarker = L.marker([lat,lon], {icon: seagrassIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	 	 		} 
        	 	 		if(page == "seaweeds"){
        	 	 			
        	 	 			allMarker = L.marker([lat,lon], {icon: seaweedsIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	        		
        	 	 		}
        	 	 		if(page == "mangrove"){      		
        	 	 			 allMarker = L.marker([lat,lon], {icon: mangroveIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	 	 		}
        	 	 		if(page == "lgu"){        		
        	 	 			allMarker = L.marker([lat,lon], {icon: lguIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	 	 		}
        	 	 		if(page == "trainingcenter"){
        	 	 			 allMarker = L.marker([lat,lon], {icon: fisheriestrainingIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	 	 		}
        	 	 		if(page == "mariculturezone"){
        	 	 			allMarker = L.marker([lat,lon], {icon: maricultureIcon}).addTo(map).bindPopup(html + edit);
        	        		AllLayerGroup.push(allMarker);
        	 	 		}
        		    
			}	
			$(".se-pre-con").hide();
				
 	 	}
