 function getProvinceForUserRegistration(){
	 var slctProvinces=$('.province').find('option').remove().end();
	  var option="";
	  
	  slctProvinces.empty();
    option = "<option value=''>-- PLS SELECT--</option>";
    $.get("/create/getProvinceList", function(data, status){     	  			
						for( let prop in data ){						 					
								option = option + "<option value='"+data[prop].province_id+ "," + data[prop].province+"'>"+data[prop].province+"</option>";	           
						console.log("province id: " + data[prop].province_id + "::" + "province name: " + data[prop].province);	
						}
					 slctProvinces.append(option);
       		 	});
 }

 function getRregions(){
	 var slctRegions=$('.region').find('option').remove().end();
	 var slctProvinces=$('.provincemap');
	 var slctMunicipality=$('.municipality');
	  var option="";
	  var ProvinceOptions="";
	  var MunicipalityOption="";
	  slctRegions.empty();
	  
    option = "<option value=''>-- PLS SELECT--</option>";
    $.get("/create/getRegionList", function(data, status){     	  	
   	
						for( let prop in data ){						 					
								option = option + "<option value='"+data[prop].region_id+ "'>"+data[prop].region+"</option>";	           
							}
						slctRegions.append(option);
       		 	});
    slctProvinces.empty();
    ProvinceOptions = "<option value=''>-- PLS SELECT --</option>";
    slctProvinces.append(ProvinceOptions);
		
//    slctMunicipality.empty();
//		MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
//		slctMunicipality.append(MunicipalityOption);
	 
 }


function refreshLocation(){
	//$('.province').find('option').remove().end();
	  var slctProvinces=$('.province').find('option').remove().end();
      var ProvinceOptions="";
	  var slctMunicipality=$('.municipality');
	  var MunicipalityOption="";
	  var selectBarangay=$('.barangay');
	  var BarangayOption="";
	  var option="";
	  slctProvinces.empty();
     option = "<option value=''>-- PLS SELECT--</option>";
     $.get("/create/getProvinceList", function(data, status){  
    	
						for( let prop in data ){	
							var provinceName = data[prop].province;
								if(typeof provinceName !== "undefined"){
									option = option + "<option value='"+data[prop].province_id+ "," + data[prop].province+"'>"+data[prop].province+"</option>";	   
								}
							 
								        
							}
					 slctProvinces.append(option);
        		 	}); 
		
		slctMunicipality.empty();
 			MunicipalityOption = "<option value=''>-- PLS SELECT --</option>";
 			slctMunicipality.append(MunicipalityOption);
        
	selectBarangay.empty();
		BarangayOption = "<option value=''>-- PLS SELECT --</option>";
		selectBarangay.append(BarangayOption);
	 }

var pageName="";

function getPage(page){  	  
	  pageName = page;
}
	

 	function updateList(content){
 		var markup = "";
			for ( var y = 0; y < content.fishSanctuaries.length; y++) {
			   console.log("success: " +content.fishSanctuaries[y].id);
			  
			   markup +="<tr data-href="+ content.fishSanctuaries[y].uniqueKey+"><td>"+content.fishSanctuaries[y].region+"</td><td>"+content.fishSanctuaries[y].province+"</td><td>"+content.fishSanctuaries[y].municipality+"</td><td>"+content.fishSanctuaries[y].barangay+"</td><td>"+content.fishSanctuaries[y].nameOfFishSanctuary+"</td><td>"+content.fishSanctuaries[y].lat+","+content.fishSanctuaries[y].lon+"</td></tr>";
		}
		$("#export-buttons-table").find('tbody').empty();
		$("#export-buttons-table").find('tbody').append(markup);
	$("#paging").empty();		
	$("#paging").append(content.pageNumber); 
 	 }
 	
 function getProvinceMunicipalityBarangay(obj){
	 var slctProvice = $('.province'), optionProvince = "";
		slctProvice.empty();
		optionProvince = "<option value='" + obj.province_id + ","
		+ obj.province + "'>" + obj.province + "</option>";

	 $.get("/create/getProvinceList", function(data, status) {
			for ( let prop in data) {
				var getprovince = data[prop].province_id;
			
				if(typeof getprovince !== "undefined"){
					if (getprovince != obj.province_id) {
						optionProvince = optionProvince + "<option value='"
								+ data[prop].province_id + ","
								+ data[prop].province + "'>" + data[prop].province
								+ "</option>";
					}
				}
				
			}

			slctProvice.append(optionProvince);
		});

		var slctMunicipal = $('.municipality'), option = "";
		var municipal = obj.municipality_id;
		slctMunicipal.empty();
		option = "<option value='" + obj.municipality_id + ","
				+ obj.municipality + "'>" + obj.municipality + "</option>";

		$.get("/create/getMunicipalityList/" + obj.province_id, function(data, status) {
			for ( let prop in data) {
				var getMunicipal = data[prop].municipal_id;
				if(typeof getMunicipal !== "undefined"){
				if (getMunicipal != municipal) {
					option = option + "<option value='"
							+ data[prop].municipal_id + ","
							+ data[prop].municipal + "'>"
							+ data[prop].municipal + "</option>";
				}
				}
			}
			slctMunicipal.append(option);
		});

		var slctBarangay = $('.barangay'), brgyoption = "";
		var barangay = obj.barangay_id;
		slctBarangay.empty();
		brgyoption = "<option value='" + obj.barangay_id + "," + obj.barangay
				+ "'>" + obj.barangay + "</option>";

		$.get("/create/getBarangayList/" + obj.municipality_id, function(data, status) {
			
			for ( let prop in data) {
				var getbarangay = data[prop].barangay_id;
				if (getbarangay != barangay) {
					brgyoption = brgyoption + "<option value='"
							+ data[prop].barangay_id + ","
							+ data[prop].barangay + "'>" + data[prop].barangay
							+ "</option>";
				}

			}
			slctBarangay.append(brgyoption);
		});
	 
 }
 
 var clearProvince = $('.province');
 var clearMunicipality=$('.municipality');
 var clearOption = "<option value=''>-- PLS SELECT --</option>";
 var clearBarangay=$('.barangay');
 
 
 
 $('.region').change(function(){
	 var SelectedRegion = $(this).val();
	// console.log("SelectedRegion: " + SelectedRegion);
	//var provinceIndex = SelectedProvince.indexOf( ',' );
	var regionID = SelectedRegion;
	//console.log(regionID + " " + regionID);
	if(regionID === ""){
		clearProvince.empty();	
		clearProvince.append(clearOption);
		
		clearMunicipality.empty();
		clearMunicipality.append(clearOption);
	}else{
	$.ajax({
		type: 'GET',
		url: "/create/getProvince/" + regionID,
		success: function(data){
			var provinceList=$('.provincemap'), optionChanges="";
			//var provinceCheck = $('.provinceCheck'), checkChanges="";
			provinceList.empty();
			//provinceCheck.empty();
			optionChanges = "<option value=''>-- PLS SELECT --</option>";
			
			for(var i=0; i<data.length; i++){
				//console.log(data[i].province_id  +" : " + data[i].province);
				optionChanges = optionChanges + "<option value='"+data[i].province_id + "'>"+data[i].province + "</option>";
//				checkChanges = checkChanges + "<li class='dropdown-item'>" +
//				        					"<input type='checkbox' name='check_province' value="+data[i].province_id + "> "+ 	 		 
//					"<span class='text-white'>"+data[i].province +"</span></li>";
			}
//			checkChanges + "<li><button>click here</button></li>";
//			  	
//			provinceCheck.append(checkChanges);
			provinceList.append(optionChanges);
		},
		error:function(){
			alert("error");
		}

	});
	}
	});
 

$('.province').change(function(){
	console.log($('.province').val());
 var SelectedProvince = $(this).val();
// console.log("SelectedProvince: " + SelectedProvince);
var provinceIndex = SelectedProvince.indexOf( ',' );
var provinceID = SelectedProvince.substring(0, provinceIndex);
//console.log(provinceID + " " + provinceIndex);
if(SelectedProvince === ""){
	
	clearMunicipality.empty();
	clearMunicipality.append(clearOption);

   clearBarangay.append(clearOption);
  // validationOfInput(); 
  // $('.code').val("");
}else{
$.ajax({
	type: 'GET',
	url: "/create/getMunicipality/" + provinceID,
	success: function(data){
		var slctSubcat=$('.municipality'), optionChanges="";
		slctSubcat.empty();
		optionChanges = "<option value=''>-- PLS SELECT --</option>";

		for(var i=0; i<data.length; i++){
			optionChanges = optionChanges + "<option value='"+data[i].municipal_id + ","+data[i].municipal+"'>"+data[i].municipal + "</option>";
		}
		slctSubcat.append(optionChanges);
	},
	error:function(){
		alert("error");
	}

});
}
});



$('.municipality').change(function(){
   var municipal = $(this).val();
  if(municipal == ""){
	   slctSubbarangay.empty();
       optionChanges = "<option value=''>-- PLS SELECT --</option>";
       slctSubbarangay.append(optionChanges); 
  }else{
   $.ajax({
       type: 'GET',
       url: "/create/getBarangay/" + municipal,
       success: function(data){
           var slctSubcat=$('.barangay'), optionMunicipalChanges="";
           slctSubcat.empty();
           optionMunicipalChanges = "<option value=''>-- PLS SELECT --</option>";
           for(var i=0; i<data.length; i++){
           	optionMunicipalChanges = optionMunicipalChanges + "<option value='"+data[i].barangay_id + ","+data[i].barangay+"'>"+data[i].barangay + "</option>";
           }
           slctSubcat.append(optionMunicipalChanges);
       },
       error:function(){
           alert("error");
       }

   });
  }
});  



$('.barangay').change(function(){
   var barangay = $(this).val();
  // console.log(barangay);
   var getCode = $(".code").val();
   if(getCode == ""){
   	console.log("EMPTY");
   $.ajax({
       type: 'GET',
       url: "/create/code/" + barangay+"/" +pageName,
       success: function(data){
       $('.code').val(data);
          
       },
       error:function(){
          // alert("error");
       }

   });
   }
});  

 function changeMunicipality() {
	    var selectBox = document.getElementById("province");
	    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
	    alert(selectedValue);
	    
	    var slctMunicipal = $('.municipality'), option = "";
		//var municipal = obj.municipality_id;
		slctMunicipal.empty(); 
	    option = "<option value='select'>--- SELCECT ---/>";
		$.get("/create/getMunicipalityList/" + selectedValue, function(data, status) {
			for ( let prop in data) {
				//var getMunicipal = data[prop].municipal_id;
				//if (getMunicipal != municipal) {
					option = option + "<option value='"+ data[prop].municipal_id + "'>"+ data[prop].municipal + "</option>";
					console.log("option: " + option);
			//}
			}
			slctMunicipal.append(option);
			console.log("slctMunicipal.append(option): " + slctMunicipal);
		});
	   }
 
 function changeBarangay() {
	    var selectBox = document.getElementById("municipality");
	    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
	    alert(selectedValue);
	    
	    var slctBarangay = $('.barangay'), brgyoption = "";
		var barangay = obj.barangay_id;
		slctBarangay.empty();
		

		$.get("/create/getBarangayList/" + selectedValue, function(data, status) {
			//console.log("getBarangayList: " + data);
			for ( let prop in data) {
				var getbarangay = data[prop].barangay_id;
				//if (getbarangay != barangay) {
					brgyoption = brgyoption + "<option value='"
							+ data[prop].barangay_id + "'>" + data[prop].barangay
							+ "</option>";
				//}

			}
			slctBarangay.append(brgyoption);
		});
	   }

 
 $('.lat').change(function(){
	    var latValue = $(this).val();
	  
	    if(latValue == ''){
	    	return;
	    }
	    $.ajax({
	        type: 'GET',
	        url: "/create/latValue/" + latValue,
	        success: function(data){
	        console.log(data);
	        	if(data === 'invalid'){
	        		 alert("INVALID LATITUDE");
	        		$('.lat').val("");
	        	}else{
	        	//	$('.lat').val(data);
	        	}
	             
	        },
	        error:function(){
	            alert("INVALID LATITUDE");
	        }

	    });
	});


	  $('.lon').change(function(){
	    var lonValue = $(this).val();
	 
	    if(lonValue == ''){
	    	return;
	    }
	    $.ajax({
	        type: 'GET',
	        url: "/create/lonValue/" + lonValue,
	        success: function(data){
	        console.log(data);
	        	if(data === 'invalid'){
	        		alert("INVALID LONGITUDE");
	        		$('.lon').val("");	        	
	        	}else{	        
	        		//$('.lon').val(data);	    
	        	}
	             
	        },
	        error:function(){
	            alert("INVALID LONGITUDE");
	        }

	    });
	});

	  
  	
//  	$("#fishSanctuary").hide();
//      $('#sidebarCollapse').on('click', function () {
//          $('#sidebar').toggleClass('active');
//      });
//      
//      $('#formFishSanctuaries').on('click', function () {
//      	$("#fishSanctuary").show();
//      });
//    
//      var d = new Date();
      
//     $(function() {
//         $(".dateAsOf").datepicker({
//      	   showOn: "both",
//     		buttonImage: "/static/images/calendar_2.png",
//     	      buttonImageOnly: true,
//     	      buttonText: "Select date",
//      	    yearRange: '1950:' + new Date().getFullYear().toString(),
//        	 	minDate: new Date(2014, 10, 30),
//        	    maxDate: new Date().getFullYear().toString(),
//        	    setDate: new Date(2014, 10, 30),
//     	    	changeMonth: true,
//     	    	changeYear: true,
//     	    	minDate: new Date('30/01/1950'),
//     	       maxDate: new Date().getFullYear().toString(),
//     		 	dateFormat: 'mm/dd/yy'
//         });
//     });
     
//     $(function() {
//         $(".dateEstablished").datepicker({
//
//      	   	yearRange: '1950:' + new Date().getFullYear().toString(),
//     	    	changeMonth: true,
//     	    	changeYear: true,
//     		 	dateFormat: 'mm/dd/yy'
//         });
//     });
//     
 /*    $(function() {
  	   var myDate = $("#startDate").attr('value');
  	   
  	   $('#startDate').focusin(function(){
  		   $('.ui-datepicker-calendar').css("display","none");
  		 });

  	$('#startDate').datepicker({
  		showOn: "both",
  		buttonImage: "/static/images/calendar.jpeg",
  	      buttonImageOnly: true,
  	      buttonText: "Select date",
  	    yearRange: '1950:' + new Date().getFullYear().toString(),
  	    maxDate: '-1M',
  	    changeMonth: true,
  	    changeYear: true,
  	    setDate: myDate,
  	    showButtonPanel: false,
  	    dateFormat: 'yy',
  	    onClose: function(dateText, inst) {
  	       
  	        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
  	        $(this).datepicker('setDate', new Date(year, 1));
  	    }
  	});
  	   $('#startDate').datepicker('setDate', myDate);
  	});

     $('#startDate').change(function(){
  	   console.log($(this).val());
     });


     $(function() {
         $("#croppingStart").datepicker({});
     });

     $(function() {
         $("#croppingEnd").datepicker({});
     });

     $('#croppingStart').change(function() {
         startDate = $(this).datepicker('getDate');
         $("#croppingEnd").datepicker("option", "minDate", startDate);
     })

     $('#croppingEnd').change(function() {
         endDate = $(this).datepicker('getDate');
         $("#croppingStart").datepicker("option", "maxDate", endDate);
     })
     
     
     
          $(function() {
         $("#monthStart").datepicker({});
     });

     $(function() {
         $("#monthEnd").datepicker({});
     });

     $('#monthStart').change(function() {
         startDate = $(this).datepicker('getDate');
         $("#monthEnd").datepicker("option", "minDate", startDate);
     })

     $('#monthEnd').change(function() {
         endDate = $(this).datepicker('getDate');
         $("#monthStart").datepicker("option", "maxDate", endDate);
     })

       $(function() {
         $("#dateLaunched").datepicker({});
     });

     $(function() {
         $("#dateInacted").datepicker({});
     });

     $('#dateLaunched').change(function() {
         startDate = $(this).datepicker('getDate');
         $("#dateInacted").datepicker("option", "minDate", startDate);
     })

     $('#dateInacted').change(function() {
         endDate = $(this).datepicker('getDate');
         $("#dateLaunched").datepicker("option", "maxDate", endDate);
     })   
       $(function() {
         $(".dateicon").datepicker({

      	   	yearRange: '1950:' + new Date().getFullYear().toString(),
     	    	changeMonth: true,
     	    	changeYear: true,
     		 	dateFormat: 'mm/dd/yy'
         });
     });   */
     
     
     