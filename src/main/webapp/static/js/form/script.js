
    // Application specific.
    var clockTorontoElement = L.DomUtil.get("details"),
      //  clockBurlingtonElement = L.DomUtil.get("clockBurlington"),
        zones = [
          {
            element: clockTorontoElement, // Using the HTML Element for now.
            width: parseInt(window.getComputedStyle(clockTorontoElement, null).getPropertyValue("width"), 10),
          //  area: L.latLngBounds([14.604782, 125.738333], [13.589232, 125.718163]) // Using L.latLngBounds for now.
          // region 10    area: L.latLngBounds([8.822154, 129.666819],[7.473633, 129.578928])
          
              //region 1 17.333516, 123.269344 16.792628, 123.263851
              //area: L.latLngBounds([17.187971, 124.075057],[16.551906, 124.058578])
          
            //region 2
           // area: L.latLngBounds([19.508786, 125.347020],[18.532471, 125.588720])
            
            //region 3 15.847246, 125.525066 14.938856, 125.646640          
            //area: L.latLngBounds([15.847246, 125.525066],[14.938856, 125.646640])
            
            //memaropa 12.489235, 114.360583 10.411197, 114.294665
            //area: L.latLngBounds([12.489235, 114.360583],[10.411197, 114.294665])
            area: L.latLngBounds([18.063304, 129.583810],[5.752440, 129.323086])
            
          }
          /*,
          {
            element: clockBurlingtonElement, // Using the HTML Element for now.
            width: parseInt(window.getComputedStyle(clockBurlingtonElement, null).getPropertyValue("width"), 10),
            area: L.latLngBounds([43.28, -79.96], [43.48, -79.71]) // Using L.latLngBounds for now.
          }*/
        ];
    
    // Initialization
    var controlsContainer = map._container.getElementsByClassName("leaflet-control-container")[0],
        firstCorner = controlsContainer.firstChild,
        mapContainerWidth;
    
    map.on("resize", setMapContainerWidth);
    setMapContainerWidth();
    
    // Applying the zones.
    for (var i = 0; i < zones.length; i += 1) {
      setZone(zones[i]);
    }
    
    function setZone(zoneData) {
      // Visualize the area.
     // L.rectangle(zoneData.area).addTo(map);
      console.log("width: " + zoneData.width);
      
      controlsContainer.insertBefore(zoneData.element, firstCorner);
      map.on("move resize", function () {
        updateZone(zoneData);
      });
      updateZone(zoneData);
    }
    
    function updateZone(zoneData) {
      var mapBounds = map.getBounds(),
          zoneArea = zoneData.area,
          style = zoneData.element.style;
      
      if (mapBounds.intersects(zoneArea)) {
        style.display = "block";
        
        var hcenterLng = getIntersectionHorizontalCenter(mapBounds, zoneArea),
            hcenter = isNaN(hcenterLng) ? 0 : map.latLngToContainerPoint([0, hcenterLng]).x;
        
        // Update Element position.
        // Could be refined to keep the entire Element visible, rather than cropping it.
        style.left = (hcenter - (zoneData.width / 2)) + "px";
      } else {
        style.display = "none";
      }
    }
    
    function getIntersectionHorizontalCenter(bounds1, bounds2) {
      var west1 = bounds1.getWest(),
          west2 = bounds2.getWest(),
          westIn = west1 < west2 ? west2 : west1,
          east1 = bounds1.getEast(),
          east2 = bounds2.getEast(),
          eastIn = east1 < east2 ? east1 : east2;
      
      return (westIn + eastIn) / 2;
    }
    
    function setMapContainerWidth() {
      mapContainerWidth = map.getSize().x;
    }
