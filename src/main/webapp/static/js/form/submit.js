	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });
	
		$(".submitRegionForm").click(function(e){
			e.preventDefault();
			var developerData = {};
			
			/* CHANGE ONLY */
			developerData["id"] = $(".id").val();
			developerData["firstName"] = $(".firstName").val();
			developerData["lastName"] = $(".lastName").val();
			developerData["ssoId"] = $(".ssoId").val();
			developerData["pass"] = $(".pass").val();
			developerData["email"] = $(".email").val();
			developerData["region"] = $(".txtRegion").val();
			developerData["userProfiles"] = $(".txtRole").val();
			developerData["password"] = $(".pass").val();
			developerData["state"] = $(".state").val();
			console.log(developerData);
        	
        	//add //
			if($(".firstName").val() == '' || $(".lastName").val() == ''||
					$(".ssoId").val()== '' || $(".email").val()==''){
					alert("All fields are required!");
				}
				
				var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
				if(!$(".email").val().match(mailformat)){
					alert("You have entered an invalid email address!");
					return false;
				}else{

        	//end //
        	
	        	
	        	/* CHANGE END */
	        	
			$.ajax({
				type : "POST",
				contentType : "application/json",
				
				/* CHANGE DEFEND ON CLASS NAME */
				url : "regionUser",
				/* CHANGE END */
				
				data : JSON.stringify(developerData),
				dataType : 'json',				
				success : function(data) {
					console.log("SUCCESS", data);
					if(data == null){
						console.log("ERROR");
						alert(data.defaultMessage);
						return false;
					}else{
						alert("users successfully Registered");
						console.log(data);
						data.forEach(function(point){
							console.log(point.firstName);
							$("#view_user").empty();
		        		});
						$('#myModalUser').modal('toggle');
		    			$('body').removeClass('modal-open');
						$('.modal-backdrop').remove();
					}
					
					$(".firstName").val("");
					 $(".lastName").val("");
					$(".ssoId").val("");
					$(".pass").val("");
					$(".email").val("");
					$(".region").val("");
					$(".userProfiles").val("");
					//window.location.reload(true);
				},
				error: function(data){
					alert(data);
				console.log("error", data);
				}
			});
				}
		});	
		
		$(".submitAdminForm").click(function(e){
			e.preventDefault();
			var developerData = {};
			
			/* CHANGE ONLY */
			developerData["id"] = $(".id").val();
			developerData["firstName"] = $(".firstName").val();
			developerData["lastName"] = $(".lastName").val();
			developerData["ssoId"] = $(".ssoId").val();
			developerData["pass"] = $(".pass").val();
			developerData["email"] = $(".email").val();
			developerData["region"] = $(".txtRegion").val();
			developerData["userProfiles"] = $(".txtRole").val();
			developerData["password"] = $(".pass").val();
			developerData["state"] = $(".state").val();
			console.log(developerData);
        	
        	//add //
			
			if($(".firstName").val() == '' || $(".lastName").val() == ''||
				$(".ssoId").val()== '' || $(".email").val()=='' ||
				$(".txtRegion").val()=='' || $(".txtRole").val()==''){
				alert("All fields are required!");
			}
			
			var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			if(!$(".email").val().match(mailformat)){
				alert("You have entered an invalid email address!");
				return false;
			}else{
			
        	//end //
        	
	        	
	        	/* CHANGE END */
	        	
			$.ajax({
				type : "POST",
				contentType : "application/json",
				
				/* CHANGE DEFEND ON CLASS NAME */
				url : "adminUser",
				/* CHANGE END */
				
				data : JSON.stringify(developerData),
				dataType : 'json',				
				success : function(data) {
					console.log("SUCCESS", data);
					if(data.messages == null){
						console.log("ERROR");
						alert(data.defaultMessage);
						return false;
					}else{
						alert("users successfully Registered");
						$('#myModalUser').modal('toggle');
		    			$('body').removeClass('modal-open');
						$('.modal-backdrop').remove();
					}
					
					$(".firstName").val("");
					 $(".lastName").val("");
					$(".ssoId").val("");
					$(".pass").val("");
					$(".email").val("");
					$(".region").val("");
					$(".userProfiles").val("");
					//window.location.reload(true);
				},
				error: function(data){
					alert(data);
				console.log("error", data);
				}
			});
			}
		});	
	});