
	jQuery.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
	{
		return {
			"iStart":         oSettings._iDisplayStart,
			"iEnd":           oSettings.fnDisplayEnd(),
			"iLength":        oSettings._iDisplayLength,
			"iTotal":         oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage":          oSettings._iDisplayLength === -1 ?
				0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
			"iTotalPages":    oSettings._iDisplayLength === -1 ?
				0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
		};
	};
	
function FishSanctuaryList(active){
var table = $("#fishsanctuaries").dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sort": "position",
        //bStateSave variable you can use to save state on client cookies: set value "true" 
        "bStateSave": false,
        //Default: Page display length
        "iDisplayLength": 10,
        //We will use below variable to track page number on server side(For more information visit: http://legacy.datatables.net/usage/options#iDisplayStart)
        "iDisplayStart": 0,
        "fnDrawCallback": function () {
            //Get page numer on client. Please note: number start from 0 So
            //for the first page you will see 0 second page 1 third page 2...
            //Un-comment below alert to see page number
        	//alert("Current page number: "+this.fnPagingInfo().iPage);    
        }, 
        dom: 'Bfrtip',
        buttons: [
            
            {
                extend: 'excelHtml5',
                messageTop: 'FISH SANCTUARY LIST'
            },
            
        ],
      /*  "columnDefs": [
            {
                "targets": [ 5,6,7,8,9,10,11,12,13,14,15],
                "visible": false,
                "searchable": false
            }
        ],
        */
        "sAjaxSource": "fishsanctuaryDataTables.web",
        "fnRowCallback": function( nRow, mData, iDisplayIndex ) {
      
/*         	$('td:eq(0)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.province + '</a>');
        	$('td:eq(1)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.municipality + '</a>');
            $('td:eq(2)', nRow).html('<a href="edit-' + mData.id+ '">' +
                mData.barangay + '</a>');   
            $('td:eq(3)', nRow).html('<a href="edit-' + mData.id+ '">' +
                    mData.nameOfFishSanctuary + '</a>');
            $('td:eq(4)', nRow).html('<a href="edit-' + mData.id+ '">' +
                        mData.dateEncoded + '</a>');
            
            
            return nRow; */
            
        	   $(nRow).click(function() {
        	   
        	   target = mData.sanctuaries.uniqueKey;
        	   console.log(mData.sanctuaries.uniqueKey );
        	   $.get("unique/" + target, function(content, status){
        	   console.log("STATUS: " + status);
        	   console.log("content: " + content);
        	   markerFishSancturiesDel();
         	   refreshRecord(content,"fishsanctuaries","old");
         	   
    			$('#myModalList').modal('toggle');
    			$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
    		  });
                  // document.location.href = "mapview/" + mData.sanctuaries.uniqueKey ;
               });
        },
        "aoColumns": [
            { "mData": "sanctuaries.province" },
            { "mData": "sanctuaries.municipality" },
            { "mData": "sanctuaries.barangay" },       
            { "mData": "sanctuaries.nameOfFishSanctuary" },
            { "mData": "sanctuaries.dateEncoded" },
            { "mData": "sanctuaries.dateAsOf" },
            { "mData": "sanctuaries.code" },
            { "mData": "sanctuaries.area" },
            { "mData": "sanctuaries.type" },           
            { "mData": "sanctuaries.fishSanctuarySource" },
            { "mData": "sanctuaries.remarks" },
            { "mData": "sanctuaries.encodedBy" },
            { "mData": "sanctuaries.editedBy" },          
            { "mData": "sanctuaries.dateEdited" },
            { "mData": "sanctuaries.lat" },
            { "mData": "sanctuaries.lon" }
            
           /*  { "mData": "lat" },
            { "mData": "lon" }, */
             
        ]
    } );

if(active){
		 table.fnDraw(true);
	}
active = false
}