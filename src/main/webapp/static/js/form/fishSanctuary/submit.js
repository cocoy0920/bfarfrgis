	jQuery(document).ready(function($) {
		
	    var token = $("meta[name='_csrf']").attr("content");

	            var header = $("meta[name='_csrf_header']").attr("content");

	            

	            $(document).ajaxSend(function(e, xhr, options) {

	                xhr.setRequestHeader(header, token);

	              });

		$(".submitFishSanctuaries").click(function(e){
		e.preventDefault();
			var developerData = {};
			
			/* CHANGE ONLY */
			developerData["id"] = $(".id").val();
			developerData["user"] = $(".user").val();
			developerData["uniqueKey"] = $(".uniqueKey").val();
			developerData["encodedBy"] = $(".encodedBy").val();
			developerData["editedBy"] = $(".editedBy").val();
			developerData["dateEncoded"] = $(".dateEncoded").val();
			developerData["dateEdited"] = $(".dateEdited").val();
			developerData["dateAsOf"] = $(".dateAsOf").val();
        	developerData["region"] = $(".region").val();
			developerData["province"] = $(".fishsanctuaryprovince").val();
        	developerData["municipality"] =	$(".fishsanctuarymunicipality").val();
        	developerData["barangay"] = $(".fishsanctuarybarangay").val();
        	developerData["nameOfFishSanctuary"] = $(".nameOfFishSanctuary").val();
        	developerData["code"] = $(".code").val();
        	developerData["area"] = $(".area").val();
        	developerData["type"] = $(".type").val();
        	developerData["fishSanctuarySource"] = $(".fishSanctuarySource").val();
        	developerData["remarks"] = $(".remarksContent").val();
        	developerData["bfardenr"] = $(".bfardenr").val();
        	developerData["sheltered"] = $(".sheltered").val();
        	developerData["lat"] = $(".lat").val();
        	developerData["lon"] = $(".lon").val();
        	developerData["image"] = $(".image").val();
        	developerData["image_src"] = $(".image_src").val();
        	console.log(developerData);
        	
        	if($(".dateAsOf").val().replace(/\s/g,"") == "" ||
        	  $(".fishsanctuaryprovince").val().replace(/\s/g,"") == "" ||
        	 $(".fishsanctuarymunicipality").val().replace(/\s/g,"") == "" ||
        	$(".fishsanctuarybarangay").val().replace(/\s/g,"") == "" ||
        	 $(".nameOfFishSanctuary").val().replace(/\s/g,"") == "" ||
        	 $(".code").val().replace(/\s/g,"") == "" ||
        	 $(".area").val().replace(/\s/g,"") == "" ||
        	 $(".type").val().replace(/\s/g,"") == "" ||
        	 $(".fishSanctuarySource").val().replace(/\s/g,"") == "" ||
        	 $(".remarksContent").val().replace(/\s/g,"") == "" ||
        	 $(".bfardenr").val().replace(/\s/g,"") == "" ||
        	 $(".sheltered").val().replace(/\s/g,"") == "" ||
        	 $(".lat").val().replace(/\s/g,"") == "" ||
        	 $(".lon").val().replace(/\s/g,"") == "" ||
        	// $(".image").val().replace(/\s/g,"") == "" ||
        	 $(".image_src").val().replace(/\s/g,"") == ""){
        		alert("ALL FIELDS ARE REQUIRED");
        		return false;
        	}
        	
        		
			$.ajax({
				type : "POST",
				contentType : "application/json",
				
				/* CHANGE DEFEND ON CLASS NAME */
				url : "getFishSanctuary",
				/* CHANGE END */
				
				data : JSON.stringify(developerData),
				dataType : 'json',				
				success : function(data) {
					FishSanctuaryList(true);
					console.log("SUCCESS", data);
					// var container = $('#myModalList .modal-dialog .modal-content .modal-body');
					 //container.load("fishsanctuariesList");	
					alert("SUCCESSFULLY SAVE");
				refreshRecord(data,"fishsanctuaries","old");
					
		        	$(".dateAsOf").val("");
		
					$(".fishsanctuaryprovince").val("");
		        	$(".fishsanctuarymunicipality").val("");
		        	$(".fishsanctuarybarangay").val("");
		        	$(".nameOfFishSanctuary").val("");
		        	$(".code").val("");
		        	$(".area").val("");
		        	$(".type").val("");
		        	$(".fishSanctuarySource").val("");
		        	$(".remarksContent").val("");
		        	$(".bfardenr").val("");
		        	$(".sheltered").val("");
		        	$(".lat").val("");
		        	$(".lon").val("");
		        	$(".image").val("");
		        	$(".image_src").val("");
		        	$(".preview").val("");
				},
				error: function(data){
				console.log("error", data);
				}
			});
		});
		
	});