     
       $(document).ready(function(){  
       
       var bpmnJsonString = '${provincess}';
     
		var resProvince = bpmnJsonString.substring(1, bpmnJsonString.length-1);
		var municipal;
		var resMunicipal;
///////////////////////////////////////////////////////////////////// 
    $(".fishsanctuaries_id a").live('click',function(e){   
     		id =  $(this).attr('id');    	
     	$.get("info/" + id, function(data, status){ 
     				//getdata.js      
     				
     				for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						 var bfardenr = obj.bfardenr;
	            		console.log(bfardenr);
	            		if(bfardenr == 'BFAR'){
	            			$('#bfarData').show();
           				 	$('#denrData').hide();
	            		}
	            		if(bfardenr =='DENR'){
	            			$('#bfarData').hide();
           				 $('#denrData').show();
	            		}    					
     					var slctMunicipal=$('.fishsanctuarymunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.fishsanctuarybarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            		
	             
	            	}
     			getFishSanctuaryInfoById(data, status,resProvince,resMunicipal );
     	});
        	$("#myFishSanctuaryModal").modal('show');
    });
///////////////////////////////////////////////////////////////////// 
        
          $(".fishprocessingplants_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getFishProcessingplants/" + id, function(data, status){
        
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						   var plantRegistered = obj.processingEnvironmentClassification;
	            		console.log(plantRegistered);
	            		if(plantRegistered == 'Plant'){
	            			$('#plantData').show();
	            		}
	            		   					
     					var slctMunicipal=$('.fishprocessingplantmunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.fishprocessingplantbarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            		 
	             
	            	}
        
			getFishProcessingInfoById(data, status,resProvince );
    		  });
             
             $("#myFishProcessingPlants").modal('show');
        });
///////////////////////////////////////////////////////////////////// 
          $(".fishlanding_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getFishLanding/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						console.log("getFishLanding: " + obj);     					
     					var slctMunicipal=$('.fishlandingmunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.fishlandingbarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            		 
	             
	            	}
			getFishLandingInfoById(data, status,resProvince );
    		  });
             
             $("#myFishLanding").modal('show');
        });
/////////////////////////////////////////////////////////////////////       
        $(".fishpen_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getFishPen/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						     					
     					var slctMunicipal=$('.fishpenmunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.fishpenbarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            		 
	             
	            	}
			getFishPenInfoById(data, status,resProvince );
    		  });
             
             $("#myFishPen").modal('show');
        });
///////////////////////////////////////////////////////////////////// 
 $(".fishcage_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getFishCage/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						     					
     					var slctMunicipal=$('.fishcagemunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.fishcagebarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            		 
	             
	            	}
			getFishCageInfoById(data, status,resProvince );
    		  });
             
             $("#myFishCage").modal('show');
        });
/////////////////////////////////////////////////////////////////////         
 $(".fishpond_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getFishPond/" + id, function(data, status){
         for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						var fishPondType = obj.fishPondType;
						var fishPondCondition = obj.fishPondCondition;
	            		console.log(fishPondType +"," + fishPondCondition);
	            		if(fishPondType == 'FLA'){
	            			$('#FLAData').show();
	            		}else{
	            			$('#FLAData').hide();
	            		}
	            		if(fishPondCondition == 'Non-Active'){
	            			$('#nonActiveData').show();
	            		}else{
	            			$('#nonActiveData').hide();
	            		}
	            		  
	            		      					
     					var slctMunicipal=$('.fishpondmunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.fishpondbarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            		 
	             
	            	}
			getFishPondInfoById(data, status,resProvince );
    		  });
             
             $("#myFishPond").modal('show');
        });
/////////////////////////////////////////////////////////////////////           
  $(".hatchery_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getHatchery/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						     					
     					var slctMunicipal=$('.hatcherymunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.hatcherybarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            		 
	             
	            	}
			getHatcheryInfoById(data, status,resProvince );
    		  });
             
             $("#myHatchery").modal('show');
        });
/////////////////////////////////////////////////////////////////////         
   $(".iceplantorcoldstorage_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getIce/" + id, function(data, status){
                for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						 var withValidSanitaryPermit = obj.withValidSanitaryPermit;
	            		console.log(withValidSanitaryPermit);
	            		if(withValidSanitaryPermit == 'YES'){
	            			$('#yesData').show();
	            		}else{
	            			$('#yesData').hide();
	            		}    					
     					var slctMunicipal=$('.iceplantorcoldstoragemunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.iceplantorcoldstoragebarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            		 
	             
	            	}
			getIceInfoById(data, status,resProvince );
    		  });
             
             $("#myIcePlantorColdStorage").modal('show');
        });
/////////////////////////////////////////////////////////////////////         
    $(".fishcorals_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getFishCoral/" + id, function(data, status){
        	 for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						     					
     					var slctMunicipal=$('.fishcoralmunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.fishcoralbarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            		 
	             
	            	}
			getFishCoralInfoById(data, status,resProvince );
    		  });
             
             $("#myFishCorals").modal('show');
        });
/////////////////////////////////////////////////////////////////////         
    $(".FISHPORT_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getFishPort/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						     					
     					var slctMunicipal=$('.fishportmunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.fishportbarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            		 
	             
	            	}
			getFishPortInfoById(data, status,resProvince );
    		  });
             
             $("#myFISHPORT").modal('show');
        });
/////////////////////////////////////////////////////////////////////         
        
    $(".marineprotected_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getMarine/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						     					
     					var slctMunicipal=$('.marineprotectedareamunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.marineprotectedareabarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            		 
	             
	            	}
			getMarineInfoById(data, status,resProvince );
    		  });
             
             $("#myMarineProtected").modal('show');
        });
/////////////////////////////////////////////////////////////////////         
         
    $(".market_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getMarket/" + id, function(data, status){
         for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						     					
     					var slctMunicipal=$('.marketmunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.marketbarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            }	
			getMarketInfoById(data, status,resProvince );
    		  });
             
             $("#myMarket").modal('show');
        });
/////////////////////////////////////////////////////////////////////                 
          
    $(".schooloffisheries_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getSchool/" + id, function(data, status){
         for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						     					
     					var slctMunicipal=$('.schooloffisheriesmunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.schooloffisheriesbarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            }
			getSchoolInfoById(data, status,resProvince );
    		  });
             
             $("#mySchool").modal('show');
        });
/////////////////////////////////////////////////////////////////////                 
                      
          
    $(".seagrass_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getSeagrass/" + id, function(data, status){
         for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						     					
     					var slctMunicipal=$('.seagrassmunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.seagrassbarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            }
			getSeagrassInfoById(data, status,resProvince );
    		  });
             
             $("#mySeagrass").modal('show');
        });
/////////////////////////////////////////////////////////////////////                 
                      
          
    $(".seaweeds_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getSeaweeds/" + id, function(data, status){
         for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						     					
     					var slctMunicipal=$('.seaweedsmunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.seaweedsbarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            }
			getSeaweedsInfoById(data, status,resProvince );
    		  });
             
             $("#mySeaweeds").modal('show');
        });
/////////////////////////////////////////////////////////////////////                 
                      
           
    $(".mangrove_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getMangrove/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						     					
     					var slctMunicipal=$('.mangrovemunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.mangrovebarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            }
			getMangroveInfoById(data, status,resProvince );
    		  });
             
             $("#myMangrove").modal('show');
        });
/////////////////////////////////////////////////////////////////////                 
                      
           
    $(".lgu_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getLGU/" + id, function(data, status){
         for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						     					
     					var slctMunicipal=$('.localgovernmentunitmunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.localgovernmentunitbarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            }
			getLGUInfoById(data, status,resProvince );
    		  });
             
             $("#myLGU").modal('show');
        });
/////////////////////////////////////////////////////////////////////                 
                      
           
    $(".trainingcenter_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getTraining/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						     					
     					var slctMunicipal=$('.fisheriestrainingcentermunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.fisheriestrainingcenterbarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            }
			getTrainingInfoById(data, status,resProvince );
    		  });
             
             $("#myTrainingCenter").modal('show');
        });
/////////////////////////////////////////////////////////////////////                 
                      
           
    $(".mariculturezone_id a").live('click',function(e){   
     	id =  $(this).attr('id');
     	
     $.get("getMaricultureZone/" + id, function(data, status){
        for ( var x = 0; x < data.length; x++) {
						var obj = data[x];
						     					
     					var slctMunicipal=$('.mariculturezonemunicipality'), option="";
         				slctMunicipal.empty();
	            			option = "<option value='"+obj.municipality_id+ "," + obj.municipality+"'>"+obj.municipality+"</option>";
	            			
     					$.get("getMunicipalityList/" + obj.province_id, function(data, status){     					
     							for( let prop in data ){						 					
	 								option = option + "<option value='"+data[prop].municipal_id+ "," + data[prop].municipal+"'>"+data[prop].municipal+"</option>";	           
	  							}
	            			slctMunicipal.append(option);
	            		}); 
	            		var slctBarangay=$('.mariculturezonebarangay'), brgyoption="";
         					slctBarangay.empty();
	            			brgyoption = "<option value='"+obj.barangay_id+ "," + obj.barangay+"'>"+obj.barangay+"</option>";
	           
	            		$.get("getBarangayList/" + obj.municipality_id, function(data, status){   
	            		console.log("getBarangayList: " + data);  					
     							for( let prop in data ){						 					
	 								brgyoption = brgyoption + "<option value='"+data[prop].barangay_id+ "," + data[prop].barangay+"'>"+data[prop].barangay+"</option>";	           
	  							}
	            			slctBarangay.append(brgyoption);
	            		}); 
	            }
			getMaricultureZoneInfoById(data, status,resProvince );
    		  });
             
             $("#myMaricultureZone").modal('show');
        });
/////////////////////////////////////////////////////////////////////                 
                      
                      
                       
                         
                        
                         
         
     });
        
