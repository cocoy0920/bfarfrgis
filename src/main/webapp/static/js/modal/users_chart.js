function Menu(){
/* contents */
const left = `<div class="header">FRGIS</div>`;
const right = '<div class="header">Slide Menu (Right)</div>';

let contents = 
	`<div class="content"> 
    <ul class="list-unstyled components mb-5"> 
      <li> 
    		<a href="#aqualist" data-toggle="collapse"  aria-expanded="false"> 
	  			<span class="text-white"><i class="fas fa-list"></i>&nbsp;AQUACULTURE</span></a> 
    		<ul class="collapse list-unstyled" id="aqualist"> 

   			<li class="dropdown-subitem"> 
   				<a href="#farmlist" data-toggle="collapse"  aria-expanded="false"> 
  	 			<span class="text-white">&nbsp;I. Aquafarms</span></a> 
    				<ul class="collapse list-unstyled" id="farmlist"> 
     				<li><input id="aquafarm" type="checkbox"><span class="text-white">Select all</span></li>
   					<li class="dropdown-item"> 
   						<input type="checkbox" id="checked_pond" value="fishpond" name="fishpond" class="aquafarm">
    						<img src="/static/images/pin2022/FishPond.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Fish Pond</span>
    					</li>
    					<li class="dropdown-item">
   						<input type="checkbox" id="checked_fishpen" value="fishpen" class="aquafarm">
   						<img src="/static/images/pin2022/FishPen.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Fish Pen</span>
   					</li>
   					<li class="dropdown-item">
    						<input type="checkbox" id="checked_fishcage" value="fishcage" class="aquafarm">
    						<img src="/static/images/pin2022/FishCage.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Fish Cage</span>
   					</li>
   					<li class="dropdown-item">
   						<input type="checkbox" id="checked_fishcoral" value="fishcoral" class="aquafarm">
   						<img src="/static/images/pin2022/FishCorral.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Fish Corral</span>
   					</li>
    				  </ul> 
      		  </li> 
      		<li class="dropdown-subitem"> 
   				<a href="#aquaseaweedlist" data-toggle="collapse"  aria-expanded="false"> 
  	 			<span class="text-white">&nbsp;II. Seaweed</span></a> 
    				<ul class="collapse list-unstyled" id="aquaseaweedlist"> 
     				<li><input id="aquaseaweed" type="checkbox"><span class="text-white">Select all</span></li>
   					<li class="dropdown-item"> 
   						<input type="checkbox" id="checked_seaweednursery" value="nursery" name="nursery" class="aquaseaweed">
    						<img src="/static/images/pin2022/Seaweeds_Nursery.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Seaweed Nursery</span>
    					</li>
    					<li class="dropdown-item">
   						<input type="checkbox" id="checked_seaweedlaboratory" value="laboratory" name="laboratory" class="aquaseaweed">
   						<img src="/static/images/pin2022/Seaweeds_laboratory.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Seaweed Laboratory</span>
   					</li>
   					
    				</ul> 
      		</li>
      		<li class="dropdown-subitem"> 
   				<a href="#mariculturelist" data-toggle="collapse"  aria-expanded="false"> 
  	 			<span class="text-white">&nbsp;III. Mariculture Park</span></a> 
    				<ul class="collapse list-unstyled" id="mariculturelist"> 
     				<li><input id="aquamariculture" type="checkbox"><span class="text-white">Select all</span></li>
   					<li class="dropdown-item"> 
   						<input type="checkbox" id="checked_mariculturebfar" value="bfar" name="bfar" class="aquamariculture">
    						<img src="/static/images/pin2022/Mariculture_BFAR.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">BFAR Managed</span>
    					</li>
    					<li class="dropdown-item">
   						<input type="checkbox" id="checked_mariculturelgu" value="lgu" name="lgu" class="aquamariculture">
   						<img src="/static/images/pin2022/Mariculture_LGU.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">LGU Managed</span>
   					</li>
   					<li class="dropdown-item">
   						<input type="checkbox" id="checked_maricultureprivate" value="private" name="private" class="aquamariculture">
   						<img src="/static/images/pin2022/Mariculture_PrivateSector.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Private Sector</span>
   					</li>
   					
    				</ul> 
      		</li>
      		<li class="dropdown-subitem"> 
   				<a href="#hatcherylist" data-toggle="collapse"  aria-expanded="false"> 
  	 			<span class="text-white">&nbsp;IV. Hatchery</span></a> 
    				<ul class="collapse list-unstyled" id="hatcherylist"> 
     			<li><input id="aquahatchery" type="checkbox"><span class="text-white">Select all</span></li>
   					<li class="dropdown-subitem"> 
   						<a href="#bfarmanaged" data-toggle="collapse"  aria-expanded="false"> 
  	 						<span class="text-white">&nbsp;BFAR Managed</span></a> 
    							<ul class="collapse list-unstyled" id="bfarmanaged">  							
   								<li class="dropdown-item">
    									<input type="checkbox" id="checked_legislated" value="hatcheries" class="aquahatchery">
    									<img src="/static/images/pin2022/Hatchery.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Legislated</span>
   								</li>
   								<li class="dropdown-item">
   									<input type="checkbox" id="checked_nonlegislated" value="hatcheries" class="aquahatchery">
    									<img src="/static/images/pin/20x34/hatcheries.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Non-Legislated</span>
   								</li>
   					
    							</ul> 
      					</li>
    					<li class="dropdown-item">
   						<input type="checkbox" id="checked_hatcherylgu" value="lgu" name="lgu" class="aquahatchery">
   						<img src="/static/images/pin/new/Hatchery_LGU.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">LGU Managed</span>
   					</li>
   					<li class="dropdown-item">
   						<input type="checkbox" id="checked_hatcheryprivate" value="private" name="private" class="aquahatchery">
   						<img src="/static/images/pin2022/Hatchery_PrivateSector.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Private Sector</span>
   					</li>
   					
    				</ul> 
      		</li>
    			</ul> 
    		</li>
    		<li> 
    			<a href="#capturedList" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;CAPTURE FISHERIES</span></a> 
    				<ul class="collapse list-unstyled" id="capturedList"> 
     				<li><input id="capture" type="checkbox"><span class="text-white">Select all</span></li>
   					<li class="dropdown-item"> 
   						<input type="checkbox" id="checked_payao" value="payao" name="payao" class="capture">
    						<img src="/static/images/pin2022/Payao.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Payao</span>
    					</li> 
    					<li class="dropdown-item">
   						<input type="checkbox" id="checked_lambaklad" value="lambaklad" name="lambaklad" class="capture">
    						<img src="/static/images/pin2022/Lambaklad.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Lambaklad</span>
    					</li>

   					<li class="dropdown-item">
   						<input type="checkbox" id="checked_frpboats" value="frpboats" name="frpboats" class="capture">
    						<img src="/static/images/pin2022/FRP_Boats.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">FRP Boats</span>
    					</li>	
    				</ul>
    		</li>
    		<li> 
    			<a href="#postharvestList" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;POST HARVEST</span></a> 
    				<ul class="collapse list-unstyled" id="postharvestList"> 
     				<li class="dropdown-subitem">
    						<a href="#postharvestFishlanding" data-toggle="collapse"  aria-expanded="false">
  	 						<span class="text-white">&nbsp;I. Fish Landing</span>
    						</a>
     					<ul class="collapse list-unstyled" id="postharvestFishlanding">
     						<li><input id="fishlanding" type="checkbox"><span class="text-white">Select all</span></li>
     						<li class="dropdown-subitem">
    								<a href="#postharvestCFLC" data-toggle="collapse"  aria-expanded="false">
  	 							<span class="text-white">&nbsp;I.1 CFLC</span>
    								</a>
     								<ul class="collapse list-unstyled" id="postharvestCFLC">
     						
     									<li class="dropdown-item">
   											<input type="checkbox" id="check_cflc_operational" value="cflc" name="FISH_LANDING" class="fishlanding">
    											<img src="/static/images/pin/new/fish-landing.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">OPERATIONAL</span>
    										</li>
    										<li class="dropdown-item">
   											<input type="checkbox" id="check_cflc_for_operation" value="cflc" name="FISH_LANDING" class="fishlanding">
    											<img src="/static/images/pin/new/fish-landing.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">FOR OPERATION</span>
    										</li>
    										<li class="dropdown-item">
   											<input type="checkbox" id="check_cflc_for_complition" value="cflc" name="FISH_LANDING" class="fishlanding">
    											<img src="/static/images/pin/new/fish-landing.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">FOR COMPLETION</span>
    										</li>
    										<li class="dropdown-item">
   											<input type="checkbox" id="check_cflc_for_transfer" value="cflc" name="FISH_LANDING" class="fishlanding">
    											<img src="/static/images/pin/new/fish-landing.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">FOR TRANSFER</span>
    										</li>
    										<li class="dropdown-item">
   											<input type="checkbox" id="check_cflc_damaged" value="cflc" name="FISH_LANDING" class="fishlanding">
    											<img src="/static/images/pin/new/fish-landing.png" style="width: 20px; height: 20px;"/>
    											<span class="text-white">DAMAGED</span>
    										</li>
   									</ul>
   								</li>		
   								<li class="dropdown-item">
   								<span class="text-white">I. 2</span>
   									<input type="checkbox" id="check_traditional" value="cflc" name="FISH_LANDING" class="fishlanding">
    									<img src="/static/images/pin/new/fish-landing.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">TRADITIONAL</span>
    								</li>
    								<li class="dropdown-item">
    								<span class="text-white">I. 3</span>
   									<input type="checkbox" id="check_noncflc" value="cflc" name="FISH_LANDING" class="fishlanding">
    									<img src="/static/images/pin/new/fish-landing.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">NON-TRADITIONAL</span>
    								</li>
    									
    						</ul>
    					</li>
    					<li class="dropdown-subitem">
    						<a href="#postharvestColdStorage" data-toggle="collapse"  aria-expanded="false">
  	 						<span class="text-white">&nbsp;II. Cold Storage/IPCS</span>
    						</a>
     					<ul class="collapse list-unstyled" id="postharvestColdStorage">
     						<li><input id="coldstorage" type="checkbox"><span class="text-white">Select all</span></li>
     						<li class="dropdown-item">
   									<input type="checkbox" id="check_coldpfda" value="pfda" name="pfda" class="coldstorage">
    									<img src="/static/images/pin2022/IcePlant_BFAR.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">BFAR</span>
    							</li>
    							<li class="dropdown-item">
   									<input type="checkbox" id="check_coldprivate" value="private" name="private" class="coldstorage">
    									<img src="/static/images/pin2022/IcePlant_PrivateSector.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Private Sector</span>
    							</li>
    							<li class="dropdown-item">
   									<input type="checkbox" id="check_coldcooperative" value="cooperative" name="cooperative" class="coldstorage">
    									<img src="/static/images/pin2022/IcePlant_Cooperative.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Cooperative</span>
    							</li>
     					</ul>
     				</li>
     				<li class="dropdown-subitem">
    						<a href="#postharvestprocessing" data-toggle="collapse"  aria-expanded="false">
  	 						<span class="text-white">&nbsp;III. Fish Processing Facility</span>
    						</a>
     					<ul class="collapse list-unstyled" id="postharvestprocessing">
     						<li><input id="processing" type="checkbox"><span class="text-white">Select all</span></li>
     						<li class="dropdown-item">
   									<input type="checkbox" id="check_processingbfar" value="bfar" name="bfar" class="processing">
    									<img src="/static/images/pin/new/Fish_Processing.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">BFAR</span>
    							</li>
    							<li class="dropdown-item">
   									<input type="checkbox" id="check_processinglgu" value="lgu" name="lgu" class="processing">
    									<img src="/static/images/pin2022/Fish_Processing_LGU.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">LGU</span>
    							</li>
    							<li class="dropdown-item">
   									<input type="checkbox" id="check_processingprivate" value="private" name="private" class="processing">
    									<img src="/static/images/pin2022/Fish_Processing_PrivateSector.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Private Sector</span>
    							</li>
    							<li class="dropdown-item">
   									<input type="checkbox" id="check_processingcooperative" value="cooperative" name="cooperative" class="processing">
    									<img src="/static/images/pin2022/Fish_Processing_Cooperative.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Cooperative</span>
    							</li>
     					</ul>
     				</li>
     				<li class="dropdown-subitem">
    						<a href="#postharvestseaweed" data-toggle="collapse"  aria-expanded="false">
  	 						<span class="text-white">&nbsp;IV. Seaweed</span>
    						</a>
     					<ul class="collapse list-unstyled" id="postharvestseaweed">
     						<li><input id="postseaweed" type="checkbox"><span class="text-white">Select all</span></li>
     						<li class="dropdown-item">
   									<input type="checkbox" id="check_warehouse" value="warehouse" name="warehouse" class="postseaweed">
    									<img src="/static/images/pin2022/Seaweeds(postharvest).png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Seaweed Warehouse</span>
    							</li>
    							<li class="dropdown-item">
   									<input type="checkbox" id="check_dryer" value="dryer" name="dryer" class="postseaweed">
    									<img src="/static/images/pin2022/seaweeds_dryer.png" style="width: 20px; height: 20px;"/>
    									<span class="text-white">Seaweed Dryer</span>
    							</li>
    							
     					</ul>
     				</li>
     				
     				<li class="dropdown-item">
     					<span class="text-white">V.</span>
   						<input type="checkbox" id="checked_market" value="market">
    						<img src="/static/images/pin2022/Market.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Market</span>
    					</li>	
    					<li class="dropdown-item">
    					<span class="text-white">VI.</span>
    						<input type="checkbox" id="checked_fishport" value="FISHPORT">
   						<img src="/static/images/pin2022/FishPort.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">Fish Port</span>
   					</li>
    			</ul>
    		</li>
    		



	  	<li>         
    		<a href="#pageOther" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;OTHER RESOURCES</span></a> 
    	<ul class="collapse list-unstyled" id="pageOther"> 
     
     <li class="dropdown-subitem">         
    		<a href="#pageBodiesofWater" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white">&nbsp;I. Bodies of Water</span></a> 
    			<ul class="collapse list-unstyled" id="pageBodiesofWater"> 			
      <li class="dropdown-subitem">         
    		<a href="#pageCreate" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white"><i class="fas fa-list"></i>&nbsp;Fisheries Management Areas</span></a> 
    			<ul class="collapse list-unstyled" id="pageCreate"> 
     			<li><input id="FMA" type="checkbox"><span class="text-white">Select all</span></li>
   					<li class="dropdown-item"> 
   						<input type="checkbox" id="FMA-1" value="FMA-1" name="FMA" class="FMA">
    						<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    						<span class="text-white">FMA 1</span>
    					</li> 
    				<li class="dropdown-item">
   					<input type="checkbox" id="FMA-2" value="FMA-2" name="FMA" class="FMA">
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 2</span>
    				</li>

   				<li class="dropdown-item">
   					<input type="checkbox" id="FMA-3" value="FMA-3" name="FMA" class="FMA">
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 3</span>
    				</li>
    				<li class="dropdown-item">
   					<input type="checkbox" id="FMA-4" value="FMA-4" name="FMA" class="FMA">
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 4</span>
    				</li>
    				<li class="dropdown-item">
   					<input type="checkbox" id="FMA-5" value="FMA-5" name="FMA" class="FMA">
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 5</span> 
    				</li> 
    				<li class="dropdown-item">
   					<input type="checkbox" id="FMA-6" value="FMA-6" name="FMA" class="FMA">
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 6</span>
    				</li>
    				<li class="dropdown-item">
   					<input type="checkbox" id="FMA-7" value="FMA-7" name="FMA" class="FMA">
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 7</span> 
    				</li>
    				<li class="dropdown-item">
   					<input type="checkbox" id="FMA-8" value="FMA-8" name="FMA" class="FMA">
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 8</span>
    				</li>
    				<li class="dropdown-item"> 
   					<input type="checkbox" id="FMA-9" value="FMA-9" name="FMA" class="FMA">
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 9</span>
    				</li>
    				<li class="dropdown-item"> 
   					<input type="checkbox" id="FMA-10" value="FMA-10" name="FMA" class="FMA">
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 10</span>
    				</li>
    				<li class="dropdown-item"> 
   					<input type="checkbox" id="FMA-11" value="FMA-11" name="FMA" class="FMA">
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 11</span>
    				</li>
    				<li class="dropdown-item"> 
   					<input type="checkbox" id="FMA-12" value="FMA-12" name="FMA" class="FMA">
    					<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">FMA 12</span> 
	  				</li> 
    	      	
   			</ul>
   		</li>      
   		<li class="dropdown-subitem">
    <a href="#major_fg" data-toggle="collapse"  aria-expanded="false">
  	 <span class="text-white"><i class="fas fa-list"></i>&nbsp;24 Major Fishing Grounds</span>
    </a>
     <ul class="collapse list-unstyled" id="major_fg">
     <li><input id="major24_fg" type="checkbox"><span class="text-white">Select all</span></li>
   	<li class="dropdown-item">
   		<input type="checkbox" id="checked_lingayen_gulf" value="LINGAYEN GULF" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">1 - LINGAYEN GULF</span>
    	</li>
if("manila" === "manila"){
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_manila_bay" value="Manila Bay" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">2 - Manila Bay</span>
    	</li>
    	}else{
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_manila_bay" value="Manila Bay" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">2 - Not Manila Bay</span>
    	</li>
    	}
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_batangas_coast" value="Batangas Coast" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">3 - Batangas Coast</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_tayabas_bay" value="Tayabas Bay" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">4 - Tayabas Bay</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_wpalawan_waters" value="West Palawan waters" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">5 - West Palawan waters</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_cuyo_pass" value="Cuyo Pass" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">6 - Cuyo Pass</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_wsulu_sea" value="West Sulu Sea" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">7 - West Sulu Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_ssulu_sea" value="South Sulu Sea" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">8- South Sulu Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_esulu_sea" value="East Sulu Sea" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">9 - East Sulu Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_moro_gulf" value="Moro Gulf" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">10 - Moro Gulf</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_davao_gulf" value="Davao Gulf" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">11 - Davao Gulf</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_samar_gulf" value="Samar Sea" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">12 - Samar Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_sibuyan_gulf" value="Sibuyan Sea" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">13 - Sibuyan Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_camotes_gulf" value="Camotes Sea" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">14 - Camotes Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_visayan_gulf" value="Visayan Sea" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">15 - Visayan Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_guimaras_gulf" value="Guimaras Strait" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">16 - Guimaras Strait</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_bohol_gulf" value="Bohol Sea" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">17 - Bohol Sea</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_ragay_gulf" value="Ragay Gulf" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">18 - Ragay Gulf</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_leyte_gulf" value="Leyte Gulf" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">19 - Leyte Gulf</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_lagonoy_gulf" value="Lagonoy Gulf" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">20 - Lagonoy Gulf</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_lamon_bay" value="Lamon Bay" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">21 - Lamon Bay</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_casiguran_sound" value="Casiguran Sound" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">22 - Casiguran Sound</span>
    	</li>
    	<li class="dropdown-item">
   		<input type="checkbox" id="checked_palanan_bay" value="Palanan Bay" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">23 - Palanan Bay</span>
    	</li>
    <li class="dropdown-item">
   		<input type="checkbox" id="checked_babuyan_channel" value="Babuyan Channel" class="FG">
    			<img src="/static/images/red-dot.png" style="width: 20px; height: 20px;" />
    			<span class="text-white">24 - Babuyan Channel</span>
    	</li>
    	</ul>
    	</li>
        <li class="dropdown-item">
   	 		<input type="checkbox" id="checked_municipal_waters" value="Municipal Water ">
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Municipal Waters</span>
	  	</li>
	  <li class="dropdown-item">
   	 		<input type="checkbox" id="checked_rivers" value="River">
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Rivers</span>
	  </li>
	  <li class="dropdown-item">
   	 		<input type="checkbox" id="checked_lakes" value="Lakes">
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Lakes</span>
	  </li>
	  <li class="dropdown-item">
   	 		<input type="checkbox" id="checked_bay" value="Bay">
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Bay</span>
	  </li>
	  <li class="dropdown-item">
   	 		<input type="checkbox" id="checked_strait" value="Strait">
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Strait</span>
	  </li>
	  <li class="dropdown-item">
   	 		<input type="checkbox" id="checked_channel" value="Channel">
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Channel</span>
	  </li>
	  <li class="dropdown-item">
   	 		<input type="checkbox" id="checked_gulf" value="Gulf">
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Gulf</span>
	  </li>
	  <li class="dropdown-item">
   	 		<input type="checkbox" id="checked_sea" value="Sea">
   	 		 <img src="/static/images/red-dot.png" style="width: 20px; height: 20px;"/>
  	 		<span class="text-white">Sea</span>
	  </li>
	  
	  </ul>	  
	  </li>
	  <li class="dropdown-subitem">         
    		<a href="#pagefishhabitat" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white">&nbsp;II. Fish Habitat</span></a> 
    			<ul class="collapse list-unstyled" id="pagefishhabitat"> 
    			<li><input id="habitat" type="checkbox"><span class="text-white">Select all</span></li>
   			<li class="dropdown-item">
   				<input type="checkbox" id="checked_seagrass" value="seagrass" class="habitat">
    				<img src="/static/images/pin2022/Seagrass.png" style="width: 20px; height: 20px;"/>
    				<span class="text-white">Sea Grass</span>
   			</li>
   			<li class="dropdown-item">
   				<input type="checkbox" id="checked_mangrove" value="mangrove" class="habitat">
     			<img src="/static/images/pin2022/Mangrove.png" style="width: 20px; height: 20px;"/>
    				<span class="text-white">Mangrove</span>
   			</li>
   			<li class="dropdown-item">
   				<input type="checkbox" id="checked_coralreefs" value="fishcoral" class="habitat">
   				<img src="/static/images/pin2022/Coral-Reefs.png" style="width: 20px; height: 20px;"/>
    				<span class="text-white">Coral Reefs</span>
   			</li>
    			</ul>
    </li>
    <li class="dropdown-subitem">         
    		<a href="#pageinfrustracture" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white">&nbsp;III. BFAR Infrastructure</span></a> 
    			<ul class="collapse list-unstyled" id="pageinfrustracture"> 
    				<li><input id="infrastructure" type="checkbox"><span class="text-white">Select all</span></li>
   				<li class="dropdown-item">
   					<input type="checkbox" id="check_nationalcenter" value="national_center" name="infrastructure" class="infrastructure">
    					<img src="/static/images/pin2022/BINTCenter.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">National Technology Center</span>
    				</li>
    				<li class="dropdown-item">
   					<input type="checkbox" id="check_regionaloffices" value="regional_offices" name="infrastructure" class="infrastructure">
    					<img src="/static/images/pin2022/BFAR_Infrastructure_RegionalFisheriesOffice.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">Regional Fisheries Office</span>
    				</li>
    				<li class="dropdown-item">
   					<input type="checkbox" id="check_rdtc" value="regional_offices" name="infrastructure" class="infrastructure">
    					<img src="/static/images/pin2022/BIRDTC.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">Regional Development Training Center</span>
    				</li>
    				<li  class="dropdown-item">
    					<input type="checkbox" id="checked_lgu" value="lgu" name="infrastructure" class="infrastructure">
     				<img src="/static/images/pin2022/BFAR_Insfrastructure_ProvincialFisheriesOffice.png" style="width: 20px; height: 20px;"/>
     				<span class="text-white">Provincial Fisheries Office</span>
   				</li>
   				<li class="dropdown-item">
   					<input type="checkbox" id="check_tos" value="technology_outreach_station" name="infrastructure" class="infrastructure">
    					<img src="/static/images/pin2022/BFAR_Insfrastructure_TechnologyOutreachStation.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">Technology Outreach Station</span>
    				</li>
    				<li class="dropdown-item">
   					<input type="checkbox" id="checked_schoolOfFisheries" value="schoolOfFisheries" name="infrastructure" class="infrastructure">
    					<img src="/static/images/pin2022/BFAR_Insfrastructure_FisheriesSchool.png" style="width: 20px; height: 20px;"/>
    					<span class="text-white">Fisheries School</span>
	  				</li>
    			</ul>
    </li>
    <li class="dropdown-subitem">         
    		<a href="#pageoma" data-toggle="collapse"  aria-expanded="false"> 
  	 		<span class="text-white">&nbsp;IV. Other Management Area</span></a> 
    			<ul class="collapse list-unstyled" id="pageoma"> 
    			<li class="dropdown-item">
   	 		<input type="checkbox" id="checked_fishsanctuaries" value="fishsanctuaries" class="habitat">
   	 		 <img src="/static/images/pin2022/FishSanctuary.png" style="width: 20px; height: 20px;" data-toggle="tooltip" title="Fish Sanctuary"/>
  	 		<span class="text-white">Fish Sanctuary</span>
	  		</li>
    			</ul>
    </li>
   
    </ul>
    </li>
     <li class="dropdown-subitem">
        <a href="#pageusers" data-toggle="collapse"  aria-expanded="false">
          <span class="text-white">Create Resources</span></a>
         <ul class="collapse list-unstyled" id="pageusers">
          <li class="dropdown-item">
          	<a href="/create/fishsanctuary">
          		<img src="/static/images/iconCircle/fishsanctuary.png" style="width: 20px; height: 20px;"/>
          	  	<span class="text-white">Fish Sanctuary</span>
          	  </a>
          </li>
          <li class="dropdown-item"><a href="/create/fishprocessingplants"><img src="/static/images/iconCircle/fishprocessing.png" style="width: 20px; height: 20px;"/> Fish Processing</a></li>
          <li class="dropdown-item"><a href="/create/fishlanding"><img src="/static/images/iconCircle/fish-landing.png" style="width: 20px; height: 20px;"/> Fish Landing</a></li>
          <li class="dropdown-item"><a href="/create/fishport"><img src="/static/images/iconCircle/fishport.png" style="width: 20px; height: 20px;"/> Fish Port</a></li>
          <li class="dropdown-item"><a href="/create/fishpen"><img src="/static/images/iconCircle/fishpen.png" style="width: 20px; height: 20px;"/>Fish Pen</a></li>
          <li class="dropdown-item"><a href="/create/fishcages"><img src="/static/images/iconCircle/fishcage.png" style="width: 20px; height: 20px;"/> Fish Cage</a></li>
          <li class="dropdown-item"><a href="/create/hatchery"><img src="/static/images/iconCircle/hatcheries.png" style="width: 20px; height: 20px;"/>Hatchery</a></li>
          <li class="dropdown-item"><a href="/create/iceplantorcoldstorage"><img src="/static/images/iconCircle/iceplant.png" style="width: 20px; height: 20px;"/>Ice Plant/Cold Storage</a></li>
          <li class="dropdown-item"><a href="/create/market"><img src="/static/images/iconCircle/market.png" style="width: 20px; height: 20px;"/>Market</a></li>
          <li class="dropdown-item"><a href="/create/schooloffisheries"><img src="/static/images/iconCircle/schoolof-fisheries.png" style="width: 20px; height: 20px;"/>School of Fisheries</a></li>
          <li class="dropdown-item"><a href="/create/fishcorals"><img src="/static/images/iconCircle/fishcorral.png" style="width: 20px; height: 20px;"/>Fish Corral(Baklad)</a></li>
          <li class="dropdown-item"><a href="/create/seagrass"><img src="/static/images/iconCircle/seagrass.png" style="width: 20px; height: 20px;"/>Seagrass</a></li>
          <li class="dropdown-item"><a href="/create/seaweeds"><img src="/static/images/iconCircle/seaweeds.png" style="width: 20px; height: 20px;"/>Seaweeds</a></li>
          <li class="dropdown-item"><a href="/create/mangrove"><img src="/static/images/iconCircle/mangrove.png" style="width: 20px; height: 20px;"/>Mangrove</a></li>
          <li class="dropdown-item"><a href="/create/mariculturezone"> <img src="/static/images/iconCircle/mariculturezone.png" style="width: 20px; height: 20px;"/>Mariculture Zone</a></li>
          <li class="dropdown-item"><a href="/create/lgu"><img src="/static/images/iconCircle/lgu.png" style="width: 20px; height: 20px;"/>PFO</a></li>
          <li class="dropdown-item"><a href="/create/trainingcenter"><img src="/static/images/iconCircle/fisheriestraining.png" style="width: 20px; height: 20px;"/>Training Center</a></li>
		  <li class="dropdown-item"><a href="/create/nationalcenter"><img src="/static/images/iconCircle/fisheriestraining.png" style="width: 20px; height: 20px;"/>National Center</a></li>
		 <li class="dropdown-item"><a href="/create/regionaloffice"><img src="/static/images/iconCircle/fisheriestraining.png" style="width: 20px; height: 20px;"/>Regional Office</a></li>
		 <li class="dropdown-item"><a href="/create/tos"><img src="/static/images/iconCircle/fisheriestraining.png" style="width: 20px; height: 20px;"/>TOS</a></li>
		 <li class="dropdown-item"><a href="/create/payao"><img src="/static/images/iconCircle/fisheriestraining.png" style="width: 20px; height: 20px;"/>Payao</a></li>
		</ul>
      </li>
       <li class="dropdown-item">
			<a href="bar"> 	
 					<span class="text-white">Chart Report</span>
 				</a>
		</li>
		<li class="dropdown-item">
			<a href="report">
				<span class="text-white">Excel Report</span>
			</a>
		</li>
        	
    	</ul>


   </div>`;

/* left */
L.control.slideMenu(left + contents).addTo(content);
}