
  //  var data =  ${maps};

  //  	var getMap = 'https://geo.bfar.da.gov.ph/wms';
    //	var mapLayer = ${region_map};
     //	var map = L.map('mapContainer').setView([${set_center}], 7),
		
    	vector = L.geoJson().addTo(map);
    	 var wmsLayer = L.tileLayer.wms(getMap, {
    		layers: mapLayer,
    		attribution: "FIMC",
    		transparent: true,
    		maxZoom: 15
		}).addTo(map); 
         
      
          var legend = L.control({position: 'bottomright'});

         legend.onAdd = function (map) {

             var div = L.DomUtil.create('div', 'info legend'),
                 grades = [0, 10, 20, 50, 100, 200, 500, 1000],
                 labels = [];
                 div.innerHTML += " <img src='/frgis/static/images/nswe.jpeg' height='50' width='50'>";

             return div;
         };

         legend.addTo(map);
         
           var regionLegend = L.control({position: 'bottomleft'});

         regionLegend.onAdd = function (map) {

             var div = L.DomUtil.create('div', 'info legend'),
                 grades = [0, 10, 20, 50, 100, 200, 500, 1000],
                 labels = [];
//                 div.innerHTML += ${region_name} ;

             return div;
         };

         regionLegend.addTo(map);
                     
                    	var LeafIcon = L.Icon.extend({
        	   				 options: {
        	        		iconSize:     [30, 48]
        	    		}
        				});
            
  
          
          var fishSanctuaryLayerGroup = false; 
          
          var getAllMaps = new Array();
       	  var layerControl = false;
  
 //      	var role = ${role};		
  		var page;
  		var latestpage;
 function refreshRecord(value,link,status)
    {
console.log("value: " + value);
console.log("link: " + link);
console.log("status: " + status);

       data = value;
		page = link;

	if(status === 'new'){
	getRecordList(page);
	}

			if(page == "fishsanctuaries"){	
			//viewListOnMap.js				
				fishsanctuariesGetListMap(data,role);  	
				
 	 		}
 	 		if(page == "fishprocessingplants"){
 	 			fishprocessingplantsGetListMap(data,role); 	
      				
 	 		}
		 	if(page == "fishlanding"){
				fishlandingGetListMap(data,role);         		
 	 		}   
 	 	 	if(page == "fishpen"){
 	 	 		fishpenGetListMap(data,role);
 	 		} 

			if(page == "fishcage"){
				fishcageGetListMap(data,role);
			} 
 	 	 	if(page == "fishpond"){
 				fishpondGetListMap(data,role);
 			} 	 	        		
        	if(page == "hatchery"){
 	 			hatcheryGetListMap(data,role);
 	 		}
 	 	 	if(page == "iceplantorcoldstorage"){
 	 			iceplantorcoldstorageGetListMap(data,role);
 	 		}
 	 		if(page == "fishcorals"){
 	 			fishcorralsGetListMap(data,role);
 	 		}  
 	 		if(page == "FISHPORT"){
 	 			FISHPORTGetListMap(data,role);
 	 		}   
 	 		if(page == "marineprotected"){
 	 			marineprotectedGetListMap(data,role);
 	 		} 
 	 		if(page == "market"){ 
 	 			marketGetListMap(data,role);
 	 		} 
 	 		if(page == "schooloffisheries"){ 
 	 			schooloffisheriesGetListMap(data,role);
 	 		} 
 	 		if(page == "seagrass"){ 	 	
 	 			seagrassGetListMap(data,role);
 	 		} 
 	 		if(page == "seaweeds"){
        			seaweedsGetListMap(data,role);
 	 		}
 	 		if(page == "mangrove"){      		
 	 			mangroveGetListMap(data,role);
 	 		}
 	 		if(page == "lgu"){        		
        		lguGetListMap(data,role);
 	 		}
 	 		if(page == "trainingcenter"){
        		trainingcenterGetListMap(data,role);
 	 		}
 	 		if(page == "mariculturezone"){
 	 			mariculturezoneGetListMap(data,role);
 	 		}
  
	//}

       	 
    }

	 function getRecordList(link){

       if(link == "fishsanctuaries"){	
       		
				FishSanctuaryList(true);
 	 		}
 	 		if(link == "fishprocessingplants"){ 	 				
      			FisProcessingList(true); 	
 	 		}
		 	if(link == "fishlanding"){
				      FishLandingList(true); 		
 	 		}   
 	 	 	if(link == "fishpen"){
 	 	 		FishPenList(true);
 	 		} 

			if(link == "fishcage"){
				FishCageList(true);
			} 
 	 	 	if(link == "fishpond"){
 				FishPondList(true);
 			} 	 	        		
        	if(link == "hatchery"){
 	 			HatcheryList(true);
 	 		}
 	 	 	if(link == "iceplantorcoldstorage"){
 	 			IcePlantColdStorage(true);
 	 		}
 	 		if(link == "fishcorals"){
 	 			FishCorralList(true);
 	 		}  
 	 		if(link == "FISHPORT"){
 	 			FishPortList(true);
 	 		}   
 	 		if(link == "marineprotected"){
 	 			MarineProtectedAreaList(true);
 	 		} 
 	 		if(link == "market"){ 
 	 			MarketList(true);
 	 		} 
 	 		if(link == "schooloffisheries"){ 
 	 			 SchoolOfFisheriesList(true);
 	 		} 
 	 		if(link == "seagrass"){ 	 	
 	 			 SeaGrassList(true);
 	 		} 
 	 		if(link == "seaweeds"){
        			SeaweedsList(true);
 	 		}
 	 		if(link == "mangrove"){      		
 	 			MangroveList(true);
 	 		}
 	 		if(link == "lgu"){        		
        	 LGUList(true);
 	 		}
 	 		if(link == "trainingcenter"){
        		 TrainingCenterList(true);
 	 		}
 	 		if(link == "mariculturezone"){
 	 			 MaricultureZoneList(true);
 	 		}
}
		