package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.FishPondPage;
import ph.gov.da.bfar.frgis.model.FishPond;

@Repository
public interface FishPondRepo extends JpaRepository<FishPond, Integer> {

	@Query("SELECT c from FishPond c where c.user = :username")
	public List<FishPond> getListFishPondByUsername(@Param("username") String username);
	
	@Query("SELECT COUNT(f) FROM FishPond f WHERE f.user=:username")
	public int  FishPondCount(@Param("username") String username);
	
	@Query("SELECT c from FishPond c where c.uniqueKey = :uniqueKey")
	public FishPond findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from FishPond c where c.region_id = :region_id")
	public List<FishPond> getListFishPondByRegion(@Param("region_id") String region_id);
	
	@Modifying(clearAutomatically = true)
	@Query("update FishPond f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update FishPond f set f.reason =:reason  where f.id =:id")
	public void updateFishPondByReason( @Param("id") int id, @Param("reason") String reason);
	
	@Query("SELECT c from FishPond c where c.uniqueKey = :uniqueKey")
	List<FishPond> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from FishPond c where c.user = :user")
	 public List<FishPond> getPageableFishPond(@Param("user") String user,Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishPondPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameoffishpondoperator,c.lat,c.lon,c.enabled) from FishPond c where c.user = :user")
	public Page<FishPondPage> findByUsername(@Param("user") String user, Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishPondPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameoffishpondoperator,c.lat,c.lon,c.enabled) from FishPond c where c.region_id = :region_id")
	public Page<FishPondPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishPondPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameoffishpondoperator,c.lat,c.lon,c.enabled) from FishPond c ")
	public Page<FishPondPage> findAllFishPond(Pageable pageable);
	
	@Query("SELECT c from FishPond c where c.region_id = :region_id")
	public List<FishPond> findByRegionList(@Param("region_id") String region_id);
	
	@Query("SELECT c from FishPond c where c.province_id = :province_id")
	public List<FishPond> findByProvinceList(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM FishPond f")
	public Long countAllFishPond();
	
	@Query("SELECT COUNT(f) FROM FishPond f WHERE f.region_id=:region_id")
	public Long countFishPondPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM FishPond f WHERE f.province_id=:province_id")
	public Long countFishPondPerProvince(@Param("province_id") String province_id);
	
	
	
}
