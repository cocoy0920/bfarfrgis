package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.MariculturePage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.MaricultureZone;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.MaricultureZoneService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.PageUtils;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class MaricultureZoneController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	MaricultureZoneService zoneService;
	
	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;	
	
	@RequestMapping(value = "/mariculturezone", method = RequestMethod.GET)
	public ModelAndView mariculturezone(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();
		
		user = authenticationFacade.getUserInfo();

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "mariculture");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		
		result = new ModelAndView("users/form");
		return result;
	}
	
	@RequestMapping(value = "/mariculturezones", method = RequestMethod.GET)
	public ModelAndView mariculturezones(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "mariculture");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		
		result = new ModelAndView("users/form");
		return result;
	}
		
	@RequestMapping(value = "/zoneMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 String icon="";
		 ObjectMapper mapper = new ObjectMapper();
			MaricultureZone data = zoneService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	MapsData  mapsData = new MapsData();
		 	Features features = new Features();
			 features = MapBuilder.getMaricultureZoneMapData(data);
			
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	System.out.println("MARICULTURE TYPE: " + data.getMaricultureType());
		 	if(data.getMaricultureType().equals("BFAR")) {
		 		icon = "/static/images/pin2022/Mariculture_BFAR.png";
		 	}
		 	if(data.getMaricultureType().equals("LGU")) {
		 		icon = "/static/images/pin2022/Mariculture_LGU.png";
		 	}
		 	if(data.getMaricultureType().equals("PRIVATE")) {
		 		icon = "/static/images/pin2022/Mariculture_PrivateSector.png";
		 	}
		 	mapsData.setIcon(icon);
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			System.out.println(jsonString);
			map_data= jsonString;
		result = new ModelAndView("redirect:../mariculturezones");
		
	
		return result;
	}
	
@RequestMapping(value = "/zoneMapDeleteById/{id}", method = RequestMethod.GET)
public ModelAndView zoneMapDeleteById(@PathVariable("id") int id,ModelMap model)
		throws JsonGenerationException, JsonMappingException, IOException {

	 ModelAndView result;
	// zoneService.deleteMaricultureZoneById(id);
	 zoneService.updateByEnabled(id);
	result = new ModelAndView("redirect:../mariculturezones");
	

	return result;
}

@RequestMapping(value = "/zoneMapDeleteById/{id}/{reason}", method = RequestMethod.GET)
public ModelAndView zoneMapDeleteById(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
	throws JsonGenerationException, JsonMappingException, IOException {

 ModelAndView result;

 zoneService.updateByEnabled(id);
 zoneService.updateMaricultureZoneByReason(id, reason);
result = new ModelAndView("redirect:../../mariculturezones");


return result;
}
	@RequestMapping(value = "/zoneMap", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "mariculturezone");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("users/map/zonemap");
		
	
		return result;
	}
	
	   @RequestMapping(value = "/Postmariculturezone", method = RequestMethod.POST)
	    public String doLogin(@Valid @ModelAttribute("MaricultureForm") MaricultureZone MaricultureForm,
	            BindingResult result, ModelMap model) {
	    
	    	boolean error = false;
	    	MaricultureZone zone = new MaricultureZone();
	    	zone = MaricultureForm;
	       
	        	 if("select".equals(MaricultureForm.getProvince())){
	        	      
	        	        model.addAttribute("province","Please select Province");
	        	        error = true;
	        	    }
	        	     
	        	    if("select".equals(MaricultureForm.getMunicipality())){
	        	    	
	        	    	model.addAttribute("municipality","Please select Municipality");  
	        	        error = true;
	        	    }
	        	     
	        	    if("select".equals(MaricultureForm.getBarangay())){
	        	    	
	        	    	model.addAttribute("barangay","Please select Barangay"); 
	        	        error = true;
	        	    }
	        	    if(PageUtils.isNullEmpty(MaricultureForm.getNameOfMariculture())){
	          	    	 
	          	        error = true;
	           	    }
	        	    if(PageUtils.isNullEmpty(MaricultureForm.getArea())){
	          	    	 
	          	        error = true;
	           	    }
	        	    if(PageUtils.isNullEmpty(MaricultureForm.getDateLaunched())){
	          	    	 
	          	        error = true;
	           	    }
	        	    if(PageUtils.isNullEmpty(MaricultureForm.getDateInacted())){
	          	    	 
	          	        error = true;
	           	    }
//	        	    if(PageUtils.isNullEmpty(MaricultureForm.getECcNumber())){
//	          	    	 
//	          	        error = true;
//	           	    }
	        	    if(PageUtils.isNullEmpty(MaricultureForm.getMaricultureType())){
	          	    	 
	          	        error = true;
	           	    }
	        	    if(PageUtils.isNullEmpty(MaricultureForm.getMunicipalOrdinanceNo())){
	          	    	 
	          	        error = true;
	           	    }
	        	    if(PageUtils.isNullEmpty(MaricultureForm.getDateApproved())){
	          	    	 
	          	        error = true;
	           	    }
	        	    if(PageUtils.isNullEmpty(MaricultureForm.getDateAsOf())){
	          	    	 
	          	        error = true;
	           	    }
	           	    if(PageUtils.isNullEmpty(MaricultureForm.getDataSource())){
	           	    	
	    	        	        error = true;
	    	        	    } 
	           	    if(PageUtils.isNullEmpty(MaricultureForm.getRemarks())){
	           	    	
	   	 	        	        error = true;
	   	 	        	    }
	           	    if(PageUtils.isNullEmpty(MaricultureForm.getLat())){
	           	    	
	   	 	        	        error = true;
	   	 	        	    }
	           	    if(PageUtils.isNullEmpty(MaricultureForm.getLon())){
	           	    	
	   	 	        	        error = true;
	   	 	        	    }
	           	    if(PageUtils.isNullEmpty(MaricultureForm.getImage_src())){
	           	    	
	   	 	        	        error = true;
	   	 	        	    }
	           	     
	        	    if(error) {
	        	    	model.addAttribute("region", MaricultureForm.getRegion());
	        			model.addAttribute("username",MaricultureForm.getUser());
	        			model.addAttribute("page",MaricultureForm.getPage());
	        	        
	        	        
	        	        return "users/mariculturezone";
	        	    }
	      
	        	  if(!error) {
	        	System.out.println("SAVING FISH SANCTUARY");
	        	savingResources(zone);
	        	model.addAttribute("SAVE", "data save FISH PROCESSING PLANT");
	        	zone = new MaricultureZone();
	        	model.put("MaricultureForm", zone);
	        	return "redirect:new_mariculturezone";
	        	  }
	        return "redirect:new_mariculturezone";
	    }

	@RequestMapping(value = "/mariculturezone/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMariculturezone(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<MariculturePage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 5, Sort.by("id"));
	      
	          page = zoneService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<MariculturePage> MaricultureZoneList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setMaricultureZone(MaricultureZoneList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/getMaricultureZone/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMaricultureZoneById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<MaricultureZone> mapsDatas = zoneService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}

	@RequestMapping(value = "/saveMaricultureZone", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postMaricultureZone(@RequestBody MaricultureZone maricultureZone) throws JsonGenerationException, JsonMappingException, IOException {


		savingResources(maricultureZone);

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
		

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);
	}

	
	@RequestMapping(value = "/maricultureList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getmaricultureList() throws JsonGenerationException, JsonMappingException, IOException {

//		user = getUserInfo();
		List<MaricultureZone> list = null;

		list = zoneService.ListByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}


	private void savingResources(MaricultureZone maricultureZone) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		maricultureZone.setRegion_id(region_data[0]);
		maricultureZone.setRegion(region_data[1]);

		province = maricultureZone.getProvince();
		data = province.split(",", 2);
		maricultureZone.setProvince_id(data[0]);
		maricultureZone.setProvince(data[1]);

		municipal = maricultureZone.getMunicipality();
		municipal_data = municipal.split(",", 2);
		maricultureZone.setMunicipality_id(municipal_data[0]);
		maricultureZone.setMunicipality(municipal_data[1]);

		barangay = maricultureZone.getBarangay();
		barangay_data = barangay.split(",", 3);
		maricultureZone.setBarangay_id(barangay_data[0]);
		maricultureZone.setBarangay(barangay_data[1]);
		maricultureZone.setEnabled(true);

		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(maricultureZone.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		if (maricultureZone.getId() != 0) {
			Optional<MaricultureZone> zone = zoneService.findById(maricultureZone.getId());
			String image_server = zone.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				maricultureZone.setImage("/images/" + image_server);
				newUniqueKeyforMap = maricultureZone.getUniqueKey();
				zoneService.saveMaricultureZone(ResourceData.getMaricultureZone(maricultureZone, user, "", UPDATE));
			}else {
				newUniqueKeyforMap = maricultureZone.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", "");  
				maricultureZone.setImage("/images/" + imageName);
				Base64Converter.convertToImage(maricultureZone.getImage_src(), maricultureZone.getImage());
				zoneService.saveMaricultureZone(ResourceData.getMaricultureZone(maricultureZone, user, "", UPDATE));
				
			}
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", "");  
			maricultureZone.setImage("/images/" + imageName);
			Base64Converter.convertToImage(maricultureZone.getImage_src(), maricultureZone.getImage());
			zoneService.saveMaricultureZone(ResourceData.getMaricultureZone(maricultureZone, user, newUniqueKey, SAVE));
			save = true;

		}

	}
	@RequestMapping(value = "/getLastPageZone", method = RequestMethod.GET)
	public @ResponseBody String getLastPageZone(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = zoneService.MaricultureZoneCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}


	
}
