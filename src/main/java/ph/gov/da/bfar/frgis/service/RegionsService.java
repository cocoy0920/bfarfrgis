package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import ph.gov.da.bfar.frgis.model.Market;
import ph.gov.da.bfar.frgis.model.Regions;



public interface RegionsService {
	
	Optional<Regions> findById(int id);
	
	void save(Regions regions);
	
		
	List<Regions> findAllRegions();
	
	Regions findRegionDetails(String region_id);
	


}