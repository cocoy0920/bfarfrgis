package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.FishSanctuaryPage;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;

@Repository
public interface FishSanctuaryRepo extends JpaRepository<FishSanctuaries, Integer> {

	@Query("SELECT c from FishSanctuaries c where c.user = :username")
	public List<FishSanctuaries> getListFishSanctuariesByUsername(@Param("username") String username);
	
	@Query("SELECT COUNT(f) FROM FishSanctuaries f WHERE f.user=:username")
	public int  FishSanctuariesCount(@Param("username") String username);
	
	@Query("SELECT c from FishSanctuaries c where c.uniqueKey = :uniqueKey")
	public FishSanctuaries findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Modifying(clearAutomatically = true)
	@Query("update FishSanctuaries f set f.enabled =false  where f.id =:id")
	public void updateFishSanctuariesByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update FishSanctuaries f set f.reason =:reason  where f.id =:id")
	public void updateFishSanctuariesByReason( @Param("id") int id, @Param("reason") String reason);
	
	@Query("SELECT c from FishSanctuaries c where c.uniqueKey = :uniqueKey")
	List<FishSanctuaries> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from FishSanctuaries c where c.user = :user")
	 public List<FishSanctuaries> getPageableFishSanctuaries(@Param("user") String user,Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishSanctuaryPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfFishSanctuary,c.lat,c.lon,c.enabled) from FishSanctuaries c where c.user = :user")
	public Page<FishSanctuaryPage> findByUsername(@Param("user") String user, Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishSanctuaryPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfFishSanctuary,c.lat,c.lon,c.enabled) from FishSanctuaries c where c.region_id = :region_id")
	public Page<FishSanctuaryPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishSanctuaryPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfFishSanctuary,c.lat,c.lon,c.enabled) from FishSanctuaries c ")
	public Page<FishSanctuaryPage> findAllFishSanctuary(Pageable pageable);
	
	@Query("SELECT c from FishSanctuaries c where c.region_id = :region_id")
	public List<FishSanctuaries> findByRegionList(@Param("region_id") String region_id);
	
	@Query("SELECT c from FishSanctuaries c where c.province_id = :province_id")
	public List<FishSanctuaries> findByProvinceList(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM FishSanctuaries f")
	public Long countAllFishSanctuaries();
	
	@Query("SELECT COUNT(f) FROM FishSanctuaries f WHERE f.region_id=:region_id")
	public Long countFishSanctuariesPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM FishSanctuaries f WHERE f.province_id=:province_id")
	public Long countFishSanctuariesPerProvince(@Param("province_id") String province_id);

	@Query("SELECT COUNT(f) FROM FishSanctuaries f WHERE f.municipality_id=:municipality_id")
	public Long countFishSanctuariesPerMunicipal(@Param("municipality_id") String municipality_id);
	
	
}
