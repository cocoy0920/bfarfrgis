package ph.gov.da.bfar.frgis.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@Entity
@Table(name="fishsanctuaries")
public class FishSanctuaries implements Serializable {
	


	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String user;
	
	@Column(name = "unique_key")
	private String uniqueKey;
	
	@NotEmpty(message = "Date as Of may not be empty")
	@Column(name = "date_as_of")
	private String dateAsOf;
	
	private String region;
	
	private String region_id;
	

	@NotEmpty(message = "Province may not be empty")
	private String province;
	
	private String province_id;
	
	@NotEmpty(message = "Municipality may not be empty")
	private String municipality;
	
	private String municipality_id; 
	
	@NotEmpty(message = "Barangay may not be empty")
	private String barangay;
	
	private String barangay_id;
	
	@NotEmpty(message = "Name Of Fish Sanctuary may not be empty")
	@Column(name = "name_of_fishsanctuary")
	private String nameOfFishSanctuary;
	
	private String code;
	
	@NotEmpty(message = "Area may not be empty")
	private String area;
	@NotEmpty(message = "Type may not be empty")
	private String type;
	
	@NotEmpty(message = "Data Source may not be empty")
	@Column(name = "source_of_data")
	private String fishSanctuarySource;
	
	@NotEmpty(message = "Remarks may not be empty")
	private String remarks;
	
	@NotEmpty(message = "Pls select")
	@Column(name = "bfar_denr")
	private String bfardenr;
	
	private String sheltered;
	
	@Column(name = "name_of_sensitive_habitat_mpa")
	private String nameOfSensitiveHabitatMPA;
	
	@Column(name = "ordinance_no")
	private String ordinanceNo;
	
	@Column(name = "ordinance_title")
	private String ordinanceTitle;
	
	@Column(name = "date_established")
	private String dateEstablished;
	
	@Column(name = "encode_by")
	private String encodedBy;
	
	@Column(name = "edited_by")
	private String editedBy;

	@Column(name = "date_encoded")
	private String dateEncoded;
	
	@Column(name = "date_edited")
	private String dateEdited;
	
	@NotEmpty(message = "Latitude may not be empty")
	private String lat;
	
	@NotEmpty(message = "Longitude may not be empty")
	private String lon;

	private String image;
	
	private String page;
	
	private String image_src;
	

	private boolean enabled;
	
	@Column(name = "reason")
	private String reason;
	
	@Column(name = "active")
	private String active;


}

	