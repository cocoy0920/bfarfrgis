package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.ColdStoragePage;
import ph.gov.da.bfar.frgis.Page.FishCagePage;
import ph.gov.da.bfar.frgis.Page.FishCoralPage;
import ph.gov.da.bfar.frgis.Page.FishLandingPage;
import ph.gov.da.bfar.frgis.Page.FishPenPage;
import ph.gov.da.bfar.frgis.Page.FishPortPage;
import ph.gov.da.bfar.frgis.Page.FishProcessingPlantPage;
import ph.gov.da.bfar.frgis.Page.FishSanctuaryPage;
import ph.gov.da.bfar.frgis.Page.HatcheryPage;
import ph.gov.da.bfar.frgis.Page.MangrovePage;
import ph.gov.da.bfar.frgis.Page.MariculturePage;
import ph.gov.da.bfar.frgis.Page.MarketPage;
import ph.gov.da.bfar.frgis.Page.PFOPage;
import ph.gov.da.bfar.frgis.Page.SchoolPage;
import ph.gov.da.bfar.frgis.Page.SeaGrassPage;
import ph.gov.da.bfar.frgis.Page.SeaWeedsPage;
import ph.gov.da.bfar.frgis.Page.TrainingPage;
import ph.gov.da.bfar.frgis.model.Chart;
import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.FishCoral;
import ph.gov.da.bfar.frgis.model.FishLanding;
import ph.gov.da.bfar.frgis.model.FishPen;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.Fishport;
import ph.gov.da.bfar.frgis.model.Hatchery;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;
import ph.gov.da.bfar.frgis.model.LGU;
import ph.gov.da.bfar.frgis.model.Mangrove;
import ph.gov.da.bfar.frgis.model.MaricultureZone;
import ph.gov.da.bfar.frgis.model.Market;
import ph.gov.da.bfar.frgis.model.Provinces;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;
import ph.gov.da.bfar.frgis.model.SeaGrass;
import ph.gov.da.bfar.frgis.model.Seaweeds;
import ph.gov.da.bfar.frgis.model.TrainingCenter;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.service.ProvinceService;
import ph.gov.da.bfar.frgis.service.RegionsService;
import ph.gov.da.bfar.frgis.service.UserService;

@Controller
@RequestMapping("/admin")
public class AdminController extends SuperAdminController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	RegionsService regionsService;
	
	@Autowired
	ProvinceService provinceService;

	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	private String provinceName="";
	private String provinceID="";
	private String regionID = "";
	private String regionName= "";

	@RequestMapping(value = "home", method = RequestMethod.GET)
	public ModelAndView adminhome(ModelMap model) {
		ModelAndView modelAndView = new ModelAndView();
		
		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		//fgfgf
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("chart_all_region", getAllRegion());
		
		modelAndView.setViewName("/admin/admin_home"); // resources/template/admin.html
		return modelAndView;
	}
	
	@RequestMapping(value = "/payao", method = RequestMethod.GET)
	public ModelAndView newfishcage(ModelMap model) throws Exception {

		ModelAndView result;

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		//fgfgf
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("chart_all_region", getAllRegion());

		result = new ModelAndView("admin/payao");
		return result;
	}
	
	
	@RequestMapping(value = "/acctList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String fishcagePrintList() throws JsonGenerationException, JsonMappingException, IOException {

	//	user = getUserInfo();
		List<User> list = null;

		list = userService.findAllUsers();


		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	
	@RequestMapping(value = "/getUserAcct/{username}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getUserAcct(@PathVariable("username") String username) throws JsonGenerationException, JsonMappingException, IOException {

	//	user = getUserInfo();
		List<User> list = null;

		list = userService.ListfindByUsername(username);


		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	@RequestMapping(value = "/updateAcctStatus/{username}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String updateAcctStatus(@PathVariable("username") String username)throws JsonGenerationException, JsonMappingException, IOException {

		String server_response;
		User user = userService.findByUsername(username);
		
		user.setEnabled(false);
		
		userService.saveUser(user);
		
		ObjectMapper objectMapper = new ObjectMapper();
		server_response = objectMapper.writeValueAsString("User is DISABLED successfully!");
		return server_response;

	}

	@RequestMapping(value = "/enableAcctStatus/{username}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String enableAcctStatus(@PathVariable("username") String username)throws JsonGenerationException, JsonMappingException, IOException {

		String server_response;
		User user = userService.findByUsername(username);
		
		user.setEnabled(true);
		
		userService.saveUser(user);
		
		ObjectMapper objectMapper = new ObjectMapper();
		server_response = objectMapper.writeValueAsString("User is ENABLED successfully!");
		return server_response;

	}
	
	@RequestMapping(value = "/bar", method = RequestMethod.GET)
	public ModelAndView provincebChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
//		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
//		 List<Regions> regions = regionsService.findAllRegions();
//		 
//		 for(Regions region : regions ) {
//			 
//			 if(regionID.equals(region.getRegion_id())) {
//				 regionName = region.getRegion();
//				 break;
//			 }
//		 }
//		 
//		 	model.addAttribute("region_title",regionName);
//		 	model.addAttribute("all_region", "All Region");
//		 	model.addAttribute("chart_all_region", getAllRegion());
//			model.addAttribute("chart_by_region", getByRegion(regionID));
//			model.addAttribute("province_title",provinceName);
//			model.addAttribute("provinceChart",getChartByProvince(provinceID));
//			model.addAttribute("regions", regions);
//			model.addAttribute("provinces", provinces);
//			modelAndView.setViewName("/admin/Bar");
		
		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());

		 
		 	model.addAttribute("region_title",authenticationFacade.getRegionName());
			//model.addAttribute("chart_by_region", getByRegion(authenticationFacade.getRegionID()));
			//model.addAttribute("province_title",provinceName);
			//model.addAttribute("provinceChart",chartsUtil.getChartByProvince(provinces));
		 	model.addAttribute("user", user);
		 	model.addAttribute("provinces", provinces);
			model.addAttribute("page",false);
			model.addAttribute("show",true);
			modelAndView.setViewName("/users/Bar");
			
		return modelAndView;
	}
	
	@RequestMapping(value = "/bar_per_region/{region}", method = RequestMethod.GET)
	public ModelAndView allregionChart(ModelMap model,@PathVariable("region") String region)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
 
		regionID = region;
//		String regionid= region; 
//
//		System.out.println(region);
//
//		String[] data = new String[0];	
//		
//		if(!region.equals("all")) {
//			System.out.println(regionid);
//			String province_split = regionid;
//			data = province_split.split(",", 2);		
//			regionID = data[0];
//			regionName= data[1];
//		}

		modelAndView.setViewName("redirect:../bar");
		return modelAndView;
		
	}
	
	@RequestMapping(value = "/barPerRegion/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String barPerRegion(@PathVariable("region") String region) throws JsonGenerationException, JsonMappingException, IOException {

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(getByRegion(region));
		
		

		return jsonArray;

	}
	
	@RequestMapping(value = "/barPerProvince/{province}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String barPerProvince(@PathVariable("province") String province) throws JsonGenerationException, JsonMappingException, IOException {

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(getChartByProvince(province));
		
		

		return jsonArray;

	}

	
	@RequestMapping(value = "/bar/{province}", method = RequestMethod.GET)
	public ModelAndView regionChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
 
		String provinceid= province; 

		System.out.println(province);

		String[] data = new String[0];	
		
		if(!provinceid.equals("all")) {
			System.out.println(provinceid);
			String province_split = provinceid;
			data = province_split.split(",", 2);		
			provinceID = data[0];
			provinceName= data[1];
		}

		modelAndView.setViewName("redirect:../bar");
		return modelAndView;
		
	}
	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public ModelAndView reportall(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
 
		

		if(regionID.equals("all")) {
			

		}
		User user = authenticationFacade.getUserInfo();

		model.addAttribute("user", user);

		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		modelAndView.setViewName("admin/report");
		return modelAndView;
		
	}
	
	@RequestMapping(value = "/report/{region}", method = RequestMethod.GET)
	public ModelAndView reportByRegion(ModelMap model,@PathVariable("region") String region)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
 
		regionID = region; 

		

		modelAndView.setViewName("redirect:../report");
		return modelAndView;
		
	}
	
	private List<Chart> getAllRegion(){
		
		
		List<Chart> allChart = new ArrayList<Chart>();
		
		long fishcount = fishSanctuariesService.countAllFishSanctuaries();
		long processingCount = fishProcessingPlantService.countAllFishProcessingPlant();
		long fishlandingCount = fishLandingService.countAllFishLanding();
		long fishportCount = fishPortService.countAllFishport();
		long fishpenCount = fishPenService.countAllFishPen();
		long fishcageCount = fishCageService.countAllFishCage();
		long fishcoldstorageCount = icePlantColdStorageService.countAllIcePlantColdStorage();
		long fishmarketCount =marketService.countAllMarket();
		long fishschoolofFisheriesCount = schoolOfFisheriesService.countAllSchoolOfFisheries();
		long fishcoralCount = fishCorralService.countAllFishCoral();
		long seagrassCount = seaGrassService.countAllSeaGrass();
		long seaweedsCount = seaweedsService.countAllSeaweeds();
		long mangroveCount = mangroveService.countAllMangrove();
		long maricultureCount = zoneService.countAllMaricultureZone();
		long PfoCount = lGUService.countAllLGU();
		long trainingCount = trainingCenterService.countAllTrainingCenter();
		long hatcheryCount = hatcheryService.countAllHatchery();
		
		
		allChart.add(new Chart("Fish Sanctuary", fishcount,"#FFD124"));
		allChart.add(new Chart("Fish Processing", processingCount,"#EAEA7F"));
		allChart.add(new Chart("Fish Landing", fishlandingCount,"#5EE6EB"));
		allChart.add(new Chart("Fish Port", fishportCount,"#E2D784"));
		allChart.add(new Chart("Fish Pen", fishpenCount,"#4D96FF"));
		allChart.add(new Chart("Fish Cage", fishcageCount,"#FF1700"));
		allChart.add(new Chart("Cold Storage", fishcoldstorageCount,"#97DBAE"));
		allChart.add(new Chart("Market", fishmarketCount,"#D49B54"));
		allChart.add(new Chart("School of Fisheries", fishschoolofFisheriesCount,"#C74B50"));
		allChart.add(new Chart("Seagrass", seagrassCount,"#6BCB77"));
		allChart.add(new Chart("Seaweeds", seaweedsCount,"#019267"));
		allChart.add(new Chart("Mangrove", mangroveCount,"#05595B"));
		allChart.add(new Chart("Fish Corral", fishcoralCount,"#000957"));
		allChart.add(new Chart("Mariculture Zone", maricultureCount,"#F55353"));
		allChart.add(new Chart("PFO", PfoCount,"#FF6464"));
		allChart.add(new Chart("Training Center", trainingCount,"#0F2C67"));
		allChart.add(new Chart("Hatchery", hatcheryCount,"#0F2C67"));
		return allChart;
	}
	
	
	private List<Chart> getByRegion(String region_id){
		
		System.out.println("getByRegion: " + region_id);
		List<Chart> allChart = new ArrayList<Chart>();
		
		long fishcount = fishSanctuariesService.countFishSanctuariesPerRegion(region_id);
		long processingCount = fishProcessingPlantService.countFishProcessingPlantPerRegion(region_id);
		long fishlandingCount = fishLandingService.countFishLandingPerRegion(region_id);
		long fishportCount = fishPortService.countFishportPerRegion(region_id);
		long fishpenCount = fishPenService.countFishPenPerRegion(region_id);
		long fishcageCount = fishCageService.countFishCagePerRegion(region_id);
		long fishcoldstorageCount = icePlantColdStorageService.countIcePlantColdStoragePerRegion(region_id);
		long fishmarketCount =marketService.countMarketRegion(region_id);
		long fishschoolofFisheriesCount = schoolOfFisheriesService.countSchoolOfFisheriesPerRegion(region_id);
		long fishcoralCount = fishCorralService.countFishCoralPerRegion(region_id);
		long seagrassCount = seaGrassService.countSeaGrassPerRegion(region_id);
		long seaweedsCount = seaweedsService.countSeaweedsPerRegion(region_id);
		long mangroveCount = mangroveService.countMangrovePerRegion(region_id) ;
		long maricultureCount = zoneService.countMariculturePerRegion(region_id);
		long PfoCount = lGUService.countLGUPerRegion(region_id);
		long trainingCount = trainingCenterService.countTrainingCenterPerRegion(region_id);
		long hatcheryCount = hatcheryService.countHatcheryPerRegion(region_id);
		
		
		allChart.add(new Chart("Fish Sanctuary", fishcount,"#FFD124"));
		allChart.add(new Chart("Fish Processing", processingCount,"#EAEA7F"));
		allChart.add(new Chart("Fish Landing", fishlandingCount,"#5EE6EB"));
		allChart.add(new Chart("Fish Port", fishportCount,"#E2D784"));
		allChart.add(new Chart("Fish Pen", fishpenCount,"#4D96FF"));
		allChart.add(new Chart("Fish Cage", fishcageCount,"#FF1700"));
		allChart.add(new Chart("Cold Storage", fishcoldstorageCount,"#97DBAE"));
		allChart.add(new Chart("Market", fishmarketCount,"#D49B54"));
		allChart.add(new Chart("School of Fisheries", fishschoolofFisheriesCount,"#C74B50"));
		allChart.add(new Chart("Seagrass", seagrassCount,"#6BCB77"));
		allChart.add(new Chart("Seaweeds", seaweedsCount,"#019267"));
		allChart.add(new Chart("Mangrove", mangroveCount,"#05595B"));
		allChart.add(new Chart("Fish Corral", fishcoralCount,"#000957"));
		allChart.add(new Chart("Mariculture Zone", maricultureCount,"#F55353"));
		allChart.add(new Chart("PFO", PfoCount,"#FF6464"));
		allChart.add(new Chart("Training Center", trainingCount,"#0F2C67"));
		allChart.add(new Chart("Hatchery", hatcheryCount,"#0F2C67"));
		//System.out.println("getByRegion: " + allChart.toString());
		
		return allChart;
	}
	
	private List<Chart> getChartByProvince(String province_id){
		System.out.println("getChartByProvince: " + province_id);
		List<Chart> provinceChart = new ArrayList<Chart>();
		
		 String provinceid = province_id;
		 
		
			long fishcount = fishSanctuariesService.countFishSanctuariesPerProvince(provinceid);
			long processingCount = fishProcessingPlantService.countFishProcessingPlantPerProvince(provinceid);
			long fishlandingCount = fishLandingService.countFishLandingPerProvince(provinceid);
			long fishportCount = fishPortService.countFishportPerProvince(provinceid);
			long fishpenCount = fishPenService.countFishPenPerProvince(provinceid);
			long fishcageCount = fishCageService.countFishCagePerProvince(provinceid);
			long fishcoldstorageCount = icePlantColdStorageService.countIcePlantColdStoragePerProvince(provinceid);
			long fishmarketCount =marketService.countMarketProvince(provinceid);
			long fishschoolofFisheriesCount = schoolOfFisheriesService.countSchoolOfFisheriesPerProvince(provinceid);
			long fishcoralCount = fishCorralService.countFishCoralPerProvince(provinceid);
			long seagrassCount = seaGrassService.countSeaGrassPerProvince(provinceid);
			long seaweedsCount = seaweedsService.countSeaweedsPerProvince(provinceid);
			long mangroveCount = mangroveService.countMangrovePerProvince(provinceid) ;
			long maricultureCount = zoneService.countMariculturePerProvince(provinceid);
			long PfoCount = lGUService.countLGUPerProvince(provinceid);
			long trainingCount = trainingCenterService.countTrainingCenterPerProvince(provinceid);
			long hatcheryCount = hatcheryService.countHatcheryPerProvince(province_id);
			
			 

			provinceChart.add(new Chart("Fish Sanctuary", fishcount,"#FFD124"));
			provinceChart.add(new Chart("Fish Processing", processingCount,"#EAEA7F"));
			provinceChart.add(new Chart("Fish Landing", fishlandingCount,"#5EE6EB"));
			provinceChart.add(new Chart("Fish Port", fishportCount,"#E2D784"));
			provinceChart.add(new Chart("Fish Pen", fishpenCount,"#4D96FF"));
			provinceChart.add(new Chart("Fish Cage", fishcageCount,"#FF1700"));
			provinceChart.add(new Chart("Cold Storage", fishcoldstorageCount,"#97DBAE"));
			provinceChart.add(new Chart("Market", fishmarketCount,"#D49B54"));
			provinceChart.add(new Chart("School of Fisheries", fishschoolofFisheriesCount,"#C74B50"));
			provinceChart.add(new Chart("Seagrass", seagrassCount,"#6BCB77"));
			provinceChart.add(new Chart("Seaweeds", seaweedsCount,"#019267"));
			provinceChart.add(new Chart("Mangrove", mangroveCount,"#05595B"));
			provinceChart.add(new Chart("Fish Corals", fishcoralCount,"#000957"));
			provinceChart.add(new Chart("Mariculture Zone", maricultureCount,"#F55353"));
			provinceChart.add(new Chart("PFO", PfoCount,"#FF6464"));
			provinceChart.add(new Chart("Training Center", trainingCount,"#0F2C67"));
			provinceChart.add(new Chart("Hatchery", hatcheryCount,"#0F2C67"));
			
			
			System.out.println("getChartByProvince: " + provinceChart.toString());
			return provinceChart;
	}
	
	@RequestMapping(value = "/getAllfishsanctuariesList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getfishsanctuariesList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishSanctuaries> list = null;
		
		System.out.println("region: " + region);
		if(region.equals("all")) {
			list = fishSanctuariesService.findAllFishSanctuaries();
		}else {
			list = fishSanctuariesService.findByRegionList(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllfishcageList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getfishCageList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishCage> list = null;
		
		System.out.println("region: " + region);
		if(region.equals("all")) {
			list = fishCageService.findAllFishCages();
		}else {
			list = fishCageService.getListByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllfishcoralList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllfishcoralList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishCoral> list = null;
		
		System.out.println("region: " + region);
		if(region.equals("all")) {
			list = fishCorralService.findAllFishCorals();
		}else {
			list = fishCorralService.ListByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllfishlandingList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllfishlandingList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishLanding> list = null;
		
		System.out.println("region: " + region);
		if(region.equals("all")) {
			list = fishLandingService.findAllFishLandings();
		}else {
			list = fishLandingService.findByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllfishpenList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllfishpenList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishPen> list = null;
		
		System.out.println("region: " + region);
		if(region.equals("all")) {
			list = fishPenService.findAllFishPens();
		}else {
			list = fishPenService.findByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllfishportList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllfishportList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<Fishport> list = null;
		
		System.out.println("region: " + region);
		if(region.equals("all")) {
			list = fishPortService.findAllFishports();
		}else {
			list = fishPortService.findByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllfishprocessingList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllfishprocessingList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishProcessingPlant> list = null;
		
		System.out.println("region: " + region);
		if(region.equals("all")) {
			list = fishProcessingPlantService.findAllFishProcessingPlants();
		}else {
			list = fishProcessingPlantService.ListByRegionList(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllhatcheryList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllhatcheryList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<Hatchery> list = null;
		
		System.out.println("region: " + region);
		if(region.equals("all")) {
			list = hatcheryService.findAllHatcherys();
		}else {
			list = hatcheryService.findByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllIPCSList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllIPCSList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<IcePlantColdStorage> list = null;
		
		System.out.println("region: " + region);
		if(region.equals("all")) {
			list = icePlantColdStorageService.findAllIcePlantColdStorages();
		}else {
			list = icePlantColdStorageService.ListByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAlllguList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAlllguList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<LGU> list = null;
		
		System.out.println("pfo region: " + region);
		if(region.equals("all")) {
			list = lGUService.findAllLGUs();
		}else {
			list = lGUService.ListByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllmangroveList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllmangroveList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<Mangrove> list = null;
		
		System.out.println("region: " + region);
		if(region.equals("all")) {
			list = mangroveService.findAllMangroves();
		}else {
			list = mangroveService.ListByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllmariculturezoneList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllmariculturezoneList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<MaricultureZone> list = null;
		
		System.out.println("region: " + region);
		if(region.equals("all")) {
			list = zoneService.findAllMaricultureZones();
		}else {
			list = zoneService.ListByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllmarketList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllmarketList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<Market> list = null;
		
		System.out.println("region: " + region);
		if(region.equals("all")) {
			list = marketService.findAllMarkets();
		}else {
			list = marketService.ListByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllSchooloffisheriesList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllSchooloffisheriesList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<SchoolOfFisheries> list = null;
		
		System.out.println("Schooloffisheries region: " + region);
		if(region.equals("all")) {
			list = schoolOfFisheriesService.findAllSchoolOfFisheriess();
		}else {
			list = schoolOfFisheriesService.ListByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllSeagrassList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllSeagrassList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<SeaGrass> list = null;
		
		System.out.println("region: " + region);
		if(region.equals("all")) {
			list = seaGrassService.findAllSeaGrasss();
		}else {
			list = seaGrassService.ListByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllSeaweedsList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllSeaweedsList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<Seaweeds> list = null;
		
		System.out.println("region: " + region);
		if(region.equals("all")) {
			list = seaweedsService.findAllSeaweedss();
		}else {
			list = seaweedsService.ListByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllTrainingcenterList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllTrainingcenterList(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<TrainingCenter> list = null;
		
		System.out.println("training region: " + region);
		if(region.equals("all")) {
			list = trainingCenterService.findAllTrainingCenters();
		}else {
			list = trainingCenterService.ListByRegion(region);
			
		}
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/fishsanctuaries_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishsanctuariesForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishSanctuaryPage> page = null;
		 List<FishSanctuaryPage> FishSanctuariesList=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = fishSanctuariesService.findAllFishSanctuary(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          FishSanctuariesList = page.getContent();
	          
	          pageable = page.nextPageable();
	      }else {
	    	  page = fishSanctuariesService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          FishSanctuariesList = page.getContent();
	         
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishSanctuaries(FishSanctuariesList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	
	@RequestMapping(value = "/fishcage_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishcageForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishCagePage> page = null;
		 List<FishCagePage> FishCageList=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = fishCageService.findAllFishcage(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          FishCageList = page.getContent();

	          pageable = page.nextPageable();
	      }else {
	    	  page = fishCageService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          FishCageList = page.getContent();
	       
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishCagePage(FishCageList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/fishcoral_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishcoralForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishCoralPage> page = null;
		 List<FishCoralPage> FishCoralList=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id,20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = fishCorralService.findAllFishCoral(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          FishCoralList = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = fishCorralService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          FishCoralList = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishCoral(FishCoralList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/fishlanding_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishlandingForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		System.out.println("region and id : " + region + " : " + page_id);
		 Page<FishLandingPage> page = null;
		 List<FishLandingPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("region_id"));
	      
	      if(region.equals("all")) {
	    	  page = fishLandingService.findAllFishLanding(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = fishLandingService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishLanding(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/fishpen_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishPenForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishPenPage> page = null;
		 List<FishPenPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = fishPenService.findAllFishPen(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = fishPenService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishPen(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/fishport_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishPortForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishPortPage> page = null;
		 List<FishPortPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = fishPortService.findAllFishPort(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = fishPortService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishPort(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/fishprocessing_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishProcessingForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishProcessingPlantPage> page = null;
		 List<FishProcessingPlantPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = fishProcessingPlantService.findAllFishProcessing(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = fishProcessingPlantService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishProcessingPlantPage(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/hatchery_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getHatcheryForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<HatcheryPage> page = null;
		 List<HatcheryPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = hatcheryService.findAllHatchery(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = hatcheryService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setHatcheries(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/ipcs_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getIPCSForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<ColdStoragePage> page = null;
		 List<ColdStoragePage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = icePlantColdStorageService.findAllICPS(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = icePlantColdStorageService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setIcePlantColdStorage(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/lgu_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getlguForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<PFOPage> page = null;
		 List<PFOPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = lGUService.findAllPFO(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = lGUService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setLgu(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/mangrove_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMangroveForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<MangrovePage> page = null;
		 List<MangrovePage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = mangroveService.findAllMangrove(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("Mangrove page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = mangroveService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setMangrove(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}	
	@RequestMapping(value = "/market_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMarketForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<MarketPage> page = null;
		 List<MarketPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = marketService.findAllMarket(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = marketService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setMarket(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	
	@RequestMapping(value = "/schooloffisheries_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSchooloffisheriesForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<SchoolPage> page = null;
		 List<SchoolPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = schoolOfFisheriesService.findAllSchool(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("school page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = schoolOfFisheriesService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("School page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setSchoolOfFisheries(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	
	@RequestMapping(value = "/seagrass_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSeagrassForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<SeaGrassPage> page = null;
		 List<SeaGrassPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = seaGrassService.findAllSeaGrass(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = seaGrassService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setSeaGrass(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/seaweeds_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSeaweedsForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<SeaWeedsPage> page = null;
		 List<SeaWeedsPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = seaweedsService.findAllSeaweeds(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = seaweedsService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setSeaweeds(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/trainings_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getTrainingsForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {


		 Page<TrainingPage> page = null;
		 List<TrainingPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = trainingCenterService.findAllTraining(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
                      + "totalElements: %s, totalPages: %s%n",
              number, numberOfElements, size, totalElements, totalPages);	
	          List = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = trainingCenterService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setTrainingCenter(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/mariculturezone_admin/{region}/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMariculturezoneForAdmin(@PathVariable("region") String region,@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<MariculturePage> page = null;
		 List<MariculturePage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	      if(region.equals("all")) {
	    	  page = zoneService.findAllMariculture(pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	          pageable = page.nextPageable();
	      }else {
	    	  page = zoneService.findByRegion(region,pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	      }
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setMaricultureZone(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	
	@RequestMapping(value = "/fishsanctuaryExcel", method = RequestMethod.GET)
	public ModelAndView fishsanctuaries(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/fishsanctuaries");
		//result = new ModelAndView("create/fishsanctuary");

		return result;
	}
	@RequestMapping(value = "/fishcageExcel", method = RequestMethod.GET)
	public ModelAndView fishcageexcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/fishcage");

		return result;
	}
	
	@RequestMapping(value = "/fishcoralExcel", method = RequestMethod.GET)
	public ModelAndView fishcoralxcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/fishcorals");

		return result;
	}
	@RequestMapping(value = "/fishlandingExcel", method = RequestMethod.GET)
	public ModelAndView fishlandingExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/fishlanding");

		return result;
	}
	@RequestMapping(value = "/fishpenExcel", method = RequestMethod.GET)
	public ModelAndView fishpenExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/fishpen");

		return result;
	}
	
	@RequestMapping(value = "/fishportExcel", method = RequestMethod.GET)
	public ModelAndView fishportExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/FISHPORT");

		return result;
	}
	@RequestMapping(value = "/fishprocessingExcel", method = RequestMethod.GET)
	public ModelAndView fishprocessingExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/fishprocessingplants");

		return result;
	}
	@RequestMapping(value = "/hatcheryExcel", method = RequestMethod.GET)
	public ModelAndView hatcheryExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/hatchery");

		return result;
	}
	
	@RequestMapping(value = "/ipcsExcel", method = RequestMethod.GET)
	public ModelAndView ipcsExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/iceplantorcoldstorage");

		return result;
	}
	@RequestMapping(value = "/lguExcel", method = RequestMethod.GET)
	public ModelAndView lguExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/lgu");

		return result;
	}
	@RequestMapping(value = "/mangroveExcel", method = RequestMethod.GET)
	public ModelAndView mangroveExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/mangrove");

		return result;
	}
	@RequestMapping(value = "/mariculturezoneExcel", method = RequestMethod.GET)
	public ModelAndView mariculturezoneExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/mariculturezone");

		return result;
	}
	@RequestMapping(value = "/marketExcel", method = RequestMethod.GET)
	public ModelAndView marketExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/market");

		return result;
	}
	@RequestMapping(value = "/schooloffisheriesExcel", method = RequestMethod.GET)
	public ModelAndView schooloffisheriesExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/schooloffisheries");

		return result;
	}
	@RequestMapping(value = "/seagrassExcel", method = RequestMethod.GET)
	public ModelAndView seagrassExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/seagrass");

		return result;
	}
	@RequestMapping(value = "/seaweedsExcel", method = RequestMethod.GET)
	public ModelAndView seaweedsExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/seaweeds");

		return result;
	}
	@RequestMapping(value = "/trainingcenterExcel", method = RequestMethod.GET)
	public ModelAndView trainingcenterExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/trainingcenter");

		return result;
	}
	
	
	
}
