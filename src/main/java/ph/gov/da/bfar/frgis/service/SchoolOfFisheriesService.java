package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.SchoolPage;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;



public interface SchoolOfFisheriesService {
	
	Optional<SchoolOfFisheries> findById(int id);
	
	List<SchoolOfFisheries> ListByUsername(String username);
	List<SchoolOfFisheries> ListByRegion(String region_id);
	SchoolOfFisheries findByUniqueKey(String uniqueKey);
	List<SchoolOfFisheries> findByUniqueKeyForUpdate(String uniqueKey);
	void saveSchoolOfFisheries(SchoolOfFisheries SchoolOfFisheries);
	void deleteSchoolOfFisheriesById(int id);
	void updateByEnabled(int id);
	void updateSchoolOfFisheriesByReason(int  id,String  reason);
	List<SchoolOfFisheries> findAllSchoolOfFisheriess(); 
    public List<SchoolOfFisheries> getAllSchoolOfFisheriess(int index,int pagesize,String user);
    public int SchoolOfFisheriesCount(String username);
    public Page<SchoolPage> findByUsername(@Param("user") String user, Pageable pageable);
    public Page<SchoolPage> findByRegion(@Param("region") String region, Pageable pageable);
    public Page<SchoolPage> findAllSchool(Pageable pageable);
    public Long countAllSchoolOfFisheries();
    public Long countSchoolOfFisheriesPerRegion(String region_id);
    public Long countSchoolOfFisheriesPerProvince(String province_id);
    public Long countSchoolOfFisheriesPerMunicipality(String municipality_id);
}