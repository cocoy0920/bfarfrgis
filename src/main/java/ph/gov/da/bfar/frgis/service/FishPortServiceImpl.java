package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.FishPortPage;
import ph.gov.da.bfar.frgis.model.Fishport;
import ph.gov.da.bfar.frgis.repository.FishPortRepo;


@Service("FishportService")
@Transactional
public class FishPortServiceImpl implements FishPortService{

	@Autowired
	private FishPortRepo dao;

	
	public Optional<Fishport> findById(int id) {
		return dao.findById(id);
	}

	public List<Fishport> findByUsername(String username) {
		List<Fishport>fishport = dao.getListFishportByUsername(username);
		return fishport;
	}

	public void saveFishport(Fishport fishport) {
		dao.save(fishport);
	}
	

	@Override
	public void deleteFishportById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateForEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateFishportByReason(int id, String reason) {
		dao.updateFishportByReason(id, reason);
		
	}

	public List<Fishport> findAllFishports() {
		return dao.findAll();
	}

	@Override
	public List<Fishport> getAllFishports(int index, int pagesize, String user) {
		
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<Fishport> page = dao.getPageableFishports(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int FishportCount(String username) {
		return dao.FishportCount(username);
	}

	@Override
	public Fishport findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<Fishport> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<FishPortPage> findByUsername(String user, Pageable pageable) {
		
		return dao.findByUsername(user, pageable);
	}
	
	@Override
	public Page<FishPortPage> findAllFishPort(Pageable pageable) {

		return dao.findAllFishPort(pageable);
	}
	@Override
	public Page<FishPortPage> findByRegion(String region, Pageable pageable) {

		return dao.findByRegion(region, pageable);
	}
	

	@Override
	public List<Fishport> findByRegion(String region_id) {
		
		return dao.getListFishportByRegion(region_id);
	}

	@Override
	public Long countFishportPerRegion(String region_id) {

		return dao.countFishportPerRegion(region_id);
	}

	@Override
	public Long countFishportPerProvince(String province_id) {
	
		return dao.countFishportPerProvince(province_id);
	}

	@Override
	public Long countAllFishport() {

		return dao.countAllFishport();
	}

	@Override
	public Long countFishportPerMunicipality(String municipality_id) {
	
		return dao.countFishportPerMunicipality(municipality_id);
	}

//	@Override
//	public int FishPortCountByMuunicipality(String municipalityID) {
//		return dao.FishPortCountByMuunicipality(municipalityID);
//	}
//
//	@Override
//	public int FishPortCountByProvince(String provinceID) {
//		return dao.FishPortCountByProvince(provinceID);
//	}
//
//	@Override
//	public int FishPortCountByRegion(String regionID) {
//		return dao.FishPortCountByRegion(regionID);
//	}

/*	public boolean isConsignationSSOUnique(Integer id, String sso) {
		Consignation consignation = findBySSO(sso);
		return ( consignation == null || ((id != null) && (consignation.getId() == id)));
	}*/
	
}
