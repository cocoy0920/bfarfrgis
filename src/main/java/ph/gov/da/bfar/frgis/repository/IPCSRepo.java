package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.ColdStoragePage;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;

@Repository
public interface IPCSRepo extends JpaRepository<IcePlantColdStorage, Integer> {

	@Query("SELECT c from IcePlantColdStorage c where c.user = :username")
	public List<IcePlantColdStorage> getListIcePlantColdStorageByUsername(@Param("username") String username);
	
	@Query("SELECT c from IcePlantColdStorage c where c.region_id = :region_id")
	public List<IcePlantColdStorage> getListIcePlantColdStorageByRegion(@Param("region_id") String region_id);
	
	@Query("SELECT c from IcePlantColdStorage c where c.region_id = :region_id and c.operator = :operator")
	public List<IcePlantColdStorage> getListIcePlantColdStorageByRegionAndType(@Param("region_id") String region_id,@Param("operator") String operator);
	
	@Query("SELECT COUNT(f) FROM IcePlantColdStorage f WHERE f.user=:username")
	public int  IcePlantColdStorageCount(@Param("username") String username);
	
	@Query("SELECT c from IcePlantColdStorage c where c.uniqueKey = :uniqueKey")
	public IcePlantColdStorage findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from IcePlantColdStorage c where c.uniqueKey = :uniqueKey")
	List<IcePlantColdStorage> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from IcePlantColdStorage c where c.operator = :operator")
	List<IcePlantColdStorage> findAllByType(@Param("operator") String operator);
	
	@Query("SELECT c from IcePlantColdStorage c where c.user = :username")
	 public List<IcePlantColdStorage> getPageableIcePlantColdStorage(@Param("username") String username,Pageable pageable);

	@Modifying(clearAutomatically = true)
	@Query("update IcePlantColdStorage f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update IcePlantColdStorage f set f.reason =:reason  where f.id =:id")
	public void updateIcePlantColdStorageByReason( @Param("id") int id, @Param("reason") String reason);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.ColdStoragePage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfIcePlant,c.lat,c.lon,c.enabled) from IcePlantColdStorage c where c.user = :user")
	public Page<ColdStoragePage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.ColdStoragePage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfIcePlant,c.lat,c.lon,c.enabled) from IcePlantColdStorage c where c.region_id = :region_id")
	public Page<ColdStoragePage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.ColdStoragePage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfIcePlant,c.lat,c.lon,c.enabled) from IcePlantColdStorage c")
	public Page<ColdStoragePage> findAllIPCS(Pageable pageable);

	@Query("SELECT COUNT(f) FROM IcePlantColdStorage f")
	public Long countAllIcePlantColdStorage();
	
	@Query("SELECT COUNT(f) FROM IcePlantColdStorage f WHERE f.region_id=:region_id")
	public Long countIcePlantColdStoragePerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM IcePlantColdStorage f WHERE f.province_id=:province_id")
	public Long countIcePlantColdStoragePerProvince(@Param("province_id") String province_id);

	@Query("SELECT COUNT(f) FROM IcePlantColdStorage f WHERE f.municipality_id=:municipality_id")
	public Long countIcePlantColdStoragePerMunicipality(@Param("municipality_id") String municipality_id);
	
	
	@Query("SELECT COUNT(f) FROM IcePlantColdStorage f WHERE f.region_id=:region_id and f.operator=:operator")
	public Long countIcePlantColdStoragePerRegion(@Param("region_id") String region_id,@Param("operator") String operator);
	
	@Query("SELECT COUNT(f) FROM IcePlantColdStorage f WHERE f.province_id=:province_id and f.operator=:operator")
	public Long countIcePlantColdStoragePerProvince(@Param("province_id") String province_id,@Param("operator") String operator);

	@Query("SELECT COUNT(f) FROM IcePlantColdStorage f WHERE f.municipality_id=:municipality_id and f.operator=:operator")
	public Long countIcePlantColdStoragePerMunicipality(@Param("municipality_id") String municipality_id,@Param("operator") String operator);
}
