package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.TrainingPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.TrainingCenter;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.TrainingCenterService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.PageUtils;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class TrainingCenterController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	TrainingCenterService trainingCenterService;
	
	 @Autowired
	 private IAuthenticationFacade authenticationFacade;
	

	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;
	@RequestMapping(value = "/trainingcenter", method = RequestMethod.GET)
	public ModelAndView trainingcenter(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();
		
		user = authenticationFacade.getUserInfo();

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "trainingcenter");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		

		result = new ModelAndView("users/form");
		return result;
	}
	@RequestMapping(value = "/trainingcenters", method = RequestMethod.GET)
	public ModelAndView trainingcenters(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();
		
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "trainingcenter");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		

		result = new ModelAndView("users/form");
		return result;
	}
	
	@RequestMapping(value = "/trainingMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
		 TrainingCenter data = trainingCenterService.findByUniqueKey(uniqueKey);
		 List<MapsData> mapsDatas = new ArrayList<>();
		 Features features = new Features();
		 features = MapBuilder.getTrainingCenterMapData(data);
		 MapsData  mapsData = new MapsData();
		 
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	mapsData.setIcon("/static/images/pin2022/BINTCenter.png");
		 	//mapsData.setImage(data.getImage_src());
		 	
		 	String html = features.getInformation();
		 	mapsData.setHtml(html);
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			
			map_data = jsonString;
		
		result = new ModelAndView("redirect:../trainingcenters");
		
	
		return result;
	}
	
	@RequestMapping(value = "/trainingDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 //trainingCenterService.deleteTrainingCenterById(id);
		 trainingCenterService.updateByEnabled(id);
		result = new ModelAndView("redirect:../trainingcenters");
		
	
		return result;
	}
	@RequestMapping(value = "/trainingDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 //trainingCenterService.deleteTrainingCenterById(id);
		 trainingCenterService.updateByEnabled(id);
		 trainingCenterService.updateTrainingCenterByReason(id, reason);
		result = new ModelAndView("redirect:../../trainingcenters");
		
	
		return result;
	}
	
	@RequestMapping(value = "/trainingMap", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "trainingcenter");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("users/map/trainingmap");
		
	
		return result;
	}
	
	
	   @RequestMapping(value = "/Posttrainingcenter", method = RequestMethod.POST)
	    public String doLogin(@Valid @ModelAttribute("TrainingForm") TrainingCenter TrainingForm,
	            BindingResult result, ModelMap model) {
	    
	    	boolean error = false;
	    	TrainingCenter center = new TrainingCenter();
	    	center = TrainingForm;
	 
	        	System.out.println("result.hasErrors(): " + TrainingForm);
	        	 if("select".equals(TrainingForm.getProvince())){
	        	        
	        	        model.addAttribute("province","Please select Province");
	        	        error = true;
	        	    }
	        	     
	        	    if("select".equals(TrainingForm.getMunicipality())){
	        	    	
	        	    	model.addAttribute("municipality","Please select Municipality");  
	        	        error = true;
	        	    }
	        	     
	        	    if("select".equals(TrainingForm.getBarangay())){
	        	    	
	        	    	model.addAttribute("barangay","Please select Barangay"); 
	        	        error = true;
	        	    }
	        	    if(PageUtils.isNullEmpty(TrainingForm.getName())){
	          	    	
	          	        error = true;
	           	    }
	        	    if(PageUtils.isNullEmpty(TrainingForm.getSpecialization())){
	          	    	
	          	        error = true;
	           	    }
	        	    if(PageUtils.isNullEmpty(TrainingForm.getFacilityWithinTheTrainingCenter())){
	          	    	
	          	        error = true;
	           	    }
	        	    if(PageUtils.isNullEmpty(TrainingForm.getType())){
	          	   
	          	        error = true;
	           	    }
	        	    if(PageUtils.isNullEmpty(TrainingForm.getDateAsOf())){
	          	    	
	          	        error = true;
	           	    }
	           	    if(PageUtils.isNullEmpty(TrainingForm.getDataSource())){
	           	    	
	    	        	        error = true;
	    	        	    } 
	           	    if(PageUtils.isNullEmpty(TrainingForm.getRemarks())){
	           	    	
	   	 	        	        error = true;
	   	 	        	    }
	           	    if(PageUtils.isNullEmpty(TrainingForm.getLat())){
	           	    	
	   	 	        	        error = true;
	   	 	        	    }
	           	    if(PageUtils.isNullEmpty(TrainingForm.getLon())){
	           	    	
	   	 	        	        error = true;
	   	 	        	    }
	           	    if(PageUtils.isNullEmpty(TrainingForm.getImage_src())){
	           	    	
	   	 	        	        error = true;
	   	 	        	    }
	           	     
	        	    if(error) {
	        	    	model.addAttribute("region", TrainingForm.getRegion());
	        			model.addAttribute("username",TrainingForm.getUser());
	        			model.addAttribute("page",TrainingForm.getPage());
	        	        
	        	        
	        	        return "users/trainingcenter";
	        	    }
	        if(!error) {
	        	savingResources(center);
	        	center = new TrainingCenter();
	        	model.put("TrainingForm", center);
	        	return "redirect:new_trainingcenter";
	        }
	        return "redirect:new_trainingcenter";
	    }

	@RequestMapping(value = "/trainingcenter/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getTrainingcenter(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<TrainingPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 15, Sort.by("id"));
	      
	          page = trainingCenterService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<TrainingPage> TrainingCenterList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setTrainingCenter(TrainingCenterList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/trainingcenterList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String gettrainingcenterList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<TrainingCenter> list = null;

		list = trainingCenterService.ListByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}


	@RequestMapping(value = "/getTraining/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getTrainingById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<TrainingCenter> mapsDatas = trainingCenterService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}

	@RequestMapping(value = "/getTraining", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postTraining(@RequestBody TrainingCenter trainingCenter) throws JsonGenerationException, JsonMappingException, IOException {

		savingResources(trainingCenter);
		

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
		

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);

	}

	private void savingResources(TrainingCenter trainingCenter) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		trainingCenter.setRegion_id(region_data[0]);
		trainingCenter.setRegion(region_data[1]);

		province = trainingCenter.getProvince();
		data = province.split(",", 2);
		trainingCenter.setProvince_id(data[0]);
		trainingCenter.setProvince(data[1]);
		trainingCenter.setEnabled(true);

		municipal = trainingCenter.getMunicipality();
		municipal_data = municipal.split(",", 2);
		trainingCenter.setMunicipality_id(municipal_data[0]);
		trainingCenter.setMunicipality(municipal_data[1]);

		barangay = trainingCenter.getBarangay();
		barangay_data = barangay.split(",", 3);
		trainingCenter.setBarangay_id(barangay_data[0]);
		trainingCenter.setBarangay(barangay_data[1]);

		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(trainingCenter.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		if (trainingCenter.getId() != 0) {
			Optional<TrainingCenter> tc_image = trainingCenterService.findById(trainingCenter.getId());
			String image_server = tc_image.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				newUniqueKeyforMap = trainingCenter.getUniqueKey();
				trainingCenter.setImage("/images/" + image_server);
				trainingCenterService.saveTrainingCenter(ResourceData.getTrainingCenter(trainingCenter, user, "", UPDATE));
			}else {
				newUniqueKeyforMap = trainingCenter.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", "");  
				trainingCenter.setImage("/images/" + imageName);
				Base64Converter.convertToImage(trainingCenter.getImage_src(), trainingCenter.getImage());
				trainingCenterService.saveTrainingCenter(ResourceData.getTrainingCenter(trainingCenter, user, "", UPDATE));
			}
			if (!trainingCenter.getImage_src().isEmpty()) {

				Base64Converter.convertToImage(trainingCenter.getImage_src(), trainingCenter.getImage());
				trainingCenterService.saveTrainingCenter(ResourceData.getTrainingCenter(trainingCenter, user, "", UPDATE));
				newUniqueKeyforMap = trainingCenter.getUniqueKey();
			} else {
				newUniqueKeyforMap = trainingCenter.getUniqueKey();
				trainingCenterService.saveTrainingCenter(ResourceData.getTrainingCenter(trainingCenter, user, "", UPDATE));

			}

			//List<MapsData> mapsDatas = mapsService.findByUniqueKey(trainingCenter.getUniqueKey());
			//mapsService.saveMapsData(ResourceMapsData.getMapDataUpdate(trainingCenter, mapsDatas));

			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			Base64Converter.convertToImage(trainingCenter.getImage_src(), trainingCenter.getImage());
			trainingCenterService.saveTrainingCenter(ResourceData.getTrainingCenter(trainingCenter, user, newUniqueKey, SAVE));
			//mapsService.saveMapsData(ResourceMapsData.getMapData(trainingCenter, user, newUniqueKey));

			save = true;

		}

	}
	@RequestMapping(value = "/getLastPageTC", method = RequestMethod.GET)
	public @ResponseBody String getLastPageTC(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = trainingCenterService.TrainingCenterCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}

	
}
