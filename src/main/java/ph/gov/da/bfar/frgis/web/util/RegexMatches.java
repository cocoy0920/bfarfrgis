package ph.gov.da.bfar.frgis.web.util;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.FishCoral;
import ph.gov.da.bfar.frgis.model.FishLanding;
import ph.gov.da.bfar.frgis.model.FishPen;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.Fishport;
import ph.gov.da.bfar.frgis.model.Frm;
import ph.gov.da.bfar.frgis.model.Hatchery;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;
import ph.gov.da.bfar.frgis.model.LGU;
import ph.gov.da.bfar.frgis.model.Mangrove;
import ph.gov.da.bfar.frgis.model.MaricultureZone;
import ph.gov.da.bfar.frgis.model.Market;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;
import ph.gov.da.bfar.frgis.model.SeaGrass;
import ph.gov.da.bfar.frgis.model.Seaweeds;
import ph.gov.da.bfar.frgis.model.TrainingCenter;

public class RegexMatches {
	
	
	public boolean validateLatLon(String page, Frm frm) {
		 boolean value = false;
		 
		 
   
   if (page.equals("fishsanctuaries")) {
		
		 FishSanctuaries sanctuaries = new FishSanctuaries();
		 sanctuaries = frm.getSanctuaries();
		 
		 String latitude="";
		 String longitude="";
		 
		
		 
		 String latpattern = "^(\\+|-)?(?:90(?:(?:\\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\\.[0-9]{1,6})?))$";
	      String lonpattern = "^(\\+|-)?(?:180(?:(?:\\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\\.[0-9]{1,6})?))$";

	      // Create a Pattern object
	      Pattern r = Pattern.compile(latpattern);
	      Pattern lon = Pattern.compile(lonpattern);
	      // Now create matcher object.
	      Matcher m = r.matcher(sanctuaries.getLat());
	      Matcher mlon = lon.matcher(sanctuaries.getLon());
	      if (m.find( )) {
	        latitude = "equal";
	      }else {
	    	  latitude = "notequal";
	      }
	      if (mlon.find( )) {
	    	  longitude = "equal";
	       }else {
	    	   longitude = "notequal";
	       }
	      
	      if(latitude.equals("equal") && longitude.equals("equal")) {
	    	  value = true;
	      }else {
	    	  value = false;
	      }
	      
	      return value;
						
	} /*else if (page.equals("fishprocessingplants")) {

		FishProcessingPlant fishProcessingPlant = new FishProcessingPlant();
		fishProcessingPlant = frm.getProcessingPlant();
		
		
		
	} else if (page.equals("fishlanding")) {

		FishLanding fishLanding = new FishLanding();
		fishLanding = frm.getFishLanding();
		
		
					
	} else if (page.equals("fishpen")) {
		
		FishPen fishPen = new FishPen();
		fishPen = frm.getFishPen();
		
	} else if (page.equals("fishpond")) {
		FishPond fishPond = new FishPond();
		fishPond = frm.getFishPond();
		
					
	} else if (page.equals("fishcage")) {
		FishCage fishCage = new FishCage();
		fishCage = frm.getFishCage();
				
	} else if (page.equals("hatchery")) {
		Hatchery hatchery = new Hatchery();
		hatchery = frm.getHatchery();
			
	} else if (page.equals("iceplantorcoldstorage")) {
		IcePlantColdStorage coldStorage = new IcePlantColdStorage();
		coldStorage = frm.getIcePlantColdStorage();
		
		
	} else if (page.equals("FISHPORT")) {

		Fishport fishport = new Fishport();
		fishport  =frm.getFishport();
		
		
	} else if (page.equals("fishcorrals")) {
		FishCoral fishCoral = new FishCoral();
		fishCoral = frm.getFishCoral();
		
		
	} else if (page.equals("marineprotected")) {
		
		MarineProtectedArea marineProtectedArea = new MarineProtectedArea();
		marineProtectedArea = frm.getMarineProtectedArea();
					
	} else if (page.equals("market")) {
		Market market = new Market();
		market = frm.getMarket();
		
	
		
	} else if (page.equals("schooloffisheries")) {
		SchoolOfFisheries schoolOfFisheries = new SchoolOfFisheries();
		schoolOfFisheries = frm.getSchoolOfFisheries();
		
		
		
	} else if (page.equals("seagrass")) {
		SeaGrass seaGrass = new SeaGrass();
		seaGrass = frm.getSeaGrass();
		
					
	} else if (page.equals("seaweeds")) {
		Seaweeds seaweeds = new Seaweeds();
		seaweeds = frm.getSeaweeds();
		
		
	} else if (page.equals("mangrove")) {
		Mangrove mangrove = new Mangrove();
		mangrove = frm.getMangrove();
		
		
		
	} else if (page.equals("lgu")) {
		LGU lgu = new LGU();
		lgu = frm.getLgu();
		
		
		
	} else if (page.equals("trainingcenter")) {
		TrainingCenter trainingCenter = new TrainingCenter();
		trainingCenter = frm.getTrainingCenter();
		
		
		
	} else if (page.equals("mariculturezone")) {
		MaricultureZone maricultureZone = new MaricultureZone();
		maricultureZone = frm.getMaricultureZone();
}*/
	
		return value;
	}
	
	public boolean validateLat(String latitude) {
		 boolean value = false;
		 
		 String latpattern = "^(\\+|-)?(?:90(?:(?:\\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\\.[0-9]{1,6})?))$";
	     
	      // Create a Pattern object
	      Pattern r = Pattern.compile(latpattern);
	      
	      // Now create matcher object.
	      Matcher m = r.matcher(latitude);
	     
	      if (m.find( )) {
	        latitude = "equal";
	      }else {
	    	  latitude = "notequal";
	      }
	    	      
	      if(latitude.equals("equal")) {
	    	  value = true;
	      }else {
	    	  value = false;
	      }
	      
	      return value;
						

	
	}
	
	public boolean validateLon(String longitude) {
		 boolean value = false;
		 
		String lonpattern = "^(\\+|-)?(?:180(?:(?:\\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\\.[0-9]{1,6})?))$";

	      
	      Pattern lon = Pattern.compile(lonpattern);
	      
	      Matcher mlon = lon.matcher(longitude);
	      
	      if (mlon.find( )) {
	    	  longitude = "equal";
	       }else {
	    	   longitude = "notequal";
	       }
	      
	      if(longitude.equals("equal")) {
	    	  value = true;
	      }else {
	    	  value = false;
	      }
	      
	      return value;
						

	
	}
	
	public boolean validateArea(String longitude) {
		 boolean value = false;
		 
		 
		 
		String lonpattern = "^(\\d+|\\d{1,3},\\d{3}|\\d{1,3},\\d{3},\\d{3}|\\d{1,3}(,\\d{3})*|\\d{1,3}(,\\d{3})*\\.\\d+)$";

	      
	      Pattern lon = Pattern.compile(lonpattern);
	      
	      Matcher mlon = lon.matcher(longitude);
	      
	      if (mlon.find( )) {
	    	  longitude = "equal";
	       }else {
	    	   longitude = "notequal";
	       }
	      
	      if(longitude.equals("equal")) {
	    	  value = true;
	      }else {
	    	  value = false;
	      }
	      
	      return value;
						

	
	}
	
	public static String getAbbr(String page) {
		 String abbr="";
		 
		 
  
  if (page.equals("fishsanctuaries")) {
	abbr = "FS";
						
	}else if (page.equals("fishprocessingplants")) {

		abbr = "FPROC";
	} else if (page.equals("fishlanding")) {

		abbr = "FL";			
	} else if (page.equals("fishpen")) {
		abbr = "FP";
		
	} else if (page.equals("fishpond")) {
		abbr = "FPD";
		
					
	} else if (page.equals("fishcage")) {
		abbr = "FC";
				
	} else if (page.equals("hatchery")) {
		abbr = "FH";
			
	} else if (page.equals("iceplantorcoldstorage")) {
		abbr = "IPCS";
		
		
	} else if (page.equals("FISHPORT")) {

		abbr = "FPR";
		
		
	} else if (page.equals("fishcorrals")) {
		abbr = "FCR";
		
		
	} else if (page.equals("marineprotected")) {
		abbr = "MPA";
					
	} else if (page.equals("market")) {
		abbr = "MKT";
		
	
		
	} else if (page.equals("schooloffisheries")) {
		abbr = "SF";
		
		
		
	} else if (page.equals("seagrass")) {
		abbr = "SG";
		
					
	} else if (page.equals("seaweeds")) {
		abbr = "SW";
		
		
	} else if (page.equals("mangrove")) {
		abbr = "MG";
		
		
		
	} else if (page.equals("lgu")) {
		abbr = "LGU";
		
		
		
	} else if (page.equals("trainingcenter")) {
		abbr = "TC";
		
		
		
	} else if (page.equals("mariculturezone")) {
		abbr = "MCZ";
}
	
		return abbr;
	}
}

