package ph.gov.da.bfar.frgis.web.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.model.Chart;
import ph.gov.da.bfar.frgis.model.Municipalities;
import ph.gov.da.bfar.frgis.model.Provinces;
import ph.gov.da.bfar.frgis.service.BarangayService;
import ph.gov.da.bfar.frgis.service.FishCageService;
import ph.gov.da.bfar.frgis.service.FishCorralService;
import ph.gov.da.bfar.frgis.service.FishLandingService;
import ph.gov.da.bfar.frgis.service.FishPenService;
import ph.gov.da.bfar.frgis.service.FishPondService;
import ph.gov.da.bfar.frgis.service.FishPortService;
import ph.gov.da.bfar.frgis.service.FishProcessingPlantService;
import ph.gov.da.bfar.frgis.service.FishSanctuariesService;
import ph.gov.da.bfar.frgis.service.HatcheryService;
import ph.gov.da.bfar.frgis.service.IcePlantColdStorageService;
import ph.gov.da.bfar.frgis.service.LGUService;
import ph.gov.da.bfar.frgis.service.MangroveService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.MaricultureZoneService;
import ph.gov.da.bfar.frgis.service.MarketService;
import ph.gov.da.bfar.frgis.service.MunicipalityService;
import ph.gov.da.bfar.frgis.service.NationalCenterService;
import ph.gov.da.bfar.frgis.service.PayaoService;
import ph.gov.da.bfar.frgis.service.ProvinceService;
import ph.gov.da.bfar.frgis.service.RegionalOfficeService;
import ph.gov.da.bfar.frgis.service.RegionsService;
import ph.gov.da.bfar.frgis.service.SchoolOfFisheriesService;
import ph.gov.da.bfar.frgis.service.SeaGrassService;
import ph.gov.da.bfar.frgis.service.SeaweedsService;
import ph.gov.da.bfar.frgis.service.TOSService;
import ph.gov.da.bfar.frgis.service.TrainingCenterService;
import ph.gov.da.bfar.frgis.service.UserService;

public class ChartsUtil {
	
	@Autowired
	UserService userService;

	
	@Autowired
	static
	ProvinceService provinceService;

	@Autowired
	static RegionsService regionsService;

	@Autowired
	static MunicipalityService municipalityService;
	
	@Autowired
	static BarangayService barangayService;
	
	@Autowired
	static MapsService mapsService;
	
	@Autowired
	static FishSanctuariesService fishSanctuariesService;
	
	@Autowired
	static FishProcessingPlantService fishProcessingPlantService;
	
	@Autowired
	static FishLandingService fishLandingService;
	
	@Autowired
	static FishPortService fishPortService;
	
	@Autowired
	static FishPenService fishPenService;
	
	@Autowired
	static FishCageService fishCageService;
	
	@Autowired
	static FishPondService fishPondService;

	@Autowired
	static IcePlantColdStorageService icePlantColdStorageService;
	
	@Autowired
	static MarketService marketService;

	@Autowired
	static SchoolOfFisheriesService schoolOfFisheriesService;
	
	@Autowired
	static FishCorralService fishCorralService;
	
	@Autowired
	static SeaGrassService seaGrassService;
	
	@Autowired
	static SeaweedsService seaweedsService;
	
	@Autowired
	static MangroveService mangroveService;
	
	@Autowired
	static LGUService lGUService;
	
	@Autowired
	static MaricultureZoneService zoneService;
	
	@Autowired
	static TrainingCenterService trainingCenterService;
	
	@Autowired
	static HatcheryService hatcheryService;
	
	@Autowired
	static NationalCenterService nationalCenterService;
	
	@Autowired
	static RegionalOfficeService regionalOfficeService;
	
	@Autowired
	static TOSService tosService;
	
	@Autowired
	static PayaoService payaoService;
	
	@Autowired
	 private static IAuthenticationFacade authenticationFacade;

	public List<Chart> getByRegion(String region_id){
		
		System.out.println("getByRegion: " + region_id);
		List<Chart> allChart = new ArrayList<Chart>();
		
		long fishcount = fishSanctuariesService.countFishSanctuariesPerRegion(region_id);
		allChart.add(new Chart("FS", authenticationFacade.getRegionName(),fishcount));
		
		long processingCount = fishProcessingPlantService.countFishProcessingPlantPerRegion(region_id);
		allChart.add(new Chart("FP", authenticationFacade.getRegionName(),processingCount));
		
		long fishlandingCount = fishLandingService.countFishLandingPerRegion(region_id);
		allChart.add(new Chart("FL", authenticationFacade.getRegionName(),fishlandingCount));
		
		long fishportCount = fishPortService.countFishportPerRegion(region_id);
		allChart.add(new Chart("FPort", authenticationFacade.getRegionName(),fishportCount));
		
		long fishpenCount = fishPenService.countFishPenPerRegion(region_id);
		allChart.add(new Chart("FPen", authenticationFacade.getRegionName(),fishpenCount));
		
		long fishcageCount = fishCageService.countFishCagePerRegion(region_id);
		allChart.add(new Chart("FCage", authenticationFacade.getRegionName(),fishcageCount));
		
		long fishcoldstorageCount = icePlantColdStorageService.countIcePlantColdStoragePerRegion(region_id);
		allChart.add(new Chart("IPCS", authenticationFacade.getRegionName(),fishcoldstorageCount));
		
		long fishmarketCount =marketService.countMarketRegion(region_id);
		allChart.add(new Chart("FMarket", authenticationFacade.getRegionName(),fishmarketCount));
		
		long fishschoolofFisheriesCount = schoolOfFisheriesService.countSchoolOfFisheriesPerRegion(region_id);
		allChart.add(new Chart("School", authenticationFacade.getRegionName(),fishschoolofFisheriesCount));
		
		long fishcoralCount = fishCorralService.countFishCoralPerRegion(region_id);
		allChart.add(new Chart("FCoral", authenticationFacade.getRegionName(),fishcoralCount));
		
		long seagrassCount = seaGrassService.countSeaGrassPerRegion(region_id);
		allChart.add(new Chart("SGrass", authenticationFacade.getRegionName(),seagrassCount));
		
		long seaweedsCount = seaweedsService.countSeaweedsPerRegion(region_id);
		allChart.add(new Chart("SGrass", authenticationFacade.getRegionName(),seagrassCount));
		
		long mangroveCount = mangroveService.countMangrovePerRegion(region_id) ;
		allChart.add(new Chart("SGrass", authenticationFacade.getRegionName(),seagrassCount));
		
		long maricultureCount = zoneService.countMariculturePerRegion(region_id);
		allChart.add(new Chart("SGrass", authenticationFacade.getRegionName(),seagrassCount));
		
		long PfoCount = lGUService.countLGUPerRegion(region_id);
		allChart.add(new Chart("SGrass", authenticationFacade.getRegionName(),seagrassCount));
		
		long trainingCount = trainingCenterService.countTrainingCenterPerRegion(region_id);
		allChart.add(new Chart("SGrass", authenticationFacade.getRegionName(),seagrassCount));
		
		long hatcheryCount = hatcheryService.countHatcheryPerRegion(region_id);
		
		long BFARhatcheryCount = hatcheryService.countHatcheryPerRegion(region_id,"BFAR");
		allChart.add(new Chart("BFAR_HATCHERY",authenticationFacade.getRegionName(),BFARhatcheryCount));
		
		long LGUhatcheryCount = hatcheryService.countHatcheryPerRegion(region_id,"LGU");
		allChart.add(new Chart("LGU_HATCHERY", authenticationFacade.getRegionName(),LGUhatcheryCount));
		
		long PRIVATEhatcheryCount = hatcheryService.countHatcheryPerRegion(region_id,"PRIVATE");
		allChart.add(new Chart("PRIVATE_HATCHERY",authenticationFacade.getRegionName(),PRIVATEhatcheryCount));
		
		return allChart;
	}
	
	public List<Chart> getChartByProvince(String province){
		System.out.println("getChartByProvince: " + province);
		String[] provinceData = province.split(",", 2);

		String province_id = provinceData[0];
		String provinceName = provinceData[1];
		
		List<Chart> provinceChart = new ArrayList<Chart>();
		
		long fishcount = fishSanctuariesService.countFishSanctuariesPerProvince(province_id);
		provinceChart.add(new Chart("FS", provinceName,fishcount));
		
		long processingCount = fishProcessingPlantService.countFishProcessingPlantPerProvince(province_id);
		provinceChart.add(new Chart("FP", provinceName,processingCount));
		
		long fishlandingCount = fishLandingService.countFishLandingPerProvince(province_id);
		provinceChart.add(new Chart("FL", provinceName,fishlandingCount));
		
		long fishportCount = fishPortService.countFishportPerProvince(province_id);
		provinceChart.add(new Chart("FPort", provinceName,fishportCount));
		
		long fishpenCount = fishPenService.countFishPenPerProvince(province_id);
		provinceChart.add(new Chart("FPen", provinceName,fishpenCount));
		
		long fishcageCount = fishCageService.countFishCagePerProvince(province_id);
		provinceChart.add(new Chart("FCage",provinceName,fishcageCount));
		
		long fishcoldstorageCount = icePlantColdStorageService.countIcePlantColdStoragePerProvince(province_id);
		provinceChart.add(new Chart("IPCS", provinceName,fishcoldstorageCount));
		
		long fishmarketCount =marketService.countMarketProvince(province_id);
		provinceChart.add(new Chart("FMarket", provinceName,fishmarketCount));
		
		long fishschoolofFisheriesCount = schoolOfFisheriesService.countSchoolOfFisheriesPerProvince(province_id);
		provinceChart.add(new Chart("School",provinceName,fishschoolofFisheriesCount));
		
		long fishcoralCount = fishCorralService.countFishCoralPerProvince(province_id);
		provinceChart.add(new Chart("FCoral",provinceName,fishcoralCount));
		
		long seagrassCount = seaGrassService.countSeaGrassPerProvince(province_id);
		provinceChart.add(new Chart("SGrass", provinceName,seagrassCount));
		
		long seaweedsNurceryCount = seaweedsService.countSeaweedsPerProvince(province_id,"SEAWEED NURSERY");
		provinceChart.add(new Chart("SNURSERY", provinceName,seaweedsNurceryCount));
		
		long seaweedsLaboratoryCount = seaweedsService.countSeaweedsPerProvince(province_id,"SEAWEED LABORATORY");
		provinceChart.add(new Chart("SLABORATORY",provinceName,seaweedsLaboratoryCount));
		
		long mangroveCount = mangroveService.countMangrovePerProvince(province_id);
		provinceChart.add(new Chart("Mangrove", provinceName,mangroveCount));
		
		long BFARmaricultureCount = zoneService.countMariculturePerProvince(province_id,"BFAR");
		provinceChart.add(new Chart("BFAR_Mariculture",provinceName,BFARmaricultureCount));
		
		long LGUmaricultureCount = zoneService.countMariculturePerProvince(province_id,"LGU");
		provinceChart.add(new Chart("LGU_Mariculture",provinceName,LGUmaricultureCount));
		
		long PRIVATEmaricultureCount = zoneService.countMariculturePerProvince(province_id,"PRIVATE");
		provinceChart.add(new Chart("PRIVATE_Mariculture",provinceName,PRIVATEmaricultureCount));
		
		long BFARhatcheryCount = hatcheryService.countHatcheryPerProvince(province_id,"BFAR");
		provinceChart.add(new Chart("BFAR_HATCHERY",provinceName,BFARhatcheryCount));
		
		long LGUhatcheryCount = hatcheryService.countHatcheryPerProvince(province_id,"LGU");
		provinceChart.add(new Chart("LGU_HATCHERY", provinceName,LGUhatcheryCount));
		
		long PRIVATEhatcheryCount = hatcheryService.countHatcheryPerProvince(province_id,"PRIVATE");
		provinceChart.add(new Chart("PRIVATE_HATCHERY",provinceName,PRIVATEhatcheryCount));
			
			
			System.out.println("getChartByProvince: " + provinceChart.toString());
			return provinceChart;
	}
		
	public List<Chart> getChartByProvince(){
		List<Chart> provinceChart = null;
	
		List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 
		for(Provinces province: provinces) {
			provinceChart = new ArrayList<Chart>();
	
			long fishcount = fishSanctuariesService.countFishSanctuariesPerProvince(province.getProvince_id());
			provinceChart.add(new Chart("FS", province.getProvince(),fishcount));
			
			long processingCount = fishProcessingPlantService.countFishProcessingPlantPerProvince(province.getProvince_id());
			provinceChart.add(new Chart("FP", province.getProvince(),processingCount));
			
			long fishlandingCount = fishLandingService.countFishLandingPerProvince(province.getProvince_id());
			provinceChart.add(new Chart("FL", province.getProvince(),fishlandingCount));
			
			long fishportCount = fishPortService.countFishportPerProvince(province.getProvince_id());
			provinceChart.add(new Chart("FPort", province.getProvince(),fishportCount));
			
			long fishpenCount = fishPenService.countFishPenPerProvince(province.getProvince_id());
			provinceChart.add(new Chart("FPen", province.getProvince(),fishpenCount));
			
			long fishcageCount = fishCageService.countFishCagePerProvince(province.getProvince_id());
			provinceChart.add(new Chart("FCage", province.getProvince(),fishcageCount));
			
			long fishcoldstorageCount = icePlantColdStorageService.countIcePlantColdStoragePerProvince(province.getProvince_id());
			provinceChart.add(new Chart("IPCS", province.getProvince(),fishcoldstorageCount));
			
			long fishmarketCount =marketService.countMarketProvince(province.getProvince_id());
			provinceChart.add(new Chart("FMarket", province.getProvince(),fishmarketCount));
			
			long fishschoolofFisheriesCount = schoolOfFisheriesService.countSchoolOfFisheriesPerProvince(province.getProvince_id());
			provinceChart.add(new Chart("School", province.getProvince(),fishschoolofFisheriesCount));
			
			long fishcoralCount = fishCorralService.countFishCoralPerProvince(province.getProvince_id());
			provinceChart.add(new Chart("FCoral", province.getProvince(),fishcoralCount));
			
			long seagrassCount = seaGrassService.countSeaGrassPerProvince(province.getProvince_id());
			provinceChart.add(new Chart("SGrass", province.getProvince(),seagrassCount));
			
			long seaweedsNurceryCount = seaweedsService.countSeaweedsPerProvince(province.getProvince_id(),"SEAWEED NURSERY");
			provinceChart.add(new Chart("SNURSERY", province.getProvince(),seaweedsNurceryCount));
			
			long seaweedsLaboratoryCount = seaweedsService.countSeaweedsPerProvince(province.getProvince_id(),"SEAWEED LABORATORY");
			provinceChart.add(new Chart("SLABORATORY", province.getProvince(),seaweedsLaboratoryCount));
			
			long mangroveCount = mangroveService.countMangrovePerProvince(province.getProvince_id());
			provinceChart.add(new Chart("Mangrove", province.getProvince(),mangroveCount));
			
			long BFARmaricultureCount = zoneService.countMariculturePerProvince(province.getProvince_id(),"BFAR");
			provinceChart.add(new Chart("BFAR_Mariculture", province.getProvince(),BFARmaricultureCount));
			
			long LGUmaricultureCount = zoneService.countMariculturePerProvince(province.getProvince_id(),"LGU");
			provinceChart.add(new Chart("LGU_Mariculture", province.getProvince(),LGUmaricultureCount));
			
			long PRIVATEmaricultureCount = zoneService.countMariculturePerProvince(province.getProvince_id(),"PRIVATE");
			provinceChart.add(new Chart("PRIVATE_Mariculture", province.getProvince(),PRIVATEmaricultureCount));
			
			long BFARhatcheryCount = hatcheryService.countHatcheryPerProvince(province.getProvince_id(),"BFAR");
			provinceChart.add(new Chart("BFAR_HATCHERY", province.getProvince(),BFARhatcheryCount));
			
			long LGUhatcheryCount = hatcheryService.countHatcheryPerProvince(province.getProvince_id(),"LGU");
			provinceChart.add(new Chart("LGU_HATCHERY", province.getProvince(),LGUhatcheryCount));
			
			long PRIVATEhatcheryCount = hatcheryService.countHatcheryPerProvince(province.getProvince_id(),"PRIVATE");
			provinceChart.add(new Chart("PRIVATE_HATCHERY", province.getProvince(),PRIVATEhatcheryCount));

		}	

			return provinceChart;
	}
	public static List<Chart> getChartByMunicipality(List<Municipalities> municipalities){
		
		List<Chart> provinceChart = new ArrayList<Chart>();
		
	
		for(Municipalities municipal: municipalities) {
			provinceChart = new ArrayList<Chart>();
	
			long fishcount = fishSanctuariesService.countFishSanctuariesPerMunicipal(municipal.getMunicipal_id());
			provinceChart.add(new Chart("FS",  municipal.getMunicipal(),fishcount));
			
			long processingCount = fishProcessingPlantService.countFishProcessingPlantPerMuunicipality(municipal.getMunicipal_id());
			provinceChart.add(new Chart("FP", municipal.getMunicipal(),processingCount));
			
//			long fishlandingCount = fishLandingService.countFishLandingPerProvince(province.getProvince_id());
//			provinceChart.add(new Chart("FL", municipal.getMunicipal(),fishlandingCount));
//			
//			long fishportCount = fishPortService.countFishportPerProvince(province.getProvince_id());
//			provinceChart.add(new Chart("FPort", municipal.getMunicipal(),fishportCount));
//			
//			long fishpenCount = fishPenService.countFishPenPerProvince(province.getProvince_id());
//			provinceChart.add(new Chart("FPen", municipal.getMunicipal(),fishpenCount));
//			
//			long fishcageCount = fishCageService.countFishCagePerProvince(province.getProvince_id());
//			provinceChart.add(new Chart("FCage", municipal.getMunicipal(),fishcageCount));
//			
//			long fishcoldstorageCount = icePlantColdStorageService.countIcePlantColdStoragePerProvince(province.getProvince_id());
//			provinceChart.add(new Chart("IPCS", municipal.getMunicipal(),fishcoldstorageCount));
//			
//			long fishmarketCount =marketService.countMarketProvince(province.getProvince_id());
//			provinceChart.add(new Chart("FMarket", municipal.getMunicipal(),fishmarketCount));
//			
//			long fishschoolofFisheriesCount = schoolOfFisheriesService.countSchoolOfFisheriesPerProvince(province.getProvince_id());
//			provinceChart.add(new Chart("School", municipal.getMunicipal(),fishschoolofFisheriesCount));
//			
//			long fishcoralCount = fishCorralService.countFishCoralPerProvince(province.getProvince_id());
//			provinceChart.add(new Chart("FCoral", municipal.getMunicipal(),fishcoralCount));
//			
//			long seagrassCount = seaGrassService.countSeaGrassPerProvince(province.getProvince_id());
//			provinceChart.add(new Chart("SGrass", municipal.getMunicipal(),seagrassCount));
//			
//			long seaweedsNurceryCount = seaweedsService.countSeaweedsPerProvince(province.getProvince_id(),"SEAWEED NURSERY");
//			provinceChart.add(new Chart("SNURSERY", municipal.getMunicipal(),seaweedsNurceryCount));
//			
//			long seaweedsLaboratoryCount = seaweedsService.countSeaweedsPerProvince(province.getProvince_id(),"SEAWEED LABORATORY");
//			provinceChart.add(new Chart("SLABORATORY", municipal.getMunicipal(),seaweedsLaboratoryCount));
//			
//			long mangroveCount = mangroveService.countMangrovePerProvince(province.getProvince_id());
//			provinceChart.add(new Chart("Mangrove", municipal.getMunicipal(),mangroveCount));
//			
//			long BFARmaricultureCount = zoneService.countMariculturePerProvince(province.getProvince_id(),"BFAR");
//			provinceChart.add(new Chart("BFAR_Mariculture", municipal.getMunicipal(),BFARmaricultureCount));
//			
//			long LGUmaricultureCount = zoneService.countMariculturePerProvince(province.getProvince_id(),"LGU");
//			provinceChart.add(new Chart("LGU_Mariculture", municipal.getMunicipal(),LGUmaricultureCount));
//			
//			long PRIVATEmaricultureCount = zoneService.countMariculturePerProvince(province.getProvince_id(),"PRIVATE");
//			provinceChart.add(new Chart("PRIVATE_Mariculture", municipal.getMunicipal(),PRIVATEmaricultureCount));
//			
//			long BFARhatcheryCount = hatcheryService.countHatcheryPerProvince(province.getProvince_id(),"BFAR");
//			provinceChart.add(new Chart("BFAR_HATCHERY", municipal.getMunicipal(),BFARhatcheryCount));
//			
//			long LGUhatcheryCount = hatcheryService.countHatcheryPerProvince(province.getProvince_id(),"LGU");
//			provinceChart.add(new Chart("LGU_HATCHERY", municipal.getMunicipal(),LGUhatcheryCount));
//			
//			long PRIVATEhatcheryCount = hatcheryService.countHatcheryPerProvince(province.getProvince_id(),"PRIVATE");
//			provinceChart.add(new Chart("PRIVATE_HATCHERY", municipal.getMunicipal(),PRIVATEhatcheryCount));

		}	
		
			System.out.println("getChartByProvince: " + provinceChart.toString());
			return provinceChart;
	}
}
