package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.SeaWeedsPage;
import ph.gov.da.bfar.frgis.model.Seaweeds;

@Repository
public interface SeaWeedsRepo extends JpaRepository<Seaweeds, Integer> {

	@Query("SELECT c from Seaweeds c where c.user = :username")
	public List<Seaweeds> getListSeaweedsByUsername(@Param("username") String username);
	
	@Query("SELECT c from Seaweeds c where c.region_id = :region_id")
	public List<Seaweeds> getListSeaweedsByRegion(@Param("region_id") String region_id);
	
	@Query("SELECT c from Seaweeds c where c.region_id = :region_id and c.type = :type")
	public List<Seaweeds> getListSeaweedsByRegionAndType(@Param("region_id") String region_id,@Param("type") String type);

	@Modifying(clearAutomatically = true)
	@Query("update Seaweeds f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update Seaweeds f set f.reason =:reason  where f.id =:id")
	public void updateSeaweedsByReason( @Param("id") int id, @Param("reason") String reason);
	
	@Query("SELECT COUNT(f) FROM Seaweeds f WHERE f.user=:username")
	public int  SeaweedsCount(@Param("username") String username);
	
	@Query("SELECT c from Seaweeds c where c.uniqueKey = :uniqueKey")
	public Seaweeds findByUniqueKey(@Param("uniqueKey") String uniqueKey);
		
	@Query("SELECT c from Seaweeds c where c.uniqueKey = :uniqueKey")
	List<Seaweeds> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from Seaweeds c where c.type = :type")
	List<Seaweeds> findAllByType(@Param("type") String type);
	
	@Query("SELECT c from Seaweeds c where c.user = :user")
	 public List<Seaweeds> getPageableSeaweeds(@Param("user") String user,Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.SeaWeedsPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.culturedMethodUsed,c.lat,c.lon,c.enabled) from Seaweeds c where c.user = :user")
	public Page<SeaWeedsPage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.SeaWeedsPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.culturedMethodUsed,c.lat,c.lon,c.enabled) from Seaweeds c where c.region_id = :region_id")
	public Page<SeaWeedsPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.SeaWeedsPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.culturedMethodUsed,c.lat,c.lon,c.enabled) from Seaweeds c")
	public Page<SeaWeedsPage> findAllSeaweeds(Pageable pageable);

	@Query("SELECT COUNT(f) FROM Seaweeds f")
	public Long countAllSeaweeds();
	
	@Query("SELECT COUNT(f) FROM Seaweeds f WHERE f.region_id=:region_id")
	public Long countSeaweedsPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM Seaweeds f WHERE f.region_id=:region_id and f.type=:type")
	public Long countSeaweedsPerRegionAndType(@Param("region_id") String region_id, @Param("type") String type);
	
	@Query("SELECT COUNT(f) FROM Seaweeds f WHERE f.province_id=:province_id")
	public Long countSeaweedsPerProvince(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM Seaweeds f WHERE f.province_id=:province_id and f.type=:type")
	public Long countSeaweedsPerProvinceAndType(@Param("province_id") String province_id, @Param("type") String type);

	@Query("SELECT COUNT(f) FROM Seaweeds f WHERE f.municipality_id=:municipality_id")
	public Long countSeaweedsPerMunicipality(@Param("municipality_id") String municipality_id);
	
	@Query("SELECT COUNT(f) FROM Seaweeds f WHERE f.municipality_id=:municipality_id and f.type=:type")
	public Long countSeaweedsPerMunicipalityAndType(@Param("municipality_id") String municipality_id, @Param("type") String type);

}
