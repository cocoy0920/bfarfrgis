package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.FishPortPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.Fishport;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.service.FishPortService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class FishPortController{

	@Autowired
	UserService userService;
	
	@Autowired
	FishPortService fishPortService;
	
	 @Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;	
	
	@RequestMapping(value = "/fishport", method = RequestMethod.GET)
	public ModelAndView FISHPORT(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();	 
		user = authenticationFacade.getUserInfo();
//		Set<UserProfile> setOfRole = new HashSet<>();
//		setOfRole = user.getUserProfiles();
//		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
//		UserProfile role = list.get(0);
		
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "fishport");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}
	@RequestMapping(value = "/fishports", method = RequestMethod.GET)
	public ModelAndView Fishports(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();

		user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);	
		model.addAttribute("page", "fishport");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}

	@RequestMapping(value = "/fishPortMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishPortByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			Fishport data = fishPortService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	
		 	Features features = new Features();
			 features = MapBuilder.getFishPortMapData(data);
			 
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	mapsData.setIcon("/static/images/pin2022/FishPort.png");
		 	
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			map_data = jsonString;
		
		result = new ModelAndView("redirect:../fishports");
		
	
		return result;
	}
	@RequestMapping(value = "/fishPortDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView fishPortByUniqueKey(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		//fishPortService.deleteFishportById(id);
		fishPortService.updateForEnabled(id);
		result = new ModelAndView("redirect:../fishports");		
		return result;
	}

	@RequestMapping(value = "/fishPortDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView fishPortByUniqueKey(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		fishPortService.updateForEnabled(id);
		fishPortService.updateFishportByReason(id, reason);
		
		result = new ModelAndView("redirect:../../fishports");		
		return result;
	}

	
	@RequestMapping(value = "/FISHPORT/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFISHPORT(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<FishPortPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 15, Sort.by("id"));
	      
	          page = fishPortService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<FishPortPage> FishportList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			//builder.append("<ul class=\"pagination pagination-sm\">");
	          builder.append("<ul class=\"list-group list-group-horizontal-sm\">");
				
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				if(number+1 == y) {
					//String numberColor = "<span class=\"btn-primary\">" + y + "</span>";
					builder.append("<li class=\"list-group-item active\"><a data-target=" + i + ">" + y + "</a></li>");
				}else {
					builder.append("<li class=\"list-group-item\"><a data-target=" + i + ">" + y + "</a></li>");
				}
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishPort(FishportList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}

	@RequestMapping(value = "/fishportList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getfishportList() throws JsonGenerationException, JsonMappingException, IOException {

	//	user = getUserInfo();
		List<Fishport> list = null;

		list = fishPortService.findByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	
	@RequestMapping(value = "/getFishPort/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishPortById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<Fishport> mapsDatas = fishPortService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}
	@RequestMapping(value = "/saveFishPort", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postFishPort(@RequestBody Fishport fishPort) throws JsonGenerationException, JsonMappingException, IOException {

		savingResources(fishPort);

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);


		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);
		
	}


	private void savingResources(Fishport fishport) {

		User user = new User();
		user = authenticationFacade.getUserInfo();

		boolean save = false;
		
		UUID newID = UUID.randomUUID();
		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		fishport.setRegion_id(region_data[0]);
		fishport.setRegion(region_data[1]);

		province = fishport.getProvince();
		data = province.split(",", 2);
		fishport.setProvince_id(data[0]);
		fishport.setProvince(data[1]);

		municipal = fishport.getMunicipality();
		municipal_data = municipal.split(",", 2);
		fishport.setMunicipality_id(municipal_data[0]);
		fishport.setMunicipality(municipal_data[1]);

		barangay = fishport.getBarangay();
		barangay_data = barangay.split(",", 3);
		fishport.setBarangay_id(barangay_data[0]);
		fishport.setBarangay(barangay_data[1]);
		fishport.setEnabled(true);
		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(fishport.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		if (fishport.getId() != 0) {
			Optional<Fishport> port = fishPortService.findById(fishport.getId());
			String image_server = port.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				fishport.setImage("/images/" + image_server);
				newUniqueKeyforMap = fishport.getUniqueKey();
				fishPortService.saveFishport(ResourceData.getFishport(fishport, user, "", UPDATE));
			}else {
				imageName.replaceAll("[^a-zA-Z0-9]", "");
				fishport.setImage("/images/" + imageName);
				newUniqueKeyforMap = fishport.getUniqueKey();
				Base64Converter.convertToImage(fishport.getImage_src(), fishport.getImage());
				fishPortService.saveFishport(ResourceData.getFishport(fishport, user, "", UPDATE));
			}
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", "");
			fishport.setImage("/images/" + imageName);
			Base64Converter.convertToImage(fishport.getImage_src(), fishport.getImage());
			fishPortService.saveFishport(ResourceData.getFishport(fishport, user, newUniqueKey, SAVE));
			
			save = true;

		}

	}
	@RequestMapping(value = "/getLastPageFPport", method = RequestMethod.GET)
	public @ResponseBody String getLastPageFPort(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = fishPortService.FishportCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}


	
}
