package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.MariculturePage;
import ph.gov.da.bfar.frgis.model.MaricultureZone;

@Repository
public interface MaricultureRepo extends JpaRepository<MaricultureZone, Integer> {

	@Query("SELECT c from MaricultureZone c where c.user = :username")
	public List<MaricultureZone> getListMaricultureZoneByUsername(@Param("username") String username);
	
	@Query("SELECT c from MaricultureZone c where c.region_id = :region_id")
	public List<MaricultureZone> getListMaricultureZoneByRegion(@Param("region_id") String region_id);
	
	@Modifying(clearAutomatically = true)
	@Query("update MaricultureZone f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update MaricultureZone f set f.reason =:reason  where f.id =:id")
	public void updateMaricultureZoneByReason( @Param("id") int id, @Param("reason") String reason);
	
	
	@Query("SELECT c from MaricultureZone c where c.region_id = :region_id and c.maricultureType = :mariculture_type")
	public List<MaricultureZone> getListMaricultureZoneByRegion(@Param("region_id") String region_id, @Param("mariculture_type") String mariculture_type);
	
	@Query("SELECT COUNT(f) FROM MaricultureZone f WHERE f.user=:username")
	public int  MaricultureZoneCount(@Param("username") String username);
	
	@Query("SELECT c from MaricultureZone c where c.uniqueKey = :uniqueKey")
	public MaricultureZone findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from MaricultureZone c where c.uniqueKey = :uniqueKey")
	List<MaricultureZone> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from MaricultureZone c where c.maricultureType = :mariculture_type")
	List<MaricultureZone> findAllByType(@Param("mariculture_type") String mariculture_type);
	
	@Query("SELECT c from MaricultureZone c where c.user = :username")
	 public List<MaricultureZone> getPageableMaricultureZone(@Param("username") String username,Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.MariculturePage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfMariculture,c.lat,c.lon,c.enabled) from MaricultureZone c where c.user = :user")
	public Page<MariculturePage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.MariculturePage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfMariculture,c.lat,c.lon,c.enabled) from MaricultureZone c where c.region_id = :region_id")
	public Page<MariculturePage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.MariculturePage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfMariculture,c.lat,c.lon,c.enabled) from MaricultureZone c")
	public Page<MariculturePage> findAllMariculture(Pageable pageable);

	@Query("SELECT COUNT(f) FROM MaricultureZone f")
	public Long countAllMaricultureZone();
	
	@Query("SELECT COUNT(f) FROM MaricultureZone f WHERE f.region_id=:region_id")
	public Long countMaricultureZonePerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM MaricultureZone f WHERE f.region_id=:region_id and f.maricultureType=:type")
	public Long countMaricultureZonePerRegion(@Param("region_id") String region_id,@Param("type") String type);

	
	@Query("SELECT COUNT(f) FROM MaricultureZone f WHERE f.province_id=:province_id")
	public Long countMaricultureZonePerProvince(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM MaricultureZone f WHERE f.province_id=:province_id and f.maricultureType=:type")
	public Long countMaricultureZonePerProvince(@Param("province_id") String province_id,@Param("type") String type);


	@Query("SELECT COUNT(f) FROM MaricultureZone f WHERE f.municipality_id=:municipality_id")
	public Long countMaricultureZonePerMunicipality(@Param("municipality_id") String municipality_id);
	
	@Query("SELECT COUNT(f) FROM MaricultureZone f WHERE f.municipality_id=:municipality_id and f.maricultureType=:type")
	public Long countMaricultureZonePerMunicipality(@Param("municipality_id") String municipality_id,@Param("type") String type);

}
