package ph.gov.da.bfar.frgis.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@Entity
@Table(name="hatchery")
public class Hatchery implements Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String page;
	
	private String user;
	
	@Column(name = "unique_key")
	private String uniqueKey;
	

	@Column(name = "date_as_of")
	private String dateAsOf;
	
	private String region;
	
	private String region_id;

	private String province;
	
	private String province_id;
	
	
	private String municipality;
	
	private String municipality_id; 
	
	
	private String barangay;
	
	private String barangay_id;
	
	
	@Column(name = "name_of_hatchery")	
	private String nameOfHatchery;
	
	
	@Column(name = "name_of_operator")
	private String nameOfOperator;
	
	
	@Column(name = "address_of_operator")	
	private String addressOfOperator;
	
	@Column(name = "operator_classification")
	private String operatorClassification;
	
	
	private String  area;
	
	@Column(name = "public_private")
	private String publicprivate;
	
	@Column(name = "indicate_species")
	private String indicateSpecies;
	
	@Column(name = "prod_stocking")
	private String prodStocking;
	
	@Column(name = "prod_cycle")
	private String prodCycle;
	
	@Column(name = "actual_prod_for_the_year")
	private String actualProdForTheYear;
	
	@Column(name = "month_start")
	private String monthStart;
	
	@Column(name = "month_end")
	private String monthEnd;
	
	
	@Column(name = "source_of_data")
	private String dataSource;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "legislated")
	private String legislated;
	
	@Column(name = "ra")
	private String ra;
	
	@Column(name = "title")
	private  String title;
	
	private String code;
	
	
	private String remarks;
	
	@Column(name = "encode_by")
	private String encodedBy;
	
	@Column(name = "edited_by")
	private String editedBy;

	@Column(name = "date_encoded")
	private String dateEncoded;
	
	@Column(name = "date_edited")
	private String dateEdited;
	
	private String last_used;
	

	private String lat;
	

	private String lon;
	
	private String image;
	private String image_src;		
	private boolean enabled;
	private String reason;
}
