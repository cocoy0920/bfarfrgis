package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.FishProcessingPlantPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.FishProcessingPlantService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
//@RequestMapping("/users")
public class FishProcessingController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	FishProcessingPlantService fishProcessingPlantService;
	
	 @Autowired
	 private IAuthenticationFacade authenticationFacade;
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	private static final Logger logger = LoggerFactory.getLogger(FishProcessingController.class);

	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;
	@RequestMapping(value = "/fishprocessingplants", method = RequestMethod.GET)
	public ModelAndView fishprocessingplants(ModelMap model)
			throws Exception{

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();
		
			user = authenticationFacade.getUserInfo();
//			Set<UserProfile> setOfRole = new HashSet<>();
//			setOfRole = user.getUserProfiles();
//			List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
//			UserProfile role = list.get(0);
			model.addAttribute("mapsData", map_data);
			model.addAttribute("user", user);
			model.addAttribute("page", "fishprocessingplants");
			model.addAttribute("username", user.getUsername());
			model.addAttribute("region", user.getRegion());
			
			result = new ModelAndView("users/form");
		return result;
	}
	
	@RequestMapping(value = "/fishprocessingplant", method = RequestMethod.GET)
	public ModelAndView fishprocessingplant(ModelMap model)
			throws Exception{

		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
			model.addAttribute("page", "fishprocessingplants");
			model.addAttribute("mapsData", map_data);
			model.addAttribute("username", user.getUsername());
			model.addAttribute("region", user.getRegion());
			
			result = new ModelAndView("users/form");
		return result;
	}
	
	
	@RequestMapping(value = "/fishProcessingMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 	ObjectMapper mapper = new ObjectMapper();
			FishProcessingPlant data = fishProcessingPlantService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	
		 	Features features = new Features();
			 features = MapBuilder.getFishProcessingPlantMapData(data);
			 
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	
		 	if(data.getOperatorClassification().equals("BFAR_MANAGED")) {
		 		mapsData.setIcon("/static/images/pin2022/Fish_Processing.png");
		 	}else if(data.getOperatorClassification().equals("LGU_MANAGED"))  {
		 		mapsData.setIcon("/static/images/pin2022/Fish_Processing_LGU.png");
		 	}else if(data.getOperatorClassification().equals("PRIVATE_SECTOR")) {
		 		mapsData.setIcon("/static/images/pin2022/Fish_Processing_PrivateSector.png");
		 	}else if(data.getOperatorClassification().equals("COOPERATIVE"))  {
		 		mapsData.setIcon("/static/images/pin2022/Fish_Processing_Cooperative.png");
		 	}
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);

			map_data= jsonString;
		result = new ModelAndView("redirect:../fishprocessingplant");
		
	
		return result;
	}
	@RequestMapping(value = "/fishProcessingDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 //fishProcessingPlantService.deleteFishProcessingById(id);
		 fishProcessingPlantService.updateForEnabled(id);
		result = new ModelAndView("redirect:../fishprocessingplant");
		
	
		return result;
	}
	@RequestMapping(value = "/fishProcessingDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 //fishProcessingPlantService.deleteFishProcessingById(id);
		 fishProcessingPlantService.updateForEnabled(id);
		 fishProcessingPlantService.updateForReason(id, reason);
		result = new ModelAndView("redirect:../../fishprocessingplant");
		
	
		return result;
	}
	
	@RequestMapping(value = "/fishProcessingMap", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "fishprocessingplants");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("users/map/fishprocessingmap");
		
	
		return result;
	}
	

	@RequestMapping(value = "/saveFishprocessingplants", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postFishProcessingPlant(@RequestBody FishProcessingPlant fishprocessingplant) throws JsonGenerationException, JsonMappingException, IOException {
	//	public @ResponseBody String postFishProcessingPlant(@RequestParam("fishprocessingplant")FishProcessingPlant[] processingPlant, HttpSession session) throws JsonGenerationException, JsonMappingException, IOException {
			
		logger.info(fishprocessingplant.toString());
		
		savingResources(fishprocessingplant);

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);

		
	}
	@RequestMapping(value = "/getFishProcessingplants/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishProcessingplantsById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<FishProcessingPlant> mapsDatas = fishProcessingPlantService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}
	
	private void savingResources(FishProcessingPlant fishProcessingPlant) {
		
		
		User user = null;
		user = authenticationFacade.getUserInfo();
		boolean save = false;
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		fishProcessingPlant.setRegion_id(region_data[0]);
		fishProcessingPlant.setRegion(region_data[1]);
		province = fishProcessingPlant.getProvince();

		
		data = province.split(",", 2);
		fishProcessingPlant.setProvince_id(data[0]);
		fishProcessingPlant.setProvince(data[1]);

		municipal = fishProcessingPlant.getMunicipality();
		municipal_data = municipal.split(",", 2);
		fishProcessingPlant.setMunicipality_id(municipal_data[0]);
		fishProcessingPlant.setMunicipality(municipal_data[1]);

		barangay = fishProcessingPlant.getBarangay();
		barangay_data = barangay.split(",", 3);
		fishProcessingPlant.setBarangay_id(barangay_data[0]);
		fishProcessingPlant.setBarangay(barangay_data[1]);
		fishProcessingPlant.setEnabled(true);
		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(fishProcessingPlant.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		

		if (fishProcessingPlant.getId() != 0) {
			Optional<FishProcessingPlant> plant = fishProcessingPlantService.findById(fishProcessingPlant.getId());
			String image_server = plant.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				fishProcessingPlant.setImage("/images/" + image_server);
				newUniqueKeyforMap = fishProcessingPlant.getUniqueKey();
				fishProcessingPlantService.saveFishProcessingPlant(ResourceData.getFishProcessingPlant(fishProcessingPlant, user, "", UPDATE));
			}else {
				newUniqueKeyforMap = fishProcessingPlant.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", "");  
				fishProcessingPlant.setImage("/images/" + imageName);
				Base64Converter.convertToImage(fishProcessingPlant.getImage_src(), fishProcessingPlant.getImage());
				fishProcessingPlantService.saveFishProcessingPlant(ResourceData.getFishProcessingPlant(fishProcessingPlant, user, "", UPDATE));
			}
				save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", "");  
			fishProcessingPlant.setImage("/images/" + imageName);
			Base64Converter.convertToImage(fishProcessingPlant.getImage_src(), fishProcessingPlant.getImage());
			fishProcessingPlantService.saveFishProcessingPlant(ResourceData.getFishProcessingPlant(fishProcessingPlant, user, newUniqueKey, SAVE));
			save = true;

		}

	}

	@RequestMapping(value = "/fishprocessingplants/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishprocessingplants(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<FishProcessingPlantPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 5, Sort.by("id"));
	      
	          page = fishProcessingPlantService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<FishProcessingPlantPage> FishProcessingPlantList = page.getContent();
	          
	          System.out.println(FishProcessingPlantList);

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishProcessingPlantPage(FishProcessingPlantList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	
	@RequestMapping(value = "/fishprocessingsList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getfishprocessingsList()
			throws JsonGenerationException, JsonMappingException, IOException {

	
		List<FishProcessingPlant> list = null;

		list = fishProcessingPlantService.ListByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	
	@RequestMapping(value = "/fishprocessingsPrintList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String fishprocessingsPrintList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishProcessingPlant> list = null;

		list = fishProcessingPlantService.ListByUsername(authenticationFacade.getUsername());

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}

	@RequestMapping(value = "/getLastPageFP", method = RequestMethod.GET)
	public @ResponseBody String getLastPageFP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = fishProcessingPlantService.FishProcessingPlantCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}


	
}
