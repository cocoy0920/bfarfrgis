package ph.gov.da.bfar.frgis.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@Entity
@Table(name="mapdata")
public class MapsData  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	//@NotEmpty
	@Column(name="user")
	private String user;
	
	//@NotEmpty
	@Column(name = "unique_key")
	private String uniqueKey;
	
	//@NotEmpty
	@Column(name="page")
	private String page;
	
	//@NotEmpty
	@Column(name="lat")
	private String lat;
	
	//@NotEmpty
	@Column(name="lon")
	private String lon;
	
	//@NotEmpty
	@Column(name="html")
	private String html;
	
	//@NotEmpty
	@Column(name="zoom")
	private String zoom;
	
	//@NotEmpty
	@Column(name="icon")
	private String icon;
	
	@Column(name="image")
	private String image;
	
	//@NotEmpty
	@Column(name="region")
	private String region;
	
	@Column(name="region_id")
	private String region_id;
	
	//@NotEmpty
	@Column(name="province")
	private String  province;
	
	@Column(name="province_id")
	private String  province_id;
	
	//@NotEmpty
	@Column(name="municipality")
	private String municipality;
	
	@Column(name="municipality_id")
	private String municipality_id;
	
	//@NotEmpty
	@Column(name="barangay")
	private String barangay;
	
	@Column(name="legislated")
	private String legislated;
	
}
