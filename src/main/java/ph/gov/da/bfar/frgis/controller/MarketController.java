package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.MarketPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.Market;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.MarketService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class MarketController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	MarketService marketService;
	
	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";

	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;	
	
	@RequestMapping(value = "/market", method = RequestMethod.GET)
	public ModelAndView market(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();
		
		user = authenticationFacade.getUserInfo();

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "market");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("users/form");
		return result;
	}
	@RequestMapping(value = "/markets", method = RequestMethod.GET)
	public ModelAndView markets(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "market");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("users/form");
		return result;
	}

	@RequestMapping(value = "/marketMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			Market data = marketService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	
		 	Features features = new Features();
			 features = MapBuilder.getMarketMapData(data);
		 	
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	mapsData.setIcon("/static/images/pin2022/Market.png");
		 	
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			map_data  =jsonString;
		
		result = new ModelAndView("redirect:../markets");
		
	
		return result;
	}

	@RequestMapping(value = "/marketDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		// marketService.deleteMarketById(id);
		 marketService.updateByEnabled(id);
		result = new ModelAndView("redirect:../markets");
		
	
		return result;
	}

	@RequestMapping(value = "/marketDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView marketDeleteById(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		// marketService.deleteMarketById(id);
		 marketService.updateByEnabled(id);
		 marketService.updateMarketeByReason(id, reason);
		result = new ModelAndView("redirect:../../markets");
		
	
		return result;
	}

	@RequestMapping(value = "/marketMap", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "market");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("users/map/marketmap");
		
	
		return result;
	}

	@RequestMapping(value = "/market/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMarket(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<MarketPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 5, Sort.by("id"));
	      
	          page = marketService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<MarketPage> MarketList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setMarket(MarketList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}

	@RequestMapping(value = "/marketList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getmarketList() throws JsonGenerationException, JsonMappingException, IOException {

		//user = getUserInfo();
		List<Market> list = null;

		list = marketService.ListByUsername(authenticationFacade.getUsername());

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	@RequestMapping(value = "/getMarket/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMarketById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<Market> mapsDatas = marketService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}
	@RequestMapping(value = "/saveMarket", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postMarket(@RequestBody Market market) throws JsonGenerationException, JsonMappingException, IOException {

		savingResources(market);

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
		

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);
	}

/*	@RequestMapping(value = "/getMarket", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMarket(@RequestBody Market market) throws JsonGenerationException, JsonMappingException, IOException {

		List<Market> list = null;
		int pageIndex = 1;
		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;
		int page = (pageIndex * numberOfRecordsPerPage) - numberOfRecordsPerPage;

		totalNumberOfRecords = marketService.MarketCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		StringBuilder builder = new StringBuilder();
		builder.append("<ul class=\"pagination pagination-sm\">");

		for (int i = 1; i <= noOfPages; i++) {
			builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + i + "</a></li>");
		}
		builder.append("</ul>");

		list = marketService.getAllMarkets(page, numberOfRecordsPerPage, authenticationFacade.getUsername());
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setMarket(list);
		resourcesPaginations.setPageNumber(pageNumber);

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);
	}
*/
	private void savingResources(Market market) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		market.setRegion_id(region_data[0]);
		market.setRegion(region_data[1]);

		province = market.getProvince();
		data = province.split(",", 2);
		market.setProvince_id(data[0]);
		market.setProvince(data[1]);

		municipal = market.getMunicipality();
		municipal_data = municipal.split(",", 2);
		market.setMunicipality_id(municipal_data[0]);
		market.setMunicipality(municipal_data[1]);

		barangay = market.getBarangay();
		barangay_data = barangay.split(",", 3);
		market.setBarangay_id(barangay_data[0]);
		market.setBarangay(barangay_data[1]);
		market.setEnabled(true);

		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(market.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		if (market.getId() != 0) {
			Optional<Market> mar = marketService.findById(market.getId());
			String image_server = mar.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				market.setImage("/images/" + image_server);
				newUniqueKeyforMap = market.getUniqueKey();
				marketService.saveMarket(ResourceData.getMarket(market, user, "", UPDATE));
			}else {
				newUniqueKeyforMap = market.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", "");
				market.setImage("/images/" + imageName);
				Base64Converter.convertToImage(market.getImage_src(), market.getImage());
				marketService.saveMarket(ResourceData.getMarket(market, user, "", UPDATE));
			}
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", "");
			market.setImage("/images/" + imageName);
			Base64Converter.convertToImage(market.getImage_src(), market.getImage());
			marketService.saveMarket(ResourceData.getMarket(market, user, newUniqueKey, SAVE));
			save = true;

		}

	}
	@RequestMapping(value = "/getLastPageMarket", method = RequestMethod.GET)
	public @ResponseBody String getLastPageMarket(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = marketService.MarketCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}


}
