package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.RegionalOfficePage;
import ph.gov.da.bfar.frgis.model.RegionalOffice;
import ph.gov.da.bfar.frgis.repository.RegionalOfficeRepo;



@Service("RegionalOfficeService")
@Transactional
public class RegionalOfficeServiceImpl implements RegionalOfficeService{

	@Autowired
	private RegionalOfficeRepo dao;

	@Override
	public Optional<RegionalOffice> findById(int id) {

		return dao.findById(id);
	}

	@Override
	public List<RegionalOffice> ListByUsername(String username) {

		return dao.getListRegionalOfficeByUsername(username);
	}

	@Override
	public List<RegionalOffice> ListByRegion(String region_id) {

		return dao.getListRegionalOfficeByRegion(region_id);
	}

	@Override
	public RegionalOffice findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<RegionalOffice> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public void saveRegionalOffice(RegionalOffice RegionalOffice) {
		dao.save(RegionalOffice);
		
	}

	@Override
	public void deleteRegionalOfficeById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateByEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateRegionalOfficeByReason(int id, String reason) {
		dao.updateRegionalOfficeByReason(id, reason);
		
	}

	@Override
	public List<RegionalOffice> findAllRegionalOffice() {

		//return dao.findAll(Sort.by(Sort.Direction.ASC, "id"));
		return dao.findAll();

	}

	@Override
	public List<RegionalOffice> getAllRegionalOffice(int index, int pagesize, String user) {
		
		return dao.findAll();
	}

	@Override
	public int RegionalOfficeCount(String username) {
		
		return dao.RegionalOfficeCount(username);
	}

	@Override
	public Page<RegionalOfficePage> findByUsername(String user, Pageable pageable) {
		
		return dao.findByUsername(user, pageable);
	}

	@Override
	public Page<RegionalOfficePage> findByRegion(String region, Pageable pageable) {
		
		return dao.findByRegion(region, pageable);
	}

	@Override
	public Page<RegionalOfficePage> findAllRegionalOffice(Pageable pageable) {
		
		return dao.findAllRegionalOffice(pageable);
	}

	@Override
	public Long countAllRegionalOffice() {
		
		return dao.countAllRegionalOffice();
	}

	@Override
	public Long countRegionalOfficePerRegion(String region_id) {
		
		return dao.countRegionalOfficePerRegion(region_id);
	}

	@Override
	public Long countRegionalOfficeProvince(String province_id) {

		return dao.countRegionalOfficePerProvince(province_id);
	}


	
	

	
}
