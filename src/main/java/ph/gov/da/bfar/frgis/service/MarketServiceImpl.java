package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.MarketPage;
import ph.gov.da.bfar.frgis.model.Market;
import ph.gov.da.bfar.frgis.repository.MarketRepo;


@Service("MarketService")
@Transactional
public class MarketServiceImpl implements MarketService{

	@Autowired
	private MarketRepo dao;

	
	public Optional<Market> findById(int id) {
		return dao.findById(id);
	}

	public List<Market> ListByUsername(String sso) {
		List<Market>Market = dao.getListMarketByUsername(sso);
		return Market;
	}

	public void saveMarket(Market Market) {
		dao.save(Market);
	}

	@Override
	public void deleteMarketById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateByEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateMarketeByReason(int id, String reason) {
		dao.updateMarketByReason(id, reason);
		
	}

	public List<Market> findAllMarkets() {
		return dao.findAll();
	}

	@Override
	public List<Market> getAllMarkets(int index, int pagesize, String user) {
		
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<Market> page = dao.getPageableMarkets(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int MarketCount(String username) {
		return dao.MarketCount(username);
	}

	@Override
	public Market findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<Market> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<MarketPage> findByUsername(String user, Pageable pageable) {
		
		return dao.findByUsername(user, pageable);
	}
	
	@Override
	public Page<MarketPage> findAllMarket(Pageable pageable) {
		
		return dao.findAllMarket(pageable);
	}
	
	@Override
	public Page<MarketPage> findByRegion(String region, Pageable pageable) {

		return dao.findByRegion(region, pageable);
	}

	@Override
	public List<Market> ListByRegion(String region_id) {
		
		return dao.getListMarketByRegion(region_id);
	}

	@Override
	public Long countMarketRegion(String region_id) {

		return dao.countMarketRegion(region_id);
	}

	@Override
	public Long countMarketProvince(String province_id) {
	
		return dao.countMarketPerProvince(province_id);
	}

	@Override
	public Long countAllMarket() {

		return dao.countAllMarket();
	}

	@Override
	public Long countMarketMunicipality(String municipality_id) {
		return dao.countMarketPerMunicipality(municipality_id);
	}


}
