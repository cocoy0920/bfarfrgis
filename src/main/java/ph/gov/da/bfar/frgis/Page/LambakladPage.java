package ph.gov.da.bfar.frgis.Page;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class LambakladPage {
	
	
	private int id;

	private String uniqueKey;

	private String region;
	
	private String province;
	
	private String municipality;

	private String barangay;

	
	private String lat;

	private String lon;
	
	private boolean enabled;

	public LambakladPage(int id, String uniqueKey, String region, String province, String municipality, String barangay,
			String lat, String lon, boolean enabled) {
		super();
		this.id = id;
		this.uniqueKey = uniqueKey;
		this.region = region;
		this.province = province;
		this.municipality = municipality;
		this.barangay = barangay;
		this.lat = lat;
		this.lon = lon;
		this.enabled = enabled;
	}



}
