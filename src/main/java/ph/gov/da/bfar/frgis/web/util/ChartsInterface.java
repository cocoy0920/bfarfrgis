package ph.gov.da.bfar.frgis.web.util;

import java.util.List;

import ph.gov.da.bfar.frgis.model.Chart;
import ph.gov.da.bfar.frgis.model.Municipalities;
import ph.gov.da.bfar.frgis.model.Provinces;
import ph.gov.da.bfar.frgis.model.Regions;

public interface ChartsInterface {
	
	public long getFishpenCountByRegion(String region);
	public long getFishpondCountByRegion(String region);
	public long getFishcageCountByRegion(String region);
	public long getHatcheryCountByRegion(String region);
	public long getFishportCountByRegion(String region);
	public long getFishcoralCountByRegion(String region);
	public long getMaricultureCountByRegion(String region);
	public long getMarketCountByRegion(String region);
	public long getSchoolCountByRegion(String region);
	public long getSeagrassCountByRegion(String region);
	public long getSeaweedsCountByRegion(String region);
	public long getMangroveCountByRegion(String region);
	public long getPFOCountByRegion(String region);
	public long getTrainingCenterCountByRegion(String region);
	public long getPayaoCountByRegion(String region);
	public long getLambakladCountByRegion(String region);
	public long getTOSByRegion(String region);
	public List<Chart> getAquafarmChartByRegion(List<Regions> regions );
	public List<Chart> getFishPenChartByRegion(List<Regions> regions );
	public List<Chart> getAquafarmChartByProvince(List<Provinces> provinces );
	public List<Chart> getAquafarmChartByMunicipality(List<Municipalities> municipalities );
	
	public List<Chart> getHatcheryChartByRegion(List<Regions> regions );
	public List<Chart> getHatcheryChartByProvince(List<Provinces> provinces );
	public List<Chart> getHatcheryChartByMunicipality(List<Municipalities> municipalities );
	
	
	
	public List<Chart> getMaricultureChartByRegion(List<Regions> regions );
	public List<Chart> getMaricultureChartByProvince(List<Provinces> provinces );
	public List<Chart> getMaricultureChartByMunicipality(List<Municipalities> municipalities );
	
	public List<Chart> getSeaweedsChartByRegion(List<Regions> regions );
	public List<Chart> getSeaweedsChartByProvince(List<Provinces> provinces );
	public List<Chart> getSeaweedsChartByMunicipality(List<Municipalities> municipalities );
	
	public long getColdCountByRegion(String region);
	public List<Chart> getColdChartByRegion(List<Regions> regions );
	public List<Chart> getColdChartByProvince(List<Provinces> provinces );
	public List<Chart> getColdChartByMunicipality(List<Municipalities> municipalities );
	
	public long getFishlandingCountByRegion(String region );
	public List<Chart> getFishlandingChartByRegion(List<Regions> regions );
	public List<Chart> getFishlandingChartByProvince(List<Provinces> provinces );
	public List<Chart> getFishlandingChartByMunicipality(List<Municipalities> municipalities );
	
	public long getFishProcessingCountByRegion(String regions );
	public List<Chart> getFishProcessingChartByRegion(List<Regions> regions );
	public List<Chart> getFishProcessingChartByProvince(List<Provinces> provinces );
	public List<Chart> getFishProcessingChartByMunicipality(List<Municipalities> municipalities );
	
	public List<Chart> getSeaweedsWDChartByRegion(List<Regions> regions );
	public List<Chart> getSeaweedsWDChartByProvince(List<Provinces> provinces );
	public List<Chart> getSeaweedsWDChartByMunicipality(List<Municipalities> municipalities );
	
	public List<Chart> getMarketChartByRegion(List<Regions> regions );
	public List<Chart> getMarketChartByProvince(List<Provinces> provinces );
	public List<Chart> getMarketChartByMunicipality(List<Municipalities> municipalities );
	
	public List<Chart> getSeaGrassChartByRegion(List<Regions> regions );
	public List<Chart> getSeaGrassChartByProvince(List<Provinces> provinces );
	public List<Chart> getSeaGrassChartByMunicipality(List<Municipalities> municipalities );
	
	public List<Chart> getFishPortChartByRegion(List<Regions> regions );
	public List<Chart> getFishPortChartByProvince(List<Provinces> provinces );
	public List<Chart> getFishPortChartByMunicipality(List<Municipalities> municipalities );
	
	public List<Chart> getMangroveChartByRegion(List<Regions> regions );
	public List<Chart> getMangroveChartByProvince(List<Provinces> provinces );
	public List<Chart> getMangroveChartByMunicipality(List<Municipalities> municipalities );
	
	public long  getFishSanctuaryCountByRegion(String region);
	public List<Chart> getFishSanctuaryChartByRegion(List<Regions> regions );
	public List<Chart> getFishSanctuaryChartByProvince(List<Provinces> provinces );
	public List<Chart> getFishSanctuaryChartByMunicipality(List<Municipalities> municipalities );
	
	public List<Chart> getPayaoChartByRegion(List<Regions> regions );
	public List<Chart> getPayaoChartByProvince(List<Provinces> provinces );
	public List<Chart> getPayaoChartByMunicipality(List<Municipalities> municipalities );
	
	public List<Chart> getLambakladChartByRegion(List<Regions> regions );
	public List<Chart> getLambakladChartByProvince(List<Provinces> provinces );
	public List<Chart> getLambakladChartByMunicipality(List<Municipalities> municipalities );
	
	public List<Chart> getFRPChartByRegion(List<Regions> regions );
	public List<Chart> getFRPChartByProvince(List<Provinces> provinces );
	public List<Chart> getFRPChartByMunicipality(List<Municipalities> municipalities );
	
	public List<Chart> getCoralReefChartByRegion(List<Regions> regions );
	public List<Chart> getCoralReefChartByProvince(List<Provinces> provinces );
	public List<Chart> getCoralReefChartByMunicipality(List<Municipalities> municipalities );
}
