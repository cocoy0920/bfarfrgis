package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.MarketPage;
import ph.gov.da.bfar.frgis.model.Market;

@Repository
public interface MarketRepo extends JpaRepository<Market, Integer> {

	@Query("SELECT c from Market c where c.user = :username")
	public List<Market> getListMarketByUsername(@Param("username") String username);

	@Query("SELECT c from Market c where c.region_id = :region_id")
	public List<Market> getListMarketByRegion(@Param("region_id") String region_id);

	@Modifying(clearAutomatically = true)
	@Query("update Market f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update Market f set f.reason =:reason  where f.id =:id")
	public void updateMarketByReason( @Param("id") int id, @Param("reason") String reason);
	
	@Query("SELECT COUNT(f) FROM Market f WHERE f.user=:username")
	public int  MarketCount(@Param("username") String username);
	
	@Query("SELECT c from Market c where c.uniqueKey = :uniqueKey")
	public Market findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from Market c where c.uniqueKey = :uniqueKey")
	List<Market> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from Market c where c.user = :user")
	 public List<Market> getPageableMarkets(@Param("user") String user,Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.MarketPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfMarket,c.lat,c.lon,c.enabled) from Market c where c.user = :user ")
	public Page<MarketPage> findByUsername(@Param("user") String user, Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.MarketPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfMarket,c.lat,c.lon,c.enabled) from Market c where c.region_id = :region_id")
	public Page<MarketPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.MarketPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfMarket,c.lat,c.lon,c.enabled) from Market c")
	public Page<MarketPage> findAllMarket(Pageable pageable);


	@Query("SELECT COUNT(f) FROM Market f")
	public Long countAllMarket();
	
	@Query("SELECT COUNT(f) FROM Market f WHERE f.region_id=:region_id")
	public Long countMarketRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM Market f WHERE f.province_id=:province_id")
	public Long countMarketPerProvince(@Param("province_id") String province_id);

	@Query("SELECT COUNT(f) FROM Market f WHERE f.municipality_id=:municipality_id")
	public Long countMarketPerMunicipality(@Param("municipality_id") String municipality_id);
}
