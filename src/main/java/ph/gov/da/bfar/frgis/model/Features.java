
package ph.gov.da.bfar.frgis.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.service.FishSanctuariesService;


//{
//"type": "FeatureCollection",
//"name": "Status of Legislated Hatcheries",
//"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
//"features": [
//{ "type": "Feature", "properties": { "RANo": "RA 10787", "Location": "Lingig, Surigao del Sur", "Status": "Ongoing Construction", "Latitude": 8.038053, "Longitude": 126.412664, "F7": null }, "geometry": { "type": "Point", "coordinates": [ 126.412664, 8.038053 ] } }package ph.gov.da.bfar.frgis.model;

public class Features {
	

	private String Location;
	private String area;
	private String name;
	private String image;
	private String resources;
	private String raNo;
	private String status;
	private String legislated;
	private String title;
	private String longtitude;
	private String latitude;
	private String region;
	private String region_id;
	private String province;
	private String province_id;
	private String municipality;
	private String municipality_id;
	private String barangay;
	private String barangay_id;
	private String information;
	private String type;
	private String cflcStatus;
	private String operator;
	private String direction;


public String getLocation() {
		return Location;
	}



	public void setLocation(String location) {
		Location = location;
	}



	public String getArea() {
		return area;
	}



	public void setArea(String area) {
		this.area = area;
	}



	public String getLongtitude() {
		return longtitude;
	}



	public void setLongtitude(String longtitude) {
		this.longtitude = longtitude;
	}



	public String getLatitude() {
		return latitude;
	}



	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}



public String getRegion() {
		return region;
	}



	public void setRegion(String region) {
		this.region = region;
	}



	public String getProvince() {
		return province;
	}



	public void setProvince(String province) {
		this.province = province;
	}



	public String getMunicipality() {
		return municipality;
	}



	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}



	public String getBarangay() {
		return barangay;
	}



	public void setBarangay(String barangay) {
		this.barangay = barangay;
	}
	
	



public String getLegislated() {
		return legislated;
	}



	public void setLegislated(String legislated) {
		this.legislated = legislated;
	}



public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getImage() {
		return image;
	}



	public void setImage(String image) {
		this.image = image;
	}



public String getRegion_id() {
		return region_id;
	}



	public void setRegion_id(String region_id) {
		this.region_id = region_id;
	}



	public String getProvince_id() {
		return province_id;
	}



	public void setProvince_id(String province_id) {
		this.province_id = province_id;
	}



public String getResources() {
		return resources;
	}



	public void setResources(String resources) {
		this.resources = resources;
	}



	public String getRaNo() {
		return raNo;
	}



	public void setRaNo(String raNo) {
		this.raNo = raNo;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getMunicipality_id() {
		return municipality_id;
	}



	public void setMunicipality_id(String municipality_id) {
		this.municipality_id = municipality_id;
	}



	public String getBarangay_id() {
		return barangay_id;
	}



	public void setBarangay_id(String barangay_id) {
		this.barangay_id = barangay_id;
	}



public String getInformation() {
		return information;
	}



	public void setInformation(String information) {
		this.information = information;
	}

public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

public String getCflcStatus() {
		return cflcStatus;
	}



	public void setCflcStatus(String cflcStatus) {
		this.cflcStatus = cflcStatus;
	}

public String getOperator() {
		return operator;
	}



	public void setOperator(String operator) {
		this.operator = operator;
	}



public String getDirection() {
		return direction;
	}



	public void setDirection(String direction) {
		this.direction = direction;
	}



public static String toJson(List<FishSanctuaries> list){

	//ArrayList<FishSanctuaries> eachElement= new ArrayList<FishSanctuaries>();
	  JSONObject featureCollection = new JSONObject();


	 // JSONArray eachElement = new JSONArray();
	 //.put("type", "Feature");
	  
	  JSONArray features = new JSONArray();
	  JSONObject feature = new JSONObject();
	  feature.put("type", "Feature");
	 // JSONObject geometry = new JSONObject();
	  
	 // JSONArray JSONArrayCoord = new JSONArray();
	  for(FishSanctuaries eachElement : list){
		  System.out.println(eachElement);
	 
	    	JSONObject geometry = new JSONObject();
	    	JSONObject featuresProperties = new JSONObject();
	        JSONArray JSONArrayCoord = new JSONArray();
	        JSONObject newFeature = new JSONObject();

	        JSONArrayCoord.put(0, eachElement.getLon());
	        JSONArrayCoord.put(1, eachElement.getLat());

	        geometry.put("type", "Point");
	        geometry.put("coordinates", JSONArrayCoord);
	        
	        featuresProperties.put("Latitude", eachElement.getLat());
	        featuresProperties.put("Longitude", eachElement.getLon());
	        featuresProperties.put("Area", eachElement.getArea());
	        featuresProperties.put("Location", eachElement.getRegion() + "," + eachElement.getProvince() + "," + eachElement.getMunicipality() + "," + eachElement.getBarangay());	       
	      
	        feature.put("properties", featuresProperties);
	        feature.put("geometry", geometry);
	        features.put(feature);
	       // features.put(newFeature);
	        featureCollection.put("features", features);
	  
	  }
	
		 featureCollection.put("type", "FeatureCollection");
		  JSONObject properties = new JSONObject();
		  properties.put("name", "urn:ogc:def:crs:OGC:1.3:CRS84");
		  JSONObject crs = new JSONObject();
		  crs.put("type", "name");
		  crs.put("properties", properties);
		  featureCollection.put("crs", crs);
		  
	  return featureCollection.toString();
	}
}


	

