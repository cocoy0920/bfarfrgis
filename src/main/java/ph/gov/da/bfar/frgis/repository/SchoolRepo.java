package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.SchoolPage;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;

@Repository
public interface SchoolRepo extends JpaRepository<SchoolOfFisheries, Integer> {

	@Query("SELECT c from SchoolOfFisheries c where c.user = :username")
	public List<SchoolOfFisheries> getListSchoolOfFisheriesByUsername(@Param("username") String username);
	
	@Query("SELECT c from SchoolOfFisheries c where c.region_id = :region_id")
	public List<SchoolOfFisheries> getListSchoolOfFisheriesByRegion(@Param("region_id") String region_id);
	
	@Modifying(clearAutomatically = true)
	@Query("update SchoolOfFisheries f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update SchoolOfFisheries f set f.reason =:reason  where f.id =:id")
	public void updateSchoolOfFisheriesByReason( @Param("id") int id, @Param("reason") String reason);
	
	
	@Query("SELECT COUNT(f) FROM SchoolOfFisheries f WHERE f.user=:username")
	public int  SchoolOfFisheriesCount(@Param("username") String username);
	
	@Query("SELECT c from SchoolOfFisheries c where c.uniqueKey = :uniqueKey")
	public SchoolOfFisheries findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from SchoolOfFisheries c where c.uniqueKey = :uniqueKey")
	List<SchoolOfFisheries> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from SchoolOfFisheries c where c.user = :username")
	 public List<SchoolOfFisheries> getPageableSchoolOfFisheries(@Param("username") String username,Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.SchoolPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.name,c.lat,c.lon,c.enabled) from SchoolOfFisheries c where c.user = :user")
	public Page<SchoolPage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.SchoolPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.name,c.lat,c.lon,c.enabled) from SchoolOfFisheries c where c.region_id = :region_id")
	public Page<SchoolPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.SchoolPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.name,c.lat,c.lon,c.enabled) from SchoolOfFisheries c")
	public Page<SchoolPage> findAllSchool(Pageable pageable);

	@Query("SELECT COUNT(f) FROM SchoolOfFisheries f")
	public Long countAllSchoolOfFisheries();
	
	@Query("SELECT COUNT(f) FROM SchoolOfFisheries f WHERE f.region_id=:region_id")
	public Long countSchoolOfFisheriesPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM SchoolOfFisheries f WHERE f.province_id=:province_id")
	public Long countSchoolOfFisheriesPerProvince(@Param("province_id") String province_id);

	@Query("SELECT COUNT(f) FROM SchoolOfFisheries f WHERE f.municipality_id=:municipality_id")
	public Long countSchoolOfFisheriesPerMunicipality(@Param("municipality_id") String municipality_id);
}
