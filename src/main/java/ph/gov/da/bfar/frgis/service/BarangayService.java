package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import ph.gov.da.bfar.frgis.model.Barangay;


public interface BarangayService {

	public void saveBarangay(Barangay barangay);
	public Optional<Barangay> findById(int id);
	
	public List<Barangay> findByMunicipality(String municipal);
	public  List<Barangay> findAllBarangays();
	
	

}

