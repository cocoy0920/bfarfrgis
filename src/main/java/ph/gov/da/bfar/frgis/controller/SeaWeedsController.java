package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.SeaWeedsPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.Seaweeds;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.SeaweedsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class SeaWeedsController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	SeaweedsService seaweedsService;
	
	 @Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";	
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;
	
	@RequestMapping(value = "/seaweeds", method = RequestMethod.GET)
	public ModelAndView Newseaweeds(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();
		
		user = authenticationFacade.getUserInfo();

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "tos");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		
		result = new ModelAndView("users/form");

		return result;
	}
	@RequestMapping(value = "/seaweed", method = RequestMethod.GET)
	public ModelAndView seaweed(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "tos");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		
		result = new ModelAndView("users/form");

		return result;
	}

	@RequestMapping(value = "/seaweedsMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView seaWeedsMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			Seaweeds data = seaweedsService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	Features features = new Features();
			features = MapBuilder.getSeaweedsMapData(data);
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	
		 	String Type  = data.getType();
		 	
		 	if(Type.equals("SEAWEEDLABORATORY")) {
		 		mapsData.setIcon("/static/images/pin2022/Seaweeds_laboratory.png");
		 	}else if(Type.equals("SEAWEEDNURSERY")) {
		 		mapsData.setIcon("/static/images/pin2022/Seaweeds_Nursery.png");
		 	}else if(Type.equals("SEAWEEDWAREHOUSE")) {
		 		mapsData.setIcon("/static/images/pin2022/seaweeds_warehouse.png");
		 	}else if(Type.equals("SEAWEEDDRYER")) {
		 		mapsData.setIcon("/static/images/pin2022/seaweeds_dryer.png");
		 	}
		 	
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);

			map_data= jsonString;
			result = new ModelAndView("redirect:../seaweed");
		
	
		return result;
	}
	@RequestMapping(value = "/seaweedsDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView seaWeedsMapByUniqueKey(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 	//seaweedsService.deleteSeaweedsById(id);
		 	seaweedsService.updateByEnabled(id);
			result = new ModelAndView("redirect:../seaweed");
		
	
		return result;
	}
	@RequestMapping(value = "/seaweedsDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView seaWeedsMapByUniqueKey(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 	//seaweedsService.deleteSeaweedsById(id);
		 	seaweedsService.updateByEnabled(id);
			result = new ModelAndView("redirect:../../seaweed");
		
	
		return result;
	}
	
	@RequestMapping(value = "/seaweedsMap", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "seaweeds");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("users/map/seaweedsmap");
		
	
		return result;
	}
	
	@RequestMapping(value = "/seaweeds/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSeaweeds(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<SeaWeedsPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 5, Sort.by("id"));
	      
	          page = seaweedsService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<SeaWeedsPage> SeaweedsList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setSeaweeds(SeaweedsList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/seaweedsList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getseaweedsList() throws JsonGenerationException, JsonMappingException, IOException {

		List<Seaweeds> list = null;

		list = seaweedsService.ListByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}

	@RequestMapping(value = "/getSeaweeds/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSeaweedsById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);
		ObjectMapper mapper = new ObjectMapper();

		List<Seaweeds> mapsDatas = seaweedsService.findByUniqueKeyForUpdate(uniqueId);
		String jsonString =  mapper.writeValueAsString(mapsDatas);
		//Gson gson = new GsonBuilder().setPrettyPrinting().create();

		//String jsonArray = gson.toJson(mapsDatas);

		return jsonString;

	}

	@RequestMapping(value = "/saveSeaWeeds", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postSeaWeeds(@RequestBody Seaweeds seaweeds) throws JsonGenerationException, JsonMappingException, IOException {

		savingResources(seaweeds);
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);
	}

	private void savingResources(Seaweeds seaweeds) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		boolean save = false;
		UUID newID = UUID.randomUUID();
		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		seaweeds.setRegion_id(region_data[0]);
		seaweeds.setRegion(region_data[1]);

		province = seaweeds.getProvince();
		data = province.split(",", 2);
		seaweeds.setProvince_id(data[0]);
		seaweeds.setProvince(data[1]);

		municipal = seaweeds.getMunicipality();
		municipal_data = municipal.split(",", 2);
		seaweeds.setMunicipality_id(municipal_data[0]);
		seaweeds.setMunicipality(municipal_data[1]);
		seaweeds.setEnabled(true);

		barangay = seaweeds.getBarangay();
		barangay_data = barangay.split(",", 3);
		seaweeds.setBarangay_id(barangay_data[0]);
		seaweeds.setBarangay(barangay_data[1]);

		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(seaweeds.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		if (seaweeds.getId() != 0) {
			Optional<Seaweeds> weeds_image = seaweedsService.findById(seaweeds.getId());
			String image_server = weeds_image.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				newUniqueKeyforMap = seaweeds.getUniqueKey();
				seaweeds.setImage("/images/" + image_server);
				seaweedsService.saveSeaweeds(ResourceData.getSeaweeds(seaweeds, user, "", UPDATE));
			}else {
				newUniqueKeyforMap = seaweeds.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", ""); 
				seaweeds.setImage("/images/" + imageName);
				Base64Converter.convertToImage(seaweeds.getImage_src(), seaweeds.getImage());
				seaweedsService.saveSeaweeds(ResourceData.getSeaweeds(seaweeds, user, "", UPDATE));
			}
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", ""); 
			seaweeds.setImage("/images/" + imageName);
			Base64Converter.convertToImage(seaweeds.getImage_src(), seaweeds.getImage());
			seaweedsService.saveSeaweeds(ResourceData.getSeaweeds(seaweeds, user, newUniqueKey, SAVE));
			save = true;

		}

	}
	@RequestMapping(value = "/getLastPageSeaweeds", method = RequestMethod.GET)
	public @ResponseBody String getLastPageSeaweeds(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = seaweedsService.SeaweedsCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}



}
