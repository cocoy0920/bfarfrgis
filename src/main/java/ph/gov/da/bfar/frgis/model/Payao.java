package ph.gov.da.bfar.frgis.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@Entity
@Table(name="payao")
public class Payao implements Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String user;
	
	private String page;
	
	@Column(name = "unique_key")
	private String unique_key;
	
	private String region;
	
	private String region_id;
	
	private String province;
	
	private String province_id;
	
	private String municipality;
	
	private String municipality_id; 
	
	private String barangay;
	
	private String barangay_id;
		
	private String code;
	
	private String association;
	
	private String beneficiary;
	
	private int payao;
	
	@NotEmpty(message = "Date as Of may not be empty")
	@Column(name = "date_as_of")
	private String dateAsOf;
	
	@NotEmpty(message = "Data Source may not be empty")
	@Column(name = "source_of_data")
	private String sourceOfData;
	
	
	private String remarks;
	
	private String encode_by;
	
	private String edited_by;

	private String date_encoded;

	private String date_edited;
	
	private String last_used;
	
	private String lat;

	private String lon;
	
	private String image;
	
	private String image_src;
	
	private boolean enabled;
	private String reason;	
}
