package ph.gov.da.bfar.frgis.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@ToString
@Getter
@Setter
@Entity
@Table(name="ipcs")
public class IcePlantColdStorage implements Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String user;
	
	private String page;
	
	@Column(name = "unique_key")
	private String uniqueKey;
	
	@NotEmpty(message = "Date as Of may not be empty")
	@Column(name = "date_as_of")
	private String dateAsOf;
	
	private String region;
	
	private String region_id;
	
	@NotEmpty(message = "Province may not be empty")
	private String province;
	
	private String province_id;
	
	@NotEmpty(message = "Municipality may not be empty")
	private String municipality;
	
	private String municipality_id; 
	
	@NotEmpty(message = "Barangay may not be empty")
	private String barangay;
	
	private String barangay_id;
	
	@NotEmpty(message = "Name Of Ice Plant may not be empty")
	@Column(name = "name_of_ice_plant")
	private String nameOfIcePlant;
	
	@NotEmpty(message = "Sanitary Permit may not be empty")
	@Column(name = "sanitary_permit")
	private String sanitaryPermit;
	
	private String operator;
	
	@NotEmpty(message = "Type Of Facility may not be empty")
	@Column(name = "type_of_facility")
	private String typeOfFacility;
	
	@NotEmpty(message = "Structure Type may not be empty")
	@Column(name = "structure_type")
	private String structureType;
	
	private String capacity;
	
	@NotEmpty(message = "With Valid License To Operate may not be empty")
	@Column(name = "with_valid_license_to_operate")
	private String withValidLicenseToOperate;
	
	@NotEmpty(message = "With Valid Sanitary Permit may not be empty")
	@Column(name = "with_valid_sanitary_permit")
	private String withValidSanitaryPermit;
	
	private String withValidSanitaryPermitYes;
	
	@Column(name = "with_valid_sanitary_permit_others")
	private String withValidSanitaryPermitOthers;
	
	@Column(name = "business_permits_and_certificate_obtained")
	private String businessPermitsAndCertificateObtained;
	
	@Column(name = "type_of_icemaker")
	private String typeOfIceMaker;
	
	@Column(name = "food_gradule")
	private String foodGradule;
	
	@Column(name = "source_of_equipment")
	private String sourceOfEquipment;
	
	@NotEmpty(message = "Area may not be empty")
	private String  area;
	
	private String code;
	
	@NotEmpty(message = "Data Source may not be empty")
	@Column(name = "source_of_data")
	private String dataSource;
	
	@NotEmpty(message = "Remarks may not be empty")
	private String remarks;
	
	@Column(name = "encode_by")
	private String encodedBy;
	
	@Column(name = "edited_by")
	private String editedBy;

	@Column(name = "date_encoded")
	private String dateEncoded;
	
	@Column(name = "date_edited")
	private String dateEdited;
	
	private String last_used;
	
	@NotEmpty(message = "Latitude may not be empty")
	private String lat;
	
	@NotEmpty(message = "Longitude may not be empty")
	private String lon;
	
	private String image;
	
	private String image_src;
	private boolean enabled;
	private String reason;
}
