package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.PayaoPage;
import ph.gov.da.bfar.frgis.model.Payao;
import ph.gov.da.bfar.frgis.repository.PayaoRepo;



@Service("PayaoService")
@Transactional
public class PayaoServiceImpl implements PayaoService{

	@Autowired
	private PayaoRepo dao;

	@Override
	public Optional<Payao> findById(int id) {

		return dao.findById(id);
	}

	@Override
	public List<Payao> ListByUsername(String username) {

		return dao.getListPayaoByUsername(username);
	}

	@Override
	public List<Payao> ListByRegion(String region_id) {

		return dao.getListPayaoByRegion(region_id);
	}

	@Override
	public Payao findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<Payao> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public void savePayao(Payao Payao) {
		dao.save(Payao);
		
	}

	@Override
	public void updateByEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updatePayaoByReason(int id, String reason) {
		dao.updatePayaoByReason(id, reason);
		
	}

	@Override
	public List<Payao> findAllPayao() {

		return dao.findAll();
	}

	@Override
	public List<Payao> getAllPayao(int index, int pagesize, String user) {
		
		return dao.findAll();
	}

	@Override
	public int PayaoCount(String username) {
		
		return dao.PayaoCount(username);
	}
	@Override
	public int PayaoSumByRegion(String region_id) {

	return dao.PayaoSumByRegion(region_id);
}
	@Override
	public int PayaoSumAll() {
	
		return dao.PayaoSumAll();
	}
	@Override
	public Page<PayaoPage> findByUsername(String user, Pageable pageable) {
		
		return dao.findByUsername(user, pageable);
	}

	@Override
	public Page<PayaoPage> findByRegion(String region, Pageable pageable) {
		
		return dao.findByRegion(region, pageable);
	}

	@Override
	public Page<PayaoPage> findAllPayao(Pageable pageable) {
		
		return dao.findAllPayao(pageable);
	}

	@Override
	public Long countAllPayao() {
		
		return dao.countAllPayao();
	}

	@Override
	public Long countPayaoPerRegion(String region_id) {
		
		return dao.countPayaoPerRegion(region_id);
	}

	@Override
	public Long countPayaoProvince(String province_id) {

		return dao.countPayaoPerProvince(province_id);
	}

	@Override
	public Long countPayaoPerMunicipality(String municipality_id) {
		return dao.countPayaoPerMunicipality(municipality_id);
	}


	
	

	
}
