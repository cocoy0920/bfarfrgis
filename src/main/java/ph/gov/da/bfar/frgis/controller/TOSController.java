package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.TOSPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.TOS;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.TOSService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class TOSController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	TOSService tosService;
	
	 @Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;
	
	@RequestMapping(value = "/tos", method = RequestMethod.GET)
	public ModelAndView TOS(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();
		
		user = authenticationFacade.getUserInfo();

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "tos");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("users/form");
		return result;
	}
	@RequestMapping(value = "/to", method = RequestMethod.GET)
	public ModelAndView TO(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "tos");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("users/form");
		return result;
	}

	@RequestMapping(value = "/TOSMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView TOSMap(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
		 TOS data = tosService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	Features features = new Features();
		 	features  = MapBuilder.getTOSMapData(data);
		 	
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUnique_key());
		 	mapsData.setIcon("/static/images/pin2022/TechnologyOutreachStation.png");
		 	
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			
			map_data = jsonString;
		result = new ModelAndView("redirect:../to");
		
	
		return result;
	}
	@RequestMapping(value = "/TOSDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView TOSMap(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		// tosService.deleteTOSById(id);
		 tosService.updateByEnabled(id);
		result = new ModelAndView("redirect:../to");
		
	
		return result;
	}
	@RequestMapping(value = "/TOSDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView TOSMap(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		// tosService.deleteTOSById(id);
		 tosService.updateByEnabled(id);
		 tosService.updateTOSByReason(id, reason);
		result = new ModelAndView("redirect:../../to");
		
	
		return result;
	}
	
	@RequestMapping(value = "/TOSMap", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "seagrass");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("users/map/seagrassmap");
		
	
		return result;
	}

	@RequestMapping(value = "/tos/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getTOS(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<TOSPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 5, Sort.by("id"));
	      
	          page = tosService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<TOSPage> RegionalList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setTOS(RegionalList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/TOSList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getTOSList() throws JsonGenerationException, JsonMappingException, IOException {

		List<TOS> list = null;

		list = tosService.ListByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	
	@RequestMapping(value = "/getTOS/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getTOSById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<TOS> mapsDatas = tosService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}


	@RequestMapping(value = "/saveTOS", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postTOS(@RequestBody TOS TOS) throws JsonGenerationException, JsonMappingException, IOException {

		
		savingResources(TOS);
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
	

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);

	}

	private void savingResources(TOS TOS) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		TOS.setRegion_id(region_data[0]);
		TOS.setRegion(region_data[1]);

		province = TOS.getProvince();
		data = province.split(",", 2);
		TOS.setProvince_id(data[0]);
		TOS.setProvince(data[1]);

		municipal = TOS.getMunicipality();
		municipal_data = municipal.split(",", 2);
		TOS.setMunicipality_id(municipal_data[0]);
		TOS.setMunicipality(municipal_data[1]);

		barangay = TOS.getBarangay();
		barangay_data = barangay.split(",", 3);
		TOS.setBarangay_id(barangay_data[0]);
		TOS.setBarangay(barangay_data[1]);
		TOS.setEnabled(true);

		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(TOS.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		if (TOS.getId() != 0) {
			Optional<TOS> tos_image = tosService.findById(TOS.getId());
			String image_server = tos_image.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				newUniqueKeyforMap = TOS.getUnique_key();
				TOS.setImage("/images/" + image_server);
				tosService.saveTOS(ResourceData.getTOS(TOS, user, newUniqueKey, UPDATE));
			}else {
				newUniqueKeyforMap = TOS.getUnique_key();
				imageName.replaceAll("[^a-zA-Z0-9]", "");  
				TOS.setImage("/images/" + imageName);
				Base64Converter.convertToImage(TOS.getImage_src(), TOS.getImage());
				tosService.saveTOS(ResourceData.getTOS(TOS, user, newUniqueKey, UPDATE));
			}
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", "");  
			TOS.setImage("/images/" + imageName);
			Base64Converter.convertToImage(TOS.getImage_src(), TOS.getImage());
			tosService.saveTOS(ResourceData.getTOS(TOS, user, newUniqueKey, SAVE));
			save = true;

		}

	}


	
}
