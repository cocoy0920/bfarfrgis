package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import ph.gov.da.bfar.frgis.model.Municipalities;
import ph.gov.da.bfar.frgis.repository.MunicipalitiesRepo;




@Service("MunicipalityService")
@Transactional
public class MunicipalityServiceImpl implements MunicipalityService{

	@Autowired
	MunicipalitiesRepo municipalitiesRepo;

	@Override
	public Optional<Municipalities> findById(int id) {
		
		return municipalitiesRepo.findById(id);
	}

	@Override
	public void save(Municipalities municipality) {
		municipalitiesRepo.save(municipality);
		
	}

	@Override
	public void update(Municipalities municipality) {
		municipalitiesRepo.save(municipality);
		
	}

	@Override
	public List<Municipalities> findAllMunicipalities() {
		
		return municipalitiesRepo.findAll();
	}

	@Override
	public List<Municipalities> findByProvince(String provinceId) {
		
		return municipalitiesRepo.findByProvince(provinceId);
	}

	
	
}
