package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpRequest;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.Regions;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserBean;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.model.UserProfileType;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.RegionsService;
import ph.gov.da.bfar.frgis.service.RoleService;
import ph.gov.da.bfar.frgis.service.UserProfileService;
import ph.gov.da.bfar.frgis.service.UserService;


@Controller
public class AuthenticationController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	RegionsService regionsService;
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	MapsService mapsService;
	
	@Autowired
	UserProfileService userProfileService;
	
	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	

	
	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String login() {

		String url="login";
//		 Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//	        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
//	        	System.out.println("login" + authentication.getAuthorities());
//	        	url = "login";
//	        
//		        } else {
//		        url = 	determineTargetUrl(authentication);
//	       		 
//	      	}
		return url;
	}
	
	@RequestMapping(value = { "/" ,""}, method = RequestMethod.GET)
	public ModelAndView index(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {
		
		
		ModelAndView modelAndView = new ModelAndView();
//		
//		 ObjectMapper mapper = new ObjectMapper();
//	
//			String jsonString = mapper.writeValueAsString("");
//		model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
//		//model.addAttribute("page", "home_map");
//		model.addAttribute("mapsData", jsonString);

		
		modelAndView.setViewName("home"); // resources/template/login.html
 
		return modelAndView;
	}
	
	@RequestMapping(value = { "expired"}, method = RequestMethod.GET)
	public ModelAndView expired(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {
		
		
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.setViewName("expired"); // resources/template/login.html
 
		return modelAndView;
	}
	
	
//	public String toJson(ArrayList<String> array){
//		
//
//		  JSONObject featureCollection = new JSONObject();
//		  featureCollection.put("type", "FeatureCollection");
//		  JSONObject properties = new JSONObject();
//		  properties.put("name", "ESPG:4326");
//		  JSONObject crs = new JSONObject();
//		  crs.put("type", "name");
//		  crs.put("properties", properties);
//		  featureCollection.put("crs", crs);
//
//		  JSONArray features = new JSONArray();
//		  JSONObject feature = new JSONObject();
//		  feature.put("type", "Feature");
//		  JSONObject geometry = new JSONObject();
//
//		  JSONAray JSONArrayCoord = new JSONArray();
//		  for(int i=0; i<array.length; i++){
//		    eachElement = array[i];
//		    if(eachElement.getLongtitude()!=null){
//		      JSONArrayCoord.add(0, eachElement.getLongtitude());
//		      JSONArrayCoord.add(1, eachElement.getLatitude());
//		      geometry.put("type", "Point");
//		      geometry.put("coordinates", JSONArrayCoord);
//		      feature.put("geometry", geometry);
//
//		      features.add(feature);
//		      featureCollection.put("features", features);
//		    }
//		  }
//		  return featureCollection.toString();
//		}
//	
	
	
	
	
	
	
	@RequestMapping(value = { "/logout" }, method = RequestMethod.GET)
	public ModelAndView logout() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("redirect:/login");
		return modelAndView;
//		ModelAndView modelAndView = new ModelAndView();
//		modelAndView.setViewName("login"); // resources/template/login.html
//		return modelAndView;
	}
	

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView register(ModelMap model) {
		ModelAndView modelAndView = new ModelAndView();
		
		
//		 Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//	        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
//	        	System.out.println("login" + authentication.getAuthorities());
//	        	modelAndView.setViewName("login");
//	        }else {
//	        	Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
//	        	 
//	            List<String> roles = new ArrayList<String>();
//	     
//	            for (GrantedAuthority a : authorities) {
//	                roles.add(a.getAuthority());
//	            }
//	            
//	            if (roles.contains("ROLE_USER")) {
//	            	
//	            }
//	            else if (roles.contains("ROLE_ENCODER")) {
//	            	
//	            }
//	            else if (roles.contains("ROLE_ADMIN")) {
//	            	
//	            }
//	            else if (roles.contains("ROLE_SUPERADMIN")) {
//	            	
//	            }else if (roles.contains("ROLE_REGION")) {
//	            
//	            }else {
//	            	
//	            	
//	            }
//	        }
//		
//		
//
		 List<Regions> regions = null;  
		 List<UserProfile> role = null;
	 regions = regionsService.findAllRegions();
		 role = userProfileService.findAll();
//
//		 modelAndView.addObject("userForm", new UserBean());
		 model.addAttribute("regions", regions);
		 model.addAttribute("roles", role);
		modelAndView.setViewName("register"); 
//	
		return modelAndView;
	}
//	



	
//	@RequestMapping(value = "/register", method = RequestMethod.POST)
//	public ModelAndView registerUser( @ModelAttribute("userForm") @Validated  UserBean userbean, BindingResult bindingResult, ModelMap model) {
//		
//		ModelAndView modelAndView = new ModelAndView();
//		
//		if(bindingResult.hasErrors()) {
//			modelAndView.addObject("successMessage", "Please correct the errors in form!");
//			
//			model.addAttribute("bindingResult", bindingResult);
//		
//		}
//		else {
//			
//			String getUsername = userService.isUserAlreadyPresent(userbean.getUsername());
//			System.out.println("getUsername == userbean.getName(): " + getUsername + "==" + userbean.getUsername());
//			if(getUsername == userbean.getUsername()) {
//								
//				userbean.setUsername("");
//				 modelAndView.addObject("userForm", userbean);
//				modelAndView.addObject("successMessage", "Username Already taken!");
//				
//			}else {
//				
//
//				User user = new User();
//				
//				String region = userbean.getRegion();
//				String [] region_data = region.split(",", 2);
//				user.setRegion_id(region_data[0]);
//				user.setRegion(region_data[1]);
//				
//				
//				user.setName(userbean.getFirstName());
//				user.setLastName(userbean.getLastName());
//				user.setUsername(userbean.getUsername());
//				user.setState("Active");
//				user.setEmail(userbean.getEmail());
//				user.setEnabled(true);
//				user.setPass(userbean.getPassword());
//				Set<UserProfile> userProfiles = new HashSet<UserProfile>();
//				UserProfile profile = new UserProfile();
//				
//
//				String role = userbean.getUserProfiles();
//				String[] role_data = role.split(",", 2);
//				
//				profile.setId(Integer.parseInt(role_data[0]));	
//				profile.setType(role_data[1]);
//				userProfiles.add(profile);
//				user.setUserProfiles(userProfiles);
//				user.setPassword(userbean.getPassword());
//				
//				
//				
//							
//				userService.saveUser(user);
//
//				
//				modelAndView.addObject("successMessage", "User is registered successfully!");
//				
//			}
//			
//			
//			
//			
//		}
//		 List<Regions> regions = null;  
//		 List<UserProfile> role = null;
//		 regions = regionsService.findAllRegions();
//		 role = userProfileService.findAll();
//
//		
//		 model.addAttribute("regions", regions);
//		 model.addAttribute("roles", role);
//		modelAndView.setViewName("register");
//		return modelAndView;
//	}
	public Map<String,String> roles(){
		
		Map<String, String> roleMap = new HashMap<String, String>();
		
		List<UserProfile> user_role = userProfileService.findAll();
		
		for(UserProfile profile : user_role) {
			
			roleMap.put(profile.getId() + "," + profile.getType(), profile.getType());
		}
		return roleMap;
	}	
	

//	@RequestMapping("/error")
//	public String handleError(HttpServletRequest request) {
//	    Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
//	    
//	    if (status != null) {
//	        Integer statusCode = Integer.valueOf(status.toString());
//	    
//	        if(statusCode == HttpStatus.NOT_FOUND.value()) {
//	            return "error-404";
//	        }
//	        else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
//	            return "error-500";
//	        }
//	    }
//	    return "error";
//	}

	

    protected String determineTargetUrl(Authentication authentication) {
        String url = "";
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
 
        List<String> roles = new ArrayList<String>();
 
        for (GrantedAuthority a : authorities) {
            roles.add(a.getAuthority());
        }
         if (isRegionalUser(roles)) {
        	 url = "/create/user";
        } else if (isPFOUser(roles)) {
            url = "/create/user";
       }  else if(isRegionalAdmin(roles)){
        	url = "/region/home";
        	
        }  else if(isPFOAdmin(roles)){
        	url = "/province/home";
        	
        } else if(isCO(roles)){
        	url = "/province/home";
        	
        } else if(isSuperAdmin(roles)){
        	url = "/admin/home";
        	
        } else if(isDBA(roles)){
        	url = "/dba/home";
        	
        } else if(isPlanning(roles)){
        	url = "/report/home";
        	
        }// else {
//            url = "/accessDenied";
//        }
 
        return url;
    }
    
    private boolean isRegionalUser(List<String> roles) {
        if (roles.contains("ROLE_REGIONALENCODER")) {
            return true;
        }
        return false;
    }
    
    private boolean isPFOUser(List<String> roles) {
        if (roles.contains("ROLE_PFOENCODER")) {
            return true;
        }
        return false;
    }
 
    private boolean isRegionalAdmin(List<String> roles) {
        if (roles.contains("ROLE_REGIONALADMIN")) {
            return true;
        }
        return false;
    }
    private boolean isPFOAdmin(List<String> roles) {
        if (roles.contains("ROLE_PFOADMIN")) {
            return true;
        }
        return false;
    }
 
    private boolean isSuperAdmin(List<String> roles) {
        if (roles.contains("ROLE_SUPERADMIN")) {
            return true;
        }
        return false;
    }
    
    private boolean isCO(List<String> roles) {
        if (roles.contains("ROLE_CO")) {
            return true;
        }
        return false;
    }
    private boolean isDBA(List<String> roles) {
        if (roles.contains("ROLE_DBA")) {
            return true;
        }
        return false;
    }
    private boolean isPlanning(List<String> roles) {
        if (roles.contains("ROLE_PLANNING")) {
            return true;
       }
        return false;
    }
 
}
