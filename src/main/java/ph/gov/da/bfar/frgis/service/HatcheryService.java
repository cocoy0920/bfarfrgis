package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.HatcheryPage;
import ph.gov.da.bfar.frgis.model.Hatchery;


public interface HatcheryService {
	
	Optional<Hatchery> findById(int id);
	
	List<Hatchery> ListByUsername(String username);
	Hatchery findByUniqueKey(String uniqueKey);
	List<Hatchery> findByUniqueKeyForUpdate(String uniqueKey);
	List<Hatchery> findAllByType(String type);
	List<Hatchery> findAllByLegislated(String legislated);
	void saveHatchery(Hatchery Hatchery);
	void deleteHatcheryById(int id);
	void updateByEnabled(int id);
	void updateHatcheryByReason(int  id,String  reason);
	List<Hatchery> findAllHatcherys(); 
	List<Hatchery> findByRegion(String region_id); 
	List<Hatchery> findByRegionAndType(String region_id,String type); 
	List<Hatchery> findByRegionAndLegislated(String region_id,String legislated); 
    public List<Hatchery> getAllHatcherys(int index,int pagesize,String user);
    public int HatcheryCount(String username);
    public Page<HatcheryPage> findByUsername(@Param("user") String user, Pageable pageable);
    public Page<HatcheryPage> findByRegion(@Param("region") String region, Pageable pageable);
    public Page<HatcheryPage> findAllHatchery(Pageable pageable);
    public Long countAllHatchery();
    public Long countHatcheryPerRegion(String region_id);
    public Long countHatcheryPerRegion(String region_id,String operator);
    public Long countHatcheryLegislatedPerRegion(String region_id,String operator, String legislated);
    public Long countHatcheryPerProvince(String province_id);
    public Long countHatcheryPerProvince(String province_id,String operator);
    public Long countHatcheryLegislatedPerProvince(String province_id,String operator,String legislated);
    public Long countHatcheryPerMunicipality(String municipality_id);
    public Long countHatcheryPerMunicipality(String municipality_id,String operator);
    public Long countHatcheryLegislatedPerMunicipality(String municipality_id,String operator,String legislated);
//    public int HatcheryCountByMuunicipality(String municipalityID);
//    public int HatcheryCountByProvince(String provinceID);
//    public int HatcheryCountByRegion(String regionID);
	
	//boolean isHatcherySSOUnique(Integer id, String sso);

}