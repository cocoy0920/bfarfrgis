package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.FishCoral;
import ph.gov.da.bfar.frgis.model.FishLanding;
import ph.gov.da.bfar.frgis.model.FishPen;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.Fishport;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;
import ph.gov.da.bfar.frgis.model.LGU;
import ph.gov.da.bfar.frgis.model.Mangrove;
import ph.gov.da.bfar.frgis.model.MaricultureZone;
import ph.gov.da.bfar.frgis.model.Market;
import ph.gov.da.bfar.frgis.model.NationalCenter;
import ph.gov.da.bfar.frgis.model.Regions;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;
import ph.gov.da.bfar.frgis.model.SeaGrass;
import ph.gov.da.bfar.frgis.model.Seaweeds;
import ph.gov.da.bfar.frgis.model.TrainingCenter;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserBean;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.service.FishCageService;
import ph.gov.da.bfar.frgis.service.FishCorralService;
import ph.gov.da.bfar.frgis.service.FishLandingService;
import ph.gov.da.bfar.frgis.service.FishPenService;
import ph.gov.da.bfar.frgis.service.FishPondService;
import ph.gov.da.bfar.frgis.service.FishPortService;
import ph.gov.da.bfar.frgis.service.FishProcessingPlantService;
import ph.gov.da.bfar.frgis.service.FishSanctuariesService;
import ph.gov.da.bfar.frgis.service.HatcheryService;
import ph.gov.da.bfar.frgis.service.IcePlantColdStorageService;
import ph.gov.da.bfar.frgis.service.LGUService;
import ph.gov.da.bfar.frgis.service.MangroveService;
import ph.gov.da.bfar.frgis.service.MaricultureZoneService;
import ph.gov.da.bfar.frgis.service.MarketService;
import ph.gov.da.bfar.frgis.service.NationalCenterService;
import ph.gov.da.bfar.frgis.service.PayaoService;
import ph.gov.da.bfar.frgis.service.RegionalOfficeService;
import ph.gov.da.bfar.frgis.service.RegionsService;
import ph.gov.da.bfar.frgis.service.SchoolOfFisheriesService;
import ph.gov.da.bfar.frgis.service.SeaGrassService;
import ph.gov.da.bfar.frgis.service.SeaweedsService;
import ph.gov.da.bfar.frgis.service.TOSService;
import ph.gov.da.bfar.frgis.service.TrainingCenterService;
import ph.gov.da.bfar.frgis.service.UserProfileService;
import ph.gov.da.bfar.frgis.service.UserService;

@Controller
public class SuperAdminController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserProfileService userProfileService;
	
	@Autowired
	RegionsService regionsService;
	
	@Autowired
	FishSanctuariesService fishSanctuariesService;
	
	@Autowired
	FishProcessingPlantService fishProcessingPlantService;
	
	@Autowired
	FishLandingService fishLandingService;
	
	@Autowired
	FishPortService fishPortService;
	
	@Autowired
	FishPenService fishPenService;
	
	@Autowired
	FishPondService fishPondService;
	
	@Autowired
	FishCageService fishCageService;

	@Autowired
	IcePlantColdStorageService icePlantColdStorageService;
	
	@Autowired
	MarketService marketService;

	@Autowired
	SchoolOfFisheriesService schoolOfFisheriesService;
	
	@Autowired
	FishCorralService fishCorralService;
	
	@Autowired
	HatcheryService hatcheryService;
	
	@Autowired
	SeaGrassService seaGrassService;
	
	@Autowired
	SeaweedsService seaweedsService;
	
	@Autowired
	MangroveService mangroveService;
	
	@Autowired
	LGUService lGUService;
	
	@Autowired
	MaricultureZoneService zoneService;
	
	@Autowired
	TrainingCenterService trainingCenterService;
	
	@Autowired
	NationalCenterService nationalCenterService;
	
	@Autowired
	PayaoService payaoService;
	
	@Autowired
	RegionalOfficeService regionalOfficeService;
	
	@Autowired
	TOSService tosService;
	
	
	@Autowired
	 private IAuthenticationFacade authenticationFacade;




	
	@RequestMapping(value = "/addAcct", method = RequestMethod.GET)
	public ModelAndView addAcct(ModelMap model) {
		ModelAndView modelAndView = new ModelAndView();
		
		 List<Regions> regions = null;  
		 List<UserProfile> roles = null;
		 regions = regionsService.findAllRegions();
		 
		 User user = new User();		 
			user = authenticationFacade.getUserInfo();
			Set<UserProfile> setOfRole = new HashSet<>();
			setOfRole = user.getUserProfiles();
			List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
			UserProfile role = list.get(0);
			
			model.addAttribute("user", user);
			model.addAttribute("role", role);
			roles = userProfileService.findAll();

		
		 model.addAttribute("regions", regions);
		 model.addAttribute("roles", role);

		
		
		modelAndView.setViewName("superadmin/register"); // resources/template/register.html
		return modelAndView;
		
	}
	
	@RequestMapping(value = "/getAllRoles", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllRoles()throws JsonGenerationException, JsonMappingException, IOException {


		List<UserProfile> roles = userProfileService.findAll();
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonArray = gson.toJson(roles);
		
		return jsonArray;

	}

	@RequestMapping(value = "/getAllRegions", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllRegions()throws JsonGenerationException, JsonMappingException, IOException {


		List<Regions> regions = regionsService.findAllRegions();
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonArray = gson.toJson(regions);
		
		return jsonArray;

	}

	
	@RequestMapping(value = "/saveRegistration", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String saveRegistration(@RequestBody UserBean userbean) throws JsonGenerationException, JsonMappingException, IOException {

		
		System.out.println(userbean.toString());
		ObjectMapper objectMapper = new ObjectMapper();
		String validation;
		
		if(userbean.getId() != 0) {
			


			User user = new User();
			
			user.setId(userbean.getId());
			user.setName(userbean.getFirstName());
			user.setLastName(userbean.getLastName());
			user.setUsername(userbean.getUsername());
			user.setState(userbean.getState());
			user.setEmail(userbean.getEmail());
			System.out.println("userbean.isEnabled(): " + userbean.isEnabled());
			//user.setEnabled(userbean.isEnabled());
			user.setPass(userbean.getPassword());
			Set<UserProfile> userProfiles = new HashSet<UserProfile>();
			UserProfile profile = new UserProfile();
			

			String role = userbean.getUserProfiles();
			String[] role_data = role.split(",", 2);
			
			profile.setId(Integer.parseInt(role_data[0]));	
			profile.setType(role_data[1]);
			
			
			String region = userbean.getRegion();
			String [] region_data = region.split(",", 2);
			user.setRegion_id(region_data[0]);
			user.setRegion(region_data[1]);
		
			
				
			userProfiles.add(profile);
			user.setUserProfiles(userProfiles);
			//user.setPassword(userbean.getPassword());
			
			
			
						
			userService.saveUser(user);
			validation = objectMapper.writeValueAsString("User is updated successfully!");
			
			
			
			
			/////end//////
			
		}else {
			
			boolean getUsername = userService.isUserAlreadyPresent(userbean.getUsername());
			System.out.println("getUsername == userbean.getName(): " + getUsername + "==" + userbean.getUsername());
			if(getUsername) {
				validation = objectMapper.writeValueAsString("Username Already taken!");
				return validation;

				
			}
			
			User user = new User();
			

			Set<UserProfile> userProfiles = new HashSet<UserProfile>();
			UserProfile profile = new UserProfile();
			String role = userbean.getUserProfiles();
			String[] role_data = role.split(",", 2);
			
			profile.setId(Integer.parseInt(role_data[0]));	
			profile.setType(role_data[1]);
			
				String region = userbean.getRegion();
				String [] region_data = region.split(",", 2);
				user.setRegion_id(region_data[0]);
				user.setRegion(region_data[1]);

			user.setName(userbean.getFirstName());
			user.setLastName(userbean.getLastName());
			user.setUsername(userbean.getUsername());
			user.setState("Active");
			user.setEmail(userbean.getEmail());
			user.setEnabled(true);
			user.setPass(userbean.getPassword());
			
			
			
			userProfiles.add(profile);
			user.setUserProfiles(userProfiles);
			//user.setPassword(userbean.getPassword());
			
			
			
						
			userService.saveUser(user);

			validation = objectMapper.writeValueAsString("User is registered successfully!");
			
		}
			
		
		
		

		return validation;

	}
	
	@RequestMapping(value = "/getAllGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return alltoJson(allResourcesFeaturesList());

	}

	
	private ArrayList<Features> allResourcesFeaturesList(){



		ArrayList<Features> list2 = new ArrayList<>();
		
			ArrayList<FishSanctuaries> fishsanctuariesList = null;


			fishsanctuariesList = (ArrayList<FishSanctuaries>) fishSanctuariesService.findAllFishSanctuaries();
	 			
	 			for(FishSanctuaries data : fishsanctuariesList) {
				
	 				Features features = new Features();
	 				features.setArea(data.getArea());
	 				features.setResources("fishsanctuaries");
	 				features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
	 				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	 				features.setLatitude(data.getLat());
	 				features.setLongtitude(data.getLon());
	 				features.setName(data.getNameOfFishSanctuary());
	 				features.setImage(data.getImage());
	 				list2.add(features);
	 			}

			ArrayList<FishProcessingPlant> fishprocessingplantsList = null;
	
			fishprocessingplantsList = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.findAllFishProcessingPlants();
			
					for(FishProcessingPlant data : fishprocessingplantsList) {
						Features features = new Features();
						features.setArea(data.getArea());
						features.setResources("fishprocessingplants");
						features.setRegion(data.getRegion());
						features.setRegion_id(data.getRegion_id());
						features.setProvince(data.getProvince());
						features.setProvince_id(data.getProvince_id());
						features.setMunicipality(data.getMunicipality());
						features.setMunicipality_id(data.getMunicipality_id());
						features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
						features.setLatitude(data.getLat());
						features.setLongtitude(data.getLon());
						features.setName(data.getNameOfProcessingPlants());
						features.setImage(data.getImage());
						list2.add(features);
					}

			ArrayList<FishLanding> fishlandingList = null;

			fishlandingList = (ArrayList<FishLanding>) fishLandingService.findAllFishLandings();
			
			for(FishLanding data : fishlandingList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("fishlanding");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfLanding());
				features.setImage(data.getImage());
				list2.add(features);
			}

			ArrayList<Fishport> FishPortList = null;

			FishPortList = (ArrayList<Fishport>) fishPortService.findAllFishports();
			
			for(Fishport data : FishPortList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("FISHPORT");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfFishport());
				features.setImage(data.getImage());
				list2.add(features);
				}

			ArrayList<FishPen> fishpenList = null;

			fishpenList = (ArrayList<FishPen>) fishPenService.findAllFishPens();
			
			for(FishPen data : fishpenList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("fishpen");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				//features.setName(data.get);
				features.setImage(data.getImage());
				list2.add(features);
			}

			ArrayList<FishCage> fishcageList = null;

			
			fishcageList = (ArrayList<FishCage>) fishCageService.findAllFishCages();
			
			for(FishCage data : fishcageList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("fishcage");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				list2.add(features);
			}

			
			ArrayList<IcePlantColdStorage> coldStorageList = null;

		
			coldStorageList = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.findAllIcePlantColdStorages();
			
			for(IcePlantColdStorage data : coldStorageList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("coldstorage");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfIcePlant());
				features.setImage(data.getImage());
				list2.add(features);
			}

			ArrayList<Market> marketList = null;

			marketList = (ArrayList<Market>) marketService.findAllMarkets();
			
			for(Market data : marketList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("market");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfMarket());
				features.setImage(data.getImage());
				list2.add(features);
			}

			ArrayList<SchoolOfFisheries> schoolList = null;
			
			schoolList = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.findAllSchoolOfFisheriess();
			
			for(SchoolOfFisheries data : schoolList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("schoolOfFisheries");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getName());
				features.setImage(data.getImage());
				list2.add(features);
			}
		
			ArrayList<FishCoral> fishCoralList = null;

			fishCoralList = (ArrayList<FishCoral>) fishCorralService.findAllFishCorals();
			
			for(FishCoral data : fishCoralList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("fishcoral");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				list2.add(features);
			}

			
			ArrayList<SeaGrass> seagrassList = null;

			seagrassList = (ArrayList<SeaGrass>) seaGrassService.findAllSeaGrasss();
			
			for(SeaGrass data : seagrassList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("seagrass");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				list2.add(features);
			}
			
			ArrayList<Seaweeds> seaweedsList = null;

			seaweedsList = (ArrayList<Seaweeds>) seaweedsService.findAllSeaweedss();
			
			for(Seaweeds data : seaweedsList) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("seaweeds");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				
				list2.add(features);
			}
	
			ArrayList<Mangrove> mangroveList = null;

			
			mangroveList = (ArrayList<Mangrove>) mangroveService.findAllMangroves();
			
			for(Mangrove data : mangroveList) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("mangrove");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getType());
				features.setImage(data.getImage());
				list2.add(features);
			}
	
			ArrayList<LGU> lguList = null;

			lguList = (ArrayList<LGU>) lGUService.findAllLGUs();
			
			for(LGU data : lguList) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("lgu");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getLguName());
				features.setImage(data.getImage());
				list2.add(features);
			}
			
		
			
			ArrayList<MaricultureZone> maricultureList = null;
			
			maricultureList = (ArrayList<MaricultureZone>) zoneService.findAllMaricultureZones();
			
			for(MaricultureZone data : maricultureList) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("mariculturezone");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfMariculture());
				features.setImage(data.getImage());
				list2.add(features);
			}
			
		
			ArrayList<TrainingCenter> traininglist = null;

			
			traininglist = (ArrayList<TrainingCenter>) trainingCenterService.findAllTrainingCenters();
			
			for(TrainingCenter data : traininglist) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("trainingcenter");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getName());
				features.setImage(data.getImage());
				list2.add(features);
			}
			
		
		
		
		
		return list2;
		
		
	
	
	}
	private String alltoJson(ArrayList<Features> list){



		  JSONObject featureCollection = new JSONObject();
		  JSONArray features = new JSONArray();
		  for(Features eachElement : list){
			  
		    	JSONObject geometry = new JSONObject();
		    	JSONObject featuresProperties = new JSONObject();
		        JSONArray JSONArrayCoord = new JSONArray();
		        JSONObject feature = new JSONObject();
				  feature.put("type", "Feature");

		        JSONArrayCoord.put(0, eachElement.getLongtitude());
		        JSONArrayCoord.put(1, eachElement.getLatitude());

		        geometry.put("type", "Point");
		        geometry.put("coordinates", JSONArrayCoord);
		        
		        featuresProperties.put("Latitude", eachElement.getLongtitude());
		        featuresProperties.put("Longitude", eachElement.getLatitude());
		        featuresProperties.put("Name", eachElement.getName());
		        featuresProperties.put("Image", eachElement.getImage());
		        featuresProperties.put("Location", eachElement.getLocation());	 
		        featuresProperties.put("resources", eachElement.getResources());
		        featuresProperties.put("Region", eachElement.getRegion_id());
		        featuresProperties.put("Province", eachElement.getProvince_id());
		        featuresProperties.put("Municipality", eachElement.getMunicipality_id());
		        feature.put("properties", featuresProperties);
		        feature.put("geometry", geometry);
		        features.put(feature);
		  }

		 
	        featureCollection.put("features", features);
		
			 featureCollection.put("type", "FeatureCollection");
			  JSONObject properties = new JSONObject();
			  properties.put("name", "urn:ogc:def:crs:OGC:1.3:CRS84");
			  JSONObject crs = new JSONObject();
			  crs.put("type", "name");
			  crs.put("properties", properties);
			  featureCollection.put("crs", crs);
			  
		  return featureCollection.toString();
		
	}

}
