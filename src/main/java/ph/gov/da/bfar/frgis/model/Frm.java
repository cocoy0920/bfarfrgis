package ph.gov.da.bfar.frgis.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class Frm {
		
	private FishSanctuaries sanctuaries;
	private FishProcessingPlant processingPlant;
	private FishLanding fishLanding;
	private FishPen fishPen;
	private FishCage fishCage;
	private Hatchery hatchery;
	private IcePlantColdStorage icePlantColdStorage;
	private Fishport fishport;
	private FishCoral fishCoral;
	private Market market;
	private SchoolOfFisheries schoolOfFisheries;
	private SeaGrass seaGrass;
	private Seaweeds seaweeds;
	private LGU lgu;
	private Mangrove mangrove;
	private TrainingCenter  trainingCenter;
	private MaricultureZone  maricultureZone;
	
	
	private String image_name;
	private String resource_uniqueKey;
	/*private String lat;
	private String lon;
	private String title;
	private String html;
	private final String zoom="8";*/
	private final String consignation_icon="/static/images/pin/20x34/consignation.png";
	private final String corrals_icon="/static/images/pin/20x34/corrals.png";
	private final String fishcorral_icon="/static/images/pin/20x34/fishcorral.png";
	private final String fisheriestraining_icon="/static/images/pin/20x34/fisheriestraining.png";
	private final String fishlanding_icon="/static/images/pin/20x34/fish-landing.png";
	private final String fishpen_icon="/static/images/pin/20x34/fishpen.png";
	private final String fishport_icon="/static/images/pin/20x34/fishport.png";
	private final String fishprocessing_icon="/static/images/pin/20x34/fishprocessing.png";
	private final String fishsanctuary_icon="/static/images/pin/20x34/fishsanctuary.png";
	private final String hatcheries_icon="/static/images/pin/20x34/hatcheries.png";
	private final String iceplant_icon="/static/images/pin/20x34/iceplant.png";
	private final String lgu_icon="/static/images/pin/20x34/lgu.png";
	private final String market_icon="/static/images/pin/20x34/market.png";
	private final String schooloffisheries_icon="/static/images/pin/20x34/schooloffisheries.png";
	private final String seaweeds_icon="/static/images/pin/20x34/seaweeds.png";
	private final String fishcage_icon="/static/images/pin/20x34/fishcage.png";
	private final String fishpond_icon="/static/images/pin/20x34/fishpond.png";
	private final String mangrove_icon="/static/images/pin/20x34/mangrove.png";
	private final String marine_icon="/static/images/pin/20x34/mpa.png";
	private final String seagrass_icon="/static/images/pin/20x34/seagrass.png";
	private final String mariculture_icon="/static/images/pin/20x34/mariculture.png";
	private final String others_icon="/static/images/pin/20x34/mpa.png";

	
}

