package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.SeaWeedsPage;
import ph.gov.da.bfar.frgis.model.Seaweeds;



public interface SeaweedsService {
	
	Optional<Seaweeds> findById(int id);
	
	List<Seaweeds> ListByUsername(String region);
	List<Seaweeds> ListByRegion(String region_id);
	List<Seaweeds> ListByRegion(String region_id,String type);
	Seaweeds findByUniqueKey(String uniqueKey);
	List<Seaweeds> findByUniqueKeyForUpdate(String uniqueKey);
	void saveSeaweeds(Seaweeds Seaweeds);
	void deleteSeaweedsById(int id);
	void updateByEnabled(int id);
	void updateSeaweedsByReason(int  id,String  reason);
	List<Seaweeds> findAllSeaweedss(); 
	List<Seaweeds> findAllSeaweedss(String type); 
    public List<Seaweeds> getAllSeaweedss(int index,int pagesize,String user);
    public int SeaweedsCount(String username);
    public Page<SeaWeedsPage> findByUsername(@Param("user") String user, Pageable pageable);
    public Page<SeaWeedsPage> findByRegion(@Param("region") String region, Pageable pageable);
    public Page<SeaWeedsPage> findAllSeaweeds(Pageable pageable);
    public Long countAllSeaweeds();
    public Long countSeaweedsPerRegion(String region_id);
    public Long countSeaweedsPerRegion(String region_id,String type);
    public Long countSeaweedsPerProvince(String province_id);
    public Long countSeaweedsPerProvince(String province_id,String type);
    public Long countSeaweedsPerMinucipality(String municipality_id);
    public Long countSeaweedsPerMinucipality(String municipality_id,String type);
}