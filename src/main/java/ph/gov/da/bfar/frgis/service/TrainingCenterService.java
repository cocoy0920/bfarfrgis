package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.TrainingPage;
import ph.gov.da.bfar.frgis.model.TrainingCenter;



public interface TrainingCenterService {
	
	Optional<TrainingCenter> findById(int id);
	
	List<TrainingCenter> ListByUsername(String username);
	List<TrainingCenter> ListByRegion(String region_id);
	TrainingCenter findByUniqueKey(String uniqueKey);
	List<TrainingCenter> findByUniqueKeyForUpdate(String uniqueKey);
	void saveTrainingCenter(TrainingCenter TrainingCenter);
	void deleteTrainingCenterById(int id);
	void updateByEnabled(int id);
	void updateTrainingCenterByReason(int  id,String  reason);
	List<TrainingCenter> findAllTrainingCenters(); 
    public List<TrainingCenter> getAllTrainingCenters(int index,int pagesize,String user);
    public int TrainingCenterCount(String username);
    public Page<TrainingPage> findByUsername(@Param("user") String user, Pageable pageable);
    public Page<TrainingPage> findByRegion(@Param("region") String region, Pageable pageable);
    public Page<TrainingPage> findAllTraining(Pageable pageable);
    public Long countAllTrainingCenter();
    public Long countTrainingCenterPerRegion(String region_id);
    public Long countTrainingCenterPerProvince(String province_id);

}