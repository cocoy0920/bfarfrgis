package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.PFOPage;
import ph.gov.da.bfar.frgis.model.LGU;

@Repository
public interface LGURepo extends JpaRepository<LGU, Integer> {

	@Query("SELECT c from LGU c where c.user = :username")
	public List<LGU> getListLGUByUsername(@Param("username") String username);
	
	@Query("SELECT c from LGU c where c.region_id = :region_id")
	public List<LGU> getListLGUByRegion(@Param("region_id") String region_id);

	@Modifying(clearAutomatically = true)
	@Query("update LGU f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update LGU f set f.reason =:reason  where f.id =:id")
	public void updateLGUByReason( @Param("id") int id, @Param("reason") String reason);
	
	
	@Query("SELECT COUNT(f) FROM LGU f WHERE f.user=:username")
	public int  LGUCount(@Param("username") String username);
	
	@Query("SELECT c from LGU c where c.uniqueKey = :uniqueKey")
	public LGU findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from LGU c where c.uniqueKey = :uniqueKey")
	List<LGU> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from LGU c where c.user = :username")
	 public List<LGU> getPageableLGU(@Param("username") String username,Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.PFOPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.lguName,c.lat,c.lon,c.enabled) from LGU c where c.user = :user")
	public Page<PFOPage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.PFOPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.lguName,c.lat,c.lon,c.enabled) from LGU c where c.region_id = :region_id")
	public Page<PFOPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.PFOPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.lguName,c.lat,c.lon,c.enabled) from LGU c")
	public Page<PFOPage> findAllPFO(Pageable pageable);

	@Query("SELECT COUNT(f) FROM LGU f")
	public Long countAllLGU();
	
	@Query("SELECT COUNT(f) FROM LGU f WHERE f.region_id=:region_id")
	public Long countLGUPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM LGU f WHERE f.province_id=:province_id")
	public Long countLGUPerProvince(@Param("province_id") String province_id);

}
