package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.TOSPage;
import ph.gov.da.bfar.frgis.model.TOS;

@Repository
public interface TOSRepo extends JpaRepository<TOS, Integer> {

	@Query("SELECT c from TOS c where c.user = :username")
	public List<TOS> getListTOSByUsername(@Param("username") String username);
	
	@Query("SELECT c from TOS c where c.region_id = :region_id")
	public List<TOS> getListTOSByRegion(@Param("region_id") String region_id);
	
	@Modifying(clearAutomatically = true)
	@Query("update TOS f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update TOS f set f.reason =:reason  where f.id =:id")
	public void updateTOSByReason( @Param("id") int id, @Param("reason") String reason);
	
	
	@Query("SELECT COUNT(f) FROM TOS f WHERE f.user=:username")
	public int  TOSCount(@Param("username") String username);
	
	@Query("SELECT c from TOS c where c.unique_key = :uniqueKey")
	public TOS findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from TOS c where c.unique_key = :uniqueKey")
	List<TOS> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from TOS c where c.user = :user")
	 public List<TOS> getPageableTOS(@Param("user") String user,Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.TOSPage ( c.id,c.unique_key,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled) from TOS c where c.user = :user")
	public Page<TOSPage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.TOSPage ( c.id,c.unique_key,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled) from TOS c where c.region_id = :region_id")
	public Page<TOSPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.TOSPage ( c.id,c.unique_key,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled) from TOS c")
	public Page<TOSPage> findAllTOS(Pageable pageable);

	@Query("SELECT COUNT(f) FROM TOS f")
	public Long countAllTOS();
	
	@Query("SELECT COUNT(f) FROM TOS f WHERE f.region_id=:region_id")
	public Long countTOSPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM TOS f WHERE f.province_id=:province_id")
	public Long countTOSPerProvince(@Param("province_id") String province_id);

}
