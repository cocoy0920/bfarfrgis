package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.LambakladPage;
import ph.gov.da.bfar.frgis.model.Lambaklad;


@Repository
public interface LambakladRepo extends JpaRepository<Lambaklad, Integer> {

	@Query("SELECT c from Lambaklad c where c.user = :username")
	public List<Lambaklad> getListLambakladByUsername(@Param("username") String username);
	
	@Query("SELECT c from Lambaklad c where c.region_id = :region_id")
	public List<Lambaklad> getListLambakladByRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM Lambaklad f WHERE f.user=:username")
	public int  LambakladCount(@Param("username") String username);
	
	@Query("SELECT c from Lambaklad c where c.uniqueKey = :uniqueKey")
	public Lambaklad findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from Lambaklad c where c.uniqueKey = :uniqueKey")
	List<Lambaklad> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from Lambaklad c where c.user = :username")
	 public List<Lambaklad> getPageableLambaklads(@Param("username") String username,Pageable pageable);
	
	@Modifying(clearAutomatically = true)
	@Query("update Lambaklad f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update Lambaklad f set f.reason =:reason  where f.id =:id")
	public void updateLambakladByReason( @Param("id") int id, @Param("reason") String reason);
	
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.LambakladPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled)  from Lambaklad c where c.user = :user")
	public Page<LambakladPage> findByUsername(@Param("user") String user, Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.LambakladPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled)  from Lambaklad c where c.region_id = :region_id")
	public Page<LambakladPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.LambakladPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled)  from Lambaklad c")
	public Page<LambakladPage> findAllLambaklad(Pageable pageable);
	
	
	
	@Query("SELECT COUNT(f) FROM Lambaklad f")
	public Long countAllLambaklad();
	
	 @Query("SELECT COUNT(f) FROM Lambaklad f WHERE f.region_id=:region_id")
	 public Long countLambakladPerRegion(@Param("region_id") String region_id);
	 
	@Query("SELECT COUNT(f) FROM Lambaklad f WHERE f.province_id=:province_id")
	public Long countLambakladPerProvince(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM Lambaklad f WHERE f.municipality_id=:municipality_id")
	public Long countLambakladPerMunicipality(@Param("municipality_id") String municipality_id);

	@Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM Lambaklad c WHERE c.image = :image")
	public boolean isImageAlreadyPresent(@Param("image") String image);

	
}
