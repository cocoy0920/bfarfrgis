package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.MariculturePage;
import ph.gov.da.bfar.frgis.model.MaricultureZone;
import ph.gov.da.bfar.frgis.repository.MaricultureRepo;


@Service("MaricultureZoneService")
@Transactional
public class MaricultureZoneServiceImpl implements MaricultureZoneService{

	@Autowired
	private MaricultureRepo dao;


	
	public Optional<MaricultureZone> findById(int id) {
		return dao.findById(id);
	}

	public List<MaricultureZone> ListByUsername(String username) {
		List<MaricultureZone>maricultureZone = dao.getListMaricultureZoneByUsername(username);
		return maricultureZone;
	}

	public void saveMaricultureZone(MaricultureZone maricultureZone) {
		dao.save(maricultureZone);
	}

	@Override
	public void deleteMaricultureZoneById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateByEnabled(int id) {
	dao.updateByEnabled(id);
		
	}

	@Override
	public void updateMaricultureZoneByReason(int id, String reason) {
		dao.updateMaricultureZoneByReason(id, reason);
		
	}

	public List<MaricultureZone> findAllMaricultureZones() {
		return dao.findAll();
	}

	@Override
	public List<MaricultureZone> getAllMaricultureZones(int index, int pagesize, String user) {
		
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<MaricultureZone> page = dao.getPageableMaricultureZone(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int MaricultureZoneCount(String username) {
		return dao.MaricultureZoneCount(username);
	}

	@Override
	public MaricultureZone findByUniqueKey(String uniqueKey) {
	
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<MaricultureZone> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<MariculturePage> findByUsername(String user, Pageable pageable) {

		return dao.findByUsername(user, pageable);
	}
	
	@Override
	public Page<MariculturePage> findAllMariculture(Pageable pageable) {

		return dao.findAllMariculture(pageable);
	}
	
	@Override
	public Page<MariculturePage> findByRegion(String region, Pageable pageable) {

		return dao.findByRegion(region, pageable);
	}

	@Override
	public List<MaricultureZone> ListByRegion(String region_id) {

		return dao.getListMaricultureZoneByRegion(region_id);
	}

	@Override
	public List<MaricultureZone> ListByRegion(String region_id, String type) {
		
		return dao.getListMaricultureZoneByRegion(region_id, type);
	}

	@Override
	public List<MaricultureZone> findAllMaricultureZones(String type) {

		return dao.findAllByType(type);
	}

	@Override
	public Long countMariculturePerRegion(String region_id) {
		
		return dao.countMaricultureZonePerRegion(region_id);
	}

	@Override
	public Long countMariculturePerProvince(String province_id) {
	
		return dao.countMaricultureZonePerProvince(province_id);
	}

	@Override
	public Long countAllMaricultureZone() {

		return dao.countAllMaricultureZone();
	}

	@Override
	public Long countMariculturePerProvince(String province_id, String type) {

		return dao.countMaricultureZonePerProvince(province_id, type);
	}

	@Override
	public Long countMariculturePerMunicipality(String municipality_id) {

		return dao.countMaricultureZonePerMunicipality(municipality_id);
	}

	@Override
	public Long countMariculturePerMunicipality(String municipality_id, String type) {
		
		return dao.countMaricultureZonePerMunicipality(municipality_id, type);
	}

	@Override
	public Long countMariculturePerRegion(String region_id, String type) {

		return dao.countMaricultureZonePerRegion(region_id, type);
	}

	
}
