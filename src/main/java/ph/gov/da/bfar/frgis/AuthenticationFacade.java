package ph.gov.da.bfar.frgis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.UserService;


@Component
public class AuthenticationFacade implements IAuthenticationFacade {
	
	@Autowired
	UserService userService;

    @Override
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }
    
    public String getUsername() {
    	Authentication authentication = getAuthentication();
		return authentication.getName();
    }

	public User getUserInfo() {
		User user = new User();
		user = userService.findByUsername(getUsername());
		
		return user;
	}

	@Override
	public String getRegionID() {
		User user = new User();
		user = userService.findByUsername(getUsername());
		
		return user.getRegion_id();
	}

	@Override
	public String getRegionName() {
		User user = new User();
		user = userService.findByUsername(getUsername());
		
		return user.getRegion();
	}
	
	
	
}
