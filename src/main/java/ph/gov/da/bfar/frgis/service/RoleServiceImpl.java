package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.model.Role;
import ph.gov.da.bfar.frgis.repository.RoleRepository;

@Service("RoleService")
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	RoleRepository roleRepository;
	
	@Override
	public Optional<Role> findById(int id) {
		return roleRepository.findById(id);
	}

	@Override
	public void save(Role role) {
		roleRepository.save(role);

	}

	@Override
	public List<Role> findAllRoles() {
		
		return roleRepository.findAll();
	}

}
