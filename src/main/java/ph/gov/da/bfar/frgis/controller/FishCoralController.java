package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.validation.Valid;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.FishCoralPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.FishCoral;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.service.FishCorralService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.PageUtils;
import ph.gov.da.bfar.frgis.web.util.ResourceData;
import ph.gov.da.bfar.frgis.web.util.ResourceMapsData;

@Controller
@RequestMapping("/create")
public class FishCoralController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	FishCorralService fishCorralService;
	
	 @Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;	
	@RequestMapping(value = "/fishcorals", method = RequestMethod.GET)
	public ModelAndView fishcorals(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		map_data="";
		user   = new User();
		
		user = authenticationFacade.getUserInfo();
//		Set<UserProfile> setOfRole = new HashSet<>();
//		setOfRole = user.getUserProfiles();
//		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
//		UserProfile role = list.get(0);
		
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "fishcorals");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}
	@RequestMapping(value = "/fishcoral", method = RequestMethod.GET)
	public ModelAndView fishcoral(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

//		User user = new User();		 
		user = authenticationFacade.getUserInfo();
//		Set<UserProfile> setOfRole = new HashSet<>();
//		setOfRole = user.getUserProfiles();
//		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
//		UserProfile role = list.get(0);
//		
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "fishcorals");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}

	@RequestMapping(value = "/fishcoralMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();
		 
		 ObjectMapper mapper = new ObjectMapper();
			FishCoral data = fishCorralService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	
		 	Features features = new Features();
			 features = MapBuilder.getFishCorralMapData(data);
			 
			 
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	mapsData.setIcon("/static/images/pin2022/FishCorral.png");
		 	
		 	mapsData.setHtml(features.getInformation());
		 	//+"<a href='#' onclick='viewDeatails(" + data.getUniqueKey() + "); return false;'>LINK</a>");
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			map_data = jsonString;
		
		result = new ModelAndView("redirect:../fishcoral");
		
	
		return result;
	}
	@RequestMapping(value = "/fishcoralDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();
		 
		// fishCorralService.deleteFishCoral(id);
		 fishCorralService.UpdateForEnabled(id);
		
		result = new ModelAndView("redirect:../fishcoral");
		
	
		return result;
	}
	@RequestMapping(value = "/fishcoralDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView fishcoralDeleteById(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;

		 fishCorralService.UpdateForEnabled(id, reason);
		 fishCorralService.UpdateForEnabled(id);
			
		result = new ModelAndView("redirect:../../fishcoral");
		
	
		return result;
	}	


	@RequestMapping(value = "/fishcorals/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishcorals(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishCoralPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 15, Sort.by("id"));
	      
	          page = fishCorralService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<FishCoralPage> FishCoralList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishCoral(FishCoralList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/fishcoralList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getfishcoralList() throws JsonGenerationException, JsonMappingException, IOException {


		List<FishCoral> list = null;

		list = fishCorralService.ListByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	

	@RequestMapping(value = "/getFishCoral/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishCoralById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<FishCoral> mapsDatas = fishCorralService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}
	

	@RequestMapping(value = "/saveFishCoral", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String getFishCorral(@RequestBody FishCoral fishCoral) throws JsonGenerationException, JsonMappingException, IOException {


		savingResources(fishCoral);
		

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
		

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);

	}
	private void savingResources(FishCoral fishCoral) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		fishCoral.setRegion_id(region_data[0]);
		fishCoral.setRegion(region_data[1]);

		province = fishCoral.getProvince();
		data = province.split(",", 2);
		fishCoral.setProvince_id(data[0]);
		fishCoral.setProvince(data[1]);

		municipal = fishCoral.getMunicipality();
		municipal_data = municipal.split(",", 2);
		fishCoral.setMunicipality_id(municipal_data[0]);
		fishCoral.setMunicipality(municipal_data[1]);

		barangay = fishCoral.getBarangay();
		barangay_data = barangay.split(",", 3);
		fishCoral.setBarangay_id(barangay_data[0]);
		fishCoral.setBarangay(barangay_data[1]);
		fishCoral.setEnabled(true);

		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(fishCoral.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		// fishCoral.setImage("/images/" +imageNameSrc + ".jpeg");
		/*String imgType = Base64ImageUtil.getImageType(fishCoral.getImage_src());
        
        String imageName = imageNameSrc + "fishCoral_"+ java.time.LocalDate.now()+ "." + imgType;
        
        fishCoral.setImage(imageName);*/

		if (fishCoral.getId() != 0) {
			Optional<FishCoral> fishcoral = fishCorralService.findById(fishCoral.getId());
			String image_server = fishcoral.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				fishCoral.setImage("/images/" + image_server);
				newUniqueKeyforMap = fishCoral.getUniqueKey();				
				fishCorralService.saveFishCoral(ResourceData.getFishCoral(fishCoral, user, "", UPDATE));
				
			}else {
				newUniqueKeyforMap = fishCoral.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", "");  
				fishCoral.setImage("/images/" + imageName);				
				Base64Converter.convertToImage(fishCoral.getImage_src(), fishCoral.getImage());
				fishCorralService.saveFishCoral(ResourceData.getFishCoral(fishCoral, user, "", UPDATE));
			}
		
			save = true;

		} else {			
			newUniqueKeyforMap = newUniqueKey;	
			imageName.replaceAll("[^a-zA-Z0-9]", "");
			fishCoral.setImage("/images/" + imageName);
			Base64Converter.convertToImage(fishCoral.getImage_src(), fishCoral.getImage());
			fishCorralService.saveFishCoral(ResourceData.getFishCoral(fishCoral, user, newUniqueKey, SAVE));
			
			save = true;

		}

	}

	@RequestMapping(value = "/getLastPageFCoral", method = RequestMethod.GET)
	public @ResponseBody String getLastPageFCoral(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = fishCorralService.FishCoralCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}

	
}
