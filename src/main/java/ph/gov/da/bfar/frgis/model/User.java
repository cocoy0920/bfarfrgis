package ph.gov.da.bfar.frgis.model;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Entity
@Table(name="app_user")
public class User implements Serializable{



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private int id;

	@NotEmpty
	@Column(name="sso_id", unique=true, nullable=false)
	//@Column(name="sso_id", nullable=false)
	private String username;
	
	
	@Column(name="password", nullable=false)
	private String password;
	
	//@NotEmpty
	@Column(name="pass", nullable=false)
	private String pass;
	
	@Column(name="logged", nullable=false)
	private String logged;
		
	@NotEmpty
	@Column(name="first_name", nullable=false)
	private String name;

	@NotEmpty
	@Column(name="last_name", nullable=false)
	private String lastName;

	@NotEmpty
	@Column(name="email", unique=true,nullable=false)
	private String email;
	
	@NotEmpty
	@Column(name="state", nullable=false)
	private String state;
	
	@Column(name="enabled")
	private boolean enabled;

	
	private String region;
	
	
	@Column(name="province_id")
	private String province_id;
	
	@Column(name="province")
	private String province;
	
	
	@Column(name="region_id")
	private String region_id;
	
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "app_user_user_profile", 
             joinColumns = { @JoinColumn(name = "user_id") }, 
             inverseJoinColumns = { @JoinColumn(name = "user_profile_id") })
	private Set<UserProfile> userProfiles = new HashSet<UserProfile>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}
	public String getRegion_id() {
		return region_id;
	}

	
	public void setRegion_id(String region_id) {
		this.region_id = region_id;
	}

	public Set<UserProfile> getUserProfiles() {
		return userProfiles;
	}

	public void setUserProfiles(Set<UserProfile> userProfiles) {
		this.userProfiles = userProfiles;
	}




	public User() {
		super();
		this.enabled = true;
		
	}

	/**
	 * @return the pass
	 */
	public String getPass() {
		return pass;
	}

	/**
	 * @param pass the pass to set
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}

	/**
	 * @return the logged
	 */
	public String getLogged() {
		return logged;
	}

	/**
	 * @param logged the logged to set
	 */
	public void setLogged(String logged) {
		this.logged = logged;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getProvince_id() {
		return province_id;
	}

	public void setProvince_id(String province_id) {
		this.province_id = province_id;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}


	
}
