package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.model.Barangay;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.Municipalities;
import ph.gov.da.bfar.frgis.model.Payao;
import ph.gov.da.bfar.frgis.model.Provinces;
import ph.gov.da.bfar.frgis.model.Regions;

import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserBean;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.service.BarangayService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.MunicipalityService;
import ph.gov.da.bfar.frgis.service.PayaoService;
import ph.gov.da.bfar.frgis.service.ProvinceService;
import ph.gov.da.bfar.frgis.service.RegionsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.ChartsInterface;
import ph.gov.da.bfar.frgis.web.util.ChartsUtil;
import ph.gov.da.bfar.frgis.web.util.IResourceMapsData;
import ph.gov.da.bfar.frgis.web.util.PageUtils;
import ph.gov.da.bfar.frgis.web.util.RegexMatches;

@Controller
@RequestMapping("/create")
public class UtilController{
	
	@Autowired
	UserService userService;

	
	@Autowired
	ProvinceService provinceService;

	@Autowired
	RegionsService regionsService;

	@Autowired
	MunicipalityService municipalityService;
	
	@Autowired
	BarangayService barangayService;
	
	@Autowired
	MapsService mapsService;
		
	@Autowired
	PayaoService payaoService;
	
	
	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	ChartsInterface chartsUtil;
	
	@Autowired
	IResourceMapsData resourceMapsData;
	
//	@Autowired
//	EngineService engineService;
	
	private String abbr = "";
	private int count=0;
	private String provinces;
	private String provinceName="";
	private String provinceID="";
	private String municipalityID="";
	List<Municipalities> municipalitiesList = new ArrayList<>();
	List<Provinces> provincesList = new ArrayList<>();
	List<Regions> regionsList = new ArrayList<>();
	private ChartsUtil  charts=null;
	private String page="";
	private boolean show_hide_chart=true;
	private boolean skip_page_show=false;
	private String map_data="";
	
	@RequestMapping(value = "/createForms", method = RequestMethod.GET)
	public ModelAndView createForms(ModelMap model) throws Exception {
		
		ModelAndView result = new ModelAndView();
		map_data="";
		

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", authenticationFacade.getUserInfo());

		model.addAttribute("username", authenticationFacade.getUsername());
		model.addAttribute("region", authenticationFacade.getRegionName());
		//model.addAttribute("page", page);
		result = new ModelAndView("users/create");

		return result;
	}
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public ModelAndView userHome(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();
		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		 	ObjectMapper mapper = new ObjectMapper();
			//List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername());
			String jsonString = mapper.writeValueAsString("kiko");
			
			//System.out.println(jsonString);
			//model.addAttribute("mapsData", jsonString);*/


		model.addAttribute("set_center", PageUtils.getCenterMapPerRegion(user.getRegion_id()));
		model.addAttribute("region_map", PageUtils.getRegionName(user.getRegion_id()));
		model.addAttribute("mapsData", jsonString);
		//model.addAttribute("chart_by_region", charts.getByRegion(authenticationFacade.getRegionID()));
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		result = new ModelAndView("users/index_home");

		return result;

	}

	@RequestMapping(value = "/getUserInfo", method = RequestMethod.GET)
	public ModelAndView getUserInfo(ModelMap model) {
		ModelAndView modelAndView = new ModelAndView();
		
		
		User user = new User();
		
		user = authenticationFacade.getUserInfo();
		
		
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
	//	System.out.println("ROLE: " + setOfRole);
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
	
		
		
//		List<Provinces> provinces = null;
//		
//		provinces = provinceService.findByRegion(user.getRegion_id());
//				
//
//		model.addAttribute("provinces", provinces);
		 model.addAttribute("role", role);
		 model.addAttribute("user", user);

		
		
		modelAndView.setViewName("users/register"); // resources/template/register.html
		return modelAndView;
		
	}

	@RequestMapping(value = "/getRegionList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getRegionList()
			throws JsonGenerationException, JsonMappingException, IOException {

		
		List<Regions> regions = regionsService.findAllRegions();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonArray = gson.toJson(regions);
		
		return jsonArray;

	}
	
	
	@RequestMapping(value = "/getProvinceList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getProvinceList()
			throws JsonGenerationException, JsonMappingException, IOException {

		
		//Regions region = regionsService.findbyRegion(user.getRegion_id());
		List<Provinces> provinces = provinceService.findByRegion(getUserInfo().getRegion_id());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonArray = gson.toJson(provinces);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getProvinceList/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getProvinceListForAdmin(@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		
		//Regions region = regionsService.findbyRegion(user.getRegion_id());
		List<Provinces> provinces = provinceService.findByRegion(region);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonArray = gson.toJson(provinces);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getProvince/{region}", method = RequestMethod.GET)
	public @ResponseBody List<Provinces> getAllProvinceByRegion(@PathVariable("region") String region) {
		//logger.info(region);

//		String s = region;
//		String[] data = s.split(",", 2);
//		String regionId = data[0];
		return provinceService.findByRegion(region);

	}

	@RequestMapping(value = "/getMunicipalityList/{province_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMunicipalityList(@PathVariable("province_id") String province_id)
			throws JsonGenerationException, JsonMappingException, IOException {
		
		List<Municipalities> municipalities = municipalityService.findByProvince(province_id);
	
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonArray = gson.toJson(municipalities);

		return jsonArray;

	}

	@RequestMapping(value = "/getBarangayList/{municipal_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getBarangayList(@PathVariable("municipal_id") String municipal_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		List<Barangay> barangays = barangayService.findByMunicipality(municipal_id);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonArray = gson.toJson(barangays);
		// logger.info("getBarangayList: " + jsonArray);
		return jsonArray;

	}
	


	@RequestMapping(value = "/getMunicipality/{province}", method = RequestMethod.GET)
	public @ResponseBody List<Municipalities> getAllSubcategories(@PathVariable("province") String province) {
	//	logger.info(province);

		String s = province;
		String[] data = s.split(",", 2);
		String provinceId = data[0];
		return municipalityService.findByProvince(provinceId);

	}

	@RequestMapping(value = "/getBarangay/{municipal}", method = RequestMethod.GET)
	public @ResponseBody List<Barangay> getAllBarangay(@PathVariable("municipal") String municipal) {
		//logger.info(municipal);

		String s = municipal;
		String[] data = s.split(",", 2);
		String municipalId = data[0];
		return barangayService.findByMunicipality(municipalId);

	}

	@RequestMapping(value = "/code/{municipality}/{page}", method = RequestMethod.GET)
	public @ResponseBody String getCode(@PathVariable("municipality") String municipality,
			@PathVariable("page") String page) {
		String code = "";

		String resourcesCode = getAbbr(page);
		code = resourcesCode;

		return code;

	}
	@RequestMapping(value = "/code/{municipality}/{page}", method = RequestMethod.POST)
	public @ResponseBody String postCode(@PathVariable("municipality") String municipality,
			@PathVariable("page") String page) {
		String code = "";

		String m = municipality;
		String[] m_data = m.split(",", 2);
		String municipalId = m_data[0];
		String resourcesCode = getAbbr(page);
		// String pm = municipalId.substring(0, 5);

		LocalDate today = LocalDate.now();
		int currentDate = today.getDayOfMonth();
		int currentMonth = today.getMonthValue();
		int currentYear = today.getYear();
		UUID newID = UUID.randomUUID();
		String getUUID = newID.toString();
		String[] getuuid = getUUID.split("-", 4);

		code = municipalId + "-" + currentYear + currentDate + currentMonth + "-" + resourcesCode;

		return code;

	}
	private String getAbbr(String page) {
		UUID newID = UUID.randomUUID();
		String region = authenticationFacade.getRegionName().replaceAll("\\s", "");
		
		LocalDate today = LocalDate.now();
		int currentDate = today.getDayOfMonth();
		int currentMonth = today.getMonthValue();
		int currentYear = today.getYear();
		
		if (page.equals("fishsanctuaries")) {
			
			long row_count = chartsUtil.getFishSanctuaryCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_FS_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;
			
		} else if (page.equals("fishprocessingplants")) {
			
			long row_count =chartsUtil.getFishProcessingCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_FPROC_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;
			
		} else if (page.equals("fishlanding")) {
			
			long row_count =chartsUtil.getFishlandingCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_FL_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;
			
		} else if (page.equals("fishpen")) {
			
			long row_count =chartsUtil.getFishpenCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_FP_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;

		} else if (page.equals("fishpond")) {
			
			long row_count =chartsUtil.getFishpondCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_FPD_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;

		} else if (page.equals("fishcage")) {
			
			long row_count =chartsUtil.getFishcageCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_FC_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;

		} else if (page.equals("hatchery")) {
			
			long row_count =chartsUtil.getHatcheryCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_FH_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;	

		} else if (page.equals("iceplantorcoldstorage")) {
			
			long row_count =chartsUtil.getColdCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_IPCS_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;

		} else if (page.equals("FISHPORT")) {

			long row_count =chartsUtil.getFishportCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_FPR_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;

		} else if (page.equals("fishcorals")) {

			long row_count =chartsUtil.getFishcoralCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_FCR_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;

		} else if (page.equals("marineprotected")) {

			long row_count =chartsUtil.getMaricultureCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_MPA_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;

		} else if (page.equals("market")) {

			long row_count =chartsUtil.getMarketCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_MKT_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;

		} else if (page.equals("schooloffisheries")) {

			long row_count =chartsUtil.getSchoolCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_SF_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;

		} else if (page.equals("seagrass")) {

			long row_count =chartsUtil.getSeagrassCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_SG_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;

		} else if (page.equals("seaweeds")) {

			long row_count =chartsUtil.getSeaweedsCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_SW_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;

		} else if (page.equals("mangrove")) {

			long row_count =chartsUtil.getMangroveCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_MG_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;

		} else if (page.equals("lgu")) {

			long row_count =chartsUtil.getPFOCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_LGU_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;

		} else if (page.equals("trainingcenter")) {

			long row_count =chartsUtil.getTrainingCenterCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_TC_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;

		} else if (page.equals("mariculturezone")) {

			long row_count =chartsUtil.getMaricultureCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_MCZ_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;
		}else if (page.equals("payao")) {

			long row_count =chartsUtil.getPayaoCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_PY_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;
		}else if (page.equals("lambaklad")) {

			long row_count =chartsUtil.getLambakladCountByRegion(authenticationFacade.getRegionID());
			abbr = region+"_LBD_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;
		}else if (page.equals("tos")) {

			long row_count =chartsUtil.getTOSByRegion(authenticationFacade.getRegionID());
			abbr = region+"_TOS_"+currentMonth + currentDate + currentYear + "_" + ++row_count ;
		}
		
		

		return abbr;
	}

	@RequestMapping(value = "/latValue/{latValue}", method = RequestMethod.GET)
	public @ResponseBody String getlatValue(@PathVariable("latValue") String latValue) {
		//logger.info("LATITUDE: " + latValue);

		RegexMatches matches = new RegexMatches();
		boolean match = matches.validateLat(latValue);
		String latData = "";
		if (match) {
			latData = latValue;
		} else {
			latData = "invalid";
		}
		return latData;

	}

	@RequestMapping(value = "/areaValue/{areaValue}", method = RequestMethod.GET)
	public @ResponseBody String getareaValue(@PathVariable("areaValue") String areaValue) {
		//logger.info(areaValue);

		RegexMatches matches = new RegexMatches();
		boolean match = matches.validateArea(areaValue);
		String latData = "";
		if (match) {
			latData = areaValue;
		} else {
			latData = "invalid";
		}
		return latData;

	}

	@RequestMapping(value = "/lonValue/{lonValue}", method = RequestMethod.GET)
	public @ResponseBody String getlonValue(@PathVariable("lonValue") String lonValue) {
		//logger.info("LONGITUDE: " + lonValue);
		RegexMatches matches = new RegexMatches();
		boolean match = matches.validateLon(lonValue);
		String lonData = "";
		if (match) {
			lonData = lonValue;
		} else {
			lonData = "invalid";
		}
		return lonData;

	}

	@RequestMapping(value = "/resources-get-by-pages", 
			method = RequestMethod.GET)	
		public ModelAndView PagesHomeUser(
				@RequestParam(value="resources[]") String[] resources,ModelMap model
					) throws JsonGenerationException, JsonMappingException, IOException{
		 ModelAndView result;
		
		int i;
		String page;
		List<MapsData> mapsDatas = null;
		String jsonString = "";
		ObjectMapper mapper = new ObjectMapper();
		for (i = 0; i < resources.length; i++) {
			  
            // accessing each element of array
			page = resources[i];
            System.out.print(page + " Pages to fetch in Database   ::" + "\r");
            
             mapsDatas = mapsService.findAllMapsByResources(page);
             
             jsonString += mapper.writeValueAsString(mapsDatas) + "\r";
             
             System.out.print(jsonString);
        }

	
			
		
			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
			result = new ModelAndView("users/index_home");
        
			return result;
		}
	

	private User getUserInfo() {
		User user = new User();
		user = userService.findByUsername(authenticationFacade.getUsername());
		
		
		return user;
	}
	
	
	@RequestMapping(value = "/getGeoJsonByRegion", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonByRegion()throws JsonGenerationException, JsonMappingException, IOException {

		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));

	}
	@RequestMapping(value = "/getGeoJsonByRegion/{resources}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonByRegionAndResources(@PathVariable("resources") String resources)throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return alltoJson(featuresData.getFeaturesListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion(authenticationFacade.getRegionID(),resources));

	}
	@RequestMapping(value = "/getGeoJsonByRegion/{resources}/{type}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonByRegionAndResources(@PathVariable("resources") String resources,@PathVariable("type") String type)throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return alltoJson(featuresData.getFeaturesListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion(authenticationFacade.getRegionID(),resources,type));

	}
	@RequestMapping(value = "/getAllGeoJson/{resources}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonByAllResources(@PathVariable("resources") String resources)throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return alltoJson(featuresData.getFeaturesListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListAll(resources));

	}

	
	@RequestMapping(value = "/getGeoJsonRegion", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegion1()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("010000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegion2", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegion2()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getFeaturesListByRegresourceMapsData.resourceMapsData.featuresListByRegioncade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("020000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegion3", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegion3()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.resourceMapsData.resourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("030000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegion4a", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegion4a()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("040000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegion4b", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegion4b()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("170000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegion5", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegion5()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("050000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegion6", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegion6()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("060000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegion7", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegion7()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("070000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegion8", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegion8()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("080000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegion9", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegion9()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("090000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegion10", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegion10()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("100000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegion11", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegion11()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("110000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegion12", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegion12()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("120000000"));

	}
	@RequestMapping(value = "/getGeoJsonNCR", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonNCR()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("130000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegionCAR", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegionCAR()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("140000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegionARMM", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegionARMM()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion("150000000"));

	}
	@RequestMapping(value = "/getGeoJsonRegion/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getGeoJsonRegionCARAGA(@PathVariable("region") String region)throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion(region));

	}
	@RequestMapping(value = "/getAllGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListAll());

	}
	
	@RequestMapping(value = "/getPAYAOGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getPAYAOGeoJson()throws JsonGenerationException, JsonMappingException, IOException {
		
		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getFeaturesListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(payaoListAll());

	}
	
	@RequestMapping(value = "/getPAYAOGeoJsonByRegion", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getPAYAOGeoJsonByRegion()throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getFeaturesListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(payaoByRegionListAll());

	}
	
//	@RequestMapping(value = "/getFishSanctuaryGeoJson", method = RequestMethod.GET, produces = "application/json")
//	public @ResponseBody String getFishSanctuaryGeoJson()throws JsonGenerationException, JsonMappingException, IOException {
//
//		return alltoJson(fishsanctuaryListAll());
//
//	}
//	
//	@RequestMapping(value = "/getFishPenGeoJson", method = RequestMethod.GET, produces = "application/json")
//	public @ResponseBody String getFishPenGeoJson()throws JsonGenerationException, JsonMappingException, IOException {
//			System.out.println(fishpenListAll());
//		return alltoJson(fishpenListAll());
//
//	}
	
	
	
	
	@RequestMapping(value = "/regionMap/{region}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String regionMapByRegion(@PathVariable("region") String region)throws JsonGenerationException, JsonMappingException, IOException {
		User user = new User();
		 
		user = authenticationFacade.getUserInfo();
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(PageUtils.getRegionName(region));
		
		return jsonString;

	}
	


	private ArrayList<Features> payaoListAll(){
		ArrayList<Features> list2 = new ArrayList<>();
		
		ArrayList<Payao> fslist = null;
		
		fslist = (ArrayList<Payao>) payaoService.findAllPayao();
		int sumOfAll = payaoService.PayaoSumAll();
		System.out.println("Total of all payao: " +sumOfAll);
		for(Payao data : fslist) {
			Features features = new Features();
			features.setResources("payao");
			features.setRegion(data.getRegion());
			features.setRegion_id(data.getRegion_id());
			features.setProvince(data.getProvince());
			features.setProvince_id(data.getProvince_id());
			features.setMunicipality(data.getMunicipality());
			features.setMunicipality_id(data.getMunicipality_id());
			//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
			features.setInformation(
					 
					"<div class=\"p-3 mb-2 bg-primary text-white text-center\">PAYAO</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table class=\"table table-sm table-dark\">" + 
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Association:</td> " + 
					"<td>" +data.getAssociation() + "</td></tr>" +
					"<tr><td>Beneficiary:</td> " + 
					"<td>" +data.getBeneficiary() + "</td></tr>" +
					"<tr><td>Payao:</td> " + 
					"<td>" +data.getRemarks() + "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

			features.setLatitude(data.getLat());
			features.setLongtitude(data.getLon());
			features.setName(data.getBeneficiary());
			//features.setImage(data.getImage());
			list2.add(features);
		}
		
		
		return list2;
	}
		
	private ArrayList<Features> payaoByRegionListAll(){
		ArrayList<Features> list2 = new ArrayList<>();
		
		ArrayList<Payao> fslist = null;
		
		fslist = (ArrayList<Payao>) payaoService.ListByRegion(authenticationFacade.getRegionID());
		
		for(Payao data : fslist) {
			Features features = new Features();
			features.setResources("payao");
			features.setRegion(data.getRegion());
			features.setRegion_id(data.getRegion_id());
			features.setProvince(data.getProvince());
			features.setProvince_id(data.getProvince_id());
			features.setMunicipality(data.getMunicipality());
			features.setMunicipality_id(data.getMunicipality_id());
			//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
			features.setInformation(
					 
					"<div class=\"p-3 mb-2 bg-primary text-white text-center\">PAYAO</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table class=\"table table-sm table-dark\">" + 
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Association:</td> " + 
					"<td>" +data.getAssociation() + "</td></tr>" +
					"<tr><td>Beneficiary:</td> " + 
					"<td>" +data.getBeneficiary() + "</td></tr>" +
					"<tr><td>Payao:</td> " + 
					"<td>" +data.getRemarks() + "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

			features.setLatitude(data.getLat());
			features.setLongtitude(data.getLon());
			features.setName(data.getBeneficiary());
			//features.setImage(data.getImage());
			list2.add(features);
		}
		
		
		return list2;
	}
	//kiko
	


	
	

	
	
	
//	public Map CountResources() {
//		HashMap<String, Object> hashMap = new HashMap<>();
//		String region_id = authenticationFacade.getRegionID();
//		List<Provinces> provinceList = new ArrayList<>();
//		
//		provinceList = provinceService.findByRegion(region_id);
//		
//		for(Provinces provinces:provinceList) {
//			Long fs_count = fishSanctuariesService.countFishSanctuariesPerProvince(provinces.getProvince_id());
//			hashMap.put(provinces.getProvince(), fs_count);
//		}
//
//		return hashMap;
//	}
//	
//	@RequestMapping(value = "bar", method = RequestMethod.GET)
//	public ModelAndView adminhome(ModelMap model) {
//		ModelAndView modelAndView = new ModelAndView();
//		
//
//		modelAndView.setViewName("/region/Bar"); // resources/template/admin.html
//		return modelAndView;
//	}
	
	@RequestMapping(value = "/updateUserInfo", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String saveUserRegistration(@RequestBody UserBean userbean) throws JsonGenerationException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();


			System.out.println(userbean);

			User user = new User();
			user.setId(userbean.getId());
			user.setRegion(userbean.getRegion());
			user.setRegion_id(userbean.getRegion_id());
			user.setName(userbean.getFirstName());
			user.setLastName(userbean.getLastName());
			user.setUsername(userbean.getUsername());
			user.setState("Active");
			user.setEmail(userbean.getEmail());
			user.setEnabled(true);
			user.setPass(userbean.getPassword());
			Set<UserProfile> userProfiles = new HashSet<UserProfile>();
			UserProfile profile = new UserProfile();
			

			String role = userbean.getUserProfiles();
			String[] role_data = role.split(",", 2);
			
			profile.setId(Integer.parseInt(role_data[0]));	
			profile.setType(role_data[1]);
			
			userProfiles.add(profile);
			user.setUserProfiles(userProfiles);
						
			userService.saveUser(user);

	
			
		
		


		return objectMapper.writeValueAsString("User Info Updated successfully!");

	}
	
	
	@RequestMapping(value = "bar", method = RequestMethod.GET)
	public ModelAndView mainChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());

		 
		 	model.addAttribute("region_title",authenticationFacade.getRegionName());
			//model.addAttribute("chart_by_region", getByRegion(authenticationFacade.getRegionID()));
			//model.addAttribute("province_title",provinceName);
			//model.addAttribute("provinceChart",chartsUtil.getChartByProvince(provinces));
		 	model.addAttribute("user", user);
		 	model.addAttribute("provinces", provinces);
			model.addAttribute("page",false);
			model.addAttribute("show",true);
			modelAndView.setViewName("/users/Bar");
			
		return modelAndView;
	}
	@RequestMapping(value = "canvas", method = RequestMethod.GET)
	public ModelAndView canvasChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		
		model.addAttribute("user", user);
		modelAndView.setViewName("/region/canvas");
			
		return modelAndView;
	}
	@RequestMapping(value = "aquaChartByRegion", method = RequestMethod.GET)
	public ModelAndView aquaChartByRegion(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		
		 List<Regions> regions = regionsService.findAllRegions();
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 System.out.println("regionsList: " + regionsList);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("provinceChart",chartsUtil.getAquafarmChartByRegion(regionsList));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("regions", regions); 
				model.addAttribute("type", "region");
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "aquaChartByRegion";


			modelAndView.setViewName("/charts/aquafarm");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "aquaChart", method = RequestMethod.GET)
	public ModelAndView AquaChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());

		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("provinceChart",chartsUtil.getAquafarmChartByProvince(provincesList));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("type", "province");
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "aquaChart";


			modelAndView.setViewName("/charts/aquafarm");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "fishpondChart", method = RequestMethod.GET)
	public ModelAndView fishpondChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
				
		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 
		 		model.addAttribute("user", user);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("provinceChart",chartsUtil.getAquafarmChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("type", "province");
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
			//	page = "aquaChart";


			modelAndView.setViewName("/charts/fishpond");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "fishpondAllChart", method = RequestMethod.GET)
	public ModelAndView fishpondAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
				
		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		
		 //List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		// Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 List<Regions> regions = regionsService.findAllRegions();
		 
		 		model.addAttribute("user", user);
		 		model.addAttribute("regionID","123");
		 		model.addAttribute("region_description", "CENTRAL OFFICE");
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("provinceChart",chartsUtil.getAquafarmChartByRegion(regions));
				model.addAttribute("type", "province");
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
			//	page = "aquaChart";


			modelAndView.setViewName("/charts/fishpond");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "fishpenChart", method = RequestMethod.GET)
	public ModelAndView fishpenChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
			
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("provinceChart",chartsUtil.getAquafarmChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("type", "province");
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
			//	page = "aquaChart";


			modelAndView.setViewName("/charts/fishpen");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "fishpenAllChart", method = RequestMethod.GET)
	public ModelAndView fishpenAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		List<Regions> regions = regionsService.findAllRegions();

		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
			
		 		model.addAttribute("regionID","123");
		 		model.addAttribute("region_description", "REGION ");
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("provinceChart",chartsUtil.getAquafarmChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("type", "region");
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
			//	page = "aquaChart";


			modelAndView.setViewName("/charts/fishpen");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "fishcageChart", method = RequestMethod.GET)
	public ModelAndView fishcageChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
			 	User user = new User();		 
				user = authenticationFacade.getUserInfo();
				model.addAttribute("user", user);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", "Province - " + region.getRegion() + "("+ region.getDescription() + ")");
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("provinceChart",chartsUtil.getAquafarmChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("type", "province");
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
			//	page = "aquaChart";


			modelAndView.setViewName("/charts/fishcage");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "fishcageAllChart", method = RequestMethod.GET)
	public ModelAndView fishcageAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
				List<Regions> regions = regionsService.findAllRegions();
			 	User user = new User();		 
				user = authenticationFacade.getUserInfo();
				model.addAttribute("user", user);
		 		model.addAttribute("regionID","123");
		 		model.addAttribute("region_description","CENTRAL OFFICE");
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("provinceChart",chartsUtil.getAquafarmChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("type", "region");
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
			//	page = "aquaChart";


			modelAndView.setViewName("/charts/fishcage");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "fishcorralChart", method = RequestMethod.GET)
	public ModelAndView fishcorralChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("provinceChart",chartsUtil.getAquafarmChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("type", "province");
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
			//	page = "aquaChart";


			modelAndView.setViewName("/charts/fishcorral");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "fishcorralAllChart", method = RequestMethod.GET)
	public ModelAndView fishcorralAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		List<Regions> regions = regionsService.findAllRegions();
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
		 		model.addAttribute("regionID","123");
		 		model.addAttribute("region_description", "");
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("provinceChart",chartsUtil.getAquafarmChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("type", "region");
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
			//	page = "aquaChart";


			modelAndView.setViewName("/charts/fishcorral");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/aquaMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView AquaChartMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
			 	model.addAttribute("regionID",authenticationFacade.getRegionID());
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
			 	model.addAttribute("region_description", region.getDescription());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getAquafarmChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("type", "municipality");
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			//modelAndView.setViewName("/charts/province/aquafarm");
				modelAndView.setViewName("/charts/aquafarm");
				show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/aquaChart/{province}", method = RequestMethod.GET)
	public ModelAndView AquaChartByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="aquaMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../aquaMunicipalityChart");
 	
			
		return modelAndView;
	}
	
	@RequestMapping(value = "seaweedsNLChart", method = RequestMethod.GET)
	public ModelAndView seaweedsNLChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);			
			
				model.addAttribute("region_description", region.getDescription());
				model.addAttribute("type", "province");		
		 		
				model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				//model.addAttribute("provinceChart",chartsUtil.getSeaweedsChartByProvince(provincesList));
				model.addAttribute("provinceChart",chartsUtil.getSeaweedsChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "seaweedsNLChart";


			modelAndView.setViewName("/charts/seaweeds");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "seaweedsNLAllChart", method = RequestMethod.GET)
	public ModelAndView seaweedsNLAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		List<Regions> regions = regionsService.findAllRegions();
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);			
			
				model.addAttribute("region_description", "CENTRAL OFFICE");
				model.addAttribute("type", "region");		
		 		
				model.addAttribute("regionID","123");			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				//model.addAttribute("provinceChart",chartsUtil.getSeaweedsChartByProvince(provincesList));
				model.addAttribute("provinceChart",chartsUtil.getSeaweedsChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "seaweedsNLChart";


			modelAndView.setViewName("/charts/seaweeds");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/seaweedsNLMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView seaweedsNLChartMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
				model.addAttribute("regionID",authenticationFacade.getRegionID());
				model.addAttribute("region_description", region.getDescription());
				model.addAttribute("type", "municipality");	
			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getSeaweedsChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/seaweeds");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/seaweedsNLChart/{province}", method = RequestMethod.GET)
	public ModelAndView seaweedsNLByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="seaweedsNLMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../seaweedsNLMunicipalityChart");
 	
			
		return modelAndView;
	}
	
	@RequestMapping(value = "maricultureChart", method = RequestMethod.GET)
	public ModelAndView maricultureChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
			
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
				model.addAttribute("region_description", region.getDescription());
				model.addAttribute("type", "province");
				
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				//model.addAttribute("provinceChart",chartsUtil.getMaricultureChartByProvince(provincesList));
				model.addAttribute("provinceChart",chartsUtil.getMaricultureChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "maricultureChart";


			modelAndView.setViewName("/charts/mariculture");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "maricultureAllChart", method = RequestMethod.GET)
	public ModelAndView maricultureAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();

		List<Regions> regions = regionsService.findAllRegions();
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
				model.addAttribute("region_description", "CENTRAL OFFICE");
				model.addAttribute("type", "region");
				
		 		model.addAttribute("regionID","123");			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				//model.addAttribute("provinceChart",chartsUtil.getMaricultureChartByProvince(provincesList));
				model.addAttribute("provinceChart",chartsUtil.getMaricultureChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "maricultureChart";


			modelAndView.setViewName("/charts/mariculture");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/maricultureMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView maricultureChartMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
		 		model.addAttribute("type", "municipality");	 
		 		
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getMaricultureChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/mariculture");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/maricultureChart/{province}", method = RequestMethod.GET)
	public ModelAndView maricultureLByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="maricultureMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../maricultureMunicipalityChart");
 	
			
		return modelAndView;
	}
	
	@RequestMapping(value = "hatcheryChart", method = RequestMethod.GET)
	public ModelAndView hatcheryChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
			
	 			model.addAttribute("region_description", region.getDescription());
	 			model.addAttribute("type", "province");	
	 			
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				//model.addAttribute("provinceChart",chartsUtil.getHatcheryChartByProvince(provincesList));
				model.addAttribute("provinceChart",chartsUtil.getHatcheryChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "hatcheryChart";


			modelAndView.setViewName("/charts/hatchery");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "hatcheryAllChart", method = RequestMethod.GET)
	public ModelAndView hatcheryAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		List<Regions> regions = regionsService.findAllRegions();
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
			
			model.addAttribute("region_description", "CENTRAL OFFICE");
			model.addAttribute("type", "region");		
	 		model.addAttribute("regionID","123");			
		 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				//model.addAttribute("provinceChart",chartsUtil.getHatcheryChartByProvince(provincesList));
				model.addAttribute("provinceChart",chartsUtil.getHatcheryChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "hatcheryChart";


			modelAndView.setViewName("/charts/hatchery");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/hatcheryMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView hatcheryChartMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());

			 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);
			 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
			 	User user = new User();		 
				user = authenticationFacade.getUserInfo();
				model.addAttribute("user", user);
			 	model.addAttribute("regionID",authenticationFacade.getRegionID());
			 	model.addAttribute("region_description", region.getDescription());
			 	model.addAttribute("type", "municipality");
			 	
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getHatcheryChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/hatchery");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/hatcheryChart/{province}", method = RequestMethod.GET)
	public ModelAndView hatcheryLByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="hatcheryMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../hatcheryMunicipalityChart");
 	
			
		return modelAndView;
	}
	
	@RequestMapping(value = "coldChart", method = RequestMethod.GET)
	public ModelAndView coldChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
		 		model.addAttribute("region_description", region.getDescription());
		 		model.addAttribute("type", "province");
		 		
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				//model.addAttribute("provinceChart",chartsUtil.getColdChartByProvince(provincesList));
				model.addAttribute("provinceChart",chartsUtil.getColdChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "coldChart";


			modelAndView.setViewName("/charts/cold");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "coldAllChart", method = RequestMethod.GET)
	public ModelAndView coldAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
				
			List<Regions> regions = regionsService.findAllRegions();
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
			
			model.addAttribute("region_description", "CENTRAL OFFICE");
			model.addAttribute("type", "region");		
	 		model.addAttribute("regionID","123");			
		 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				//model.addAttribute("provinceChart",chartsUtil.getColdChartByProvince(provincesList));
				model.addAttribute("provinceChart",chartsUtil.getColdChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "coldChart";


			modelAndView.setViewName("/charts/cold");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/coldMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView coldMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 		List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 		List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);
		 		Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
			 	User user = new User();		 
				user = authenticationFacade.getUserInfo();
				model.addAttribute("user", user);
			 	model.addAttribute("regionID",authenticationFacade.getRegionID());
			 	model.addAttribute("region_description", region.getDescription());
			 	model.addAttribute("type", "municipality");
			 	
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getColdChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/cold");
			show_hide_chart = true;
			return modelAndView;
	}
	@RequestMapping(value = "/coldChart/{province}", method = RequestMethod.GET)
	public ModelAndView coldByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="coldMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../coldMunicipalityChart");
 	
			
		return modelAndView;
	}
	@RequestMapping(value = "fishprocessingChart", method = RequestMethod.GET)
	public ModelAndView fishprocessingChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
			
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
	 			model.addAttribute("region_description", region.getDescription());
	 			model.addAttribute("type", "province");
	 			
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("provinceChart",chartsUtil.getFishProcessingChartByProvince(provinces));
				//model.addAttribute("provinceChart",chartsUtil.getFishProcessingChartByProvince(provincesList));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "fishprocessingChart";


			modelAndView.setViewName("/charts/fishprocessing");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "fishprocessingAllChart", method = RequestMethod.GET)
	public ModelAndView fishprocessingAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();

		List<Regions> regions = regionsService.findAllRegions();
	 	User user = new User();		 
		user = authenticationFacade.getUserInfo();
		model.addAttribute("user", user);
		
		model.addAttribute("region_description", "CENTRAL OFFICE");
		model.addAttribute("type", "region");		
 		model.addAttribute("regionID","123");			
	 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("provinceChart",chartsUtil.getFishProcessingChartByRegion(regions));
				//model.addAttribute("provinceChart",chartsUtil.getFishProcessingChartByProvince(provincesList));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "fishprocessingChart";


			modelAndView.setViewName("/charts/fishprocessing");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/fishprocessingMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView fishprocessingChartMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
		 		model.addAttribute("type", "municipality");
		 
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getFishProcessingChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/fishprocessing");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/fishprocessingChart/{province}", method = RequestMethod.GET)
	public ModelAndView fishprocessingChartByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="fishprocessingMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../fishprocessingMunicipalityChart");
 	
			
		return modelAndView;
	}
	@RequestMapping(value = "seaweedsWDChart", method = RequestMethod.GET)
	public ModelAndView seaweedsWDChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
			
			
				model.addAttribute("region_description", region.getDescription());
				model.addAttribute("type", "province");					 	
				User user = new User();		 
				user = authenticationFacade.getUserInfo();
				model.addAttribute("user", user);
				
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				//model.addAttribute("provinceChart",chartsUtil.getSeaweedsWDChartByProvince(provincesList));
				model.addAttribute("provinceChart",chartsUtil.getSeaweedsWDChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "seaweedsWDChart";


			modelAndView.setViewName("/charts/seaweedsWD");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "seaweedsWDAllChart", method = RequestMethod.GET)
	public ModelAndView seaweedsWDAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		List<Regions> regions = regionsService.findAllRegions();
	 	User user = new User();		 
		user = authenticationFacade.getUserInfo();
		model.addAttribute("user", user);
		
		model.addAttribute("region_description", "CENTRAL OFFICE");
		model.addAttribute("type", "region");		
 		model.addAttribute("regionID","123");			
	 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				//model.addAttribute("provinceChart",chartsUtil.getSeaweedsWDChartByProvince(provincesList));
				model.addAttribute("provinceChart",chartsUtil.getSeaweedsWDChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "seaweedsWDChart";


			modelAndView.setViewName("/charts/seaweedsWD");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/seaweedsWDMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView seaweedsWDChartMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
				model.addAttribute("regionID",authenticationFacade.getRegionID());
				model.addAttribute("region_description", region.getDescription());
				model.addAttribute("type", "municipality");		
			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getSeaweedsWDChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/seaweedsWD");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/seaweedsWDChart/{province}", method = RequestMethod.GET)
	public ModelAndView seaweedsWDChartByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="seaweedsWDMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../seaweedsWDMunicipalityChart");
 	
			
		return modelAndView;
	}
	@RequestMapping(value = "marketChart", method = RequestMethod.GET)
	public ModelAndView marketChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
			
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
				model.addAttribute("region_description", region.getDescription());
				model.addAttribute("type", "province");
			
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getMarketChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getMarketChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "marketChart";


			modelAndView.setViewName("/charts/market");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "marketAllChart", method = RequestMethod.GET)
	public ModelAndView marketAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		List<Regions> regions = regionsService.findAllRegions();
	 	User user = new User();		 
		user = authenticationFacade.getUserInfo();
		model.addAttribute("user", user);
		
		model.addAttribute("region_description", "CENTRAL OFFICE");
		model.addAttribute("type", "region");		
 		model.addAttribute("regionID","123");			
	 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getMarketChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getMarketChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "marketChart";


			modelAndView.setViewName("/charts/market");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/marketMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView marketChartMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
	 			model.addAttribute("regionID",authenticationFacade.getRegionID());
	 			model.addAttribute("region_description", region.getDescription());
	 			model.addAttribute("type", "municipality");	 
	 			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getMarketChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/market");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/marketChart/{province}", method = RequestMethod.GET)
	public ModelAndView marketChartByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="marketMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../marketMunicipalityChart");
 	
			
		return modelAndView;
	}
	@RequestMapping(value = "fishportChart", method = RequestMethod.GET)
	public ModelAndView fishportChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
			
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
	 			model.addAttribute("region_description", region.getDescription());
	 			model.addAttribute("type", "province");	
	 			
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				//model.addAttribute("provinceChart",chartsUtil.getFishPortChartByProvince(provincesList));
				model.addAttribute("provinceChart",chartsUtil.getFishPortChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "fishportChart";


			modelAndView.setViewName("/charts/fishport");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "fishportAllChart", method = RequestMethod.GET)
	public ModelAndView fishportAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		List<Regions> regions = regionsService.findAllRegions();
	 	User user = new User();		 
		user = authenticationFacade.getUserInfo();
		model.addAttribute("user", user);
		
		model.addAttribute("region_description", "CENTRAL OFFICE");
		model.addAttribute("type", "region");		
 		model.addAttribute("regionID","123");			
	 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				//model.addAttribute("provinceChart",chartsUtil.getFishPortChartByProvince(provincesList));
				model.addAttribute("provinceChart",chartsUtil.getFishPortChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "fishportChart";


			modelAndView.setViewName("/charts/fishport");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/fishportMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView fishportChartMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
		 		model.addAttribute("type", "municipality");
			 
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getFishPortChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/fishport");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/fishportChart/{province}", method = RequestMethod.GET)
	public ModelAndView fishportChartByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="fishportMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../fishportMunicipalityChart");
 	
			
		return modelAndView;
	}
	@RequestMapping(value = "fishlandingChart", method = RequestMethod.GET)
	public ModelAndView fishlandingChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
			
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
	 			model.addAttribute("region_description", region.getDescription());
	 			model.addAttribute("type", "province");	
	 			
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "fishlandingChart";

				
			modelAndView.setViewName("/charts/fishlanding");
			show_hide_chart = true;
			
		return modelAndView;
	}
	@RequestMapping(value = "fishlandingAllChart", method = RequestMethod.GET)
	public ModelAndView fishlandingAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();

		List<Regions> regions = regionsService.findAllRegions();
		 

		User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
	 			model.addAttribute("region_description", "CENTRAL OFFICE");
	 			model.addAttribute("type", "region");	
	 			
		 		model.addAttribute("regionID","123");			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "fishlandingChart";

				
			modelAndView.setViewName("/charts/fishlanding");
			show_hide_chart = true;
			
		return modelAndView;
	}

	@RequestMapping(value = "/fishlandingMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView fishlandingMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);		
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
		 		model.addAttribute("type", "municipality");
		 		
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/fishlanding");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/fishlandingChart/{province}", method = RequestMethod.GET)
	public ModelAndView fishlandingByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="fishlandingMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../fishlandingMunicipalityChart");
 	
			
		return modelAndView;
	}
	@RequestMapping(value = "seagrassChart", method = RequestMethod.GET)
	public ModelAndView seagrassChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
			
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
	 			model.addAttribute("region_description", region.getDescription());
	 			model.addAttribute("type", "province");	
	 			
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getSeaGrassChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "fishlandingChart";

				
			modelAndView.setViewName("/charts/seagrass");
			show_hide_chart = true;
			
		return modelAndView;
	}
	@RequestMapping(value = "seagrassAllChart", method = RequestMethod.GET)
	public ModelAndView seagrassAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		List<Regions> regions = regionsService.findAllRegions();
	 	User user = new User();		 
		user = authenticationFacade.getUserInfo();
		model.addAttribute("user", user);
		
		model.addAttribute("region_description", "CENTRAL OFFICE");
		model.addAttribute("type", "region");		
 		model.addAttribute("regionID","123");			
	 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getSeaGrassChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "fishlandingChart";

				
			modelAndView.setViewName("/charts/seagrass");
			show_hide_chart = true;
			
		return modelAndView;
	}

	@RequestMapping(value = "/seagrassMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView seagrassMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);		
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
		 		model.addAttribute("type", "municipality");
		 		
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getSeaGrassChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/seagrass");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/seagrassChart/{province}", method = RequestMethod.GET)
	public ModelAndView seagrassByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="seagrassMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../seagrassMunicipalityChart");
 	
			
		return modelAndView;
	}
	@RequestMapping(value = "mangroveChart", method = RequestMethod.GET)
	public ModelAndView mangroveChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
			
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
	 			model.addAttribute("region_description", region.getDescription());
	 			model.addAttribute("type", "province");	
	 			
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getMangroveChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "mangroveChart";

				
			modelAndView.setViewName("/charts/mangrove");
			show_hide_chart = true;
			
		return modelAndView;
	}
	@RequestMapping(value = "mangroveAllChart", method = RequestMethod.GET)
	public ModelAndView mangroveAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		List<Regions> regions = regionsService.findAllRegions();
	 	User user = new User();		 
		user = authenticationFacade.getUserInfo();
		model.addAttribute("user", user);
		
		model.addAttribute("region_description", "CENTRAL OFFICE");
		model.addAttribute("type", "region");		
 		model.addAttribute("regionID","123");			
	 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getMangroveChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "mangroveChart";

				
			modelAndView.setViewName("/charts/mangrove");
			show_hide_chart = true;
			
		return modelAndView;
	}

	@RequestMapping(value = "/mangroveMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView mangroveMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);		
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
		 		model.addAttribute("type", "municipality");
		 		
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getSeaGrassChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/mangrove");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/mangroveChart/{province}", method = RequestMethod.GET)
	public ModelAndView mangroveByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="mangroveMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../mangroveMunicipalityChart");
 	
			
		return modelAndView;
	}
	@RequestMapping(value = "fishSanctuaryChart", method = RequestMethod.GET)
	public ModelAndView fishSanctuaryChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
			
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
	 			model.addAttribute("region_description", region.getDescription());
	 			model.addAttribute("type", "province");	
	 			
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getFishSanctuaryChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "fishSanctuaryChart";

				
			modelAndView.setViewName("/charts/fishsanctuary");
			show_hide_chart = true;
			
		return modelAndView;
	}
	@RequestMapping(value = "fishSanctuaryAllChart", method = RequestMethod.GET)
	public ModelAndView fishSanctuaryAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		List<Regions> regions = regionsService.findAllRegions();
	 	User user = new User();		 
		user = authenticationFacade.getUserInfo();
		model.addAttribute("user", user);
		
		model.addAttribute("region_description", "CENTRAL OFFICE");
		model.addAttribute("type", "region");		
 		model.addAttribute("regionID","123");			
	 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getFishSanctuaryChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "fishSanctuaryChart";

				
			modelAndView.setViewName("/charts/fishsanctuary");
			show_hide_chart = true;
			
		return modelAndView;
	}

	@RequestMapping(value = "/fishSanctuaryMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView fishSanctuaryMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);		
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
		 		model.addAttribute("type", "municipality");
		 		
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getFishSanctuaryChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/fishsanctuary");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/fishSanctuaryChart/{province}", method = RequestMethod.GET)
	public ModelAndView fishSanctuaryByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="fishSanctuaryMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../fishSanctuaryMunicipalityChart");
 	
			
		return modelAndView;
	}
	@RequestMapping(value = "payaoChart", method = RequestMethod.GET)
	public ModelAndView payaoChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
			
	 			model.addAttribute("region_description", region.getDescription());
	 			model.addAttribute("type", "province");	
	 			
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getPayaoChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "payaoChart";

				
			modelAndView.setViewName("/charts/payao");
			show_hide_chart = true;
			
		return modelAndView;
	}
	@RequestMapping(value = "payaoAllChart", method = RequestMethod.GET)
	public ModelAndView payaoAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		List<Regions> regions = regionsService.findAllRegions();
	 	User user = new User();		 
		user = authenticationFacade.getUserInfo();
		model.addAttribute("user", user);
		
		model.addAttribute("region_description", "CENTRAL OFFICE");
		model.addAttribute("type", "region");		
 		model.addAttribute("regionID","123");			
	 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getPayaoChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "payaoChart";

				
			modelAndView.setViewName("/charts/payao");
			show_hide_chart = true;
			
		return modelAndView;
	}

	@RequestMapping(value = "/payaoMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView payaoMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);		
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
		 		model.addAttribute("type", "municipality");
		 		
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getPayaoChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/payao");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/payaoChart/{province}", method = RequestMethod.GET)
	public ModelAndView payaoByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="payaoMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../payaoMunicipalityChart");
 	
			
		return modelAndView;
	}
	@RequestMapping(value = "lambakladChart", method = RequestMethod.GET)
	public ModelAndView lambakladChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
			
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
	 			model.addAttribute("region_description", region.getDescription());
	 			model.addAttribute("type", "province");	
	 			
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getLambakladChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "lambakladChart";

				
			modelAndView.setViewName("/charts/lambaklad");
			show_hide_chart = true;
			
		return modelAndView;
	}
	@RequestMapping(value = "lambakladAllChart", method = RequestMethod.GET)
	public ModelAndView lambakladAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		List<Regions> regions = regionsService.findAllRegions();
	 	User user = new User();		 
		user = authenticationFacade.getUserInfo();
		model.addAttribute("user", user);
		
		model.addAttribute("region_description", "CENTRAL OFFICE");
		model.addAttribute("type", "region");		
 		model.addAttribute("regionID","123");			
	 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getLambakladChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "lambakladChart";

				
			modelAndView.setViewName("/charts/lambaklad");
			show_hide_chart = true;
			
		return modelAndView;
	}

	@RequestMapping(value = "/lambakladMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView lambakladMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);		
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
		 		model.addAttribute("type", "municipality");
		 		
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getLambakladChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/lambaklad");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/lambakladChart/{province}", method = RequestMethod.GET)
	public ModelAndView lambakladByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="lambakladMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../lambakladMunicipalityChart");
 	
			
		return modelAndView;
	}
	@RequestMapping(value = "frpChart", method = RequestMethod.GET)
	public ModelAndView frpChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
			
	 			model.addAttribute("region_description", region.getDescription());
	 			model.addAttribute("type", "province");	
	 			
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getFRPChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "frpChart";

				
			modelAndView.setViewName("/charts/frp");
			show_hide_chart = true;
			
		return modelAndView;
	}
	@RequestMapping(value = "frpAllChart", method = RequestMethod.GET)
	public ModelAndView frpAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		List<Regions> regions = regionsService.findAllRegions();
	 	User user = new User();		 
		user = authenticationFacade.getUserInfo();
		model.addAttribute("user", user);
		
		model.addAttribute("region_description", "CENTRAL OFFICE");
		model.addAttribute("type", "region");		
 		model.addAttribute("regionID","123");			
	 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getFRPChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "frpChart";

				
			modelAndView.setViewName("/charts/frp");
			show_hide_chart = true;
			
		return modelAndView;
	}

	@RequestMapping(value = "/frpMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView frpMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);		
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
		 		model.addAttribute("type", "municipality");
		 		
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getFRPChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/frp");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/frpChart/{province}", method = RequestMethod.GET)
	public ModelAndView frpByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="frpMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../frpMunicipalityChart");
 	
			
		return modelAndView;
	}
	@RequestMapping(value = "coralreefChart", method = RequestMethod.GET)
	public ModelAndView coralreefChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		

		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
			
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
	 			model.addAttribute("region_description", region.getDescription());
	 			model.addAttribute("type", "province");	
	 			
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());			
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getFRPChartByProvince(provinces));
				model.addAttribute("provinces", provinces); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "coralreefChart";

				
			modelAndView.setViewName("/charts/coralreef");
			show_hide_chart = true;
			
		return modelAndView;
	}
	@RequestMapping(value = "coralreefAllChart", method = RequestMethod.GET)
	public ModelAndView coralreefAllChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		List<Regions> regions = regionsService.findAllRegions();
	 	User user = new User();		 
		user = authenticationFacade.getUserInfo();
		model.addAttribute("user", user);
		
		model.addAttribute("region_description", "CENTRAL OFFICE");
		model.addAttribute("type", "region");		
 		model.addAttribute("regionID","123");			
	 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByProvince(provincesList));
			 	model.addAttribute("provinceChart",chartsUtil.getFRPChartByRegion(regions));
				model.addAttribute("provinces", regions); 
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
				page = "coralreefChart";

				
			modelAndView.setViewName("/charts/coralreef");
			show_hide_chart = true;
			
		return modelAndView;
	}

	@RequestMapping(value = "/coralreefMunicipalityChart", method = RequestMethod.GET)
	public ModelAndView coralreefMunicipalityChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
	
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);		
		 Regions region = regionsService.findRegionDetails(authenticationFacade.getRegionID());
		 	User user = new User();		 
			user = authenticationFacade.getUserInfo();
			model.addAttribute("user", user);
		 		model.addAttribute("regionID",authenticationFacade.getRegionID());
		 		model.addAttribute("region_description", region.getDescription());
		 		model.addAttribute("type", "municipality");
		 		
			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
				model.addAttribute("province_title",provinceName);
				model.addAttribute("provinceChart",chartsUtil.getFRPChartByMunicipality(municipalitiesList));
				
				model.addAttribute("provinces", provinces);
				model.addAttribute("municipalities", municipalities);
				model.addAttribute("provinceID",provinceID);
				model.addAttribute("provinceName",provinceName);
				model.addAttribute("page",skip_page_show);
				model.addAttribute("show",show_hide_chart);
				
		 
			modelAndView.setViewName("/charts/coralreef");
			show_hide_chart = true;
		return modelAndView;
	}
	@RequestMapping(value = "/coralreefChart/{province}", method = RequestMethod.GET)
	public ModelAndView coralreefByProvinceChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			String s = province;
			String[] data = s.split(",", 2);
			provinceID = data[0];
			provinceName= data[1]; 
				
			skip_page_show = false;
			show_hide_chart = true;
				page ="coralreefMunicipalityChart";
		 	
				modelAndView.setViewName("redirect:../coralreefMunicipalityChart");
 	
			
		return modelAndView;
	}
//	@RequestMapping(value = "/fishlandingChartByMunicipal/{municipalities}", method = RequestMethod.GET)
//	public ModelAndView fishlandingByMunicipalitiesChart(ModelMap model,@PathVariable("municipalities") List<String> Listmunicipalities)throws JsonGenerationException, JsonMappingException, IOException  {
//		ModelAndView modelAndView = new ModelAndView();
//		
//		List<String> municipalitiesList = new ArrayList<>();
//		municipalitiesList = Listmunicipalities;
//		 List<Municipalities> municipalities = new ArrayList<>();
//		 Municipalities municipality = new Municipalities();
//		for(String municipals: municipalitiesList) {
//			
//			String[] municipal = municipals.split(",", 2);
//			String municipalID = municipal[0]; 
//			String municipalName =municipal[1] ;
//			
//			municipality.setMunicipal_id(municipalID);
//			municipality.setMunicipal(municipalName);
//			
//			municipalities.add(municipality);
//		}
//		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
//
//			// List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);
//			 
//			 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//				model.addAttribute("province_title",provinceName);
//				model.addAttribute("provinceChart",chartsUtil.getFishlandingChartByMunicipality(municipalities));
//				model.addAttribute("provinces", provinces);
//				//model.addAttribute("municipalities", municipalities);
//				model.addAttribute("provinceID",provinceID);
//				model.addAttribute("provinceName",provinceName);
//				model.addAttribute("page",true);
//		 
//		 	
//			modelAndView.setViewName("/charts/province/fishlanding");
//			
//		return modelAndView;
//	}
//	

	@RequestMapping(value = "/municipalList/{municipal_ids}", method = RequestMethod.GET)	
	public ModelAndView resourcesByMunicipalitiesChart(ModelMap model,@PathVariable("municipal_ids") String municipal_ids)throws JsonGenerationException, JsonMappingException, IOException  {
		
		ModelAndView modelAndView = new ModelAndView();
		
		List<String> myList = new ArrayList<String>(Arrays.asList(municipal_ids.split(",")));
		 List<Municipalities> municipalities = new ArrayList<>();
		 
		 
		for(String municipals: myList) {
			
			 Municipalities municipality = new Municipalities();
			 
			String[] municipal = municipals.split("-", 2);
			String municipalID = municipal[0]; 
			String municipalName =municipal[1] ;
			
			municipality.setMunicipal_id(municipalID);
			municipality.setMunicipal(municipalName);
			
			municipalities.add(municipality);
		}
		show_hide_chart = false;
		skip_page_show = false;
		municipalitiesList = municipalities;
		
		if(page =="aquaMunicipalityChart") {
	 		modelAndView.setViewName("redirect:../aquaMunicipalityChart");
	 	}
		if(page =="seaweedsNLMunicipalityChart") {
	 		modelAndView.setViewName("redirect:../seaweedsNLMunicipalityChart");
	 	}
		if(page =="maricultureMunicipalityChart") {
	 		modelAndView.setViewName("redirect:../maricultureMunicipalityChart");
	 	}
		if(page =="hatcheryMunicipalityChart") {
	 		modelAndView.setViewName("redirect:../hatcheryMunicipalityChart");
	 	}
	 	if(page =="fishlandingMunicipalityChart") {
	 		modelAndView.setViewName("redirect:../fishlandingMunicipalityChart");
	 	}
	 	if(page =="coldMunicipalityChart") {
	 		modelAndView.setViewName("redirect:../coldMunicipalityChart");
	 	}
	 	if(page =="fishprocessingMunicipalityChart") {
	 		modelAndView.setViewName("redirect:../fishprocessingMunicipalityChart");
	 	}
	 	if(page =="seaweedsWDMunicipalityChart") {
	 		modelAndView.setViewName("redirect:../seaweedsWDMunicipalityChart");
	 	}
	 	if(page =="marketMunicipalityChart") {
	 		modelAndView.setViewName("redirect:../marketMunicipalityChart");
	 	}
	 	if(page =="fishportMunicipalityChart") {
	 		modelAndView.setViewName("redirect:../fishportMunicipalityChart");
	 	}
		
		
	return modelAndView;
	}
	@RequestMapping(value = "/provinceList/{province_ids}", method = RequestMethod.GET)	
	public ModelAndView resourcesListByProvinceChart(ModelMap model,@PathVariable("province_ids") String province_ids)throws JsonGenerationException, JsonMappingException, IOException  {
		
		ModelAndView modelAndView = new ModelAndView();

		List<String> myList = new ArrayList<String>(Arrays.asList(province_ids.split(",")));
		 List<Provinces> Listprovinces = new ArrayList<>();
		 
		 
		for(String provinces: myList) {
			
			Provinces province = new Provinces();
			 
			String[] provinceString = provinces.split("-", 2);
			String provinceID = provinceString[0]; 
			String provinceName =provinceString[1] ;
			
			province.setProvince_id(provinceID);
			province.setProvince(provinceName);
			
			Listprovinces.add(province);
		}
		
		provincesList = Listprovinces;
		
		if(page == "aquaChart") {
			skip_page_show = false;
			show_hide_chart = false;
			modelAndView.setViewName("redirect:../aquaChart");
			
		}
		if(page == "seaweedsNLChart") {
			skip_page_show = false;
			show_hide_chart = false;
			modelAndView.setViewName("redirect:../seaweedsNLChart");
			
		}
		if(page == "maricultureChart") {
			skip_page_show = false;
			show_hide_chart = false;
			modelAndView.setViewName("redirect:../maricultureChart");
			
		}
		if(page == "hatcheryChart") {
			skip_page_show = false;
			show_hide_chart = false;
			modelAndView.setViewName("redirect:../hatcheryChart");
			
		}
		if(page == "fishlandingChart") {
			skip_page_show = false;
			show_hide_chart = false;
			modelAndView.setViewName("redirect:../fishlandingChart");
			
		}
		if(page == "coldChart") {
			skip_page_show = false;
			show_hide_chart = false;
			modelAndView.setViewName("redirect:../coldChart");
		}
		if(page == "fishprocessingChart") {
			skip_page_show = false;
			show_hide_chart = false;
			modelAndView.setViewName("redirect:../fishprocessingChart");
		}
		if(page == "seaweedsWDChart") {
			skip_page_show = false;
			show_hide_chart = false;
			modelAndView.setViewName("redirect:../seaweedsWDChart");
		}
		if(page == "marketChart") {
			skip_page_show = false;
			show_hide_chart = false;
			modelAndView.setViewName("redirect:../marketChart");
		}
		if(page == "fishportChart") {
			skip_page_show = false;
			show_hide_chart = false;
			modelAndView.setViewName("redirect:../fishportChart");
		}
			
	
	 	
		
		
	return modelAndView;
	}	
	@RequestMapping(value = "/regionList/{region_ids}", method = RequestMethod.GET)	
	public ModelAndView resourcesListByRegionChart(ModelMap model,@PathVariable("region_ids") String region_ids)throws JsonGenerationException, JsonMappingException, IOException  {
		
		System.out.println("region_ids: " +region_ids);
		ModelAndView modelAndView = new ModelAndView();

		List<String> myList = new ArrayList<String>(Arrays.asList(region_ids.split(",")));
		 List<Regions> Listregions = new ArrayList<>();
		 
		 
		for(String regions: myList) {
			
			Regions region = new Regions();
			 
			String[] regionString = regions.split("-", 2);
			String regionID = regionString[0]; 
			String regionName =regionString[1] ;
			
			region.setRegion_id(regionID);
			region.setRegion(regionName);
			
			Listregions.add(region);
		}
		
		regionsList = Listregions;
		
		if(page == "aquaChartByRegion") {
			skip_page_show = false;
			show_hide_chart = false;
			modelAndView.setViewName("redirect:../aquaChartByRegion");
			
		}
//		if(page == "seaweedsNLChart") {
//			skip_page_show = false;
//			show_hide_chart = false;
//			modelAndView.setViewName("redirect:../seaweedsNLChart");
//			
//		}
//		if(page == "maricultureChart") {
//			skip_page_show = false;
//			show_hide_chart = false;
//			modelAndView.setViewName("redirect:../maricultureChart");
//			
//		}
//		if(page == "hatcheryChart") {
//			skip_page_show = false;
//			show_hide_chart = false;
//			modelAndView.setViewName("redirect:../hatcheryChart");
//			
//		}
//		if(page == "fishlandingChart") {
//			skip_page_show = false;
//			show_hide_chart = false;
//			modelAndView.setViewName("redirect:../fishlandingChart");
//			
//		}
//		if(page == "coldChart") {
//			skip_page_show = false;
//			show_hide_chart = false;
//			modelAndView.setViewName("redirect:../coldChart");
//		}
//		if(page == "fishprocessingChart") {
//			skip_page_show = false;
//			show_hide_chart = false;
//			modelAndView.setViewName("redirect:../fishprocessingChart");
//		}
//		if(page == "seaweedsWDChart") {
//			skip_page_show = false;
//			show_hide_chart = false;
//			modelAndView.setViewName("redirect:../seaweedsWDChart");
//		}
//		if(page == "marketChart") {
//			skip_page_show = false;
//			show_hide_chart = false;
//			modelAndView.setViewName("redirect:../marketChart");
//		}
//		if(page == "fishportChart") {
//			skip_page_show = false;
//			show_hide_chart = false;
//			modelAndView.setViewName("redirect:../fishportChart");
//		}
			
	
	 	
		
		
	return modelAndView;
	}
	
	@RequestMapping(value = "bar/province", method = RequestMethod.GET)
	public ModelAndView muniChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		// List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);

		 
		 //	model.addAttribute("region_title",authenticationFacade.getRegionName());
		 model.addAttribute("provinces", provinces);
			model.addAttribute("provinceID",provinceID);
			model.addAttribute("provinceName",provinceName);
			model.addAttribute("page",false);
			modelAndView.setViewName("/users/BarProvince");
			
		return modelAndView;
	}
	
	@RequestMapping(value = "/bar/{province}", method = RequestMethod.GET)
	public ModelAndView regionChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		
		if(province != "all") {
		String s = province;
		provinces = province;
		String[] data = s.split(",", 2);
		provinceID = data[0];
		provinceName= data[1]; 
		}else {
			
		}

		//model.modelAndView.setViewName("redirect:../bar/province");
		modelAndView.setViewName("redirect:../bar/province");
		return modelAndView;
		
	}
	
	@RequestMapping(value = "sampleNav", method = RequestMethod.GET)
	public ModelAndView sampleNav(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
			modelAndView.setViewName("/charts/sampleSideNav");
			
		return modelAndView;
	}
	
	@RequestMapping(value = "/getLatitudeAsDegrees/{latitude}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getLatitudeAsDegrees(@PathVariable("latitude") String latitude)throws JsonGenerationException, JsonMappingException, IOException {

		//FeaturesData  featuresData = new FeaturesData();
		//return resourceMapsData.alltoJson(featuresData.getresourceMapsData.featuresListByRegion(authenticationFacade.getRegionID()));
		return resourceMapsData.alltoJson(resourceMapsData.featuresListByRegion(latitude));

	}

	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public ModelAndView reportall(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
 
		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		model.addAttribute("user", user);		
		modelAndView.setViewName("users/report");
		return modelAndView;
		
	}
	
//	@RequestMapping(value = "/getEngine", method = RequestMethod.GET, produces = "application/json")
//	public @ResponseBody String getEngine()throws JsonGenerationException, JsonMappingException, IOException {
//
//			List<Engine> engineList = engineService.findAllEngines();
//			ObjectMapper objectMapper = new ObjectMapper();
//		
//			return objectMapper.writeValueAsString(engineList);
//
//	}
	
}
