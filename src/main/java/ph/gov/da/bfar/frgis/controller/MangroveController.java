package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.MangrovePage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.Mangrove;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.MangroveService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class MangroveController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	MangroveService mangroveService;
	
	 @Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;
	
	@RequestMapping(value = "/mangrove", method = RequestMethod.GET)
	public ModelAndView mangrove(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();
		
		user = authenticationFacade.getUserInfo();
		
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "mangrove");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("users/form");
		return result;
	}
	@RequestMapping(value = "/mangroves", method = RequestMethod.GET)
	public ModelAndView mangroves(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "mangrove");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("users/form");
		return result;
	}

	@RequestMapping(value = "/mangroveMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			Mangrove data = mangroveService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	MapsData  mapsData = new MapsData();
		 	Features features = new Features();
		 	features = MapBuilder.getManggroveMapData(data);
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	mapsData.setIcon("/static/images/pin2022/Mangrove.png");
		 	
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			
			map_data= jsonString;
		result = new ModelAndView("redirect:../mangroves");
		
	
		return result;
	}
	@RequestMapping(value = "/mangroveDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView mangroveDeleteById(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		// mangroveService.deleteMangroveById(id);
		 mangroveService.updateByEnabled(id);
		result = new ModelAndView("redirect:../mangroves");
		
	
		return result;
	}
	@RequestMapping(value = "/mangroveDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView mangroveDeleteById(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;

		 mangroveService.updateByEnabled(id);
		 mangroveService.updateMangroveByReason(id, reason);
		 
		result = new ModelAndView("redirect:../../mangroves");
		
	
		return result;
	}
		
	@RequestMapping(value = "/mangroveMap", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "mangrove");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("users/map/mangrovemap");
		
	
		return result;
	}
	
	@RequestMapping(value = "/mangrove/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMangrove(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<MangrovePage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 5, Sort.by("id"));
	      
	          page = mangroveService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<MangrovePage> MangroveList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setMangrove(MangroveList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/mangroveList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getmangroveList() throws JsonGenerationException, JsonMappingException, IOException {

//		user = getUserInfo();
		List<Mangrove> list = null;

		list = mangroveService.ListByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	

	@RequestMapping(value = "/getMangrove/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMangroveById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<Mangrove> mapsDatas = mangroveService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}

	@RequestMapping(value = "/saveMangrove", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postMangrove(@RequestBody Mangrove mangrove) throws JsonGenerationException, JsonMappingException, IOException {

		savingResources(mangrove);

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
		

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);
	}



	private void savingResources(Mangrove mangrove) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		mangrove.setRegion_id(region_data[0]);
		mangrove.setRegion(region_data[1]);

		province = mangrove.getProvince();
		data = province.split(",", 2);
		mangrove.setProvince_id(data[0]);
		mangrove.setProvince(data[1]);

		municipal = mangrove.getMunicipality();
		municipal_data = municipal.split(",", 2);
		mangrove.setMunicipality_id(municipal_data[0]);
		mangrove.setMunicipality(municipal_data[1]);

		barangay = mangrove.getBarangay();
		barangay_data = barangay.split(",", 3);
		mangrove.setBarangay_id(barangay_data[0]);
		mangrove.setBarangay(barangay_data[1]);
		mangrove.setEnabled(true);

		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(mangrove.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		if (mangrove.getId() != 0) {
			Optional<Mangrove> grove = mangroveService.findById(mangrove.getId());
			String image_server = grove.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				mangrove.setImage("/images/" + image_server);
				newUniqueKeyforMap = mangrove.getUniqueKey();
				mangroveService.saveMangrove(ResourceData.getMangrove(mangrove, user, "", UPDATE));
				
			}else {
				newUniqueKeyforMap = mangrove.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", ""); 
				mangrove.setImage("/images/" + imageName);
				Base64Converter.convertToImage(mangrove.getImage_src(), mangrove.getImage());
				mangroveService.saveMangrove(ResourceData.getMangrove(mangrove, user, "", UPDATE));
			}
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", ""); 
			mangrove.setImage("/images/" + imageName);
			Base64Converter.convertToImage(mangrove.getImage_src(), mangrove.getImage());
			mangroveService.saveMangrove(ResourceData.getMangrove(mangrove, user, newUniqueKey, SAVE));
			save = true;

		}

	}
	@RequestMapping(value = "/getLastPageMangrove", method = RequestMethod.GET)
	public @ResponseBody String getLastPageMangrove(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = mangroveService.MangroveCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}


}
