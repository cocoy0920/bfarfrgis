package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import ph.gov.da.bfar.frgis.model.Barangay;
import ph.gov.da.bfar.frgis.repository.BarangayRepo;




@Service("BarangayService")
@Transactional
public class BarangayServiceImpl implements BarangayService{

	@Autowired
	private BarangayRepo  barangayRepo;

	@Override
	public Optional<Barangay> findById(int id) {
	
		return barangayRepo.findById(id);
	}

	@Override
	public List<Barangay> findByMunicipality(String municipal) {
		
		return barangayRepo.findByMunicipality(municipal);
	}

	@Override
	public void saveBarangay(Barangay barangay) {
		barangayRepo.save(barangay);
		
	}


	@Override
	public List<Barangay> findAllBarangays() {
		
		return barangayRepo.findAll();
	}

	
	
	}
