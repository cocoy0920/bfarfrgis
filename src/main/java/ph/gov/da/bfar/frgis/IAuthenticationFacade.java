package ph.gov.da.bfar.frgis;

import org.springframework.security.core.Authentication;

import ph.gov.da.bfar.frgis.model.User;

public interface IAuthenticationFacade {
	  Authentication getAuthentication();
	  
	  public String getUsername();
	  public User getUserInfo();
	  public String getRegionID();
	  public String getRegionName();
}
