package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.FishPenPage;
import ph.gov.da.bfar.frgis.model.FishPen;

@Repository
public interface FishPenRepo extends JpaRepository<FishPen, Integer> {

	@Query("SELECT c from FishPen c where c.user = :username")
	public List<FishPen> getListFishPenByUsername(@Param("username") String username);
	
	@Query("SELECT c from FishPen c where c.region_id = :region_id")
	public List<FishPen> getListFishPenByRegion(@Param("region_id") String region_id);
	
	@Modifying(clearAutomatically = true)
	@Query("update FishPen f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update FishPen f set f.reason =:reason  where f.id =:id")
	public void updateFishPenByReason( @Param("id") int id, @Param("reason") String reason);
	
	
	@Query("SELECT COUNT(f) FROM FishPen f WHERE f.user=:username")
	public int  FishPenCount(@Param("username") String username);
	
	@Query("SELECT c from FishPen c where c.uniqueKey = :uniqueKey")
	public FishPen findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from FishPen c where c.uniqueKey = :uniqueKey")
	List<FishPen> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from FishPen c where c.user = :username")
	 public List<FishPen> getPageableFishPens(@Param("username") String username,Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishPenPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfOperator,c.lat,c.lon,c.enabled) from FishPen c where c.user = :user")
	public Page<FishPenPage> findByUsername(@Param("user")String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishPenPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfOperator,c.lat,c.lon,c.enabled) from FishPen c where c.region_id = :region_id")
	public Page<FishPenPage> findByRegion(@Param("region_id")String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishPenPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfOperator,c.lat,c.lon,c.enabled) from FishPen c")
	public Page<FishPenPage> findAllFishPen(Pageable pageable);

	
	@Query("SELECT COUNT(f) FROM FishPen f")
	public Long countAllFishPen();
	
	@Query("SELECT COUNT(f) FROM FishPen f WHERE f.region_id=:region_id")
	public Long countFishPenPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM FishPen f WHERE f.province_id=:province_id")
	public Long countFishPenPerProvince(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM FishPen f WHERE f.municipality_id=:municipality_id")
	public Long countFishPenPerMunicipality(@Param("municipality_id") String municipality_id);
}
