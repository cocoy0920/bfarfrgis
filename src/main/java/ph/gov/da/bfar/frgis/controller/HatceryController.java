package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.validation.Valid;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.HatcheryPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.Fishport;
import ph.gov.da.bfar.frgis.model.Hatchery;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.service.HatcheryService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.PageUtils;
import ph.gov.da.bfar.frgis.web.util.ResourceData;
import ph.gov.da.bfar.frgis.web.util.ResourceMapsData;

@Controller
@RequestMapping("/create")
public class HatceryController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	HatcheryService hatcheryService;
	
	 @Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	MapsService mapsService;

	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;
	
	@RequestMapping(value = "/hatchery", method = RequestMethod.GET)
	public ModelAndView newhatchery(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();		 
		user = authenticationFacade.getUserInfo();
//		Set<UserProfile> setOfRole = new HashSet<>();
//		setOfRole = user.getUserProfiles();
//		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
//		UserProfile role = list.get(0);
		
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "hatchery");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		
		
		result = new ModelAndView("users/form");
		return result;
	}
	
	@RequestMapping(value = "/hatcheries", method = RequestMethod.GET)
	public ModelAndView hatcheries(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();
		user   = new User();		 
		user = authenticationFacade.getUserInfo();
		
		model.addAttribute("user", user);
		model.addAttribute("page", "hatchery");
		model.addAttribute("mapsData", map_data);
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		
		
		result = new ModelAndView("users/form");
		return result;
	}
	
	@RequestMapping(value = "/hatcheryMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();
		 
		 ObjectMapper mapper = new ObjectMapper();
			Hatchery data = hatcheryService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	
		 	Features features = new Features();
			 features = MapBuilder.getHatcheryMapData(data);
			 
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	if(data.getOperatorClassification().equals("LGU")) {
		 		mapsData.setIcon("/static/images/pin2022/Hatchery_LGU.png");
		 	}else if(data.getOperatorClassification().equals("Private")){
		 		mapsData.setIcon("/static/images/pin2022/Hatchery_PrivateSector.png");
		 	}else if(data.getOperatorClassification().equals("BFAR")){
		 		if(data.getLegislated().equals("legislated")) {
			 		mapsData.setIcon("/static/images/pin2022/Hatchery.png");
			 	}else if(data.getLegislated().equals("nonLegislated")) {
			 		mapsData.setIcon("/static/images/pin/20x34/hatcheries.png");
			 	}
		 	}
		 	
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			map_data = jsonString;
		
		result = new ModelAndView("redirect:../hatcheries");
		
	
		return result;
	}
	@RequestMapping(value = "/hatcheryDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView hatcheryDeleteById(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();
		// hatcheryService.deleteHatcheryById(id);
		 hatcheryService.updateByEnabled(id);
		result = new ModelAndView("redirect:../hatcheries");
		
	
		return result;
	}
	@RequestMapping(value = "/hatcheryDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView hatcheryDeleteById(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();

		 hatcheryService.updateByEnabled(id);
		 hatcheryService.updateHatcheryByReason(id, reason);
		 
		result = new ModelAndView("redirect:../../hatcheries");
		
	
		return result;
	}

	@RequestMapping(value = "/hatcheryLegislated/{legislated}", method = RequestMethod.GET)
	public ModelAndView hatcheryLegislated(@PathVariable("legislated") String legislated,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findallMapsOfHatcheryByLegislated(authenticationFacade.getUsername(),"hatchery", legislated);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			String lString = mapper.writeValueAsString(legislated);
			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
			model.addAttribute("hl", lString);
		
		result = new ModelAndView("users/map/hatcherymap");
		
	
		return result;
	}
	
	@RequestMapping(value = "/hatcheryMap", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "hatchery");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("users/map/hatcherymap");
		
	
		return result;
	}	
    @RequestMapping(value = "/Posthatchery", method = RequestMethod.POST)
    public String saveHatchery(@Valid @ModelAttribute("HatcheryForm") Hatchery HatcheryForm,
            BindingResult result, ModelMap model) {
    
    	boolean error = false;
    	Hatchery hatchery = new Hatchery();
    	hatchery = HatcheryForm;
        
        	 if("select".equals(HatcheryForm.getProvince())){
        	        System.out.println(HatcheryForm.getProvince());
        	        model.addAttribute("province","Please select Province");
        	        error = true;
        	    }
        	     
        	    if("select".equals(HatcheryForm.getMunicipality())){
        
        	    	model.addAttribute("municipality","Please select Municipality");  
        	        error = true;
        	    }
        	     
        	    if("select".equals(HatcheryForm.getBarangay())){
        	    	
        	    	model.addAttribute("barangay","Please select Barangay"); 
        	        error = true;
        	    }
        	    if(PageUtils.isNullEmpty(HatcheryForm.getNameOfHatchery())){
          	    	
          	        error = true;
           	    }
        	    if(PageUtils.isNullEmpty(HatcheryForm.getNameOfOperator())){
          	    	
          	        error = true;
           	    }
        	    if(PageUtils.isNullEmpty(HatcheryForm.getAddressOfOperator())){
          	    	
          	        error = true;
           	    }
        	    if(PageUtils.isNullEmpty(HatcheryForm.getOperatorClassification())){
          	    	
          	        error = true;
           	    }
        	    if(PageUtils.isNullEmpty(HatcheryForm.getArea())){
          	    	
          	        error = true;
           	    }
        	    if(PageUtils.isNullEmpty(HatcheryForm.getIndicateSpecies())){
          	    	
          	        error = true;
           	    }
        	    if(PageUtils.isNullEmpty(HatcheryForm.getPublicprivate())){
          	    	
          	        error = true;
           	    }
        	    if(PageUtils.isNullEmpty(HatcheryForm.getProdStocking())){
          	    	
          	        error = true;
           	    }
        	    if(PageUtils.isNullEmpty(HatcheryForm.getProdCycle())){
          	    	
          	        error = true;
           	    }
        	    if(PageUtils.isNullEmpty(HatcheryForm.getActualProdForTheYear())){
          	    	
          	        error = true;
           	    }
        	    if(PageUtils.isNullEmpty(HatcheryForm.getMonthStart())){
          	    	
          	        error = true;
           	    }
        	    if(PageUtils.isNullEmpty(HatcheryForm.getMonthEnd())){
          	    	
          	        error = true;
           	    }
        	    if(PageUtils.isNullEmpty(HatcheryForm.getDateAsOf())){
          	    	
          	        error = true;
           	    }
           	    if(PageUtils.isNullEmpty(HatcheryForm.getDataSource())){
           	    	
    	        	        error = true;
    	        	    } 
           	    if(PageUtils.isNullEmpty(HatcheryForm.getRemarks())){
           	    	
   	 	        	        error = true;
   	 	        	    }
           	    if(PageUtils.isNullEmpty(HatcheryForm.getLat())){
           	    	
   	 	        	        error = true;
   	 	        	    }
           	    if(PageUtils.isNullEmpty(HatcheryForm.getLon())){
           	    
   	 	        	        error = true;
   	 	        	    }
           	    if(PageUtils.isNullEmpty(HatcheryForm.getImage_src())){
           	    	
   	 	        	        error = true;
   	 	        	    }
           	     
        	     
        	    if(error) {
        	    	model.addAttribute("region", HatcheryForm.getRegion());
        			model.addAttribute("username",HatcheryForm.getUser());
        			model.addAttribute("page",HatcheryForm.getPage());
        	        
        	        
        	        return "users/hatchery";
        	    }
      if(!error) {
        	System.out.println("SAVING HATCHERY");
        	savingResources(hatchery);
        	model.addAttribute("SAVE", "data save HATCHERY");
        	hatchery = new Hatchery();
        	model.put("HatcheryForm", hatchery);
        	return "redirect:new_hatchery";
        
      }
        return "redirect:new_hatchery";
    }

	@RequestMapping(value = "/hatchery/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getHatchery(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<HatcheryPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 15, Sort.by("id"));
	      
	          page = hatcheryService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<HatcheryPage> HatcheryList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setHatcheries(HatcheryList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}

	@RequestMapping(value = "/hatcheryList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getfhatcheryList() throws JsonGenerationException, JsonMappingException, IOException {

//		user = getUserInfo();
		List<Hatchery> list = null;

		list = hatcheryService.ListByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	@RequestMapping(value = "/getHatchery/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getHatcheryById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<Hatchery> mapsDatas = hatcheryService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}
	@RequestMapping(value = "/saveHatchery", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postHatchery(@RequestBody Hatchery hatchery) throws JsonGenerationException, JsonMappingException, IOException {

		savingResources(hatchery);

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
		

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);

	}

	
	private void savingResources(Hatchery hatchery) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		
		UUID newID = UUID.randomUUID();
		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		hatchery.setRegion_id(region_data[0]);
		hatchery.setRegion(region_data[1]);

		province = hatchery.getProvince();
		data = province.split(",", 2);
		hatchery.setProvince_id(data[0]);
		hatchery.setProvince(data[1]);

		municipal = hatchery.getMunicipality();
		municipal_data = municipal.split(",", 2);
		hatchery.setMunicipality_id(municipal_data[0]);
		hatchery.setMunicipality(municipal_data[1]);
		barangay = hatchery.getBarangay();
		barangay_data = barangay.split(",", 3);
		hatchery.setBarangay_id(barangay_data[0]);
		hatchery.setBarangay(barangay_data[1]);
		hatchery.setEnabled(true);
	/*	if(!hatchery.getBarangay().equals("") ||!hatchery.getBarangay().equals(null)) {
		barangay = hatchery.getBarangay();
		barangay_data = barangay.split(",", 3);
		hatchery.setBarangay_id(barangay_data[0]);
		hatchery.setBarangay(barangay_data[1]);
		}
		LocalDate today = LocalDate.now();
        Calendar now = Calendar.getInstance();
 
        int hrs = now.get(Calendar.HOUR_OF_DAY);
       	int minute =  now.get(Calendar.MINUTE);
		int currentDate = today.getDayOfMonth();
		int currentMonth = today.getMonthValue();
		int currentYear = today.getYear();


		String hcode = "FH" + currentYear + currentDate + currentMonth + hrs + minute;
	
		hatchery.setCode(hcode);*/
		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(hatchery.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		if (hatchery.getId() != 0) {
			Optional<Hatchery> hatch = hatcheryService.findById(hatchery.getId());
			String image_server = hatch.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				hatchery.setImage("/images/" + image_server);
				newUniqueKeyforMap = hatchery.getUniqueKey();
				hatcheryService.saveHatchery(ResourceData.getHatchery(hatchery, user, "", UPDATE));
			}else {
				newUniqueKeyforMap = hatchery.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", "");
				hatchery.setImage("/images/" + imageName);
				Base64Converter.convertToImage(hatchery.getImage_src(), hatchery.getImage());
				hatcheryService.saveHatchery(ResourceData.getHatchery(hatchery, user, "", UPDATE));
			}
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", "");
			hatchery.setImage("/images/" + imageName);
			Base64Converter.convertToImage(hatchery.getImage_src(), hatchery.getImage());
			hatcheryService.saveHatchery(ResourceData.getHatchery(hatchery, user, newUniqueKey, SAVE));
			//mapsService.saveMapsData(ResourceMapsData.getMapData(hatchery, user, newUniqueKey));

			save = true;

		}

	}
	@RequestMapping(value = "/getLastPageHatchery", method = RequestMethod.GET)
	public @ResponseBody String getLastPageHatchery(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = hatcheryService.HatcheryCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}

}
