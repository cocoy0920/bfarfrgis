package ph.gov.da.bfar.frgis;

import java.io.File;
import java.io.IOException;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.common.RationalNumber;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.GpsTagConstants;
import org.apache.commons.imaging.formats.tiff.taginfos.TagInfo;

public class ReadMetaData {

	/**
	 * Reference : https://github.com/apache/commons-imaging/blob/master/src/test/java/org/apache/commons/imaging/examples/MetadataExample.java
	 */
	public static void readImageMeta(final File imgFile) throws ImageReadException, IOException {
	    /** get all metadata stored in EXIF format (ie. from JPEG or TIFF). **/
	    final ImageMetadata metadata = Imaging.getMetadata(imgFile);
	    System.out.println(metadata);
	    System.out.println("--------------------------------------------------------------------------------");
	    
	    /** Get specific meta data information by drilling down the meta **/
	    if (metadata instanceof JpegImageMetadata) {
	        JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
	        printTagValue(jpegMetadata, GpsTagConstants.GPS_TAG_GPS_LATITUDE_REF);
	        printTagValue(jpegMetadata, GpsTagConstants.GPS_TAG_GPS_LATITUDE);
	        printTagValue(jpegMetadata, GpsTagConstants.GPS_TAG_GPS_LONGITUDE_REF);
	        printTagValue(jpegMetadata, GpsTagConstants.GPS_TAG_GPS_LONGITUDE);
	        
	        // simple interface to GPS data
	        final TiffImageMetadata exifMetadata = jpegMetadata.getExif();
	        if (null != exifMetadata) {
	            final TiffImageMetadata.GPSInfo gpsInfo = exifMetadata.getGPS();
	            if (null != gpsInfo) {
	                final String gpsDescription = gpsInfo.toString();
	                final double longitude = gpsInfo.getLongitudeAsDegreesEast();
	                final double latitude = gpsInfo.getLatitudeAsDegreesNorth();

	                System.out.println("    " + "GPS Description: " + gpsDescription);
	                System.out.println("    " + "GPS Longitude (Degrees East): " + longitude);
	                System.out.println("    " + "GPS Latitude (Degrees North): " + latitude);
	            }
	        }

	        // more specific example of how to manually access GPS values
	        final TiffField gpsLatitudeRefField = jpegMetadata.findEXIFValueWithExactMatch(GpsTagConstants.GPS_TAG_GPS_LATITUDE_REF);
	        final TiffField gpsLatitudeField = jpegMetadata.findEXIFValueWithExactMatch(GpsTagConstants.GPS_TAG_GPS_LATITUDE);
	        final TiffField gpsLongitudeRefField = jpegMetadata.findEXIFValueWithExactMatch(GpsTagConstants.GPS_TAG_GPS_LONGITUDE_REF);
	        final TiffField gpsLongitudeField = jpegMetadata.findEXIFValueWithExactMatch(GpsTagConstants.GPS_TAG_GPS_LONGITUDE);
	        if (gpsLatitudeRefField != null && gpsLatitudeField != null && gpsLongitudeRefField != null && gpsLongitudeField != null) {
	            // all of these values are strings.
	            final String gpsLatitudeRef = (String) gpsLatitudeRefField.getValue();
	            final RationalNumber[] gpsLatitude = (RationalNumber[]) (gpsLatitudeField.getValue());
	            final String gpsLongitudeRef = (String) gpsLongitudeRefField.getValue();
	            final RationalNumber[] gpsLongitude = (RationalNumber[]) gpsLongitudeField.getValue();

	            final RationalNumber gpsLatitudeDegrees = gpsLatitude[0];
	            final RationalNumber gpsLatitudeMinutes = gpsLatitude[1];
	            final RationalNumber gpsLatitudeSeconds = gpsLatitude[2];

	            final RationalNumber gpsLongitudeDegrees = gpsLongitude[0];
	            final RationalNumber gpsLongitudeMinutes = gpsLongitude[1];
	            final RationalNumber gpsLongitudeSeconds = gpsLongitude[2];

	            // This will format the gps info like so:
	            //
	            // gpsLatitude: 8 degrees, 40 minutes, 42.2 seconds S
	            // gpsLongitude: 115 degrees, 26 minutes, 21.8 seconds E

	            System.out.println("    " + "GPS Latitude: " + gpsLatitudeDegrees.toDisplayString() + " degrees, " + gpsLatitudeMinutes.toDisplayString() + " minutes, " + gpsLatitudeSeconds.toDisplayString() + " seconds " + gpsLatitudeRef);
	            System.out.println("    " + "GPS Longitude: " + gpsLongitudeDegrees.toDisplayString() + " degrees, " + gpsLongitudeMinutes.toDisplayString() + " minutes, " + gpsLongitudeSeconds.toDisplayString() + " seconds " + gpsLongitudeRef);
	        }
	    }
	}

	private static void printTagValue(final JpegImageMetadata jpegMetadata, TagInfo tagInfo) {
	    final TiffField field = jpegMetadata.findEXIFValueWithExactMatch(tagInfo);
	    if (field == null) {
	        System.out.println(tagInfo.name + ": " + "Not Found.");
	    } else {
	        System.out.println(tagInfo.name + ": " + field.getValueDescription());
	    }
	}


	public static void main(String[] args) throws IOException, ImageReadException {
	 //  File sourceFile = new File("/Users/kiko/Downloads/IMG_20230111_145353.jpg");
	   // File sourceFile = new File("/Users/kiko/Desktop/TOMCAT/images/IMG_20230111_145353.jpeg");
	  //  readImageMeta(sourceFile);
	    String str = "Patungan, Maragondon, Cavite";
	    str = str.replaceAll("[^a-zA-Z0-9]", "");  
	    
	    System.out.println(str);
	}

}
