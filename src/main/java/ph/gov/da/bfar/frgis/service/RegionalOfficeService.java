package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.RegionalOfficePage;
import ph.gov.da.bfar.frgis.model.RegionalOffice;
import ph.gov.da.bfar.frgis.model.RegionalOffice;




public interface RegionalOfficeService {
	
	Optional<RegionalOffice> findById(int id);
	
	List<RegionalOffice> ListByUsername(String region);
	List<RegionalOffice> ListByRegion(String region_id);
	RegionalOffice findByUniqueKey(String uniqueKey);
	List<RegionalOffice> findByUniqueKeyForUpdate(String uniqueKey);
	void saveRegionalOffice(RegionalOffice RegionalOffice);
	void deleteRegionalOfficeById(int id);
	void updateByEnabled(int id);
	void updateRegionalOfficeByReason(int  id,String  reason);
	List<RegionalOffice> findAllRegionalOffice(); 
    public List<RegionalOffice> getAllRegionalOffice(int index,int pagesize,String user);
    public int RegionalOfficeCount(String username);
    public Page<RegionalOfficePage> findByUsername(@Param("user") String user, Pageable pageable);
    public Page<RegionalOfficePage> findByRegion(@Param("region") String region, Pageable pageable);
    public Page<RegionalOfficePage> findAllRegionalOffice(Pageable pageable);
    public Long countAllRegionalOffice();
    public Long countRegionalOfficePerRegion(String region_id);
    public Long countRegionalOfficeProvince(String province_id);

}