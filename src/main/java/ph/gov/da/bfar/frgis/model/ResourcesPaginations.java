package ph.gov.da.bfar.frgis.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ph.gov.da.bfar.frgis.Page.ColdStoragePage;
import ph.gov.da.bfar.frgis.Page.FishCagePage;
import ph.gov.da.bfar.frgis.Page.FishCoralPage;
import ph.gov.da.bfar.frgis.Page.FishLandingPage;
import ph.gov.da.bfar.frgis.Page.FishPenPage;
import ph.gov.da.bfar.frgis.Page.FishPondPage;
import ph.gov.da.bfar.frgis.Page.FishPortPage;
import ph.gov.da.bfar.frgis.Page.FishProcessingPlantPage;
import ph.gov.da.bfar.frgis.Page.FishSanctuaryPage;
import ph.gov.da.bfar.frgis.Page.HatcheryPage;
import ph.gov.da.bfar.frgis.Page.LambakladPage;
import ph.gov.da.bfar.frgis.Page.MangrovePage;
import ph.gov.da.bfar.frgis.Page.MariculturePage;
import ph.gov.da.bfar.frgis.Page.MarketPage;
import ph.gov.da.bfar.frgis.Page.NationalCenterPage;
import ph.gov.da.bfar.frgis.Page.PFOPage;
import ph.gov.da.bfar.frgis.Page.PayaoPage;
import ph.gov.da.bfar.frgis.Page.RegionalOfficePage;
import ph.gov.da.bfar.frgis.Page.SchoolPage;
import ph.gov.da.bfar.frgis.Page.SeaGrassPage;
import ph.gov.da.bfar.frgis.Page.SeaWeedsPage;
import ph.gov.da.bfar.frgis.Page.TOSPage;
import ph.gov.da.bfar.frgis.Page.TrainingPage;

@ToString
@Getter
@Setter

public class ResourcesPaginations {
	
	private String pageNumber;
	private String lastpageNumber;
	private String uniqueKey;
	private List<FishSanctuaryPage> fishSanctuaries;
	private List<FishProcessingPlantPage> fishProcessingPlantPage;
	private List<FishLandingPage> fishLanding;
	private List<FishPenPage> fishPen;
	private List<FishCagePage> fishCagePage;
	private List<FishPondPage> fishPond;
	private List<HatcheryPage> hatcheries;
	private List<ColdStoragePage> icePlantColdStorage;
	private List<FishPortPage> fishPort;
	private List<FishCoralPage> fishCoral;
	private List<MarketPage> market;
	private List<SchoolPage> schoolOfFisheries;
	private List<SeaGrassPage> seaGrass;
	private List<SeaWeedsPage> seaweeds;
	private List<MangrovePage> mangrove;
	private List<PFOPage> lgu;
	private List<TrainingPage> trainingCenter;
	private List<MariculturePage> maricultureZone;
	private List<NationalCenterPage> center;
	private List<RegionalOfficePage> regionalOffice;
	private List<TOSPage> TOS; 
	private List<PayaoPage> payao;
	private List<LambakladPage> lambaklad;
	

}
