package ph.gov.da.bfar.frgis.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.model.Regions;

@Repository
public interface RegionRepo extends JpaRepository<Regions, Integer> {

	
	@Query("SELECT r from Regions r where r.region_id = :region_id")
	public Regions getRegionDetails(@Param("region_id") String region_id);
}
