package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.model.Barangay;

@Repository
public interface BarangayRepo extends JpaRepository<Barangay, Integer> {

	@Query("SELECT b from Barangay b where b.municipal_id = :municipal")
	List<Barangay> findByMunicipality(String municipal);
}
