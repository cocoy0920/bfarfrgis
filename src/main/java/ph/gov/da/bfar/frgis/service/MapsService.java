package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import ph.gov.da.bfar.frgis.model.MapsData;



public interface MapsService {
	
	Optional<MapsData> findById(int id);
	
	List<MapsData> findByUniqueKey(String uniqueKey);
		
	void saveMapsData(MapsData mapsData);
	
	List<MapsData> findAllMapsData(); 
	
	List<MapsData> findAllMapsByUser(String user,String page);
	List<MapsData> findAllMapsByUser(String user);
	List<MapsData> findAllMapsByResources(String page);
	List<MapsData> findAllMapsByRegion(String region_id);
	List<MapsData> findallMapsOfHatcheryByLegislated(String user,String page, String legislated);
	List<MapsData> findallMapsOfHatcheryByLegislated(String page, String legislated);
	
	List<MapsData> findAllMapsByRegionAndResources(String region,String page);
	/*List<MapsData> findAllMapsByRegionAndProvince(String region,String province);
	List<MapsData> findAllMapsByRegionAndProvinceAndResources(String region,String province,String resources);
	List<MapsData> findAllMapsByRegionAndProvinceAndMunicipality(String region,String province,String municipality);
	List<MapsData> findAllMapsByRegionAndProvinceAndMunicipalityAndResources(String region,String province,String municipality,String resources);
*/
}