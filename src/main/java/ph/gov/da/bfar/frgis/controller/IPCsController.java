package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.ColdStoragePage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.IcePlantColdStorageService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class IPCsController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	IcePlantColdStorageService icePlantColdStorageService;
	
	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;	
	
	@RequestMapping(value = "/iceplantorcoldstorage", method = RequestMethod.GET)
	public ModelAndView iceplantorcoldstorage(ModelMap model)
			throws Exception {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();
		
		user = authenticationFacade.getUserInfo();

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "ipcs");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}

	@RequestMapping(value = "/iceplantorcoldstorages", method = RequestMethod.GET)
	public ModelAndView iceplantorcoldstorages(ModelMap model)
			throws Exception {

		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "ipcs");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}

	@RequestMapping(value = "/ipcsMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			IcePlantColdStorage data = icePlantColdStorageService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	Features features = new Features();
		 	features = MapBuilder.getIceColdMapData(data);
		 	
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	String Operator = data.getOperator().replace(" ", "");
		 	if(Operator.equals("BFAR")) {
		 		mapsData.setIcon("/static/images/pin2022/ColdStorage_BFAR.png");
		 	}else if(Operator.equals("COOPERATIVE")) {
		 		mapsData.setIcon("/static/images/pin2022/ColdStorage_Cooperative.png");
		 	}else if(Operator.equals("PRIVATE")) {
		 		mapsData.setIcon("/static/images/pin2022/ColdStorage_PrivateSector.png");
		 	}
		
		 	mapsData.setHtml(features.getInformation());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			
			
			
			map_data= jsonString;
		
		result = new ModelAndView("redirect:../iceplantorcoldstorages");
		
	
		return result;
	}

	@RequestMapping(value = "/ipcsMapDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView ipcsMapDeleteById(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		// icePlantColdStorageService.deleteIcePlantColdStorageById(id);
		 icePlantColdStorageService.updateeByEnabled(id);
		result = new ModelAndView("redirect:../iceplantorcoldstorages");
		
	
		return result;
	}
	@RequestMapping(value = "/ipcsMapDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView ipcsMapDeleteById(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;

		 icePlantColdStorageService.updateeByEnabled(id);
		 icePlantColdStorageService.updateIcePlantColdStorageeByReason(id, reason);
		 
		result = new ModelAndView("redirect:../../iceplantorcoldstorages");
		
	
		return result;
	}

	@RequestMapping(value = "/ipcsMap", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "iceplantorcoldstorage");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("users/map/ipcsmap");
		
	
		return result;
	}
	
	@RequestMapping(value = "/iceplantorcoldstorage/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getIceplantorcoldstorage(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<ColdStoragePage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 5, Sort.by("id"));
	      
	          page = icePlantColdStorageService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<ColdStoragePage> IcePlantColdStorageList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setIcePlantColdStorage(IcePlantColdStorageList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/icecoldList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String geticecoldListList() throws JsonGenerationException, JsonMappingException, IOException {

	//	user = getUserInfo();
		List<IcePlantColdStorage> list = null;

		list = icePlantColdStorageService.ListByUsername(authenticationFacade.getUsername());

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	@RequestMapping(value = "/getIce/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getIceById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<IcePlantColdStorage> mapsDatas = icePlantColdStorageService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}

	@RequestMapping(value = "/saveIcePlant", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postIce(@RequestBody IcePlantColdStorage icePlantColdStorage) throws JsonGenerationException, JsonMappingException, IOException {

		savingResources(icePlantColdStorage);

		List<IcePlantColdStorage> list = null;

		int pageIndex = 1;
		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;
		int page = (pageIndex * numberOfRecordsPerPage) - numberOfRecordsPerPage;

		totalNumberOfRecords = icePlantColdStorageService.IcePlantColdStorageCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		StringBuilder builder = new StringBuilder();
		
		builder.append("<ul class=\"pagination pagination-sm\">");

		for (int i = 1; i <= noOfPages; i++) {
			builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + i + "</a></li>");
		}
		builder.append("</ul>");

		list = icePlantColdStorageService.getAllIcePlantColdStorages(page, numberOfRecordsPerPage, authenticationFacade.getUsername());
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
		resourcesPaginations.setPageNumber(pageNumber);

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);
	}

//	@RequestMapping(value = "/getIcePlant", method = RequestMethod.GET, produces = "application/json")
//	public @ResponseBody String getIce(@RequestBody IcePlantColdStorage icePlantColdStorage) throws JsonGenerationException, JsonMappingException, IOException {
//
//
//		List<IcePlantColdStorage> list = null;
//
//		int pageIndex = 1;
//		int totalNumberOfRecords = 0;
//		int numberOfRecordsPerPage = 20;
//		int page = (pageIndex * numberOfRecordsPerPage) - numberOfRecordsPerPage;
//
//		totalNumberOfRecords = icePlantColdStorageService.IcePlantColdStorageCount(authenticationFacade.getUsername());
//
//		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
//		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
//			noOfPages = noOfPages + 1;
//		}
//		StringBuilder builder = new StringBuilder();
//		
//		builder.append("<ul class=\"pagination pagination-sm\">");
//
//		for (int i = 1; i <= noOfPages; i++) {
//			builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + i + "</a></li>");
//		}
//		builder.append("</ul>");
//
//		list = icePlantColdStorageService.getAllIcePlantColdStorages(page, numberOfRecordsPerPage, authenticationFacade.getUsername());
//		String pageNumber = builder.toString();
//
//		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
//		resourcesPaginations.setIcePlantColdStorage(list);
//		resourcesPaginations.setPageNumber(pageNumber);
//
//		ObjectMapper objectMapper = new ObjectMapper();
//
//		return objectMapper.writeValueAsString(resourcesPaginations);
//	}

	private void savingResources(IcePlantColdStorage coldStorage) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		coldStorage.setRegion_id(region_data[0]);
		coldStorage.setRegion(region_data[1]);

		province = coldStorage.getProvince();
		data = province.split(",", 2);
		coldStorage.setProvince_id(data[0]);
		coldStorage.setProvince(data[1]);

		municipal = coldStorage.getMunicipality();
		municipal_data = municipal.split(",", 2);
		coldStorage.setMunicipality_id(municipal_data[0]);
		coldStorage.setMunicipality(municipal_data[1]);

		barangay = coldStorage.getBarangay();
		barangay_data = barangay.split(",", 3);
		coldStorage.setBarangay_id(barangay_data[0]);
		coldStorage.setBarangay(barangay_data[1]);
		coldStorage.setEnabled(true);
		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(coldStorage.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		if (coldStorage.getId() != 0) {
			Optional<IcePlantColdStorage> ipcs = icePlantColdStorageService.findById(coldStorage.getId());
			String image_server = ipcs.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				coldStorage.setImage("/images/" + image_server);
				newUniqueKeyforMap = coldStorage.getUniqueKey();
				icePlantColdStorageService.saveIcePlantColdStorage(ResourceData.getIcePlantColdStorage(coldStorage, user, "", UPDATE));
			}else {
				newUniqueKeyforMap = coldStorage.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", ""); 
				coldStorage.setImage("/images/" + imageName);
				Base64Converter.convertToImage(coldStorage.getImage_src(), coldStorage.getImage());
				icePlantColdStorageService.saveIcePlantColdStorage(ResourceData.getIcePlantColdStorage(coldStorage, user, "", UPDATE));
			}
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", ""); 
			coldStorage.setImage("/images/" + imageName);			
			Base64Converter.convertToImage(coldStorage.getImage_src(), coldStorage.getImage());
			icePlantColdStorageService.saveIcePlantColdStorage(ResourceData.getIcePlantColdStorage(coldStorage, user, newUniqueKey, SAVE));
			save = true;

		}

	}

	@RequestMapping(value = "/getLastPageIcePlant", method = RequestMethod.GET)
	public @ResponseBody String getLastPageIcePlant(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = icePlantColdStorageService.IcePlantColdStorageCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}


}
