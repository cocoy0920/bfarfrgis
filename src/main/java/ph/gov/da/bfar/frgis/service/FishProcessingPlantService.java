package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.FishProcessingPlantPage;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;


public interface FishProcessingPlantService {
	
	Optional<FishProcessingPlant> findById(int id);
	
	List<FishProcessingPlant> ListByUsername(String username);
	List<FishProcessingPlant> ListByRegionList(String region_id);
	List<FishProcessingPlant> ListByRegionList(String region_id,String type);
	List<FishProcessingPlant> ListByProvinceList(String province_id);
	FishProcessingPlant findByUniqueKey(String uniqueKey);
	void saveFishProcessingPlant(FishProcessingPlant FishProcessingPlant);
	List<FishProcessingPlant> findByUniqueKeyForUpdate(String uniqueKey);
	void deleteFishProcessingById(int id);
	void updateForEnabled(int  id);
	void updateForReason(int  id,String reason);
	List<FishProcessingPlant> findAllFishProcessingPlants(); 
	List<FishProcessingPlant> findAllFishProcessingPlants(String type); 
    public List<FishProcessingPlant> getAllFishProcessingPlants(int index,int pagesize,String user);
    public int FishProcessingPlantCount(String username);
    public Page<FishProcessingPlantPage> findByUsername(@Param("user") String user, Pageable pageable); 
    public Page<FishProcessingPlantPage> findByRegion(@Param("region") String region, Pageable pageable); 
    public Page<FishProcessingPlantPage> findAllFishProcessing(Pageable pageable); 
    
    public Long countAllFishProcessingPlant();
    public Long countFishProcessingPlantPerRegion(String region_id);
    public Long countFishProcessingPlantPerProvince(String province_id);
    public Long countFishProcessingPlantPerMuunicipality(String municipality_id);
    
    public Long countFishProcessingPlantPerRegion(String region_id,String operator_classification);
    public Long countFishProcessingPlantPerProvince(String province_id,String operator_classification);
    public Long countFishProcessingPlantPerMuunicipality(String municipality_id,String operator_classification);

	
	//boolean isFishProcessingPlantSSOUnique(Integer id, String sso);

}