package ph.gov.da.bfar.frgis.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.FishSanctuaryPage;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.repository.FishSanctuaryRepo;



@Service("FishSanctuariesService")
@Transactional
public class FishSanctuariesServiceImpl implements FishSanctuariesService{

	@Autowired
	private FishSanctuaryRepo dao;
	

//	@Autowired
//	MapsService mapsService;
	
	public Optional<FishSanctuaries> findById(int id) {
		return dao.findById(id);
	}
	
	@Override
	public FishSanctuaries findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	public List<FishSanctuaries> getListFishSanctuariesByUsername(String username) {
		List<FishSanctuaries>fishSanctuaries = dao.getListFishSanctuariesByUsername(username);
		return fishSanctuaries;
	}

	public void saveFishSanctuaries(FishSanctuaries sanctuaries) {
				
			 dao.save(sanctuaries);		
	}
	

	@Override
	public void deleteFishSanctuariesrById(int id) {
		dao.deleteById(id);
		
	}

	public List<FishSanctuaries> findAllFishSanctuaries() {
		return dao.findAll();
	}

	@Override
	public List<FishSanctuaries> getAllFishSanctuaries(int index, int pagesize, String user) {
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<FishSanctuaries> page = dao.getPageableFishSanctuaries(user,secondPageWithFiveElements);
		
		return page;
	}

	@Override
	public int FishSanctuariesCount(String username) {
		return dao.FishSanctuariesCount(username);
	}

	@Override
	public List<FishSanctuaries> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}


	@Override
	public void findByUniqueKeyForEnabled( int id) {
	dao.updateFishSanctuariesByEnabled( id);
		
	}

	@Override
	public void findByUniqueKeyForReason(int  id,String reason) {
		dao.updateFishSanctuariesByReason(id,reason);
		
	}

	@Override
	public Page<FishSanctuaryPage> findByUsername(String username, Pageable pageable) {
	
		return dao.findByUsername(username, pageable);
	}
	@Override
	public Page<FishSanctuaryPage> findByRegion(String region, Pageable pageable) {
		return dao.findByRegion(region, pageable);
	}
	
	@Override
	public Page<FishSanctuaryPage> findAllFishSanctuary(Pageable pageable) {

	return dao.findAllFishSanctuary(pageable);
}

	@Override
	public List<FishSanctuaries> findByRegionList(String regionID) {
		return dao.findByRegionList(regionID);
	}
	
	@Override
	public List<FishSanctuaries> findByProvinceList(String provinceID) {
		return dao.findByProvinceList(provinceID);
	}
	/*
	@Override
	public List<FishSanctuaries> findByMunicipality(String municipalityID) {
		return dao.findByMunicipality(municipalityID);
	}

	@Override
	public int FishSanctuariesCountByMuunicipality(String municipalityID) {
		return dao.FishSanctuariesCountByMuunicipality(municipalityID);
	}

	@Override
	public int FishSanctuariesCountByProvince(String provinceID) {
		return dao.FishSanctuariesCountByProvince(provinceID);
	}

	@Override
	public int FishSanctuariesCountByRegion(String regionID) {
		return dao.FishSanctuariesCountByRegion(regionID);
	}

	@Override
	public List<FishSanctuaries> getAllFishSanctuariesByRegion(int index, int pagesize, String region_id) {
		return dao.getAllFishSanctuariesByRegion(index, pagesize, region_id);
	}

	@Override
	public List<FishSanctuaries> getAllFishSanctuariesByAllRegion(int index, int pagesize) {
		return dao.getAllFishSanctuariesByAllRegion(index, pagesize);
	}
*/

	@Override
	public Long countFishSanctuariesPerRegion(String region_id) {

		return dao.countFishSanctuariesPerRegion(region_id);
	}

	@Override
	public Long countFishSanctuariesPerProvince(String province_id) {
	
		return dao.countFishSanctuariesPerProvince(province_id);
	}

	@Override
	public Long countAllFishSanctuaries() {
		
		return dao.countAllFishSanctuaries();
	}

	@Override
	public Long countFishSanctuariesPerMunicipal(String municipal_id) {
	
		return dao.countFishSanctuariesPerMunicipal(municipal_id);
	}



/*	public boolean isfishSanctuariesSSOUnique(Integer id, String sso) {
		fishSanctuaries fishSanctuaries = findBySSO(sso);
		return ( fishSanctuaries == null || ((id != null) && (fishSanctuaries.getId() == id)));
	}*/

	
	
	
}
