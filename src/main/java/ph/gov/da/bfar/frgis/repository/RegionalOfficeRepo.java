package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.RegionalOfficePage;
import ph.gov.da.bfar.frgis.model.RegionalOffice;

@Repository
public interface RegionalOfficeRepo extends JpaRepository<RegionalOffice, Integer> {

	@Query("SELECT c from RegionalOffice c where c.user = :username")
	public List<RegionalOffice> getListRegionalOfficeByUsername(@Param("username") String username);
	
	@Query("SELECT c from RegionalOffice c where c.region_id = :region_id")
	public List<RegionalOffice> getListRegionalOfficeByRegion(@Param("region_id") String region_id);

	@Modifying(clearAutomatically = true)
	@Query("update RegionalOffice f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update RegionalOffice f set f.reason =:reason  where f.id =:id")
	public void updateRegionalOfficeByReason( @Param("id") int id, @Param("reason") String reason);
	
	
	@Query("SELECT COUNT(f) FROM RegionalOffice f WHERE f.user=:username")
	public int  RegionalOfficeCount(@Param("username") String username);
	
	@Query("SELECT c from RegionalOffice c where c.unique_key = :uniqueKey")
	public RegionalOffice findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from RegionalOffice c where c.unique_key = :uniqueKey")
	List<RegionalOffice> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from RegionalOffice c where c.user = :user")
	 public List<RegionalOffice> getPageableRegionalOffice(@Param("user") String user,Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.RegionalOfficePage ( c.id,c.unique_key,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled) from RegionalOffice c where c.user = :user")
	public Page<RegionalOfficePage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.RegionalOfficePage ( c.id,c.unique_key,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled) from RegionalOffice c where c.region_id = :region_id")
	public Page<RegionalOfficePage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.RegionalOfficePage ( c.id,c.unique_key,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled) from RegionalOffice c")
	public Page<RegionalOfficePage> findAllRegionalOffice(Pageable pageable);

	@Query("SELECT COUNT(f) FROM RegionalOffice f")
	public Long countAllRegionalOffice();
	
	@Query("SELECT COUNT(f) FROM RegionalOffice f WHERE f.region_id=:region_id")
	public Long countRegionalOfficePerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM RegionalOffice f WHERE f.province_id=:province_id")
	public Long countRegionalOfficePerProvince(@Param("province_id") String province_id);

}
