package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.PFOPage;
import ph.gov.da.bfar.frgis.model.LGU;


public interface LGUService {
	
	Optional<LGU> findById(int id);
	
	List<LGU> ListByUsername(String username);
	List<LGU> ListByRegion(String region_id);
	LGU findByUniqueKey(String uniqueKey);
	List<LGU> findByUniqueKeyForUpdate(String uniqueKey);
	void saveLGU(LGU LGU);
	void deleteLGUById(int id);
	void updateByEnabled(int id);
	void updateLGUByReason(int  id,String  reason);
	List<LGU> findAllLGUs(); 
    public List<LGU> getAllLGUs(int index,int pagesize,String user);
    public int LGUCount(String username);
    public Page<PFOPage> findByUsername(@Param("user") String user, Pageable pageable);
    public Page<PFOPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);
    public Page<PFOPage> findAllPFO(Pageable pageable);
    public Long countAllLGU();
    public Long countLGUPerRegion(String region_id);
    public Long countLGUPerProvince(String province_id);

}