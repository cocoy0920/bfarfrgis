package ph.gov.da.bfar.frgis.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Entity
@Table(name="fishcage")
public class FishCage implements Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String user;
	
	private String page;
	
	@Column(name = "unique_key")
	private String uniqueKey;
	
	private String region;
	
	private String region_id;
	
	@NotEmpty(message = "Province may not be empty")
	private String province;
	
	private String province_id;
	
	@NotEmpty(message = "Municipality may not be empty")
	private String municipality;
	
	private String municipality_id; 
	
	@NotEmpty(message = "Barangay may not be empty")
	private String barangay;
	
	private String barangay_id;
		
	private String code;
	
	//INFORMATION

	
	@NotEmpty(message = "Name Of Operator may not be empty")
	@Column(name = "name_of_operator")
	private String nameOfOperator;
	
	@NotEmpty(message = "Classification of Operator may not be empty")
	@Column(name = "classification_of_operator")
	private String classificationofOperator;
	
	@NotEmpty(message = "Cage Dimension may not be empty")
	@Column(name = "cage_dimension")
	private String cageDimension;
	
	@Column(name = "cage_design")
	private String cageDesign;
	
	@NotEmpty(message = "Cage Type may not be empty")
	@Column(name = "cage_type")
	private String cageType;
	@NotEmpty(message = "Cage No.Of Compartments may not be empty")
	@Column(name = "cage_no_of_compartments")
	private String cageNoOfCompartments;
	
	@NotEmpty(message = "Indicate Species may not be empty")
	@Column(name = "indicate_species")
	private String indicateSpecies;
	
	//ADDITIONAL INFORMATION
	@NotEmpty(message = "Date as Of may not be empty")
	@Column(name = "date_as_of")
	private String dateAsOf;
	
	@NotEmpty(message = "Data Source may not be empty")
	@Column(name = "source_of_data")
	private String sourceOfData;
	
	@NotEmpty(message = "Area may not be empty")
	private String  area;

	@NotEmpty(message = "Remarks may not be empty")
	private String remarks;
	
	@Column(name = "encode_by")
	private String encodedBy;
	
	@Column(name = "edited_by")
	private String editedBy;

	@Column(name = "date_encoded")
	private String dateEncoded;
	
	@Column(name = "date_edited")
	private String dateEdited;
	
	private String last_used;
	
	@NotEmpty(message = "Latitude may not be empty")
	private String lat;
	
	@NotEmpty(message = "Longitude may not be empty")
	private String lon;
	
	private String image;
	
	private String image_src;
	
	private boolean enabled;
	
	private String reason;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getUniqueKey() {
		return uniqueKey;
	}

	public void setUniqueKey(String uniqueKey) {
		this.uniqueKey = uniqueKey;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getRegion_id() {
		return region_id;
	}

	public void setRegion_id(String region_id) {
		this.region_id = region_id;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getProvince_id() {
		return province_id;
	}

	public void setProvince_id(String province_id) {
		this.province_id = province_id;
	}

	public String getMunicipality() {
		return municipality;
	}

	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	public String getMunicipality_id() {
		return municipality_id;
	}

	public void setMunicipality_id(String municipality_id) {
		this.municipality_id = municipality_id;
	}

	public String getBarangay() {
		return barangay;
	}

	public void setBarangay(String barangay) {
		this.barangay = barangay;
	}

	public String getBarangay_id() {
		return barangay_id;
	}

	public void setBarangay_id(String barangay_id) {
		this.barangay_id = barangay_id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNameOfOperator() {
		return nameOfOperator;
	}

	public void setNameOfOperator(String nameOfOperator) {
		this.nameOfOperator = nameOfOperator;
	}

	public String getClassificationofOperator() {
		return classificationofOperator;
	}

	public void setClassificationofOperator(String classificationofOperator) {
		this.classificationofOperator = classificationofOperator;
	}

	public String getCageDimension() {
		return cageDimension;
	}

	public void setCageDimension(String cageDimension) {
		this.cageDimension = cageDimension;
	}

	public String getCageDesign() {
		return cageDesign;
	}

	public void setCageDesign(String cageDesign) {
		this.cageDesign = cageDesign;
	}

	public String getCageType() {
		return cageType;
	}

	public void setCageType(String cageType) {
		this.cageType = cageType;
	}

	public String getCageNoOfCompartments() {
		return cageNoOfCompartments;
	}

	public void setCageNoOfCompartments(String cageNoOfCompartments) {
		this.cageNoOfCompartments = cageNoOfCompartments;
	}

	public String getIndicateSpecies() {
		return indicateSpecies;
	}

	public void setIndicateSpecies(String indicateSpecies) {
		this.indicateSpecies = indicateSpecies;
	}

	public String getDateAsOf() {
		return dateAsOf;
	}

	public void setDateAsOf(String dateAsOf) {
		this.dateAsOf = dateAsOf;
	}

	public String getSourceOfData() {
		return sourceOfData;
	}

	public void setSourceOfData(String sourceOfData) {
		this.sourceOfData = sourceOfData;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getEncodedBy() {
		return encodedBy;
	}

	public void setEncodedBy(String encodedBy) {
		this.encodedBy = encodedBy;
	}

	public String getEditedBy() {
		return editedBy;
	}

	public void setEditedBy(String editedBy) {
		this.editedBy = editedBy;
	}

	public String getDateEncoded() {
		return dateEncoded;
	}

	public void setDateEncoded(String dateEncoded) {
		this.dateEncoded = dateEncoded;
	}

	public String getDateEdited() {
		return dateEdited;
	}

	public void setDateEdited(String dateEdited) {
		this.dateEdited = dateEdited;
	}

	public String getLast_used() {
		return last_used;
	}

	public void setLast_used(String last_used) {
		this.last_used = last_used;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImage_src() {
		return image_src;
	}

	public void setImage_src(String image_src) {
		this.image_src = image_src;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
		
}
