package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.SeaGrassPage;
import ph.gov.da.bfar.frgis.model.SeaGrass;
import ph.gov.da.bfar.frgis.repository.SeaGrassRepo;


@Service("SeaGrassService")
@Transactional
public class SeaGrassServiceImpl implements SeaGrassService{

	@Autowired
	private SeaGrassRepo dao;


	
	public Optional<SeaGrass> findById(int id) {
		return dao.findById(id);
	}

	public List<SeaGrass> ListByUsername(String username) {
		List<SeaGrass>SeaGrass = dao.getListSeaGrassByUsername(username);
		return SeaGrass;
	}

	public void saveSeaGrass(SeaGrass SeaGrass) {
		dao.save(SeaGrass);
	}

	@Override
	public void deleteSeaGrassById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateByEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateSeaGrassByReason(int id, String reason) {
		dao.updateSeaGrassByReason(id, reason);
		
	}

	public List<SeaGrass> findAllSeaGrasss() {
		return dao.findAll();
	}

	@Override
	public List<SeaGrass> getAllSeaGrasss(int index, int pagesize, String user) {
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<SeaGrass> page = dao.getPageableSeaGrass(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int SeaGrassCount(String username) {
		return dao.SeaGrassCount(username);
	}

	@Override
	public SeaGrass findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<SeaGrass> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<SeaGrassPage> findByUsername(String user, Pageable pageable) {
		
		return dao.findByUsername(user, pageable);
	}

	@Override
	public Page<SeaGrassPage> findAllSeaGrass(Pageable pageable) {
		
		return dao.findAllSeaGrass(pageable);
	}
	
	@Override
	public Page<SeaGrassPage> findByRegion(String region, Pageable pageable) {
		
		return dao.findByRegion(region, pageable);
	}
	@Override
	public List<SeaGrass> ListByRegion(String region_id) {
	
		return dao.getListSeaGrassByRegion(region_id);
	}

	@Override
	public Long countSeaGrassPerRegion(String region_id) {

		return dao.countSeaGrassPerRegion(region_id);
	}

	@Override
	public Long countSeaGrassPerProvince(String province_id) {
		
		return dao.countSeaGrassPerProvince(province_id);
	}

	@Override
	public Long countAllSeaGrass() {
		return dao.countAllSeaGrass();
	}

	@Override
	public Long countSeaGrassPerMunicipality(String municipality_id) {

		return dao.countSeaGrassPerMunicipality(municipality_id);
	}

	
}
