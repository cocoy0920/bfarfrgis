package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.SchoolPage;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;
import ph.gov.da.bfar.frgis.repository.SchoolRepo;



@Service("SchoolOfFisheriesService")
@Transactional
public class SchoolOfFisheriesServiceImpl implements SchoolOfFisheriesService{

	@Autowired
	private SchoolRepo dao;


	
	public Optional<SchoolOfFisheries> findById(int id) {
		return dao.findById(id);
	}

	public List<SchoolOfFisheries> ListByUsername(String username) {
		List<SchoolOfFisheries>SchoolOfFisheries = dao.getListSchoolOfFisheriesByUsername(username);
		return SchoolOfFisheries;
	}

	public void saveSchoolOfFisheries(SchoolOfFisheries SchoolOfFisheries) {
		//user.setPassword(passwordEncoder.encode(user.getPassword()));
		dao.save(SchoolOfFisheries);
	}
	@Override
	public void deleteSchoolOfFisheriesById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateByEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateSchoolOfFisheriesByReason(int id, String reason) {
		dao.updateSchoolOfFisheriesByReason(id, reason);
		
	}

	public List<SchoolOfFisheries> findAllSchoolOfFisheriess() {
		return dao.findAll();
	}

	@Override
	public List<SchoolOfFisheries> getAllSchoolOfFisheriess(int index, int pagesize, String user) {
		
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<SchoolOfFisheries> page = dao.getPageableSchoolOfFisheries(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int SchoolOfFisheriesCount(String username) {
		return dao.SchoolOfFisheriesCount(username);
	}

	@Override
	public SchoolOfFisheries findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<SchoolOfFisheries> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<SchoolPage> findByUsername(String user, Pageable pageable) {
	
		return dao.findByUsername(user, pageable);
	}
	
	@Override
	public Page<SchoolPage> findAllSchool(Pageable pageable) {
		return dao.findAllSchool(pageable);
	}
	
	@Override
	public Page<SchoolPage> findByRegion(String region, Pageable pageable) {
		return dao.findByRegion(region, pageable);
	}

	@Override
	public List<SchoolOfFisheries> ListByRegion(String region_id) {
	
		return dao.getListSchoolOfFisheriesByRegion(region_id);
	}

	@Override
	public Long countSchoolOfFisheriesPerRegion(String region_id) {

		return dao.countSchoolOfFisheriesPerRegion(region_id);
	}

	@Override
	public Long countSchoolOfFisheriesPerProvince(String province_id) {
		
		return dao.countSchoolOfFisheriesPerProvince(province_id);
	}

	@Override
	public Long countAllSchoolOfFisheries() {
		return dao.countAllSchoolOfFisheries();
	}

	@Override
	public Long countSchoolOfFisheriesPerMunicipality(String municipality_id) {
		
		return dao.countSchoolOfFisheriesPerMunicipality(municipality_id);
	}

}
