package ph.gov.da.bfar.frgis.web.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.model.Chart;
import ph.gov.da.bfar.frgis.model.Municipalities;
import ph.gov.da.bfar.frgis.model.Provinces;
import ph.gov.da.bfar.frgis.model.Regions;
import ph.gov.da.bfar.frgis.service.BarangayService;
import ph.gov.da.bfar.frgis.service.FishCageService;
import ph.gov.da.bfar.frgis.service.FishCorralService;
import ph.gov.da.bfar.frgis.service.FishLandingService;
import ph.gov.da.bfar.frgis.service.FishPenService;
import ph.gov.da.bfar.frgis.service.FishPondService;
import ph.gov.da.bfar.frgis.service.FishPortService;
import ph.gov.da.bfar.frgis.service.FishProcessingPlantService;
import ph.gov.da.bfar.frgis.service.FishSanctuariesService;
import ph.gov.da.bfar.frgis.service.HatcheryService;
import ph.gov.da.bfar.frgis.service.IcePlantColdStorageService;
import ph.gov.da.bfar.frgis.service.LGUService;
import ph.gov.da.bfar.frgis.service.LambakladService;
import ph.gov.da.bfar.frgis.service.MangroveService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.MaricultureZoneService;
import ph.gov.da.bfar.frgis.service.MarketService;
import ph.gov.da.bfar.frgis.service.MunicipalityService;
import ph.gov.da.bfar.frgis.service.NationalCenterService;
import ph.gov.da.bfar.frgis.service.PayaoService;
import ph.gov.da.bfar.frgis.service.ProvinceService;
import ph.gov.da.bfar.frgis.service.RegionalOfficeService;
import ph.gov.da.bfar.frgis.service.RegionsService;
import ph.gov.da.bfar.frgis.service.SchoolOfFisheriesService;
import ph.gov.da.bfar.frgis.service.SeaGrassService;
import ph.gov.da.bfar.frgis.service.SeaweedsService;
import ph.gov.da.bfar.frgis.service.TOSService;
import ph.gov.da.bfar.frgis.service.TrainingCenterService;
import ph.gov.da.bfar.frgis.service.UserService;

@Component
public class ChartsImplements  implements ChartsInterface{
	@Autowired
	UserService userService;

	
	@Autowired
	
	ProvinceService provinceService;

	@Autowired
	 RegionsService regionsService;

	@Autowired
	 MunicipalityService municipalityService;
	
	@Autowired
	 BarangayService barangayService;
	
	@Autowired
	 MapsService mapsService;
	
	@Autowired
	 FishSanctuariesService fishSanctuariesService;
	
	@Autowired
	 FishProcessingPlantService fishProcessingPlantService;
	
	@Autowired
	 FishLandingService fishLandingService;
	
	@Autowired
	 FishPortService fishPortService;
	
	@Autowired
	 FishPenService fishPenService;
	
	@Autowired
	 FishCageService fishCageService;
	
	@Autowired
	 FishPondService fishPondService;

	@Autowired
	 IcePlantColdStorageService icePlantColdStorageService;
	
	@Autowired
	 MarketService marketService;

	@Autowired
	 SchoolOfFisheriesService schoolOfFisheriesService;
	
	@Autowired
	 FishCorralService fishCorralService;
	
	@Autowired
	 SeaGrassService seaGrassService;
	
	@Autowired
	 SeaweedsService seaweedsService;
	
	@Autowired
	 MangroveService mangroveService;
	
	@Autowired
	 LGUService lGUService;
	
	@Autowired
	 MaricultureZoneService zoneService;
	
	@Autowired
	 TrainingCenterService trainingCenterService;
	
	@Autowired
	 HatcheryService hatcheryService;
	
	@Autowired
	 NationalCenterService nationalCenterService;
	
	@Autowired
	 RegionalOfficeService regionalOfficeService;
	
	@Autowired
	 TOSService tosService;
	
	@Autowired
	 PayaoService payaoService;
	
	@Autowired
	LambakladService lambakladService;
	

	@Override
	public List<Chart> getAquafarmChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();;
		long id=1;
		for(Regions region: regions) {
			Chart chart = new Chart(); 
			long fishpenCount = fishPenService.countFishPenPerRegion(region.getRegion_id());
			long fishcoralCount = fishCorralService.countFishCoralPerRegion(region.getRegion_id());
			long fishcageCount = fishCageService.countFishCagePerRegion(region.getRegion_id());
			long fishpondCount = fishPondService.countFishPondPerRegion(region.getRegion_id());
			//charts.add(new Chart(id++,region.getRegion(),fishpenCount,fishcoralCount,fishcageCount,fishpondCount));
		
			chart.setId(id++);
			chart.setProvince(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setFishpenCount(fishpenCount);
			chart.setFishcoralCount(fishcoralCount);
			chart.setFishcageCount(fishcageCount);
			chart.setFishpondCount(fishpondCount);
			
			charts.add(chart);

		}	

			return charts;
	}

	public List<Chart> getFishPenChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();;
		long id=1;
		for(Regions region: regions) {
			Chart chart = new Chart(); 
			long fishpenCount = fishPenService.countFishPenPerRegion(region.getRegion_id());
			
			chart.setId(id++);
			chart.setProvince(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setFishpenCount(fishpenCount);
			
			charts.add(chart);

		}	

			return charts;
	}	
	@Override
	public List<Chart> getAquafarmChartByProvince(List<Provinces> provinces ) {
		List<Chart> charts = new ArrayList<Chart>();;
		long id=1;
		for(Provinces province: provinces) {
			Chart chart = new Chart(); 
			
			long fishpenCount = fishPenService.countFishPenPerProvince(province.getProvince_id());
			long fishcoralCount = fishCorralService.countFishCoralPerProvince(province.getProvince_id());
			long fishcageCount = fishCageService.countFishCagePerProvince(province.getProvince_id());
			long fishpondCount = fishPondService.countFishPondPerProvince(province.getProvince_id());
			//charts.add(new Chart(id++,province.getProvince(),fishpenCount,fishcoralCount,fishcageCount,fishpondCount));
			
			chart.setId(id++);
			chart.setProvince(province.getProvince());
			chart.setLocation(province.getProvince());
			chart.setFishpenCount(fishpenCount);
			chart.setFishcoralCount(fishcoralCount);
			chart.setFishcageCount(fishcageCount);
			chart.setFishpondCount(fishpondCount);
			
			charts.add(chart);
		}	

			return charts;
	}

	@Override
	public List<Chart> getAquafarmChartByMunicipality(List<Municipalities> municipalities) {


		List<Chart> charts = new ArrayList<Chart>();;
		long id=1;
		for(Municipalities municipal: municipalities) {
			Chart chart = new Chart();
			long fishpenCount = fishPenService.countFishPenPerMunicipality(municipal.getMunicipal_id());
			long fishcoralCount = fishCorralService.countFishCoralPerMunicipality(municipal.getMunicipal_id());
			long fishcageCount = fishCageService.countFishCagePerRegion(municipal.getMunicipal_id());
			long fishpondCount = fishPondService.countFishPondPerRegion(municipal.getMunicipal_id());
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setFishpenCount(fishpenCount);
			chart.setFishcoralCount(fishcoralCount);
			chart.setFishcageCount(fishcageCount);
			chart.setFishpondCount(fishpondCount);
			
			charts.add(chart);
		}	

			return charts;
	
	}

	@Override
	public List<Chart> getHatcheryChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();
		
		long id=1;
		for(Regions region: regions) {
			Chart chart = new Chart(); 
			long BFARhatcherylegislatedCount = hatcheryService.countHatcheryLegislatedPerRegion(region.getRegion_id(),"BFAR","legislated");
			long BFARhatcheryNONlegislatedCount = hatcheryService.countHatcheryLegislatedPerRegion(region.getRegion_id(),"BFAR","nonLegislated");
			long LGUhatcheryCount = hatcheryService.countHatcheryPerRegion(region.getRegion_id(),"LGU");
			long PRIVATEhatcheryCount = hatcheryService.countHatcheryPerRegion(region.getRegion_id(),"PRIVATE");
			
			chart.setId(id++);
			chart.setRegion(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setLegislated(BFARhatcherylegislatedCount);
			chart.setNonLegislated(BFARhatcheryNONlegislatedCount);
			chart.setLgu(LGUhatcheryCount);
			chart.setPrivateSector(PRIVATEhatcheryCount);
			charts.add(chart);
			//charts.add(new Chart(id++, region.getRegion(),BFARhatcherylegislatedCount,BFARhatcheryNONlegislatedCount,PRIVATEhatcheryCount,LGUhatcheryCount));
		}	

			return charts;
	}
			
	@Override
	public List<Chart> getHatcheryChartByProvince(List<Provinces> provinces ) {
		List<Chart> charts = new ArrayList<Chart>();;
		long id=1;
		for(Provinces province: provinces) {
			Chart chart = new Chart(); 
			
			long BFARhatcherylegislatedCount = hatcheryService.countHatcheryLegislatedPerProvince(province.getProvince_id(),"BFAR","legislated");
			long BFARhatcheryNONlegislatedCount = hatcheryService.countHatcheryLegislatedPerProvince(province.getProvince_id(),"BFAR","nonLegislated");
			long LGUhatcheryCount = hatcheryService.countHatcheryPerProvince(province.getProvince_id(),"LGU");
			long PRIVATEhatcheryCount = hatcheryService.countHatcheryPerProvince(province.getProvince_id(),"PRIVATE");
			
			chart.setId(id++);
			chart.setProvince(province.getProvince());
			chart.setLocation(province.getProvince());
			chart.setLegislated(BFARhatcherylegislatedCount);
			chart.setNonLegislated(BFARhatcheryNONlegislatedCount);
			chart.setLgu(LGUhatcheryCount);
			chart.setPrivateSector(PRIVATEhatcheryCount);
			
			charts.add(chart);

		}	

			return charts;
	}

	@Override
	public List<Chart> getHatcheryChartByMunicipality(List<Municipalities> municipalities) {

		long id=1;
		List<Chart> charts = new ArrayList<Chart>();;
	 
		for(Municipalities municipal: municipalities) {
			Chart chart = new Chart(); 
			long BFARhatcherylegislatedCount = hatcheryService.countHatcheryLegislatedPerMunicipality(municipal.getMunicipal_id(),"BFAR","legislated");
			long BFARhatcheryNONlegislatedCount = hatcheryService.countHatcheryLegislatedPerMunicipality(municipal.getMunicipal_id(),"BFAR","nonLegislated");
			long LGUhatcheryCount = hatcheryService.countHatcheryPerMunicipality(municipal.getMunicipal_id(),"LGU");
			long PRIVATEhatcheryCount = hatcheryService.countHatcheryPerMunicipality(municipal.getMunicipal_id(),"PRIVATE");
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setLegislated(BFARhatcherylegislatedCount);
			chart.setNonLegislated(BFARhatcheryNONlegislatedCount);
			chart.setLgu(LGUhatcheryCount);
			chart.setPrivateSector(PRIVATEhatcheryCount);
			
			charts.add(chart);
		}	

			return charts;
	
	}
	@Override
	public List<Chart> getMaricultureChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();;
		long id=1;
		for(Regions region: regions) {

			long BFARmaricultureCount = zoneService.countMariculturePerRegion(region.getRegion_id(),"BFAR");
			long LGUmaricultureCount = zoneService.countMariculturePerRegion(region.getRegion_id(),"LGU");
			long PRIVATEmaricultureCount = zoneService.countMariculturePerRegion(region.getRegion_id(),"PRIVATE");
			
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setRegion(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setBfar(BFARmaricultureCount);
			chart.setLgu(LGUmaricultureCount);
			chart.setPrivateSector(PRIVATEmaricultureCount);
			
			charts.add(chart);
			
		}	

			return charts;
	}
		
	@Override
	public List<Chart> getMaricultureChartByProvince(List<Provinces> provinces ) {
		List<Chart> charts = new ArrayList<Chart>();;
		long id=1;
		for(Provinces province: provinces) {

			long BFARmaricultureCount = zoneService.countMariculturePerProvince(province.getProvince_id(),"BFAR");
			long LGUmaricultureCount = zoneService.countMariculturePerProvince(province.getProvince_id(),"LGU");
			long PRIVATEmaricultureCount = zoneService.countMariculturePerProvince(province.getProvince_id(),"PRIVATE");

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setProvince(province.getProvince());
			chart.setLocation(province.getProvince());
			chart.setBfar(BFARmaricultureCount);
			chart.setLgu(LGUmaricultureCount);
			chart.setPrivateSector(PRIVATEmaricultureCount);
			
			charts.add(chart);

		}	

			return charts;
	}

	@Override
	public List<Chart> getMaricultureChartByMunicipality(List<Municipalities> municipalities) {


		List<Chart> charts = new ArrayList<Chart>();;
		long id=1;
		for(Municipalities municipal: municipalities) {

			long BFARmaricultureCount = zoneService.countMariculturePerMunicipality(municipal.getMunicipal_id(),"BFAR");
			long LGUmaricultureCount = zoneService.countMariculturePerMunicipality(municipal.getMunicipal_id(),"LGU");
			long PRIVATEmaricultureCount = zoneService.countMariculturePerMunicipality(municipal.getMunicipal_id(),"PRIVATE");

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setBfar(BFARmaricultureCount);
			chart.setLgu(LGUmaricultureCount);
			chart.setPrivateSector(PRIVATEmaricultureCount);
			
			charts.add(chart);
		}	

			return charts;
	
	}
	@Override
	public List<Chart> getSeaweedsChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();;
		long id=1;
		for(Regions region: regions) {

			long seaweedsNurseryCount = seaweedsService.countSeaweedsPerRegion(region.getRegion_id(),"SEAWEEDNURSERY");
			long seaweedsLaboratoryCount = seaweedsService.countSeaweedsPerRegion(region.getRegion_id(),"SEAWEEDLABORATORY");

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setRegion(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setSeaweedsNurseryCount(seaweedsNurseryCount);
			chart.setSeaweedsLaboratoryCount(seaweedsLaboratoryCount);

			
			charts.add(chart);
		}	

			return charts;
	}
		
	@Override
	public List<Chart> getSeaweedsChartByProvince(List<Provinces> provinces ) {
		List<Chart> charts = new ArrayList<Chart>();;
		long id=1;
		for(Provinces province: provinces) {

			long seaweedsNurseryCount = seaweedsService.countSeaweedsPerProvince(province.getProvince_id(),"SEAWEEDNURSERY");
			long seaweedsLaboratoryCount = seaweedsService.countSeaweedsPerProvince(province.getProvince_id(),"SEAWEEDLABORATORY");

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setProvince(province.getProvince());
			chart.setLocation(province.getProvince());
			chart.setSeaweedsNurseryCount(seaweedsNurseryCount);
			chart.setSeaweedsLaboratoryCount(seaweedsLaboratoryCount);

			
			charts.add(chart);
		}	

			return charts;
	}

	@Override
	public List<Chart> getSeaweedsChartByMunicipality(List<Municipalities> municipalities) {

		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Municipalities municipal: municipalities) {
			long seaweedsNurseryCount = seaweedsService.countSeaweedsPerMinucipality(municipal.getMunicipal_id(), "SEAWEEDNURSERY");
			long seaweedsLaboratoryCount = seaweedsService.countSeaweedsPerMinucipality(municipal.getMunicipal_id(), "SEAWEEDLABORATORY");

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setSeaweedsNurseryCount(seaweedsNurseryCount);
			chart.setSeaweedsLaboratoryCount(seaweedsLaboratoryCount);

			
			charts.add(chart);
		}
		return charts;
	
	}
	@Override
	public List<Chart> getFishlandingChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();;
		 long id = 1;
		for(Regions region: regions) {

			long CFLCOperationalCount = fishLandingService.countFishLandingPerRegion(region.getRegion_id(), "CFLC", "Operational");			
			long CFLCForOperationalCount = fishLandingService.countFishLandingPerRegion(region.getRegion_id(), "CFLC", "ForOperation");
			long CFLCForCompletionlCount = fishLandingService.countFishLandingPerRegion(region.getRegion_id(), "CFLC", "ForCompletion");
			long CFLCForTransferCount = fishLandingService.countFishLandingPerRegion(region.getRegion_id(), "CFLC", "ForTransfer");
			long CFLCDamagedCount = fishLandingService.countFishLandingPerRegion(region.getRegion_id(), "CFLC", "Damaged");
			long CFLCTraditionalCount = fishLandingService.countFishLandingPerRegion(region.getRegion_id(), "Traditional");
			long CFLCNONTraditionalCount = fishLandingService.countFishLandingPerRegion(region.getRegion_id(), "Non-Traditional");
			
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setProvince(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setOperational(CFLCOperationalCount);
			chart.setForOperation(CFLCForOperationalCount);
			chart.setForCompletion(CFLCForCompletionlCount);
			chart.setForTransfer(CFLCForTransferCount);
			chart.setDamaged(CFLCDamagedCount);
			chart.setTraditional(CFLCTraditionalCount);
			chart.setNonTraditional(CFLCNONTraditionalCount);
						
			charts.add(chart);
		}	
	
			return charts;
	}
		
	@Override
	public List<Chart> getFishlandingChartByProvince(List<Provinces> provinces ) {
		List<Chart> charts = new ArrayList<Chart>();;
	 long id = 1;
		for(Provinces province: provinces) {
			Chart chart = new Chart(); 
			
			long CFLCOperationalCount = fishLandingService.countFishLandingPerProvince(province.getProvince_id(), "CFLC", "Operational");
			long CFLCForOperationalCount = fishLandingService.countFishLandingPerProvince(province.getProvince_id(), "CFLC", "ForOperation");
			long CFLCForCompletionlCount = fishLandingService.countFishLandingPerProvince(province.getProvince_id(), "CFLC", "ForCompletion");
			long CFLCForTransferCount = fishLandingService.countFishLandingPerProvince(province.getProvince_id(), "CFLC", "ForTransfer");
			long CFLCDamagedCount = fishLandingService.countFishLandingPerProvince(province.getProvince_id(), "CFLC", "Damaged");
			long CFLCTraditionalCount = fishLandingService.countFishLandingPerProvince(province.getProvince_id(), "Traditional");
			long CFLCNONTraditionalCount = fishLandingService.countFishLandingPerProvince(province.getProvince_id(), "Non-Traditional");
			
			chart.setId(id++);
			chart.setProvince(province.getProvince());
			chart.setLocation(province.getProvince());
			chart.setOperational(CFLCOperationalCount);
			chart.setForOperation(CFLCForOperationalCount);
			chart.setForCompletion(CFLCForCompletionlCount);
			chart.setForTransfer(CFLCForTransferCount);
			chart.setDamaged(CFLCDamagedCount);
			chart.setTraditional(CFLCTraditionalCount);
			chart.setNonTraditional(CFLCNONTraditionalCount);
						
			charts.add(chart);
		}	

			return charts;
	}

	@Override
	public List<Chart> getFishlandingChartByMunicipality(List<Municipalities> municipalities) {


		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Municipalities municipal: municipalities) {
			
			
			
			long CFLCOperationalCount = fishLandingService.countFishLandingPerMinicipality(municipal.getMunicipal_id(), "CFLC", "Operational");
			long CFLCForOperationalCount = fishLandingService.countFishLandingPerMinicipality(municipal.getMunicipal_id(), "CFLC", "ForOperation");
			long CFLCForCompletionlCount = fishLandingService.countFishLandingPerMinicipality(municipal.getMunicipal_id(), "CFLC", "ForCompletion");
			long CFLCForTransferCount = fishLandingService.countFishLandingPerMinicipality(municipal.getMunicipal_id(), "CFLC", "ForTransfer");
			long CFLCDamagedCount = fishLandingService.countFishLandingPerMinicipality(municipal.getMunicipal_id(), "CFLC", "Damaged");
			long CFLCTraditionalCount = fishLandingService.countFishLandingPerMinicipality(municipal.getMunicipal_id(), "Traditional");
			long CFLCNONTraditionalCount = fishLandingService.countFishLandingPerMinicipality(municipal.getMunicipal_id(), "Non-Traditional");
			
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setOperational(CFLCOperationalCount);
			chart.setForOperation(CFLCForOperationalCount);
			chart.setForCompletion(CFLCForCompletionlCount);
			chart.setForTransfer(CFLCForTransferCount);
			chart.setDamaged(CFLCDamagedCount);
			chart.setTraditional(CFLCTraditionalCount);
			chart.setNonTraditional(CFLCNONTraditionalCount);
						
			charts.add(chart);
		
		}	

			return charts;
	
	}

	@Override
	public List<Chart> getColdChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Regions region: regions) {
		Chart chart = new Chart(); 
		
		long coldBFARCount = icePlantColdStorageService.countIcePlantColdStoragePerRegion(region.getRegion_id(), "BFAR");
		long coldPRIVATECount = icePlantColdStorageService.countIcePlantColdStoragePerRegion(region.getRegion_id(), "PRIVATE");
		long coldCOOPERATIVECount = icePlantColdStorageService.countIcePlantColdStoragePerRegion(region.getRegion_id(), "COOPERATIVE");
		charts.add(new Chart(id++,region.getRegion(),coldBFARCount,coldPRIVATECount,coldCOOPERATIVECount));
		
		chart.setId(id++);
		chart.setRegion(region.getRegion());
		chart.setLocation(region.getRegion());
		chart.setBfar(coldBFARCount);
		chart.setPrivateSector(coldPRIVATECount);
		chart.setCooperativw(coldCOOPERATIVECount);
		
		charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getColdChartByProvince(List<Provinces> provinces) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Provinces province: provinces) {
			
		Chart chart = new Chart(); 	
		
		long coldBFARCount = icePlantColdStorageService.countIcePlantColdStoragePerProvince(province.getProvince_id(), "BFAR");
		long coldPRIVATECount = icePlantColdStorageService.countIcePlantColdStoragePerProvince(province.getProvince_id(), "PRIVATE");
		long coldCOOPERATIVECount = icePlantColdStorageService.countIcePlantColdStoragePerProvince(province.getProvince_id(), "COOPERATIVE");
		//charts.add(new Chart(id++,province.getProvince(),coldBFARCount,coldPRIVATECount,coldCOOPERATIVECount));
		

		chart.setId(id++);
		chart.setProvince(province.getProvince());
		chart.setLocation(province.getProvince());
		chart.setBfar(coldBFARCount);
		chart.setPrivateSector(coldPRIVATECount);
		chart.setCooperativw(coldCOOPERATIVECount);
		
		charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getColdChartByMunicipality(List<Municipalities> municipalities) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Municipalities municipal: municipalities) {
			
		Chart chart = new Chart(); 	
		
		long coldBFARCount = icePlantColdStorageService.countIcePlantColdStoragePerMunicipality(municipal.getMunicipal_id(), "BFAR");
		long coldPRIVATECount = icePlantColdStorageService.countIcePlantColdStoragePerMunicipality(municipal.getMunicipal_id(), "PRIVATE");
		long coldCOOPERATIVECount = icePlantColdStorageService.countIcePlantColdStoragePerMunicipality(municipal.getMunicipal_id(), "COOPERATIVE");
		

		chart.setId(id++);
		chart.setProvince(municipal.getMunicipal());
		chart.setLocation(municipal.getMunicipal());
		chart.setBfar(coldBFARCount);
		chart.setPrivateSector(coldPRIVATECount);
		chart.setCooperativw(coldCOOPERATIVECount);
		
		charts.add(chart);
		//charts.add(new Chart(id++,municipal.getMunicipal(),coldBFARCount,coldPRIVATECount,coldCOOPERATIVECount));
		}
		return charts;
	}

	@Override
	public List<Chart> getFishProcessingChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Regions region: regions) {
			long bfarCount = fishProcessingPlantService.countFishProcessingPlantPerRegion(region.getRegion_id(), "BFAR_MANAGED");
			long lguCounts = fishProcessingPlantService.countFishProcessingPlantPerRegion(region.getRegion_id(), "LGU_MANAGED");
			long privateSectorCount = fishProcessingPlantService.countFishProcessingPlantPerRegion(region.getRegion_id(), "PRIVATE_SECTOR");
			long cooperativeCount = fishProcessingPlantService.countFishProcessingPlantPerRegion(region.getRegion_id(), "COOPERATIVE");
			
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setRegion(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setBfar(bfarCount);
			chart.setLgu(lguCounts);
			chart.setPrivateSector(privateSectorCount);
			chart.setCooperativw(cooperativeCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getFishProcessingChartByProvince(List<Provinces> provinces) {

		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Provinces province: provinces) {
			long bfarCount = fishProcessingPlantService.countFishProcessingPlantPerProvince(province.getProvince_id(), "BFAR_MANAGED");
			long lguCounts = fishProcessingPlantService.countFishProcessingPlantPerProvince(province.getProvince_id(), "LGU_MANAGED");
			long privateSectorCount = fishProcessingPlantService.countFishProcessingPlantPerProvince(province.getProvince_id(), "PRIVATE_SECTOR");
			long cooperativeCount = fishProcessingPlantService.countFishProcessingPlantPerProvince(province.getProvince_id(), "COOPERATIVE");

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setProvince(province.getProvince());
			chart.setLocation(province.getProvince());
			chart.setBfar(bfarCount);
			chart.setLgu(lguCounts);
			chart.setPrivateSector(privateSectorCount);
			chart.setCooperativw(cooperativeCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getFishProcessingChartByMunicipality(List<Municipalities> municipalities) {

		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Municipalities municipal: municipalities) {
			long bfarCount = fishProcessingPlantService.countFishProcessingPlantPerMuunicipality(municipal.getMunicipal_id(), "BFAR_MANAGED");
			long lguCounts = fishProcessingPlantService.countFishProcessingPlantPerMuunicipality(municipal.getMunicipal_id(), "LGU_MANAGED");
			long privateSectorCount = fishProcessingPlantService.countFishProcessingPlantPerMuunicipality(municipal.getMunicipal_id(), "PRIVATE_SECTOR");
			long cooperativeCount = fishProcessingPlantService.countFishProcessingPlantPerMuunicipality(municipal.getMunicipal_id(), "COOPERATIVE");
			
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setBfar(bfarCount);
			chart.setLgu(lguCounts);
			chart.setPrivateSector(privateSectorCount);
			chart.setCooperativw(cooperativeCount);
			
			charts.add(chart);
			
			}
		return charts;
	}

	@Override
	public List<Chart> getSeaweedsWDChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Regions region: regions) {
			long WAREHOUSECount = seaweedsService.countSeaweedsPerRegion(region.getRegion_id(), "SEAWEEDWAREHOUSE");
			long DRYERCounts = seaweedsService.countSeaweedsPerRegion(region.getRegion_id(), "SEAWEEDDRYER");

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setRegion(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setSeaweedsWarehouseCount(WAREHOUSECount);
			chart.setSeaweedsDryerCount(DRYERCounts);
			
			charts.add(chart);
			
		
		}
		return charts;
	}

	@Override
	public List<Chart> getSeaweedsWDChartByProvince(List<Provinces> provinces) {
		
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Provinces province: provinces) {
			long WAREHOUSECount = seaweedsService.countSeaweedsPerProvince(province.getProvince_id(), "SEAWEEDWAREHOUSE");
			long DRYERCounts = seaweedsService.countSeaweedsPerProvince(province.getProvince_id(), "SEAWEEDDRYER");
			
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setProvince(province.getProvince());
			chart.setLocation(province.getProvince());
			chart.setSeaweedsWarehouseCount(WAREHOUSECount);
			chart.setSeaweedsDryerCount(DRYERCounts);
			
			charts.add(chart);
			
			}
		return charts;
	}

	@Override
	public List<Chart> getSeaweedsWDChartByMunicipality(List<Municipalities> municipalities) {
		
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Municipalities municipal: municipalities) {
			long WAREHOUSECount = seaweedsService.countSeaweedsPerMinucipality(municipal.getMunicipal_id(), "SEAWEEDWAREHOUSE");
			long DRYERCounts = seaweedsService.countSeaweedsPerMinucipality(municipal.getMunicipal_id(), "SEAWEEDDRYER");
			

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setSeaweedsWarehouseCount(WAREHOUSECount);
			chart.setSeaweedsDryerCount(DRYERCounts);
			
			charts.add(chart);		
		
		}
		return charts;
	}

	@Override
	public List<Chart> getMarketChartByRegion(List<Regions> regions) {
		
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Regions region: regions) {
			long MarketCount = marketService.countMarketRegion(region.getRegion_id());
			
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setRegion(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setFishmarketCount(MarketCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getMarketChartByProvince(List<Provinces> provinces) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Provinces province: provinces) {
			long MarketCount = marketService.countMarketProvince(province.getProvince_id());


			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setProvince(province.getProvince());
			chart.setLocation(province.getProvince());
			chart.setFishmarketCount(MarketCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getMarketChartByMunicipality(List<Municipalities> municipalities) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Municipalities municipal: municipalities) {
			long MarketCount = marketService.countMarketMunicipality(municipal.getMunicipal_id());

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setFishmarketCount(MarketCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getFishPortChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Regions region: regions) {
			
			long FishPortCount = fishPortService.countFishportPerRegion(region.getRegion_id());
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setRegion(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getFishPortChartByProvince(List<Provinces> provinces) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Provinces province: provinces) {
			
			long FishPortCount = fishPortService.countFishportPerProvince(province.getProvince_id());

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setProvince(province.getProvince());
			chart.setLocation(province.getProvince());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getFishPortChartByMunicipality(List<Municipalities> municipalities) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Municipalities municipal: municipalities) {
			
			long FishPortCount = fishPortService.countFishportPerMunicipality(municipal.getMunicipal_id());

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getSeaGrassChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Regions region: regions) {
			
			long FishPortCount = seaGrassService.countSeaGrassPerRegion(region.getRegion_id());
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setRegion(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getSeaGrassChartByProvince(List<Provinces> provinces) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Provinces province: provinces) {
			
			long FishPortCount = seaGrassService.countSeaGrassPerProvince(province.getProvince_id());

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setProvince(province.getProvince());
			chart.setLocation(province.getProvince());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getSeaGrassChartByMunicipality(List<Municipalities> municipalities) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Municipalities municipal: municipalities) {
			
			long FishPortCount = seaGrassService.countSeaGrassPerMunicipality(municipal.getMunicipal_id());

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getMangroveChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Regions region: regions) {
			
			long FishPortCount = mangroveService.countMangrovePerRegion(region.getRegion_id());
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setProvince(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getMangroveChartByProvince(List<Provinces> provinces) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Provinces province: provinces) {
			
			long FishPortCount = mangroveService.countMangrovePerProvince(province.getProvince_id());

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setProvince(province.getProvince());
			chart.setLocation(province.getProvince());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getMangroveChartByMunicipality(List<Municipalities> municipalities) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Municipalities municipal: municipalities) {
			
			long FishPortCount = mangroveService.countMangrovePerMunicipality(municipal.getMunicipal_id());

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getFishSanctuaryChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Regions region: regions) {
			
			long FishPortCount = fishSanctuariesService.countFishSanctuariesPerRegion(region.getRegion_id());
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setProvince(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getFishSanctuaryChartByProvince(List<Provinces> provinces) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Provinces province: provinces) {
			
			long FishPortCount = fishSanctuariesService.countFishSanctuariesPerProvince(province.getProvince_id());

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setProvince(province.getProvince());
			chart.setLocation(province.getProvince());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getFishSanctuaryChartByMunicipality(List<Municipalities> municipalities) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Municipalities municipal: municipalities) {
			
			long FishPortCount = fishSanctuariesService.countFishSanctuariesPerMunicipal(municipal.getMunicipal_id());

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getPayaoChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Regions region: regions) {
			
			long FishPortCount = payaoService.countPayaoPerRegion(region.getRegion_id());
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setRegion(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setFishportCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getPayaoChartByProvince(List<Provinces> provinces) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Provinces province: provinces) {
			
			long FishPortCount = payaoService.countPayaoProvince(province.getProvince_id());

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setProvince(province.getProvince());
			chart.setLocation(province.getProvince());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getPayaoChartByMunicipality(List<Municipalities> municipalities) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Municipalities municipal: municipalities) {
			
			long FishPortCount = payaoService.countPayaoPerMunicipality(municipal.getMunicipal_id());

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getLambakladChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Regions region: regions) {
			
			long FishPortCount = lambakladService.countLambakladPerRegion(region.getRegion_id());
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setRegion(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setFishportCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getLambakladChartByProvince(List<Provinces> provinces) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Provinces province: provinces) {
			
			long FishPortCount = lambakladService.countLambakladPerProvince(province.getProvince_id());

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setProvince(province.getProvince());
			chart.setLocation(province.getProvince());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getLambakladChartByMunicipality(List<Municipalities> municipalities) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Municipalities municipal: municipalities) {
			
			long FishPortCount = lambakladService.countLambakladPerMunicipality(municipal.getMunicipal_id());

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getFRPChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Regions region: regions) {
			
			long FishPortCount = fishSanctuariesService.countFishSanctuariesPerRegion(region.getRegion_id());
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setRegion(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setFishportCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getFRPChartByProvince(List<Provinces> provinces) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Chart> getFRPChartByMunicipality(List<Municipalities> municipalities) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Municipalities municipal: municipalities) {
			
			long FishPortCount = lambakladService.countLambakladPerMunicipality(municipal.getMunicipal_id());

			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setMunicipality(municipal.getMunicipal());
			chart.setLocation(municipal.getMunicipal());
			chart.setFishmarketCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getCoralReefChartByRegion(List<Regions> regions) {
		List<Chart> charts = new ArrayList<Chart>();
		long id=1;
		for(Regions region: regions) {
			
			long FishPortCount = fishSanctuariesService.countFishSanctuariesPerRegion(region.getRegion_id());
			Chart chart = new Chart(); 
			
			chart.setId(id++);
			chart.setRegion(region.getRegion());
			chart.setLocation(region.getRegion());
			chart.setFishportCount(FishPortCount);
			
			charts.add(chart);
		}
		return charts;
	}

	@Override
	public List<Chart> getCoralReefChartByProvince(List<Provinces> provinces) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Chart> getCoralReefChartByMunicipality(List<Municipalities> municipalities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getFishSanctuaryCountByRegion(String region) {
		
		return fishSanctuariesService.countFishSanctuariesPerRegion(region);
	}

	@Override
	public long getFishProcessingCountByRegion(String regions) {
		
		return fishProcessingPlantService.countFishProcessingPlantPerRegion(regions);
	}

	@Override
	public long getFishlandingCountByRegion(String region) {
		return fishLandingService.countFishLandingPerRegion(region);
	}

	@Override
	public long getFishpenCountByRegion(String region) {		
		return fishPenService.countFishPenPerRegion(region);
	}

	@Override
	public long getFishpondCountByRegion(String region) {
	
		return fishPondService.countFishPondPerRegion(region);
	}

	@Override
	public long getFishcageCountByRegion(String region) {
		return fishCageService.countFishCagePerRegion(region);
	}

	@Override
	public long getHatcheryCountByRegion(String region) {
		
		return hatcheryService.countHatcheryPerRegion(region);
	}

	@Override
	public long getColdCountByRegion(String region) {
	
		return icePlantColdStorageService.countIcePlantColdStoragePerRegion(region);
	}

	@Override
	public long getFishportCountByRegion(String region) {
		
		return fishPortService.countFishportPerRegion(region);
	}

	@Override
	public long getFishcoralCountByRegion(String region) {

		return fishCorralService.countFishCoralPerRegion(region);
	}

	@Override
	public long getMaricultureCountByRegion(String region) {
		
		return zoneService.countMariculturePerRegion(region);
	}

	@Override
	public long getMarketCountByRegion(String region) {

		return marketService.countMarketRegion(region);
	}

	@Override
	public long getSchoolCountByRegion(String region) {
	
		return schoolOfFisheriesService.countSchoolOfFisheriesPerRegion(region);
	}

	@Override
	public long getSeagrassCountByRegion(String region) {
	
		return seaGrassService.countSeaGrassPerRegion(region);
	}

	@Override
	public long getSeaweedsCountByRegion(String region) {
	
		return seaweedsService.countSeaweedsPerRegion(region);
	}

	@Override
	public long getMangroveCountByRegion(String region) {

		return mangroveService.countMangrovePerRegion(region);
	}

	@Override
	public long getPFOCountByRegion(String region) {
	
		return lGUService.countLGUPerRegion(region);
	}

	@Override
	public long getTrainingCenterCountByRegion(String region) {

		return trainingCenterService.countTrainingCenterPerRegion(region);
	}

	@Override
	public long getPayaoCountByRegion(String region) {
		
		return payaoService.countPayaoPerRegion(region);
	}

	@Override
	public long getLambakladCountByRegion(String region) {
		
		return lambakladService.countLambakladPerRegion(region);
	}

	@Override
	public long getTOSByRegion(String region) {
		
		return tosService.countTOSPerRegion(region);
	}
	
	
	

}


