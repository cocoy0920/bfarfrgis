package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.model.Provinces;

@Repository
public interface ProvinceRepo extends JpaRepository<Provinces, Integer> {
	
	@Query("SELECT p from Provinces p where p.region_id = :region_id")
	public List<Provinces> findByRegion(String region_id);

}
