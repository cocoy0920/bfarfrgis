package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ph.gov.da.bfar.frgis.Page.FishPenPage;
import ph.gov.da.bfar.frgis.model.FishPen;


public interface FishPenService {
	
	Optional<FishPen> findById(int id);
	
	List<FishPen> findByUsername(String username);
	List<FishPen> findByRegion(String region_id);
	FishPen findByUniqueKey(String uniqueKey);
	List<FishPen> findByUniqueKeyForUpdate(String uniqueKey);
	void saveFishPen(FishPen FishPen);
	void deleteFishPenById(int id);
	void updateForEnabled(int  id);
	void updateFishPenByReason(int  id, String reason);
	List<FishPen> findAllFishPens(); 
    public List<FishPen> getAllFishPens(int index,int pagesize,String user);
    public int FishPenCount(String username);
    Page<FishPenPage> findByUsername(String user, Pageable pageable);
    Page<FishPenPage> findByRegion(String region, Pageable pageable);
    Page<FishPenPage> findAllFishPen(Pageable pageable);
    public Long countFishPenPerRegion(String region_id);
    public Long countFishPenPerProvince(String province_id);
    public Long countFishPenPerMunicipality(String municipality_id);
    public Long countAllFishPen();
//    public int FishPenCountByMuunicipality(String municipalityID);
//    public int FishPenCountByProvince(String provinceID);
//    public int FishPenCountByRegion(String regionID);
	//boolean isFishPenSSOUnique(Integer id, String sso);

}