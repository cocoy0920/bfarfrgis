package ph.gov.da.bfar.frgis.model;


import java.io.Serializable;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class UserBean implements Serializable{



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private int id;
	private String username;
	private String password;
	private String pass;
	private String firstName;
	private String lastName;
	private String email;
	private String state;
	private boolean enabled; 
	private String region;
	private String region_id;
	private String province;
	private String province_id;
	private String userProfiles;
	private int userProfiles_id;



	
}
