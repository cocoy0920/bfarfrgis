package ph.gov.da.bfar.frgis.controller;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
//import net.sf.jasperreports.engine.JRException;
//import net.sf.jasperreports.engine.JasperCompileManager;
//import net.sf.jasperreports.engine.JasperExportManager;
//import net.sf.jasperreports.engine.JasperFillManager;
//import net.sf.jasperreports.engine.JasperPrint;
//import net.sf.jasperreports.engine.JasperReport;
//import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
//import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
//import net.sf.jasperreports.export.SimpleExporterInput;
//import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
//import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.service.UserService;

@Controller
@RequestMapping("/report")
public class ReportController {
	
	@Autowired
	UserService userService;

	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@RequestMapping(value = "home", method = RequestMethod.GET)
	public ModelAndView adminhome(ModelMap model) {
		ModelAndView modelAndView = new ModelAndView();
		
		
		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		modelAndView.setViewName("/report/report_home"); 
		return modelAndView;
	}
	
//	@GetMapping("/pdf/{resources}")
//    public void getDocumentPDFD(HttpServletResponse response) throws IOException, JRException {
//
//        List<FishSanctuaries> dataList = new ArrayList<FishSanctuaries>();
//  
//        
//        File file = ResourceUtils.getFile("classpath:employees.jrxml");
//        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
//       
//        JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(dataList);
//        Map<String, Object> parameters = new HashMap<>();
//        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
//        //JasperExportManager.exportReportToPdfFile(jasperPrint, path + "employees.pdf");
//        
//        response.setContentType("application/x-download");
//        response.addHeader("Content-disposition", "attachment; filename=StatisticsrReport1.pdf");
//        OutputStream out = response.getOutputStream();
//        JasperExportManager.exportReportToPdfStream(jasperPrint,out);//export PDF directly
//
//    }
//    
//    @GetMapping("/excel/{resources}")
//    public void getDocument(HttpServletResponse response) throws IOException, JRException {
//
//
//    	   List<FishSanctuaries> dataList = new ArrayList<FishSanctuaries>();
//
//        File file = ResourceUtils.getFile("classpath:employees.jrxml");
//        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
//       
//        JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(dataList);
//        Map<String, Object> parameters = new HashMap<>();
//        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
//        JRXlsxExporter exporter = new JRXlsxExporter();
//        SimpleXlsxReportConfiguration reportConfigXLS = new SimpleXlsxReportConfiguration();
//        reportConfigXLS.setSheetNames(new String[] { "Sheet 1" });
//        exporter.setConfiguration(reportConfigXLS);
//        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
//        response.setHeader("Content-Disposition", "attachment;filename=jasperfile.xlsx");
//        response.setContentType("application/octet-stream");
//        exporter.exportReport();
//    }

}
