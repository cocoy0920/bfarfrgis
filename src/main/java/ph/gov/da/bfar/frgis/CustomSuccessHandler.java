package ph.gov.da.bfar.frgis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
 
@Component("customSuccessHandler")
public class CustomSuccessHandler implements AuthenticationSuccessHandler {
	
	static final Logger logger = LoggerFactory.getLogger(CustomSuccessHandler.class);
 
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
    

	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;
    
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
    	
    	String targetUrl = "/login?error=true";


    		targetUrl = determineTargetUrl(authentication);
    		
    		request.getSession(false).setMaxInactiveInterval(30*60);


         if (response.isCommitted()) {
 
             return;
         }
  
         redirectStrategy.sendRedirect(request, response, targetUrl);
    	 		
	}
    


    protected String determineTargetUrl(Authentication authentication) {
        String url = "";
        logger.info("Privilege: {} determineTargetUrl", authentication);
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
 
        List<String> roles = new ArrayList<String>();
 
        for (GrantedAuthority a : authorities) {
            roles.add(a.getAuthority());
        }
         if (isRegionalUser(roles)) {
        	 url = "/create/user";
        } else if (isUser(roles)) {
        	
            url = "/create/user";
       } else if (isPFOUser(roles)) {
        	
            url = "/create/user";
       }  else if(isRegionalAdmin(roles)){
        	url = "/region/home";
        	
        }  else if(isPFOAdmin(roles)){
        	url = "/province/home";
        	
        } else if(isCO(roles)){
        	url = "/province/home";
        	
        } else if(isSuperAdmin(roles)){
        	url = "/admin/home";
        	
        } else if(isDBA(roles)){
        	url = "/dba/home";
        	
        } else if(isPlanning(roles)){
        	url = "/report/home";
        	
        }// else {
//            url = "/accessDenied";
//        }
 
        return url;
    }
    
    private boolean isUser(List<String> roles) {
        if (roles.contains("ROLE_USER")) {
            return true;
        }
        return false;
    }
 
    private boolean isRegionalUser(List<String> roles) {
        if (roles.contains("ROLE_REGIONALENCODER")) {
            return true;
        }
        return false;
    }
    
    private boolean isPFOUser(List<String> roles) {
        if (roles.contains("ROLE_PFOENCODER")) {
            return true;
        }
        return false;
    }
 
    private boolean isRegionalAdmin(List<String> roles) {
        if (roles.contains("ROLE_REGIONALADMIN")) {
            return true;
        }
        return false;
    }
    private boolean isPFOAdmin(List<String> roles) {
        if (roles.contains("ROLE_PFOADMIN")) {
            return true;
        }
        return false;
    }
 
    private boolean isSuperAdmin(List<String> roles) {
        if (roles.contains("ROLE_SUPERADMIN")) {
            return true;
        }
        return false;
    }
    
    private boolean isCO(List<String> roles) {
        if (roles.contains("ROLE_CO")) {
            return true;
        }
        return false;
    }
    private boolean isDBA(List<String> roles) {
        if (roles.contains("ROLE_DBA")) {
            return true;
        }
        return false;
    }
    private boolean isPlanning(List<String> roles) {
        if (roles.contains("ROLE_PLANNING")) {
            return true;
       }
        return false;
    }
 
  
 
}
