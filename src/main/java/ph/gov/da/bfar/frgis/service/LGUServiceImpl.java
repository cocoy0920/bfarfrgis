package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.PFOPage;
import ph.gov.da.bfar.frgis.model.LGU;
import ph.gov.da.bfar.frgis.repository.LGURepo;


@Service("LGUService")
@Transactional
public class LGUServiceImpl implements LGUService{

	@Autowired
	private LGURepo dao;

	
	public Optional<LGU> findById(int id) {
		return dao.findById(id);
	}

	public List<LGU> ListByUsername(String username) {
		List<LGU>LGU = dao.getListLGUByUsername(username);
		return LGU;
	}

	public void saveLGU(LGU LGU) {
		dao.save(LGU);
	}

	@Override
	public void deleteLGUById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateByEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateLGUByReason(int id, String reason) {
		dao.updateLGUByReason(id, reason);
		
	}

	public List<LGU> findAllLGUs() {
		return dao.findAll();
	}

	@Override
	public List<LGU> getAllLGUs(int index, int pagesize, String user) {
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<LGU> page = dao.getPageableLGU(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int LGUCount(String username) {
		return dao.LGUCount(username);
	}

	@Override
	public LGU findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<LGU> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<PFOPage> findByUsername(String user, Pageable pageable) {

		return dao.findByUsername(user, pageable);
	}
	
	@Override
	public Page<PFOPage> findAllPFO(Pageable pageable) {

		return dao.findAllPFO(pageable);
	}
	
	@Override
	public Page<PFOPage> findByRegion(String region_id, Pageable pageable) {

		return dao.findByRegion(region_id, pageable);
	}

	@Override
	public List<LGU> ListByRegion(String region_id) {		
		return dao.getListLGUByRegion(region_id);
	}

	@Override
	public Long countLGUPerRegion(String region_id) {
		return dao.countLGUPerRegion(region_id);
	}

	@Override
	public Long countLGUPerProvince(String province_id) {
	
		return dao.countLGUPerProvince(province_id);
	}

	@Override
	public Long countAllLGU() {
	
		return dao.countAllLGU();
	}

	
}
