package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import ph.gov.da.bfar.frgis.Page.FishSanctuaryPage;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;



public interface FishSanctuariesService {
	
	Optional<FishSanctuaries> findById(int id);
	
	FishSanctuaries findByUniqueKey(String uniqueKey);
	
	List<FishSanctuaries> findByUniqueKeyForUpdate(String uniqueKey);
	void findByUniqueKeyForEnabled(int  id);
	void findByUniqueKeyForReason(int  id,String  reason);
	List<FishSanctuaries> getListFishSanctuariesByUsername(String user);
	Page<FishSanctuaryPage> findByUsername(String username, Pageable pageable);
	Page<FishSanctuaryPage> findByRegion(String region,Pageable pageable);
	Page<FishSanctuaryPage> findAllFishSanctuary(Pageable pageable);
    List<FishSanctuaries> findByRegionList(String regionID);
	List<FishSanctuaries> findByProvinceList(String provinceID);
//	List<FishSanctuaries> findByMunicipality(String municipalityID);
	
	void saveFishSanctuaries(FishSanctuaries fishSanctuaries);
	
//	void updateFishSanctuaries(FishSanctuaries fishSanctuaries);
	
		void deleteFishSanctuariesrById(int id);

	List<FishSanctuaries> findAllFishSanctuaries(); 
    public List<FishSanctuaries> getAllFishSanctuaries(int index,int pagesize,String user);
    public int FishSanctuariesCount(String username);
    public Long countAllFishSanctuaries();
    public Long countFishSanctuariesPerRegion(String region_id);
    public Long countFishSanctuariesPerProvince(String province_id);
    public Long countFishSanctuariesPerMunicipal(String municipal_id);


}