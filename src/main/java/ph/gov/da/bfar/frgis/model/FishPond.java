package ph.gov.da.bfar.frgis.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Entity
@Table(name="fishpond")
public class FishPond implements Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String page;
	
	private String user;
	
	@Column(name = "unique_key")
	private String uniqueKey;
	
	private String dataasof;
	
	private String region;
	
	private String region_id;
	
	private String province;
	
	private String province_id;
	
	private String municipality;
	
	private String municipality_id; 
	
	private String barangay;
	
	private String barangay_id;
	
	private String area;
	
	private String nameoffishpondoperator;
	
	private String nameofactualoccupant;
	
	private String typeofwater;
	
	private String fishpond;
	
	private String fishpondtype;
	
	//private String yearissued;
	
	//private String yearexpired;
		
	private String nonactive;
	
	private String speciescultured;
	private String hatchery;
	private String status;
	
	private String kind;
	
	private String datasource;
	
	
	private String code;
	
	private String remarks;
	
	private String encodedby;
	
	private String editedby;

	private String dateencoded;
	
	private String dateedited;
	
	private String lastused;

	
	private String lat;
	
	private String lon;
	
	private String image;
	
	private String image_src;
	
	private boolean enabled;
	private String reason;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getUniqueKey() {
		return uniqueKey;
	}
	public void setUniqueKey(String uniqueKey) {
		this.uniqueKey = uniqueKey;
	}
	public String getDataasof() {
		return dataasof;
	}
	public void setDataasof(String dataasof) {
		this.dataasof = dataasof;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getRegion_id() {
		return region_id;
	}
	public void setRegion_id(String region_id) {
		this.region_id = region_id;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getProvince_id() {
		return province_id;
	}
	public void setProvince_id(String province_id) {
		this.province_id = province_id;
	}
	public String getMunicipality() {
		return municipality;
	}
	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}
	public String getMunicipality_id() {
		return municipality_id;
	}
	public void setMunicipality_id(String municipality_id) {
		this.municipality_id = municipality_id;
	}
	public String getBarangay() {
		return barangay;
	}
	public void setBarangay(String barangay) {
		this.barangay = barangay;
	}
	public String getBarangay_id() {
		return barangay_id;
	}
	public void setBarangay_id(String barangay_id) {
		this.barangay_id = barangay_id;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getNameoffishpondoperator() {
		return nameoffishpondoperator;
	}
	public void setNameoffishpondoperator(String nameoffishpondoperator) {
		this.nameoffishpondoperator = nameoffishpondoperator;
	}
	public String getNameofactualoccupant() {
		return nameofactualoccupant;
	}
	public void setNameofactualoccupant(String nameofactualoccupant) {
		this.nameofactualoccupant = nameofactualoccupant;
	}
	public String getTypeofwater() {
		return typeofwater;
	}
	public void setTypeofwater(String typeofwater) {
		this.typeofwater = typeofwater;
	}
	public String getFishpond() {
		return fishpond;
	}
	public void setFishpond(String fishpond) {
		this.fishpond = fishpond;
	}
	public String getFishpondtype() {
		return fishpondtype;
	}
	public void setFishpondtype(String fishpondtype) {
		this.fishpondtype = fishpondtype;
	}
	public String getNonactive() {
		return nonactive;
	}
	public void setNonactive(String nonactive) {
		this.nonactive = nonactive;
	}
	public String getSpeciescultured() {
		return speciescultured;
	}
	public void setSpeciescultured(String speciescultured) {
		this.speciescultured = speciescultured;
	}
	public String getHatchery() {
		return hatchery;
	}
	public void setHatchery(String hatchery) {
		this.hatchery = hatchery;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public String getDatasource() {
		return datasource;
	}
	public void setDatasource(String datasource) {
		this.datasource = datasource;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getEncodedby() {
		return encodedby;
	}
	public void setEncodedby(String encodedby) {
		this.encodedby = encodedby;
	}
	public String getEditedby() {
		return editedby;
	}
	public void setEditedby(String editedby) {
		this.editedby = editedby;
	}
	public String getDateencoded() {
		return dateencoded;
	}
	public void setDateencoded(String dateencoded) {
		this.dateencoded = dateencoded;
	}
	public String getDateedited() {
		return dateedited;
	}
	public void setDateedited(String dateedited) {
		this.dateedited = dateedited;
	}
	public String getLastused() {
		return lastused;
	}
	public void setLastused(String lastused) {
		this.lastused = lastused;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getImage_src() {
		return image_src;
	}
	public void setImage_src(String image_src) {
		this.image_src = image_src;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}	
	
	
}
