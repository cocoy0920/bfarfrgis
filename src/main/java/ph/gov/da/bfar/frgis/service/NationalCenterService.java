package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.NationalCenterPage;
import ph.gov.da.bfar.frgis.model.NationalCenter;




public interface NationalCenterService {
	
	Optional<NationalCenter> findById(int id);
	
	List<NationalCenter> ListByUsername(String region);
	List<NationalCenter> ListByRegion(String region_id);
	NationalCenter findByUniqueKey(String uniqueKey);
	List<NationalCenter> findByUniqueKeyForUpdate(String uniqueKey);
	void saveNationalCenter(NationalCenter nationalCenter);
	void deleteNationalCenterById(int id);
	void updateByEnabled(int id);
	void updateNationalCenterByReason(int  id,String  reason);
	List<NationalCenter> findAllNationalCenter(); 
    public List<NationalCenter> getAllNationalCenter(int index,int pagesize,String user);
    public int NationalCenterCount(String username);
    public Page<NationalCenterPage> findByUsername(@Param("user") String user, Pageable pageable);
    public Page<NationalCenterPage> findByRegion(@Param("region") String region, Pageable pageable);
    public Page<NationalCenterPage> findAllNationalCenter(Pageable pageable);
    public Long countAllNationalCenter();
    public Long countNationalCenterPerRegion(String region_id);
    public Long countNationalCenterProvince(String province_id);

}