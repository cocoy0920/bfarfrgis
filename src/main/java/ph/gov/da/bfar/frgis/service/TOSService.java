package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.TOSPage;
import ph.gov.da.bfar.frgis.model.TOS;
import ph.gov.da.bfar.frgis.model.TOS;




public interface TOSService {
	
	Optional<TOS> findById(int id);
	
	List<TOS> ListByUsername(String region);
	List<TOS> ListByRegion(String region_id);
	TOS findByUniqueKey(String uniqueKey);
	List<TOS> findByUniqueKeyForUpdate(String uniqueKey);
	void saveTOS(TOS TOS);
	void deleteTOSById(int id);
	void updateByEnabled(int id);
	void updateTOSByReason(int  id,String  reason);
	List<TOS> findAllTOS(); 
    public List<TOS> getAllTOS(int index,int pagesize,String user);
    public int TOSCount(String username);
    public Page<TOSPage> findByUsername(@Param("user") String user, Pageable pageable);
    public Page<TOSPage> findByRegion(@Param("region") String region, Pageable pageable);
    public Page<TOSPage> findAllTOS(Pageable pageable);
    public Long countAllTOS();
    public Long countTOSPerRegion(String region_id);
    public Long countTOSProvince(String province_id);

}