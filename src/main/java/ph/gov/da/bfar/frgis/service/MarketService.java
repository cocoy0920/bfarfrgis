package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.MarketPage;
import ph.gov.da.bfar.frgis.model.Market;



public interface MarketService {
	
	Optional<Market> findById(int id);
	
	List<Market> ListByUsername(String username);
	List<Market> ListByRegion(String region_id);
	void saveMarket(Market Market);
	void deleteMarketById(int id);
	void updateByEnabled(int id);
	void updateMarketeByReason(int  id,String  reason);
	Market findByUniqueKey(String uniqueKey);
	List<Market> findByUniqueKeyForUpdate(String uniqueKey);
	
	List<Market> findAllMarkets(); 
    public List<Market> getAllMarkets(int index,int pagesize,String user);
    public int MarketCount(String username);
    public Page<MarketPage> findByUsername(@Param("user") String user, Pageable pageable);
    public Page<MarketPage> findByRegion(@Param("region") String region, Pageable pageable);
    public Page<MarketPage> findAllMarket(Pageable pageable);
    public Long countAllMarket();
    public Long countMarketRegion(String region_id);
    public Long countMarketProvince(String province_id);
    public Long countMarketMunicipality(String municipality_id);
    
}