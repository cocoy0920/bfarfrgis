package ph.gov.da.bfar.frgis.web.util;

import java.util.ArrayList;

import ph.gov.da.bfar.frgis.model.Features;


public interface IResourceMapsData {
	
	public String alltoJson(ArrayList<Features> list);
	public ArrayList<Features> featuresListByRegion(String region);
	public ArrayList<Features> featuresListByRegion(String region,String resources);
	public ArrayList<Features> featuresListByRegion(String region,String resources,String type);
	public ArrayList<Features> featuresListByFishsanctuary(String region);
	public ArrayList<Features> featuresListByFishProcessing(String region);
	public  ArrayList<Features> featuresListAll();
	public  ArrayList<Features> featuresListAll(String resources);
	public  ArrayList<Features> featuresListAll(String resources,String type);
}
		