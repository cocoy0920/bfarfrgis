package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.PFOPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.LGU;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.LGUService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class PFOController{
	
	@Autowired
	UserService userService;

	@Autowired
	LGUService lGUService;
	
	 @Autowired
	 private IAuthenticationFacade authenticationFacade;
		
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;
	
	@RequestMapping(value = "/lgu", method = RequestMethod.GET)
	public ModelAndView lgu(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();
		
		user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "lgu");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("users/form");
		return result;
	}
	
	@RequestMapping(value = "/pfo", method = RequestMethod.GET)
	public ModelAndView pfo(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "lgu");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("users/form");
		return result;
	}
	@RequestMapping(value = "/lguMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			LGU data = lGUService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	
		 	Features features = new Features();
			 features = MapBuilder.getLGUMapData(data);
			 
		 	
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	mapsData.setIcon("/static/images/pin2022/ProvincialFisheriesOffice.png");
		 	
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			
			map_data =jsonString;
		
		result = new ModelAndView("redirect:../pfo");
		
	
		return result;
	}
	@RequestMapping(value = "/lguDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView lguDeleteById(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		// lGUService.deleteLGUById(id);
		 lGUService.updateByEnabled(id);
		result = new ModelAndView("redirect:../pfo");
		
	
		return result;
	}
	@RequestMapping(value = "/lguDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView lguDeleteById(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		// lGUService.deleteLGUById(id);
		 lGUService.updateByEnabled(id);
		 lGUService.updateLGUByReason(id, reason);
		result = new ModelAndView("redirect:../../pfo");
		
	
		return result;
	}

	@RequestMapping(value = "/lguMap", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "lgu");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("users/map/lgumap");
		
	
		return result;
	}


	@RequestMapping(value = "/lgu/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getlgu(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<PFOPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 5, Sort.by("id"));
	      
	          page = lGUService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<PFOPage> LGUList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setLgu(LGUList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}

	@RequestMapping(value = "/saveLGU", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postLGU(@RequestBody LGU lgu) throws JsonGenerationException, JsonMappingException, IOException {

		savingResources(lgu);
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
		
		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);
	}

	@RequestMapping(value = "/getLGU/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getLGUById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<LGU> mapsDatas = lGUService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}
	
	@RequestMapping(value = "/pfoList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getpfoList() throws JsonGenerationException, JsonMappingException, IOException {

		List<LGU> list = null;

		list = lGUService.ListByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}

	private void savingResources(LGU lgu) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		lgu.setRegion_id(region_data[0]);
		lgu.setRegion(region_data[1]);
		
		province = lgu.getProvince();
		data = province.split(",", 2);
		lgu.setProvince_id(data[0]);
		lgu.setProvince(data[1]);

		municipal = lgu.getMunicipality();
		municipal_data = municipal.split(",", 2);
		lgu.setMunicipality_id(municipal_data[0]);
		lgu.setMunicipality(municipal_data[1]);

		barangay = lgu.getBarangay();
		barangay_data = barangay.split(",", 3);
		lgu.setBarangay_id(barangay_data[0]);
		lgu.setBarangay(barangay_data[1]);
		lgu.setEnabled(true);

		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(lgu.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		if (lgu.getId() != 0) {

			Optional<LGU> lgu_image = lGUService.findById(lgu.getId());
			String image_server = lgu_image.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				lgu.setImage("/images/" + image_server);
				newUniqueKeyforMap = lgu.getUniqueKey();
				lGUService.saveLGU(ResourceData.getLGU(lgu, user, "", UPDATE));
				
			}else {
				newUniqueKeyforMap = lgu.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", ""); 
				lgu.setImage("/images/" + imageName);
				Base64Converter.convertToImage(lgu.getImage_src(), lgu.getImage());
				lGUService.saveLGU(ResourceData.getLGU(lgu, user, "", UPDATE));
			}
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", ""); 
			lgu.setImage("/images/" + imageName);
			Base64Converter.convertToImage(lgu.getImage_src(), lgu.getImage());
			lGUService.saveLGU(ResourceData.getLGU(lgu, user, newUniqueKey, SAVE));
			//mapsService.saveMapsData(ResourceMapsData.getMapData(lgu, user, newUniqueKey));

			save = true;

		}

	}
	@RequestMapping(value = "/getLastPageLGU", method = RequestMethod.GET)
	public @ResponseBody String getLastPageLGU(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = lGUService.LGUCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}

	
	
}
