package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.SeaGrassPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.SeaGrass;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.SeaGrassService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class SeaGrassController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	SeaGrassService seaGrassService;
	
	 @Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;
	
	@RequestMapping(value = "/seagrass", method = RequestMethod.GET)
	public ModelAndView seagrass(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		map_data="";
		user = authenticationFacade.getUserInfo();
		
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "seagrass");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}
	@RequestMapping(value = "/seagras", method = RequestMethod.GET)
	public ModelAndView seagras(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "seagrass");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}

	@RequestMapping(value = "/seagrassMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			SeaGrass data = seaGrassService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	Features features = new Features();
			 features = MapBuilder.getSeagrassMapData(data);
			 
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	mapsData.setIcon("/static/images/pin2022/Seagrass.png");
	
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			map_data= jsonString;
		
			result = new ModelAndView("redirect:../seagras");
		
	
		return result;
	}

	@RequestMapping(value = "/seagrassDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView seagrassDeleteById(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		// seaGrassService.deleteSeaGrassById(id);
		 seaGrassService.updateByEnabled(id);
			result = new ModelAndView("redirect:../seagras");
		
	
		return result;
	}
	
	@RequestMapping(value = "/seagrassDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView seagrassDeleteById(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		// seaGrassService.deleteSeaGrassById(id);
		 seaGrassService.updateByEnabled(id);
		 seaGrassService.updateSeaGrassByReason(id, reason);
			result = new ModelAndView("redirect:../../seagras");
		
	
		return result;
	}

	@RequestMapping(value = "/seagrassMap", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "seagrass");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("users/map/seagrassmap");
		
	
		return result;
	}

	@RequestMapping(value = "/seagrass/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSeagrass(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<SeaGrassPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 5, Sort.by("id"));
	      
	          page = seaGrassService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<SeaGrassPage> SeaGrassList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setSeaGrass(SeaGrassList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/seagrassList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getseagrassList() throws JsonGenerationException, JsonMappingException, IOException {

		List<SeaGrass> list = null;

		list = seaGrassService.ListByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	
	@RequestMapping(value = "/getSeagrass/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSeagrassById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<SeaGrass> mapsDatas = seaGrassService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}


	@RequestMapping(value = "/saveSeaGrass", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postSeaGrass(@RequestBody SeaGrass seaGrass) throws JsonGenerationException, JsonMappingException, IOException {

		
		savingResources(seaGrass);
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
	

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);

	}
//	@RequestMapping(value = "/getSeaGrass", method = RequestMethod.GET, produces = "application/json")
//	public @ResponseBody String getSeaGrass(@RequestBody SeaGrass seaGrass) throws JsonGenerationException, JsonMappingException, IOException {
//
//		List<SeaGrass> list = null;
//
//		int pageIndex = 1;
//		int totalNumberOfRecords = 0;
//		int numberOfRecordsPerPage = 20;
//		int page = (pageIndex * numberOfRecordsPerPage) - numberOfRecordsPerPage;
//
//		totalNumberOfRecords = seaGrassService.SeaGrassCount(authenticationFacade.getUsername());
//
//		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
//		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
//			noOfPages = noOfPages + 1;
//		}
//		StringBuilder builder = new StringBuilder();
//		
//		builder.append("<ul class=\"pagination pagination-sm\">");
//
//		for (int i = 1; i <= noOfPages; i++) {
//			builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + i + "</a></li>");
//		}
//		builder.append("</ul>");
//
//		list = seaGrassService.getAllSeaGrasss(page, numberOfRecordsPerPage, authenticationFacade.getUsername());
//		String pageNumber = builder.toString();
//
//		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
//		resourcesPaginations.setSeaGrass(list);
//		resourcesPaginations.setPageNumber(pageNumber);
//
//		ObjectMapper objectMapper = new ObjectMapper();
//
//		return objectMapper.writeValueAsString(resourcesPaginations);
//	}

	private void savingResources(SeaGrass seaGrass) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		seaGrass.setRegion_id(region_data[0]);
		seaGrass.setRegion(region_data[1]);

		province = seaGrass.getProvince();
		data = province.split(",", 2);
		seaGrass.setProvince_id(data[0]);
		seaGrass.setProvince(data[1]);

		municipal = seaGrass.getMunicipality();
		municipal_data = municipal.split(",", 2);
		seaGrass.setMunicipality_id(municipal_data[0]);
		seaGrass.setMunicipality(municipal_data[1]);
		seaGrass.setEnabled(true);

		barangay = seaGrass.getBarangay();
		barangay_data = barangay.split(",", 3);
		seaGrass.setBarangay_id(barangay_data[0]);
		seaGrass.setBarangay(barangay_data[1]);

		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(seaGrass.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		if (seaGrass.getId() != 0) {
			Optional<SeaGrass> grass_image = seaGrassService.findById(seaGrass.getId());
			String image_server = grass_image.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				newUniqueKeyforMap = seaGrass.getUniqueKey();
				seaGrass.setImage("/images/" + image_server);
				seaGrassService.saveSeaGrass(ResourceData.getSeaGrass(seaGrass, user, newUniqueKey, UPDATE));
			}else {
				newUniqueKeyforMap = seaGrass.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", ""); 
				seaGrass.setImage("/images/" + imageName);
				Base64Converter.convertToImage(seaGrass.getImage_src(), seaGrass.getImage());
				seaGrassService.saveSeaGrass(ResourceData.getSeaGrass(seaGrass, user, newUniqueKey, UPDATE));
			}
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", ""); 
			seaGrass.setImage("/images/" + imageName);
			Base64Converter.convertToImage(seaGrass.getImage_src(), seaGrass.getImage());
			seaGrassService.saveSeaGrass(ResourceData.getSeaGrass(seaGrass, user, newUniqueKey, SAVE));
			save = true;

		}

	}
	@RequestMapping(value = "/getLastPageSeagrass", method = RequestMethod.GET)
	public @ResponseBody String getLastPageSeagrass(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = seaGrassService.SeaGrassCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}


	
}
