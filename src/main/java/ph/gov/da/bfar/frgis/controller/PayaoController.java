package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.PayaoPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.Payao;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.PayaoService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class PayaoController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	PayaoService payaoService;
	
	 @Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;	
	
	@RequestMapping(value = "/payao", method = RequestMethod.GET)
	public ModelAndView payao(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();
		
		user = authenticationFacade.getUserInfo();

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "payao");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}
	@RequestMapping(value = "/payaos", method = RequestMethod.GET)
	public ModelAndView payaos(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "payao");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}

	@RequestMapping(value = "/payaoMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView regionalofficeMap(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
		 Payao data = payaoService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	MapsData  mapsData = new MapsData();
		 	
			Features features = new Features();
			 features = MapBuilder.getPayaoMapData(data);
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUnique_key());
		 	mapsData.setIcon("/static/images/pin2022/Payao.png");
		 //	mapsData.setImage(data.getImage_src());
//		 	String html = "<p>Location: " + data.getProvince() + "," + data.getMunicipality() +
//		 					"," + data.getBarangay()+"<br>" +		 					
//		 					//"Area: " + data.getArea() + "<br>" +
//		 					"Coordinates: " + data.getLat() + "," + data.getLon();
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
//			User user = new User();		 
//			user = authenticationFacade.getUserInfo();
//			model.addAttribute("user", user);
//			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
//			model.addAttribute("lat", data.getLat());
//			model.addAttribute("lon", data.getLon());
//			model.addAttribute("mapsData", jsonString);
//			
		 	map_data= jsonString;
		
		result = new ModelAndView("redirect:../payaos");
	
		return result;
	}
	@RequestMapping(value = "/payaoDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView payaoDeleteById(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		// Optional<FishSanctuaries> optional =  fishSanctuariesService.findById(id);
		// optional.forEach(s -> System.out.println(s));
		// System.out.println("DATA:  " + optional.);
		// fishSanctuariesService.deleteFishSanctuariesrById(id);
		 payaoService.updateByEnabled(id);
			
		result = new ModelAndView("redirect:../payaos");
		
	
		return result;
	}
	@RequestMapping(value = "/payaoDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView payaoDeleteById(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 payaoService.updateByEnabled(id);
		payaoService.updatePayaoByReason(id, reason);
		result = new ModelAndView("redirect:../../payaos");
		
	
		return result;
	}
	
//	@RequestMapping(value = "/payaoMap", method = RequestMethod.GET)
//	public ModelAndView fishCageMAP(ModelMap model)
//			throws JsonGenerationException, JsonMappingException, IOException {
//
//		 ModelAndView result;
//		 
//		 ObjectMapper mapper = new ObjectMapper();
//			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "seagrass");
//			String jsonString = mapper.writeValueAsString(mapsDatas);
//
//			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
//			//model.addAttribute("page", "home_map");
//			model.addAttribute("mapsData", jsonString);
//		
//		result = new ModelAndView("users/map/payaomap");
//		
//	
//		return result;
//	}

	@RequestMapping(value = "/payao/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getRegionaloffice(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<PayaoPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 5, Sort.by("id"));
	      
	          page = payaoService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<PayaoPage> PayaoList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				if(number == y) {
					String numberColor = "<span class=\"create btn-primary\">" + y + "</span>";
					builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + numberColor + "</a></li>");
				}else {
					builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
				}
				
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setPayao(PayaoList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/payaoList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getpayaoList() throws JsonGenerationException, JsonMappingException, IOException {

		List<Payao> list = null;

		list = payaoService.ListByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	
	@RequestMapping(value = "/getPayao/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getRegionalofficeById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<Payao> mapsDatas = payaoService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}


	@RequestMapping(value = "/savePayao", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postPayao(@RequestBody Payao payao) throws JsonGenerationException, JsonMappingException, IOException {

		
		savingResources(payao);
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
	

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);

	}

	private void savingResources(Payao payao) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		payao.setRegion_id(region_data[0]);
		payao.setRegion(region_data[1]);

		province = payao.getProvince();
		data = province.split(",", 2);
		payao.setProvince_id(data[0]);
		payao.setProvince(data[1]);

		municipal = payao.getMunicipality();
		municipal_data = municipal.split(",", 2);
		payao.setMunicipality_id(municipal_data[0]);
		payao.setMunicipality(municipal_data[1]);

		barangay = payao.getBarangay();
		barangay_data = barangay.split(",", 3);
		payao.setBarangay_id(barangay_data[0]);
		payao.setBarangay(barangay_data[1]);
		payao.setEnabled(true);

		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(payao.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		if (payao.getId() != 0) {
			Optional<Payao> payao_image = payaoService.findById(payao.getId());
			String image_server = payao_image.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				payao.setImage("/images/" + image_server);
				newUniqueKeyforMap = payao.getUnique_key();
				payaoService.savePayao(ResourceData.getPayao(payao, user, newUniqueKey, UPDATE));
			}else {
				newUniqueKeyforMap = payao.getUnique_key();
				imageName.replaceAll("[^a-zA-Z0-9]", "");  
				payao.setImage("/images/" + imageName);
				Base64Converter.convertToImage(payao.getImage_src(), payao.getImage());
				payaoService.savePayao(ResourceData.getPayao(payao, user, newUniqueKey, UPDATE));
			}
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", "");  
			payao.setImage("/images/" + imageName);
			Base64Converter.convertToImage(payao.getImage_src(), payao.getImage());
			payaoService.savePayao(ResourceData.getPayao(payao, user, newUniqueKey, SAVE));
			save = true;

		}

	}


	
}
