package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.PayaoPage;
import ph.gov.da.bfar.frgis.model.Payao;

@Repository
public interface PayaoRepo extends JpaRepository<Payao, Integer> {

	@Query("SELECT c from Payao c where c.user = :username")
	public List<Payao> getListPayaoByUsername(@Param("username") String username);
	
	@Query("SELECT c from Payao c where c.region_id = :region_id")
	public List<Payao> getListPayaoByRegion(@Param("region_id") String region_id);

	@Modifying(clearAutomatically = true)
	@Query("update Payao f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update Payao f set f.reason =:reason  where f.id =:id")
	public void updatePayaoByReason( @Param("id") int id, @Param("reason") String reason);
	
	@Query("SELECT COUNT(f) FROM Payao f WHERE f.user=:username")
	public int  PayaoCount(@Param("username") String username);
	
	@Query("SELECT SUM(f.payao) FROM Payao f WHERE f.region_id=:region_id")
	public int PayaoSumByRegion(@Param("region_id") String region_id);
	
	@Query("SELECT SUM(f.payao) FROM Payao f")
	public int PayaoSumAll();
	
	@Query("SELECT c from Payao c where c.unique_key = :uniqueKey")
	public Payao findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from Payao c where c.unique_key = :uniqueKey")
	List<Payao> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from Payao c where c.user = :user")
	 public List<Payao> getPageablePayao(@Param("user") String user,Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.PayaoPage ( c.id,c.unique_key,c.region,c.province,c.municipality,c.barangay,c.association,c.lat,c.lon,c.enabled) from Payao c where c.user = :user")
	public Page<PayaoPage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.PayaoPage ( c.id,c.unique_key,c.region,c.province,c.municipality,c.barangay,c.association,c.lat,c.lon,c.enabled) from Payao c where c.region_id = :region_id")
	public Page<PayaoPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.PayaoPage ( c.id,c.unique_key,c.region,c.province,c.municipality,c.barangay,c.association,c.lat,c.lon,c.enabled) from Payao c")
	public Page<PayaoPage> findAllPayao(Pageable pageable);

	@Query("SELECT COUNT(f) FROM Payao f")
	public Long countAllPayao();
	
	@Query("SELECT COUNT(f) FROM Payao f WHERE f.region_id=:region_id")
	public Long countPayaoPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM Payao f WHERE f.province_id=:province_id")
	public Long countPayaoPerProvince(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM Payao f WHERE f.municipality_id=:municipality_id")
	public Long countPayaoPerMunicipality(@Param("municipality_id") String municipality_id);

}
