package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.TrainingPage;
import ph.gov.da.bfar.frgis.model.TrainingCenter;

@Repository
public interface TrainingCenterRepo extends JpaRepository<TrainingCenter, Integer> {

	@Query("SELECT c from TrainingCenter c where c.user = :username")
	public List<TrainingCenter> getListTrainingCenterByUsername(@Param("username") String username);
	
	@Query("SELECT c from TrainingCenter c where c.region_id = :region_id")
	public List<TrainingCenter> getListTrainingCenterByRegion(@Param("region_id") String region_id);

	@Modifying(clearAutomatically = true)
	@Query("update TrainingCenter f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update TrainingCenter f set f.reason =:reason  where f.id =:id")
	public void updateTrainingCenterByReason( @Param("id") int id, @Param("reason") String reason);
	
	@Query("SELECT COUNT(f) FROM TrainingCenter f WHERE f.user=:username")
	public int  TrainingCenterCount(@Param("username") String username);
	
	@Query("SELECT c from TrainingCenter c where c.uniqueKey = :uniqueKey")
	public TrainingCenter findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from TrainingCenter c where c.uniqueKey = :uniqueKey")
	List<TrainingCenter> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from TrainingCenter c where c.user = :user")
	 public List<TrainingCenter> getPageableTrainingCenter(@Param("user") String user,Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.TrainingPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.name,c.lat,c.lon,c.enabled) from TrainingCenter c where c.user = :user")
	public Page<TrainingPage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.TrainingPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.name,c.lat,c.lon,c.enabled) from TrainingCenter c where c.region_id = :region_id")
	public Page<TrainingPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.TrainingPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.name,c.lat,c.lon,c.enabled) from TrainingCenter c")
	public Page<TrainingPage> findAllTraining(Pageable pageable);

	@Query("SELECT COUNT(f) FROM TrainingCenter f")
	public Long countAllTrainingCenter();
	
	@Query("SELECT COUNT(f) FROM TrainingCenter f WHERE f.region_id=:region_id")
	public Long countTrainingCenterPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM TrainingCenter f WHERE f.province_id=:province_id")
	public Long countTrainingCenterPerProvince(@Param("province_id") String province_id);
}
