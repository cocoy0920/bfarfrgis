package ph.gov.da.bfar.frgis.web.util;

import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.FishCoral;
import ph.gov.da.bfar.frgis.model.FishLanding;
import ph.gov.da.bfar.frgis.model.FishPen;
import ph.gov.da.bfar.frgis.model.FishPond;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.Fishport;
import ph.gov.da.bfar.frgis.model.Hatchery;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;
import ph.gov.da.bfar.frgis.model.LGU;
import ph.gov.da.bfar.frgis.model.Lambaklad;
import ph.gov.da.bfar.frgis.model.Mangrove;
import ph.gov.da.bfar.frgis.model.MaricultureZone;
import ph.gov.da.bfar.frgis.model.Market;
import ph.gov.da.bfar.frgis.model.NationalCenter;
import ph.gov.da.bfar.frgis.model.Payao;
import ph.gov.da.bfar.frgis.model.RegionalOffice;
import ph.gov.da.bfar.frgis.model.Resources;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;
import ph.gov.da.bfar.frgis.model.SeaGrass;
import ph.gov.da.bfar.frgis.model.Seaweeds;
import ph.gov.da.bfar.frgis.model.TOS;
import ph.gov.da.bfar.frgis.model.TrainingCenter;

public class MapBuilder {
	
	public  String getResources(Resources frm) {
		StringBuilder builder = new StringBuilder();
		
		if(frm.getPage() == "fishsanctuaries") {
		//	builder =  getFishSanctuary(frm);
		}else if(frm.getPage() == "fishprocessingplants") {
		//	builder =  getFishProcessingPlantMapData(frm);
		}
		return builder.toString();

	}

	
	public static Features getFishSanctuary(FishSanctuaries data) {
		Features features = new Features();
		features.setArea(data.getArea());
		features.setResources("fishsanctuaries");
		features.setRegion(data.getRegion());
		features.setRegion_id(data.getRegion_id());
		features.setProvince(data.getProvince());
		features.setProvince_id(data.getProvince_id());
		features.setMunicipality(data.getMunicipality());
		features.setMunicipality_id(data.getMunicipality_id());
		String html =
				"<div class=\"p-3 mb-2 bg-primary text-white text-center\">FISH SANCTUARY</div>"+
						"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
						"<table>" + 
				"<tr><td>Location:</td> " + 
				"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
				"<tr><td>Name:</td> " + 
				"<td>" +data.getNameOfFishSanctuary() + "</td></tr>" +
				"<tr><td>Implementer:</td> " +						
				"<td>"+data.getBfardenr() + "</td></tr>";
				if(data.getBfardenr().equalsIgnoreCase("BFAR")) {
				html += "<tr><td>Species:</td> " + 
				"<td>"+ data.getSheltered() + "</td></tr>";
				}else if(data.getBfardenr().equalsIgnoreCase("DENR")){
				html +=	"<tr><td>Name of Sensitive Habitat:</td> " + 
							"<td>"+ data.getNameOfSensitiveHabitatMPA() + "</td></tr>" + 
							"<tr><td>Ordinance No.:</td> " + 
							"<td>"+ data.getOrdinanceNo() + "</td></tr>" + 
							"<tr><td>Ordinance Title: </td> " + 
							"<td>"+  data.getOrdinanceTitle()+ "</td></tr>" + 
							"<tr><td>Date Established: </td> " + 
							"<td>"+ data.getDateEstablished()+"</td></tr>";
							
				}
				html += "<tr><td>Area:</td> " + 
				"<td>" + data.getArea() + "</td></tr>" +
				"<tr><td>Type:</td> " + 
				"<td>" + data.getType()+ "</td></tr>" +
				"<tr><td>Coordinates:</td> " +
				"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>";
			 	
	
			features.setInformation(html);
			
		features.setLatitude(data.getLat());
		features.setLongtitude(data.getLon());
		features.setName(data.getNameOfFishSanctuary());
		
		return features;
		
	}

	public static Features getFishCageMapData(FishCage data) {
		Features features = new Features();
		features.setArea(data.getArea());
		features.setResources("fishcage");
		features.setRegion(data.getRegion());
		features.setRegion_id(data.getRegion_id());
		features.setProvince(data.getProvince());
		features.setProvince_id(data.getProvince_id());
		features.setMunicipality(data.getMunicipality());
		features.setMunicipality_id(data.getMunicipality_id());
		//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
		features.setInformation(
				"<div class=\"p-3 mb-2 bg-primary text-white text-center\">FISH CAGE</div>"+
						"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
						"<table>" +  
						"<tr><td>Location:</td> " + 
						"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
						"<tr><td>Name of Operator:</td> " + 
						"<td>" +data.getNameOfOperator() + "</td></tr>" +					
						"<tr><td>Classification:</td> " + 
						"<td>" + data.getClassificationofOperator() + "</td></tr>" +
						"<tr><td>Cage Type:</td> " + 
						"<td>" + data.getCageType()+ "</td></tr>" +
						"<tr><td>Area:</td> " + 
						"<td>" + data.getArea()+ "</td></tr>" +
						"<tr><td>Coordinates:</td> " +
						"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

		features.setLatitude(data.getLat());
		features.setLongtitude(data.getLon());
		
		return features;
	}
	public static Features getFishPondMapData(FishPond data) {
		Features features = new Features();
		features.setArea(data.getArea());
		features.setResources("fishpond");
		features.setRegion(data.getRegion());
		features.setRegion_id(data.getRegion_id());
		features.setProvince(data.getProvince());
		features.setProvince_id(data.getProvince_id());
		features.setMunicipality(data.getMunicipality());
		features.setMunicipality_id(data.getMunicipality_id());
		String html =
				"<div class=\"p-3 mb-2 bg-primary text-white text-center\">FISHPOND</div>"+
						"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
						"<table>" + 
				"<tr><td>Location:</td> " + 
				"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
				"<tr><td>Name:</td> " + 
				"<td>" +data.getNameoffishpondoperator() + "</td></tr>" +
				"<tr><td>Name Actual Occupant:</td> " +						
				"<td>"+data.getNameofactualoccupant() + "</td></tr>"+
				"<tr><td>Area:</td> " + 
				"<td>" + data.getArea() + "</td></tr>" +
				"<tr><td>Hatchery:</td> " + 
				"<td>" + data.getHatchery()+ "</td></tr>" +
				"<tr><td>Species Cultured:</td> " + 
				"<td>" + data.getSpeciescultured()+ "</td></tr>" +
				"<tr><td>Kind:</td> " + 
				"<td>" + data.getKind()+ "</td></tr>";
				
				if(data.getStatus().equals("Non-Active")) {
					html+= "<tr><td>Status:</td> " + 
							"<td>" + data.getStatus()+ " = " + data.getNonactive() + "</td></tr>";

				}else {
					
					html+="<tr><td>Status:</td> " + 
						"<td>" + data.getStatus()+ "</td></tr>";
					}
				html+=	"<tr><td>Coordinates:</td> " +
				"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>";
		
		features.setInformation(html);
		features.setLatitude(data.getLat());
		features.setLongtitude(data.getLon());
		
		return features;
	}
	
	public static Features getFishCorralMapData(FishCoral data) {
		Features features = new Features();
		features.setArea(data.getArea());
		features.setResources("fishcoral");
		features.setRegion(data.getRegion());
		features.setRegion_id(data.getRegion_id());
		features.setProvince(data.getProvince());
		features.setProvince_id(data.getProvince_id());
		features.setMunicipality(data.getMunicipality());
		features.setMunicipality_id(data.getMunicipality_id());
		//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
		features.setInformation(

				"<div class=\"p-3 mb-2 bg-primary text-white text-center\">FISH CORRAL</div>"+
						"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
						"<table>" + 
						"<tr><td>Location:</td> " + 
						"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
						"<tr><td>Name of Operator:</td> " + 
						"<td>" +data.getNameOfOperator() + "</td></tr>" +	
						"<tr><td>TOperator Classification:</td> " + 
						"<td>" + data.getOperatorClassification()+ "</td></tr>" +
						"<tr><td>Gear Use:</td> " + 
						"<td>" + data.getNameOfStationGearUse()+ "</td></tr>" +
						"<tr><td>Fishes Caught:</td> " + 
						"<td>" + data.getFishesCaught()+ "</td></tr>" +
						"<tr><td>Area:</td> " + 
						"<td>" + data.getArea()+ "</td></tr>" +
						"<tr><td>Coordinates:</td> " +
						"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");
		
		features.setLatitude(data.getLat());
		features.setLongtitude(data.getLon());
		return features;
	}
	public static Features getFishLandingMapData(FishLanding data) {
		Features features = new Features();
		features.setArea(data.getArea());
		features.setResources("fishlanding");
		features.setRegion(data.getRegion());
		features.setRegion_id(data.getRegion_id());
		features.setProvince(data.getProvince());
		features.setProvince_id(data.getProvince_id());
		features.setMunicipality(data.getMunicipality());
		features.setMunicipality_id(data.getMunicipality_id());
		//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
		features.setInformation(
				"<div class=\"p-3 mb-2 bg-primary text-white text-center\">FISH LANDING</div>"+
						"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
						"<table>" + 
						"<tr><td>Location:</td> " + 
						"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
						"<tr><td>Name of Fish Landing :</td> " + 
						"<td>" +data.getNameOfLanding() + "</td></tr>" +					
						"<tr><td>Area:</td> " + 
						"<td>" + data.getArea() + "</td></tr>" +
						"<tr><td>Volume of Unloading:</td> " + 
						"<td>" + data.getVolumeOfUnloadingMT()+ "</td></tr>" +
						"<tr><td>Classification:</td> " + 
						"<td>" + data.getClassification()+ "</td></tr>" +
						"<tr><td>Type:</td> " + 
						"<td>" + data.getType()+ "</td></tr>" +
						"<tr><td>CFLC Status:</td> " + 
						"<td>" + data.getCflc_status()+ "</td></tr>" +
						"<tr><td>Coordinates:</td> " +
						"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");
		if(data.getCflc_status()!=null) {
			features.setCflcStatus(data.getCflc_status().replace(" ", ""));	
		}
		
		features.setType(data.getType());
		features.setLatitude(data.getLat());
		features.setLongtitude(data.getLon());
		features.setName(data.getNameOfLanding());
		
		return features;
	}
	public static Features getFishPenMapData(FishPen data) {
		Features features = new Features();
		features.setArea(data.getArea());
		features.setResources("fishpen");
		features.setRegion(data.getRegion());
		features.setRegion_id(data.getRegion_id());
		features.setProvince(data.getProvince());
		features.setProvince_id(data.getProvince_id());
		features.setMunicipality(data.getMunicipality());
		features.setMunicipality_id(data.getMunicipality_id());
		//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
		features.setInformation(
				"<div class=\"p-3 mb-2 bg-primary text-white text-center\">FISH PEN</div>"+
						"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
						"<table>" +  
						"<tr><td>Location:</td> " + 
						"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
						"<tr><td>Name of Operator:</td> " + 
						"<td>" +data.getNameOfOperator() + "</td></tr>" +					
						"<tr><td>Number of Fish Pen:</td> " + 
						"<td>" + data.getNoOfFishPen() + "</td></tr>" +
						"<tr><td>Species:</td> " + 
						"<td>" + data.getSpeciesCultured()+ "</td></tr>" +
						"<tr><td>Area:</td> " + 
						"<td>" + data.getArea()+ "</td></tr>" +
						"<tr><td>Coordinates:</td> " +
						"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

		features.setLatitude(data.getLat());
		features.setLongtitude(data.getLon());
		
		return features;
	}
	
	public static Features getFishProcessingPlantMapData(FishProcessingPlant data) {
	 
	Features features = new Features();
	features.setArea(data.getArea());
	features.setResources("fishprocessingplants");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	features.setInformation(
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">FISH PROCESSING PLANT</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table>" +  
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Name:</td> " + 
					"<td>" +data.getNameOfProcessingPlants() + "</td></tr>" +
					"<tr><td>Name of Operator:</td> " + 
					"<td>"+data.getNameOfOperator() + "</td></tr>" + 					
					"<tr><td>Area:</td> " + 
					"<td>" + data.getArea() + "</td></tr>" +
					"<tr><td>Classification:</td> " + 
					"<td>" + data.getOperatorClassification()+ "</td></tr>" +
					"<tr><td>Processing Technique:</td> " + 
					"<td>" + data.getProcessingTechnique()+ "</td></tr>" +
					"<tr><td>Processing Environment:</td> " + 
					"<td>" + data.getProcessingEnvironmentClassification()+ "</td></tr>" +
					"<tr><td>BFAR Registered:</td> " + 
					"<td>" + data.getBfarRegistered()+ "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");
			
	features.setOperator(data.getOperatorClassification().replace(" ", ""));
	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	features.setName(data.getNameOfProcessingPlants());
	
	return features;
}
	public static Features getIceColdMapData(IcePlantColdStorage data) {
	Features features = new Features();
	features.setArea(data.getArea());
	features.setResources("coldstorage");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
features.setInformation(
			
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">COLD STORAGE/ICE PALNT</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table>" +  
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Name of Ice Plant or Cold Storage:</td> " + 
					"<td>" +data.getNameOfIcePlant() + "</td></tr>" +					
					"<tr><td>Valid Sanitary Permit:</td> " + 
					"<td>" + data.getWithValidSanitaryPermit() + "</td></tr>" +
					"<tr><td>Operator:</td> " + 
					"<td>" + data.getOperator()+ "</td></tr>" +
					"<tr><td>Type of Facility:</td> " + 
					"<td>" + data.getTypeOfFacility()+ "</td></tr>" +
					"<tr><td>Structure Type:</td> " + 
					"<td>" + data.getStructureType()+ "</td></tr>" +
					"<tr><td>Capacity:</td> " + 
					"<td>" + data.getCapacity()+ "</td></tr>" +
					"<tr><td>With Valid License to Operate:</td> " + 
					"<td>" + data.getWithValidLicenseToOperate()+ "</td></tr>" +
					"<tr><td>With Valid Sanitary Permit:</td> " + 
					"<td>" + data.getWithValidSanitaryPermit()+ "</td></tr>" +
					"<tr><td>Area:</td> " + 
					"<td>" + data.getArea()+ "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

	features.setOperator(data.getOperator());
	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	features.setName(data.getNameOfIcePlant());
	return features;
	}
	public static Features getLGUMapData(LGU data) {
	Features features = new Features();
	//features.setArea(data.getArea());
	features.setResources("lgu");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	features.setInformation(
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">PROVINCIAL FISHERIES OFFICE</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table>" + 
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>PFO name:</td> " + 
					"<td>" +data.getLguName() + "</td></tr>" +
					"<tr><td>Number of Municipality Coastal:</td> " + 
					"<td>" +data.getNoOfBarangayCoastal() + "</td></tr>" +
					"<tr><td>Number of Municipality Inland:</td> " + 
					"<td>" +data.getNoOfBarangayInland() + "</td></tr>" +
					"<tr><td>LGU Coastal Length(km):</td> " + 
					"<td>" +data.getLguCoastalLength() + "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	features.setName(data.getLguName());
	return features;
}
	public static Features getManggroveMapData(Mangrove data) {
	 
	Features features = new Features();
	//features.setArea(data.getArea());
	features.setResources("mangrove");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	features.setInformation(

			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">MANGROVE</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table>" +  
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Indicate Species:</td> " + 
					"<td>" +data.getIndicateSpecies() + "</td></tr>" +
					"<tr><td>Type:</td> " + 
					"<td>" +data.getType() + "</td></tr>" +
					"<tr><td>Area:</td> " + 
					"<td>" + data.getArea()+ "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	features.setName(data.getType());
	return features;
}
	public static Features getMarketMapData(Market data) {
	 
	Features features = new Features();
	features.setArea(data.getArea());
	features.setResources("market");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	features.setInformation(
			
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">MARKET</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table>" + 
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Name of Market:</td> " + 
					"<td>" +data.getNameOfMarket() + "</td></tr>" +	
					"<tr><td>Type of Facility:</td> " + 
					"<td>" + data.getPublicprivate()+ "</td></tr>" +
					"<tr><td>Classification of Market:</td> " + 
					"<td>" + data.getMajorMinorMarket()+ "</td></tr>" +
					"<tr><td>Volume Traded:</td> " + 
					"<td>" + data.getVolumeTraded()+ "</td></tr>" +
					"<tr><td>Area:</td> " + 
					"<td>" + data.getArea()+ "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");
			
	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	features.setName(data.getNameOfMarket());
	
	return features;

}
	public static Features getSchoolOfFisheriesMapData(SchoolOfFisheries data) {
	Features features = new Features();
	features.setArea(data.getArea());
	features.setResources("schoolOfFisheries");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	features.setInformation(
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">FISHERIES SCHOOL</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table>" + 
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Name of School::</td> " + 
					"<td>" +data.getName() + "</td></tr>" +	
					"<tr><td>Date Established:</td> " + 
					"<td>" + data.getDateEstablished()+ "</td></tr>" +
					"<tr><td>Number of Student Enrolled:</td> " + 
					"<td>" + data.getNumberStudentsEnrolled()+ "</td></tr>" +
					"<tr><td>Area:</td> " + 
					"<td>" + data.getArea()+ "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");
	
	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	features.setName(data.getName());
	return features;
}
	public static Features getSeagrassMapData(SeaGrass data) {
	Features features = new Features();
	features.setArea(data.getArea());
	features.setResources("seagrass");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	features.setInformation(
			
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">SEAGRASS</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table>" + 
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Genus Species:</td> " + 
					"<td>" +data.getIndicateGenus() + "</td></tr>" +
					"<tr><td>Area:</td> " + 
					"<td>" + data.getArea()+ "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");
	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	return features;
}
	public static Features getSeaweedsMapData(Seaweeds data) {
	Features features = new Features();
	//features.setArea(data.getArea());
	features.setResources("seaweeds");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	features.setInformation(

			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">SEAWEED</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table>" +  
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Indicate Genus:</td> " + 
					"<td>" +data.getIndicateGenus() + "</td></tr>" +
					"<tr><td>Cultured Method Used:</td> " + 
					"<td>" +data.getCulturedMethodUsed() + "</td></tr>" +
					"<tr><td>Cultured Period:</td> " + 
					"<td>" +data.getCulturedPeriod() + "</td></tr>" +
					"<tr><td>Volume per Cropping (Kgs.):</td> " + 
					"<td>" +data.getProductionKgPerCropping() + "</td></tr>" +
					"<tr><td>No. of Cropping per Year:</td> " + 
					"<td>" +data.getProductionNoOfCroppingPerYear() + "</td></tr>" +
					"<tr><td>Area:</td> " + 
					"<td>" + data.getArea()+ "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");
	if(data.getType()!=null) {
	features.setType(data.getType());
	}
	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	return features;
	
}
	public static Features getTrainingCenterMapData(TrainingCenter data) {
	Features features = new Features();
	//features.setArea(data.getArea());
	features.setResources("trainingcenter");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	features.setInformation(
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">TRAINING CENTER</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table>" + 
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Name of Training Center:</td> " + 
					"<td>" +data.getName() + "</td></tr>" +
					"<tr><td>Specialization:</td> " + 
					"<td>" +data.getSpecialization() + "</td></tr>" +
					"<tr><td>Facility Within The Training Center:</td> " + 
					"<td>" +data.getFacilityWithinTheTrainingCenter() + "</td></tr>" +
					"<tr><td>Type:</td> " + 
					"<td>" +data.getType() + "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	features.setName(data.getName());
	features.setImage(data.getImage());
	return features;
}
	public static Features getHatcheryMapData(Hatchery data) {
	 
	Features features = new Features();
	//features.setArea(data.getArea());
	features.setResources("hatchery");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	features.setInformation(
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">HATCHERY</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table>" + 
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Name of Hatchery:</td> " + 
					"<td>" +data.getNameOfHatchery() + "</td></tr>" +
					"<tr><td>Name of Operator:</td> " + 
					"<td>" +data.getNameOfOperator() + "</td></tr>" +
					"<tr><td>Address of Operator:</td> " + 
					"<td>" +data.getAddressOfOperator() + "</td></tr>" +
					"<tr><td>Type:</td> " + 
					"<td>" +data.getPublicprivate() + "</td></tr>" +
					"<tr><td>Status:</td> " + 
					"<td>" +data.getStatus() + "</td></tr>" +
					"<tr><td>Legislative:</td> " + 
					"<td>" +data.getLegislated() + "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

	features.setOperator(data.getOperatorClassification());
	features.setStatus(data.getStatus());
	features.setLegislated(data.getLegislated());
	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	features.setName(data.getNameOfHatchery());
	return features;
}
	public static Features getMaricultureZoneMapData(MaricultureZone data) {
	Features features = new Features();
	//features.setArea(data.getArea());
	features.setResources("mariculturezone");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	features.setInformation(
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">MARICULTURE PARK</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table>" + 
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Name of Mariculture:</td> " + 
					"<td>" +data.getNameOfMariculture() + "</td></tr>" +
					"<tr><td>Hectare:</td> " + 
					"<td>" +data.getArea() + "</td></tr>" +
					"<tr><td>Mariculture Type:</td> " + 
					"<td>" +data.getMaricultureType() + "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

	features.setType(data.getMaricultureType());
	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	features.setName(data.getNameOfMariculture());
	return features;
}
	public static Features getFishPortMapData(Fishport data) {
	 
	Features features = new Features();
	features.setArea(data.getArea());
	features.setResources("FISHPORT");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	features.setInformation(
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">FISHPORT</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table>" + 
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Name of Fish Port :</td> " + 
					"<td>" +data.getNameOfFishport() + "</td></tr>" +					
					"<tr><td>Operated by:</td> " + 
					"<td>" + data.getNameOfCaretaker() + "</td></tr>" +
					"<tr><td>Type:</td> " + 
					"<td>" + data.getType()+ "</td></tr>" +
					"<tr><td>Classification:</td> " + 
					"<td>" + data.getClassification()+ "</td></tr>" +
					"<tr><td>Type:</td> " + 
					"<td>" + data.getType()+ "</td></tr>" +
					"<tr><td>Volume of Unloading:</td> " + 
					"<td>" + data.getVolumeOfUnloading()+ "</td></tr>" +
					"<tr><td>Area:</td> " + 
					"<td>" + data.getArea()+ "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");
	
	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	features.setName(data.getNameOfFishport());
	
	return features;
}

	public static Features getNationalCenterMapData(NationalCenter data) {
	
	Features features = new Features();
	features.setResources("nationalcenter");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	features.setInformation(
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">NATIONAL CENTER</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table>" + 
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Name of Center:</td> " + 
					"<td>" +data.getName_of_center() + "</td></tr>" +
					"<tr><td>Description:</td> " + 
					"<td>" +data.getDescription() + "</td></tr>" +
					"<tr><td>Center Head:</td> " + 
					"<td>" +data.getCenter_head() + "</td></tr>" +
					"<tr><td>Number of Personnel:</td> " + 
					"<td>" +data.getNumber_of_personnel() + "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");


	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	features.setName(data.getName_of_center());

	return features;
}
	public static Features getRegionalOfficeMapData(RegionalOffice data) {
	
	Features features = new Features();
	features.setResources("regionaloffice");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	features.setInformation(
			 
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">REGIONAL OFFICE</div>"+
			"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
			"<table>" + 
			"<tr><td>Location:</td> " + 
			"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
			"<tr><td>Name of Director:</td> " + 
			"<td>" +data.getHead() + "</td></tr>" +
			"<tr><td>Number of Personnel:</td> " + 
			"<td>" +data.getNumber_of_personnel() + "</td></tr>" +
			"<tr><td>Coordinates:</td> " +
			"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	features.setName(data.getHead());

	return features;
}
	public static Features getTOSMapData(TOS data) {
	
	Features features = new Features();
	features.setResources("tos");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	features.setInformation(
			 
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">TECHNOLOGY OUTREACH STATION</div>"+
			"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
			"<table>" + 
			"<tr><td>Location:</td> " + 
			"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
			"<tr><td>TOS Address:</td> " + 
			"<td>" +data.getAddress() + "</td></tr>" +
			
			"<tr><td>Coordinates:</td> " +
			"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	features.setName(data.getAddress());

	return features;
}
	
	public static Features getPayaoMapData(Payao data) {
		
	Features features = new Features();
	features.setResources("payao");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	features.setInformation(
			 
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">PAYAO</div>"+
			"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
			"<table>" + 
			"<tr><td>Location:</td> " + 
			"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
			//"<tr><td>TOS Address:</td> " + 
			//"<td>" +data.getAddress() + "</td></tr>" +
			
			"<tr><td>Coordinates:</td> " +
			"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	//features.setName(data.getAddress());

	return features;
}
	public static Features getLambakladMapData(Lambaklad data) {
		
	Features features = new Features();
	features.setResources("lambaklad");
	features.setRegion(data.getRegion());
	features.setRegion_id(data.getRegion_id());
	features.setProvince(data.getProvince());
	features.setProvince_id(data.getProvince_id());
	features.setMunicipality(data.getMunicipality());
	features.setMunicipality_id(data.getMunicipality_id());
	features.setInformation(
			 
			"<div class=\"p-3 mb-2 bg-primary text-white text-center\">Lambaklad</div>"+
			"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
			"<table>" + 
			"<tr><td>Location:</td> " + 
			"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
			//"<tr><td>TOS Address:</td> " + 
			//"<td>" +data.getAddress() + "</td></tr>" +
			
			"<tr><td>Coordinates:</td> " +
			"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

	features.setLatitude(data.getLat());
	features.setLongtitude(data.getLon());
	//features.setName(data.getAddress());

	return features;
}

}
