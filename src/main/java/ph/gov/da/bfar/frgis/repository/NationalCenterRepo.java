package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.NationalCenterPage;
import ph.gov.da.bfar.frgis.model.NationalCenter;

@Repository
public interface NationalCenterRepo extends JpaRepository<NationalCenter, Integer> {

	@Query("SELECT c from NationalCenter c where c.user = :username")
	public List<NationalCenter> getListNationalCenterByUsername(@Param("username") String username);
	
	@Query("SELECT c from NationalCenter c where c.region_id = :region_id")
	public List<NationalCenter> getListNationalCenterByRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM NationalCenter f WHERE f.user=:username")
	public int  NationalCenterCount(@Param("username") String username);
	
	@Query("SELECT c from NationalCenter c where c.unique_key = :uniqueKey")
	public NationalCenter findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from NationalCenter c where c.unique_key = :uniqueKey")
	List<NationalCenter> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from NationalCenter c where c.user = :user")
	 public List<NationalCenter> getPageableNationalCenter(@Param("user") String user,Pageable pageable);

	@Modifying(clearAutomatically = true)
	@Query("update NationalCenter f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update NationalCenter f set f.reason =:reason  where f.id =:id")
	public void updateNationalCenterByReason( @Param("id") int id, @Param("reason") String reason);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.NationalCenterPage ( c.id,c.unique_key,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled) from NationalCenter c where c.user = :user")
	public Page<NationalCenterPage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.NationalCenterPage ( c.id,c.unique_key,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled) from NationalCenter c where c.region_id = :region_id")
	public Page<NationalCenterPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.NationalCenterPage ( c.id,c.unique_key,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled) from NationalCenter c")
	public Page<NationalCenterPage> findAllNationalCenter(Pageable pageable);

	@Query("SELECT COUNT(f) FROM NationalCenter f")
	public Long countAllNationalCenter();
	
	@Query("SELECT COUNT(f) FROM NationalCenter f WHERE f.region_id=:region_id")
	public Long countNationalCenterPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM NationalCenter f WHERE f.province_id=:province_id")
	public Long countNationalCenterPerProvince(@Param("province_id") String province_id);

}
