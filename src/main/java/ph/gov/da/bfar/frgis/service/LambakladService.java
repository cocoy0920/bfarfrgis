package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ph.gov.da.bfar.frgis.Page.LambakladPage;
import ph.gov.da.bfar.frgis.model.Lambaklad;



public interface LambakladService {
	
	Optional<Lambaklad> findById(int id);
	
	List<Lambaklad> getListLambakladByUsername(String region);
	
	 List<Lambaklad> getListByRegion(String regionID);
	
	Lambaklad findByUniqueKey(String uniqueKey);
	List<Lambaklad> findByUniqueKeyForUpdate(String uniqueKey);
	
	void saveLambaklad(Lambaklad Lambaklad);
		
	void deleteLambaklad(int id);
	void updateByEnabled(int id);
	void updateLambakladByReason(int  id,String  reason);
	List<Lambaklad> findAllLambaklads(); 
    public List<Lambaklad> getAllLambaklads(int index,int pagesize,String user);
    public int LambakladCount(String username);
    Page<LambakladPage> findByUsername(String user, Pageable pageable);
    Page<LambakladPage> findByRegion(String region, Pageable pageable);
    Page<LambakladPage> findAllLambaklad(Pageable pageable);
    public Long countAllLambaklad();
    public Long countLambakladPerRegion(String region_id);
    public Long countLambakladPerProvince(String province_id);
    public Long countLambakladPerMunicipality(String municipality_id);

}