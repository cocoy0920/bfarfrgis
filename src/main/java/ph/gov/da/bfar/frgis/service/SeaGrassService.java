package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.SeaGrassPage;
import ph.gov.da.bfar.frgis.model.SeaGrass;



public interface SeaGrassService {
	
	Optional<SeaGrass> findById(int id);
	
	List<SeaGrass> ListByUsername(String region);
	List<SeaGrass> ListByRegion(String region_id);
	SeaGrass findByUniqueKey(String uniqueKey);
	List<SeaGrass> findByUniqueKeyForUpdate(String uniqueKey);
	void saveSeaGrass(SeaGrass SeaGrass);
	void deleteSeaGrassById(int id);
	void updateByEnabled(int id);
	void updateSeaGrassByReason(int  id,String  reason);
	List<SeaGrass> findAllSeaGrasss(); 
    public List<SeaGrass> getAllSeaGrasss(int index,int pagesize,String user);
    public int SeaGrassCount(String username);
    public Page<SeaGrassPage> findByUsername(@Param("user") String user, Pageable pageable);
    public Page<SeaGrassPage> findByRegion(@Param("region") String region, Pageable pageable);
    public Page<SeaGrassPage> findAllSeaGrass(Pageable pageable);
    public Long countAllSeaGrass();
    public Long countSeaGrassPerRegion(String region_id);
    public Long countSeaGrassPerProvince(String province_id);
    public Long countSeaGrassPerMunicipality(String municipality_id);

}