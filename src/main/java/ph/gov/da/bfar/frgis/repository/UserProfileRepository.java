package ph.gov.da.bfar.frgis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.model.UserProfile;

@Repository
public interface UserProfileRepository extends JpaRepository<UserProfile,Integer>{

	
	@Query("SELECT u from UserProfile u Where u.type = :type")
	UserProfile findByType(@Param("type") String type);

}
