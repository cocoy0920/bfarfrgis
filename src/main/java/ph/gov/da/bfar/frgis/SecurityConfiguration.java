package ph.gov.da.bfar.frgis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	

	@Autowired
	@Qualifier("customSuccessHandler")
	CustomSuccessHandler customSuccessHandler;
	
//	@Autowired
//	private DataSource dataSource;

//	@Value("${spring.queries.users-query}")
//	private String usersQuery;
//
//	@Value("${spring.queries.roles-query}")
//	private String rolesQuery;

	@Autowired
	@Qualifier("customUserDetailsService")
	UserDetailsService userDetailsService;
	
//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.jdbcAuthentication().usersByUsernameQuery(usersQuery).authoritiesByUsernameQuery(rolesQuery)
//				.dataSource(dataSource).passwordEncoder(bCryptPasswordEncoder);
//	}
//	
	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
		auth.authenticationProvider(authenticationProvider());
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(userDetailsService);
		authenticationProvider.setPasswordEncoder(bCryptPasswordEncoder);
		return authenticationProvider;
	}


	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests()
				// URLs matching for access rights
				.antMatchers("/").permitAll()
				.antMatchers("/login").permitAll()
				.antMatchers("/register").permitAll()
				
				.antMatchers("/home/**").permitAll()
				.antMatchers("/invasive/**").permitAll()
				.antMatchers("/admin/**").access("hasRole('ROLE_SUPERADMIN')")
				.antMatchers("/report/**").access("hasRole('ROLE_PLANNING')")
				.antMatchers("/dba/**").access("hasRole('ROLE_DBA')")
				.antMatchers("/create/**").access("hasRole('ROLE_USER') or hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REGIONALADMIN') or hasRole('ROLE_REGIONALENCODER')")
				.antMatchers("/excel/**").access("hasRole('ROLE_USER') or hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_REGIONALADMIN') or hasRole('ROLE_REGIONALENCODER')")
				.antMatchers("/region/**").access("hasRole('ROLE_REGIONALADMIN')")
				.antMatchers("/province/**").access("hasRole('ROLE_PFOADMIN')")
				.anyRequest().authenticated()
				.and()
				// form login
				.csrf().disable().formLogin()
				.loginPage("/login")
				.failureUrl("/login?error=true")
				.successHandler(customSuccessHandler)
				.usernameParameter("username")
				.passwordParameter("password")							
				.and().logout()
	            .logoutSuccessHandler(new CustomLogoutSuccessHandler())
	            .invalidateHttpSession(true)
				.deleteCookies("JSESSIONID");

//	            .and()
//	            .sessionManagement()
//	            .invalidSessionUrl("/login")
//	            .maximumSessions(1)
//	            .expiredUrl("/login?expired=true")
//	            .maxSessionsPreventsLogin(true)
//	            .sessionRegistry(sessionRegistry());
			            
	}
	
//	@Bean
//	public SessionRegistry sessionRegistry() {
//	    SessionRegistry sessionRegistry = new SessionRegistryImpl();
//	    return sessionRegistry;
//	}
//	
//	@Bean
//	public static ServletListenerRegistrationBean httpSessionEventPublisher() {
//	    return new ServletListenerRegistrationBean(new HttpSessionEventPublisher());
//	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**");
	}
	


}
