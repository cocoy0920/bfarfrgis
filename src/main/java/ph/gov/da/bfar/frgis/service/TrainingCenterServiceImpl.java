package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.TrainingPage;
import ph.gov.da.bfar.frgis.model.TrainingCenter;
import ph.gov.da.bfar.frgis.repository.TrainingCenterRepo;


@Service("TrainingCenterService")
@Transactional
public class TrainingCenterServiceImpl implements TrainingCenterService{

	@Autowired
	private TrainingCenterRepo dao;

	
	public Optional<TrainingCenter> findById(int id) {
		return dao.findById(id);
	}

	public List<TrainingCenter> ListByUsername(String username) {
		List<TrainingCenter>TrainingCenter = dao.getListTrainingCenterByUsername(username);
		return TrainingCenter;
	}

	public void saveTrainingCenter(TrainingCenter TrainingCenter) {
		//user.setPassword(passwordEncoder.encode(user.getPassword()));
		dao.save(TrainingCenter);
	}

	@Override
	public void deleteTrainingCenterById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateByEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateTrainingCenterByReason(int id, String reason) {
		dao.updateTrainingCenterByReason(id, reason);
		
	}

	public List<TrainingCenter> findAllTrainingCenters() {
		return dao.findAll();
	}

	@Override
	public List<TrainingCenter> getAllTrainingCenters(int index, int pagesize, String user) {
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<TrainingCenter> page = dao.getPageableTrainingCenter(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int TrainingCenterCount(String username) {
		return dao.TrainingCenterCount(username);
	}

	@Override
	public TrainingCenter findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<TrainingCenter> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<TrainingPage> findByUsername(String user, Pageable pageable) {
	
		return dao.findByUsername(user, pageable);
	}
	
	@Override
	public Page<TrainingPage> findAllTraining(Pageable pageable) {

		return dao.findAllTraining(pageable);
	}
	
	@Override
	public Page<TrainingPage> findByRegion(String region, Pageable pageable) {

		return dao.findByRegion(region, pageable);
	}
	
	

	@Override
	public List<TrainingCenter> ListByRegion(String region_id) {
		
		return dao.getListTrainingCenterByRegion(region_id);
	}

	@Override
	public Long countTrainingCenterPerRegion(String region_id) {

		return dao.countTrainingCenterPerRegion(region_id);
	}

	@Override
	public Long countTrainingCenterPerProvince(String province_id) {
		
		return dao.countTrainingCenterPerProvince(province_id);
	}

	@Override
	public Long countAllTrainingCenter() {
		return dao.countAllTrainingCenter();
	}

	
}
