package ph.gov.da.bfar.frgis.service;



import java.util.List;


import ph.gov.da.bfar.frgis.model.User;

public interface UserService {

	List<User> findAllUsers();
	User findByUsername(String username);
	List<User> ListfindByUsername(String username);
	public void saveUser(User user);
	public boolean isUserAlreadyPresent(String  username);
}
