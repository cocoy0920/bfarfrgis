package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import ph.gov.da.bfar.frgis.model.UserProfile;


public interface UserProfileService {

	Optional<UserProfile> findById(int id);

	UserProfile findByType(String type);
	
	List<UserProfile> findAll();
	
}
