package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.MariculturePage;
import ph.gov.da.bfar.frgis.model.MaricultureZone;



public interface MaricultureZoneService {
	
	Optional<MaricultureZone> findById(int id);
	
	List<MaricultureZone> ListByUsername(String username);
	
	List<MaricultureZone> ListByRegion(String region_id);
	List<MaricultureZone> ListByRegion(String region_id,String type);
	
	void saveMaricultureZone(MaricultureZone maricultureZone);
	void deleteMaricultureZoneById(int id);
	void updateByEnabled(int id);
	void updateMaricultureZoneByReason(int  id,String  reason);
	List<MaricultureZone> findByUniqueKeyForUpdate(String uniqueKey);
	MaricultureZone findByUniqueKey(String uniqueKey);
	
	List<MaricultureZone> findAllMaricultureZones(); 
	List<MaricultureZone> findAllMaricultureZones(String type); 
    public List<MaricultureZone> getAllMaricultureZones(int index,int pagesize,String user);
    public int MaricultureZoneCount(String username);
    public Page<MariculturePage> findByUsername(@Param("user") String user, Pageable pageable);
    public Page<MariculturePage> findByRegion(@Param("region") String region, Pageable pageable);
    public Page<MariculturePage> findAllMariculture(Pageable pageable);
    public Long countAllMaricultureZone();
    public Long countMariculturePerRegion(String region_id); 
    public Long countMariculturePerRegion(String region_id,String type);
    public Long countMariculturePerProvince(String province_id);
    public Long countMariculturePerProvince(String province_id,String type);
    public Long countMariculturePerMunicipality(String municipality_id);
    public Long countMariculturePerMunicipality(String municipality_id,String type);
   
}