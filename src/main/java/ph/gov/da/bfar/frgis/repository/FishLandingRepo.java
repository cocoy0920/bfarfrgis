package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.FishLandingPage;
import ph.gov.da.bfar.frgis.model.FishLanding;

@Repository
public interface FishLandingRepo extends JpaRepository<FishLanding, Integer> {

	@Query("SELECT c from FishLanding c where c.user = :username")
	public List<FishLanding> getListFishLandingByUsername(@Param("username") String username);
	
	@Query("SELECT COUNT(f) FROM FishLanding f WHERE f.user=:username")
	public int  FishLandingCount(@Param("username") String username);
	
	@Query("SELECT c from FishLanding c where c.uniqueKey = :uniqueKey")
	public FishLanding findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Modifying(clearAutomatically = true)
	@Query("update FishLanding f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update FishLanding f set f.reason =:reason  where f.id =:id")
	public void updateFishLandingByReason( @Param("id") int id, @Param("reason") String reason);
	
	
	@Query("SELECT c from FishLanding c where c.uniqueKey = :uniqueKey")
	List<FishLanding> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from FishLanding c where c.cflc_status = :cflc_status")
	List<FishLanding> findAllByType(@Param("cflc_status") String cflc_status);
	
	@Query("SELECT c from FishLanding c where c.user = :username")
	 public List<FishLanding> getPageableFishLandings(@Param("username") String username,Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishLandingPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfLanding,c.lat,c.lon,c.enabled) from FishLanding c where c.user = :user")
	public Page<FishLandingPage> findByUsername(@Param("user") String user, Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishLandingPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfLanding,c.lat,c.lon,c.enabled) from FishLanding c where c.region_id = :region_id")
	public Page<FishLandingPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishLandingPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfLanding,c.lat,c.lon,c.enabled) from FishLanding c")
	public Page<FishLandingPage> findAllFishLanding(Pageable pageable);

	@Query("SELECT c from FishLanding c where c.region_id = :region_id")
	 public List<FishLanding> findByRegion(@Param("region_id") String region_id);
	
	@Query("SELECT c from FishLanding c where c.region_id = :region_id and c.cflc_status = :cflc_status")
	 public List<FishLanding> findByRegion(@Param("region_id") String region_id,@Param("cflc_status") String cflc_status);
	
	@Query("SELECT COUNT(f) FROM FishLanding f")
	public Long countAllFishLanding();

	@Query("SELECT COUNT(f) FROM FishLanding f WHERE f.region_id=:region_id")
	public Long countFishLandingPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM FishLanding f WHERE f.province_id=:province_id")
	public Long countFishLandingPerProvince(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM FishLanding f WHERE f.municipality_id=:municipality_id")
	public Long countFishLandingPerMunicipality(@Param("municipality_id") String municipality_id);
	
	@Query("SELECT COUNT(f) FROM FishLanding f WHERE f.region_id=:region_id and f.type=:type")
	public Long countFishLandingPerRegion(@Param("region_id") String region_id, @Param("type") String type);
	
	@Query("SELECT COUNT(f) FROM FishLanding f WHERE f.province_id=:province_id and f.type=:type")
	public Long countFishLandingPerProvince(@Param("province_id") String province_id, @Param("type") String type);
	
	@Query("SELECT COUNT(f) FROM FishLanding f WHERE f.municipality_id=:municipality_id and f.type=:type")
	public Long countFishLandingPerMunicipality(@Param("municipality_id") String municipality_id, @Param("type") String type);
	
	@Query("SELECT COUNT(f) FROM FishLanding f WHERE f.region_id=:region_id and f.type=:type and f.cflc_status=:cflc_status")
	public Long countFishLandingPerRegion(@Param("region_id") String region_id, @Param("type") String type, @Param("cflc_status") String cflc_status);
	
	@Query("SELECT COUNT(f) FROM FishLanding f WHERE f.province_id=:province_id and f.type=:type and f.cflc_status=:cflc_status")
	public Long countFishLandingPerProvince(@Param("province_id") String province_id, @Param("type") String type, @Param("cflc_status") String cflc_status);
	
	@Query("SELECT COUNT(f) FROM FishLanding f WHERE f.municipality_id=:municipality_id and f.type=:type and f.cflc_status=:cflc_status")
	public Long countFishLandingPerMunicipality(@Param("municipality_id") String municipality_id, @Param("type") String type, @Param("cflc_status") String cflc_status);
}
