package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ph.gov.da.bfar.frgis.Page.FishCoralPage;
import ph.gov.da.bfar.frgis.model.FishCoral;



public interface FishCorralService {
	
	Optional<FishCoral> findById(int id);
	
	List<FishCoral> ListByUsername(String region);
	List<FishCoral> ListByRegion(String region_id);
	FishCoral findByUniqueKey(String uniqueKey);
	List<FishCoral> findByUniqueKeyForUpdate(String uniqueKey);
	void saveFishCoral(FishCoral FishCoral);
	void UpdateForEnabled(int  id);
	void UpdateForEnabled(int  id,String reason);
	void deleteFishCoral(int id);
	List<FishCoral> findAllFishCorals(); 
    public List<FishCoral> getAllFishCorals(int index,int pagesize,String user);
    public int FishCoralCount(String username);
    Page<FishCoralPage> findByUsername(String user, Pageable pageable);
    Page<FishCoralPage> findByRegion(String region, Pageable pageable);
    Page<FishCoralPage> findAllFishCoral(Pageable pageable);
    Long countAllFishCoral();
    Long countFishCoralPerRegion(String region_id);
    Long countFishCoralPerProvince(String province_id); 
    Long countFishCoralPerMunicipality(String municipality_id); 

}