package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.ColdStoragePage;
import ph.gov.da.bfar.frgis.Page.FishCagePage;
import ph.gov.da.bfar.frgis.Page.FishCoralPage;
import ph.gov.da.bfar.frgis.Page.FishLandingPage;
import ph.gov.da.bfar.frgis.Page.FishPenPage;
import ph.gov.da.bfar.frgis.Page.FishPondPage;
import ph.gov.da.bfar.frgis.Page.FishPortPage;
import ph.gov.da.bfar.frgis.Page.FishProcessingPlantPage;
import ph.gov.da.bfar.frgis.Page.FishSanctuaryPage;
import ph.gov.da.bfar.frgis.Page.HatcheryPage;
import ph.gov.da.bfar.frgis.Page.MangrovePage;
import ph.gov.da.bfar.frgis.Page.MariculturePage;
import ph.gov.da.bfar.frgis.Page.MarketPage;
import ph.gov.da.bfar.frgis.Page.NationalCenterPage;
import ph.gov.da.bfar.frgis.Page.PFOPage;
import ph.gov.da.bfar.frgis.Page.PayaoPage;
import ph.gov.da.bfar.frgis.Page.RegionalOfficePage;
import ph.gov.da.bfar.frgis.Page.SchoolPage;
import ph.gov.da.bfar.frgis.Page.SeaGrassPage;
import ph.gov.da.bfar.frgis.Page.SeaWeedsPage;
import ph.gov.da.bfar.frgis.Page.TOSPage;
import ph.gov.da.bfar.frgis.Page.TrainingPage;
import ph.gov.da.bfar.frgis.model.Chart;
import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.FishCoral;
import ph.gov.da.bfar.frgis.model.FishLanding;
import ph.gov.da.bfar.frgis.model.FishPen;
import ph.gov.da.bfar.frgis.model.FishPond;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.Fishport;
import ph.gov.da.bfar.frgis.model.Hatchery;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;
import ph.gov.da.bfar.frgis.model.LGU;
import ph.gov.da.bfar.frgis.model.Mangrove;
import ph.gov.da.bfar.frgis.model.MaricultureZone;
import ph.gov.da.bfar.frgis.model.Market;
import ph.gov.da.bfar.frgis.model.NationalCenter;
import ph.gov.da.bfar.frgis.model.Payao;
import ph.gov.da.bfar.frgis.model.Provinces;
import ph.gov.da.bfar.frgis.model.RegionalOffice;
import ph.gov.da.bfar.frgis.model.Regions;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;
import ph.gov.da.bfar.frgis.model.SeaGrass;
import ph.gov.da.bfar.frgis.model.Seaweeds;
import ph.gov.da.bfar.frgis.model.TOS;
import ph.gov.da.bfar.frgis.model.TrainingCenter;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.service.ProvinceService;
import ph.gov.da.bfar.frgis.service.RegionsService;
import ph.gov.da.bfar.frgis.service.UserService;

@Controller
@RequestMapping("/excel")
public class ExcelController extends SuperAdminController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	RegionsService regionsService;
	
	@Autowired
	ProvinceService provinceService;

	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	private String provinceName="";
	private String provinceID="";
	private String regionID = "";
	private String regionName= "";
	

	@RequestMapping(value = "/getAllfishsanctuariesList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getfishsanctuariesList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishSanctuaries> list = null;

		list = fishSanctuariesService.findByRegionList(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllfishcageList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getfishCageList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishCage> list = null;

		list = fishCageService.getListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllfishcoralList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllfishcoralList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishCoral> list = null;
		
		list = fishCorralService.ListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllfishlandingList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllfishlandingList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishLanding> list = null;
		
		list = fishLandingService.findByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllfishpenList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllfishpenList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishPen> list = null;
		
		list = fishPenService.findByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllfishpondList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllfishpondList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishPond> list = null;
		
		list = fishPondService.findByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllfishportList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllfishportList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<Fishport> list = null;
		
		list = fishPortService.findByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllfishprocessingList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllfishprocessingList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishProcessingPlant> list = null;
		
		list = fishProcessingPlantService.ListByRegionList(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllhatcheryList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllhatcheryList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<Hatchery> list = null;

		list = hatcheryService.findByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllIPCSList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllIPCSList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<IcePlantColdStorage> list = null;

		list = icePlantColdStorageService.ListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAlllguList/", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAlllguList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<LGU> list = null;

		list = lGUService.ListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllmangroveList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllmangroveList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<Mangrove> list = null;

		list = mangroveService.ListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllmariculturezoneList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllmariculturezoneList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<MaricultureZone> list = null;

		list = zoneService.ListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllmarketList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllmarketList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<Market> list = null;

		list = marketService.ListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllNationalCenterList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllNationalCenterList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<NationalCenter> list = null;

		list = nationalCenterService.ListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllPayaoList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllPayaoList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<Payao> list = null;

		list = payaoService.ListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllRegionalList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllRegionalList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<RegionalOffice> list = null;

		list = regionalOfficeService.ListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllSchooloffisheriesList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllSchooloffisheriesList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<SchoolOfFisheries> list = null;

		list = schoolOfFisheriesService.ListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllTOSList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllTOSList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<TOS> list = null;

		list = tosService.ListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllSeagrassList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllSeagrassList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<SeaGrass> list = null;

		list = seaGrassService.ListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllSeaweedsList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllSeaweedsList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<Seaweeds> list = null;
		
		list = seaweedsService.ListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@RequestMapping(value = "/getAllTrainingcenterList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllTrainingcenterList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<TrainingCenter> list = null;

		list = trainingCenterService.ListByRegion(authenticationFacade.getRegionID());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	
	@RequestMapping(value = "/fishsanctuaries_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishsanctuariesForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishSanctuaryPage> page = null;
		 List<FishSanctuaryPage> FishSanctuariesList=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      
	   
	    	  page = fishSanctuariesService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          FishSanctuariesList = page.getContent();
	         
	          pageable = page.nextPageable();  
	      
	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishSanctuaries(FishSanctuariesList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}	
	@RequestMapping(value = "/fishcage_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishcageForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishCagePage> page = null;
		 List<FishCagePage> FishCageList=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      

	    	  page = fishCageService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          FishCageList = page.getContent();
	       
	          pageable = page.nextPageable();  

	         

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishCagePage(FishCageList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/fishcoral_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishcoralForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishCoralPage> page = null;
		 List<FishCoralPage> FishCoralList=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id,20, Sort.by("id"));
	      

	    	  page = fishCorralService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          FishCoralList = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishCoral(FishCoralList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/fishlanding_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishlandingForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

	
		 Page<FishLandingPage> page = null;
		 List<FishLandingPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("region_id"));
	      

	    	  page = fishLandingService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishLanding(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/fishpen_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishPenForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishPenPage> page = null;
		 List<FishPenPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));

	    	  page = fishPenService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	
	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishPen(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/fishpond_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishPondForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishPondPage> page = null;
		 List<FishPondPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));

	    	  page = fishPondService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	
	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishPond(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/fishport_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishPortForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishPortPage> page = null;
		 List<FishPortPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));

	    	  page = fishPortService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishPort(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/fishprocessing_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishProcessingForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishProcessingPlantPage> page = null;
		 List<FishProcessingPlantPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      

	    	  page = fishProcessingPlantService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishProcessingPlantPage(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/hatchery_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getHatcheryForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<HatcheryPage> page = null;
		 List<HatcheryPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));

	    	  page = hatcheryService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setHatcheries(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/ipcs_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getIPCSForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<ColdStoragePage> page = null;
		 List<ColdStoragePage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      

	    	  page = icePlantColdStorageService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setIcePlantColdStorage(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/lgu_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getlguForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<PFOPage> page = null;
		 List<PFOPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      

	    	  page = lGUService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	
	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setLgu(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/mangrove_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMangroveForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<MangrovePage> page = null;
		 List<MangrovePage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));

	    	  page = mangroveService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setMangrove(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}	
	@RequestMapping(value = "/market_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMarketForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<MarketPage> page = null;
		 List<MarketPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      

	    	  page = marketService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setMarket(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/national_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMarketForUser(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<NationalCenterPage> page = null;
		 List<NationalCenterPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      

	    	  page = nationalCenterService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setCenter(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/regional_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getRegionalForUser(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<RegionalOfficePage> page = null;
		 List<RegionalOfficePage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      

	    	  page = regionalOfficeService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setRegionalOffice(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/payao_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getPayaoForUser(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<PayaoPage> page = null;
		 List<PayaoPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      

	    	  page = payaoService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setPayao(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
		
	@RequestMapping(value = "/schooloffisheries_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSchooloffisheriesForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<SchoolPage> page = null;
		 List<SchoolPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));

	    	  page = schoolOfFisheriesService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("School page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setSchoolOfFisheries(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/tos_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String tos_user(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<TOSPage> page = null;
		 List<TOSPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));

	    	  page = tosService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("School page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setTOS(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}

	@RequestMapping(value = "/seagrass_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSeagrassForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<SeaGrassPage> page = null;
		 List<SeaGrassPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));

	    	  page = seaGrassService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setSeaGrass(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/seaweeds_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSeaweedsForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<SeaWeedsPage> page = null;
		 List<SeaWeedsPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));

	    	  page = seaweedsService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  
	          
	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setSeaweeds(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/trainings_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getTrainingsForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {


		 Page<TrainingPage> page = null;
		 List<TrainingPage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      

	    	  page = trainingCenterService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setTrainingCenter(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/mariculturezone_user/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMariculturezoneForAdmin(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<MariculturePage> page = null;
		 List<MariculturePage> List=null;
		 int totalPages =0;
	      Pageable pageable = PageRequest.of(page_id, 20, Sort.by("id"));
	      

	    	  page = zoneService.findByRegion(authenticationFacade.getRegionID(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List = page.getContent();
	    
	          pageable = page.nextPageable();  

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setMaricultureZone(List);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	
	@RequestMapping(value = "/fishsanctuaryExcel", method = RequestMethod.GET)
	public ModelAndView fishsanctuaries(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/fishsanctuaries");

		return result;
	}
	@RequestMapping(value = "/fishcageExcel", method = RequestMethod.GET)
	public ModelAndView fishcageexcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/fishcage");

		return result;
	}
	
	@RequestMapping(value = "/fishcoralExcel", method = RequestMethod.GET)
	public ModelAndView fishcoralxcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/fishcorals");

		return result;
	}
	@RequestMapping(value = "/fishlandingExcel", method = RequestMethod.GET)
	public ModelAndView fishlandingExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/fishlanding");

		return result;
	}
	@RequestMapping(value = "/fishpenExcel", method = RequestMethod.GET)
	public ModelAndView fishpenExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/fishpen");

		return result;
	}
	@RequestMapping(value = "/fishpondExcel", method = RequestMethod.GET)
	public ModelAndView fishpondExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/fishpond");

		return result;
	}

	@RequestMapping(value = "/fishportExcel", method = RequestMethod.GET)
	public ModelAndView fishportExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/FISHPORT");

		return result;
	}
	@RequestMapping(value = "/fishprocessingExcel", method = RequestMethod.GET)
	public ModelAndView fishprocessingExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/fishprocessingplants");

		return result;
	}
	@RequestMapping(value = "/hatcheryExcel", method = RequestMethod.GET)
	public ModelAndView hatcheryExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/hatchery");

		return result;
	}
	
	@RequestMapping(value = "/ipcsExcel", method = RequestMethod.GET)
	public ModelAndView ipcsExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/iceplantorcoldstorage");

		return result;
	}
	@RequestMapping(value = "/lguExcel", method = RequestMethod.GET)
	public ModelAndView lguExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/lgu");

		return result;
	}
	@RequestMapping(value = "/mangroveExcel", method = RequestMethod.GET)
	public ModelAndView mangroveExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/mangrove");

		return result;
	}
	@RequestMapping(value = "/mariculturezoneExcel", method = RequestMethod.GET)
	public ModelAndView mariculturezoneExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/mariculturezone");

		return result;
	}
	@RequestMapping(value = "/marketExcel", method = RequestMethod.GET)
	public ModelAndView marketExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/market");

		return result;
	}
	@RequestMapping(value = "/nationalExcel", method = RequestMethod.GET)
	public ModelAndView nationalExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/nationalcenter");

		return result;
	}
	@RequestMapping(value = "/payaoExcel", method = RequestMethod.GET)
	public ModelAndView payaoExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/payao");

		return result;
	}
	@RequestMapping(value = "/regionalExcel", method = RequestMethod.GET)
	public ModelAndView regionalExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/regional");

		return result;
	}
	@RequestMapping(value = "/schooloffisheriesExcel", method = RequestMethod.GET)
	public ModelAndView schooloffisheriesExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/schooloffisheries");

		return result;
	}
	@RequestMapping(value = "/TOSExcel", method = RequestMethod.GET)
	public ModelAndView TOSExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/tos");

		return result;
	}
	@RequestMapping(value = "/seagrassExcel", method = RequestMethod.GET)
	public ModelAndView seagrassExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/seagrass");

		return result;
	}
	@RequestMapping(value = "/seaweedsExcel", method = RequestMethod.GET)
	public ModelAndView seaweedsExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/seaweeds");

		return result;
	}
	@RequestMapping(value = "/trainingcenterExcel", method = RequestMethod.GET)
	public ModelAndView trainingcenterExcel(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();

		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("report/user/trainingcenter");

		return result;
	}
	
	
	
}
