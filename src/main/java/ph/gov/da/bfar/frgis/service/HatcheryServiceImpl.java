package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.HatcheryPage;
import ph.gov.da.bfar.frgis.model.Hatchery;
import ph.gov.da.bfar.frgis.repository.HatcheryRepo;


@Service("HatcheryService")
@Transactional
public class HatcheryServiceImpl implements HatcheryService{

	@Autowired
	private HatcheryRepo dao;


	
	public Optional<Hatchery> findById(int id) {
		return dao.findById(id);
	}

	public List<Hatchery> ListByUsername(String username) {
		List<Hatchery>Hatchery = dao.getListHatcheryByUsername(username);
		return Hatchery;
	}

	public void saveHatchery(Hatchery Hatchery) {
		dao.save(Hatchery);
	}

	@Override
	public void deleteHatcheryById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateByEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateHatcheryByReason(int id, String reason) {
		dao.updateHatcheryByReason(id, reason);
		
	}

	@Override
	public List<Hatchery> findAllByType(String type) {
		
		return dao.findAllByType(type);
	}

	@Override
	public List<Hatchery> findByRegionAndType(String region_id, String type) {
		
		return dao.getListByRegionAndType(region_id, type);
	}

	@Override
	public List<Hatchery> findAllByLegislated(String legislated) {
		
		return dao.findAllByLegislated(legislated);
	}

	@Override
	public List<Hatchery> findByRegionAndLegislated(String region_id, String legislated) {
		
		return dao.getListByRegionAndLegislated(region_id, legislated);
	}

	public List<Hatchery> findAllHatcherys() {
		return dao.findAll();
	}
	@Override
	public List<Hatchery> findByRegion(String region_id) {

	return dao.getListByRegion(region_id);
}

	@Override
	public List<Hatchery> getAllHatcherys(int index, int pagesize, String user) {
		
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<Hatchery> page = dao.getPageableHatchery(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int HatcheryCount(String username) {
		return dao.HatcheryCount(username);
	}

	@Override
	public Hatchery findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<Hatchery> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<HatcheryPage> findByUsername(String user, Pageable pageable) {

		return dao.findByUsername(user, pageable);
	}
	
	@Override
	public Page<HatcheryPage> findAllHatchery(Pageable pageable) {

		return dao.findAllHatchery(pageable);
	}
	
	@Override
	public Page<HatcheryPage> findByRegion(String region, Pageable pageable) {

		return dao.findByRegion(region, pageable);
	}

	@Override
	public Long countHatcheryPerRegion(String region_id) {

		return dao.countHatcheryPerRegion(region_id);
	}

	@Override
	public Long countHatcheryPerProvince(String province_id) {

		return dao.countHatcheryPerProvince(province_id);
	}

	@Override
	public Long countAllHatchery() {
		
		return dao.countAllHatchery();
	}

	@Override
	public Long countHatcheryPerProvince(String province_id, String operator) {

		return dao.countHatcheryPerProvince(province_id, operator);
	}
	
	@Override
	public Long countHatcheryPerRegion(String region_id, String operator) {
	
	return dao.countHatcheryPerRegion(region_id, operator);
	}
//	@Override
//	public int HatcheryCountByMuunicipality(String municipalityID) {
//		return dao.HatcheryCountByMuunicipality(municipalityID);
//	}
//
//	@Override
//	public int HatcheryCountByProvince(String provinceID) {
//		return dao.HatcheryCountByProvince(provinceID);
//	}
//
//	@Override
//	public int HatcheryCountByRegion(String regionID) {
//		return dao.HatcheryCountByRegion(regionID);
//	}

	@Override
	public Long countHatcheryPerMunicipality(String municipality_id) {
	
		return dao.countHatcheryPerMunicipality(municipality_id);
	}

	@Override
	public Long countHatcheryPerMunicipality(String municipality_id, String operator) {

		return dao.countHatcheryPerMunicipality(municipality_id, operator);
	}

	@Override
	public Long countHatcheryLegislatedPerRegion(String region_id, String operator, String legislated) {

		return dao.countHatcheryLegislatedPerRegion(region_id, operator, legislated);
	}

	@Override
	public Long countHatcheryLegislatedPerProvince(String province_id, String operator, String legislated) {
		
		return dao.countHatcheryLegislatedPerProvince(province_id, operator, legislated);
	}

	@Override
	public Long countHatcheryLegislatedPerMunicipality(String municipality_id, String operator, String legislated) {
		
		return dao.countHatcheryLegislatedPerMunicipality(municipality_id, operator, legislated);
	}

/*	public boolean isHatcherySSOUnique(Integer id, String sso) {
		Hatchery Hatchery = findBySSO(sso);
		return ( Hatchery == null || ((id != null) && (Hatchery.getId() == id)));
	}*/
	
}
