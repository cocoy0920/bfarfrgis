package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.FishSanctuaryPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.service.FishSanctuariesService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class FishSanctuaryController{
	
	@Autowired
	UserService userService;

	@Autowired
	FishSanctuariesService fishSanctuariesService;
	
	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	private String newUniqueKeyforMap="";
	private String abbr = "";
	private int count=0;
	private String map_data="";
	User user;	

	@RequestMapping(value = "/fishsanctuary", method = RequestMethod.GET)
	public ModelAndView fishsanctuary(ModelMap model) throws Exception {
		
		ModelAndView result = new ModelAndView();
		map_data="";
		//user   = new User();
		
		user = authenticationFacade.getUserInfo();
//		Set<UserProfile> setOfRole = new HashSet<>();
//		setOfRole = user.getUserProfiles();
//		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
//		UserProfile role = list.get(0);

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "fishsanctuaries");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("users/form");

		return result;
	}
	@RequestMapping(value = "/fishsanctuaries", method = RequestMethod.GET)
	public ModelAndView fishsanctuaries(ModelMap model) throws Exception {
		
		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();
		
		model.addAttribute("user", user);
		model.addAttribute("mapsData", map_data);
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		
		result = new ModelAndView("users/form");

		return result;
	}
	@RequestMapping(value = "/fishSanctuaryMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishSanctuaryMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			//List<MapsData> mapsDatas = mapsService.findByUniqueKey(uniqueKey);
		 	FishSanctuaries data = fishSanctuariesService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	Features features = new Features();
			 features = MapBuilder.getFishSanctuary(data);
			 
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	mapsData.setIcon("/static/images/pin2022/FishSanctuary.png");
		 	
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			
		
			map_data= jsonString;
			
		result = new ModelAndView("redirect:../fishsanctuaries");
		
	
		return result;
	}	
	@RequestMapping(value = "/fishSanctuaryDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView fishSanctuaryDeleteById(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		// Optional<FishSanctuaries> optional =  fishSanctuariesService.findById(id);
		// optional.forEach(s -> System.out.println(s));
		// System.out.println("DATA:  " + optional.);
		// fishSanctuariesService.deleteFishSanctuariesrById(id);
		 fishSanctuariesService.findByUniqueKeyForEnabled(id);;
			
		result = new ModelAndView("redirect:../fishsanctuaries");
		
	
		return result;
	}
	@RequestMapping(value = "/fishSanctuaryDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView fishSanctuaryDeleteById(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		// Optional<FishSanctuaries> optional =  fishSanctuariesService.findById(id);
		// optional.forEach(s -> System.out.println(s));
		// System.out.println("DATA:  " + optional.);
		// fishSanctuariesService.deleteFishSanctuariesrById(id);
		 fishSanctuariesService.findByUniqueKeyForReason(id,reason);
		 fishSanctuariesService.findByUniqueKeyForEnabled(id);
			
		result = new ModelAndView("redirect:../../fishsanctuaries");
		
	
		return result;
	}
	@RequestMapping(value = "/fishsanctuariesList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getfishsanctuariesList()
			throws JsonGenerationException, JsonMappingException, IOException {

		List<FishSanctuaries> list = null;
		
		list = fishSanctuariesService.getListFishSanctuariesByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}
	@ResponseBody 
	@RequestMapping(value = "/saveFishSanctuary", method = RequestMethod.POST, produces = "application/json")
	public String postFishSanctuary(@RequestBody FishSanctuaries fishSanctuaries)
			throws JsonGenerationException, JsonMappingException, IOException {


		savingResources(fishSanctuaries);
		
//		redirectAttributes.addFlashAttribute("message", "Successfully save the data");
//		response.sendRedirect("/create/fishcages");
//
//		 Page<FishSanctuaries> page = null;
//	      Pageable pageable = PageRequest.of(0, 5, Sort.by("id"));
//	      
//	          page = fishSanctuariesService.findByUsername(authenticationFacade.getUsername(),pageable);
//	          int number = page.getNumber();
//	          int numberOfElements = page.getNumberOfElements();
//	          int size = page.getSize();
//	          long totalElements = page.getTotalElements();
//	          int totalPages = page.getTotalPages();
//	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
//	                          + "totalElements: %s, totalPages: %s%n",
//	                  number, numberOfElements, size, totalElements, totalPages);
//	          List<FishSanctuaries> FishSanctuariesList = page.getContent();
//
//	          pageable = page.nextPageable();
//
//	          StringBuilder builder = new StringBuilder();
//			
//			builder.append("<ul class=\"pagination pagination-sm\">");
//	
//			for (int i = 0; i <= totalPages-1; i++) {
//				int y = i + 1;
//				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
//			}
//		builder.append("</ul>");
//		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
		//resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	private void savingResources(FishSanctuaries sanctuaries) {
		
		User user = new User();
		user = authenticationFacade.getUserInfo();
		

		boolean save = false;
		UUID newID = UUID.randomUUID();
		
		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		sanctuaries.setRegion_id(region_data[0]);
		sanctuaries.setRegion(region_data[1]);

		province = sanctuaries.getProvince();
		data = province.split(",", 2);

		sanctuaries.setProvince_id(data[0]);
		sanctuaries.setProvince(data[1]);

		municipal = sanctuaries.getMunicipality();
		municipal_data = municipal.split(",", 2);

		sanctuaries.setMunicipality_id(municipal_data[0]);
		sanctuaries.setMunicipality(municipal_data[1]);

		barangay = sanctuaries.getBarangay();

		barangay_data = barangay.split(",", 3);
		sanctuaries.setBarangay_id(barangay_data[0]);
		sanctuaries.setBarangay(barangay_data[1]);
		sanctuaries.setEnabled(true);
		
		
		String imageNameSrc = Base64ImageUtil.removeExtension(sanctuaries.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		String newUniqueKey = newID.toString();

		if (sanctuaries.getId() != 0) {
			Optional<FishSanctuaries> fishSanctuaries = fishSanctuariesService.findById(sanctuaries.getId());			
			String image_server = fishSanctuaries.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				sanctuaries.setImage("/images/" + image_server);
				newUniqueKeyforMap = sanctuaries.getUniqueKey();
				fishSanctuariesService.saveFishSanctuaries(ResourceData.getFishSanctuaries(sanctuaries, user, "", UPDATE));
				
			}else {
				newUniqueKeyforMap = sanctuaries.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", "");  
				sanctuaries.setImage("/images/" +imageName );
				Base64Converter.convertToImage(sanctuaries.getImage_src(), sanctuaries.getImage());
				fishSanctuariesService.saveFishSanctuaries(ResourceData.getFishSanctuaries(sanctuaries, user, "", UPDATE));
				
			}

			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", "");
			sanctuaries.setImage("/images/" +imageName );
			Base64Converter.convertToImage(sanctuaries.getImage_src(), sanctuaries.getImage());
			fishSanctuariesService.saveFishSanctuaries(ResourceData.getFishSanctuaries(sanctuaries, user, newUniqueKey, SAVE));
			
			save = true;

		}
	}
	@RequestMapping(value = "/info/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getInfoById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<FishSanctuaries> mapsDatas = fishSanctuariesService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);
		/*
		 * List <Provinces> provinces= getProvinces(); String jsonProvince =
		 * gson.toJson(provinces); String data = jsonArray +jsonProvince; show = "ok";
		 * data = data.replace("][",",");
		 */
		// logger.info("/info/{uniqueId}: " +jsonArray );
		return jsonArray;

	}
	@RequestMapping(value = "/fishsanctuaries/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishsanctuariesPagination(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishSanctuaryPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 15, Sort.by("id"));
	      
	          page = fishSanctuariesService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info fishsanctuaries - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<FishSanctuaryPage> FishSanctuariesList = page.getContent();
	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishSanctuaries(FishSanctuariesList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
//	@RequestMapping(value = "/getLastPage", method = RequestMethod.GET, produces = "application/json")
//	public @ResponseBody String getLastPage(ModelMap model)
//			throws JsonGenerationException, JsonMappingException, IOException {
//
//		int totalNumberOfRecords = 0;
//		int numberOfRecordsPerPage = 20;
//
//		totalNumberOfRecords = 10;//fishSanctuariesService.FishSanctuariesCount(authenticationFacade.getUsername());
//
//		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
//		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
//			noOfPages = noOfPages + 1;
//		}
//		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
//
//		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));
//
//		Gson gson = new GsonBuilder().setPrettyPrinting().create();
//
//		String jsonArray = gson.toJson(String.valueOf(noOfPages));
//
//		return jsonArray;
//
//	}

//    @RequestMapping(value = "/Postfishsanctuary", method = RequestMethod.POST)
//    public String savefishsanctuary(@Valid @ModelAttribute("FishSanctuariesForm") FishSanctuaries FishSanctuariesForm,
//    		Errors errors, ModelMap model) {
//    System.out.println("FishSanctuariesForm: " + FishSanctuariesForm);
//    	
//    
//    	FishSanctuaries fishSanctuaries = new FishSanctuaries();
//    	FishSanctuariesForm.setId(Integer.valueOf(0));
//    	fishSanctuaries = FishSanctuariesForm;
//    	 
//    	if (errors.hasErrors()) {
//
//	            
//	            String errorString = errors.getFieldErrors()
//                     .stream().map(x -> x.getDefaultMessage())
//                     .collect(Collectors.joining("<br>"));	
//	            
//	            model.addAttribute("error", errorString);
//	           
//
//	            return "users/fishsanctuaries";
//
//	        }
//       
//        	System.out.println("SAVING FISH SANCTUARY");
//        	savingResources(fishSanctuaries);
//        	model.addAttribute("error", "");
//        	model.addAttribute("SAVE", "data Already save");
//        	model.put("FishSanctuariesForm", new FishSanctuaries());
//        	return "redirect:fishsanctuary";
//        
//    }
//	@RequestMapping(value = "/fishSanctuaryMap", method = RequestMethod.GET)
//	public ModelAndView fishSanctuaryMAP(ModelMap model)
//			throws JsonGenerationException, JsonMappingException, IOException {
//
//		 ModelAndView result;
//		 
//		 ObjectMapper mapper = new ObjectMapper();
//			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "fishsanctuaries");
//			String jsonString = mapper.writeValueAsString(mapsDatas);
//
//			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
//			//model.addAttribute("page", "home_map");
//			model.addAttribute("mapsData", jsonString);
//		
//		result = new ModelAndView("users/map/fishsanctuarymap");
//		
//	
//		return result;
//	}
//		
//	 @PostMapping("/api/saveFishSanctuary")
//	    public ResponseEntity<?> saveFishSanctuary(
//	            @Valid @RequestBody FishSanctuaries fishSanctuaries, Errors errors) {
//
//	        AjaxResponseBody result = new AjaxResponseBody();
//
//	        //If error, just return a 400 bad request, along with the error message
//	        if (errors.hasErrors()) {
//
//	            result.setMsg(errors.getAllErrors()
//	                        .stream().map(x -> x.getDefaultMessage())
//	                        .collect(Collectors.joining("<br>")));
//	            String errorString = errors.getAllErrors()
//                     .stream().map(x -> x.getDefaultMessage())
//                     .collect(Collectors.joining("<br>"));	
//	            return ResponseEntity.badRequest().body(errorString);
//
//	        }
//	        savingResources(fishSanctuaries);
////	        List<User> users = userService.findByUserNameOrEmail(search.getUsername());
////	        if (users.isEmpty()) {
////	            result.setMsg("no user found!");
////	        } else {
////	            result.setMsg("success");
////	        }
//	        result.setMsg("Data Already save to Database");
//
//	        return ResponseEntity.ok(result);
//
//	    }
	

}
