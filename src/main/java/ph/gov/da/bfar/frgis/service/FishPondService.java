package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ph.gov.da.bfar.frgis.Page.FishPondPage;
import ph.gov.da.bfar.frgis.model.FishPond;



public interface FishPondService {
	
	Optional<FishPond> findById(int id);
	
	FishPond findByUniqueKey(String uniqueKey);
	
	List<FishPond> findByUniqueKeyForUpdate(String uniqueKey);
	
	List<FishPond> getListFishPondByUsername(String user);
	List<FishPond> findByRegion(String region_id);
	Page<FishPondPage> findByUsername(String username, Pageable pageable);
	Page<FishPondPage> findByRegion(String region,Pageable pageable);
	Page<FishPondPage> findAllFishPond(Pageable pageable);
    List<FishPond> findByRegionList(String regionID);
	List<FishPond> findByProvinceList(String provinceID);
//	List<FishPond> findByMunicipality(String municipalityID);
	
	void saveFishPond(FishPond FishPond);
	
//	void updateFishPond(FishPond FishPond);
	void updateForEnabled(int  id);
	void updateFishPondByReason(int  id,String reason);
	void deleteFishPondById(int id);

	List<FishPond> findAllFishPond(); 
    public List<FishPond> getAllFishPond(int index,int pagesize,String user);
    public int FishPondCount(String username);
    public Long countAllFishPond();
    public Long countFishPondPerRegion(String region_id);
    public Long countFishPondPerProvince(String province_id);


}