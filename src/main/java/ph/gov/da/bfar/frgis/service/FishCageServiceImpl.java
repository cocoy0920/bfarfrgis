package ph.gov.da.bfar.frgis.service;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ph.gov.da.bfar.frgis.Page.FishCagePage;
import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.repository.FishCageRepo;




@Service("FishCageService")
@Transactional
public class FishCageServiceImpl implements FishCageService{


//	@Autowired
//	MapsService mapsService;
//	
//	
	@Autowired
	FishCageRepo cageRepo;
	
	public Optional<FishCage>findById(int id) {
		return cageRepo.findById(id);
	}

	public List<FishCage> getListFishCageByUsername(String username) {
		List<FishCage> FishCage = cageRepo.getListFishCageByUsername(username);
		return FishCage;
	}

	public void saveFishCage(FishCage fishCage) {
		cageRepo.save(fishCage);
		 
	}
	
	public void deleteFishCage(int id) {
		cageRepo.deleteById(id);
	}

	@Override
	public void updateFishCageByEnabled(int id) {	
		cageRepo.updateFishCageByEnabled(id);
	}

	@Override
	public void updateFishCageByForReason(int id, String reason) {
		cageRepo.updateFishCageByReason(id, reason);
		
	}

	public List<FishCage> findAllFishCages() {
		return (List<FishCage>) cageRepo.findAll();
	}

	@Override
	public List<FishCage> getAllFishCages(int index, int pagesize, String user) {

		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<FishCage> page = cageRepo.getPageableFishCages(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int FishCageCount(String username) {
		return cageRepo.FishCageCount(username);
	}

	@Override
	public FishCage findByUniqueKey(String uniqueKey) {
		
		return cageRepo.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<FishCage> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return cageRepo.findByUniqueKeyForUpdate(uniqueKey);
	}
	@Override
	public Page<FishCagePage> findByUsername(String user, Pageable pageable) {
		return cageRepo.findByUsername(user, pageable);
	}
	
	@Override
		public Page<FishCagePage> findByRegion(String region, Pageable pageable) {
		
			return cageRepo.findByRegion(region, pageable);
		}

	@Override
		public Page<FishCagePage> findAllFishcage(Pageable pageable) {
			return cageRepo.findAllFishcage(pageable);
		}
	@Override
	public List<FishCage> getListByRegion(String regionID) {

		return cageRepo.getListFishCageByRegion(regionID);
	}

	@Override
	public Long countFishCagePerRegion(String region_id) {
		
		return cageRepo.countFishCagePerRegion(region_id);
	}

	@Override
	public Long countFishCagePerProvince(String province_id) {

		return cageRepo.countFishCagePerProvince(province_id);
	}

	@Override
	public Long countAllFishCage() {

		return cageRepo.countAllFishCage();
	}

	@Override
	public Long countFishCagePerMunicipality(String municipality_id) {
		
		return cageRepo.countFishCagePerMunicipality(municipality_id);
	}

//
//	@Override
//	public List<FishCage> getAllFishCageByRegion(int index, int pagesize, String region_id) {
//		return dao.getAllFishCageByRegion(index, pagesize, region_id);
//	}
//
//	@Override
//	public List<FishCage> getAllFishCageByAllRegion(int index, int pagesize) {
//		return dao.getAllFishCageByAllRegion(index, pagesize);
//	}
//
//	@Override
//	public int FishCageCountByMuunicipality(String municipalityID) {
//		return dao.FishCageCountByMuunicipality(municipalityID);
//	}
//
//	@Override
//	public int FishCageCountByProvince(String provinceID) {
//		return dao.FishCageCountByProvince(provinceID);
//	}
//
//	@Override
//	public int FishCageCountByRegion(String regionID) {
//		return dao.FishCageCountByRegion(regionID);
//	}
//
//	@Override
//	public List<FishCage> findByRegion(String regionID) {
//		return dao.findByRegion(regionID);
//	}
//
//	@Override
//	public List<FishCage> findByProvince(String provinceID) {
//		return dao.findByProvince(provinceID);
//	}
//
//	@Override
//	public List<FishCage> findByMunicipality(String municipalityID) {
//		return dao.findByMunicipality(municipalityID);
//	}


/*	public boolean isFishCageSSOUnique(Integer id, String sso) {
		FishCage FishCage = findBySSO(sso);
		return ( FishCage == null || ((id != null) && (FishCage.getId() == id)));
	}*/
	
}
