package ph.gov.da.bfar.frgis.web.util;

import java.util.HashMap;
import java.util.Map;

public class PageUtils {
	
	
	public static String getList(String page){
		
		String pageList="";		
		if(page.equals("consignation")) {
						
			pageList =  "consignationList";
			
		}else if(page.equals("fishcage")) {
			
			pageList =  "fishcageList";
			
		}else if(page.equals("fishcorrals")) {
			
			pageList =  "fishcorralsList";
			
		}else if(page.equals("fisherfolkform")) {
			
			pageList =  "fishcageList";
			
		}else if(page.equals("fishlanding")) {
			
			pageList =  "fishlandingList";
			
		}else if(page.equals("fishpen")) {
			
			pageList =  "fishpenList";
			
		}else if(page.equals("fishpond")) {
			
			pageList =  "fishpondList";
			
		}else if(page.equals("fishprocessingplants")) {
			
			pageList =  "fishprocessingplantsList";
			
		}else if(page.equals("fishsanctuaries")) {
			
			pageList =  "fishsanctuariesList";
			
		}else if(page.equals("iceplantorcoldstorage")) {
			
			pageList =  "iceplantorcoldstorageList";
		}else if(page.equals("lgu")) {
			
			pageList =  "lguList";
		}else if(page.equals("mangrove")) {
			
			pageList =  "mangroveList";
		}else if(page.equals("marineprotected")) {
			
			pageList =  "marineprotectedList";
			
		}else if(page.equals("market")) {
			
			pageList =  "marketList";
			
		}else if(page.equals("schooloffisheries")) {
			
			pageList =  "schooloffisheriesList";
			
		}else if(page.equals("seagrass")) {
			
			pageList =  "seagrassList";
		}else if(page.equals("seaweeds")) {
			
			pageList =  "seaweedsList";
		}else if(page.equals("trainingcenter")) {
			
			pageList =  "trainingcenterList";
			
		}else if(page.equals("hatchery")) {
			
			pageList =  "hatcheryList";
			
		}else if(page.equals("mariculturezone")) {
			
			pageList =  "mariculturezoneList";
			
		}else if(page.equals("FISHPORT")) {
			
			pageList =  "fishportList";
			
		}else if(page.equals("otherfishpond")) {
			pageList =  "otherfishpondList";
		}else {
			return "FisherFolkList";
		}
		
		 return pageList;
	}
	
	public static String getPages(String page){
		
		String status="";
		
		  HashMap<Integer,String> hm=new HashMap<Integer,String>();  
		  hm.put(1,"consignation");  
		  hm.put(2,"fishcage");  
		  hm.put(3,"fishcorrals");
		  hm.put(4,"fisherfolkform");
		  hm.put(5,"fishlanding");
		  hm.put(6,"fishpen");
		  hm.put(7,"fishpond");
		  hm.put(8,"fishprocessingplants");
		  hm.put(9,"fishsanctuaries");
		  hm.put(10,"iceplantorcoldstorage");
		  hm.put(11,"lgu");
		  hm.put(12,"mangrove");
		  hm.put(13,"marineprotected");
		  hm.put(14,"market");
		  hm.put(15,"schooloffisheries");
		  hm.put(16,"seagrass");
		  hm.put(17,"seaweeds");
		  hm.put(18,"trainingcenter");
		  hm.put(19,"hatchery");
		  hm.put(20,"mariculturezone");
		  hm.put(21,"FISHPORT");
		  hm.put(22,"otherfishpond");
		  
		  for(Map.Entry m:hm.entrySet()){  
			  // System.out.println(m.getKey()+" "+m.getValue());  
			   
			   if(page.equals(m.getValue())){
				   status = m.getValue().toString();
			   }
			   
			  } 
		  
		  return status;
	}
	
	public static boolean isNullEmpty(String str) {

	    // check if string is null
	    if (str == null) {
	      return true;
	    }

	    // check if string is empty
	    else if(str.trim().isEmpty()){
	      return true;
	    }

	    else {
	      return false;
	    }
	  }
	public static String getCenterMapPerRegion(String region){
		
		String  set_center="";
		
		  if(region.equals("010000000")) {
			  //REGION 1
			  set_center =   "16.913544, 120.879809";
		  }else if(region.equals("020000000")){
			  //REGION 2
			  set_center =   "18.809179, 121.037159";
		  }else if(region.equals("030000000")){
			  //REGION 3
			  set_center =   "15.542543, 121.078241";
		  }else if(region.equals("040000000")){
			  //REGION 4
			  set_center =   "14.191743, 121.786513";
		  }else if(region.equals("050000000")){
			  //REGION 5
			  set_center =   "12.781953, 123.097564";
		  }else if(region.equals("060000000")){
			  //REGION 6
			  set_center =   "10.936328, 122.926743";
		  }else if(region.equals("070000000")){
			  //REGION 7
			  set_center =   "10.406513, 124.110502";
		  }else if(region.equals("080000000")){
			  //REGION 8
			  set_center =   "11.499164, 125.089589";
		  }else if(region.equals("090000000")){
			  //REGION 9
			  set_center =   "7.634044, 122.974429";
		  }else if(region.equals("100000000")){
			  //REGION 10
			  set_center =   "8.046255, 124.572526";
		  }else if(region.equals("110000000")){
			  //REGION 11
			  set_center =   "6.742378, 125.896436";
		  }else if(region.equals("120000000")){
			  //REGION 12
			  set_center =   "6.649221, 124.845753";
		  }else if(region.equals("130000000")){
			  //REGION 13
			  set_center =   "14.564432, 120.974627";
		  }else if(region.equals("140000000")){
			  //CAR
			  set_center =   "17.269665, 121.067215";
		  }else if(region.equals("150000000")){
			  //ARMM
			  set_center =   "6.728290, 122.341598";
		  }else if(region.equals("160000000")){
			  //CARAGA
			  set_center =   "8.875153, 125.799698";
		  }else if(region.equals("170000000")){
			  //REGION 4B
			  set_center =   "9.902934, 121.051223";
		  }else if(region.equals("180000000")){
			  //CO
			  set_center =   "14.655784, 121.048458";
		  }
		  
		  return set_center;
	}
	
	public static String getRegionName(String region){
		
		String  region_name="";
		
		  if(region.equals("010000000")) {
			  region_name =   "Ilocos";
		  }else if(region.equals("Ilocos")){
			  region_name =   "010000000";
		  }else if(region.equals("020000000")){
			  region_name =   "CagayanValley";
		  }else if(region.equals("CagayanValley")){
			  region_name =   "020000000";
		  }else if(region.equals("030000000")){
			  region_name =   "CentralLuzon";
		  }else if(region.equals("CentralLuzon")){
			  region_name =   "030000000";
		  }else if(region.equals("040000000")){
			  region_name =   "CALABARZON";
		  }else if(region.equals("CALABARZON")){
			  region_name =   "040000000";
		  }else if(region.equals("050000000")){
			  region_name =   "Bicol";
		  }else if(region.equals("Bicol")){
			  region_name =   "050000000";
		  }else if(region.equals("060000000")){
			  region_name =   "Western";
		  }else if(region.equals("Western")){
			  region_name =   "060000000";
		  }else if(region.equals("070000000")){
			  region_name =   "Central";
		  }else if(region.equals("Central")){
			  region_name =   "070000000";
		  }else if(region.equals("080000000")){
			  region_name =   "Eastern";
		  }else if(region.equals("Eastern")){
			  region_name =   "080000000";
		  }else if(region.equals("090000000")){
			  region_name =   "ZamboangaPeninsula";
		  }else if(region.equals("ZamboangaPeninsula")){
			  region_name =   "090000000";
		  }else if(region.equals("100000000")){
			  region_name =   "NorthernMindanao";
		  }else if(region.equals("NorthernMindanao")){
			  region_name =   "100000000";
		  }else if(region.equals("110000000")){
			  region_name =   "DavaoRegion";
		  }else if(region.equals("DavaoRegion")){
			  region_name =   "110000000";
		  }else if(region.equals("120000000")){
			  region_name =   "SOCCSKSARGEN";
		  }else if(region.equals("SOCCSKSARGEN")){
			  region_name =   "120000000";
		  }else if(region.equals("130000000")){
			  region_name =   "NCR";
		  }else if(region.equals("NCR")){
			  region_name =   "130000000";
		  }else if(region.equals("140000000")){
			  region_name =   "CAR";
		  }else if(region.equals("CAR")){
			  region_name =   "140000000";
		  }else if(region.equals("150000000")){
			  region_name =   "ARMM";
		  }else if(region.equals("ARMM")){
			  region_name =   "150000000";
		  }else if(region.equals("160000000")){
			  region_name =   "Caraga";
		  }else if(region.equals("Caraga")){
			  region_name =   "160000000";
		  }else if(region.equals("170000000")){
			  region_name =   "MIMAROPA";
		  }else if(region.equals("MIMAROPA")){
			  region_name =   "170000000";
		  }
		  
		  return region_name;
	}
	
}
