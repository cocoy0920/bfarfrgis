package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.NationalCenterPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.NationalCenter;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.NationalCenterService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class NationalCenterController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	NationalCenterService nationalCenterService;
	
	 @Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;
	
	@RequestMapping(value = "/nationalcenter", method = RequestMethod.GET)
	public ModelAndView nationalcenter(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();
		
		user = authenticationFacade.getUserInfo();

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "center");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}
	@RequestMapping(value = "/nationalcenters", method = RequestMethod.GET)
	public ModelAndView nationalcenters(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "center");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}

	@RequestMapping(value = "/nationalcenterMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView nationalCenterByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			NationalCenter data = nationalCenterService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	
		 	Features features = new Features();
			 features = MapBuilder.getNationalCenterMapData(data);
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUnique_key());
		 	mapsData.setIcon("/static/images/pin2022/BINTCenter.png");
		 	
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			
			map_data =  jsonString;
		
		result = new ModelAndView("redirect:../nationalcenters");
		
	
		return result;
	}
	@RequestMapping(value = "/nationalcenterDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView nationalCenterByUniqueKey(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;

		//nationalCenterService.deleteNationalCenterById(id);
		nationalCenterService.updateByEnabled(id);
		result = new ModelAndView("redirect:../nationalcenters");
		
	
		return result;
	}
	@RequestMapping(value = "/nationalcenterDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView nationalCenterByUniqueKey(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;

		//nationalCenterService.deleteNationalCenterById(id);
		nationalCenterService.updateByEnabled(id);
		nationalCenterService.updateNationalCenterByReason(id, reason);
		result = new ModelAndView("redirect:../../nationalcenters");
		
	
		return result;
	}
	
	@RequestMapping(value = "/nationalcenterMap", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "seagrass");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("users/map/seagrassmap");
		
	
		return result;
	}

	@RequestMapping(value = "/nationalcenter/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSeagrass(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<NationalCenterPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 5, Sort.by("id"));
	      
	          page = nationalCenterService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<NationalCenterPage> SeaGrassList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setCenter(SeaGrassList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/nationalcenterList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getseagrassList() throws JsonGenerationException, JsonMappingException, IOException {

		List<NationalCenter> list = null;

		list = nationalCenterService.ListByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	
	@RequestMapping(value = "/getNationalCenter/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSeagrassById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<NationalCenter> mapsDatas = nationalCenterService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}


	@RequestMapping(value = "/saveNationalCenter", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postNationalCenter(@RequestBody NationalCenter nationalCenter) throws JsonGenerationException, JsonMappingException, IOException {

		
		savingResources(nationalCenter);
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
	

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);

	}
//	@RequestMapping(value = "/getSeaGrass", method = RequestMethod.GET, produces = "application/json")
//	public @ResponseBody String getSeaGrass(@RequestBody SeaGrass seaGrass) throws JsonGenerationException, JsonMappingException, IOException {
//
//		List<SeaGrass> list = null;
//
//		int pageIndex = 1;
//		int totalNumberOfRecords = 0;
//		int numberOfRecordsPerPage = 20;
//		int page = (pageIndex * numberOfRecordsPerPage) - numberOfRecordsPerPage;
//
//		totalNumberOfRecords = seaGrassService.SeaGrassCount(authenticationFacade.getUsername());
//
//		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
//		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
//			noOfPages = noOfPages + 1;
//		}
//		StringBuilder builder = new StringBuilder();
//		
//		builder.append("<ul class=\"pagination pagination-sm\">");
//
//		for (int i = 1; i <= noOfPages; i++) {
//			builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + i + "</a></li>");
//		}
//		builder.append("</ul>");
//
//		list = seaGrassService.getAllSeaGrasss(page, numberOfRecordsPerPage, authenticationFacade.getUsername());
//		String pageNumber = builder.toString();
//
//		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
//		resourcesPaginations.setSeaGrass(list);
//		resourcesPaginations.setPageNumber(pageNumber);
//
//		ObjectMapper objectMapper = new ObjectMapper();
//
//		return objectMapper.writeValueAsString(resourcesPaginations);
//	}

	private void savingResources(NationalCenter nationalCenter) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		nationalCenter.setRegion_id(region_data[0]);
		nationalCenter.setRegion(region_data[1]);

		province = nationalCenter.getProvince();
		data = province.split(",", 2);
		nationalCenter.setProvince_id(data[0]);
		nationalCenter.setProvince(data[1]);

		municipal = nationalCenter.getMunicipality();
		municipal_data = municipal.split(",", 2);
		nationalCenter.setMunicipality_id(municipal_data[0]);
		nationalCenter.setMunicipality(municipal_data[1]);
		nationalCenter.setEnabled(true);

		barangay = nationalCenter.getBarangay();
		barangay_data = barangay.split(",", 3);
		nationalCenter.setBarangay_id(barangay_data[0]);
		nationalCenter.setBarangay(barangay_data[1]);

		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(nationalCenter.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		if (nationalCenter.getId() != 0) {
			Optional<NationalCenter> center = nationalCenterService.findById(nationalCenter.getId());
			String image_server = center.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				nationalCenter.setImage("/images/" + image_server);
				newUniqueKeyforMap = nationalCenter.getUnique_key();
				nationalCenterService.saveNationalCenter(ResourceData.getNationalCenter(nationalCenter, user, newUniqueKey, UPDATE));
			}else {
				newUniqueKeyforMap = nationalCenter.getUnique_key();
				imageName.replaceAll("[^a-zA-Z0-9]", "");  
				nationalCenter.setImage("/images/" + imageName);
				Base64Converter.convertToImage(nationalCenter.getImage_src(), nationalCenter.getImage());
				nationalCenterService.saveNationalCenter(ResourceData.getNationalCenter(nationalCenter, user, newUniqueKey, UPDATE));
			}
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", "");  
			nationalCenter.setImage("/images/" + imageName);
			Base64Converter.convertToImage(nationalCenter.getImage_src(), nationalCenter.getImage());
			nationalCenterService.saveNationalCenter(ResourceData.getNationalCenter(nationalCenter, user, newUniqueKey, SAVE));
			save = true;

		}

	}


	
}
