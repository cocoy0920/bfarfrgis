package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.FishCoral;
import ph.gov.da.bfar.frgis.model.FishLanding;
import ph.gov.da.bfar.frgis.model.FishPen;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.Fishport;
import ph.gov.da.bfar.frgis.model.Hatchery;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;
import ph.gov.da.bfar.frgis.model.LGU;
import ph.gov.da.bfar.frgis.model.Mangrove;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.MaricultureZone;
import ph.gov.da.bfar.frgis.model.Market;
import ph.gov.da.bfar.frgis.model.NationalCenter;
import ph.gov.da.bfar.frgis.model.Payao;
import ph.gov.da.bfar.frgis.model.RegionalOffice;
import ph.gov.da.bfar.frgis.model.Regions;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;
import ph.gov.da.bfar.frgis.model.SeaGrass;
import ph.gov.da.bfar.frgis.model.Seaweeds;
import ph.gov.da.bfar.frgis.model.TOS;
import ph.gov.da.bfar.frgis.model.TrainingCenter;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserBean;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.service.FishCageService;
import ph.gov.da.bfar.frgis.service.FishCorralService;
import ph.gov.da.bfar.frgis.service.FishLandingService;
import ph.gov.da.bfar.frgis.service.FishPenService;
import ph.gov.da.bfar.frgis.service.FishPortService;
import ph.gov.da.bfar.frgis.service.FishProcessingPlantService;
import ph.gov.da.bfar.frgis.service.FishSanctuariesService;
import ph.gov.da.bfar.frgis.service.HatcheryService;
import ph.gov.da.bfar.frgis.service.IcePlantColdStorageService;
import ph.gov.da.bfar.frgis.service.LGUService;
import ph.gov.da.bfar.frgis.service.MangroveService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.MaricultureZoneService;
import ph.gov.da.bfar.frgis.service.MarketService;
import ph.gov.da.bfar.frgis.service.NationalCenterService;
import ph.gov.da.bfar.frgis.service.PayaoService;
import ph.gov.da.bfar.frgis.service.RegionalOfficeService;
import ph.gov.da.bfar.frgis.service.RegionsService;
import ph.gov.da.bfar.frgis.service.SchoolOfFisheriesService;
import ph.gov.da.bfar.frgis.service.SeaGrassService;
import ph.gov.da.bfar.frgis.service.SeaweedsService;
import ph.gov.da.bfar.frgis.service.TOSService;
import ph.gov.da.bfar.frgis.service.TrainingCenterService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.ChartsInterface;
import ph.gov.da.bfar.frgis.web.util.IResourceMapsData;
import ph.gov.da.bfar.frgis.web.util.PageUtils;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
@RequestMapping("/home")
public class HomeMapController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	MapsService mapsService;
	
	@Autowired
	RegionsService regionsService;
	
	@Autowired
	FishSanctuariesService fishSanctuariesService;
	
	@Autowired
	FishProcessingPlantService fishProcessingPlantService;
	
	@Autowired
	FishLandingService fishLandingService;
	
	@Autowired
	FishPortService fishPortService;
	
	@Autowired
	FishPenService fishPenService;
	
	@Autowired
	FishCageService fishCageService;

	@Autowired
	IcePlantColdStorageService icePlantColdStorageService;
	
	@Autowired
	MarketService marketService;

	@Autowired
	SchoolOfFisheriesService schoolOfFisheriesService;
	
	@Autowired
	FishCorralService fishCorralService;
	
	@Autowired
	SeaGrassService seaGrassService;
	
	@Autowired
	SeaweedsService seaweedsService;
	
	@Autowired
	MangroveService mangroveService;
	
	@Autowired
	LGUService lGUService;
	
	@Autowired
	MaricultureZoneService zoneService;
	
	@Autowired
	TrainingCenterService trainingCenterService;
	
	@Autowired
	HatcheryService  hatcheryService;
	
	@Autowired
	NationalCenterService nationalCenterService;
	
	@Autowired
	RegionalOfficeService regionalOfficeService;
	
	@Autowired
	TOSService tosService;
	@Autowired
	PayaoService payaoService;
	
	@Autowired
	 private IAuthenticationFacade authenticationFacade;

	@Autowired
	ChartsInterface chartsUtil;
	
	@Autowired
	IResourceMapsData resourceMapsData;
	@RequestMapping(value = "/Hfishsanctuaries", method = RequestMethod.GET)
	public ModelAndView fishsanctuaries(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		
		result = new ModelAndView("home/fishsanctuaries");
		
		ObjectMapper mapper = new ObjectMapper();
		List<MapsData> mapsDatas = mapsService.findAllMapsByResources("fishsanctuaries");
		String jsonString = mapper.writeValueAsString(mapsDatas);

		model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
		//model.addAttribute("page", "home_map");
		model.addAttribute("mapsData", jsonString);
		
		return result;
	}
	
	@RequestMapping(value = "/regionp", method = RequestMethod.GET)
	public ModelAndView regionp(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		
		result = new ModelAndView("region_home");
				
		return result;
	}
	
	
	@RequestMapping(value = "/hLegislated/{legislated}", method = RequestMethod.GET)
	public ModelAndView hatcheryLegislated(@PathVariable("legislated") String legislated,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findallMapsOfHatcheryByLegislated("hatchery", legislated);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			String lString = mapper.writeValueAsString(legislated);
			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
			model.addAttribute("hl", lString);
		
		result = new ModelAndView("home/fishHatchery");
		
	
		return result;
	}
	
	@RequestMapping(value = "/Hfishprocessingplants", method = RequestMethod.GET)
	public ModelAndView fishprocessingplants(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
		 System.out.println( "fishprocessingplants");
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("fishprocessingplants");
			String jsonString = mapper.writeValueAsString(mapsDatas);
			System.out.println(mapsDatas);
			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("home/fishprocessingplants");

	
		return result;
	}
	@RequestMapping(value = "/HfishLanding", method = RequestMethod.GET)
	public ModelAndView fishLanding(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("fishlanding");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("home/fishLanding");
		
		return result;
	}
	@RequestMapping(value = "/HfishPen", method = RequestMethod.GET)
	public ModelAndView fishPenMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("fishpen");
			String jsonString = mapper.writeValueAsString(mapsDatas);
			
		

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("home/fishPen");
		
	
		return result;
	}
	@RequestMapping(value = "/HfishCage", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("fishcage");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("home/fishCage");
		
	
		return result;
	}
	@RequestMapping(value = "/HfishPond", method = RequestMethod.GET)
	public ModelAndView fishPondMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("fishpond");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("home/fishPond");
		
	
		return result;
	}
	@RequestMapping(value = "/HHatchery", method = RequestMethod.GET)
	public ModelAndView HatcheryMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("hatchery");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("home/fishHatchery");
		
	
		return result;
	}
	
	@RequestMapping(value = "/HIPCS", method = RequestMethod.GET)
	public ModelAndView IPCSMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("iceplantorcoldstorage");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("home/IPCS");
		
	
		return result;
	}
	
	@RequestMapping(value = "/HFishPort", method = RequestMethod.GET)
	public ModelAndView FishPortMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("FISHPORT");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("home/fishPort");
		
	
		return result;
	}
	
	@RequestMapping(value = "/HfishCorral", method = RequestMethod.GET)
	public ModelAndView fishCorralMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("fishcorals");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
			
		result = new ModelAndView("home/fishCoral");
		
	
		return result;
	}
	
	@RequestMapping(value = "/HseaGrass", method = RequestMethod.GET)
	public ModelAndView seaGrassMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("seagrass");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("home/seaGrass");
		
	
		return result;
	}
	@RequestMapping(value = "/HseaWeeds", method = RequestMethod.GET)
	public ModelAndView seaWeedsMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("seaweeds");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
			
		result = new ModelAndView("home/seaWeeds");
		
	
		return result;
	}
	
	@RequestMapping(value = "/Hmangrove", method = RequestMethod.GET)
	public ModelAndView mangroveMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("mangrove");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("home/mangrove");
		
	
		return result;
	}
	
	@RequestMapping(value = "/Hmariculture", method = RequestMethod.GET)
	public ModelAndView maricultureMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("mariculturezone");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
			
		
		result = new ModelAndView("home/mariculture");
		
	
		return result;
	}
	
	@RequestMapping(value = "/Hmarket", method = RequestMethod.GET)
	public ModelAndView marketMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("market");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("home/market");
		
	
		return result;
	}
	
	@RequestMapping(value = "/Hschool", method = RequestMethod.GET)
	public ModelAndView schoolMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("schooloffisheries");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("home/school");
		
	
		return result;
	}
	
	@RequestMapping(value = "/Hlgu", method = RequestMethod.GET)
	public ModelAndView lguMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("lgu");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("home/pfo");
		
	
		return result;
	}
	
	@RequestMapping(value = "/regionMap", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String regionMap()throws JsonGenerationException, JsonMappingException, IOException {
		User user = new User();
		 
		user = authenticationFacade.getUserInfo();
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(PageUtils.getRegionName(user.getRegion_id()));
		
		return jsonString;

	}
	@RequestMapping(value = "/regionMap/{region}", method = RequestMethod.GET)
	public ModelAndView regionMapHome(ModelMap model,@PathVariable("region") String region)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		
		result = new ModelAndView("region_home");
		ObjectMapper mapper = new ObjectMapper();
		String regionID = PageUtils.getRegionName(region);
		model.addAttribute("regionID", regionID);
		model.addAttribute("regionName", region);
		return result;
	}

	@RequestMapping(value = "/regionMapCenter", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String regionMapCenter()throws JsonGenerationException, JsonMappingException, IOException {
		User user = new User();
		 
		user = authenticationFacade.getUserInfo();
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(PageUtils.getCenterMapPerRegion(user.getRegion_id()));
		
		return jsonString;

	}
	@RequestMapping(value = "/userRegion", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String userRegion()throws JsonGenerationException, JsonMappingException, IOException {
		User user = new User();
		 
		user = authenticationFacade.getUserInfo();
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(user.getRegion());
		
		return jsonString;

	}
	@RequestMapping(value = "/Htraining", method = RequestMethod.GET)
	public ModelAndView trainingMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources("trainingcenter");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("home/training");
		
	
		return result;
	}

	@RequestMapping(value = "/resources-get-by-pages-home", method = RequestMethod.GET)	
		public ModelAndView PagesHomeData(
				@RequestParam(value="resources[]") String[] resources,ModelMap model
					) throws JsonGenerationException, JsonMappingException, IOException{
		 ModelAndView result;
		
		int i;
		String page;
		String jsonString = "";
		ObjectMapper mapper = new ObjectMapper();
		for (i = 0; i < resources.length; i++) {
			  
            // accessing each element of array
			page = resources[i];
            System.out.print(page + " Pages to fetch in Database   ::" + "\r");
            
            List<MapsData>  mapsDatas = mapsService.findAllMapsByResources(page);
             
             jsonString += mapper.writeValueAsString(mapsDatas) + "\r";
             
             System.out.print(jsonString);
        }

	
			
		
			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
			result = new ModelAndView("home");
        
			return result;
		}

	@RequestMapping(value = "/getFPData/{page}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishcageData(@PathVariable("page") String page)
			throws JsonGenerationException, JsonMappingException, IOException {


		 ObjectMapper mapper = new ObjectMapper();
		 System.out.println( "fishprocessingplants" + "/getFPData/" +page);
			List<MapsData> mapsDatas = mapsService.findAllMapsByResources(page);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			System.out.println(jsonString);
		return jsonString;

	}
	
	@RequestMapping(value = "/getAllRegion", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllRegion()throws JsonGenerationException, JsonMappingException, IOException {

		 ObjectMapper mapper = new ObjectMapper();
		
			List<Regions> regions = regionsService.findAllRegions();
			String jsonString = mapper.writeValueAsString(regions);

		return jsonString;

	}
	
	@RequestMapping(value = "/getAllGeoJsonHome", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

	//	return alltoJson(resourceMapsData.featuresListAll(resources));
		return alltoJson(allResourcesFeaturesList());

	}
	@RequestMapping(value = "/getAllGeoJsonHome/{resources}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllGeoJson(@PathVariable("resources") String resources)throws JsonGenerationException, JsonMappingException, IOException {

		return resourceMapsData.alltoJson(resourceMapsData.featuresListAll(resources));
	

	}
	@RequestMapping(value = "/getAllGeoJsonHome/{resources}/{type}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getAllGeoJson(@PathVariable("resources") String resources,@PathVariable("type") String type)throws JsonGenerationException, JsonMappingException, IOException {

		return resourceMapsData.alltoJson(resourceMapsData.featuresListAll(resources, type));
	

	}

	@RequestMapping(value = "/getFSGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFSGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("fishsanctuaries"));

	}
	@RequestMapping(value = "/getFSGeoJson/{uniqueKey}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFSGeoJsonbyID(@PathVariable("uniqueKey") String uniqueKey)throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresListByID("fishsanctuaries",uniqueKey));

	}
	@RequestMapping(value = "/getFPGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFPGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("fishprocessingplants"));

	}
	@RequestMapping(value = "/getFLGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFLGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("fishlanding"));

	}
	
	@RequestMapping(value = "/getFPTGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFPTGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("FISHPORT"));

	}
	
	@RequestMapping(value = "/getFPenGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFPenGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("fishpen"));

	}
	
	@RequestMapping(value = "/getFCageGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFCageGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("fishcage"));

	}
	
	@RequestMapping(value = "/getIPCSGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getIPCSGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("iceplantorcoldstorage"));

	}
	
	@RequestMapping(value = "/getMarketGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMarketGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("market"));

	}
	
	@RequestMapping(value = "/getSchoolGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSchoolGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("schooloffisheries"));

	}
	@RequestMapping(value = "/getFishCoralsGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishCoralsGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("fishcorals"));

	}
	
	@RequestMapping(value = "/getSeagrassGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSeagrassGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("seagrass"));

	}
	
	@RequestMapping(value = "/getSeaweedsGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSeaweedsGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("seaweeds"));

	}
	
	@RequestMapping(value = "/getMangroveGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMangroveGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("mangrove"));

	}
	
	@RequestMapping(value = "/getLGUGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getLGUGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("lgu"));

	}
	
	@RequestMapping(value = "/getMaricultureGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMaricultureGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("mariculturezone"));

	}
	
	@RequestMapping(value = "/getTrainingcenterGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getTrainingcenterGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		return toJson(featuresList("trainingcenter"));

	}
	
	private ArrayList<Features> allResourcesFeaturesList(){


		ArrayList<Features> list2 = new ArrayList<>();
		
			ArrayList<FishSanctuaries> fishsanctuariesList = null;


			fishsanctuariesList = (ArrayList<FishSanctuaries>) fishSanctuariesService.findAllFishSanctuaries();
	 			
	 			for(FishSanctuaries data : fishsanctuariesList) {
				
	 				Features features = new Features();
	 				features.setArea(data.getArea());
	 				features.setResources("fishsanctuaries");
	 				features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
	 				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	 				features.setLatitude(data.getLat());
	 				features.setLongtitude(data.getLon());
	 				features.setName(data.getNameOfFishSanctuary());
	 				features.setImage(data.getImage());
	 				list2.add(features);
	 			}

			ArrayList<FishProcessingPlant> fishprocessingplantsList = null;
	
			fishprocessingplantsList = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.findAllFishProcessingPlants();
			
					for(FishProcessingPlant data : fishprocessingplantsList) {
						Features features = new Features();
						features.setArea(data.getArea());
						features.setResources("fishprocessingplants");
						features.setRegion(data.getRegion());
						features.setRegion_id(data.getRegion_id());
						features.setProvince(data.getProvince());
						features.setProvince_id(data.getProvince());
						features.setMunicipality(data.getMunicipality());
						features.setMunicipality_id(data.getMunicipality_id());
						features.setOperator(data.getOperatorClassification());
						features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
						features.setLatitude(data.getLat());
						features.setLongtitude(data.getLon());
						features.setName(data.getNameOfProcessingPlants());
						features.setImage(data.getImage());
						list2.add(features);
					}

			ArrayList<FishLanding> fishlandingList = null;

			fishlandingList = (ArrayList<FishLanding>) fishLandingService.findAllFishLandings();
			
			for(FishLanding data : fishlandingList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("fishlanding");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfLanding());
				features.setImage(data.getImage());
				features.setCflcStatus(data.getCflc_status());
				list2.add(features);
			}

			ArrayList<Fishport> FishPortList = null;

			FishPortList = (ArrayList<Fishport>) fishPortService.findAllFishports();
			
			for(Fishport data : FishPortList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("FISHPORT");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfFishport());
				features.setImage(data.getImage());
				list2.add(features);
				}

			ArrayList<FishPen> fishpenList = null;

			fishpenList = (ArrayList<FishPen>) fishPenService.findAllFishPens();
			
			for(FishPen data : fishpenList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("fishpen");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				//features.setName(data.get);
				features.setImage(data.getImage());
				list2.add(features);
			}

			ArrayList<FishCage> fishcageList = null;

			
			fishcageList = (ArrayList<FishCage>) fishCageService.findAllFishCages();
			
			for(FishCage data : fishcageList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("fishcage");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				list2.add(features);
			}

			
			ArrayList<IcePlantColdStorage> coldStorageList = null;

		
			coldStorageList = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.findAllIcePlantColdStorages();
			
			for(IcePlantColdStorage data : coldStorageList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("coldstorage");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setOperator(data.getOperator());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfIcePlant());
				features.setImage(data.getImage());
				list2.add(features);
			}

			ArrayList<Market> marketList = null;

			marketList = (ArrayList<Market>) marketService.findAllMarkets();
			
			for(Market data : marketList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("market");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfMarket());
				features.setImage(data.getImage());
				list2.add(features);
			}

			ArrayList<SchoolOfFisheries> schoolList = null;
			
			schoolList = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.findAllSchoolOfFisheriess();
			
			for(SchoolOfFisheries data : schoolList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("schoolOfFisheries");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getName());
				features.setImage(data.getImage());
				list2.add(features);
			}
		
			ArrayList<FishCoral> fishCoralList = null;

			fishCoralList = (ArrayList<FishCoral>) fishCorralService.findAllFishCorals();
			
			for(FishCoral data : fishCoralList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("fishcoral");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				list2.add(features);
			}

			
			ArrayList<SeaGrass> seagrassList = null;

			seagrassList = (ArrayList<SeaGrass>) seaGrassService.findAllSeaGrasss();
			
			for(SeaGrass data : seagrassList) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("seagrass");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				list2.add(features);
			}
			
			ArrayList<Seaweeds> seaweedsList = null;

			seaweedsList = (ArrayList<Seaweeds>) seaweedsService.findAllSeaweedss();
			
			for(Seaweeds data : seaweedsList) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("seaweeds");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setType(data.getType());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				
				list2.add(features);
			}
	
			ArrayList<Mangrove> mangroveList = null;

			
			mangroveList = (ArrayList<Mangrove>) mangroveService.findAllMangroves();
			
			for(Mangrove data : mangroveList) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("mangrove");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getType());
				features.setImage(data.getImage());
				list2.add(features);
			}
	
			ArrayList<LGU> lguList = null;

			lguList = (ArrayList<LGU>) lGUService.findAllLGUs();
			
			for(LGU data : lguList) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("lgu");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getLguName());
				features.setImage(data.getImage());
				list2.add(features);
			}
			
		
			
			ArrayList<MaricultureZone> maricultureList = null;
			
			maricultureList = (ArrayList<MaricultureZone>) zoneService.findAllMaricultureZones();
			
			for(MaricultureZone data : maricultureList) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("mariculturezone");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setType(data.getMaricultureType());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfMariculture());
				features.setImage(data.getImage());
				list2.add(features);
			}
			
		
			ArrayList<TrainingCenter> traininglist = null;

			
			traininglist = (ArrayList<TrainingCenter>) trainingCenterService.findAllTrainingCenters();
			
			for(TrainingCenter data : traininglist) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("trainingcenter");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getName());
				features.setImage(data.getImage());
				list2.add(features);
			}
			
			ArrayList<Hatchery> hatcherylist = null;

			
			hatcherylist = (ArrayList<Hatchery>) hatcheryService.findAllHatcherys();
			
			for(Hatchery data : hatcherylist) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("hatchery");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setStatus(data.getStatus());
				features.setLegislated(data.getLegislated());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfHatchery());
				features.setImage(data.getImage());
				list2.add(features);
			}
			
			ArrayList<NationalCenter> NClist = null;
			NClist = (ArrayList<NationalCenter>) nationalCenterService.findAllNationalCenter();
			
			for(NationalCenter data : NClist) {
	
				Features features = new Features();
				features.setResources("nationalcenter");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation(
						"<div class=\"p-3 mb-2 bg-primary text-white text-center\">NATIONAL CENTER</div>"+
								"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
								"<table class=\"table table-sm table-dark\">" + 
								"<tr><td>Location:</td> " + 
								"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
								"<tr><td>Name of Center:</td> " + 
								"<td>" +data.getName_of_center() + "</td></tr>" +
								"<tr><td>Description:</td> " + 
								"<td>" +data.getDescription() + "</td></tr>" +
								"<tr><td>Center Head:</td> " + 
								"<td>" +data.getCenter_head() + "</td></tr>" +
								"<tr><td>Number of Personnel:</td> " + 
								"<td>" +data.getNumber_of_personnel() + "</td></tr>" +
								"<tr><td>Coordinates:</td> " +
								"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");


				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getName_of_center());
				features.setImage(data.getImage());
				if (data.getId() % 2 == 0) {
					features.setDirection("left");	
				}else {
					features.setDirection("right");	
				}
				
				
				list2.add(features);
				
			}
			ArrayList<RegionalOffice> ROlist = null;
			ROlist = (ArrayList<RegionalOffice>) regionalOfficeService.findAllRegionalOffice();
			
			for(RegionalOffice data : ROlist) {
				Features features = new Features();
				features.setResources("regionaloffice");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation(
						 
								"<div class=\"p-3 mb-2 bg-primary text-white text-center\">REGIONAL OFFICE</div>"+
								"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
								"<table class=\"table table-sm table-dark\">" + 
								"<tr><td>Location:</td> " + 
								"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
								"<tr><td>Name of Director:</td> " + 
								"<td>" +data.getHead() + "</td></tr>" +
								"<tr><td>Number of Personnel:</td> " + 
								"<td>" +data.getNumber_of_personnel() + "</td></tr>" +
								"<tr><td>Coordinates:</td> " +
								"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getHead());
				//features.setImage(data.getImage());
				list2.add(features);
				
			}
			ArrayList<TOS> TOSlist = null;
			TOSlist = (ArrayList<TOS>) tosService.findAllTOS();
			
			for(TOS data : TOSlist) {
				Features features = new Features();
				features.setResources("tos");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation(
						 
						"<div class=\"p-3 mb-2 bg-primary text-white text-center\">TECHNOLOGY OUTREACH STATION</div>"+
						"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
						"<table class=\"table table-sm table-dark\">" + 
						"<tr><td>Location:</td> " + 
						"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
						"<tr><td>TOS Address:</td> " + 
						"<td>" +data.getAddress() + "</td></tr>" +
						
						"<tr><td>Coordinates:</td> " +
						"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getAddress());
				//features.setImage(data.getImage());
				list2.add(features);
			}

		return list2;
		
		
	
	}
	
	private ArrayList<Features> featuresList(String page){
		
		 List<String> roles = new ArrayList<String>();
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {

        }else {
        	Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        	 
           
     
            for (GrantedAuthority a : authorities) {
                roles.add(a.getAuthority());
            }
            
            if (roles.contains("ROLE_USER")) {

            }
            else if (roles.contains("ROLE_ENCODER")) {
  
            }
            else if (roles.contains("ROLE_ADMIN")) {

            }
            else if (roles.contains("ROLE_SUPERADMIN")) {

            }else if (roles.contains("ROLE_SUPERVISOR")) {

            }else {

            	
            }
        }
		
		ArrayList<Features> list2 = new ArrayList<>();
		
		if(page == "fishsanctuaries") {
			ArrayList<FishSanctuaries> list = null;
//			 if (roles.contains("ROLE_USER")) {
//				 list = (ArrayList<FishSanctuaries>) fishSanctuariesService.getListFishSanctuariesByUsername(authenticationFacade.getUsername());
//				 
//				 for(FishSanctuaries data : list) {
//		 				
//		 				Features features = new Features();
//		 				features.setArea(data.getArea());
//		 				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
//		 				features.setLatitude(data.getLat());
//		 				features.setLongtitude(data.getLon());
//		 				features.setName(data.getNameOfFishSanctuary());
//		 				features.setImage(data.getImage());
//		 				list2.add(features);
//		 			}
//				 
//			 }else if(roles.contains("ROLE_ADMIN")) {
//	        	 list = (ArrayList<FishSanctuaries>) fishSanctuariesService.findByRegionList(authenticationFacade.getRegionID());
//	        	 
//	        	 for(FishSanctuaries data : list) {
//	  				
//		 				Features features = new Features();
//		 				features.setArea(data.getArea());
//		 				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
//		 				features.setLatitude(data.getLat());
//		 				features.setLongtitude(data.getLon());
//		 				features.setName(data.getNameOfFishSanctuary());
//		 				features.setImage(data.getImage());
//		 				list2.add(features);
//		 			}
//	        	 
//	         }else {

	 			list = (ArrayList<FishSanctuaries>) fishSanctuariesService.findAllFishSanctuaries();
	 			
	 			for(FishSanctuaries data : list) {
 				
	 				Features features = new Features();
	 				features.setArea(data.getArea());
	 				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	 				features.setLatitude(data.getLat());
	 				features.setLongtitude(data.getLon());
	 				features.setName(data.getNameOfFishSanctuary());
	 				features.setImage(data.getImage());
	 				list2.add(features);
	 			}

		}else if(page == "fishprocessingplants") {
			ArrayList<FishProcessingPlant> list = null;
			
				if (roles.contains("ROLE_USER")) {
					list = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.ListByUsername(authenticationFacade.getUsername());
				
					for(FishProcessingPlant data : list) {
						Features features = new Features();
						features.setArea(data.getArea());
						features.setOperator(data.getOperatorClassification());
						features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
						features.setLatitude(data.getLat());
						features.setLongtitude(data.getLon());
						features.setName(data.getNameOfProcessingPlants());
						features.setImage(data.getImage());
						list2.add(features);
					}
				}else if(roles.contains("ROLE_ADMIN")) {
					list = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.ListByRegionList(authenticationFacade.getRegionID());
				
					for(FishProcessingPlant data : list) {
						Features features = new Features();
						features.setArea(data.getArea());
						features.setOperator(data.getOperatorClassification());
						features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
						features.setLatitude(data.getLat());
						features.setLongtitude(data.getLon());
						features.setName(data.getNameOfProcessingPlants());
						features.setImage(data.getImage());
						list2.add(features);
					}
				}else {	
					
					list = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.findAllFishProcessingPlants();
			
					for(FishProcessingPlant data : list) {
						Features features = new Features();
						features.setArea(data.getArea());
						features.setOperator(data.getOperatorClassification());
						features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
						features.setLatitude(data.getLat());
						features.setLongtitude(data.getLon());
						features.setName(data.getNameOfProcessingPlants());
						features.setImage(data.getImage());
						list2.add(features);
					}
				}
			
		}else if(page == "fishlanding") {
			ArrayList<FishLanding> list = null;
			
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<FishLanding>) fishLandingService.findByUsername(authenticationFacade.getUsername());
				
				for(FishLanding data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfLanding());
					features.setImage(data.getImage());
					list2.add(features);
				}
			}else if(roles.contains("ROLE_ADMIN")) {
				list = (ArrayList<FishLanding>) fishLandingService.findByRegion(authenticationFacade.getRegionID());
				
				for(FishLanding data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfLanding());
					features.setImage(data.getImage());
					list2.add(features);
				}
			}else {	
			
			list = (ArrayList<FishLanding>) fishLandingService.findAllFishLandings();
			
			for(FishLanding data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfLanding());
				features.setImage(data.getImage());
				list2.add(features);
			}
			}
		}else if(page == "FISHPORT") {
			ArrayList<Fishport> list = null;
			
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<Fishport>) fishPortService.findByUsername(authenticationFacade.getUsername());
				
				for(Fishport data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfFishport());
					features.setImage(data.getImage());
					list2.add(features);
				}
			}else if(roles.contains("ROLE_ADMIN")) {
				list = (ArrayList<Fishport>) fishPortService.findByRegion(authenticationFacade.getRegionID());
				
				for(Fishport data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfFishport());
					features.setImage(data.getImage());
					list2.add(features);
				}		
			}else {	
			
			list = (ArrayList<Fishport>) fishPortService.findAllFishports();
			
			for(Fishport data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfFishport());
				features.setImage(data.getImage());
				list2.add(features);
				}
			}
			
		}else if(page == "fishpen") {
			ArrayList<FishPen> list = null;
			
			if (roles.contains("ROLE_USER")) {

				list = (ArrayList<FishPen>) fishPenService.findByUsername(authenticationFacade.getUsername());
				
				for(FishPen data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					//features.setName(data.get);
					features.setImage(data.getImage());
					list2.add(features);
				}				
				
			}else if(roles.contains("ROLE_ADMIN")) {
				list = (ArrayList<FishPen>) fishPenService.findByRegion(authenticationFacade.getRegionID());
				
				for(FishPen data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					//features.setName(data.get);
					features.setImage(data.getImage());
					list2.add(features);
				}	
			}else {	
			
			list = (ArrayList<FishPen>) fishPenService.findAllFishPens();
			
			for(FishPen data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				//features.setName(data.get);
				features.setImage(data.getImage());
				list2.add(features);
			}
			}
		}else if(page == "fishcage") {
			ArrayList<FishCage> list = null;
			
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<FishCage>) fishCageService.getListFishCageByUsername(authenticationFacade.getUsername());
				
				for(FishCage data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage());
					list2.add(features);
				}				
				
			}else if(roles.contains("ROLE_ADMIN")) {
	
				list = (ArrayList<FishCage>) fishCageService.getListByRegion(authenticationFacade.getRegionID());
				
				for(FishCage data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage());
					list2.add(features);
				}				
			}else {	
			
			list = (ArrayList<FishCage>) fishCageService.findAllFishCages();
			
			for(FishCage data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				list2.add(features);
			}
			}
		}else if(page == "iceplantorcoldstorage") {
			
			ArrayList<IcePlantColdStorage> list = null;
			
			if (roles.contains("ROLE_USER")) {
		
				list = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.ListByUsername(authenticationFacade.getUsername());
				
				for(IcePlantColdStorage data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfIcePlant());
					features.setImage(data.getImage());
					list2.add(features);
				}
			}else if(roles.contains("ROLE_ADMIN")) {
		
				list = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.ListByRegion(authenticationFacade.getRegionID());
				
				for(IcePlantColdStorage data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfIcePlant());
					features.setImage(data.getImage());
					list2.add(features);
				}				
			}else {	
		
			list = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.findAllIcePlantColdStorages();
			
			for(IcePlantColdStorage data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfIcePlant());
				features.setImage(data.getImage());
				list2.add(features);
			}
			}
		}else if(page == "market") {
			ArrayList<Market> list = null;
						
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<Market>) marketService.ListByUsername(authenticationFacade.getUsername());
				
				for(Market data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfMarket());
					features.setImage(data.getImage());
					list2.add(features);
				}	
			}else if(roles.contains("ROLE_ADMIN")) {
				
				list = (ArrayList<Market>) marketService.ListByRegion(authenticationFacade.getRegionID());
				
				for(Market data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfMarket());
					features.setImage(data.getImage());
					list2.add(features);
				}
			}else {	
			
			list = (ArrayList<Market>) marketService.findAllMarkets();
			
			for(Market data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfMarket());
				features.setImage(data.getImage());
				list2.add(features);
			}
			}
		}else if(page == "schooloffisheries") {
			ArrayList<SchoolOfFisheries> list = null;
			
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.ListByUsername(authenticationFacade.getUsername());
				
				for(SchoolOfFisheries data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getName());
					features.setImage(data.getImage());
					list2.add(features);
				}
				
			}else if(roles.contains("ROLE_ADMIN")) {
				list = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.ListByRegion(authenticationFacade.getRegionID());
				
				for(SchoolOfFisheries data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getName());
					features.setImage(data.getImage());
					list2.add(features);
				}
			}else {	
			
			list = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.findAllSchoolOfFisheriess();
			
			for(SchoolOfFisheries data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getName());
				features.setImage(data.getImage());
				list2.add(features);
			}
			}
		}else if(page == "fishcorals") {
			
			ArrayList<FishCoral> list = null;
			
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<FishCoral>) fishCorralService.ListByUsername(authenticationFacade.getUsername());
				
				for(FishCoral data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage());
					list2.add(features);
				}
				
			}else if(roles.contains("ROLE_ADMIN")) {
				list = (ArrayList<FishCoral>) fishCorralService.ListByRegion(authenticationFacade.getRegionID());
				
				for(FishCoral data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage());
					list2.add(features);
				}
			}else {	
			
			list = (ArrayList<FishCoral>) fishCorralService.findAllFishCorals();
			
			for(FishCoral data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				list2.add(features);
			}
			}
		}else if(page == "seagrass") {
			
			ArrayList<SeaGrass> list = null;
			
			if (roles.contains("ROLE_USER")) {
			
				list = (ArrayList<SeaGrass>) seaGrassService.ListByUsername(authenticationFacade.getUsername());
				
				for(SeaGrass data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage());
					list2.add(features);
				}
				
			}else if(roles.contains("ROLE_ADMIN")) {
		
				list = (ArrayList<SeaGrass>) seaGrassService.ListByRegion(authenticationFacade.getRegionID());
				
				for(SeaGrass data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage());
					list2.add(features);
				}
			}else {	
			
			list = (ArrayList<SeaGrass>) seaGrassService.findAllSeaGrasss();
			
			for(SeaGrass data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				list2.add(features);
			}
			}
		}else if(page == "seaweeds") {
			
			ArrayList<Seaweeds> list = null;
			
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<Seaweeds>) seaweedsService.ListByUsername(authenticationFacade.getUsername());
				
				for(Seaweeds data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage());
					
					list2.add(features);
				}
				
			}else if(roles.contains("ROLE_ADMIN")) {
				list = (ArrayList<Seaweeds>) seaweedsService.ListByRegion(authenticationFacade.getRegionID());
				
				for(Seaweeds data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage());
					
					list2.add(features);
				}
				
			}else {	
			
			list = (ArrayList<Seaweeds>) seaweedsService.findAllSeaweedss();
			
			for(Seaweeds data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				
				list2.add(features);
			}
			}
		}else if(page == "mangrove") {
			
			ArrayList<Mangrove> list = null;
			
			if (roles.contains("ROLE_USER")) {
			
				list = (ArrayList<Mangrove>) mangroveService.ListByUsername(authenticationFacade.getUsername());
				
				for(Mangrove data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getType());
					features.setImage(data.getImage());
					list2.add(features);
				}
				
			}else if(roles.contains("ROLE_ADMIN")) {
		
				list = (ArrayList<Mangrove>) mangroveService.ListByRegion(authenticationFacade.getRegionID());
				
				for(Mangrove data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getType());
					features.setImage(data.getImage());
					list2.add(features);
				}
				
			}else {	
			
			list = (ArrayList<Mangrove>) mangroveService.findAllMangroves();
			
			for(Mangrove data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getType());
				features.setImage(data.getImage());
				list2.add(features);
			}
			}
		}else if(page == "lgu") {
			
			ArrayList<LGU> list = null;
			
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<LGU>) lGUService.ListByUsername(authenticationFacade.getUsername());
				
				for(LGU data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getLguName());
					features.setImage(data.getImage());
					list2.add(features);
				}
				
			}else if(roles.contains("ROLE_ADMIN")) {
		
				list = (ArrayList<LGU>) lGUService.ListByRegion(authenticationFacade.getRegionID());
				
				for(LGU data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getLguName());
					features.setImage(data.getImage());
					list2.add(features);
				}
			}else {	
	
			list = (ArrayList<LGU>) lGUService.findAllLGUs();
			
			for(LGU data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getLguName());
				features.setImage(data.getImage());
				list2.add(features);
			}
			}
		}else if(page == "mariculturezone") {
			
			ArrayList<MaricultureZone> list = null;
			
			if (roles.contains("ROLE_USER")) {
				
				list = (ArrayList<MaricultureZone>) zoneService.ListByUsername(authenticationFacade.getUsername());
				
				for(MaricultureZone data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfMariculture());
					features.setImage(data.getImage());
					list2.add(features);
				}
			}else if(roles.contains("ROLE_ADMIN")) {
		
				list = (ArrayList<MaricultureZone>) zoneService.ListByRegion(authenticationFacade.getRegionID());
				
				for(MaricultureZone data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfMariculture());
					features.setImage(data.getImage());
					list2.add(features);
				}
			}else {	
			
			list = (ArrayList<MaricultureZone>) zoneService.findAllMaricultureZones();
			
			for(MaricultureZone data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfMariculture());
				features.setImage(data.getImage());
				list2.add(features);
			}
			}
		}else if(page == "trainingcenter") {
			ArrayList<TrainingCenter> list = null;
			
			if (roles.contains("ROLE_USER")) {
				
				list = (ArrayList<TrainingCenter>) trainingCenterService.ListByUsername(authenticationFacade.getUsername());
				
				for(TrainingCenter data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getName());
					features.setImage(data.getImage());
					list2.add(features);
				}
			}else if(roles.contains("ROLE_ADMIN")) {
		
				list = (ArrayList<TrainingCenter>) trainingCenterService.ListByRegion(authenticationFacade.getRegionID());
				
				for(TrainingCenter data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getName());
					features.setImage(data.getImage());
					list2.add(features);
				}
			}else {	
			
			list = (ArrayList<TrainingCenter>) trainingCenterService.findAllTrainingCenters();
			
			for(TrainingCenter data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getName());
				features.setImage(data.getImage());
				list2.add(features);
			}
			}
		}
		
		
		
		return list2;
		
		
	}
	
	
	private ArrayList<Features> featuresListByID(String page, String uniqueKey){
		
		 List<String> roles = new ArrayList<String>();
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
       if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {

       }else {
       	Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
       	 
          
    
           for (GrantedAuthority a : authorities) {
               roles.add(a.getAuthority());
           }
           
           if (roles.contains("ROLE_USER")) {

           }
           else if (roles.contains("ROLE_ENCODER")) {
 
           }
           else if (roles.contains("ROLE_ADMIN")) {

           }
           else if (roles.contains("ROLE_SUPERADMIN")) {

           }else if (roles.contains("ROLE_SUPERVISOR")) {

           }else {

           	
           }
       }
		
		ArrayList<Features> list2 = new ArrayList<>();
		
		if(page == "fishsanctuaries") {
			ArrayList<FishSanctuaries> list = null;
			 if (roles.contains("ROLE_USER")) {
				 FishSanctuaries data = fishSanctuariesService.findByUniqueKey(uniqueKey);
				 
			
		 				
		 				Features features = new Features();
		 				features.setArea(data.getArea());
		 				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
		 				features.setLatitude(data.getLat());
		 				features.setLongtitude(data.getLon());
		 				features.setName(data.getNameOfFishSanctuary());
		 				features.setImage(data.getImage_src());
		 				list2.add(features);
		 	
				 
			 }else if(roles.contains("ROLE_ADMIN")) {
	        	 list = (ArrayList<FishSanctuaries>) fishSanctuariesService.findByRegionList(authenticationFacade.getRegionID());
	        	 
	        	 for(FishSanctuaries data : list) {
	  				
		 				Features features = new Features();
		 				features.setArea(data.getArea());
		 				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
		 				features.setLatitude(data.getLat());
		 				features.setLongtitude(data.getLon());
		 				features.setName(data.getNameOfFishSanctuary());
		 				features.setImage(data.getImage_src());
		 				list2.add(features);
		 			}
	        	 
	         }else {

	 			list = (ArrayList<FishSanctuaries>) fishSanctuariesService.findAllFishSanctuaries();
	 			
	 			for(FishSanctuaries data : list) {
				
	 				Features features = new Features();
	 				features.setArea(data.getArea());
	 				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	 				features.setLatitude(data.getLat());
	 				features.setLongtitude(data.getLon());
	 				features.setName(data.getNameOfFishSanctuary());
	 				features.setImage(data.getImage_src());
	 				list2.add(features);
	 			}
	         }
		}else if(page == "fishprocessingplants") {
			ArrayList<FishProcessingPlant> list = null;
			
				if (roles.contains("ROLE_USER")) {
					list = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.ListByUsername(authenticationFacade.getUsername());
				
					for(FishProcessingPlant data : list) {
						Features features = new Features();
						features.setArea(data.getArea());
						features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
						features.setLatitude(data.getLat());
						features.setLongtitude(data.getLon());
						features.setName(data.getNameOfProcessingPlants());
						features.setImage(data.getImage_src());
						list2.add(features);
					}
				}else if(roles.contains("ROLE_ADMIN")) {
					list = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.ListByRegionList(authenticationFacade.getRegionID());
				
					for(FishProcessingPlant data : list) {
						Features features = new Features();
						features.setArea(data.getArea());
						features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
						features.setLatitude(data.getLat());
						features.setLongtitude(data.getLon());
						features.setName(data.getNameOfProcessingPlants());
						features.setImage(data.getImage_src());
						list2.add(features);
					}
				}else {	
					
					list = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.findAllFishProcessingPlants();
			
					for(FishProcessingPlant data : list) {
						Features features = new Features();
						features.setArea(data.getArea());
						features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
						features.setLatitude(data.getLat());
						features.setLongtitude(data.getLon());
						features.setName(data.getNameOfProcessingPlants());
						features.setImage(data.getImage_src());
						list2.add(features);
					}
				}
			
		}else if(page == "fishlanding") {
			ArrayList<FishLanding> list = null;
			
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<FishLanding>) fishLandingService.findByUsername(authenticationFacade.getUsername());
				
				for(FishLanding data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfLanding());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			}else if(roles.contains("ROLE_ADMIN")) {
				list = (ArrayList<FishLanding>) fishLandingService.findByRegion(authenticationFacade.getRegionID());
				
				for(FishLanding data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfLanding());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			}else {	
			
			list = (ArrayList<FishLanding>) fishLandingService.findAllFishLandings();
			
			for(FishLanding data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfLanding());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
			}
		}else if(page == "FISHPORT") {
			ArrayList<Fishport> list = null;
			
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<Fishport>) fishPortService.findByUsername(authenticationFacade.getUsername());
				
				for(Fishport data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfFishport());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			}else if(roles.contains("ROLE_ADMIN")) {
				list = (ArrayList<Fishport>) fishPortService.findByRegion(authenticationFacade.getRegionID());
				
				for(Fishport data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfFishport());
					features.setImage(data.getImage_src());
					list2.add(features);
				}		
			}else {	
			
			list = (ArrayList<Fishport>) fishPortService.findAllFishports();
			
			for(Fishport data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfFishport());
				features.setImage(data.getImage_src());
				list2.add(features);
				}
			}
			
		}else if(page == "fishpen") {
			ArrayList<FishPen> list = null;
			
			if (roles.contains("ROLE_USER")) {

				list = (ArrayList<FishPen>) fishPenService.findByUsername(authenticationFacade.getUsername());
				
				for(FishPen data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					//features.setName(data.get);
					features.setImage(data.getImage_src());
					list2.add(features);
				}				
				
			}else if(roles.contains("ROLE_ADMIN")) {
				list = (ArrayList<FishPen>) fishPenService.findByRegion(authenticationFacade.getRegionID());
				
				for(FishPen data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					//features.setName(data.get);
					features.setImage(data.getImage_src());
					list2.add(features);
				}	
			}else {	
			
			list = (ArrayList<FishPen>) fishPenService.findAllFishPens();
			
			for(FishPen data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				//features.setName(data.get);
				features.setImage(data.getImage_src());
				list2.add(features);
			}
			}
		}else if(page == "fishcage") {
			ArrayList<FishCage> list = null;
			
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<FishCage>) fishCageService.getListFishCageByUsername(authenticationFacade.getUsername());
				
				for(FishCage data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage_src());
					list2.add(features);
				}				
				
			}else if(roles.contains("ROLE_ADMIN")) {
	
				list = (ArrayList<FishCage>) fishCageService.getListByRegion(authenticationFacade.getRegionID());
				
				for(FishCage data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage_src());
					list2.add(features);
				}				
			}else {	
			
			list = (ArrayList<FishCage>) fishCageService.findAllFishCages();
			
			for(FishCage data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
			}
		}else if(page == "iceplantorcoldstorage") {
			
			ArrayList<IcePlantColdStorage> list = null;
			
			if (roles.contains("ROLE_USER")) {
		
				list = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.ListByUsername(authenticationFacade.getUsername());
				
				for(IcePlantColdStorage data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfIcePlant());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			}else if(roles.contains("ROLE_ADMIN")) {
		
				list = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.ListByRegion(authenticationFacade.getRegionID());
				
				for(IcePlantColdStorage data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfIcePlant());
					features.setImage(data.getImage_src());
					list2.add(features);
				}				
			}else {	
		
			list = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.findAllIcePlantColdStorages();
			
			for(IcePlantColdStorage data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfIcePlant());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
			}
		}else if(page == "market") {
			ArrayList<Market> list = null;
						
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<Market>) marketService.ListByUsername(authenticationFacade.getUsername());
				
				for(Market data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfMarket());
					features.setImage(data.getImage_src());
					list2.add(features);
				}	
			}else if(roles.contains("ROLE_ADMIN")) {
				
				list = (ArrayList<Market>) marketService.ListByRegion(authenticationFacade.getRegionID());
				
				for(Market data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfMarket());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			}else {	
			
			list = (ArrayList<Market>) marketService.findAllMarkets();
			
			for(Market data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfMarket());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
			}
		}else if(page == "schooloffisheries") {
			ArrayList<SchoolOfFisheries> list = null;
			
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.ListByUsername(authenticationFacade.getUsername());
				
				for(SchoolOfFisheries data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getName());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
				
			}else if(roles.contains("ROLE_ADMIN")) {
				list = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.ListByRegion(authenticationFacade.getRegionID());
				
				for(SchoolOfFisheries data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getName());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			}else {	
			
			list = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.findAllSchoolOfFisheriess();
			
			for(SchoolOfFisheries data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getName());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
			}
		}else if(page == "fishcorals") {
			
			ArrayList<FishCoral> list = null;
			
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<FishCoral>) fishCorralService.ListByUsername(authenticationFacade.getUsername());
				
				for(FishCoral data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
				
			}else if(roles.contains("ROLE_ADMIN")) {
				list = (ArrayList<FishCoral>) fishCorralService.ListByRegion(authenticationFacade.getRegionID());
				
				for(FishCoral data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			}else {	
			
			list = (ArrayList<FishCoral>) fishCorralService.findAllFishCorals();
			
			for(FishCoral data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
			}
		}else if(page == "seagrass") {
			
			ArrayList<SeaGrass> list = null;
			
			if (roles.contains("ROLE_USER")) {
			
				list = (ArrayList<SeaGrass>) seaGrassService.ListByUsername(authenticationFacade.getUsername());
				
				for(SeaGrass data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
				
			}else if(roles.contains("ROLE_ADMIN")) {
		
				list = (ArrayList<SeaGrass>) seaGrassService.ListByRegion(authenticationFacade.getRegionID());
				
				for(SeaGrass data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			}else {	
			
			list = (ArrayList<SeaGrass>) seaGrassService.findAllSeaGrasss();
			
			for(SeaGrass data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
			}
		}else if(page == "seaweeds") {
			
			ArrayList<Seaweeds> list = null;
			
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<Seaweeds>) seaweedsService.ListByUsername(authenticationFacade.getUsername());
				
				for(Seaweeds data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage_src());
					
					list2.add(features);
				}
				
			}else if(roles.contains("ROLE_ADMIN")) {
				list = (ArrayList<Seaweeds>) seaweedsService.ListByRegion(authenticationFacade.getRegionID());
				
				for(Seaweeds data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage_src());
					
					list2.add(features);
				}
				
			}else {	
			
			list = (ArrayList<Seaweeds>) seaweedsService.findAllSeaweedss();
			
			for(Seaweeds data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage_src());
				
				list2.add(features);
			}
			}
		}else if(page == "mangrove") {
			
			ArrayList<Mangrove> list = null;
			
			if (roles.contains("ROLE_USER")) {
			
				list = (ArrayList<Mangrove>) mangroveService.ListByUsername(authenticationFacade.getUsername());
				
				for(Mangrove data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getType());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
				
			}else if(roles.contains("ROLE_ADMIN")) {
		
				list = (ArrayList<Mangrove>) mangroveService.ListByRegion(authenticationFacade.getRegionID());
				
				for(Mangrove data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getType());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
				
			}else {	
			
			list = (ArrayList<Mangrove>) mangroveService.findAllMangroves();
			
			for(Mangrove data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getType());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
			}
		}else if(page == "lgu") {
			
			ArrayList<LGU> list = null;
			
			if (roles.contains("ROLE_USER")) {
				list = (ArrayList<LGU>) lGUService.ListByUsername(authenticationFacade.getUsername());
				
				for(LGU data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getLguName());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
				
			}else if(roles.contains("ROLE_ADMIN")) {
		
				list = (ArrayList<LGU>) lGUService.ListByRegion(authenticationFacade.getRegionID());
				
				for(LGU data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getLguName());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			}else {	
	
			list = (ArrayList<LGU>) lGUService.findAllLGUs();
			
			for(LGU data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getLguName());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
			}
		}else if(page == "mariculturezone") {
			
			ArrayList<MaricultureZone> list = null;
			
			if (roles.contains("ROLE_USER")) {
				
				list = (ArrayList<MaricultureZone>) zoneService.ListByUsername(authenticationFacade.getUsername());
				
				for(MaricultureZone data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfMariculture());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			}else if(roles.contains("ROLE_ADMIN")) {
		
				list = (ArrayList<MaricultureZone>) zoneService.ListByRegion(authenticationFacade.getRegionID());
				
				for(MaricultureZone data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfMariculture());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			}else {	
			
			list = (ArrayList<MaricultureZone>) zoneService.ListByRegion(authenticationFacade.getRegionID());
			
			for(MaricultureZone data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfMariculture());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
			}
		}else if(page == "trainingcenter") {
			ArrayList<TrainingCenter> list = null;
			
			if (roles.contains("ROLE_USER")) {
				
				list = (ArrayList<TrainingCenter>) trainingCenterService.ListByUsername(authenticationFacade.getUsername());
				
				for(TrainingCenter data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getName());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			}else if(roles.contains("ROLE_ADMIN")) {
		
				list = (ArrayList<TrainingCenter>) trainingCenterService.ListByRegion(authenticationFacade.getRegionID());
				
				for(TrainingCenter data : list) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getName());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			}else {	
			
			list = (ArrayList<TrainingCenter>) trainingCenterService.findAllTrainingCenters();
			
			for(TrainingCenter data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getName());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
			}
		}else if(page == "hatchery") {
			ArrayList<Hatchery> list = null;
			list = (ArrayList<Hatchery>) hatcheryService.findAllHatcherys();
			
			for(Hatchery data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLegislated(data.getLegislated());
				features.setStatus(data.getStatus());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfHatchery());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
			
		}
		
		
		
		return list2;
		
		
	}
	
	
	
	private String alltoJson(ArrayList<Features> list){


		  JSONObject featureCollection = new JSONObject();
		  JSONArray features = new JSONArray();
		  for(Features eachElement : list){
			  
		    	JSONObject geometry = new JSONObject();
		    	JSONObject featuresProperties = new JSONObject();
		        JSONArray JSONArrayCoord = new JSONArray();
		        JSONObject feature = new JSONObject();
				  feature.put("type", "Feature");

		        JSONArrayCoord.put(0, eachElement.getLongtitude());
		        JSONArrayCoord.put(1, eachElement.getLatitude());

		        geometry.put("type", "Point");
		        geometry.put("coordinates", JSONArrayCoord);
		        
		        featuresProperties.put("Latitude", eachElement.getLongtitude());
		        featuresProperties.put("Longitude", eachElement.getLatitude());
		        featuresProperties.put("Name", eachElement.getName());
		        featuresProperties.put("Image", eachElement.getImage());
		        featuresProperties.put("Location", eachElement.getLocation());	
		        featuresProperties.put("Information", eachElement.getInformation());
		        featuresProperties.put("Legislative", eachElement.getLegislated());
		        featuresProperties.put("Type", eachElement.getType());
		        featuresProperties.put("CFLCSTATUS", eachElement.getCflcStatus());
		        featuresProperties.put("Operator", eachElement.getOperator());
		        featuresProperties.put("Status", eachElement.getStatus());
		        featuresProperties.put("resources", eachElement.getResources());
		        featuresProperties.put("Region", eachElement.getRegion());
		        featuresProperties.put("RegionID", eachElement.getRegion_id());
		        featuresProperties.put("Direction", eachElement.getDirection());
		        featuresProperties.put("Province", eachElement.getProvince_id());
		        featuresProperties.put("Municipality", eachElement.getMunicipality_id());
		        feature.put("properties", featuresProperties);
		        feature.put("geometry", geometry);
		        features.put(feature);
		  }

		 
	        featureCollection.put("features", features);
		
			 featureCollection.put("type", "FeatureCollection");
			  JSONObject properties = new JSONObject();
			  properties.put("name", "urn:ogc:def:crs:OGC:1.3:CRS84");
			  JSONObject crs = new JSONObject();
			  crs.put("type", "name");
			  crs.put("properties", properties);
			  featureCollection.put("crs", crs);
			  
		  return featureCollection.toString();
		}	
	
	private String toJson(ArrayList<Features> list){

	//	System.out.println(list.toString());

		  JSONObject featureCollection = new JSONObject();
		  JSONArray features = new JSONArray();
		  for(Features eachElement : list){
			  
		    	JSONObject geometry = new JSONObject();
		    	JSONObject featuresProperties = new JSONObject();
		        JSONArray JSONArrayCoord = new JSONArray();
		        JSONObject feature = new JSONObject();
				  feature.put("type", "Feature");

		        JSONArrayCoord.put(0, eachElement.getLongtitude());
		        JSONArrayCoord.put(1, eachElement.getLatitude());

		        geometry.put("type", "Point");
		        geometry.put("coordinates", JSONArrayCoord);
		        
		        featuresProperties.put("Latitude", eachElement.getLongtitude());
		        featuresProperties.put("Longitude", eachElement.getLatitude());
		        featuresProperties.put("Name", eachElement.getName());
		        featuresProperties.put("Image", eachElement.getImage());
		        featuresProperties.put("Location", eachElement.getLocation());	
		        featuresProperties.put("Operator", eachElement.getOperator());
		        feature.put("properties", featuresProperties);
		        feature.put("geometry", geometry);
		        features.put(feature);
		  }

		 
	        featureCollection.put("features", features);
		
			 featureCollection.put("type", "FeatureCollection");
			  JSONObject properties = new JSONObject();
			  properties.put("name", "urn:ogc:def:crs:OGC:1.3:CRS84");
			  JSONObject crs = new JSONObject();
			  crs.put("type", "name");
			  crs.put("properties", properties);
			  featureCollection.put("crs", crs);
			  
		  return featureCollection.toString();
		}
//	
//	{
//		"type": "FeatureCollection",
//		"name": "Status of Legislated Hatcheries",
//		"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
//		"features": [
//		{ "type": "Feature", "properties": { "RANo": "RA 10787", "Location": "Lingig, Surigao del Sur", "Status": "Ongoing Construction", "Latitude": 8.038053, "Longitude": 126.412664, "F7": null }, "geometry": { "type": "Point", "coordinates": [ 126.412664, 8.038053 ] } },
//	]
//	}
	
	
	
	@RequestMapping(value = "/postRegistration", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String saveRegistration(@RequestBody UserBean userbean) throws JsonGenerationException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		
		boolean getUsername = userService.isUserAlreadyPresent(userbean.getUsername());
		if(getUsername) {

			return objectMapper.writeValueAsString("Username Already taken!");
			
		}else {
			
			User user = new User();
			user.setName(userbean.getFirstName());
			user.setLastName(userbean.getLastName());
			user.setUsername(userbean.getUsername());
			user.setState("Active");
			user.setEmail(userbean.getEmail());
			user.setEnabled(true);
			user.setPass(userbean.getPassword());
			Set<UserProfile> userProfiles = new HashSet<UserProfile>();
			UserProfile profile = new UserProfile();
			
			String role = userbean.getUserProfiles();
			String[] role_data = role.split(",", 2);
			
			profile.setId(Integer.parseInt(role_data[0]));	
			profile.setType(role_data[1]);
			
			if(role_data[1].equals("REGION")) {
		
				String region = userbean.getRegion();
				String [] region_data = region.split(",", 2);
				user.setRegion_id(region_data[0]);
				user.setRegion(region_data[1]);
			}
			
			userProfiles.add(profile);
			user.setUserProfiles(userProfiles);
			//user.setPassword(userbean.getPassword());
						
			//userService.saveUser(user);
	
		}

		return objectMapper.writeValueAsString("User is registered successfully!");

	}
	
	@RequestMapping(value = "/getPAYAOGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getPAYAOGeoJson()throws JsonGenerationException, JsonMappingException, IOException {
		
		//FeaturesData  featuresData = new FeaturesData();
		//return alltoJson(featuresData.getFeaturesListByRegion(authenticationFacade.getRegionID()));
		return alltoJson(payaoListAll());

	}
	private ArrayList<Features> payaoListAll(){
		ArrayList<Features> list2 = new ArrayList<>();
		
		ArrayList<Payao> fslist = null;
		
		fslist = (ArrayList<Payao>) payaoService.findAllPayao();
		int sumOfAll = payaoService.PayaoSumAll();
		System.out.println("Total of all payao: " +sumOfAll);
		for(Payao data : fslist) {
			Features features = new Features();
			features.setResources("payao");
			features.setRegion(data.getRegion());
			features.setRegion_id(data.getRegion_id());
			features.setProvince(data.getProvince());
			features.setProvince_id(data.getProvince_id());
			features.setMunicipality(data.getMunicipality());
			features.setMunicipality_id(data.getMunicipality_id());
			//features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
			features.setInformation(
					 
					"<div class=\"p-3 mb-2 bg-primary text-white text-center\">PAYAO</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table class=\"table table-sm table-dark\">" + 
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Association:</td> " + 
					"<td>" +data.getAssociation() + "</td></tr>" +
					"<tr><td>Beneficiary:</td> " + 
					"<td>" +data.getBeneficiary() + "</td></tr>" +
					"<tr><td>Payao:</td> " + 
					"<td>" +data.getRemarks() + "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>");

			features.setLatitude(data.getLat());
			features.setLongtitude(data.getLon());
			features.setName(data.getBeneficiary());
			//features.setImage(data.getImage());
			list2.add(features);
		}
		
		
		return list2;
	}
	
	
}
