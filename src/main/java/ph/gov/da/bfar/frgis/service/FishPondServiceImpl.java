package ph.gov.da.bfar.frgis.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.FishPondPage;
import ph.gov.da.bfar.frgis.model.FishPond;
import ph.gov.da.bfar.frgis.repository.FishPondRepo;



@Service("FishPondService")
@Transactional
public class FishPondServiceImpl implements FishPondService{

	@Autowired
	private FishPondRepo dao;
	

//	@Autowired
//	MapsService mapsService;
	
	public Optional<FishPond> findById(int id) {
		return dao.findById(id);
	}
	
	@Override
	public FishPond findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	public List<FishPond> getFishPondByUsername(String username) {
		List<FishPond>fishPond = dao.getListFishPondByUsername(username);
		return fishPond;
	}

	public void saveFishPond(FishPond fishPond) {
				
			 dao.save(fishPond);		
	}

	@Override
	public void deleteFishPondById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateForEnabled(int id) {
		dao.updateByEnabled(id);
	}

	@Override
	public void updateFishPondByReason(int id, String reason) {
		dao.updateFishPondByReason(id, reason);
		
	}

	public List<FishPond> findAllFishPond() {
		return dao.findAll();
	}

	@Override
	public List<FishPond> findByRegion(String region_id) {
		
		return dao.getListFishPondByRegion(region_id);
	}

	@Override
	public List<FishPond> getAllFishPond(int index, int pagesize, String user) {
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<FishPond> page = dao.getPageableFishPond(user,secondPageWithFiveElements);
		
		return page;
	}

	@Override
	public int FishPondCount(String username) {
		return dao.FishPondCount(username);
	}

	@Override
	public List<FishPond> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<FishPondPage> findByUsername(String username, Pageable pageable) {
	
		return dao.findByUsername(username, pageable);
	}
	@Override
	public Page<FishPondPage> findByRegion(String region, Pageable pageable) {
		return dao.findByRegion(region, pageable);
	}
	
	@Override
	public Page<FishPondPage> findAllFishPond(Pageable pageable) {

	return dao.findAllFishPond(pageable);
}

	@Override
	public List<FishPond> findByRegionList(String regionID) {
		return dao.findByRegionList(regionID);
	}
	
	@Override
	public List<FishPond> findByProvinceList(String provinceID) {
		return dao.findByProvinceList(provinceID);
	}

	@Override
	public Long countFishPondPerRegion(String region_id) {

		return dao.countFishPondPerRegion(region_id);
	}

	@Override
	public Long countFishPondPerProvince(String province_id) {
	
		return dao.countFishPondPerProvince(province_id);
	}

	@Override
	public Long countAllFishPond() {
		
		return dao.countAllFishPond();
	}

	@Override
	public List<FishPond> getListFishPondByUsername(String user) {
		List<FishPond> fishPonds = dao.getListFishPondByUsername(user);
		return fishPonds;
	}


	
}
