package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.validation.Valid;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.FishCoralPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.FishCoral;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.service.FishCorralService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.PageUtils;
import ph.gov.da.bfar.frgis.web.util.ResourceData;
import ph.gov.da.bfar.frgis.web.util.ResourceMapsData;

@Controller
public class InvasivelController{
	

	
	@RequestMapping(value = "invasive" ,method = RequestMethod.GET)
	public ModelAndView invasive(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();


		result = new ModelAndView("invasive/index");
		return result;
	}
	@RequestMapping(value = "invasive/iec" ,method = RequestMethod.GET)
	public ModelAndView invasiveiec(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();


		result = new ModelAndView("invasive/iec");
		return result;
	}
	@RequestMapping(value = "invasive/proceedings" ,method = RequestMethod.GET)
	public ModelAndView invasiveproceedings(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();


		result = new ModelAndView("invasive/proceedings");
		return result;
	}
	@RequestMapping(value = "invasive/events" ,method = RequestMethod.GET)
	public ModelAndView invasiveEvents(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();


		result = new ModelAndView("invasive/events");
		return result;
	}
	@RequestMapping(value = "invasive/activities" ,method = RequestMethod.GET)
	public ModelAndView invasiveActivities(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();


		result = new ModelAndView("invasive/activities");
		return result;
	}
	@RequestMapping(value = "invasive/map" ,method = RequestMethod.GET)
	public ModelAndView invasivemap(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();


		result = new ModelAndView("invasive/map");
		return result;
	}
	@RequestMapping(value = "invasive/videogallery" ,method = RequestMethod.GET)
	public ModelAndView invasivegallery(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();


		result = new ModelAndView("invasive/videoGallery");
		return result;
	}
	@RequestMapping(value = "invasive/reference" ,method = RequestMethod.GET)
	public ModelAndView invasivereference(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();


		result = new ModelAndView("invasive/reference");
		return result;
	}
	@RequestMapping(value = "invasive/photogallery" ,method = RequestMethod.GET)
	public ModelAndView invasivephotogallery(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();


		result = new ModelAndView("invasive/photoGallery");
		return result;
	}
}
