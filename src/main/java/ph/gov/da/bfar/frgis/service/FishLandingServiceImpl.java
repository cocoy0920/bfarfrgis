package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.FishLandingPage;
import ph.gov.da.bfar.frgis.model.FishLanding;
import ph.gov.da.bfar.frgis.repository.FishLandingRepo;





@Service("FishLandingService")
@Transactional
public class FishLandingServiceImpl implements FishLandingService{

	@Autowired
	private FishLandingRepo dao;

	
	public Optional<FishLanding> findById(int id) {
		return dao.findById(id);
	}

	public List<FishLanding> findByUsername(String username) {
		List<FishLanding>FishLanding = dao.getListFishLandingByUsername(username);
		return FishLanding;
	}

	public void saveFishLanding(FishLanding FishLanding) {
		dao.save(FishLanding);
	}


	@Override
	public void updateForEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateFishLandingByReason(int id, String reason) {
		dao.updateFishLandingByReason(id, reason);
		
	}

	@Override
	public void deleteFishLandingById(int id) {
		dao.deleteById(id);
		
	}

	public List<FishLanding> findAllFishLandings() {
		return dao.findAll();
	}

	@Override
	public List<FishLanding> findAllFishLandings(String cflc_status) {
		
		return dao.findAllByType(cflc_status);
	}

	@Override
	public List<FishLanding> findAllFishLandingsByRegion(String region, String cflc_status) {
		
		return dao.findByRegion(region, cflc_status);
	}

	@Override
	public List<FishLanding> getAllFishLandings(int index, int pagesize, String user) {
		
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<FishLanding> page = dao.getPageableFishLandings(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int FishLandingCount(String username) {
		return dao.FishLandingCount(username);
	}

	@Override
	public FishLanding findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<FishLanding> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<FishLandingPage> findByUsername(String user, Pageable pageable) {
		
		return dao.findByUsername(user, pageable);
	}
	
	@Override
	public Page<FishLandingPage> findAllFishLanding(Pageable pageable) {

		return dao.findAllFishLanding(pageable);
	}
	
	@Override
	public Page<FishLandingPage> findByRegion(String region, Pageable pageable) {
	
		return dao.findByRegion(region, pageable);
	}

//	@Override
//	public List<FishLanding> getAllFishLandingByRegion(int index, int pagesize, String region_id) {
//		return  dao.getAllFishLandingByRegion(index, pagesize, region_id);
//	}
//
//	@Override
//	public List<FishLanding> getAllFishLandingByAllRegion(int index, int pagesize) {
//		return  dao.getAllFishLandingByAllRegion(index, pagesize);
//	}
//
//	@Override
//	public int FishLandingCountByMuunicipality(String municipalityID) {
//		return dao.FishLandingCountByMuunicipality(municipalityID);
//	}
//
//	@Override
//	public int FishLandingCountByProvince(String provinceID) {
//		return dao.FishLandingCountByProvince(provinceID);
//	}
//
//	@Override
//	public int FishLandingCountByRegion(String regionID) {
//		return dao.FishLandingCountByRegion(regionID);
//	}
//
	@Override
	public List<FishLanding> findByRegion(String regionID) {
		return dao.findByRegion(regionID);
	}
//
//	@Override
//	public List<FishLanding> findByProvince(String provinceID) {
//		return dao.findByProvince(provinceID);
//	}
//
//	@Override
//	public List<FishLanding> findByMunicipality(String municipalityID) {
//		return dao.findByMunicipality(municipalityID);
//	}

	@Override
	public Long countFishLandingPerRegion(String region_id) {
		return dao.countFishLandingPerRegion(region_id);
	}

	@Override
	public Long countFishLandingPerProvince(String province_id) {

		return dao.countFishLandingPerProvince(province_id);
	}

	@Override
	public Long countAllFishLanding() {
		
		return dao.countAllFishLanding();
	}

	@Override
	public Long countFishLandingPerMinicipality(String municipality_id) {
		return dao.countFishLandingPerMunicipality(municipality_id);
	}

	@Override
	public Long countFishLandingPerRegion(String region_id, String type) {

		return dao.countFishLandingPerRegion(region_id, type);
	}

	@Override
	public Long countFishLandingPerProvince(String province_id, String type) {

		return dao.countFishLandingPerProvince(province_id, type);
	}

	@Override
	public Long countFishLandingPerMinicipality(String municipality_id, String type) {
	
		return dao.countFishLandingPerMunicipality(municipality_id, type);
	}

	@Override
	public Long countFishLandingPerRegion(String region_id, String type, String cflc_status) {
		
		return dao.countFishLandingPerRegion(region_id, type, cflc_status);
	}

	@Override
	public Long countFishLandingPerProvince(String province_id, String type, String cflc_status) {
		
		return dao.countFishLandingPerProvince(province_id, type, cflc_status);
	}

	@Override
	public Long countFishLandingPerMinicipality(String municipality_id, String type, String cflc_status) {
		
		return dao.countFishLandingPerMunicipality(municipality_id, type, cflc_status);
	}

/*	public boolean isFishLandingSSOUnique(Integer id, String sso) {
		FishLanding FishLanding = findBySSO(sso);
		return ( FishLanding == null || ((id != null) && (FishLanding.getId() == id)));
	}*/
	
}
