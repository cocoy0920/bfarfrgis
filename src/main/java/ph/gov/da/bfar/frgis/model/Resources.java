package ph.gov.da.bfar.frgis.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@ToString
@Getter
@Setter
@Entity
@Table(name="resources")
public class Resources implements Serializable {
	

	

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id;
	
	private String user;
	
	private String uniqueKey;
	
	private String dateAsOf;
	
	private String region;
	
	private String region_id;
	
	private String province;
	
	private String province_id;
	
	private String municipality;
	
	private String municipality_id; 
	
	private String barangay;
	
	private String barangay_id;
	
	private String nameOfFishSanctuary;
	
	private String code;

	private String area;
	
	private String type;
	
	private String fishSanctuarySource;
	
	private String remarks;
	
	private String bfardenr;
	
	private String sheltered;
	
	//private String nameOfSensitiveHabitatMPA;
	
	//private String OrdinanceNo;
	
	//private String OrdinanceTitle;
	
	//private String DateEstablished;
	
	private String nameOfProcessingPlants;
	
	private String nameOfOperator;
	
	private String operatorClassification;
		
	private String processingTechnique;
	
	private String processingEnvironmentClassification;
	
	private String plantRegistered;
	
	private String bfarRegistered;
		
	private String packagingType;
	
	private String marketReach;
		
	private String businessPermitsAndCertificateObtained;
	
	private String indicateSpecies;
	
	
	private String encodedBy;
	
	private String editedBy;

	private String dateEncoded;
	
	private String dateEdited;
	
	private String lat;
	
	private String lon;

	private String image;
	
	private String page;
	
	private String image_src;
	private boolean enabled;
		


}
