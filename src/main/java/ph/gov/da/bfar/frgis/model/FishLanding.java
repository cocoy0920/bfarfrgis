package ph.gov.da.bfar.frgis.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter

@Entity
@Table(name="fishlanding")
public class FishLanding implements Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private int id;
	
	private String page;
	
	private String user;
	
	@Column(name = "unique_key")
	private String uniqueKey;
	
	@NotEmpty(message = "Date as Of may not be empty")
	@Column(name = "date_as_of")
	private String dateAsOf;
	
	private String region;
	
	private String region_id;
	
	@NotEmpty(message = "Province may not be empty")
	private String province;
	
	private String province_id;
	
	@NotEmpty(message = "Municipality may not be empty")
	private String municipality;
	
	private String municipality_id; 
	
	@NotEmpty(message = "Barangay may not be empty")
	private String barangay;
	
	private String barangay_id;
	
	private String code;
	
	@NotEmpty(message = "Name Of Landing may not be empty")
	@Column(name = "name_of_landing")
	private String nameOfLanding;
	
	private String classification;
	
	@NotEmpty(message = "Volume Of Unloading MT may not be empty")
	@Column(name = "volume_of_unloading_mt")
	private String volumeOfUnloadingMT;
	
	private String type;
	
	@Column(name = "cflc_status")
	private String cflc_status;
	
	@NotEmpty(message = "Area may not be empty")
	private String  area;
	
	@NotEmpty(message = "Data Source may not be empty")
	@Column(name = "data_source")
	private String dataSource;
	
	@NotEmpty(message = "Remarks may not be empty")
	private String remarks;
	
	@Column(name = "encode_by")
	private String encodedBy;
	
	@Column(name = "edited_by")
	private String editedBy;

	@Column(name = "date_encoded")
	private String dateEncoded;
	
	@Column(name = "date_edited")
	private String dateEdited;
	
	private String last_used;
	
	@NotEmpty(message = "Latitude may not be empty")
	private String lat;
	
	@NotEmpty(message = "Longitude may not be empty")
	private String lon;
	
	private String image;
	
	private String image_src;

	private boolean enabled;
	private String reason;
		
}
