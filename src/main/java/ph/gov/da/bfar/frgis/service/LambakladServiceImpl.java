package ph.gov.da.bfar.frgis.service;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ph.gov.da.bfar.frgis.Page.LambakladPage;
import ph.gov.da.bfar.frgis.model.Lambaklad;
import ph.gov.da.bfar.frgis.repository.LambakladRepo;




@Service("LambakladService")
@Transactional
public class LambakladServiceImpl implements LambakladService{


//	@Autowired
//	MapsService mapsService;
//	
//	
	@Autowired
	LambakladRepo cageRepo;
	
	public Optional<Lambaklad>findById(int id) {
		return cageRepo.findById(id);
	}

	public List<Lambaklad> getListLambakladByUsername(String username) {
		List<Lambaklad> Lambaklad = cageRepo.getListLambakladByUsername(username);
		return Lambaklad;
	}

	public void saveLambaklad(Lambaklad Lambaklad) {

		cageRepo.save(Lambaklad);
		 
	}

	
	
	public void deleteLambaklad(int id) {
		cageRepo.deleteById(id);
	}

	@Override
	public void updateByEnabled(int id) {
		cageRepo.updateByEnabled(id);
		
	}

	@Override
	public void updateLambakladByReason(int id, String reason) {
		cageRepo.updateLambakladByReason(id, reason);
		
	}

	public List<Lambaklad> findAllLambaklads() {
		return (List<Lambaklad>) cageRepo.findAll();
	}

	@Override
	public List<Lambaklad> getAllLambaklads(int index, int pagesize, String user) {

		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<Lambaklad> page = cageRepo.getPageableLambaklads(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int LambakladCount(String username) {
		return cageRepo.LambakladCount(username);
	}

	@Override
	public Lambaklad findByUniqueKey(String uniqueKey) {
		
		return cageRepo.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<Lambaklad> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return cageRepo.findByUniqueKeyForUpdate(uniqueKey);
	}
	@Override
	public Page<LambakladPage> findByUsername(String user, Pageable pageable) {
		return cageRepo.findByUsername(user, pageable);
	}
	
	@Override
		public Page<LambakladPage> findByRegion(String region, Pageable pageable) {
		
			return cageRepo.findByRegion(region, pageable);
		}

	@Override
		public Page<LambakladPage> findAllLambaklad(Pageable pageable) {
			return cageRepo.findAllLambaklad(pageable);
		}
	@Override
	public List<Lambaklad> getListByRegion(String regionID) {

		return cageRepo.getListLambakladByRegion(regionID);
	}

	@Override
	public Long countLambakladPerRegion(String region_id) {
		
		return cageRepo.countLambakladPerRegion(region_id);
	}

	@Override
	public Long countLambakladPerProvince(String province_id) {

		return cageRepo.countLambakladPerProvince(province_id);
	}

	@Override
	public Long countAllLambaklad() {

		return cageRepo.countAllLambaklad();
	}

	@Override
	public Long countLambakladPerMunicipality(String municipality_id) {
		
		return cageRepo.countLambakladPerMunicipality(municipality_id);
	}

//
//	@Override
//	public List<Lambaklad> getAllLambakladByRegion(int index, int pagesize, String region_id) {
//		return dao.getAllLambakladByRegion(index, pagesize, region_id);
//	}
//
//	@Override
//	public List<Lambaklad> getAllLambakladByAllRegion(int index, int pagesize) {
//		return dao.getAllLambakladByAllRegion(index, pagesize);
//	}
//
//	@Override
//	public int LambakladCountByMuunicipality(String municipalityID) {
//		return dao.LambakladCountByMuunicipality(municipalityID);
//	}
//
//	@Override
//	public int LambakladCountByProvince(String provinceID) {
//		return dao.LambakladCountByProvince(provinceID);
//	}
//
//	@Override
//	public int LambakladCountByRegion(String regionID) {
//		return dao.LambakladCountByRegion(regionID);
//	}
//
//	@Override
//	public List<Lambaklad> findByRegion(String regionID) {
//		return dao.findByRegion(regionID);
//	}
//
//	@Override
//	public List<Lambaklad> findByProvince(String provinceID) {
//		return dao.findByProvince(provinceID);
//	}
//
//	@Override
//	public List<Lambaklad> findByMunicipality(String municipalityID) {
//		return dao.findByMunicipality(municipalityID);
//	}


/*	public boolean isLambakladSSOUnique(Integer id, String sso) {
		Lambaklad Lambaklad = findBySSO(sso);
		return ( Lambaklad == null || ((id != null) && (Lambaklad.getId() == id)));
	}*/
	
}
