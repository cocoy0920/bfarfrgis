package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ph.gov.da.bfar.frgis.model.Regions;
import ph.gov.da.bfar.frgis.repository.RegionRepo;



@Service("RegionsService")
@Transactional
public class RegionsServiceImpl implements RegionsService{

	@Autowired
	private RegionRepo regionRepo;

	@Override
	public Optional<Regions> findById(int id) {
		
		return regionRepo.findById(id);
	}
	
	@Override
	public void save(Regions regions) {
		regionRepo.save(regions);
		
	}

	@Override
	public List<Regions> findAllRegions() {
		
		return regionRepo.findAll();
	}

	@Override
	public Regions findRegionDetails(String region_id) {

		return regionRepo.getRegionDetails(region_id);
	}


	
	
}
