package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.NationalCenterPage;
import ph.gov.da.bfar.frgis.model.NationalCenter;
import ph.gov.da.bfar.frgis.repository.NationalCenterRepo;



@Service("NationalCenterService")
@Transactional
public class NationalCenterServiceImpl implements NationalCenterService{

	@Autowired
	private NationalCenterRepo dao;

	@Override
	public Optional<NationalCenter> findById(int id) {

		return dao.findById(id);
	}

	@Override
	public List<NationalCenter> ListByUsername(String username) {

		return dao.getListNationalCenterByUsername(username);
	}

	@Override
	public List<NationalCenter> ListByRegion(String region_id) {

		return dao.getListNationalCenterByRegion(region_id);
	}

	@Override
	public NationalCenter findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<NationalCenter> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public void saveNationalCenter(NationalCenter NationalCenter) {
		dao.save(NationalCenter);
		
	}

	@Override
	public void deleteNationalCenterById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateByEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateNationalCenterByReason(int id, String reason) {
		dao.updateNationalCenterByReason(id, reason);
		
	}

	@Override
	public List<NationalCenter> findAllNationalCenter() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	public List<NationalCenter> getAllNationalCenter(int index, int pagesize, String user) {
		
		return dao.findAll();
	}

	@Override
	public int NationalCenterCount(String username) {
		
		return dao.NationalCenterCount(username);
	}

	@Override
	public Page<NationalCenterPage> findByUsername(String user, Pageable pageable) {
		
		return dao.findByUsername(user, pageable);
	}

	@Override
	public Page<NationalCenterPage> findByRegion(String region, Pageable pageable) {
		
		return dao.findByRegion(region, pageable);
	}

	@Override
	public Page<NationalCenterPage> findAllNationalCenter(Pageable pageable) {
		
		return dao.findAllNationalCenter(pageable);
	}

	@Override
	public Long countAllNationalCenter() {
		
		return dao.countAllNationalCenter();
	}

	@Override
	public Long countNationalCenterPerRegion(String region_id) {
		
		return dao.countNationalCenterPerRegion(region_id);
	}

	@Override
	public Long countNationalCenterProvince(String province_id) {

		return dao.countNationalCenterPerProvince(province_id);
	}


	
	

	
}
