package ph.gov.da.bfar.frgis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = "ph.gov.da.bfar.frgis")
public class FrgisApplication  extends SpringBootServletInitializer {
	
	 
	    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
	        return builder.sources(FrgisApplication.class);
	    }

	public static void main(String[] args) {
		SpringApplication.run(FrgisApplication.class, args);
	}
}
