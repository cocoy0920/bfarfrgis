package ph.gov.da.bfar.frgis.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@Entity
@Table(name="tos")
public class TOS implements Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String user;
	
	private String page;
	

	private String unique_key;
	
	private String region;
	
	private String region_id;
	
	private String province;
	
	private String province_id;
	
	private String municipality;
	
	private String municipality_id; 
	
	private String barangay;
	
	private String barangay_id;
		
	private String code;

	private String address;
	
	private String date_as_of;

	private String source_of_data;
	
	private String remarks;
	
	private String encode_by;
	
	private String edited_by;

	private String date_encoded;

	private String date_edited;
	
	private String last_used;
	
	private String lat;

	private String lon;
	
	private String image;
	
	private String image_src;
	
	private boolean enabled;
	private String reason;
		
}
