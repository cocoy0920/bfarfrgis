package ph.gov.da.bfar.frgis.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@ToString
@Getter
@Setter
@Entity
@Table(name="fishprocessingplants")
public class FishProcessingPlant implements Serializable {
	


	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String user;
	
	private String page;
	
	@Column(name = "unique_key")
	private String uniqueKey;
	
	@NotEmpty(message = "Date as Of may not be empty")
	@Column(name = "date_as_of")
	private String dateAsOf;
	
	private String region;
	
	private String region_id;
	
	@NotEmpty(message = "Province may not be empty")
	private String province;
	
	private String province_id;
	
	@NotEmpty(message = "Municipality may not be empty")
	private String municipality;
	
	private String municipality_id; 
	
	@NotEmpty(message = "Barangay may not be empty")
	private String barangay;
	
	private String barangay_id;
	
	@NotEmpty(message = "Area may not be empty")
	private String  area;
	
	private String code;
	@NotEmpty(message = "Remarks may not be empty")
	private String remarks;
	
	@NotEmpty(message = "Name Of Processing Plants may not be empty")
	@Column(name = "name_of_processing_plants")
	private String nameOfProcessingPlants;
	
	@NotEmpty(message = "Name Of Operator may not be empty")
	@Column(name = "name_of_operator")
	private String nameOfOperator;
	
	@Column(name = "operator_classification")	
	private String operatorClassification;
	
	@Column(name = "processing_technique")	
	private String processingTechnique;
	
	@Column(name = "processing_environment_classification")
	private String processingEnvironmentClassification;
	
	@Column(name = "plant_registered")
	private String plantRegistered;
	
	@Column(name = "bfar_registered")
	private String bfarRegistered;
	
	@Column(name = "packaging_type")	
	private String packagingType;
	
	@Column(name = "market_reach")
	private String marketReach;
	
	@Column(name = "business_permits_and_certificate_obtained")	
	private String businessPermitsAndCertificateObtained;
	
	@Column(name = "indicate_species")
	private String indicateSpecies;
			
	private String last_used;
		
	@NotEmpty(message = "Data Source may not be empty")
	@Column(name = "source_of_data")
	private String sourceOfData;
		
	@Column(name = "encode_by")
	private String encodedBy;
	
	@Column(name = "edited_by")
	private String editedBy;

	@Column(name = "date_encoded")
	private String dateEncoded;
	
	@Column(name = "date_edited")
	private String dateEdited;
	
	@NotEmpty(message = "Latitude may not be empty")
	private String lat;
	
	@NotEmpty(message = "Longitude may not be empty")
	private String lon;
	
	private String image;
	
	private String image_src;
	
	private boolean enabled;
	
	private String reason;
	
}
