package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.FishPenPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishPen;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.FishPenService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class FishPenController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	FishPenService fishPenService;
	
	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	MapsService mapsService;
	

	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;	
	@RequestMapping(value = "/fishpen", method = RequestMethod.GET)
	public ModelAndView fishpen(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();
		
		user = authenticationFacade.getUserInfo();
//		Set<UserProfile> setOfRole = new HashSet<>();
//		setOfRole = user.getUserProfiles();
//		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
//		UserProfile role = list.get(0);
		
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "fishpen");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}
	@RequestMapping(value = "/fishpens", method = RequestMethod.GET)
	public ModelAndView fishpens(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "fishpen");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}

	@RequestMapping(value = "/fishPenMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();
		 
		 ObjectMapper mapper = new ObjectMapper();
			FishPen data = fishPenService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	
		 	Features features = new Features();
			 features = MapBuilder.getFishPenMapData(data);
			 
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	mapsData.setIcon("/static/images/pin2022/FishPen.png");
		 	
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			map_data = jsonString;
		
		result = new ModelAndView("redirect:../fishpens");
		
	
		return result;
	}
	@RequestMapping(value = "/fishPenDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView fishPenDeleteById(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();
		// fishPenService.deleteFishPenById(id);
		 fishPenService.updateForEnabled(id);
		result = new ModelAndView("redirect:../fishpens");
		
	
		return result;
	}
	@RequestMapping(value = "/fishPenDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView fishPenDeleteById(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();

		 fishPenService.updateForEnabled(id);
		 fishPenService.updateFishPenByReason(id, reason);
		result = new ModelAndView("redirect:../../fishpens");
		
	
		return result;
	}
		
	@RequestMapping(value = "/fishPenMap", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "fishpen");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("users/map/fishpenmap");
		
	
		return result;
	}


	@RequestMapping(value = "/fishpen/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishpen(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<FishPenPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 5, Sort.by("id"));
	      
	          page = fishPenService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<FishPenPage> FishPenList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishPen(FishPenList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/fishpenList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getfishpenList() throws JsonGenerationException, JsonMappingException, IOException {

	//	user = getUserInfo();
		List<FishPen> list = null;

		list = fishPenService.findByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	@RequestMapping(value = "/getFishPen/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishPenById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<FishPen> mapsDatas = fishPenService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}

	@RequestMapping(value = "/saveFishPen", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postFishPen(@RequestBody FishPen fishpen) throws JsonGenerationException, JsonMappingException, IOException {

		
		savingResources(fishpen);
		

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);
	}

	private void savingResources(FishPen fishPen) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		UUID newID = UUID.randomUUID();
		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		fishPen.setRegion_id(region_data[0]);
		fishPen.setRegion(region_data[1]);

		province = fishPen.getProvince();
		data = province.split(",", 2);
		fishPen.setProvince_id(data[0]);
		fishPen.setProvince(data[1]);

		municipal = fishPen.getMunicipality();
		municipal_data = municipal.split(",", 2);
		fishPen.setMunicipality_id(municipal_data[0]);
		fishPen.setMunicipality(municipal_data[1]);

		barangay = fishPen.getBarangay();
		barangay_data = barangay.split(",", 3);
		fishPen.setBarangay_id(barangay_data[0]);
		fishPen.setBarangay(barangay_data[1]);
		fishPen.setEnabled(true);

		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(fishPen.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");

		if (fishPen.getId() != 0) {
			Optional<FishPen> pen = fishPenService.findById(fishPen.getId());
			String image_server = pen.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				fishPen.setImage("/images/" + image_server);
				newUniqueKeyforMap = fishPen.getUniqueKey();
				fishPenService.saveFishPen(ResourceData.getFishPen(fishPen, user, "", UPDATE));
			}else {
				newUniqueKeyforMap = fishPen.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", "");
				fishPen.setImage("/images/" + imageName);
				Base64Converter.convertToImage(fishPen.getImage_src(), fishPen.getImage());
				fishPenService.saveFishPen(ResourceData.getFishPen(fishPen, user, "", UPDATE));
			}			
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", "");
			fishPen.setImage("/images/" + imageName);
			Base64Converter.convertToImage(fishPen.getImage_src(), fishPen.getImage());
			fishPenService.saveFishPen(ResourceData.getFishPen(fishPen, user, newUniqueKey, SAVE));
			
			save = true;

		}
	}

	@RequestMapping(value = "/getLastPageFPen", method = RequestMethod.GET)
	public @ResponseBody String getLastPageFPen(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = fishPenService.FishPenCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}


}
