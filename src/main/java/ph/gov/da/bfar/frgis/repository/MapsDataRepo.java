package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.model.MapsData;

@Repository
public interface MapsDataRepo extends JpaRepository<MapsData, Integer> {

	@Query("SELECT c from MapsData c where c.user = :username")
	public List<MapsData> getListMapsDataByUsername(@Param("username") String username);
	
	@Query("SELECT COUNT(f) FROM MapsData f WHERE f.user=:username")
	public int  MapsDataCount(@Param("username") String username);
	
	@Query("SELECT c from MapsData c where c.uniqueKey = :uniqueKey")
	public List<MapsData> findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from MapsData c where c.uniqueKey = :uniqueKey")
	List<MapsData> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from MapsData c where c.user = :username")
	 public List<MapsData> getPageableMapsData(@Param("username") String username,Pageable pageable);

	@Query("SELECT c from MapsData c where c.user = :user and  c.page = :page")
	public List<MapsData> findAllMapsByUser(@Param("user") String user,@Param("page") String page);

	@Query("SELECT c from MapsData c where c.user = :user")
	public List<MapsData> findAllMapsByUser(@Param("user") String user);
	
	@Query("SELECT c from MapsData c where c.page = :page")
	public List<MapsData> findAllMapsByResources(@Param("page") String page);

	@Query("SELECT c from MapsData c where c.region_id = :region_id")
	public List<MapsData> findAllMapsByRegion(@Param("region_id") String region_id);
	
	@Query("SELECT c from MapsData c where c.page = :page and c.legislated = :legislated")
	List<MapsData> findallMapsOfHatcheryByLegislated(@Param("page") String page ,@Param("legislated") String legislated);
	
	@Query("SELECT c from MapsData c where c.user = :user and c.page = :page and c.legislated = :legislated")
	List<MapsData> findallMapsOfHatcheryByLegislated(@Param("user") String user, @Param("page") String page ,@Param("legislated") String legislated);
	
	@Query("SELECT c from MapsData c where c.page = :page and c.region_id = :region_id")
	public List<MapsData> findAllMapsByRegionAndResources(@Param("page") String region, @Param("region_id") String region_id);
}
