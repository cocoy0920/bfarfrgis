package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.FishProcessingPlantPage;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.repository.FishProcessingPlantRepo;



@Service("FishProcessingPlantService")
@Transactional
public class FishProcessingPlantServiceImpl implements FishProcessingPlantService{

	@Autowired
	private FishProcessingPlantRepo dao;

	
	public Optional<FishProcessingPlant> findById(int id) {
		return dao.findById(id);
	}

	public List<FishProcessingPlant> ListByUsername(String username) {
		List<FishProcessingPlant>FishProcessingPlant = dao.getListFishProcessingPlantByUsername(username);
		return FishProcessingPlant;
	}

	public void saveFishProcessingPlant(FishProcessingPlant FishProcessingPlant) {
		dao.save(FishProcessingPlant);
	}
	
	@Override
	public void deleteFishProcessingById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateForEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateForReason(int id, String reason) {
		dao.updateFishProcessingPlantByReason(id, reason);
		
	}

	public List<FishProcessingPlant> findAllFishProcessingPlants() {
		return dao.findAll();
	}

	@Override
	public List<FishProcessingPlant> ListByRegionList(String region_id, String type) {
		
		return dao.findByRegionAndType(region_id, type);
	}

	@Override
	public List<FishProcessingPlant> findAllFishProcessingPlants(String type) {
		
		return dao.findAllType(type);
	}

	@Override
	public List<FishProcessingPlant> getAllFishProcessingPlants(int index, int pagesize, String user) {
		
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<FishProcessingPlant> page = dao.getPageableFishProcessingPlant(user,secondPageWithFiveElements);
		return page;
	}

	@Override
	public int FishProcessingPlantCount(String username) {
		return dao.FishProcessingPlantCount(username);
	}

	@Override
	public FishProcessingPlant findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<FishProcessingPlant> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<FishProcessingPlantPage> findByUsername(String user, Pageable pageable) {
	
		return dao.findByUsername(user, pageable);
	}
	
	@Override
	public Page<FishProcessingPlantPage> findByRegion(String region, Pageable pageable) {
	
		return dao.findByRegion(region, pageable);
	}
	@Override
	public Page<FishProcessingPlantPage> findAllFishProcessing(Pageable pageable) {

		return dao.findAllFishProcessing(pageable);
	}
	

	@Override
	public List<FishProcessingPlant> ListByRegionList(String region_id) {
		
		return dao.findByRegionList(region_id);
	}

	@Override
	public List<FishProcessingPlant> ListByProvinceList(String province_id) {
		
		return dao.findByProvinceList(province_id);
	}

	@Override
	public Long countFishProcessingPlantPerRegion(String region_id) {

		return dao.countFishProcessingPlantPerRegion(region_id);
	}

	@Override
	public Long countFishProcessingPlantPerProvince(String province_id) {
		
		return dao.countFishProcessingPlantPerProvince(province_id);
	}

	@Override
	public Long countAllFishProcessingPlant() {

		return dao.countAllFishProcessingPlant();
	}

	@Override
	public Long countFishProcessingPlantPerMuunicipality(String municipality_id) {

		return dao.countFishProcessingPlantPerMunicipality(municipality_id);
	}

	@Override
	public Long countFishProcessingPlantPerRegion(String region_id, String operator_classification) {
		
		return dao.countFishProcessingPlantPerRegion(region_id, operator_classification);
	}

	@Override
	public Long countFishProcessingPlantPerProvince(String province_id, String operator_classification) {

		return dao.countFishProcessingPlantPerProvince(province_id, operator_classification);
	}

	@Override
	public Long countFishProcessingPlantPerMuunicipality(String municipality_id, String operator_classification) {
		
		return dao.countFishProcessingPlantPerMunicipality(municipality_id, operator_classification);
	}

/*	@Override
	public int FishProcessingPlantCountByMuunicipality(String municipalityID) {
		return dao.FishProcessingPlantCountByMuunicipality(municipalityID);
	}

	@Override
	public int FishProcessingPlantCountByProvince(String provinceID) {
		return dao.FishProcessingPlantCountByProvince(provinceID);
	}

	@Override
	public int FishProcessingPlantCountByRegion(String regionID) {
		return dao.FishProcessingPlantCountByRegion(regionID);
	}*/

/*	public boolean isFishProcessingPlantSSOUnique(Integer id, String sso) {
		FishProcessingPlant FishProcessingPlant = findBySSO(sso);
		return ( FishProcessingPlant == null || ((id != null) && (FishProcessingPlant.getId() == id)));
	}*/
	
}
