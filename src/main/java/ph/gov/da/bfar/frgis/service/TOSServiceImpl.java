package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.TOSPage;
import ph.gov.da.bfar.frgis.model.TOS;
import ph.gov.da.bfar.frgis.repository.TOSRepo;



@Service("TOSService")
@Transactional
public class TOSServiceImpl implements TOSService{

	@Autowired
	private TOSRepo dao;

	@Override
	public Optional<TOS> findById(int id) {

		return dao.findById(id);
	}

	@Override
	public List<TOS> ListByUsername(String username) {

		return dao.getListTOSByUsername(username);
	}

	@Override
	public List<TOS> ListByRegion(String region_id) {

		return dao.getListTOSByRegion(region_id);
	}

	@Override
	public TOS findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<TOS> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public void saveTOS(TOS TOS) {
		dao.save(TOS);
		
	}
	
	@Override
	public void deleteTOSById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateByEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateTOSByReason(int id, String reason) {
		dao.updateTOSByReason(id, reason);
		
	}

	@Override
	public List<TOS> findAllTOS() {

		return dao.findAll();
	}

	@Override
	public List<TOS> getAllTOS(int index, int pagesize, String user) {
		
		return dao.findAll();
	}

	@Override
	public int TOSCount(String username) {
		
		return dao.TOSCount(username);
	}

	@Override
	public Page<TOSPage> findByUsername(String user, Pageable pageable) {
		
		return dao.findByUsername(user, pageable);
	}

	@Override
	public Page<TOSPage> findByRegion(String region, Pageable pageable) {
		
		return dao.findByRegion(region, pageable);
	}

	@Override
	public Page<TOSPage> findAllTOS(Pageable pageable) {
		
		return dao.findAllTOS(pageable);
	}

	@Override
	public Long countAllTOS() {
		
		return dao.countAllTOS();
	}

	@Override
	public Long countTOSPerRegion(String region_id) {
		
		return dao.countTOSPerRegion(region_id);
	}

	@Override
	public Long countTOSProvince(String province_id) {

		return dao.countTOSPerProvince(province_id);
	}


	
	

	
}
