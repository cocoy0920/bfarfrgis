package ph.gov.da.bfar.frgis.model;

import java.io.Serializable;

public enum UserProfileType implements Serializable{
	USER("USER"),
	SUPERADMIN("SUPERADMIN"),
	CO("CO"),
	DBA("DBA"),
	PLANNING("PLANNING"),	
	REGIONALADMIN("REGIONALADMIN"),
	REGIONALENCODER("REGIONALENCODER"),
	PFOADMIN("PFOADMIN"),
	PFOENCODER("PFOENCODER");
	
	String userProfileType;
	
	private UserProfileType(String userProfileType){
		this.userProfileType = userProfileType;
	}
	
	public String getUserProfileType(){
		return userProfileType;
	}
	
}
