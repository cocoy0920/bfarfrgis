package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lowagie.text.DocumentException;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.FishCagePage;
import ph.gov.da.bfar.frgis.excel.FishcageExcelExporter;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.Provinces;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.pdf.FishcagePDFExporter;
import ph.gov.da.bfar.frgis.service.FishCageService;
import ph.gov.da.bfar.frgis.service.MapsService;
//import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.PageUtils;
import ph.gov.da.bfar.frgis.web.util.ResourceData;
import ph.gov.da.bfar.frgis.web.util.ResourceMapsData;

@Controller
@RequestMapping("/create")
public class FishCageController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	MapsService mapsService;
	
	@Autowired
	FishCageService fishCageService;
	
	 @Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;
	@RequestMapping(value = "/fishcages", method = RequestMethod.GET)
	public ModelAndView newfishcages(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();
		 map_data="";
		 user = new User();
		user = authenticationFacade.getUserInfo();
//		Set<UserProfile> setOfRole = new HashSet<>();
//		setOfRole = user.getUserProfiles();
//		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
//		UserProfile role = list.get(0);
		
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "fishcage");
		//model.addAttribute("role", role);
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}
	@RequestMapping(value = "/fishcage", method = RequestMethod.GET)
	public ModelAndView newfishcage(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();
		 user = new User();
			user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "fishcage");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());

		result = new ModelAndView("users/form");
		return result;
	}
	
	@GetMapping("/fishcage/pdf")
	public void fishcageToPDF(HttpServletResponse response)throws DocumentException,IOException {
		response.setContentType("application/pdf");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormat.format(new Date());
		
		String headerKey = "Content-Disposition";
		String headerValue = "attachement; filename=fishcage_" + currentDateTime + ".pdf";
		response.setHeader(headerKey, headerValue);
		
		List<FishCage> listFishCages = fishCageService.findAllFishCages();
		
		FishcagePDFExporter exporter = new FishcagePDFExporter(listFishCages);
		exporter.export(response);
		
	}
	
	@GetMapping("/fishcage/excel")
	public void exportToExcel(HttpServletResponse response) throws IOException{
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		
		String headerKey = "Content-Disposition";
		String headerValue = "attachement; filename=fishcage_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		
		List<FishCage> listFishCages = fishCageService.findAllFishCages();
		
		FishcageExcelExporter fishcageExcelExporter = new FishcageExcelExporter(listFishCages);
		
		fishcageExcelExporter.export(response);
	}
	
	@RequestMapping(value = "/fishCageMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();
		 
		 ObjectMapper mapper = new ObjectMapper();
			FishCage data = fishCageService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	
		 	 Features features = new Features();
			 features = MapBuilder.getFishCageMapData(data);
			 
		 	
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	mapsData.setIcon("/static/images/pin2022/FishCage.png");
		 	
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);

			map_data = jsonString;
		result = new ModelAndView("redirect:../fishcage");
		
	
		return result;
	}
	@RequestMapping(value = "/fishCageDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView fishCageDeleteById(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();
		 
		// fishCageService.deleteFishCage(uniqueKey);
		 fishCageService.updateFishCageByEnabled(id);

		result = new ModelAndView("redirect:../fishcage");
		
	
		return result;
	}
	@RequestMapping(value = "/fishCageDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView fishCageDeleteById(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();
		 
		// fishCageService.deleteFishCage(uniqueKey);
		 fishCageService.updateFishCageByEnabled(id);
		 fishCageService.updateFishCageByForReason(id, reason);
		result = new ModelAndView("redirect:../../fishcage");
		
	
		return result;
	}
	
	@RequestMapping(value = "/fishCageMap", method = RequestMethod.GET)
	public ModelAndView fishCageMAP(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "fishcage");
			String jsonString = mapper.writeValueAsString(mapsDatas);

			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
			//model.addAttribute("page", "home_map");
			model.addAttribute("mapsData", jsonString);
		
		result = new ModelAndView("users/map/fishcagemap");
		
	
		return result;
	}
	
    @RequestMapping(value = "/PostFishcages", method = RequestMethod.POST)
    public ModelAndView saveFishCage(@ModelAttribute("FishCageForm") @Validated FishCage FishCageForm,
            BindingResult result, ModelMap model) {
    	
    	ModelAndView modelAndView = new ModelAndView();
    	FishCage cage = new FishCage();
    	cage = FishCageForm;
    	
    	if (result.hasErrors()) {
//    		modelAndView.addObject("successMessage", "Please correct the errors in form!");
//			
    		
    		model.addAttribute("bindingResult", result);
    		System.out.println("FishCageForm: " + cage);
    		modelAndView.addObject("FishCageForm", cage);
        	modelAndView.setViewName("users/fishcage");
        	return modelAndView;
        
       }else {
    	   
      // 	System.out.println("SAVING FISH CAGE" + cage);
       	savingResources(cage);
      
       	model.addAttribute("SAVE", "data save FISH CAGE");
       	modelAndView.addObject("successMessage", "User is registered successfully!");
       	modelAndView.addObject("FishCageForm", new FishCage());
       }
    	
    	modelAndView.setViewName("users/fishcage");
    	return modelAndView;
     
    }
	@RequestMapping(value = "/fishcage/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishcage(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishCagePage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 5, Sort.by("id"));
	      
	          page = fishCageService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<FishCagePage> FishCageList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishCagePage(FishCageList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/fishcageList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getfishcageList() throws JsonGenerationException, JsonMappingException, IOException {

		List<FishCage> list = null;

		list = fishCageService.getListFishCageByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	
	@RequestMapping(value = "/fishcagePrintList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String fishcagePrintList() throws JsonGenerationException, JsonMappingException, IOException {

	//	user = getUserInfo();
		List<FishCage> list = null;

		list = fishCageService.getListFishCageByUsername(authenticationFacade.getUsername());


		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}


	@RequestMapping(value = "/getFishCage/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishCageById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<FishCage> mapsDatas = fishCageService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}

	@RequestMapping(value = "/savingFishCage", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postFishCage(@RequestBody FishCage fishCage) throws JsonGenerationException, JsonMappingException, IOException {

		savingResources(fishCage);
		
//		List<FishCage> list = null;
//
//		int pageIndex = 1;
//		int totalNumberOfRecords = 0;
//		int numberOfRecordsPerPage = 20;
//		int page = (pageIndex * numberOfRecordsPerPage) - numberOfRecordsPerPage;
//
//		totalNumberOfRecords = fishCageService.FishCageCount(authenticationFacade.getUsername());
//
//		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
//		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
//			noOfPages = noOfPages + 1;
//		}
//		StringBuilder builder = new StringBuilder();
//		builder.append("<ul class=\"pagination pagination-sm\">");
//
//		for (int i = 1; i <= noOfPages; i++) {
//			builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + i + "</a></li>");
//		}
//		builder.append("</ul>");
//		
//		list = fishCageService.getAllFishCages(page, numberOfRecordsPerPage, authenticationFacade.getUsername());
//		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
		//resourcesPaginations.setPageNumber(pageNumber);

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);

	}

//	@RequestMapping(value = "/savingFishCage", method = RequestMethod.GET, produces = "application/json")
//	public @ResponseBody String getFishCage(@RequestBody FishCage fishCage) throws JsonGenerationException, JsonMappingException, IOException {
//
//		List<FishCage> list = null;
//
//		int pageIndex = 1;
//		int totalNumberOfRecords = 0;
//		int numberOfRecordsPerPage = 20;
//		int page = (pageIndex * numberOfRecordsPerPage) - numberOfRecordsPerPage;
//
//		totalNumberOfRecords = fishCageService.FishCageCount(authenticationFacade.getUsername());
//
//		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
//		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
//			noOfPages = noOfPages + 1;
//		}
//		StringBuilder builder = new StringBuilder();
//		builder.append("<ul class=\"pagination pagination-sm\">");
//
//		for (int i = 1; i <= noOfPages; i++) {
//			builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + i + "</a></li>");
//		}
//		builder.append("</ul>");
//		
//		list = fishCageService.getAllFishCages(page, numberOfRecordsPerPage, authenticationFacade.getUsername());
//		String pageNumber = builder.toString();
//
//		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
//		resourcesPaginations.setFishCage(list);
//		resourcesPaginations.setPageNumber(pageNumber);
//
//		ObjectMapper objectMapper = new ObjectMapper();
//
//		return objectMapper.writeValueAsString(resourcesPaginations);
//	}
	private void savingResources(FishCage fishCage) {
		System.out.println("fishCage.getImage_src()" + fishCage.getImage_src());
		User user = new User();
		user = authenticationFacade.getUserInfo();
		//System.out.println("userInfo: " + user);
		boolean save =false;
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		
		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		fishCage.setRegion_id(region_data[0]);
		fishCage.setRegion(region_data[1]);

		province = fishCage.getProvince();
		data = province.split(",", 2);
		fishCage.setProvince_id(data[0]);
		fishCage.setProvince(data[1]);

		municipal = fishCage.getMunicipality();
		municipal_data = municipal.split(",", 2);
		fishCage.setMunicipality_id(municipal_data[0]);
		fishCage.setMunicipality(municipal_data[1]);

		barangay = fishCage.getBarangay();
		barangay_data = barangay.split(",", 3);
		fishCage.setBarangay_id(barangay_data[0]);
		fishCage.setBarangay(barangay_data[1]);
		

		fishCage.setPage("fishcage");
		fishCage.setEnabled(true);
		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(fishCage.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		
		//fishCage.setImage("/images/" +imageNameSrc + ".jpeg");
//        String imgType = Base64ImageUtil.getImageType(fishCage.getImage_src());
//        
//        String imageName = imageNameSrc + "fishCage_"+ java.time.LocalDate.now()+ "." + imgType;
//        
//        fishCage.setImage(imageName);
		
		

		if (fishCage.getId() != 0) {
			
			Optional<FishCage> fishcage = fishCageService.findById(fishCage.getId());
			
			String image_server = fishcage.get().getImage().replaceAll("/images/", "");
			if(image_server.equalsIgnoreCase(imageName)) {
				fishCage.setImage("/images/" + image_server);
				newUniqueKeyforMap = fishCage.getUniqueKey();
				fishCageService.saveFishCage(ResourceData.getFishCage(fishCage, user, "", UPDATE));
			}else {
				newUniqueKeyforMap = fishCage.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", "");  
				fishCage.setImage("/images/" + image_server);
				Base64Converter.convertToImage(fishCage.getImage_src(), fishCage.getImage());
				fishCageService.saveFishCage(ResourceData.getFishCage(fishCage, user, "", UPDATE));
			}
					
					
		/*			
			if (!fishCage.getImage_src().isEmpty()) {

				Base64Converter.convertToImage(fishCage.getImage_src(), fishCage.getImage());
				fishCageService.saveFishCage(ResourceData.getFishCage(fishCage, user, "", UPDATE));
				newUniqueKeyforMap = fishCage.getUniqueKey();
			} else {
				newUniqueKeyforMap = fishCage.getUniqueKey();
				fishCageService.saveFishCage(ResourceData.getFishCage(fishCage, user, "", UPDATE));
			}*/
			//List<MapsData> mapsDatas = mapsService.findByUniqueKey(fishCage.getUniqueKey());
			//mapsService.saveMapsData(ResourceMapsData.getMapDataUpdate(fishCage, mapsDatas));

			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			fishCage.setImage("/images/" +imageName );
			Base64Converter.convertToImage(fishCage.getImage_src(), fishCage.getImage());
			fishCageService.saveFishCage(ResourceData.getFishCage(fishCage, user, newUniqueKey, SAVE));
			//mapsService.saveMapsData(ResourceMapsData.getMapData(fishCage, user, newUniqueKey));

			save = true;

		}
	}
	
	@RequestMapping(value = "/getLastPageFC", method = RequestMethod.GET)
	public @ResponseBody String getLastPageFC(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = 10; //fishCageService.FishCageCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}



}
