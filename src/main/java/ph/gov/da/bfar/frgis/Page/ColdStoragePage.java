package ph.gov.da.bfar.frgis.Page;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class ColdStoragePage {
	
	
	private int id;

	private String uniqueKey;

	private String region;
	
	private String province;
	
	private String municipality;

	private String barangay;

	private String nameOfIcePlant;
	
	private String lat;

	private String lon;
	
	private boolean enabled;

	public ColdStoragePage(int id, String uniqueKey, String region, String province, String municipality,
			String barangay, String nameOfIcePlant, String lat, String lon, boolean enabled) {
		super();
		this.id = id;
		this.uniqueKey = uniqueKey;
		this.region = region;
		this.province = province;
		this.municipality = municipality;
		this.barangay = barangay;
		this.nameOfIcePlant = nameOfIcePlant;
		this.lat = lat;
		this.lon = lon;
		this.enabled = enabled;
	}



}
