package ph.gov.da.bfar.frgis.web.util;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.FishCoral;
import ph.gov.da.bfar.frgis.model.FishLanding;
import ph.gov.da.bfar.frgis.model.FishPen;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.Fishport;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;
import ph.gov.da.bfar.frgis.model.LGU;
import ph.gov.da.bfar.frgis.model.Mangrove;
import ph.gov.da.bfar.frgis.model.MaricultureZone;
import ph.gov.da.bfar.frgis.model.Market;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;
import ph.gov.da.bfar.frgis.model.SeaGrass;
import ph.gov.da.bfar.frgis.model.Seaweeds;
import ph.gov.da.bfar.frgis.model.TrainingCenter;
import ph.gov.da.bfar.frgis.service.FishCageService;
import ph.gov.da.bfar.frgis.service.FishCorralService;
import ph.gov.da.bfar.frgis.service.FishLandingService;
import ph.gov.da.bfar.frgis.service.FishPenService;
import ph.gov.da.bfar.frgis.service.FishPortService;
import ph.gov.da.bfar.frgis.service.FishProcessingPlantService;
import ph.gov.da.bfar.frgis.service.FishSanctuariesService;
import ph.gov.da.bfar.frgis.service.IcePlantColdStorageService;
import ph.gov.da.bfar.frgis.service.LGUService;
import ph.gov.da.bfar.frgis.service.MangroveService;
import ph.gov.da.bfar.frgis.service.MaricultureZoneService;
import ph.gov.da.bfar.frgis.service.MarketService;
import ph.gov.da.bfar.frgis.service.ProvinceService;
import ph.gov.da.bfar.frgis.service.RegionsService;
import ph.gov.da.bfar.frgis.service.RoleService;
import ph.gov.da.bfar.frgis.service.SchoolOfFisheriesService;
import ph.gov.da.bfar.frgis.service.SeaGrassService;
import ph.gov.da.bfar.frgis.service.SeaweedsService;
import ph.gov.da.bfar.frgis.service.TrainingCenterService;
import ph.gov.da.bfar.frgis.service.UserProfileService;
import ph.gov.da.bfar.frgis.service.UserService;

public class FeaturesData {
	
	@Autowired
	UserService userService;

	@Autowired
	RegionsService regionsService;
	
	@Autowired
	ProvinceService provinceService;
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	UserProfileService userProfileService;
	
	@Autowired
	FishSanctuariesService fishSanctuariesService;
	
	@Autowired
	FishProcessingPlantService fishProcessingPlantService;
	
	@Autowired
	FishLandingService fishLandingService;
	
	@Autowired
	FishPortService fishPortService;
	
	@Autowired
	FishPenService fishPenService;
	
	@Autowired
	FishCageService fishCageService;

	@Autowired
	IcePlantColdStorageService icePlantColdStorageService;
	
	@Autowired
	MarketService marketService;

	@Autowired
	SchoolOfFisheriesService schoolOfFisheriesService;
	
	@Autowired
	FishCorralService fishCorralService;
	
	@Autowired
	SeaGrassService seaGrassService;
	
	@Autowired
	SeaweedsService seaweedsService;
	
	@Autowired
	MangroveService mangroveService;
	
	@Autowired
	LGUService lGUService;
	
	@Autowired
	MaricultureZoneService zoneService;
	
	@Autowired
	TrainingCenterService trainingCenterService;
	
	
	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	public ArrayList<Features> getFeaturesListByRegion(String region){
		System.out.println("Region ID: " + region);

		
		ArrayList<Features> list2 = new ArrayList<>();
		ArrayList<FishSanctuaries> fslist = null;
		
		fslist = (ArrayList<FishSanctuaries>) fishSanctuariesService.findByRegionList(region);
	       	 
	       	 for(FishSanctuaries data : fslist) {
	       		System.out.println("FishSanctuaries: " + data);	
	       		Features features = new Features();
					features.setArea(data.getArea());
					features.setResources("fishsanctuaries");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					if(data.getBfardenr().equalsIgnoreCase("BFAR")) {
					features.setInformation("Name: " + data.getNameOfFishSanctuary() + "<br>" +
											"Implementer: " +data.getBfardenr() + "<br>" + 
											"Species: " + data.getSheltered() + "<br>" + 
											"Area: " + data.getArea() + "<br>" + 
											"Type: " + data.getType());
					}else if(data.getBfardenr().equalsIgnoreCase("DENR")){
						features.setInformation("Name: " +data.getNameOfFishSanctuary() + "<br>" +
								"Implementer: " +data.getBfardenr() + "<br>" + 
								"Name of Sensitive Habitat: " + data.getNameOfSensitiveHabitatMPA() + "<br>" + 
								"Ordinance No.: " + data.getOrdinanceNo() + "<br>" +
								"Ordinance Title: " +  data.getOrdinanceTitle()+ "<br>"+
								"Date Established: " + data.getDateEstablished()+"<br>" +
								"Area: " +data.getArea() + "<br>" + 
								"Type: " + data.getType());
					}else {
						features.setInformation("Name: " + data.getNameOfFishSanctuary() + "<br>" +
								"Implementer: " +data.getBfardenr() + "<br>" + 
								"Area: " + data.getArea() + "<br>" + 
								"Type: " + data.getType());
						
					}
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfFishSanctuary());
					features.setImage(data.getImage_src());
		 				list2.add(features);
		 			}
	       	 

			ArrayList<FishProcessingPlant> fplist = null;
			
					fplist = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.ListByRegionList(region);
				
					for(FishProcessingPlant data : fplist) {
						Features features = new Features();
						features.setArea(data.getArea());
						features.setResources("fishprocessingplants");
						features.setRegion(data.getRegion());
						features.setRegion_id(data.getRegion_id());
						features.setProvince(data.getProvince());
						features.setProvince_id(data.getProvince_id());
						features.setMunicipality(data.getMunicipality());
						features.setMunicipality_id(data.getMunicipality_id());
						features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
						features.setInformation("Name: " + data.getNameOfProcessingPlants()+ "<br>" +
												"Name of Operator: " + data.getNameOfOperator()+ "<br>" +
												"Area: " + data.getArea()+ "<br>" +
												"Classification: " + data.getOperatorClassification()+ "<br>" +
												"Processing Technique: " + data.getProcessingTechnique()+ "<br>" +
												"Processing Environment: " + data.getProcessingEnvironmentClassification()+ "<br>" +
												"BFAR Registered: " + data.getBfarRegistered()+ "<br>" +
												"Packaging Type: " + data.getPackagingType()+ "<br>" +
												"Market Reach: " + data.getMarketReach()+ "<br>" +
												"Species: " + data.getIndicateSpecies()+ "<br>" +
												"Packaging Type: " + data.getPackagingType());
						features.setLatitude(data.getLat());
						features.setLongtitude(data.getLon());
						features.setName(data.getNameOfProcessingPlants());
						features.setImage(data.getImage_src());
						list2.add(features);
					}

			ArrayList<FishLanding> fllist = null;

			fllist = (ArrayList<FishLanding>) fishLandingService.findByRegion(region);
				
				for(FishLanding data : fllist) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setResources("fishlanding");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("Name of Fish Landing: " + data.getNameOfLanding()+ "<br>" +
											"Area (sqm.): " + data.getArea() + "<br>" +
											"Volume of Unloading (MT): " + data.getVolumeOfUnloadingMT()+ "<br>" +
											"Classification: " + data.getClassification()+ "<br>" +
											"Type: " + data.getType());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfLanding());
					features.setImage(data.getImage_src());
					list2.add(features);
				}

			ArrayList<Fishport> portlist = null;

			portlist = (ArrayList<Fishport>) fishPortService.findByRegion(region);
				
				for(Fishport data : portlist) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setResources("FISHPORT");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("Name of FishPort: " + data.getNameOfFishport()+ "<br>" +
											"Operated by: " + data.getNameOfCaretaker()+ "<br>" +
											"Volume of Unloading: " + data.getVolumeOfUnloading()+ "<br>" +
											"Type: " + data.getType()+ "<br>" +
											"Classification: " + data.getClassification()+ "<br>" +
											"Area (sqm.): " +data.getArea());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfFishport());
					features.setImage(data.getImage_src());
					list2.add(features);
				}		

			ArrayList<FishPen> penlist = null;
			

			penlist = (ArrayList<FishPen>) fishPenService.findByRegion(region);
				
				for(FishPen data : penlist) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setResources("fishpen");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("Name of Operator: " + data.getNameOfOperator()+ "<br>" +
											"No. of Fish Pen: " + data.getNoOfFishPen()+ "<br>" +
											"Species Cultured: " + data.getSpeciesCultured()+ "<br>" +
											"Area (sqm.): "+ data.getArea());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					//features.setName(data.get);
					features.setImage(data.getImage_src());
					list2.add(features);
				}	

			ArrayList<FishCage> cagelist = null;
			

			cagelist = (ArrayList<FishCage>) fishCageService.getListByRegion(region);
				
				for(FishCage data : cagelist) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setResources("fishcage");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("Name of Operator: " + data.getNameOfOperator()+ "<br>" +
											"Classification of Operator: " + data.getClassificationofOperator()+ "<br>" +
											"Cage Dimension (LxWxH): " + data.getCageDimension()+ "<br>" +
											"Area (m3): " + data.getArea()+ "<br>" +
											"No. of Compartment: " + data.getCageNoOfCompartments()+ "<br>" +
											"Cage Type: " + data.getCageType()+ "<br>" +
											"Cage Design: " + data.getCageDesign()+ "<br>" +
											"Indicate Species: " + data.getIndicateSpecies());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage_src());
					list2.add(features);
				}				

			
			ArrayList<IcePlantColdStorage> coldlist = null;

			coldlist = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.ListByRegion(region);
				
				for(IcePlantColdStorage data : coldlist) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setResources("coldstorage");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("Name of Ice Plant/Cold Storage: " + data.getNameOfIcePlant()+ "<br>" +
											"Valid Sanitary Permit: " + data.getWithValidSanitaryPermit()+ "<br>" +
											"Operator: " + data.getOperator()+ "<br>" +
											"Type of Facility: " + data.getTypeOfFacility()+ "<br>" +
											"Structure Type: " + data.getStructureType()+ "<br>" +
											"Capacity: " + data.getCapacity()+ "<br>" +
											"Area (sqm.): " + data.getArea());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfIcePlant());
					features.setImage(data.getImage_src());
					list2.add(features);
				}				

			ArrayList<Market> marketlist = null;
						
			
			marketlist = (ArrayList<Market>) marketService.ListByRegion(region);
				
				for(Market data : marketlist) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setResources("market");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("Name of Market: " + data.getNameOfMarket()+ "<br>" +
											"Type: " + data.getPublicprivate()+ "<br>" +
											"Classification of Market: " + data.getMajorMinorMarket()+ "<br>" +
											"Volume Traded: " + data.getVolumeTraded()+ "<br>" +
											"Area (sqm.): " + data.getArea());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfMarket());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
		
			ArrayList<SchoolOfFisheries> schoollist = null;
			
			schoollist = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.ListByRegion(region);
				
				for(SchoolOfFisheries data : schoollist) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setResources("schoolOfFisheries");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("Name of School: " + data.getName()+ "<br>" +
											"Date Established: " + data.getDateEstablished()+ "<br>" +
											"Number of Student Enrolled: " + data.getNumberStudentsEnrolled()+ "<br>" +
											"Area (sqm.): " + data.getArea());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getName());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			
			ArrayList<FishCoral> corallist = null;
			
			corallist = (ArrayList<FishCoral>) fishCorralService.ListByRegion(region);
				
				for(FishCoral data : corallist) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setResources("fishcoral");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("Name of Operator: " + data.getNameOfOperator()+ "<br>" +
											"Operator Classification: " + data.getOperatorClassification()+ "<br>" +
											"Gear Use: " + data.getNameOfStationGearUse()+ "<br>" +
											"Unit Use: " + data.getNoOfUnitUse()+ "<br>" +
											"Fishes Caught: " + data.getFishesCaught()+ "<br>" +
											"Area (sqm.): " + data.getArea());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage());
					list2.add(features);
				}
			
			ArrayList<SeaGrass> grasslist = null;
			
			grasslist = (ArrayList<SeaGrass>) seaGrassService.ListByRegion(region);
				
				for(SeaGrass data : grasslist) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setResources("seagrass");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("Genus Species: " + data.getIndicateGenus() + "<br>" + 
											"Area (sqm.):" + data.getArea());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			
			ArrayList<Seaweeds> weedslist = null;
			
			
			weedslist = (ArrayList<Seaweeds>) seaweedsService.ListByRegion(region);
				
				for(Seaweeds data : weedslist) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setResources("seaweeds");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("Indicate Genus: " + data.getIndicateGenus() + "<br>" +
											"Cultured Method Used: " + data.getCulturedMethodUsed() + "<br>" + 
											"Cultured Period: " + data.getCulturedPeriod()+ "<br>" +
											"Volume per Cropping (Kgs.): " + data.getProductionKgPerCropping() + "<br>" + 
											"No. of Cropping per Year: " + data.getProductionNoOfCroppingPerYear()+ "<br>" +
											"Area (sqm.): " + data.getArea());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setImage(data.getImage_src());
					
					list2.add(features);
				}
				
			
			
			ArrayList<Mangrove> mangrovelist = null;
			
			
			mangrovelist = (ArrayList<Mangrove>) mangroveService.ListByRegion(region);
				
				for(Mangrove data : mangrovelist) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setResources("mangrove");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("Indicate Species: " + data.getIndicateSpecies()+ "<br>" +
											"Type: " + data.getType() + "<br>" + 
											"Area (sqm.) : " + data.getArea());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getType());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			
			ArrayList<LGU> pfolist = null;
			
			pfolist = (ArrayList<LGU>) lGUService.ListByRegion(region);
				
				for(LGU data : pfolist) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setResources("lgu");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("PFO Name: " + data.getLguName()+ "<br>" + 
											"Number of Municipality Coastal: " + data.getNoOfBarangayCoastal() + "<br>" + 
											"Number of Municipality Inland: " + data.getNoOfBarangayInland()+ "<br>" +
											"LGU Coastal Length(km.): " + data.getLguCoastalLength());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getLguName());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			
			ArrayList<MaricultureZone> zonelist = null;
		
			zonelist = (ArrayList<MaricultureZone>) zoneService.ListByRegion(region);
				
				for(MaricultureZone data : zonelist) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setResources("mariculturezone");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("Name of Mariculture: " + data.getNameOfMariculture()+ "<br>" +
											"Hectare:" + data.getArea() + "<br>" + 
											"Date Launched: " + data.getDateLaunched()+ "<br>" +
											"Date Enacted: " + data.getDateInacted() + "<br>" + 
											"eCcNumber: " + data.getEccNumber() + "<br>" + 
											"Mariculture Type: " + data.getMaricultureType() + "<br>" + 
											"Municipal Ordinance Number: " + data.getMunicipalOrdinanceNo() + "<br>" + 
											"Date Approved: " + data.getDateApproved());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfMariculture());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
			
			ArrayList<TrainingCenter> traininglist = null;
			
			traininglist = (ArrayList<TrainingCenter>) trainingCenterService.ListByRegion(region);
				
				for(TrainingCenter data : traininglist) {
					Features features = new Features();
					//features.setArea(data.getArea());
					features.setResources("trainingcenter");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("Name of Training Center: " + data.getName()+ "<br>" +
											"Specialization: " + data.getSpecialization()+ "<br>" +
											"Facility Within The Training Center: " + data.getFacilityWithinTheTrainingCenter()+ "<br>" +
											"Type: " + data.getType());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getName());
					features.setImage(data.getImage_src());
					list2.add(features);
				}
		
		
		
		
		
		return list2;
		
		

		
	}

}
