package ph.gov.da.bfar.frgis.web.util;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.FishCoral;
import ph.gov.da.bfar.frgis.model.FishLanding;
import ph.gov.da.bfar.frgis.model.FishPen;
import ph.gov.da.bfar.frgis.model.FishPond;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.Fishport;
import ph.gov.da.bfar.frgis.model.Hatchery;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;
import ph.gov.da.bfar.frgis.model.LGU;
import ph.gov.da.bfar.frgis.model.Lambaklad;
import ph.gov.da.bfar.frgis.model.Mangrove;
import ph.gov.da.bfar.frgis.model.MaricultureZone;
import ph.gov.da.bfar.frgis.model.Market;
import ph.gov.da.bfar.frgis.model.NationalCenter;
import ph.gov.da.bfar.frgis.model.Payao;
import ph.gov.da.bfar.frgis.model.RegionalOffice;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;
import ph.gov.da.bfar.frgis.model.SeaGrass;
import ph.gov.da.bfar.frgis.model.Seaweeds;
import ph.gov.da.bfar.frgis.model.TOS;
import ph.gov.da.bfar.frgis.model.TrainingCenter;
import ph.gov.da.bfar.frgis.service.BarangayService;
import ph.gov.da.bfar.frgis.service.FishCageService;
import ph.gov.da.bfar.frgis.service.FishCorralService;
import ph.gov.da.bfar.frgis.service.FishLandingService;
import ph.gov.da.bfar.frgis.service.FishPenService;
import ph.gov.da.bfar.frgis.service.FishPondService;
import ph.gov.da.bfar.frgis.service.FishPortService;
import ph.gov.da.bfar.frgis.service.FishProcessingPlantService;
import ph.gov.da.bfar.frgis.service.FishSanctuariesService;
import ph.gov.da.bfar.frgis.service.HatcheryService;
import ph.gov.da.bfar.frgis.service.IcePlantColdStorageService;
import ph.gov.da.bfar.frgis.service.LGUService;
import ph.gov.da.bfar.frgis.service.LambakladService;
import ph.gov.da.bfar.frgis.service.MangroveService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.MaricultureZoneService;
import ph.gov.da.bfar.frgis.service.MarketService;
import ph.gov.da.bfar.frgis.service.MunicipalityService;
import ph.gov.da.bfar.frgis.service.NationalCenterService;
import ph.gov.da.bfar.frgis.service.PayaoService;
import ph.gov.da.bfar.frgis.service.ProvinceService;
import ph.gov.da.bfar.frgis.service.RegionalOfficeService;
import ph.gov.da.bfar.frgis.service.RegionsService;
import ph.gov.da.bfar.frgis.service.SchoolOfFisheriesService;
import ph.gov.da.bfar.frgis.service.SeaGrassService;
import ph.gov.da.bfar.frgis.service.SeaweedsService;
import ph.gov.da.bfar.frgis.service.TOSService;
import ph.gov.da.bfar.frgis.service.TrainingCenterService;

@Component
public class ResourceMapsData implements IResourceMapsData{
	
	@Autowired
	ProvinceService provinceService;

	@Autowired
	RegionsService regionsService;

	@Autowired
	MunicipalityService municipalityService;
	
	@Autowired
	BarangayService barangayService;
	
	@Autowired
	MapsService mapsService;
	
	@Autowired
	FishSanctuariesService fishSanctuariesService;
	
	@Autowired
	FishProcessingPlantService fishProcessingPlantService;
	
	@Autowired
	FishLandingService fishLandingService;
	
	@Autowired
	FishPortService fishPortService;
	
	@Autowired
	FishPenService fishPenService;
	
	@Autowired
	FishCageService fishCageService;
	
	@Autowired
	FishPondService fishPondService;

	@Autowired
	IcePlantColdStorageService icePlantColdStorageService;
	
	@Autowired
	MarketService marketService;

	@Autowired
	SchoolOfFisheriesService schoolOfFisheriesService;
	
	@Autowired
	FishCorralService fishCorralService;
	
	@Autowired
	SeaGrassService seaGrassService;
	
	@Autowired
	SeaweedsService seaweedsService;
	
	@Autowired
	MangroveService mangroveService;
	
	@Autowired
	LGUService lGUService;
	
	@Autowired
	MaricultureZoneService zoneService;
	
	@Autowired
	TrainingCenterService trainingCenterService;
	
	@Autowired
	HatcheryService hatcheryService;
	
	@Autowired
	NationalCenterService nationalCenterService;
	
	@Autowired
	RegionalOfficeService regionalOfficeService;
	
	@Autowired
	TOSService tosService;
	
	@Autowired
	PayaoService payaoService;
	
	@Autowired
	LambakladService lambakladService;
	

	@Override
	public String alltoJson(ArrayList<Features> list) {



		  JSONObject featureCollection = new JSONObject();
		  JSONArray features = new JSONArray();
		  for(Features eachElement : list){
			  
		    	JSONObject geometry = new JSONObject();
		    	JSONObject featuresProperties = new JSONObject();
		        JSONArray JSONArrayCoord = new JSONArray();
		        JSONObject feature = new JSONObject();
				
		        feature.put("type", "Feature");

		        JSONArrayCoord.put(0, eachElement.getLongtitude());
		        JSONArrayCoord.put(1, eachElement.getLatitude());

		        geometry.put("type", "Point");
		        geometry.put("coordinates", JSONArrayCoord);
		        feature.put("geometry", geometry);
		        
		        featuresProperties.put("Latitude", eachElement.getLongtitude());
		        featuresProperties.put("Longitude", eachElement.getLatitude());
		        featuresProperties.put("Name", eachElement.getName());
		        featuresProperties.put("Image", eachElement.getImage());
		        featuresProperties.put("Location", eachElement.getLocation());	 
		        featuresProperties.put("Information", eachElement.getInformation());
		        featuresProperties.put("resources", eachElement.getResources());
		        featuresProperties.put("Legislative", eachElement.getLegislated());
		        featuresProperties.put("Type", eachElement.getType());
		        featuresProperties.put("CFLCSTATUS", eachElement.getCflcStatus());
		        featuresProperties.put("Operator", eachElement.getOperator());
		        featuresProperties.put("Status", eachElement.getStatus());
		        featuresProperties.put("RegionID", eachElement.getRegion_id());
		        featuresProperties.put("Region", eachElement.getRegion());
		        featuresProperties.put("Province", eachElement.getProvince_id());
		        featuresProperties.put("Municipality", eachElement.getMunicipality_id());
		        feature.put("properties", featuresProperties);
		       
		        features.put(feature);
		  }

		 
	        /*featureCollection.put("features", features);
		
			 featureCollection.put("type", "FeatureCollection");
			  JSONObject properties = new JSONObject();
			  properties.put("name", "urn:ogc:def:crs:OGC:1.3:CRS84");
			  JSONObject crs = new JSONObject();
			  crs.put("type", "name");
			  crs.put("properties", properties);
			  featureCollection.put("crs", crs);*/
		  featureCollection.put("type", "FeatureCollection");
			 
		  JSONObject properties = new JSONObject();
		  properties.put("name", "urn:ogc:def:crs:OGC:1.3:CRS84");
		  JSONObject crs = new JSONObject();
		  crs.put("type", "name");
		  crs.put("properties", properties);
		  featureCollection.put("crs", crs);
		  
		  featureCollection.put("features", features);
			

			  
		  return featureCollection.toString();
		
	}

	@Override
	public ArrayList<Features> featuresListByRegion(String region,String resources) {

		
		ArrayList<Features> list2 = new ArrayList<>();
		
		if(resources.equals("fishsanctuary")) {
			ArrayList<FishSanctuaries> fslist = null;		
			fslist = (ArrayList<FishSanctuaries>) fishSanctuariesService.findByRegionList(region);
			
		  	 
			for(FishSanctuaries data : fslist) {
			
			Features features = new Features();	
			features = MapBuilder.getFishSanctuary(data);
			
			list2.add(features);
			}
		}
//		if(resources.equals("fishprocessing")) {
//			ArrayList<FishProcessingPlant> fplist = null;
//			fplist = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.ListByRegionList(region);
//		
//			
//			for(FishProcessingPlant data : fplist) {
//			Features features = new Features();	
//			features = MapBuilder.getFishProcessingPlantMapData(data);
//			list2.add(features);
//				}
//		}
		if(resources.equals("schoolOfFisheries")) {
			ArrayList<SchoolOfFisheries> schoollist = null;		
			schoollist = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.ListByRegion(region);
		
			for(SchoolOfFisheries data : schoollist) {
			Features features = new Features();	
			features = MapBuilder.getSchoolOfFisheriesMapData(data);
			list2.add(features);
			}
		}
		if(resources.equals("tos")) {
			ArrayList<TOS> TOSlist = null;
			TOSlist = (ArrayList<TOS>) tosService.ListByRegion(region);
		
			for(TOS data : TOSlist) {
			Features features = new Features();
			features = MapBuilder.getTOSMapData(data);
			list2.add(features);
			}
		}
		if(resources.equals("lgu")) {
			ArrayList<LGU> pfolist = null;		
			pfolist = (ArrayList<LGU>) lGUService.ListByRegion(region);
		
			for(LGU data : pfolist) {
			Features features = new Features();	
			features = MapBuilder.getLGUMapData(data);
			list2.add(features);
			}
		}
		if(resources.equals("regionaloffices")) {
			ArrayList<RegionalOffice> ROlist = null;
			ROlist = (ArrayList<RegionalOffice>) regionalOfficeService.ListByRegion(region);
		
			for(RegionalOffice data : ROlist) {
			Features features = new Features();
			features = MapBuilder.getRegionalOfficeMapData(data);
			list2.add(features);
															
			}
		}
		if(resources.equals("nationalcenter")) {
			ArrayList<NationalCenter> NClist = null;
			NClist = (ArrayList<NationalCenter>) nationalCenterService.ListByRegion(region);
		
			for(NationalCenter data : NClist) {
			Features features = new Features();
			features = MapBuilder.getNationalCenterMapData(data);
			list2.add(features);
			
			}
		}
		
		if(resources.equals("mangrove")) {
			ArrayList<Mangrove> mangrovelist = null;
			mangrovelist = (ArrayList<Mangrove>) mangroveService.ListByRegion(region);
		
			for(Mangrove data : mangrovelist) {
			Features features = new Features();	
			features = MapBuilder.getManggroveMapData(data);
			list2.add(features);
			}
		}
		if(resources.equals("seagrass")) {
			ArrayList<SeaGrass> grasslist = null;		
			grasslist = (ArrayList<SeaGrass>) seaGrassService.ListByRegion(region);
		
			for(SeaGrass data : grasslist) {
			Features features = new Features();	
			features = MapBuilder.getSeagrassMapData(data);
			list2.add(features);
			}
		}
		if(resources.equals("fishport")) {
			ArrayList<Fishport> portlist = null;
			portlist = (ArrayList<Fishport>) fishPortService.findByRegion(region);
		
			for(Fishport data : portlist) {
			Features features = new Features();	
			features = MapBuilder.getFishPortMapData(data);
			list2.add(features);
			}	
		}
		if(resources.equals("market")) {
			ArrayList<Market> marketlist = null;		
			marketlist = (ArrayList<Market>) marketService.ListByRegion(region);
		
			for(Market data : marketlist) {
			Features features = new Features();	
			features = MapBuilder.getMarketMapData(data);
			list2.add(features);
			}	
		}
//		if(resources.equals("seaweeds")) {
//			ArrayList<Seaweeds> weedslist = null;				
//			weedslist = (ArrayList<Seaweeds>) seaweedsService.ListByRegion(region);
//		
//			for(Seaweeds data : weedslist) {
//			Features features = new Features();	
//			features = MapBuilder.getSeaweedsMapData(data);
//			list2.add(features);
//			}	
//		}
//		if(resources.equals("cold")) {
//			ArrayList<IcePlantColdStorage> coldlist = null;
//			coldlist = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.ListByRegion(region);
//		
//			for(IcePlantColdStorage data : coldlist) {
//			Features features = new Features();	
//			features = MapBuilder.getIceColdMapData(data);
//			list2.add(features);
//			}	
//		}
//		if(resources.equals("fishlanding")) {
//			ArrayList<FishLanding> fllist = null;
//			fllist = (ArrayList<FishLanding>) fishLandingService.findByRegion(region);
//		
//			for(FishLanding data : fllist) {
//			Features features = new Features();	
//			features = MapBuilder.getFishLandingMapData(data);
//			list2.add(features);
//			}	
//		}
/*		if(resources.equals("hatchery")) {
			ArrayList<Hatchery> hatcherylist = null;		
			hatcherylist = (ArrayList<Hatchery>) hatcheryService.findByRegion(region);	

			for(Hatchery data : hatcherylist) {
			Features features = new Features();	
			features = MapBuilder.getHatcheryMapData(data);
			list2.add(features);
			}	
		}
*/
		if(resources.equals("fishpen")) {
			ArrayList<FishPen> penlist = null;		
			penlist = (ArrayList<FishPen>) fishPenService.findByRegion(region);

			for(FishPen data : penlist) {
			Features features = new Features();	
			features = MapBuilder.getFishPenMapData(data);
			list2.add(features);
			}		
		}
		
		if(resources.equals("fishcage")) {
			ArrayList<FishCage> cagelist = null;		
			cagelist = (ArrayList<FishCage>) fishCageService.getListByRegion(region);
			for(FishCage data : cagelist) {
			Features features = new Features();	
			features = MapBuilder.getFishCageMapData(data);
			list2.add(features);
			}		
		}

		if(resources.equals("fishpond")) {
			ArrayList<FishPond> pondlist = null;
			pondlist = (ArrayList<FishPond>) fishPondService.findByRegionList(region);
			
			for(FishPond data : pondlist) {
			Features features = new Features();	
			features = MapBuilder.getFishPondMapData(data);
			list2.add(features);
			}			
		}
	
		if(resources.equals("fishcoral")) {
			ArrayList<FishCoral> corallist = null;		
			corallist = (ArrayList<FishCoral>) fishCorralService.ListByRegion(region);
			
			for(FishCoral data : corallist) {
			Features features = new Features();	
			features = MapBuilder.getFishCorralMapData(data);
			list2.add(features);
			}		
		}

//		if(resources.equals("mariculture")) {
//			ArrayList<MaricultureZone> zonelist = null;	
//			zonelist = (ArrayList<MaricultureZone>) zoneService.ListByRegion(region);		
//			
//			for(MaricultureZone data : zonelist) {
//			Features features = new Features();	
//			features = MapBuilder.getMaricultureZoneMapData(data);
//			list2.add(features);
//			}		
//		}
//		

		if(resources.equals("trainingcenter")) {
			ArrayList<TrainingCenter> traininglist = null;		
			traininglist = (ArrayList<TrainingCenter>) trainingCenterService.ListByRegion(region);		
			
			for(TrainingCenter data : traininglist) {
			Features features = new Features();	
			features = MapBuilder.getTrainingCenterMapData(data);
			list2.add(features);
			}		
		}
		if(resources.equals("lambaklad")) {
			ArrayList<Lambaklad> lambakladlist = null;		
			lambakladlist = (ArrayList<Lambaklad>) lambakladService.getListByRegion(region);		
			
			for(Lambaklad data : lambakladlist) {
			Features features = new Features();	
			features = MapBuilder.getLambakladMapData(data);
			list2.add(features);
			}		
		}
		if(resources.equals("payao")) {
			ArrayList<Payao> payaolist = null;		
			payaolist = (ArrayList<Payao>) payaoService.ListByRegion(region);		
			
			for(Payao data : payaolist) {
			Features features = new Features();	
			features = MapBuilder.getPayaoMapData(data);
			list2.add(features);
			}		
		}

		return list2;
		
		

		
	}

	@Override
	public ArrayList<Features> featuresListByRegion(String region, String resources, String type) {
		
		ArrayList<Features> list2 = new ArrayList<>();
		if(resources.equals("fishlanding")) {
			ArrayList<FishLanding> fllist = null;
			fllist = (ArrayList<FishLanding>) fishLandingService.findAllFishLandingsByRegion(region, type);
		
			for(FishLanding data : fllist) {
			Features features = new Features();	
			features = MapBuilder.getFishLandingMapData(data);
			list2.add(features);
			}	
		}
		if(resources.equals("cold")) {
			ArrayList<IcePlantColdStorage> coldlist = null;
			coldlist = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.ListByRegion(region, type);
		
			for(IcePlantColdStorage data : coldlist) {
			Features features = new Features();	
			features = MapBuilder.getIceColdMapData(data);
			list2.add(features);
			}	
		}
		if(resources.equals("seaweeds")) {
			ArrayList<Seaweeds> weedslist = null;				
			weedslist = (ArrayList<Seaweeds>) seaweedsService.ListByRegion(region, type);
		
			for(Seaweeds data : weedslist) {
			Features features = new Features();	
			features = MapBuilder.getSeaweedsMapData(data);
			list2.add(features);
			}	
		}
		if(resources.equals("mariculture")) {
			ArrayList<MaricultureZone> zonelist = null;	
			zonelist = (ArrayList<MaricultureZone>) zoneService.ListByRegion(region, type);		
			
			for(MaricultureZone data : zonelist) {
			Features features = new Features();	
			features = MapBuilder.getMaricultureZoneMapData(data);
			list2.add(features);
			}		
		}
		
		if(resources.equals("fishprocessing")) {
			ArrayList<FishProcessingPlant> fplist = null;
			fplist = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.ListByRegionList(region, type);
		
			
			for(FishProcessingPlant data : fplist) {
			Features features = new Features();	
			features = MapBuilder.getFishProcessingPlantMapData(data);
			list2.add(features);
				}
		}
		
		if(resources.equals("hatchery")) {
			ArrayList<Hatchery> hatcherylist = null;		
			if(type.equals("legislated")) {
				hatcherylist = (ArrayList<Hatchery>) hatcheryService.findByRegionAndLegislated(region, type);
			}else if(type.equals("nonLegislated")) {
				hatcherylist = (ArrayList<Hatchery>) hatcheryService.findByRegionAndLegislated(region, type);
			}else {
				hatcherylist = (ArrayList<Hatchery>) hatcheryService.findByRegionAndType(region, type);	

			}
			
			for(Hatchery data : hatcherylist) {
			Features features = new Features();	
			features = MapBuilder.getHatcheryMapData(data);
			list2.add(features);
			}	
		}
		
		return list2;
	}

	@Override
	public ArrayList<Features> featuresListAll() {

		
		
		ArrayList<Features> list2 = new ArrayList<>();
		ArrayList<FishSanctuaries> fslist = null;	
		fslist = (ArrayList<FishSanctuaries>) fishSanctuariesService.findAllFishSanctuaries();
		ArrayList<FishProcessingPlant> fplist = null;		
		fplist = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.findAllFishProcessingPlants();
		ArrayList<FishLanding> fllist = null;
		fllist = (ArrayList<FishLanding>) fishLandingService.findAllFishLandings();
		ArrayList<Fishport> portlist = null;
		portlist = (ArrayList<Fishport>) fishPortService.findAllFishports();   	
		ArrayList<FishPen> penlist = null;
		penlist = (ArrayList<FishPen>) fishPenService.findAllFishPens();
		ArrayList<FishCage> cagelist = null;
		cagelist = (ArrayList<FishCage>) fishCageService.findAllFishCages();
		ArrayList<FishPond> pondlist = null;
		pondlist = (ArrayList<FishPond>) fishPondService.findAllFishPond();
		ArrayList<IcePlantColdStorage> coldlist = null;
		coldlist = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.findAllIcePlantColdStorages();
		ArrayList<Market> marketlist = null;							
		marketlist = (ArrayList<Market>) marketService.findAllMarkets();
		ArrayList<SchoolOfFisheries> schoollist = null;		
		schoollist = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.findAllSchoolOfFisheriess();
		ArrayList<FishCoral> corallist = null;		
		corallist = (ArrayList<FishCoral>) fishCorralService.findAllFishCorals();
		ArrayList<SeaGrass> grasslist = null;		
		grasslist = (ArrayList<SeaGrass>) seaGrassService.findAllSeaGrasss();
		ArrayList<Seaweeds> weedslist = null;				
		weedslist = (ArrayList<Seaweeds>) seaweedsService.findAllSeaweedss();
		ArrayList<Mangrove> mangrovelist = null;
		mangrovelist = (ArrayList<Mangrove>) mangroveService.findAllMangroves();
		ArrayList<LGU> pfolist = null;		
		pfolist = (ArrayList<LGU>) lGUService.findAllLGUs();
		ArrayList<MaricultureZone> zonelist = null;	
		zonelist = (ArrayList<MaricultureZone>) zoneService.findAllMaricultureZones();
		ArrayList<TrainingCenter> traininglist = null;		
		traininglist = (ArrayList<TrainingCenter>) trainingCenterService.findAllTrainingCenters();
		ArrayList<Hatchery> hatcherylist = null;		
		hatcherylist = (ArrayList<Hatchery>) hatcheryService.findAllHatcherys();		
		ArrayList<NationalCenter> NClist = null;
		NClist = (ArrayList<NationalCenter>) nationalCenterService.findAllNationalCenter();
		ArrayList<RegionalOffice> ROlist = null;
		ROlist = (ArrayList<RegionalOffice>) regionalOfficeService.findAllRegionalOffice();
		ArrayList<TOS> TOSlist = null;
		TOSlist = (ArrayList<TOS>) tosService.findAllTOS();
		ArrayList<Lambaklad> lambakladlist = null;
		lambakladlist = (ArrayList<Lambaklad>) lambakladService.findAllLambaklads();
		ArrayList<Payao> payaolist = null;		
		payaolist = (ArrayList<Payao>) payaoService.findAllPayao();
		
      	 for(FishSanctuaries data : fslist) {
       		 
      		Features features = new Features();	
      		features = MapBuilder.getFishSanctuary(data);

	 				list2.add(features);
	 	}

		for(FishProcessingPlant data : fplist) {
					Features features = new Features();	
					features = MapBuilder.getFishProcessingPlantMapData(data);
					list2.add(features);
		}
		for(FishLanding data : fllist) {
				Features features = new Features();	
				features = MapBuilder.getFishLandingMapData(data);
				list2.add(features);
			}

		for(Fishport data : portlist) {
				Features features = new Features();	
				features = MapBuilder.getFishPortMapData(data);
				list2.add(features);
			}		

		for(FishPen data : penlist) {
				Features features = new Features();	
				features = MapBuilder.getFishPenMapData(data);
				list2.add(features);
			}	

		for(FishCage data : cagelist) {
				Features features = new Features();	
				features = MapBuilder.getFishCageMapData(data);
				list2.add(features);
			}				

		for(FishPond data : pondlist) {
			Features features = new Features();	
			features = MapBuilder.getFishPondMapData(data);
			list2.add(features);
		}				

	
		for(IcePlantColdStorage data : coldlist) {
				Features features = new Features();	
				features = MapBuilder.getIceColdMapData(data);
				list2.add(features);
			}				

		for(Market data : marketlist) {
				Features features = new Features();	
				features = MapBuilder.getMarketMapData(data);
				list2.add(features);
			}
	
		for(SchoolOfFisheries data : schoollist) {
				Features features = new Features();	
				features = MapBuilder.getSchoolOfFisheriesMapData(data);
				list2.add(features);
			}
		
		for(FishCoral data : corallist) {
				Features features = new Features();	
				features = MapBuilder.getFishCorralMapData(data);
				list2.add(features);
			}
		
		for(SeaGrass data : grasslist) {
				Features features = new Features();	
				features = MapBuilder.getSeagrassMapData(data);
				list2.add(features);
			}
		
		for(Seaweeds data : weedslist) {
				Features features = new Features();	
				features = MapBuilder.getSeaweedsMapData(data);
				list2.add(features);
			}
		for(Mangrove data : mangrovelist) {
				Features features = new Features();	
				features = MapBuilder.getManggroveMapData(data);
				list2.add(features);
			}
		for(LGU data : pfolist) {
				Features features = new Features();	
				features = MapBuilder.getLGUMapData(data);
				list2.add(features);
			}
		for(MaricultureZone data : zonelist) {
				Features features = new Features();	
				features = MapBuilder.getMaricultureZoneMapData(data);
				list2.add(features);
			}
		for(TrainingCenter data : traininglist) {
				Features features = new Features();	
				features = MapBuilder.getTrainingCenterMapData(data);
				list2.add(features);
			}
	
		for(Hatchery data : hatcherylist) {
					Features features = new Features();	
					features = MapBuilder.getHatcheryMapData(data);
					list2.add(features);
				}
		for(NationalCenter data : NClist) {
					Features features = new Features();
					features = MapBuilder.getNationalCenterMapData(data);
					list2.add(features);
					
				}
		for(RegionalOffice data : ROlist) {
					Features features = new Features();
					features = MapBuilder.getRegionalOfficeMapData(data);
					list2.add(features);
																	
				}
		for(TOS data : TOSlist) {
					Features features = new Features();
					features = MapBuilder.getTOSMapData(data);
					list2.add(features);
				}
		
		for(Lambaklad data : lambakladlist) {
			Features features = new Features();
			features = MapBuilder.getLambakladMapData(data);
			list2.add(features);
		}
		for(Payao data : payaolist) {
			Features features = new Features();	
			features = MapBuilder.getPayaoMapData(data);
			list2.add(features);
			}	

		return list2;
		
		

		
	
	}

	@Override
	public ArrayList<Features> featuresListByFishsanctuary(String region) {
		ArrayList<Features> list2 = new ArrayList<>();
		
		ArrayList<FishSanctuaries> fslist = null;		
		fslist = (ArrayList<FishSanctuaries>) fishSanctuariesService.findByRegionList(region);
		 for(FishSanctuaries data : fslist) {
       		 
	      		Features features = new Features();	
	      		features = MapBuilder.getFishSanctuary(data);

		 				list2.add(features);
		 	}
		 
		 return list2;
	}

	@Override
	public ArrayList<Features> featuresListByFishProcessing(String region) {
		
		ArrayList<Features> list2 = new ArrayList<>();
		ArrayList<FishProcessingPlant> fplist = null;		
		fplist = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.findAllFishProcessingPlants();
		for(FishProcessingPlant data : fplist) {
			Features features = new Features();	
			features = MapBuilder.getFishProcessingPlantMapData(data);
			list2.add(features);
}
		return list2;
	}

	@Override
	public ArrayList<Features> featuresListByRegion(String region) {

		
		ArrayList<Features> list2 = new ArrayList<>();
		
		
			ArrayList<FishSanctuaries> fslist = null;		
			fslist = (ArrayList<FishSanctuaries>) fishSanctuariesService.findByRegionList(region);
			
		  	 
			for(FishSanctuaries data : fslist) {
			
			Features features = new Features();	
			features = MapBuilder.getFishSanctuary(data);
			
			list2.add(features);
			}
		
		
      	ArrayList<FishProcessingPlant> fplist = null;
		fplist = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.ListByRegionList(region);
		ArrayList<FishLanding> fllist = null;
		fllist = (ArrayList<FishLanding>) fishLandingService.findByRegion(region);
		ArrayList<Fishport> portlist = null;
		portlist = (ArrayList<Fishport>) fishPortService.findByRegion(region);
		ArrayList<FishPen> penlist = null;		
		penlist = (ArrayList<FishPen>) fishPenService.findByRegion(region);
		ArrayList<FishCage> cagelist = null;		
		cagelist = (ArrayList<FishCage>) fishCageService.getListByRegion(region);
		ArrayList<FishPond> pondlist = null;
		pondlist = (ArrayList<FishPond>) fishPondService.findByRegionList(region);
		ArrayList<IcePlantColdStorage> coldlist = null;
		coldlist = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.ListByRegion(region);
		ArrayList<Market> marketlist = null;		
		marketlist = (ArrayList<Market>) marketService.ListByRegion(region);
		ArrayList<SchoolOfFisheries> schoollist = null;		
		schoollist = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.ListByRegion(region);
		ArrayList<FishCoral> corallist = null;		
		corallist = (ArrayList<FishCoral>) fishCorralService.ListByRegion(region);
		ArrayList<SeaGrass> grasslist = null;		
		grasslist = (ArrayList<SeaGrass>) seaGrassService.ListByRegion(region);
		ArrayList<Seaweeds> weedslist = null;				
		weedslist = (ArrayList<Seaweeds>) seaweedsService.ListByRegion(region);
		ArrayList<Mangrove> mangrovelist = null;
		mangrovelist = (ArrayList<Mangrove>) mangroveService.ListByRegion(region);
		ArrayList<LGU> pfolist = null;		
		pfolist = (ArrayList<LGU>) lGUService.ListByRegion(region);
		ArrayList<MaricultureZone> zonelist = null;	
		zonelist = (ArrayList<MaricultureZone>) zoneService.ListByRegion(region);			
		ArrayList<TrainingCenter> traininglist = null;		
		traininglist = (ArrayList<TrainingCenter>) trainingCenterService.ListByRegion(region);
		ArrayList<Hatchery> hatcherylist = null;		
		hatcherylist = (ArrayList<Hatchery>) hatcheryService.findByRegion(region);		
		ArrayList<NationalCenter> NClist = null;
		NClist = (ArrayList<NationalCenter>) nationalCenterService.ListByRegion(region);
		ArrayList<RegionalOffice> ROlist = null;
		ROlist = (ArrayList<RegionalOffice>) regionalOfficeService.ListByRegion(region);		
		ArrayList<TOS> TOSlist = null;
		TOSlist = (ArrayList<TOS>) tosService.ListByRegion(region);
		
																								


			for(FishProcessingPlant data : fplist) {
						Features features = new Features();	
						features = MapBuilder.getFishProcessingPlantMapData(data);
						list2.add(features);
			}
			for(FishLanding data : fllist) {
					Features features = new Features();	
					features = MapBuilder.getFishLandingMapData(data);
					list2.add(features);
				}

			for(Fishport data : portlist) {
					Features features = new Features();	
					features = MapBuilder.getFishPortMapData(data);
					list2.add(features);
				}		

			for(FishPen data : penlist) {
					Features features = new Features();	
					features = MapBuilder.getFishPenMapData(data);
					list2.add(features);
				}	

			for(FishCage data : cagelist) {
					Features features = new Features();	
					features = MapBuilder.getFishCageMapData(data);
					list2.add(features);
				}				
			for(FishPond data : pondlist) {
				Features features = new Features();	
				features = MapBuilder.getFishPondMapData(data);
				list2.add(features);
			}				

		
			
			for(IcePlantColdStorage data : coldlist) {
					Features features = new Features();	
					features = MapBuilder.getIceColdMapData(data);
					list2.add(features);
				}				

			for(Market data : marketlist) {
					Features features = new Features();	
					features = MapBuilder.getMarketMapData(data);
					list2.add(features);
				}
		
			for(SchoolOfFisheries data : schoollist) {
					Features features = new Features();	
					features = MapBuilder.getSchoolOfFisheriesMapData(data);
					list2.add(features);
				}
			
			for(FishCoral data : corallist) {
					Features features = new Features();	
					features = MapBuilder.getFishCorralMapData(data);
					list2.add(features);
				}
			
			for(SeaGrass data : grasslist) {
					Features features = new Features();	
					features = MapBuilder.getSeagrassMapData(data);
					list2.add(features);
				}
			
			for(Seaweeds data : weedslist) {
					Features features = new Features();	
					features = MapBuilder.getSeaweedsMapData(data);
					list2.add(features);
				}
			for(Mangrove data : mangrovelist) {
					Features features = new Features();	
					features = MapBuilder.getManggroveMapData(data);
					list2.add(features);
				}
			for(LGU data : pfolist) {
					Features features = new Features();	
					features = MapBuilder.getLGUMapData(data);
					list2.add(features);
				}
			for(MaricultureZone data : zonelist) {
					Features features = new Features();	
					features = MapBuilder.getMaricultureZoneMapData(data);
					list2.add(features);
				}
			for(TrainingCenter data : traininglist) {
					Features features = new Features();	
					features = MapBuilder.getTrainingCenterMapData(data);
					list2.add(features);
				}
		
			for(Hatchery data : hatcherylist) {
						Features features = new Features();	
						features = MapBuilder.getHatcheryMapData(data);
						list2.add(features);
					}
			for(NationalCenter data : NClist) {
						Features features = new Features();
						features = MapBuilder.getNationalCenterMapData(data);
						list2.add(features);
						
					}
			for(RegionalOffice data : ROlist) {
						Features features = new Features();
						features = MapBuilder.getRegionalOfficeMapData(data);
						list2.add(features);
																		
					}
			for(TOS data : TOSlist) {
						Features features = new Features();
						features = MapBuilder.getTOSMapData(data);
						list2.add(features);
					}
		
		return list2;
		
		

		
	}

	@Override
	public ArrayList<Features> featuresListAll(String resources) {

		
		ArrayList<Features> list2 = new ArrayList<>();
		
		if(resources.equals("fishsanctuary")) {
			ArrayList<FishSanctuaries> fslist = null;		
			fslist = (ArrayList<FishSanctuaries>) fishSanctuariesService.findAllFishSanctuaries();
			
		  	 
			for(FishSanctuaries data : fslist) {
			
			Features features = new Features();	
			features = MapBuilder.getFishSanctuary(data);
			
			list2.add(features);
			}
		}
		if(resources.equals("fishprocessing")) {
			ArrayList<FishProcessingPlant> fplist = null;
			fplist = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.findAllFishProcessingPlants();
		
			
			for(FishProcessingPlant data : fplist) {
			Features features = new Features();	
			features = MapBuilder.getFishProcessingPlantMapData(data);
			list2.add(features);
				}
		}
		if(resources.equals("schoolOfFisheries")) {
			ArrayList<SchoolOfFisheries> schoollist = null;		
			schoollist = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.findAllSchoolOfFisheriess();
		
			for(SchoolOfFisheries data : schoollist) {
			Features features = new Features();	
			features = MapBuilder.getSchoolOfFisheriesMapData(data);
			list2.add(features);
			}
		}
		if(resources.equals("tos")) {
			ArrayList<TOS> TOSlist = null;
			TOSlist = (ArrayList<TOS>) tosService.findAllTOS();
		
			for(TOS data : TOSlist) {
			Features features = new Features();
			features = MapBuilder.getTOSMapData(data);
			list2.add(features);
			}
		}
		if(resources.equals("lgu")) {
			ArrayList<LGU> pfolist = null;		
			pfolist = (ArrayList<LGU>) lGUService.findAllLGUs();
		
			for(LGU data : pfolist) {
			Features features = new Features();	
			features = MapBuilder.getLGUMapData(data);
			list2.add(features);
			}
		}
		if(resources.equals("regionaloffices")) {
			ArrayList<RegionalOffice> ROlist = null;
			ROlist = (ArrayList<RegionalOffice>) regionalOfficeService.findAllRegionalOffice();
		
			for(RegionalOffice data : ROlist) {
			Features features = new Features();
			features = MapBuilder.getRegionalOfficeMapData(data);
			list2.add(features);
															
			}
		}
		if(resources.equals("nationalcenter")) {
			ArrayList<NationalCenter> NClist = null;
			NClist = (ArrayList<NationalCenter>) nationalCenterService.findAllNationalCenter();
		
			for(NationalCenter data : NClist) {
			Features features = new Features();
			features = MapBuilder.getNationalCenterMapData(data);
			list2.add(features);
			
			}
		}
		
		if(resources.equals("mangrove")) {
			ArrayList<Mangrove> mangrovelist = null;
			mangrovelist = (ArrayList<Mangrove>) mangroveService.findAllMangroves();
		
			for(Mangrove data : mangrovelist) {
			Features features = new Features();	
			features = MapBuilder.getManggroveMapData(data);
			list2.add(features);
			}
		}
		if(resources.equals("seagrass")) {
			ArrayList<SeaGrass> grasslist = null;		
			grasslist = (ArrayList<SeaGrass>) seaGrassService.findAllSeaGrasss();
		
			for(SeaGrass data : grasslist) {
			Features features = new Features();	
			features = MapBuilder.getSeagrassMapData(data);
			list2.add(features);
			}
		}
		if(resources.equals("fishport")) {
			ArrayList<Fishport> portlist = null;
			portlist = (ArrayList<Fishport>) fishPortService.findAllFishports();
		
			for(Fishport data : portlist) {
			Features features = new Features();	
			features = MapBuilder.getFishPortMapData(data);
			list2.add(features);
			}	
		}
		if(resources.equals("market")) {
			ArrayList<Market> marketlist = null;		
			marketlist = (ArrayList<Market>) marketService.findAllMarkets();
		
			for(Market data : marketlist) {
			Features features = new Features();	
			features = MapBuilder.getMarketMapData(data);
			list2.add(features);
			}	
		}
		if(resources.equals("seaweeds")) {
			ArrayList<Seaweeds> weedslist = null;				
			weedslist = (ArrayList<Seaweeds>) seaweedsService.findAllSeaweedss();
		
			for(Seaweeds data : weedslist) {
			Features features = new Features();	
			features = MapBuilder.getSeaweedsMapData(data);
			list2.add(features);
			}	
		}
		if(resources.equals("fishpen")) {
			ArrayList<FishPen> penlist = null;		
			penlist = (ArrayList<FishPen>) fishPenService.findAllFishPens();

			for(FishPen data : penlist) {
			Features features = new Features();	
			features = MapBuilder.getFishPenMapData(data);
			list2.add(features);
			}		
		}
		
		if(resources.equals("fishcage")) {
			ArrayList<FishCage> cagelist = null;		
			cagelist = (ArrayList<FishCage>) fishCageService.findAllFishCages();
			for(FishCage data : cagelist) {
			Features features = new Features();	
			features = MapBuilder.getFishCageMapData(data);
			list2.add(features);
			}		
		}

		if(resources.equals("fishpond")) {
			ArrayList<FishPond> pondlist = null;
			pondlist = (ArrayList<FishPond>) fishPondService.findAllFishPond();
			
			for(FishPond data : pondlist) {
			Features features = new Features();	
			features = MapBuilder.getFishPondMapData(data);
			list2.add(features);
			}			
		}
	
		if(resources.equals("fishcoral")) {
			ArrayList<FishCoral> corallist = null;		
			corallist = (ArrayList<FishCoral>) fishCorralService.findAllFishCorals();
			
			for(FishCoral data : corallist) {
			Features features = new Features();	
			features = MapBuilder.getFishCorralMapData(data);
			list2.add(features);
			}		
		}

		if(resources.equals("mariculture")) {
			ArrayList<MaricultureZone> zonelist = null;	
			zonelist = (ArrayList<MaricultureZone>) zoneService.findAllMaricultureZones();		
			
			for(MaricultureZone data : zonelist) {
			Features features = new Features();	
			features = MapBuilder.getMaricultureZoneMapData(data);
			list2.add(features);
			}		
		}
		

		if(resources.equals("trainingcenter")) {
			ArrayList<TrainingCenter> traininglist = null;		
			traininglist = (ArrayList<TrainingCenter>) trainingCenterService.findAllTrainingCenters();		
			
			for(TrainingCenter data : traininglist) {
			Features features = new Features();	
			features = MapBuilder.getTrainingCenterMapData(data);
			list2.add(features);
			}		
		}
		
		if(resources.equals("lambaklad")) {
			
			ArrayList<Lambaklad> lambakladlist = null;
			lambakladlist = (ArrayList<Lambaklad>) lambakladService.findAllLambaklads();
			
			for(Lambaklad data : lambakladlist) {
				Features features = new Features();
				features = MapBuilder.getLambakladMapData(data);
				list2.add(features);
			}		
		}
		if(resources.equals("payao")) {
			
			ArrayList<Payao> payaolist = null;		
			payaolist = (ArrayList<Payao>) payaoService.findAllPayao();
			
			for(Payao data : payaolist) {
				Features features = new Features();
				features = MapBuilder.getPayaoMapData(data);
				list2.add(features);
			}		
		}

		return list2;
		
		

		
	}

	@Override
	public ArrayList<Features> featuresListAll(String resources, String type) {
		
		ArrayList<Features> list2 = new ArrayList<>();
		
		if(resources.equals("fishlanding")) {
			ArrayList<FishLanding> fllist = null;
			fllist = (ArrayList<FishLanding>) fishLandingService.findAllFishLandings(type);
		
			for(FishLanding data : fllist) {
			Features features = new Features();	
			features = MapBuilder.getFishLandingMapData(data);
			list2.add(features);
			}	
		}
		if(resources.equals("cold")) {
			ArrayList<IcePlantColdStorage> coldlist = null;
			coldlist = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.findAllIcePlantColdStorages(type);
		
			for(IcePlantColdStorage data : coldlist) {
			Features features = new Features();	
			features = MapBuilder.getIceColdMapData(data);
			list2.add(features);
			}	
		}
		if(resources.equals("seaweeds")) {
			ArrayList<Seaweeds> weedslist = null;				
			weedslist = (ArrayList<Seaweeds>) seaweedsService.findAllSeaweedss(type);
		
			for(Seaweeds data : weedslist) {
			Features features = new Features();	
			features = MapBuilder.getSeaweedsMapData(data);
			list2.add(features);
			}	
		}
		if(resources.equals("mariculture")) {
			ArrayList<MaricultureZone> zonelist = null;	
			zonelist = (ArrayList<MaricultureZone>) zoneService.findAllMaricultureZones(type);		
			
			for(MaricultureZone data : zonelist) {
			Features features = new Features();	
			features = MapBuilder.getMaricultureZoneMapData(data);
			list2.add(features);
			}		
		}
		
		if(resources.equals("fishprocessing")) {
			ArrayList<FishProcessingPlant> fplist = null;
			fplist = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.findAllFishProcessingPlants(type);
		
			
			for(FishProcessingPlant data : fplist) {
			Features features = new Features();	
			features = MapBuilder.getFishProcessingPlantMapData(data);
			list2.add(features);
				}
		}
		if(resources.equals("hatchery")) {
			ArrayList<Hatchery> hatcherylist = null;	
			
			if(type.equals("legislated")) {
				hatcherylist = (ArrayList<Hatchery>) hatcheryService.findAllByLegislated(type);
			}else if(type.equals("nonLegislated")) {
				hatcherylist = (ArrayList<Hatchery>) hatcheryService.findAllByLegislated(type);
			}else {
			hatcherylist = (ArrayList<Hatchery>) hatcheryService.findAllByType(type);	
			}
			for(Hatchery data : hatcherylist) {
			Features features = new Features();	
			features = MapBuilder.getHatcheryMapData(data);
			list2.add(features);
			}	
		}
		
		return list2;
	}

}
