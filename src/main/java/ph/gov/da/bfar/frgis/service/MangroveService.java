package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.MangrovePage;
import ph.gov.da.bfar.frgis.model.Mangrove;



public interface MangroveService {
	
	Optional<Mangrove> findById(int id);
	
	List<Mangrove> ListByUsername(String username);
	List<Mangrove> ListByRegion(String region_id);
	Mangrove findByUniqueKey(String uniqueKey);
	List<Mangrove> findByUniqueKeyForUpdate(String uniqueKey);
	void saveMangrove(Mangrove Mangrove);
	void deleteMangroveById(int id);
	void updateByEnabled(int id); 
	void updateMangroveByReason(int  id,String  reason);
	List<Mangrove> findAllMangroves(); 
    public List<Mangrove> getAllMangroves(int index,int pagesize,String user);
    public int MangroveCount(String username);
    public Page<MangrovePage> findByUsername(@Param("user") String user, Pageable pageable);
    public Page<MangrovePage> findByRegion(@Param("region") String region, Pageable pageable);
    public Page<MangrovePage> findAllMangrove(Pageable pageable);
    public Long countAllMangrove();
    public Long countMangrovePerRegion(String region_id); 
    public Long countMangrovePerProvince(String province_id);
    public Long countMangrovePerMunicipality(String municipality_id);
    
}