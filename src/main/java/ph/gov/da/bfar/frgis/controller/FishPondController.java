package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.FishPondPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishPond;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.FishPondService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class FishPondController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	FishPondService fishPondService;
	
	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	private String newUniqueKeyforMap="";
	private String map_data="";

	User user;	

	@RequestMapping(value = "/fishponds", method = RequestMethod.GET)
	public ModelAndView fishpond(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();
		user = authenticationFacade.getUserInfo();
		

//		Set<UserProfile> setOfRole = new HashSet<>();
//		setOfRole = user.getUserProfiles();
//		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
//		UserProfile role = list.get(0);

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", authenticationFacade.getUserInfo());
		model.addAttribute("page", "fishpond");
		model.addAttribute("username", authenticationFacade.getUsername());
		model.addAttribute("region", authenticationFacade.getRegionName());
		model.addAttribute("region_id", authenticationFacade.getRegionID());
		result = new ModelAndView("users/form");
		return result;
	}
	@RequestMapping(value = "/fishpond", method = RequestMethod.GET)
	public ModelAndView fishpondByID(ModelMap model) throws JsonGenerationException, JsonMappingException, IOException {

		ModelAndView result = new ModelAndView();
		user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("page", "fishpond");
		model.addAttribute("user", authenticationFacade.getUserInfo());
		model.addAttribute("username", authenticationFacade.getUsername());
		model.addAttribute("region", authenticationFacade.getRegionName());
		model.addAttribute("region_id", authenticationFacade.getRegionID());
		
		result = new ModelAndView("users/form");
		return result;
		
	}
	@RequestMapping(value = "/fishPondMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishPondMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();
		 
		 ObjectMapper mapper = new ObjectMapper();
			//List<MapsData> mapsDatas = mapsService.findByUniqueKey(uniqueKey);
		 	FishPond data = fishPondService.findByUniqueKey(uniqueKey);
		
		 	
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	
		 	Features features = new Features();
			 features = MapBuilder.getFishPondMapData(data);
			 
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	mapsData.setIcon("/static/images/pin2022/FishPond.png");
		 	
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			
			map_data= jsonString;
			result = new ModelAndView("redirect:../fishpond");
		
	
		return result;
	}
	@RequestMapping(value = "/fishPondDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView fishPondMapByUniqueKey(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();
		 //fishPondService.deleteFishPondById(id);
		 fishPondService.updateForEnabled(id);
			result = new ModelAndView("redirect:../fishpond");
		
	
		return result;
	}
	
	@RequestMapping(value = "/fishPondDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView fishPondMapByUniqueKey(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();

		 fishPondService.updateForEnabled(id);
		 fishPondService.updateFishPondByReason(id, reason);
		 
			result = new ModelAndView("redirect:../../fishpond");
		
	
		return result;
	}
	@RequestMapping(value = "/fishpondList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getfishpondList() throws JsonGenerationException, JsonMappingException, IOException {

	//	user = getUserInfo();
		List<FishPond> list = null;

		list = fishPondService.getListFishPondByUsername(authenticationFacade.getUsername());

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);
		
		return jsonArray;

	}	
	@RequestMapping(value = "/saveFishPond", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postFishPond(@RequestBody FishPond fishPond) throws JsonGenerationException, JsonMappingException, IOException {

		savingResources(fishPond);


		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
		//resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;
	}
	@RequestMapping(value = "/getFishPond/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishPondById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<FishPond> mapsDatas = fishPondService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}
	@RequestMapping(value = "/fishpond/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishpondPagination(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		 Page<FishPondPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 15, Sort.by("id"));
	      
	          page = fishPondService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info fishPond - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<FishPondPage> FishPondList = page.getContent();
	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();
		
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishPond(FishPondList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	private void savingResources(FishPond fishPond) {

		boolean save= false;
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		//region = user.getRegion_id() + "," + user.getRegion();
		//region_data = region.split(",", 2);
		//fishPond.setRegion_id(region_data[0]);
		//fishPond.setRegion(region_data[1]);

		province = fishPond.getProvince();
		data = province.split(",", 2);
		fishPond.setProvince_id(data[0]);
		fishPond.setProvince(data[1]);

		municipal = fishPond.getMunicipality();
		municipal_data = municipal.split(",", 2);
		fishPond.setMunicipality_id(municipal_data[0]);
		fishPond.setMunicipality(municipal_data[1]);

		barangay = fishPond.getBarangay();
		barangay_data = barangay.split(",", 3);
		fishPond.setBarangay_id(barangay_data[0]);
		fishPond.setBarangay(barangay_data[1]);
		fishPond.setEnabled(true);
		
		String imageNameSrc = Base64ImageUtil.removeExtension(fishPond.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		

		String newUniqueKey = newID.toString();

		if (fishPond.getId() != 0) {
			Optional<FishPond> pond = fishPondService.findById(fishPond.getId());
			String image_server = pond.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				fishPond.setImage("/images/" + image_server);
				newUniqueKeyforMap = fishPond.getUniqueKey();
				fishPondService.saveFishPond(ResourceData.getFishPond(fishPond,  "", UPDATE));
			}else {
				newUniqueKeyforMap = fishPond.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", ""); 
				fishPond.setImage("/images/" + imageName);
				Base64Converter.convertToImage(fishPond.getImage_src(), fishPond.getImage());
				fishPondService.saveFishPond(ResourceData.getFishPond(fishPond,  "", UPDATE));
			}	
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", ""); 
			fishPond.setImage("/images/" + imageName);
			Base64Converter.convertToImage(fishPond.getImage_src(), fishPond.getImage());
			fishPondService.saveFishPond(ResourceData.getFishPond(fishPond,newUniqueKey, SAVE));
			save = true;

		}
		

		

	}
//
//	@RequestMapping(value = "/getLastPageFPOND", method = RequestMethod.GET)
//	public @ResponseBody String getLastPageFPond(ModelMap model)
//			throws JsonGenerationException, JsonMappingException, IOException {
//
//		int totalNumberOfRecords = 0;
//		int numberOfRecordsPerPage = 20;
//
//		totalNumberOfRecords = fishPondService.FishPondCount(AppController.getPrincipal());
//
//		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
//		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
//			noOfPages = noOfPages + 1;
//		}
//		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
//
//		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));
//
//		Gson gson = new GsonBuilder().setPrettyPrinting().create();
//
//		String jsonArray = gson.toJson(String.valueOf(noOfPages));
//
//		return jsonArray;
//
//	}

//	private User getUserInfo() {
//		User user = new User();
//		user = userService.findBySSO(GetUserInfo.getPrincipal());
//		
//		return user;
//	}
}
