package ph.gov.da.bfar.frgis.model;


public class Chart {
	
	private long id;
	private String region;
	private String province;
	private String municipality;
	private String resources;
	private String location;
	private long total;
	private String color;
	
	private long fishcount;
	private long processingCount;
	private long fishlandingCount;
	private long fishportCount;
	private long fishpenCount; 
	private long fishcageCount;
	private long fishcoldstorageCount;
	private long fishmarketCount;
	private long fishschoolofFisheriesCount;
	private long fishcoralCount;
	private long fishpondCount;
	private long seagrassCount;
	private long seaweedsCount;
	private long mangroveCount;
	private long maricultureCount;
	private long pfoCount;
	private long trainingCount;
	  private long operational;
      private long forOperation;
      private long forCompletion;
      private long forTransfer;
      private long damaged;
      private long traditional;
      private long nonTraditional;
      private long bfar;
      private long privateSector;
      private long cooperativw;
      private long lgu;
      private long legislated;
      private long nonLegislated;
      private long seaweedsNurseryCount;
      private long seaweedsLaboratoryCount;
      private long seaweedsWarehouseCount;
      private long seaweedsDryerCount;
      

	
	 
	


//	
//	public Chart(long id, String resources, long total) {
//		super();
//		this.id = id;
//		this.resources = resources;
//		this.total = total;
//	}
//	
	
	public Chart() {

	}




	public Chart(String resources, String province,long total) {
		super();
		this.province = province;
		this.resources = resources;
		this.total = total;
	}




	public Chart(String resources, long total) {
		super();
		this.resources = resources;
		this.total = total;
	}




	public Chart(String resources, long total, String color) {
		super();
		this.resources = resources;
		this.total = total;
		this.color = color;
	}
	


	public Chart(long id, String province, long bfar, long privateSector, long cooperativw) {
		super();
		this.id = id;
		this.province = province;
		this.bfar = bfar;
		this.privateSector = privateSector;
		this.cooperativw = cooperativw;
	}
	




	public Chart(long id, String province, long bfar, long privateSector, long cooperativw, long lgu) {
		super();
		this.id = id;
		this.province = province;
		this.bfar = bfar;
		this.privateSector = privateSector;
		this.cooperativw = cooperativw;
		this.lgu = lgu;
	}



	public Chart(long id,String province, long operational, long forOperation, long forCompletion, long forTransfer,
			long damaged, long traditional, long nonTraditional) {
		super();
		this.id = id;
		this.province = province;
		this.operational = operational;
		this.forOperation = forOperation;
		this.forCompletion = forCompletion;
		this.forTransfer = forTransfer;
		this.damaged = damaged;
		this.traditional = traditional;
		this.nonTraditional = nonTraditional;
	}





	public String getColor() {
		return color;
	}




	public void setColor(String color) {
		this.color = color;
	}




	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getResources() {
		return resources;
	}


	public void setResources(String resources) {
		this.resources = resources;
	}


	public long getTotal() {
		return total;
	}


	public void setTotal(long total) {
		this.total = total;
	}




	public String getRegion() {
		return region;
	}




	public void setRegion(String region) {
		this.region = region;
	}




	public String getProvince() {
		return province;
	}




	public void setProvince(String province) {
		this.province = province;
	}




	public long getFishcount() {
		return fishcount;
	}




	public void setFishcount(long fishcount) {
		this.fishcount = fishcount;
	}




	public long getProcessingCount() {
		return processingCount;
	}




	public void setProcessingCount(long processingCount) {
		this.processingCount = processingCount;
	}




	public long getFishlandingCount() {
		return fishlandingCount;
	}




	public void setFishlandingCount(long fishlandingCount) {
		this.fishlandingCount = fishlandingCount;
	}




	public long getFishportCount() {
		return fishportCount;
	}




	public void setFishportCount(long fishportCount) {
		this.fishportCount = fishportCount;
	}




	public long getFishpenCount() {
		return fishpenCount;
	}




	public void setFishpenCount(long fishpenCount) {
		this.fishpenCount = fishpenCount;
	}




	public long getFishcageCount() {
		return fishcageCount;
	}




	public void setFishcageCount(long fishcageCount) {
		this.fishcageCount = fishcageCount;
	}




	public long getFishcoldstorageCount() {
		return fishcoldstorageCount;
	}




	public void setFishcoldstorageCount(long fishcoldstorageCount) {
		this.fishcoldstorageCount = fishcoldstorageCount;
	}




	public long getFishmarketCount() {
		return fishmarketCount;
	}




	public void setFishmarketCount(long fishmarketCount) {
		this.fishmarketCount = fishmarketCount;
	}




	public long getFishschoolofFisheriesCount() {
		return fishschoolofFisheriesCount;
	}




	public void setFishschoolofFisheriesCount(long fishschoolofFisheriesCount) {
		this.fishschoolofFisheriesCount = fishschoolofFisheriesCount;
	}




	public long getFishcoralCount() {
		return fishcoralCount;
	}




	public void setFishcoralCount(long fishcoralCount) {
		this.fishcoralCount = fishcoralCount;
	}




	public long getSeagrassCount() {
		return seagrassCount;
	}




	public void setSeagrassCount(long seagrassCount) {
		this.seagrassCount = seagrassCount;
	}




	public long getSeaweedsCount() {
		return seaweedsCount;
	}




	public void setSeaweedsCount(long seaweedsCount) {
		this.seaweedsCount = seaweedsCount;
	}




	public long getMangroveCount() {
		return mangroveCount;
	}




	public void setMangroveCount(long mangroveCount) {
		this.mangroveCount = mangroveCount;
	}




	public long getMaricultureCount() {
		return maricultureCount;
	}




	public void setMaricultureCount(long maricultureCount) {
		this.maricultureCount = maricultureCount;
	}



	public long getPfoCount() {
		return pfoCount;
	}




	public void setPfoCount(long pfoCount) {
		this.pfoCount = pfoCount;
	}




	public long getTrainingCount() {
		return trainingCount;
	}




	public void setTrainingCount(long trainingCount) {
		this.trainingCount = trainingCount;
	}





	public long getOperational() {
		return operational;
	}




	public void setOperational(long operational) {
		this.operational = operational;
	}




	public long getForOperation() {
		return forOperation;
	}




	public void setForOperation(long forOperation) {
		this.forOperation = forOperation;
	}




	public long getForCompletion() {
		return forCompletion;
	}




	public void setForCompletion(long forCompletion) {
		this.forCompletion = forCompletion;
	}




	public long getForTransfer() {
		return forTransfer;
	}




	public void setForTransfer(long forTransfer) {
		this.forTransfer = forTransfer;
	}




	public long getDamaged() {
		return damaged;
	}




	public void setDamaged(long damaged) {
		this.damaged = damaged;
	}




	public long getTraditional() {
		return traditional;
	}




	public void setTraditional(long traditional) {
		this.traditional = traditional;
	}




	public long getNonTraditional() {
		return nonTraditional;
	}




	public void setNonTraditional(long nonTraditional) {
		this.nonTraditional = nonTraditional;
	}




	public long getBfar() {
		return bfar;
	}




	public void setBfar(long bfar) {
		this.bfar = bfar;
	}




	public long getPrivateSector() {
		return privateSector;
	}




	public void setPrivateSector(long privateSector) {
		this.privateSector = privateSector;
	}




	public long getCooperativw() {
		return cooperativw;
	}




	public void setCooperativw(long cooperativw) {
		this.cooperativw = cooperativw;
	}
	
	public long getLgu() {
		return lgu;
	}




	public void setLgu(long lgu) {
		this.lgu = lgu;
	}




	public long getLegislated() {
		return legislated;
	}




	public void setLegislated(long legislated) {
		this.legislated = legislated;
	}




	public long getNonLegislated() {
		return nonLegislated;
	}




	public void setNonLegislated(long nonLegislated) {
		this.nonLegislated = nonLegislated;
	}




	public long getFishpondCount() {
		return fishpondCount;
	}




	public void setFishpondCount(long fishpondCount) {
		this.fishpondCount = fishpondCount;
	}




	public String getMunicipality() {
		return municipality;
	}




	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}




	public String getLocation() {
		return location;
	}




	public void setLocation(String location) {
		this.location = location;
	}




	public long getSeaweedsNurseryCount() {
		return seaweedsNurseryCount;
	}




	public void setSeaweedsNurseryCount(long seaweedsNurseryCount) {
		this.seaweedsNurseryCount = seaweedsNurseryCount;
	}




	public long getSeaweedsLaboratoryCount() {
		return seaweedsLaboratoryCount;
	}




	public void setSeaweedsLaboratoryCount(long seaweedsLaboratoryCount) {
		this.seaweedsLaboratoryCount = seaweedsLaboratoryCount;
	}




	public long getSeaweedsWarehouseCount() {
		return seaweedsWarehouseCount;
	}




	public void setSeaweedsWarehouseCount(long seaweedsWarehouseCount) {
		this.seaweedsWarehouseCount = seaweedsWarehouseCount;
	}




	public long getSeaweedsDryerCount() {
		return seaweedsDryerCount;
	}




	public void setSeaweedsDryerCount(long seaweedsDryerCount) {
		this.seaweedsDryerCount = seaweedsDryerCount;
	}




	@Override
	public String toString() {
		return "Chart [region=" + region + ", province=" + province + ", location=" + location + ", operational="
				+ operational + ", forOperation=" + forOperation + ", forCompletion=" + forCompletion + ", forTransfer="
				+ forTransfer + ", damaged=" + damaged + ", traditional=" + traditional + ", nonTraditional="
				+ nonTraditional + "]";
	}














	







	

}
