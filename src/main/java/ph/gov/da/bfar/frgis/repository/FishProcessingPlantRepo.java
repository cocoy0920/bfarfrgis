package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ph.gov.da.bfar.frgis.Page.FishProcessingPlantPage;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;


@Repository
public interface FishProcessingPlantRepo extends JpaRepository<FishProcessingPlant, Integer> {


	@Query("SELECT c from FishProcessingPlant c where c.user = :username")
	public List<FishProcessingPlant> getListFishProcessingPlantByUsername(@Param("username") String username);
	
	@Query("SELECT COUNT(f) FROM FishProcessingPlant f WHERE f.user=:username")
	public int  FishProcessingPlantCount(@Param("username") String username);
	
	@Query("SELECT c from FishProcessingPlant c where c.uniqueKey = :uniqueKey")
	public FishProcessingPlant findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from FishProcessingPlant c where c.uniqueKey = :uniqueKey")
	List<FishProcessingPlant> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);

	@Modifying(clearAutomatically = true)
	@Query("update FishProcessingPlant f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update FishProcessingPlant f set f.reason =:reason  where f.id =:id")
	public void updateFishProcessingPlantByReason( @Param("id") int id, @Param("reason") String reason);
	
	@Query("SELECT c from FishProcessingPlant c where c.user = :username")
	 public List<FishProcessingPlant> getPageableFishProcessingPlant(@Param("username") String username,Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishProcessingPlantPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfProcessingPlants,c.lat,c.lon,c.enabled) FROM FishProcessingPlant c where c.user = :user")
	public Page<FishProcessingPlantPage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishProcessingPlantPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfProcessingPlants,c.lat,c.lon,c.enabled) FROM FishProcessingPlant c where c.region_id = :region_id")
	public Page<FishProcessingPlantPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishProcessingPlantPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfProcessingPlants,c.lat,c.lon,c.enabled) FROM FishProcessingPlant c")
	public Page<FishProcessingPlantPage> findAllFishProcessing(Pageable pageable);

	@Query("SELECT c from FishProcessingPlant c where c.region_id = :region_id")
	public List<FishProcessingPlant> findByRegionList(@Param("region_id") String region_id);
	
	@Query("SELECT c from FishProcessingPlant c where c.region_id = :region_id and c.operatorClassification = :operator_classification ")
	public List<FishProcessingPlant> findByRegionAndType(@Param("region_id") String region_id,@Param("operator_classification") String operator_classification);
	
	@Query("SELECT c from FishProcessingPlant c where c.province_id = :province_id")
	public List<FishProcessingPlant> findByProvinceList(@Param("province_id") String province_id);
	
	@Query("SELECT c from FishProcessingPlant c where c.operatorClassification = :operator_classification")
	public List<FishProcessingPlant> findAllType(@Param("operator_classification") String operator_classification);

	@Query("SELECT COUNT(f) FROM FishProcessingPlant f")
	public Long countAllFishProcessingPlant();
	
	@Query("SELECT COUNT(f) FROM FishProcessingPlant f WHERE f.region_id=:region_id")
	public Long countFishProcessingPlantPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM FishProcessingPlant f WHERE f.province_id=:province_id")
	public Long countFishProcessingPlantPerProvince(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM FishProcessingPlant f WHERE f.municipality_id=:municipality_id")
	public Long countFishProcessingPlantPerMunicipality(@Param("municipality_id") String municipality_id);
	
	@Query("SELECT COUNT(f) FROM FishProcessingPlant f WHERE f.region_id=:region_id and f.operatorClassification=:operator_classification")
	public Long countFishProcessingPlantPerRegion(@Param("region_id") String region_id,@Param("operator_classification") String operator_classification);
	
	@Query("SELECT COUNT(f) FROM FishProcessingPlant f WHERE f.province_id=:province_id and f.operatorClassification=:operator_classification")
	public Long countFishProcessingPlantPerProvince(@Param("province_id") String province_id,@Param("operator_classification") String operator_classification);
	
	@Query("SELECT COUNT(f) FROM FishProcessingPlant f WHERE f.municipality_id=:municipality_id and f.operatorClassification=:operator_classification")
	public Long countFishProcessingPlantPerMunicipality(@Param("municipality_id") String municipality_id,@Param("operator_classification") String operator_classification);
	
}
