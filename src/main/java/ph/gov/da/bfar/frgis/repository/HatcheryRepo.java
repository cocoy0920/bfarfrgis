package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.HatcheryPage;
import ph.gov.da.bfar.frgis.model.Hatchery;

@Repository
public interface HatcheryRepo extends JpaRepository<Hatchery, Integer> {

	@Query("SELECT c from Hatchery c where c.user = :username")
	public List<Hatchery> getListHatcheryByUsername(@Param("username") String username);
	
	@Query("SELECT c from Hatchery c where c.region_id = :region_id")
	public List<Hatchery> getListByRegion(@Param("region_id") String region_id);
	
	@Modifying(clearAutomatically = true)
	@Query("update Hatchery f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update Hatchery f set f.reason =:reason  where f.id =:id")
	public void updateHatcheryByReason( @Param("id") int id, @Param("reason") String reason);
	

	@Query("SELECT c from Hatchery c where c.region_id = :region_id and c.operatorClassification = :operatorClassification")
	public List<Hatchery> getListByRegionAndType(@Param("region_id") String region_id,@Param("operatorClassification") String operatorClassification);
	
	@Query("SELECT c from Hatchery c where c.region_id = :region_id and c.legislated = :legislated")
	public List<Hatchery> getListByRegionAndLegislated(@Param("region_id") String region_id,@Param("legislated") String legislated);
	
	@Query("SELECT c from Hatchery c where c.operatorClassification = :operatorClassification")
	List<Hatchery> findAllByType(@Param("operatorClassification") String operatorClassification);
	
	@Query("SELECT c from Hatchery c where c.legislated = :legislated")
	List<Hatchery> findAllByLegislated(@Param("legislated") String legislated);
	
	@Query("SELECT COUNT(f) FROM Hatchery f WHERE f.user=:username")
	public int  HatcheryCount(@Param("username") String username);
	
	@Query("SELECT c from Hatchery c where c.uniqueKey = :uniqueKey")
	public Hatchery findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from Hatchery c where c.uniqueKey = :uniqueKey")
	List<Hatchery> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from Hatchery c where c.user = :username")
	 public List<Hatchery> getPageableHatchery(@Param("username") String username,Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.HatcheryPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfHatchery,c.legislated,c.lat,c.lon,c.enabled) from Hatchery c where c.user = :user")
	public Page<HatcheryPage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.HatcheryPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfHatchery,c.legislated,c.lat,c.lon,c.enabled) from Hatchery c where c.region_id = :region_id")
	public Page<HatcheryPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.HatcheryPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfHatchery,c.legislated,c.lat,c.lon,c.enabled) from Hatchery c")
	public Page<HatcheryPage> findAllHatchery(Pageable pageable);

	@Query("SELECT COUNT(f) FROM Hatchery f")
	public Long countAllHatchery();
	
	@Query("SELECT COUNT(f) FROM Hatchery f WHERE f.region_id=:region_id")
	public Long countHatcheryPerRegion(@Param("region_id")String region_id);
	
	@Query("SELECT COUNT(f) FROM Hatchery f WHERE f.region_id=:region_id and f.operatorClassification=:operator")
	public Long countHatcheryPerRegion(@Param("region_id")String region_id,@Param("operator") String operator);
	
	@Query("SELECT COUNT(f) FROM Hatchery f WHERE f.region_id=:region_id and f.operatorClassification=:operator and  f.legislated=:legislated")
	public Long countHatcheryLegislatedPerRegion(@Param("region_id")String region_id,@Param("operator") String operator, @Param("legislated") String legislated);
	
	@Query("SELECT COUNT(f) FROM Hatchery f WHERE f.province_id=:province_id")
	public Long countHatcheryPerProvince(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM Hatchery f WHERE f.province_id=:province_id and f.operatorClassification=:operator")
	public Long countHatcheryPerProvince(@Param("province_id") String province_id,@Param("operator") String operator);
	
	@Query("SELECT COUNT(f) FROM Hatchery f WHERE f.province_id=:province_id and f.operatorClassification=:operator and  f.legislated=:legislated")
	public Long countHatcheryLegislatedPerProvince(@Param("province_id") String province_id, @Param("operator") String operator, @Param("legislated") String legislated);
	
	@Query("SELECT COUNT(f) FROM Hatchery f WHERE f.municipality_id=:municipality_id")
	public Long countHatcheryPerMunicipality(@Param("municipality_id") String municipality_id);
	
	@Query("SELECT COUNT(f) FROM Hatchery f WHERE f.municipality_id=:municipality_id and f.operatorClassification=:operator")
	public Long countHatcheryPerMunicipality(@Param("municipality_id") String municipality_id, @Param("operator") String operator);
	
	@Query("SELECT COUNT(f) FROM Hatchery f WHERE f.municipality_id=:municipality_id and f.operatorClassification=:operator and  f.legislated=:legislated")
	public Long countHatcheryLegislatedPerMunicipality(@Param("municipality_id") String municipality_id, @Param("operator") String operator, @Param("legislated") String legislated);
	
}
