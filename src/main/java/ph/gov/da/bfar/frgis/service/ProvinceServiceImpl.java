package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.model.Provinces;
import ph.gov.da.bfar.frgis.repository.ProvinceRepo;



@Service("ProvinceService")
@Transactional
public class ProvinceServiceImpl implements ProvinceService{

	@Autowired
	ProvinceRepo provinceRepo; 
	
	@Override
	public Optional<Provinces> findById(int id) {
		
		return provinceRepo.findById(id);
	}

	@Override
	public void save(Provinces province) {
		provinceRepo.save(province);
		
	}

	@Override
	public void update(Provinces province) {
		provinceRepo.save(province);
		
	}

	@Override
	public List<Provinces> findAllProvinces() {
		
		return provinceRepo.findAll();
	}

	@Override
	public List<Provinces> findByRegion(String regionId) {
		
		return provinceRepo.findByRegion(regionId);
	}

	
	
}
