package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserBean;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.model.UserProfileType;
import ph.gov.da.bfar.frgis.service.ProvinceService;
import ph.gov.da.bfar.frgis.service.RegionsService;
import ph.gov.da.bfar.frgis.service.RoleService;
import ph.gov.da.bfar.frgis.service.UserProfileService;
import ph.gov.da.bfar.frgis.service.UserService;

@Controller
@RequestMapping("/province")
public class ProvinceController {
	
	@Autowired
	UserService userService;

	@Autowired
	RegionsService regionsService;
	
	@Autowired
	ProvinceService provinceService;
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	UserProfileService userProfileService;
	
	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	

	@RequestMapping(value = "home", method = RequestMethod.GET)
	public ModelAndView adminhome(ModelMap model) {
		ModelAndView modelAndView = new ModelAndView();
		
		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		
		modelAndView.setViewName("/province/province_admin"); // resources/template/admin.html
		return modelAndView;
	}

	
	@RequestMapping(value = "/province_register", method = RequestMethod.GET)
	public ModelAndView province_register(ModelMap model) {
		ModelAndView modelAndView = new ModelAndView();
		
		
		User user = new User();
		
		user  = userService.findByUsername(authenticationFacade.getUsername());
		
		
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		System.out.println("ROLE: " + setOfRole);
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		UserProfile role_account = new UserProfile();
		if(role.getType().equals("PFOADMIN")){
			role_account.setId(8);
			role_account.setType(UserProfileType.PFOENCODER.getUserProfileType());
		}
		
//		List<Provinces> provinces = null;
//		
//		provinces = provinceService.findByRegion(user.getRegion_id());
//				
//
//		model.addAttribute("provinces", provinces);
		 model.addAttribute("role", role_account);

		
		
		modelAndView.setViewName("province/register"); // resources/template/register.html
		return modelAndView;
		
	}
	
	@RequestMapping(value = "/getUserInfo", method = RequestMethod.GET)
	public ModelAndView getUserInfo(ModelMap model) {
		ModelAndView modelAndView = new ModelAndView();
		
		
		User user = new User();
		
		user  = userService.findByUsername(authenticationFacade.getUsername());
		
		
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		System.out.println("ROLE: " + setOfRole);
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
		
//		List<Provinces> provinces = null;
//		
//		provinces = provinceService.findByRegion(user.getRegion_id());
//				
//
//		model.addAttribute("provinces", provinces);
		 model.addAttribute("role", role);

		
		
		modelAndView.setViewName("province/register"); // resources/template/register.html
		return modelAndView;
		
	}
	
	
	@RequestMapping(value = "/saveUserRegistration", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String saveUserRegistration(@RequestBody UserBean userbean) throws JsonGenerationException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		
		boolean getUsername = userService.isUserAlreadyPresent(userbean.getUsername());
		
		if(getUsername) {
							
//			userbean.setUsername("");
//			 modelAndView.addObject("userForm", userbean);
//			modelAndView.addObject("successMessage", "Username Already taken!");
			return objectMapper.writeValueAsString("Username Already taken!");

			
		}else {
			

			User user = new User();
			User userAdmin = new User();
			System.out.println(authenticationFacade.getUsername() + "authenticationFacade.getUsername()");
			
			userAdmin  = userService.findByUsername(authenticationFacade.getUsername());
			
			
			System.out.println("User Details: " + userAdmin);
			
			
			
			user.setName(userbean.getFirstName());
			user.setLastName(userbean.getLastName());
			user.setUsername(userbean.getUsername());
			user.setState("Active");
			user.setEmail(userbean.getEmail());
			user.setEnabled(true);
			user.setPass(userbean.getPassword());
			Set<UserProfile> userProfiles = new HashSet<UserProfile>();
			UserProfile profile = new UserProfile();
			

			String role = userbean.getUserProfiles();
			String[] role_data = role.split(",", 2);
			
			profile.setId(Integer.parseInt(role_data[0]));	
			profile.setType(role_data[1]);
			
			Set<UserProfile> setOfRole = new HashSet<>();
			setOfRole = userAdmin.getUserProfiles();
	
			List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
			UserProfile region_role = list.get(0);
			
			if(region_role.getType().equals("PROVINCE")) {
				System.out.println(role_data[1].equals("REGION") + "role_data[1].equals(\"REGION\")");
				
				user.setRegion_id(userAdmin.getRegion_id());
				user.setRegion(userAdmin.getRegion());
			
				
				user.setProvince(userAdmin.getProvince());
				user.setProvince_id(userAdmin.getProvince_id());
				
			}
			
			userProfiles.add(profile);
			user.setUserProfiles(userProfiles);
			user.setPassword(userbean.getPassword());
			
			
			
						
			userService.saveUser(user);

	
			
		}
		


		return objectMapper.writeValueAsString("User is registered successfully!");

	}

}
