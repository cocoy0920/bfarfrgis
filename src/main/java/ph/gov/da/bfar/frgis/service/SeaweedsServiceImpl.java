package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.SeaWeedsPage;
import ph.gov.da.bfar.frgis.model.Seaweeds;
import ph.gov.da.bfar.frgis.repository.SeaWeedsRepo;


@Service("SeaweedsService")
@Transactional
public class SeaweedsServiceImpl implements SeaweedsService{

	@Autowired
	private SeaWeedsRepo dao;

	
	public Optional<Seaweeds> findById(int id) {
		return dao.findById(id);
	}

	public List<Seaweeds> ListByUsername(String username) {
		List<Seaweeds>Seaweeds = dao.getListSeaweedsByUsername(username);
		return Seaweeds;
	}

	public void saveSeaweeds(Seaweeds Seaweeds) {
		dao.save(Seaweeds);
	}
	
	@Override
	public void deleteSeaweedsById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateByEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateSeaweedsByReason(int id, String reason) {
		dao.updateSeaweedsByReason(id, reason);
		
	}

	public List<Seaweeds> findAllSeaweedss() {
		return dao.findAll();
	}

	@Override
	public List<Seaweeds> getAllSeaweedss(int index, int pagesize, String user) {
		
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<Seaweeds> page = dao.getPageableSeaweeds(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int SeaweedsCount(String username) {
		return dao.SeaweedsCount(username);
	}

	@Override
	public Seaweeds findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<Seaweeds> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<SeaWeedsPage> findByUsername(String user, Pageable pageable) {
	
		return dao.findByUsername(user, pageable);
	}
	
	@Override
	public Page<SeaWeedsPage> findAllSeaweeds(Pageable pageable) {
		
		return dao.findAllSeaweeds(pageable);
	}
	
	@Override
	public Page<SeaWeedsPage> findByRegion(String region, Pageable pageable) {

		return dao.findByRegion(region, pageable);
	}
	
	

	@Override
	public List<Seaweeds> ListByRegion(String region_id) {
	
		return dao.getListSeaweedsByRegion(region_id);
	}
	
	@Override
	public List<Seaweeds> ListByRegion(String region_id, String type) {
		
		return dao.getListSeaweedsByRegionAndType(region_id, type);
	}

	@Override
	public List<Seaweeds> findAllSeaweedss(String type) {

		return dao.findAllByType(type);
	}

	@Override
	public Long countSeaweedsPerRegion(String region_id) {

		return dao.countSeaweedsPerRegion(region_id);
	}

	@Override
	public Long countSeaweedsPerProvince(String province_id) {
	
		return dao.countSeaweedsPerProvince(province_id);
	}
	
	@Override
	public Long countSeaweedsPerProvince(String province_id, String type) {

		return dao.countSeaweedsPerProvinceAndType(province_id, type);
	}

	@Override
	public Long countAllSeaweeds() {

		return dao.countAllSeaweeds();
	}

	@Override
	public Long countSeaweedsPerMinucipality(String municipality_id) {

		return dao.countSeaweedsPerMunicipality(municipality_id);
	}

	@Override
	public Long countSeaweedsPerMinucipality(String municipality_id, String type) {

		return dao.countSeaweedsPerMunicipalityAndType(municipality_id, type);
	}

	@Override
	public Long countSeaweedsPerRegion(String region_id, String type) {

		return dao.countSeaweedsPerRegionAndType(region_id, type);
	}

}
