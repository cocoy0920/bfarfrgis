package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.FishCoralPage;
import ph.gov.da.bfar.frgis.model.FishCoral;

@Repository
public interface FishCoralRepo extends JpaRepository<FishCoral, Integer> {

	@Query("SELECT c from FishCoral c where c.user = :username")
	public List<FishCoral> getListFishCoralByUsername(@Param("username") String username);
	
	@Query("SELECT c from FishCoral c where c.region_id = :region_id")
	public List<FishCoral> getListFishCoralByRegion(@Param("region_id") String region_id);
	
	@Modifying(clearAutomatically = true)
	@Query("update FishCoral f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update FishCoral f set f.reason =:reason  where f.id =:id")
	public void updateFishCoralByReason( @Param("id") int id, @Param("reason") String reason);
	
	@Query("SELECT COUNT(f) FROM FishCoral f WHERE f.user=:username")
	public int  FishCoralCount(@Param("username") String username);
	
	@Query("SELECT c from FishCoral c where c.uniqueKey = :uniqueKey")
	public FishCoral findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from FishCoral c where c.uniqueKey = :uniqueKey")
	List<FishCoral> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from FishCoral c where c.user = :user")
	 public List<FishCoral> getPageableFishCorals(@Param("user") String username,Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishCoralPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfOperator,c.lat,c.lon,c.enabled) from FishCoral c where c.user = :user")
	public Page<FishCoralPage> findByUsername(@Param("user") String user, Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishCoralPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfOperator,c.lat,c.lon,c.enabled) from FishCoral c where c.region_id = :region_id")
	public Page<FishCoralPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishCoralPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfOperator,c.lat,c.lon,c.enabled) from FishCoral c")
	public Page<FishCoralPage> findAllFishCoral(Pageable pageable);
	
	@Query("SELECT COUNT(f) FROM FishCoral f")
	Long countAllFishCoral();
	
	@Query("SELECT COUNT(f) FROM FishCoral f WHERE f.region_id=:region_id")
	public Long countFishCoralPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM FishCoral f WHERE f.province_id=:province_id")
	public Long countFishCoralPerProvince(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM FishCoral f WHERE f.municipality_id=:municipality_id")
	public Long countFishCoralPerMunicipality(@Param("municipality_id") String municipality_id);
}
