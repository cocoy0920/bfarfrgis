package ph.gov.da.bfar.frgis.web.util;

import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.FishCoral;
import ph.gov.da.bfar.frgis.model.FishLanding;
import ph.gov.da.bfar.frgis.model.FishPen;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.Fishport;
import ph.gov.da.bfar.frgis.model.Hatchery;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;
import ph.gov.da.bfar.frgis.model.LGU;
import ph.gov.da.bfar.frgis.model.Mangrove;
import ph.gov.da.bfar.frgis.model.MaricultureZone;
import ph.gov.da.bfar.frgis.model.Market;
import ph.gov.da.bfar.frgis.model.Resources;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;
import ph.gov.da.bfar.frgis.model.SeaGrass;
import ph.gov.da.bfar.frgis.model.Seaweeds;
import ph.gov.da.bfar.frgis.model.TrainingCenter;

public class MapResourceBuilder {
	
	public  String getResources(Resources frm) {
		StringBuilder builder = new StringBuilder();
		
		if(frm.getPage() == "fishsanctuaries") {
			builder =  getFishSanctuary(frm);
		}else if(frm.getPage() == "fishprocessingplants") {
			builder =  getFishProcessingPlantMapData(frm);
		}
		return builder.toString();

	}

	
	public static StringBuilder getFishSanctuary(Resources frm) {
		 StringBuilder builder = new StringBuilder();
		   
		   builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Fish Sanctuary</span></p>");
		   builder.append("<table style=\"width:90%\">");
		   builder.append("<tr>");
		   builder.append("<th align=\"left\">Region</th>");
		   builder.append("<td>" + frm.getRegion() +"</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Province</th>");
		   builder.append("<td>" + frm.getProvince()  +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Municiapality</th>");
		   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Barangay</th>");
		   builder.append("<td>" + frm.getBarangay() +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Name Of Fish Sanctuary</th>");
		   builder.append("<td>"+ frm.getNameOfFishSanctuary()+ "</td>");
		   builder.append("</tr>");
		   if(frm.getBfardenr().equals("BFAR")) {
			   
			   builder.append("<tr>");
			   builder.append("<th align=\"left\">Implementer</th>");
			   builder.append("<td>"+ frm.getBfardenr()+ "</td>"); 
			   builder.append("</tr><tr>");
			   builder.append("<th align=\"left\">Name of Sheltered Fisheries/Species</th>");
			   builder.append("<td>"+ frm.getSheltered()+ "</td>"); 
			   builder.append("</tr>");
		   }
		   if(frm.getBfardenr().equals("DENR")) {
			   builder.append("<tr>");
			   builder.append("<th align=\"left\">Implementer</th>");
			   builder.append("<td>"+ frm.getBfardenr()+ "</td>"); 
			   builder.append("</tr>");
			   
//			   builder.append("<tr>");
//			   builder.append("<th align=\"left\">Name of Sensitive Habitat with MPA</th>");
//			   builder.append("<td>"+ frm.getNameOfSensitiveHabitatMPA()+ "</td>"); 
//			   builder.append("</tr>");
//			   
//			   builder.append("<tr>");
//			   builder.append("<th align=\"left\">Ordinance No.</th>");
//			   builder.append("<td>"+ frm.getOrdinanceNo()+ "</td>"); 
//			   builder.append("</tr>");
//			   
//			   builder.append("<tr>");
//			   builder.append("<th align=\"left\">Ordinance Title</th>");
//			   builder.append("<td>"+ frm.getOrdinanceTitle()+ "</td>"); 
//			   builder.append("</tr>");
//			   
//			   builder.append("<tr>");
//			   builder.append("<th align=\"left\">Date Established</th>");
//			   builder.append("<td>"+ frm.getDateEstablished()+ "</td>"); 
//			   builder.append("</tr>");
		   }
		  
		   builder.append("<tr>");
		   
		   builder.append("<th align=\"left\">Area</th>");
		   builder.append("<td>"+ frm.getArea()+ "</td>");
		   
		   builder.append("</tr><tr>");
		   
		   builder.append("<th align=\"left\">Type</th>");
		   builder.append("<td>"+ frm.getType()+ "</td>");
		   
		   builder.append("</tr><tr>");
		   
		   builder.append("<th align=\"left\">Latitude</th>");
		   builder.append("<td>" + frm.getLat()+ "</td>");
		   
		   builder.append("</tr><tr>");
		   
		   builder.append("<th align=\"left\">Longitude</th>");
		   builder.append("<td>"+  frm.getLon()+ "</td>");
		   
		   builder.append("</tr>");
		   builder.append("</table>");
		   
		   
		    
		    return  builder;
	}

	public static String getFishCageMapData(FishCage frm) {
        StringBuilder builder = new StringBuilder();
        builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Fish Cage</span></p>");
		   builder.append("<table style=\"width:90%\">");
		   builder.append("<tr>");
		   builder.append("<th align=\"left\">Region</th>");
		   builder.append("<td>"+ frm.getRegion() +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Province</th>");
		   builder.append("<td>" + frm.getProvince()  +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Municiapality</th>");
		   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Barangay</th>");
		   builder.append("<td>" + frm.getBarangay() +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Name of Operator</th>");
		   builder.append("<td>" + frm.getNameOfOperator()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Cage Dimension</th>");
		   builder.append("<td>" + frm.getCageDimension()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Indicate Species</th>");
		   builder.append("<td>" + frm.getIndicateSpecies()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Code</th>");
		   builder.append("<td>" + frm.getCode()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Latitude</th>");
		   builder.append("<td>" + frm.getLat()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Longitude</th>");
		   builder.append("<td>" + frm.getLon()+ "</td>");
		   builder.append("</tr>");
		   /*builder.append("<tr>");
		   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
		   builder.append("</tr>");*/
		   builder.append("</table>");
		   
		   return builder.toString();
	}
	
	public static String getFishCorralMapData(FishCoral frm) {
		 
		 StringBuilder builder = new StringBuilder();
		 builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Fish Coral</span></p>");
		   builder.append("<table style=\"width:90%\">");
		 
		   builder.append("<tr>");
		   builder.append("<th align=\"left\">Region</th>");
		   builder.append("<td>"  + frm.getRegion() +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Province</th>");
		   builder.append("<td>" + frm.getProvince()  +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Municiapality</th>");
		   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Barangay</th>");
		   builder.append("<td>" + frm.getBarangay() +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Name of Operator</th>");
		   builder.append("<td>" + frm.getNameOfOperator()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Area</th>");
		   builder.append("<td>"+ frm.getArea()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Operator Classification</th>");
		   builder.append("<td>" + frm.getOperatorClassification()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Name Of Station Gear Use</th>");
		   builder.append("<td>" + frm.getNameOfStationGearUse()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">No Of Unit Use</th>");
		   builder.append("<td>" + frm.getNoOfUnitUse()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Fishes Caught</th>");
		   builder.append("<td>" + frm.getFishesCaught()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Latitude</th>");
		   builder.append("<td>" + frm.getLat()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Longitude</th>");
		   builder.append("<td>" + frm.getLon()+ "</td>");
		   builder.append("</tr>");
		   /*builder.append("<tr>");
		   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
		   builder.append("</tr>");*/
		   builder.append("</table>");
		   
		   return builder.toString();
	}
	public static String getFishLandingMapData(FishLanding frm) {
		 StringBuilder builder = new StringBuilder();
		   
		 builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Fish Landing</span></p>");
		   builder.append("<table style=\"width:90%\">");
		 
		   builder.append("<tr>");
		   builder.append("<th align=\"left\">Region</th>");
		   builder.append("<td>" + frm.getRegion() +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Province</th>");
		   builder.append("<td>" + frm.getProvince()  +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Municiapality</th>");
		   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Barangay</th>");
		   builder.append("<td>" + frm.getBarangay() +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Name Of Landing</th>");
		   builder.append("<td>" + frm.getNameOfLanding()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Classification</th>");
		   builder.append("<td>" + frm.getClassification()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Volume Of Unloading MT</th>");
		   builder.append("<td>" + frm.getVolumeOfUnloadingMT()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Latitude</th>");
		   builder.append("<td>" + frm.getLat()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Longitude</th>");
		   builder.append("<td>" + frm.getLon()+ "</td>");
		   builder.append("</tr>");
		   /*builder.append("<tr>");
		   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
		   builder.append("</tr>");*/
		   builder.append("</table>");
		   
		    return builder.toString();
	}
	public static String getFishPenMapData(FishPen frm) {
		 StringBuilder builder = new StringBuilder();
		   
		   builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Fish Pen</span></p>");
		   builder.append("<table style=\"width:90%\">");
		   builder.append("<tr>");
		   builder.append("<th align=\"left\">Region</th>");
		   builder.append("<td>"+ frm.getRegion() +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Province</th>");
		   builder.append("<td>" + frm.getProvince()  +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Municiapality</th>");
		   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Barangay</th>");
		   builder.append("<td>" + frm.getBarangay() +  "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Name Of Operator</th>");
		   builder.append("<td>" + frm.getNameOfOperator()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Area</th>");
		   builder.append("<td>" + frm.getArea()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">No. Of Fish Pen</th>");
		   builder.append("<td>"  + frm.getNoOfFishPen()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Species Cultured</th>");
		   builder.append("<td>" + frm.getSpeciesCultured()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Cropping Start</th>");
		   builder.append("<td>"  + frm.getCroppingStart()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Cropping End</th>");
		   builder.append("<td>"+ frm.getCroppingEnd()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Latitude</th>");
		   builder.append("<td>"+ frm.getLat()+ "</td>");
		   builder.append("</tr><tr>");
		   builder.append("<th align=\"left\">Longitude</th>");
		   builder.append("<td>" + frm.getLon()+ "</td>");
		   builder.append("</tr>");
		   /*builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
		   builder.append("</tr>");*/
		   builder.append("</table>");
		   
		    return builder.toString();
	}

public static StringBuilder getFishProcessingPlantMapData(Resources frm) {
	 
	 StringBuilder builder = new StringBuilder();
	   
	 builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Fish Processing Plant</span></p>");
	   builder.append("<table style=\"width:90%\">");
	   builder.append("<tr>");
	   builder.append("<th align=\"left\">Region</th>");
	   builder.append("<td>"+ frm.getRegion() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Province</th>");
	   builder.append("<td>" + frm.getProvince()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Municiapality</th>");
	   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Barangay</th>");
	   builder.append("<td>" + frm.getBarangay() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Name Of Processing Plants</th>");
	   builder.append("<td>" + frm.getNameOfProcessingPlants()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Name Of Operator</th>");
	   builder.append("<td>"  + frm.getNameOfOperator()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Operator Classification</th>");
	   builder.append("<td>" + frm.getOperatorClassification()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Indicate Species</th>");
	   builder.append("<td>" + frm.getIndicateSpecies()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Processing Technique</th>");
	   builder.append("<td>" + frm.getProcessingTechnique()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Packaging Type</th>");
	   builder.append("<td>" + frm.getPackagingType()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Processing Environment Classification</th>");
	   builder.append("<td>"  + frm.getProcessingEnvironmentClassification()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Business Permits And Certificate Obtained</th>");
	   builder.append("<td>"  + frm.getBusinessPermitsAndCertificateObtained() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Market Reach</th>");
	   builder.append("<td>" + frm.getMarketReach()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Latitude</th>");
	   builder.append("<td>" + frm.getLat()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Longitude</th>");
	   builder.append("<td>" + frm.getLon()+ "</td>");
	   builder.append("</tr>");
	   /*builder.append("<tr>");
	   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
	   builder.append("</tr>");*/
	   builder.append("</table>");
	   
	   return builder;
}
public static String getIceColdMapData(IcePlantColdStorage frm) {
	 StringBuilder builder = new StringBuilder();
	   
	   builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Ice Plant/Cold Storage</span></p>");
	   builder.append("<table style=\"width:90%\">");
	   builder.append("<tr>");
	   builder.append("<th align=\"left\">Region</th>");
	   builder.append("<td>" + frm.getRegion() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Province</th>");
	   builder.append("<td>" + frm.getProvince()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Municiapality</th>");
	   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Barangay</th>");
	   builder.append("<td>" + frm.getBarangay() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Name Of Ice Plant</th>");
	   builder.append("<td>" + frm.getNameOfIcePlant()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Operator</th>");
	   builder.append("<td>" + frm.getOperator()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Type Of Facility</th>");
	   builder.append("<td>" + frm.getTypeOfFacility()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Structure Type</th>");
	   builder.append("<td>" + frm.getStructureType()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">ith Valid License To Operate</th>");
	   builder.append("<td>" + frm.getWithValidLicenseToOperate()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">With Valid Sanitary Permit</th>");
	   builder.append("<td>" + frm.getWithValidSanitaryPermit()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Business Permits And Certificate Obtained</th>");
	   builder.append("<td>" + frm.getBusinessPermitsAndCertificateObtained()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Type Of Ice Maker</th>");
	   builder.append("<td>" + frm.getTypeOfIceMaker()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Food Gradule</th>");
	   builder.append("<td>" + frm.getFoodGradule()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Source Of Equipment</th>");
	   builder.append("<td>" + frm.getSourceOfEquipment()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Capacity</th>");
	   builder.append("<td>"  + frm.getCapacity()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Latitude</th>");
	   builder.append("<td>" + frm.getLat()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Longitude</th>");
	   builder.append("<td>" + frm.getLon()+ "</td>");
	   builder.append("</tr>");
	   /*builder.append("<tr>");
	   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
	   builder.append("</tr>");
*/	   builder.append("</table>");
	   
	   return builder.toString();
	}
public static String getLGUMapData(LGU frm) {

	 StringBuilder builder = new StringBuilder();
	   
	 builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">PFO</span></p>");
	   builder.append("<table style=\"width:90%\">");
	   builder.append("<tr>");
	   builder.append("<th align=\"left\">Region</th>");
	   builder.append("<td>" + frm.getRegion() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Province</th>");
	   builder.append("<td>" + frm.getProvince()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Municiapality</th>");
	   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Barangay</th>");
	   builder.append("<td>" + frm.getBarangay() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">LGU Name</th>");
	   builder.append("<td>" + frm.getLguName()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">No Of Barangay Coastal</th>");
	   builder.append("<td>" + frm.getNoOfBarangayCoastal()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">No Of Barangay Inland</th>");
	   builder.append("<td>" + frm.getNoOfBarangayInland()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">LGU Coastal Length</th>");
	   builder.append("<td>" + frm.getLguCoastalLength()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Latitude</th>");
	   builder.append("<td>" + frm.getLat()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Longitude</th>");
	   builder.append("<td>" + frm.getLon()+ "</td>");
	   builder.append("</tr>");
	   /*builder.append("<tr>");
	   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
	   builder.append("</tr>");*/
	   builder.append("</table>");
	   
	    return builder.toString();
}
public static String getManggroveMapData(Mangrove frm) {
	 
	 StringBuilder builder = new StringBuilder();
	 	builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Mangrove</span></p>");
	   builder.append("<table style=\"width:90%\">");
	   builder.append("<tr>");
	   builder.append("<th align=\"left\">Region</th>");
	   builder.append("<td>" + frm.getRegion() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Province</th>");
	   builder.append("<td>" + frm.getProvince()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Municiapality</th>");
	   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Barangay</th>");
	   builder.append("<td>" + frm.getBarangay() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Indicate Species</th>");
	   builder.append("<td>" + frm.getIndicateSpecies()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Type</th>");
	   builder.append("<td>" + frm.getType()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Latitude</th>");
	   builder.append("<td>" + frm.getLat()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Longitude</th>");
	   builder.append("<td>" + frm.getLon()+ "</td>");
	   builder.append("</tr>");
	   /*builder.append("<tr>");
	   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
	   builder.append("</tr>");*/
	   builder.append("</table>");
	   
	    return builder.toString();
}
//public static String getMarineProtectedMapData(MarineProtectedArea frm) {
//	 
//	 StringBuilder builder = new StringBuilder();
//	   
//	   builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Marine Protected Area</span></p>");
//	   builder.append("<table style=\"width:90%\">");
//	   builder.append("<tr>");
//	   builder.append("<th align=\"left\">Region</th>");
//	   builder.append("<td>" + frm.getRegion() +  "</td>");
//	   builder.append("</tr><tr>");
//	   builder.append("<th align=\"left\">Province</th>");
//	   builder.append("<td>" + frm.getProvince()  +  "</td>");
//	   builder.append("</tr><tr>");
//	   builder.append("<th align=\"left\">Municiapality</th>");
//	   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
//	   builder.append("</tr><tr>");
//	   builder.append("<th align=\"left\">Barangay</th>");
//	   builder.append("<td>" + frm.getBarangay() +  "</td>");
//	   builder.append("</tr><tr>");
//	   builder.append("<th align=\"left\">Name Of Marine Protected Area</th>");
//	   builder.append("<td>" + frm.getNameOfMarineProtectedArea()+ "</td>");
//	   builder.append("</tr><tr>");
//	   builder.append("<th align=\"left\">Area</th>");
//	   builder.append("<td>" + frm.getArea()+ "</td>");
//	   builder.append("</tr><tr>");
//	   builder.append("<th align=\"left\">Type</th>");
//	   builder.append("<td>" + frm.getType()+ "</td>");
//	   builder.append("</tr><tr>");
//	   builder.append("<th align=\"left\">Sensitive Habitat with MPA</th>");
//	   builder.append("<td>" + frm.getSensitiveHabitatWithMPA()+ "</td>");
//	   builder.append("</tr><tr>");
//	   builder.append("<th align=\"left\">Latitude</th>");
//	   builder.append("<td>" + frm.getLat()+ "</td>");
//	   builder.append("</tr><tr>");
//	   builder.append("<th align=\"left\">Longitude</th>");
//	   builder.append("<td>" + frm.getLon()+ "</td>");
//	   builder.append("</tr>");
//	   /*builder.append("<tr>");
//	   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
//	   builder.append("</tr>");*/
//	   builder.append("</table>");
//	   
//	   return builder.toString();
//
//}
public static String getMarketMapData(Market frm) {
	 
	 StringBuilder builder = new StringBuilder();
	   
	   builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Market</span></p>");
	   builder.append("<table style=\"width:90%\">");
	   builder.append("<tr>");
	   builder.append("<th align=\"left\">Region</th>");
	   builder.append("<td>" + frm.getRegion() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Province</th>");
	   builder.append("<td>" + frm.getProvince()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Municiapality</th>");
	   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Barangay</th>");
	   builder.append("<td>" + frm.getBarangay() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Name Of Market</th>");
	   builder.append("<td>" + frm.getNameOfMarket()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Latitude</th>");
	   builder.append("<td>" + frm.getLat()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Longitude</th>");
	   builder.append("<td>"  + frm.getLon()+ "</td>");
	   builder.append("</tr>");
	   /*builder.append("<tr>");
	   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
	   builder.append("</tr>");*/
	   builder.append("</table>");
	   
	   return builder.toString();

}
public static String getSchoolOfFisheriesMapData(SchoolOfFisheries frm) {
	 StringBuilder builder = new StringBuilder();
	   
	   builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">School Of Fisheries</span></p>");
	   builder.append("<table style=\"width:90%\">");
	   builder.append("<tr>");
	   builder.append("<th align=\"left\">Region</th>");
	   builder.append("<td>"+ frm.getRegion() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Province</th>");
	   builder.append("<td>" + frm.getProvince()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Municiapality</th>");
	   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Barangay</th>");
	   builder.append("<td>" + frm.getBarangay() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Name</th>");
	   builder.append("<td>" + frm.getName()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Latitude</th>");
	   builder.append("<td>" + frm.getLat()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Longitude</th>");
	   builder.append("<td>" + frm.getLon()+ "</td>");
	   builder.append("</tr>");
	   /*builder.append("<tr>");
	   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
	   builder.append("</tr>");*/
	   builder.append("</table>");
	   
	    return builder.toString();
}
public static String getSeagrassMapData(SeaGrass frm) {
	 StringBuilder builder = new StringBuilder();
	   
	   builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Sea Grass</span></p>");
	   builder.append("<table style=\"width:90%\">");
	   builder.append("<tr>");
	   builder.append("<th align=\"left\">Region</th>");
	   builder.append("<td>" + frm.getRegion() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Province</th>");
	   builder.append("<td>" + frm.getProvince()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Municiapality</th>");
	   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Barangay</th>");
	   builder.append("<td>" + frm.getBarangay() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Latitude</th>");
	   builder.append("<td>" + frm.getLat()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Longitude</th>");
	   builder.append("<td>" + frm.getLon()+ "</td>");
	   builder.append("</tr>");
	   /*builder.append("<tr>");
	   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
	   builder.append("</tr>");*/
	   builder.append("</table>");
	   
	   return builder.toString();
}
public static String getSeaweedsMapData(Seaweeds frm) {
	 StringBuilder builder = new StringBuilder();
	   
	   builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Seaweeds</span></p>");
	   builder.append("<table style=\"width:90%\">");
	   builder.append("<tr>");
	   builder.append("<th align=\"left\">Region</th>");
	   builder.append("<td>" + frm.getRegion() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Province</th>");
	   builder.append("<td>" + frm.getProvince()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Municiapality</th>");
	   builder.append("<td>"  + frm.getMunicipality()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Barangay</th>");
	   builder.append("<td>" + frm.getBarangay() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Area</th>");
	   builder.append("<td>" + frm.getArea() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Indicate Genus</th>");
	   builder.append("<td>" + frm.getIndicateGenus() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Cultured Method Used</th>");
	   builder.append("<td>" + frm.getCulturedMethodUsed() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Cultured Period</th>");
	   builder.append("<td>" + frm.getCulturedPeriod() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Production Kg Per Cropping</th>");
	   builder.append("<td>" + frm.getProductionKgPerCropping() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Production No Of Cropping Per Year</th>");
	   builder.append("<td>"  + frm.getProductionNoOfCroppingPerYear() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Latitude</th>");
	   builder.append("<td>" + frm.getLat()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Longitude</th>");
	   builder.append("<td>" + frm.getLon()+ "</td>");
	   builder.append("</tr>");
	  /* builder.append("<tr>");
	   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
	   builder.append("</tr>");*/
	   builder.append("</table>");
	   
	   return builder.toString();
}
public static String getTrainingCenterMapData(TrainingCenter frm) {
	 StringBuilder builder = new StringBuilder();
	   
	   builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Training Center</span></p>");
	   builder.append("<table style=\"width:90%\">");
	   builder.append("<tr>");
	   builder.append("<th align=\"left\">Region</th>");
	   builder.append("<td>" + frm.getRegion() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Province</th>");
	   builder.append("<td>" + frm.getProvince()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Municiapality</th>");
	   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Barangay</th>");
	   builder.append("<td>" + frm.getBarangay() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Name</th>");
	   builder.append("<td>" + frm.getName() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Specialization</th>");
	   builder.append("<td>" + frm.getSpecialization() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Facility within the Training Center</th>");
	   builder.append("<td>"  + frm.getFacilityWithinTheTrainingCenter() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Latitude</th>");
	   builder.append("<td>" + frm.getLat()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Longitude</th>");
	   builder.append("<td>" + frm.getLon()+ "</td>");
	   builder.append("</tr>");
	  /* builder.append("<tr>");
	   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
	   builder.append("</tr>");*/
	   builder.append("</table>");
	   
	    return builder.toString();
}
public static String getHatcheryMapData(Hatchery frm) {
	 
	 StringBuilder builder = new StringBuilder();
	   
	  builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Hatchery</span></p>");
	   builder.append("<table style=\"width:90%\">");
	   builder.append("<tr>");
	   builder.append("<th align=\"left\">Region</th>");
	   builder.append("<td>" + frm.getRegion() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Province</th>");
	   builder.append("<td>" + frm.getProvince()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Municiapality</th>");
	   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Barangay</th>");
	   builder.append("<td>" + frm.getBarangay() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Name Of Hatchery</th>");
	   builder.append("<td>" + frm.getNameOfHatchery() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Name Of Operator</th>");
	   builder.append("<td>" + frm.getNameOfOperator() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Operator Classification</th>");
	   builder.append("<td>" + frm.getOperatorClassification() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Address of Operator</th>");
	   builder.append("<td>" + frm.getAddressOfOperator() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Area</th>");
	   builder.append("<td>" + frm.getArea() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Public or Private</th>");
	   builder.append("<td>" + frm.getPublicprivate() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Indicate Species</th>");
	   builder.append("<td>" + frm.getIndicateSpecies() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Product Stocking</th>");
	   builder.append("<td>" + frm.getProdStocking() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Product Cycle</th>");
	   builder.append("<td>" + frm.getProdCycle() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Actual Product For The Year</th>");
	   builder.append("<td>" + frm.getActualProdForTheYear() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Latitude</th>");
	   builder.append("<td>" + frm.getLat()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Longitude</th>");
	   builder.append("<td>" + frm.getLon()+ "</td>");
	   builder.append("</tr>");
	   /*builder.append("<tr>");
	   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
	   builder.append("</tr>");*/
	   builder.append("</table>");
	   
	   return builder.toString();
}
public static String getMaricultureZoneMapData(MaricultureZone frm) {
	 StringBuilder builder = new StringBuilder();
	   
	   
	   builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Mariculture Zone</span></p>");
	   builder.append("<table style=\"width:90%\">");
	   builder.append("<tr>");
	   builder.append("<th align=\"left\">Region</th>");
	   builder.append("<td>" + frm.getRegion() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Province</th>");
	   builder.append("<td>" + frm.getProvince()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Municiapality</th>");
	   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Barangay</th>");
	   builder.append("<td>" + frm.getBarangay() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Name of Mariculture</th>");
	   builder.append("<td>" + frm.getNameOfMariculture()+  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Mariculture Type</th>");
	   builder.append("<td>" + frm.getMaricultureType() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Date Inacted</th>");
	   builder.append("<td>" + frm.getDateInacted() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Date Approved</th>");
	   builder.append("<td>"  + frm.getDateApproved() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Kind</th>");
	   builder.append("<td>" + frm.getKind() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Production per Cropping</th>");
	   builder.append("<td>" + frm.getProductionPerCropping() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Code</th>");
	   builder.append("<td>" + frm.getCode() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Data Source</th>");
	   builder.append("<td>" + frm.getDataSource() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Latitude</th>");
	   builder.append("<td>"  + frm.getLat()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Longitude</th>");
	   builder.append("<td>" + frm.getLon()+ "</td>");
	   builder.append("</tr>");
	   /*builder.append("<tr>");
	   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
	   builder.append("</tr>");*/
	   builder.append("</table>");
	   
	   return builder.toString();
}
public static String getFishPortMapData(Fishport frm) {
	 
	 StringBuilder builder = new StringBuilder();
	   
	 builder.append("<p style=\"text-align:center; font-size: 24px; color:#175B81; font-weight:bold; \"><span id=\"sport\">Fish Port</span></p>");
	   builder.append("<table style=\"width:90%\">");
	   builder.append("<tr>");
	   builder.append("<th align=\"left\">Region</th>");
	   builder.append("<td>" + frm.getRegion() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Province</th>");
	   builder.append("<td>" + frm.getProvince()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Municiapality</th>");
	   builder.append("<td>" + frm.getMunicipality()  +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Barangay</th>");
	   builder.append("<td>" + frm.getBarangay() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Name of Fishport</th>");
	   builder.append("<td>" + frm.getNameOfFishport() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Classification</th>");
	   builder.append("<td>" + frm.getClassification() +  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Data Source</th>");
	   builder.append("<td>" + frm.getDataSource()+  "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Latitude</th>");
	   builder.append("<td>" + frm.getLat()+ "</td>");
	   builder.append("</tr><tr>");
	   builder.append("<th align=\"left\">Longitude</th>");
	   builder.append("<td>" + frm.getLon()+ "</td>");
	   builder.append("</tr>");
	   /*builder.append("<tr>");
	   builder.append("<td>" + "<img src=\"../../../../frgis_captured_images/"+frm.getImage()+"\"></img>" + "</td>");
	   builder.append("</tr>");*/
	   builder.append("</table>");
	   
	   return builder.toString();
}
}
