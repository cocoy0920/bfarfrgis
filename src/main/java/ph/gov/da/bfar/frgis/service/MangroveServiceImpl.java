package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.MangrovePage;
import ph.gov.da.bfar.frgis.model.Mangrove;
import ph.gov.da.bfar.frgis.repository.MangroveRepo;


@Service("MangroveService")
@Transactional
public class MangroveServiceImpl implements MangroveService{

	@Autowired
	private MangroveRepo dao;

	
	public Optional<Mangrove> findById(int id) {
		return dao.findById(id);
	}

	public List<Mangrove> ListByUsername(String username) {
		List<Mangrove>Mangrove = dao.getListMangroveByUsername(username);
		return Mangrove;
	}

	public void saveMangrove(Mangrove Mangrove) {
		dao.save(Mangrove);
	}
	
	
	@Override
	public void deleteMangroveById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateByEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateMangroveByReason(int id, String reason) {
		dao.updateMangroveByReason(id, reason);
		
	}

	public List<Mangrove> findAllMangroves() {
		return dao.findAll();
	}

	@Override
	public List<Mangrove> getAllMangroves(int index, int pagesize, String user) {
		
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<Mangrove> page = dao.getPageableMangrove(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int MangroveCount(String username) {
		return dao.MangroveCount(username);
	}

	@Override
	public Mangrove findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<Mangrove> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<MangrovePage> findByUsername(String user, Pageable pageable) {

		return dao.findByUsername(user, pageable);
	}
	
	@Override
	public Page<MangrovePage> findAllMangrove(Pageable pageable) {

		return dao.findAllMangrove(pageable);
	}
	
	@Override
	public Page<MangrovePage> findByRegion(String region, Pageable pageable) {
	
		return dao.findByRegion(region, pageable);
	}

	@Override
	public List<Mangrove> ListByRegion(String region_id) {
		
		return dao.getListMangroveByRegion(region_id);
	}

	@Override
	public Long countMangrovePerRegion(String region_id) {

		return dao.countMangrovePerRegion(region_id);
	}

	@Override
	public Long countMangrovePerProvince(String province_id) {

		return dao.countMangrovePerProvince(province_id);
	}

	@Override
	public Long countAllMangrove() {
		
		return dao.countAllMangrove();
	}

	@Override
	public Long countMangrovePerMunicipality(String municipality_id) {

		return dao.countMangrovePerMunicipality(municipality_id);
	}

	
	
}
