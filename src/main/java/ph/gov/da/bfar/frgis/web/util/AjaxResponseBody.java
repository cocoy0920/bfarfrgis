package ph.gov.da.bfar.frgis.web.util;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;

@Getter
@Setter
public class AjaxResponseBody {

	   String msg;
	    List<FishSanctuaries> resultFishSanctuaries;
	    
	    
}
