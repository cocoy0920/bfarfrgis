package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.PayaoPage;
import ph.gov.da.bfar.frgis.model.Payao;





public interface PayaoService {
	
	Optional<Payao> findById(int id);
	
	List<Payao> ListByUsername(String region);
	List<Payao> ListByRegion(String region_id);
	Payao findByUniqueKey(String uniqueKey);
	List<Payao> findByUniqueKeyForUpdate(String uniqueKey);
	void savePayao(Payao Payao);
	void updateByEnabled(int  id);
	void updatePayaoByReason(int  id,String  reason);
	List<Payao> findAllPayao(); 
    public List<Payao> getAllPayao(int index,int pagesize,String user);
    public int PayaoCount(String username);
    public int PayaoSumByRegion(String region_id);
    public int PayaoSumAll();
    public Page<PayaoPage> findByUsername(@Param("user") String user, Pageable pageable);
    public Page<PayaoPage> findByRegion(@Param("region") String region, Pageable pageable);
    public Page<PayaoPage> findAllPayao(Pageable pageable);
    public Long countAllPayao();
    public Long countPayaoPerRegion(String region_id);
    public Long countPayaoProvince(String province_id);
    public Long countPayaoPerMunicipality(String municipality_id);

}