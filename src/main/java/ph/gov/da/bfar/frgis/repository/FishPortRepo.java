package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.FishPortPage;
import ph.gov.da.bfar.frgis.model.Fishport;

@Repository
public interface FishPortRepo extends JpaRepository<Fishport, Integer> {

	@Query("SELECT c from Fishport c where c.user = :username")
	public List<Fishport> getListFishportByUsername(@Param("username") String username);
	
	@Query("SELECT c from Fishport c where c.region_id = :region_id")
	public List<Fishport> getListFishportByRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM Fishport f WHERE f.user=:username")
	public int  FishportCount(@Param("username") String username);
	
	@Query("SELECT c from Fishport c where c.uniqueKey = :uniqueKey")
	public Fishport findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Modifying(clearAutomatically = true)
	@Query("update Fishport f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update Fishport f set f.reason =:reason  where f.id =:id")
	public void updateFishportByReason( @Param("id") int id, @Param("reason") String reason);
	
	@Query("SELECT c from Fishport c where c.uniqueKey = :uniqueKey")
	List<Fishport> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from Fishport c where c.user = :user")
	 public List<Fishport> getPageableFishports(@Param("user") String user,Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishPortPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfFishport,c.lat,c.lon,c.enabled) from Fishport c where c.user = :user")
	public Page<FishPortPage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishPortPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfFishport,c.lat,c.lon,c.enabled) from Fishport c where c.region_id = :region_id")
	public Page<FishPortPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishPortPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfFishport,c.lat,c.lon,c.enabled) from Fishport c")
	public Page<FishPortPage> findAllFishPort(Pageable pageable);

	
	@Query("SELECT COUNT(f) FROM Fishport f")
	public Long countAllFishport();
	
	@Query("SELECT COUNT(f) FROM Fishport f WHERE f.region_id=:region_id")
	public Long countFishportPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM Fishport f WHERE f.province_id=:province_id")
	public Long countFishportPerProvince(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM Fishport f WHERE f.municipality_id=:municipality_id")
	public Long countFishportPerMunicipality(@Param("municipality_id") String municipality_id);
}
