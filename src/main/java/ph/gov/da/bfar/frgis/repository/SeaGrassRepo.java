package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.SeaGrassPage;
import ph.gov.da.bfar.frgis.model.SeaGrass;

@Repository
public interface SeaGrassRepo extends JpaRepository<SeaGrass, Integer> {

	@Query("SELECT c from SeaGrass c where c.user = :username")
	public List<SeaGrass> getListSeaGrassByUsername(@Param("username") String username);
	
	@Query("SELECT c from SeaGrass c where c.region_id = :region_id")
	public List<SeaGrass> getListSeaGrassByRegion(@Param("region_id") String region_id);

	@Modifying(clearAutomatically = true)
	@Query("update SeaGrass f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update SeaGrass f set f.reason =:reason  where f.id =:id")
	public void updateSeaGrassByReason( @Param("id") int id, @Param("reason") String reason);
	
	@Query("SELECT COUNT(f) FROM SeaGrass f WHERE f.user=:username")
	public int  SeaGrassCount(@Param("username") String username);
	
	@Query("SELECT c from SeaGrass c where c.uniqueKey = :uniqueKey")
	public SeaGrass findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from SeaGrass c where c.uniqueKey = :uniqueKey")
	List<SeaGrass> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from SeaGrass c where c.user = :user")
	 public List<SeaGrass> getPageableSeaGrass(@Param("user") String user,Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.SeaGrassPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled) from SeaGrass c where c.user = :user")
	public Page<SeaGrassPage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.SeaGrassPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled) from SeaGrass c where c.region_id = :region_id")
	public Page<SeaGrassPage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.SeaGrassPage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.lat,c.lon,c.enabled) from SeaGrass c")
	public Page<SeaGrassPage> findAllSeaGrass(Pageable pageable);

	@Query("SELECT COUNT(f) FROM SeaGrass f")
	public Long countAllSeaGrass();
	
	@Query("SELECT COUNT(f) FROM SeaGrass f WHERE f.region_id=:region_id")
	public Long countSeaGrassPerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM SeaGrass f WHERE f.province_id=:province_id")
	public Long countSeaGrassPerProvince(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM SeaGrass f WHERE f.municipality_id=:municipality_id")
	public Long countSeaGrassPerMunicipality(@Param("municipality_id") String municipality_id);

}
