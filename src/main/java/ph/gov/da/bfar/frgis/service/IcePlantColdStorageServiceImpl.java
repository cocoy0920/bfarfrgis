package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.ColdStoragePage;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;
import ph.gov.da.bfar.frgis.repository.IPCSRepo;




@Service("IcePlantColdStorageService")
@Transactional
public class IcePlantColdStorageServiceImpl implements IcePlantColdStorageService{

	@Autowired
	private IPCSRepo dao;


	public Optional<IcePlantColdStorage> findById(int id) {
		return dao.findById(id);
	}

	public List<IcePlantColdStorage> ListByUsername(String username) {
		List<IcePlantColdStorage>IcePlantColdStorage = dao.getListIcePlantColdStorageByUsername(username);
		return IcePlantColdStorage;
	}

	public void saveIcePlantColdStorage(IcePlantColdStorage IcePlantColdStorage) {
		dao.save(IcePlantColdStorage);
	}

	@Override
	public void deleteIcePlantColdStorageById(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void updateeByEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateIcePlantColdStorageeByReason(int id, String reason) {
		dao.updateIcePlantColdStorageByReason(id, reason);
		
	}

	public List<IcePlantColdStorage> findAllIcePlantColdStorages() {
		return dao.findAll();
	}
	
	

	@Override
	public List<IcePlantColdStorage> ListByRegion(String region_id, String type) {
		
		return dao.getListIcePlantColdStorageByRegionAndType(region_id, type);
	}

	@Override
	public List<IcePlantColdStorage> findAllIcePlantColdStorages(String type) {
	
		return dao.findAllByType(type);
	}

	@Override
	public List<IcePlantColdStorage> getAllIcePlantColdStorages(int index, int pagesize, String user) {
		
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<IcePlantColdStorage> page = dao.getPageableIcePlantColdStorage(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int IcePlantColdStorageCount(String username) {
		return dao.IcePlantColdStorageCount(username);
	}

	@Override
	public IcePlantColdStorage findByUniqueKey(String uniqueKey) {
	
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<IcePlantColdStorage> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<ColdStoragePage> findByUsername(String user, Pageable pageable) {
		
		return dao.findByUsername(user, pageable);
	}
	
	@Override
	public Page<ColdStoragePage> findAllICPS(Pageable pageable) {

		return dao.findAllIPCS(pageable);
	}
	
	@Override
	public Page<ColdStoragePage> findByRegion(String region, Pageable pageable) {

		return dao.findByRegion(region, pageable);
	}

	@Override
	public List<IcePlantColdStorage> ListByRegion(String region_id) {
		
		return dao.getListIcePlantColdStorageByRegion(region_id);
	}

	@Override
	public Long countIcePlantColdStoragePerRegion(String region_id) {

		return dao.countIcePlantColdStoragePerRegion(region_id);
	}

	@Override
	public Long countIcePlantColdStoragePerProvince(String province_id) {
	
		return dao.countIcePlantColdStoragePerProvince(province_id);
	}

	@Override
	public Long countAllIcePlantColdStorage() {
		
		return dao.countAllIcePlantColdStorage();
	}

	@Override
	public Long countIcePlantColdStoragePerMunicipality(String municipality_id) {
		
		return dao.countIcePlantColdStoragePerMunicipality(municipality_id);
	}

	@Override
	public Long countIcePlantColdStoragePerRegion(String region_id, String operator) {

		return dao.countIcePlantColdStoragePerRegion(region_id, operator);
	}

	@Override
	public Long countIcePlantColdStoragePerProvince(String province_id, String operator) {

		return dao.countIcePlantColdStoragePerProvince(province_id, operator);
	}

	@Override
	public Long countIcePlantColdStoragePerMunicipality(String municipality_id, String operator) {
		
		return dao.countIcePlantColdStoragePerMunicipality(municipality_id, operator);
	}

	
}
