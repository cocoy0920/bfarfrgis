package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import ph.gov.da.bfar.frgis.model.Municipalities;




public interface MunicipalityService {
	
	Optional<Municipalities> findById(int id);
	
	void save(Municipalities municipality);
	
	void update(Municipalities municipality);
		
	List<Municipalities> findAllMunicipalities();

	List<Municipalities> findByProvince(String provinceId);
}