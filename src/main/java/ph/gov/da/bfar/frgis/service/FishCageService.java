package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ph.gov.da.bfar.frgis.Page.FishCagePage;
import ph.gov.da.bfar.frgis.model.FishCage;



public interface FishCageService {
	
	Optional<FishCage> findById(int id);
	
	List<FishCage> getListFishCageByUsername(String region);
	
	 List<FishCage> getListByRegion(String regionID);
	
	FishCage findByUniqueKey(String uniqueKey);
	List<FishCage> findByUniqueKeyForUpdate(String uniqueKey);
	void updateFishCageByEnabled(int  id);
	void updateFishCageByForReason(int  id,String  reason);
	void saveFishCage(FishCage FishCage);
		
	void deleteFishCage(int id);

	List<FishCage> findAllFishCages(); 
    public List<FishCage> getAllFishCages(int index,int pagesize,String user);
    public int FishCageCount(String username);
    Page<FishCagePage> findByUsername(String user, Pageable pageable);
    Page<FishCagePage> findByRegion(String region, Pageable pageable);
    Page<FishCagePage> findAllFishcage(Pageable pageable);
    public Long countAllFishCage();
    public Long countFishCagePerRegion(String region_id);
    public Long countFishCagePerProvince(String province_id);
    public Long countFishCagePerMunicipality(String municipality_id);

}