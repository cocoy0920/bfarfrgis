package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import ph.gov.da.bfar.frgis.Page.ColdStoragePage;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;


public interface IcePlantColdStorageService {
	
	Optional<IcePlantColdStorage> findById(int id);
	
	List<IcePlantColdStorage> ListByUsername(String username);
	List<IcePlantColdStorage> ListByRegion(String region_id);
	List<IcePlantColdStorage> ListByRegion(String region_id,String type);
	IcePlantColdStorage findByUniqueKey(String uniqueKey);
	List<IcePlantColdStorage> findByUniqueKeyForUpdate(String uniqueKey);
	void saveIcePlantColdStorage(IcePlantColdStorage IcePlantColdStorage);
	void deleteIcePlantColdStorageById(int id);
	void updateeByEnabled(int id);
	void updateIcePlantColdStorageeByReason(int  id,String  reason);
	List<IcePlantColdStorage> findAllIcePlantColdStorages(); 
	List<IcePlantColdStorage> findAllIcePlantColdStorages(String type); 
    public List<IcePlantColdStorage> getAllIcePlantColdStorages(int index,int pagesize,String user);
    public int IcePlantColdStorageCount(String username);
    public Page<ColdStoragePage> findByUsername(@Param("user") String user, Pageable pageable);
    public Page<ColdStoragePage> findByRegion(@Param("region") String region, Pageable pageable);
    public Page<ColdStoragePage> findAllICPS(Pageable pageable);
    
    public Long countAllIcePlantColdStorage();
    public Long countIcePlantColdStoragePerRegion(String region_id);
    public Long countIcePlantColdStoragePerProvince(String province_id);
    public Long countIcePlantColdStoragePerMunicipality(String municipality_id);
    public Long countIcePlantColdStoragePerRegion(String region_id,String operator);
    public Long countIcePlantColdStoragePerProvince(String province_id,String operator);
    public Long countIcePlantColdStoragePerMunicipality(String municipality_id,String operator);

}