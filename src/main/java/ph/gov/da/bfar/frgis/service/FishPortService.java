package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ph.gov.da.bfar.frgis.Page.FishPortPage;
import ph.gov.da.bfar.frgis.model.Fishport;



public interface FishPortService {
	
	Optional<Fishport> findById(int id);
	
	List<Fishport> findByUsername(String username);
	List<Fishport> findByRegion(String region_id);
	Fishport findByUniqueKey(String uniqueKey);
	List<Fishport> findByUniqueKeyForUpdate(String uniqueKey);
	void saveFishport(Fishport fishport);
	void deleteFishportById(int id);
	void updateForEnabled(int  id);
	void updateFishportByReason(int  id,String reason);
	List<Fishport> findAllFishports(); 
    public List<Fishport> getAllFishports(int index,int pagesize,String user);
    public int FishportCount(String username);
    public Page<FishPortPage> findByUsername(String user, Pageable pageable);
    public Page<FishPortPage> findByRegion(String region, Pageable pageable);
    public Page<FishPortPage> findAllFishPort(Pageable pageable);
	public Long countFishportPerRegion(String region_id);   
	public Long countFishportPerProvince(String province_id);  
	public Long countFishportPerMunicipality(String municipality_id);  
	public Long countAllFishport();
//    public int FishPortCountByMuunicipality(String municipalityID);
//    public int FishPortCountByProvince(String provinceID);
//    public int FishPortCountByRegion(String regionID);
//	
	//boolean isConsignationSSOUnique(Integer id, String sso);

}