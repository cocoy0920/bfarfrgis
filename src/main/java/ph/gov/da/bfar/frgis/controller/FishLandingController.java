package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.FishLandingPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishLanding;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.service.FishLandingService;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class FishLandingController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	FishLandingService fishLandingService;

	 @Autowired
	 private IAuthenticationFacade authenticationFacade;
	 
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	
	private String map_data="";
	private String newUniqueKeyforMap="";
	User user;	
	@RequestMapping(value = "/fishlanding", method = RequestMethod.GET)
	public ModelAndView fishlanding(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();	 
		user = authenticationFacade.getUserInfo();

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", authenticationFacade.getUserInfo());
		model.addAttribute("page", "fishlanding");
		model.addAttribute("username", authenticationFacade.getUsername());
		model.addAttribute("region", authenticationFacade.getRegionName());
		model.addAttribute("region_id", authenticationFacade.getRegionID());
		result = new ModelAndView("users/form");
		
		return result;
	}
	@RequestMapping(value = "/fishlandings", method = RequestMethod.GET)
	public ModelAndView fishlandings(ModelMap model) throws Exception {

		ModelAndView result = new ModelAndView();
		user   = new User();	 
		user = authenticationFacade.getUserInfo();

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "fishlanding");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		result = new ModelAndView("users/form");
		
		return result;
	}

	@RequestMapping(value = "/fishLandingMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishLandingMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();
		 System.out.println("uniqueKey: " + uniqueKey);
		 ObjectMapper mapper = new ObjectMapper();
			FishLanding data = fishLandingService.findByUniqueKey(uniqueKey);
			//System.out.println(data);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	
		 	Features features = new Features();
			 features = MapBuilder.getFishLandingMapData(data);
			 
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	String CFLC ="";
		 	if(data.getCflc_status()!=null) {
		 		CFLC = data.getCflc_status();
		 	}
		 	System.out.println("CFLC: "+ CFLC);
		 	if(CFLC.equalsIgnoreCase("Operational")) {
		 		mapsData.setIcon("/static/images/pin2022/CFLC_operational.png");
		 	}else if(CFLC.equalsIgnoreCase("ForOperation")) {
		 		mapsData.setIcon("/static/images/pin2022/CFLC_foroperational.png");
		 	}else if(CFLC.equalsIgnoreCase("ForCompletion")) {
		 		mapsData.setIcon("/static/images/pin2022/CFLC_completion.png");
		 	}else if(CFLC.equalsIgnoreCase("ForTransfer")) {
		 		mapsData.setIcon("/static/images/pin2022/CFLC_transfer.png");
		 	}else if(CFLC.equalsIgnoreCase("Damaged")) {
		 		mapsData.setIcon("/static/images/pin2022/CFLC_damaged.png");
		 	}else if(CFLC.equalsIgnoreCase("NonTraditional")) {
		 		mapsData.setIcon("/static/images/pin2022/non-traditional.png");
		 	}else if(CFLC.equalsIgnoreCase("Traditional")) {
		 		mapsData.setIcon("/static/images/pin2022/traditional.png");
		 	}
	
		 	System.out.println(mapsData);
		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			map_data  = jsonString;
			result = new ModelAndView("redirect:../fishlandings");	
		return result;
	}

	@RequestMapping(value = "/fishLandingDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView fishLandingMapByUniqueKey(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();
		 
		// fishLandingService.deleteFishLandingById(id);
		 fishLandingService.updateForEnabled(id);
			result = new ModelAndView("redirect:../fishlandings");	
			return result;
	}
	@RequestMapping(value = "/fishLandingDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView fishLandingMapByUniqueKey(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result = new ModelAndView();

		 fishLandingService.updateForEnabled(id);
		 fishLandingService.updateFishLandingByReason(id, reason);
			result = new ModelAndView("redirect:../../fishlandings");	
			return result;
	}
	
	@RequestMapping(value = "/getfishLandingMap/{uniqueKey}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getByUniqueKey(@PathVariable("uniqueKey") String uniqueKey)
			throws JsonGenerationException, JsonMappingException, IOException {

		
		 
		 ObjectMapper mapper = new ObjectMapper();
			FishLanding data = fishLandingService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	String CFLC = data.getCflc_status().replace(" ", "");;
		 	if(CFLC.equals("Operational")) {
		 		mapsData.setIcon("/static/images/pin2022/CFLC_operational.png");
		 	}else if(CFLC.equalsIgnoreCase("For Operation")) {
		 		mapsData.setIcon("/static/images/pin2022/CFLC_foroperational.png");
		 	}else if(CFLC.equalsIgnoreCase("For Completion")) {
		 		mapsData.setIcon("/static/images/pin2022/CFLC_completion.png");
		 	}else if(CFLC.equalsIgnoreCase("For Transfer")) {
		 		mapsData.setIcon("/static/images/pin2022/CFLC_transfer.png");
		 	}else if(CFLC.equalsIgnoreCase("Damaged")) {
		 		mapsData.setIcon("/static/images/pin2022/CFLC_damaged.png");
		 	}else if(CFLC.equalsIgnoreCase("Non-Traditional")) {
		 		mapsData.setIcon("/static/images/pin2022/non-traditional.png");
		 	}else if(CFLC.equalsIgnoreCase("Traditional")) {
		 		mapsData.setIcon("/static/images/pin2022/traditional.png");
		 	}
		 	
		 	//mapsData.setImage(data.getImage_src());
		 	String html = "<div class=\"p-3 mb-2 bg-primary text-white text-center\">FISH LANDING</div>"+
					"<img class=\"imgicon\" id=\"myImg\" src=" +data.getImage() +" style=\"width:300px;max-width:200px;height:200px\"></img><br/>" +						
					"<table>" + 
					"<tr><td>Location:</td> " + 
					"<td>" + data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay() + "</td></tr>" +
					"<tr><td>Name of Fish Landing :</td> " + 
					"<td>" +data.getNameOfLanding() + "</td></tr>" +					
					"<tr><td>Area:</td> " + 
					"<td>" + data.getArea() + "</td></tr>" +
					"<tr><td>Volume of Unloading:</td> " + 
					"<td>" + data.getVolumeOfUnloadingMT()+ "</td></tr>" +
					"<tr><td>Classification:</td> " + 
					"<td>" + data.getClassification()+ "</td></tr>" +
					"<tr><td>Type:</td> " + 
					"<td>" + data.getType()+ "</td></tr>" +
					"<tr><td>CFLC Status:</td> " + 
					"<td>" + data.getCflc_status()+ "</td></tr>" +
					"<tr><td>Coordinates:</td> " +
					"<td>" + data.getLat() + "," + data.getLon()+ "</td></tr></table>";
		 	mapsData.setHtml(html);
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			

			return jsonString;
			
	}
	

	



	@RequestMapping(value = "/fishlandings/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishlanding(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {
		
		
		 Page<FishLandingPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 15, Sort.by("id"));
	      
	          page = fishLandingService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<FishLandingPage> FishLandingList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			//builder.append("<ul class=\"pagination pagination-sm\">");
	          builder.append("<ul class=\"list-group list-group-horizontal-sm\">");
			
			  

			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				if(number+1 == y) {
					//String numberColor = "<span class=\"btn-primary\">" + y + "</span>";
					builder.append("<li class=\"list-group-item active\"><a data-target=" + i + ">" + y + "</a></li>");
				}else {
					builder.append("<li class=\"list-group-item\"><a data-target=" + i + ">" + y + "</a></li>");
				}
//				if(number+1 == y) {
//					String numberColor = "<span class=\"btn-primary\">" + y + "</span>";
//					builder.append("  <li class=\"page-item \"><a class=\"page-link\" data-target=" + i + ">" + numberColor + "</a></li>");
//				}else {
//					builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
//				}
				
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setFishLanding(FishLandingList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}

	@RequestMapping(value = "/saveFishLanding", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postFishLanding(@RequestBody FishLanding fishLanding) throws JsonGenerationException, JsonMappingException, IOException {

		savingResources(fishLanding);


		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);
		

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);

	}



	private void savingResources(FishLanding fishLanding) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		fishLanding.setRegion_id(region_data[0]);
		fishLanding.setRegion(region_data[1]);

		province = fishLanding.getProvince();
		data = province.split(",", 2);
		fishLanding.setProvince_id(data[0]);
		fishLanding.setProvince(data[1]);

		municipal = fishLanding.getMunicipality();
		municipal_data = municipal.split(",", 2);
		fishLanding.setMunicipality_id(municipal_data[0]);
		fishLanding.setMunicipality(municipal_data[1]);

		barangay = fishLanding.getBarangay();
		barangay_data = barangay.split(",", 3);
		fishLanding.setBarangay_id(barangay_data[0]);
		fishLanding.setBarangay(barangay_data[1]);
		fishLanding.setEnabled(true);

		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(fishLanding.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");

		if (fishLanding.getId() != 0) {
			Optional<FishLanding> landing  = fishLandingService.findById(fishLanding.getId());
			String image_server = landing.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				fishLanding.setImage("/images/" + image_server);
				newUniqueKeyforMap = fishLanding.getUniqueKey();
				fishLandingService.saveFishLanding(ResourceData.getFishLanding(fishLanding, user, "", UPDATE));
			}else {
				newUniqueKeyforMap = fishLanding.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", ""); 
				fishLanding.setImage("/images/" + imageName);
				Base64Converter.convertToImage(fishLanding.getImage_src(), fishLanding.getImage());
				fishLandingService.saveFishLanding(ResourceData.getFishLanding(fishLanding, user, "", UPDATE));
			}			
			save = true;

		} else {			
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", "");
			fishLanding.setImage("/images/" + imageName);
			Base64Converter.convertToImage(fishLanding.getImage_src(), fishLanding.getImage());
			fishLandingService.saveFishLanding(ResourceData.getFishLanding(fishLanding, user, newUniqueKey, SAVE));			
			save = true;

		}

	}
	@RequestMapping(value = "/fishlandingsList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getfishlandingsList()
			throws JsonGenerationException, JsonMappingException, IOException {

		//user = getUserInfo();
		List<FishLanding> list = null;

		list = fishLandingService.findByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}
	
	@RequestMapping(value = "/getFishLanding/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishLandingById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		// logger.info(uniqueId);

		List<FishLanding> mapsDatas = fishLandingService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}
	
	@RequestMapping(value = "/getLastPageFL", method = RequestMethod.GET)
	public @ResponseBody String getLastPageFL(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = fishLandingService.FishLandingCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}


	
}
