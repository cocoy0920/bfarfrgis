package ph.gov.da.bfar.frgis.web.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.FishCoral;
import ph.gov.da.bfar.frgis.model.FishLanding;
import ph.gov.da.bfar.frgis.model.FishPen;
import ph.gov.da.bfar.frgis.model.FishPond;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.Fishport;
import ph.gov.da.bfar.frgis.model.Hatchery;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;
import ph.gov.da.bfar.frgis.model.LGU;
import ph.gov.da.bfar.frgis.model.Lambaklad;
import ph.gov.da.bfar.frgis.model.Mangrove;
import ph.gov.da.bfar.frgis.model.MaricultureZone;
import ph.gov.da.bfar.frgis.model.Market;
import ph.gov.da.bfar.frgis.model.NationalCenter;
import ph.gov.da.bfar.frgis.model.Payao;
import ph.gov.da.bfar.frgis.model.RegionalOffice;
import ph.gov.da.bfar.frgis.model.Resources;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;
import ph.gov.da.bfar.frgis.model.SeaGrass;
import ph.gov.da.bfar.frgis.model.Seaweeds;
import ph.gov.da.bfar.frgis.model.TOS;
import ph.gov.da.bfar.frgis.model.TrainingCenter;
import ph.gov.da.bfar.frgis.model.User;

public class ResourceData {
	
	 private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
	 private static LocalDateTime now = LocalDateTime.now();
	 
	 private static final String UPDATE = "update";
	 private static final String SAVE = "save";
	 
		public static Resources getResources(Resources fishSanctuaries,User user,String uniqueKey, String method) {
			
			if(method.equals(UPDATE)) {
				fishSanctuaries.setImage_src(fishSanctuaries.getImage_src());
				fishSanctuaries.setDateEdited(dtf.format(now));
				fishSanctuaries.setEditedBy(user.getName() + " " + user.getLastName());
			
			}else if(method.equals(SAVE)) {
				fishSanctuaries.setImage_src(fishSanctuaries.getImage_src());
				fishSanctuaries.setRegion(user.getRegion());
				fishSanctuaries.setUniqueKey(uniqueKey);
				fishSanctuaries.setDateEncoded(dtf.format(now));
				fishSanctuaries.setUser(user.getUsername());
				fishSanctuaries.setDateEdited("n/a");
				fishSanctuaries.setEncodedBy(user.getName() + " " + user.getLastName());
				fishSanctuaries.setEditedBy("n/a");
			}
			
			return fishSanctuaries;
		}
		
	
	public static FishSanctuaries getFishSanctuaries(FishSanctuaries fishSanctuaries,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//fishSanctuaries.setImage_src("/images/" +fishSanctuaries.getImage());
			fishSanctuaries.setDateEdited(dtf.format(now));
			fishSanctuaries.setEditedBy(user.getName() + " " + user.getLastName());
		
			if(fishSanctuaries.getBfardenr().equalsIgnoreCase("BFAR")) {
				
				fishSanctuaries.setDateEstablished("N/A");
				fishSanctuaries.setOrdinanceNo("N/A");
				fishSanctuaries.setOrdinanceTitle("N/A");
				fishSanctuaries.setNameOfSensitiveHabitatMPA("N/A");
				
			}else if(fishSanctuaries.getBfardenr().equalsIgnoreCase("DENR")) {
				
				fishSanctuaries.setSheltered("N/A");
			}
		}else if(method.equals(SAVE)) {
			//fishSanctuaries.setImage_src("/images/" +fishSanctuaries.getImage());
			fishSanctuaries.setRegion(user.getRegion());
			fishSanctuaries.setUniqueKey(uniqueKey);
			fishSanctuaries.setDateEncoded(dtf.format(now));
			fishSanctuaries.setUser(user.getUsername());
			fishSanctuaries.setDateEdited("n/a");
			fishSanctuaries.setEncodedBy(user.getName() + " " + user.getLastName());
			fishSanctuaries.setEditedBy("n/a");
			if(fishSanctuaries.getBfardenr().equalsIgnoreCase("BFAR")) {
				
				fishSanctuaries.setDateEstablished("N/A");
				fishSanctuaries.setOrdinanceNo("N/A");
				fishSanctuaries.setOrdinanceTitle("N/A");
				fishSanctuaries.setNameOfSensitiveHabitatMPA("N/A");
				
			}else if(fishSanctuaries.getBfardenr().equalsIgnoreCase("DENR")) {
				
				fishSanctuaries.setSheltered("N/A");
			}
		}
		
		return fishSanctuaries;
	}
	public static FishPond getFishPond(FishPond fishPond,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//fishSanctuaries.setImage_src("/images/" +fishSanctuaries.getImage());
			fishPond.setDateedited(dtf.format(now));
			fishPond.setEditedby(fishPond.getUser());
		
			
		}else if(method.equals(SAVE)) {
			//fishSanctuaries.setImage_src("/images/" +fishSanctuaries.getImage());
			fishPond.setRegion(fishPond.getRegion());
			fishPond.setUniqueKey(uniqueKey);
			fishPond.setDateencoded(dtf.format(now));
			fishPond.setUser(fishPond.getUser());
			fishPond.setDateedited("n/a");
			fishPond.setEncodedby(fishPond.getUser());
			fishPond.setEditedby("n/a");
			
		}
		
		return fishPond;
	}

	public static FishProcessingPlant getFishProcessingPlant(FishProcessingPlant fishProcessingPlant,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//fishProcessingPlant.setImage_src("/images/" +fishProcessingPlant.getImage());
			fishProcessingPlant.setDateEdited(dtf.format(now));
			fishProcessingPlant.setEditedBy(user.getName() + " " + user.getLastName());
			
			if(fishProcessingPlant.getProcessingEnvironmentClassification().equalsIgnoreCase("Backyard")) {
				
				fishProcessingPlant.setPlantRegistered("N/A");
			}
			
		}else if(method.equals(SAVE)) {
			//fishProcessingPlant.setImage_src("/images/" +fishProcessingPlant.getImage());
			fishProcessingPlant.setRegion(user.getRegion());
			 fishProcessingPlant.setUniqueKey(uniqueKey);
			 fishProcessingPlant.setDateEncoded(dtf.format(now));
			 fishProcessingPlant.setUser(user.getUsername());
			 fishProcessingPlant.setDateEdited("n/a");
			 fishProcessingPlant.setEncodedBy(user.getName() + " " + user.getLastName());
			 fishProcessingPlant.setEditedBy("n/a");
			 
			 if(fishProcessingPlant.getProcessingEnvironmentClassification().equalsIgnoreCase("Backyard")) {
					
					fishProcessingPlant.setPlantRegistered("N/A");
				}
		}
		
		return fishProcessingPlant;
	}
	
	public static FishLanding getFishLanding(FishLanding fishLanding,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//fishLanding.setImage_src("/images/" + fishLanding.getImage());
			fishLanding.setDateEdited(dtf.format(now));
			fishLanding.setEditedBy(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
			//fishLanding.setImage_src("/images/" + fishLanding.getImage());
			fishLanding.setRegion(user.getRegion());
			fishLanding.setUniqueKey(uniqueKey);
			fishLanding.setDateEncoded(dtf.format(now));
			fishLanding.setUser(user.getUsername());
			fishLanding.setDateEdited("n/a");
			fishLanding.setEncodedBy(user.getName() + " " + user.getLastName());
			fishLanding.setEditedBy("n/a");
		}
		
		return fishLanding;
	}

	public static FishPen getFishPen(FishPen fishPen,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//fishPen.setImage_src("/images/" + fishPen.getImage());
			fishPen.setDateEdited(dtf.format(now));
			fishPen.setEditedBy(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
			//fishPen.setImage_src("/images/" + fishPen.getImage());
			fishPen.setRegion(user.getRegion());
			fishPen.setUniqueKey(uniqueKey);
			fishPen.setDateEncoded(dtf.format(now));
			fishPen.setUser(user.getUsername());
			fishPen.setDateEdited("n/a");
			fishPen.setEncodedBy(user.getName() + " " + user.getLastName());
			fishPen.setEditedBy("n/a");
		}
		
		return fishPen;
	}

	public static FishCage getFishCage(FishCage fishCage,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//fishCage.setImage_src("/images/" + fishCage.getImage());
			fishCage.setDateEdited(dtf.format(now));
			fishCage.setEditedBy(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
			//fishCage.setImage_src("/images/" + fishCage.getImage());
			fishCage.setRegion(user.getRegion());
			fishCage.setUniqueKey(uniqueKey);
			fishCage.setDateEncoded(dtf.format(now));
			fishCage.setUser(user.getUsername());
			fishCage.setDateEdited("n/a");
			fishCage.setEncodedBy(user.getName() + " " + user.getLastName());
			fishCage.setEditedBy("n/a");
		}
		
		return fishCage;
	}
	public static Hatchery getHatchery(Hatchery hatchery,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//hatchery.setImage_src("/images/" + hatchery.getImage());
			hatchery.setDateEdited(dtf.format(now));
			hatchery.setEditedBy(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
			//hatchery.setImage_src("/images/" + hatchery.getImage());
			hatchery.setRegion(user.getRegion());
			hatchery.setUniqueKey(uniqueKey);
			hatchery.setDateEncoded(dtf.format(now));
			hatchery.setUser(user.getUsername());
			hatchery.setDateEdited("n/a");
			hatchery.setEncodedBy(user.getName() + " " + user.getLastName());
			hatchery.setEditedBy("n/a");
		}
		
		return hatchery;
	}
	public static IcePlantColdStorage getIcePlantColdStorage(IcePlantColdStorage coldStorage,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//coldStorage.setImage_src("/images/" +coldStorage.getImage());
			coldStorage.setDateEdited(dtf.format(now));
			coldStorage.setEditedBy(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
			//coldStorage.setImage_src("/images/" + coldStorage.getImage());
			coldStorage.setRegion(user.getRegion());
			coldStorage.setUniqueKey(uniqueKey);
			coldStorage.setDateEncoded(dtf.format(now));
			coldStorage.setUser(user.getUsername());
			coldStorage.setDateEdited("n/a");
			coldStorage.setEncodedBy(user.getName() + " " + user.getLastName());
			coldStorage.setEditedBy("n/a");
		}
		
		return coldStorage;
	}
	public static Fishport getFishport(Fishport fishport,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//fishport.setImage_src("/images/" + fishport.getImage());
			fishport.setDateEdited(dtf.format(now));
			fishport.setEditedBy(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
			//fishport.setImage_src("/images/" + fishport.getImage());
			fishport.setRegion(user.getRegion());
			fishport.setUniqueKey(uniqueKey);
			fishport.setDateEncoded(dtf.format(now));
			fishport.setUser(user.getUsername());
			fishport.setDateEdited("n/a");
			fishport.setEncodedBy(user.getName() + " " + user.getLastName());
			fishport.setEditedBy("n/a");
		}
		
		return fishport;
	}
	public static FishCoral getFishCoral(FishCoral fishCoral,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//fishCoral.setImage_src("/images/" + fishCoral.getImage());
			fishCoral.setDateEdited(dtf.format(now));
			fishCoral.setEditedBy(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
			//fishCoral.setImage_src("/images/" + fishCoral.getImage());
			fishCoral.setRegion(user.getRegion());
			fishCoral.setUniqueKey(uniqueKey);
			fishCoral.setDateEncoded(dtf.format(now));
			fishCoral.setUser(user.getUsername());
			fishCoral.setDateEdited("n/a");
			fishCoral.setEncodedBy(user.getName() + " " + user.getLastName());
			fishCoral.setEditedBy("n/a");
		}
		
		return fishCoral;
	}
//	public static MarineProtectedArea getMarineProtectedArea(MarineProtectedArea marineProtectedArea,User user,String uniqueKey, String method) {
//		
//		if(method.equals(UPDATE)) {
//			marineProtectedArea.setImage_src(marineProtectedArea.getImage_src());
//			marineProtectedArea.setDateEdited(dtf.format(now));
//			marineProtectedArea.setEditedBy(user.getName() + " " + user.getLastName());
//		}else if(method.equals(SAVE)) {
//			marineProtectedArea.setImage_src(marineProtectedArea.getImage_src());
//			marineProtectedArea.setRegion(user.getRegion());
//			marineProtectedArea.setUniqueKey(uniqueKey);
//			marineProtectedArea.setDateEncoded(dtf.format(now));
//			marineProtectedArea.setUser(user.getUsername());
//			marineProtectedArea.setDateEdited("n/a");
//			marineProtectedArea.setEncodedBy(user.getName() + " " + user.getLastName());
//			marineProtectedArea.setEditedBy("n/a");
//		}
//		
//		return marineProtectedArea;
//	}
	public static Market getMarket(Market market,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
		//	market.setImage_src("/images/" + market.getImage());
			market.setDateEdited(dtf.format(now));
			market.setEditedBy(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
		//	market.setImage_src("/images/" + market.getImage());
			market.setRegion(user.getRegion());
			market.setUniqueKey(uniqueKey);
			market.setDateEncoded(dtf.format(now));
			market.setUser(user.getUsername());
			market.setDateEdited("n/a");
			market.setEncodedBy(user.getName() + " " + user.getLastName());
			market.setEditedBy("n/a");
		}
		
		return market;
	}
	public static SchoolOfFisheries getSchoolOfFisheries(SchoolOfFisheries schoolOfFisheries,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//schoolOfFisheries.setImage_src("/images/" + schoolOfFisheries.getImage());
			schoolOfFisheries.setDateEdited(dtf.format(now));
			schoolOfFisheries.setEditedBy(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
			//schoolOfFisheries.setImage_src("/images/" + schoolOfFisheries.getImage());
			schoolOfFisheries.setRegion(user.getRegion());
			schoolOfFisheries.setUniqueKey(uniqueKey);
			schoolOfFisheries.setDateEncoded(dtf.format(now));
			schoolOfFisheries.setUser(user.getUsername());
			schoolOfFisheries.setDateEdited("n/a");
			schoolOfFisheries.setEncodedBy(user.getName() + " " + user.getLastName());
			schoolOfFisheries.setEditedBy("n/a");
		}
		
		return schoolOfFisheries;
	}
	public static SeaGrass getSeaGrass(SeaGrass seaGrass,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//seaGrass.setImage_src("/images/" + seaGrass.getImage());
			seaGrass.setDateEdited(dtf.format(now));
			seaGrass.setEditedBy(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
			//seaGrass.setImage_src("/images/" + seaGrass.getImage());
			seaGrass.setRegion(user.getRegion());
			seaGrass.setUniqueKey(uniqueKey);
			seaGrass.setDateEncoded(dtf.format(now));
			seaGrass.setUser(user.getUsername());
			seaGrass.setDateEdited("n/a");
			seaGrass.setEncodedBy(user.getName() + " " + user.getLastName());
			seaGrass.setEditedBy("n/a");
		}
		
		return seaGrass;
	}
	public static NationalCenter getNationalCenter(NationalCenter nationalCenter,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//seaGrass.setImage_src("/images/" + seaGrass.getImage());
			nationalCenter.setDate_edited(dtf.format(now));
			nationalCenter.setEdited_by(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
			//seaGrass.setImage_src("/images/" + seaGrass.getImage());
			nationalCenter.setRegion(user.getRegion());
			nationalCenter.setUnique_key(uniqueKey);
			nationalCenter.setDate_encoded(dtf.format(now));
			nationalCenter.setUser(user.getUsername());
			nationalCenter.setDate_edited("n/a");
			nationalCenter.setEncode_by(user.getName() + " " + user.getLastName());
			nationalCenter.setEdited_by("n/a");
		}
		
		return nationalCenter;
	}
	public static RegionalOffice getRegionalOffice(RegionalOffice regionalOffice,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//seaGrass.setImage_src("/images/" + seaGrass.getImage());
			regionalOffice.setDate_edited(dtf.format(now));
			regionalOffice.setEdited_by(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
			//seaGrass.setImage_src("/images/" + seaGrass.getImage());
			regionalOffice.setRegion(user.getRegion());
			regionalOffice.setUnique_key(uniqueKey);
			regionalOffice.setDate_encoded(dtf.format(now));
			regionalOffice.setUser(user.getUsername());
			regionalOffice.setDate_edited("n/a");
			regionalOffice.setEncode_by(user.getName() + " " + user.getLastName());
			regionalOffice.setEdited_by("n/a");
		}
		
		return regionalOffice;
	}
	public static Seaweeds getSeaweeds(Seaweeds seaweeds,User user,String uniqueKey, String method) {
			
			if(method.equals(UPDATE)) {
				//seaweeds.setImage_src("/images/" + seaweeds.getImage());
				seaweeds.setDateEdited(dtf.format(now));
				seaweeds.setEditedBy(user.getName() + " " + user.getLastName());
			}else if(method.equals(SAVE)) {
				//seaweeds.setImage_src("/images/" + seaweeds.getImage());
				seaweeds.setRegion(user.getRegion());
				seaweeds.setUniqueKey(uniqueKey);
				seaweeds.setDateEncoded(dtf.format(now));
				seaweeds.setUser(user.getUsername());
				seaweeds.setDateEdited("n/a");
				seaweeds.setEncodedBy(user.getName() + " " + user.getLastName());
				seaweeds.setEditedBy("n/a");
			}
			
			return seaweeds;
		}
	public static Mangrove getMangrove(Mangrove mangrove,User user,String uniqueKey, String method) {
			
			if(method.equals(UPDATE)) {
				//mangrove.setImage_src("/images/" + mangrove.getImage());
				mangrove.setDateEdited(dtf.format(now));
				mangrove.setEditedBy(user.getName() + " " + user.getLastName());
			}else if(method.equals(SAVE)) {
				//mangrove.setImage_src("/images/" + mangrove.getImage());
				mangrove.setRegion(user.getRegion());
				mangrove.setUniqueKey(uniqueKey);
				mangrove.setDateEncoded(dtf.format(now));
				mangrove.setUser(user.getUsername());
				mangrove.setDateEdited("n/a");
				mangrove.setEncodedBy(user.getName() + " " + user.getLastName());
				mangrove.setEditedBy("n/a");
			}
			
			return mangrove;
		}
	public static LGU getLGU(LGU lgu,User user,String uniqueKey, String method) {
			
			if(method.equals(UPDATE)) {
				//lgu.setImage_src("/images/" + lgu.getImage());
				lgu.setDateEdited(dtf.format(now));
				lgu.setEditedBy(user.getName() + " " + user.getLastName());
			}else if(method.equals(SAVE)) {
				//lgu.setImage_src("/images/" + lgu.getImage());
				lgu.setRegion(user.getRegion());
				lgu.setUniqueKey(uniqueKey);
				lgu.setDateEncoded(dtf.format(now));
				lgu.setUser(user.getUsername());
				lgu.setDateEdited("n/a");
				lgu.setEncodedBy(user.getName() + " " + user.getLastName());
				lgu.setEditedBy("n/a");
			}
			
			return lgu;
		}	
	public static TrainingCenter getTrainingCenter(TrainingCenter trainingCenter,User user,String uniqueKey, String method) {
			
			if(method.equals(UPDATE)) {
				//trainingCenter.setImage_src("/images/" + trainingCenter.getImage());
				trainingCenter.setDateEdited(dtf.format(now));
				trainingCenter.setEditedBy(user.getName() + " " + user.getLastName());
			}else if(method.equals(SAVE)) {
				//trainingCenter.setImage_src("/images/" + trainingCenter.getImage());
				trainingCenter.setRegion(user.getRegion());
				trainingCenter.setUniqueKey(uniqueKey);
				trainingCenter.setDateEncoded(dtf.format(now));
				trainingCenter.setUser(user.getUsername());
				trainingCenter.setDateEdited("n/a");
				trainingCenter.setEncodedBy(user.getName() + " " + user.getLastName());
				trainingCenter.setEditedBy("n/a");
			}
			
			return trainingCenter;
		}	
	public static MaricultureZone getMaricultureZone(MaricultureZone maricultureZone,User user,String uniqueKey, String method) {
			
			if(method.equals(UPDATE)) {
				//maricultureZone.setImage_src("/images/" + maricultureZone.getImage());
				maricultureZone.setDateEdited(dtf.format(now));
				maricultureZone.setEditedBy(user.getName() + " " + user.getLastName());
			}else if(method.equals(SAVE)) {
				//maricultureZone.setImage_src("/images/" + maricultureZone.getImage());
				maricultureZone.setRegion(user.getRegion());
				maricultureZone.setUniqueKey(uniqueKey);
				maricultureZone.setDateEncoded(dtf.format(now));
				maricultureZone.setUser(user.getUsername());
				maricultureZone.setDateEdited("n/a");
				maricultureZone.setEncodedBy(user.getName() + " " + user.getLastName());
				maricultureZone.setEditedBy("n/a");
			}
			
			return maricultureZone;
		}	
	public static TOS getTOS(TOS tos,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//seaGrass.setImage_src("/images/" + seaGrass.getImage());
			tos.setDate_edited(dtf.format(now));
			tos.setEdited_by(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
			//seaGrass.setImage_src("/images/" + seaGrass.getImage());
			tos.setRegion(user.getRegion());
			tos.setUnique_key(uniqueKey);
			tos.setDate_encoded(dtf.format(now));
			tos.setUser(user.getUsername());
			tos.setDate_edited("n/a");
			tos.setEncode_by(user.getName() + " " + user.getLastName());
			tos.setEdited_by("n/a");
		}
		
		return tos;
	}	
	
	public static Payao getPayao(Payao payao,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//seaGrass.setImage_src("/images/" + seaGrass.getImage());
			payao.setDate_edited(dtf.format(now));
			payao.setEdited_by(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
			//seaGrass.setImage_src("/images/" + seaGrass.getImage());
			payao.setRegion(user.getRegion());
			payao.setUnique_key(uniqueKey);
			payao.setDate_encoded(dtf.format(now));
			payao.setUser(user.getUsername());
			payao.setDate_edited("n/a");
			payao.setEncode_by(user.getName() + " " + user.getLastName());
			payao.setEdited_by("n/a");
		}
		
		return payao;
	}
	
	
	public static Lambaklad getLambaklad(Lambaklad lambaklad,User user,String uniqueKey, String method) {
		
		if(method.equals(UPDATE)) {
			//seaGrass.setImage_src("/images/" + seaGrass.getImage());
			lambaklad.setDateEdited(dtf.format(now));
			lambaklad.setEditedBy(user.getName() + " " + user.getLastName());
		}else if(method.equals(SAVE)) {
			//seaGrass.setImage_src("/images/" + seaGrass.getImage());
			lambaklad.setRegion(user.getRegion());
			lambaklad.setUniqueKey(uniqueKey);
			lambaklad.setDateEncoded(dtf.format(now));
			lambaklad.setUser(user.getUsername());
			lambaklad.setDateEdited("n/a");
			lambaklad.setEncodedBy(user.getName() + " " + user.getLastName());
			lambaklad.setEditedBy("n/a");
		}
		
		return lambaklad;
	}
		public static String getRegionMap(String region) {
			String region_map="";
			
			if(region.equals("010000000")) {
				region_map ="\"Ilocos\""; 
			}else if(region.equals("020000000")) {
				region_map ="\"CagayanValley\""; 
			}else if(region.equals("030000000")) {
				region_map ="\"CentralLuzon\""; 
			}else if(region.equals("040000000")) {
				region_map ="\"CALABARZON\""; 
			}else if(region.equals("050000000")) {
				region_map ="\"Bicol\""; 
			}else if(region.equals("060000000")) {
				region_map ="\"Western\""; 
			}else if(region.equals("070000000")) {
				region_map ="\"Central\""; 
			}else if(region.equals("080000000")) {
				region_map ="\"Eastern\""; 
			}else if(region.equals("090000000")) {
				region_map ="\"ZamboangaPeninsula\""; 
			}else if(region.equals("100000000")) {
				region_map ="\"NorthernMindanao\""; 
			}else if(region.equals("110000000")) {
				region_map ="\"DavaoRegion\""; 
			}else if(region.equals("120000000")) {
				region_map ="\"SOCCSKSARGEN\""; 
			}else if(region.equals("130000000")) {
				region_map ="\"NCR\""; 
			}else if(region.equals("140000000")) {
				region_map ="\"CAR\""; 
			}else if(region.equals("150000000")) {
				region_map ="\"BARRM\""; 
			}else if(region.equals("160000000")) {
				region_map ="\"Caraga\""; 
			}else if(region.equals("170000000")) {
				region_map ="\"MIMAROPA\""; 
			}else if(region.equals("180000000")) {
				region_map ="\"Philippine_Map_Bathymetry\""; 
			}else {
				region_map ="\"Philippine_Map_Bathymetry\""; 
			}
			
			
			
			return region_map;
			
		}
		
}
