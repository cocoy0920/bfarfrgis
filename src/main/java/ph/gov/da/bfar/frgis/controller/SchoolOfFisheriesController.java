package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.Page.SchoolPage;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.model.ResourcesPaginations;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.service.MapsService;
import ph.gov.da.bfar.frgis.service.SchoolOfFisheriesService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.Base64Converter;
import ph.gov.da.bfar.frgis.web.util.Base64ImageUtil;
import ph.gov.da.bfar.frgis.web.util.MapBuilder;
import ph.gov.da.bfar.frgis.web.util.ResourceData;

@Controller
@RequestMapping("/create")
public class SchoolOfFisheriesController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	SchoolOfFisheriesService schoolOfFisheriesService;
	
	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	MapsService mapsService;
	
	private static final String UPDATE = "update";
	private static final String SAVE = "save";
	private String newUniqueKeyforMap="";
	private String map_data="";
	User user;	
	
	@RequestMapping(value = "/schooloffisheries", method = RequestMethod.GET)
	public ModelAndView schooloffisheries(ModelMap model)
			throws Exception {

		ModelAndView result = new ModelAndView();
		map_data="";
		user   = new User();
		
		user = authenticationFacade.getUserInfo();

		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "school");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		
		result = new ModelAndView("users/form");
		return result;
	}
	@RequestMapping(value = "/schooloffishery", method = RequestMethod.GET)
	public ModelAndView schooloffishery(ModelMap model)
			throws Exception {

		ModelAndView result = new ModelAndView();
		user   = new User();
		
		user = authenticationFacade.getUserInfo();
		model.addAttribute("mapsData", map_data);
		model.addAttribute("user", user);
		model.addAttribute("page", "school");
		model.addAttribute("username", user.getUsername());
		model.addAttribute("region", user.getRegion());
		
		result = new ModelAndView("users/form");
		return result;
	}

	@RequestMapping(value = "/schoolMap/{uniqueKey}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("uniqueKey") String uniqueKey,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;
		 
		 ObjectMapper mapper = new ObjectMapper();
			SchoolOfFisheries data = schoolOfFisheriesService.findByUniqueKey(uniqueKey);
		 	List<MapsData> mapsDatas = new ArrayList<>();
		 	
		 	Features features = new Features();
			 features = MapBuilder.getSchoolOfFisheriesMapData(data);
			 
		 	MapsData  mapsData = new MapsData();
		 	
		 	mapsData.setLon(data.getLon());
		 	mapsData.setLat(data.getLat());
		 	mapsData.setUniqueKey(data.getUniqueKey());
		 	mapsData.setIcon("/static/images/pin2022/FisheriesSchool.png");

		 	mapsData.setHtml(features.getInformation());
		 	mapsDatas.add(mapsData);
			String jsonString = mapper.writeValueAsString(mapsDatas);
			
			map_data = jsonString;
		
		result = new ModelAndView("redirect:../schooloffishery");
		
	
		return result;
	}
	@RequestMapping(value = "/schoolDeleteById/{id}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("id") int id,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;

		//schoolOfFisheriesService.deleteSchoolOfFisheriesById(id);
		schoolOfFisheriesService.updateByEnabled(id);
		result = new ModelAndView("redirect:../schooloffishery");
		
	
		return result;
	}
	@RequestMapping(value = "/schoolDeleteById/{id}/{reason}", method = RequestMethod.GET)
	public ModelAndView fishCageMapByUniqueKey(@PathVariable("id") int id,@PathVariable("reason") String reason,ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		 ModelAndView result;

		//schoolOfFisheriesService.deleteSchoolOfFisheriesById(id);
		schoolOfFisheriesService.updateByEnabled(id);
		schoolOfFisheriesService.updateSchoolOfFisheriesByReason(id, reason);
		result = new ModelAndView("redirect:../../schooloffishery");
		
	
		return result;
	}
	
//	@RequestMapping(value = "/schoolMap", method = RequestMethod.GET)
//	public ModelAndView fishCageMAP(ModelMap model)
//			throws JsonGenerationException, JsonMappingException, IOException {
//
//		 ModelAndView result;
//		 
//		 ObjectMapper mapper = new ObjectMapper();
//			List<MapsData> mapsDatas = mapsService.findAllMapsByUser(authenticationFacade.getUsername(), "schooloffisheries");
//			String jsonString = mapper.writeValueAsString(mapsDatas);
//
//			model.addAttribute("set_center", "set_center: [8.678300, 125.800369]");
//			//model.addAttribute("page", "home_map");
//			model.addAttribute("mapsData", jsonString);
//		
//		result = new ModelAndView("users/map/schoolmap");
//		
//	
//		return result;
//	}

	@RequestMapping(value = "/schooloffisheries/{page_id}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSchooloffisheries(@PathVariable("page_id") int page_id)
			throws JsonGenerationException, JsonMappingException, IOException {

		Page<SchoolPage> page = null;
	      Pageable pageable = PageRequest.of(page_id, 5, Sort.by("id"));
	      
	          page = schoolOfFisheriesService.findByUsername(authenticationFacade.getUsername(),pageable);
	          int number = page.getNumber();
	          int numberOfElements = page.getNumberOfElements();
	          int size = page.getSize();
	          long totalElements = page.getTotalElements();
	          int totalPages = page.getTotalPages();
	          System.out.printf("page info - page number %s, numberOfElements: %s, size: %s, "
	                          + "totalElements: %s, totalPages: %s%n",
	                  number, numberOfElements, size, totalElements, totalPages);
	          List<SchoolPage> SchoolOfFisheriesList = page.getContent();

	          pageable = page.nextPageable();

	          StringBuilder builder = new StringBuilder();
			
			builder.append("<ul class=\"pagination pagination-sm\">");
	
			for (int i = 0; i <= totalPages-1; i++) {
				int y = i + 1;
				builder.append("  <li class=\"page-item\"><a class=\"page-link\" data-target=" + i + ">" + y + "</a></li>");
			}
		builder.append("</ul>");
		String pageNumber = builder.toString();

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setSchoolOfFisheries(SchoolOfFisheriesList);
		resourcesPaginations.setPageNumber(pageNumber);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(resourcesPaginations);

		return jsonArray;

	}
	@RequestMapping(value = "/schoolList", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getschoolList() throws JsonGenerationException, JsonMappingException, IOException {

	//	user = getUserInfo();
		List<SchoolOfFisheries> list = null;

		list = schoolOfFisheriesService.ListByUsername(authenticationFacade.getUsername());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(list);

		return jsonArray;

	}

	@RequestMapping(value = "/getSchool/{uniqueId}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSchoolById(@PathVariable("uniqueId") String uniqueId)
			throws JsonGenerationException, JsonMappingException, IOException {
		//logger.info(uniqueId);

		List<SchoolOfFisheries> mapsDatas = schoolOfFisheriesService.findByUniqueKeyForUpdate(uniqueId);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(mapsDatas);

		return jsonArray;

	}

	@RequestMapping(value = "/saveSchool", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String postSchool(@RequestBody SchoolOfFisheries schoolOfFisheries) throws JsonGenerationException, JsonMappingException, IOException {


		savingResources(schoolOfFisheries);

		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();
		resourcesPaginations.setUniqueKey(newUniqueKeyforMap);

		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(resourcesPaginations);
	}


	private void savingResources(SchoolOfFisheries schoolOfFisheries) {

		User user = new User();
		user = authenticationFacade.getUserInfo();
		
		boolean save = false;
		UUID newID = UUID.randomUUID();

		String region = "";
		String province = "";
		String municipal = "";
		String barangay = "";
		String[] region_data = new String[0];
		String[] data = new String[0];
		String[] municipal_data = new String[0];
		String[] barangay_data = new String[0];

		region = user.getRegion_id() + "," + user.getRegion();
		region_data = region.split(",", 2);
		schoolOfFisheries.setRegion_id(region_data[0]);
		schoolOfFisheries.setRegion(region_data[1]);

		province = schoolOfFisheries.getProvince();
		data = province.split(",", 2);
		schoolOfFisheries.setProvince_id(data[0]);
		schoolOfFisheries.setProvince(data[1]);

		municipal = schoolOfFisheries.getMunicipality();
		municipal_data = municipal.split(",", 2);
		schoolOfFisheries.setMunicipality_id(municipal_data[0]);
		schoolOfFisheries.setMunicipality(municipal_data[1]);

		barangay = schoolOfFisheries.getBarangay();
		barangay_data = barangay.split(",", 3);
		schoolOfFisheries.setBarangay_id(barangay_data[0]);
		schoolOfFisheries.setBarangay(barangay_data[1]);
		schoolOfFisheries.setEnabled(true);

		String newUniqueKey = newID.toString();
		
		String imageNameSrc = Base64ImageUtil.removeExtension(schoolOfFisheries.getImage());
		imageNameSrc = imageNameSrc + ".jpeg";
		String imageName = imageNameSrc.replaceAll("/images/","");
		
		if (schoolOfFisheries.getId() != 0) {
			Optional<SchoolOfFisheries> school_image = schoolOfFisheriesService.findById(schoolOfFisheries.getId());
			String image_server = school_image.get().getImage().replaceAll("/images/", "");
			
			if(image_server.equalsIgnoreCase(imageName)) {
				newUniqueKeyforMap = schoolOfFisheries.getUniqueKey();
				schoolOfFisheries.setImage("/images/" + image_server);
				schoolOfFisheriesService.saveSchoolOfFisheries(ResourceData.getSchoolOfFisheries(schoolOfFisheries, user, "", UPDATE));
			}else {
				newUniqueKeyforMap = schoolOfFisheries.getUniqueKey();
				imageName.replaceAll("[^a-zA-Z0-9]", "");  
				schoolOfFisheries.setImage("/images/" + imageName);
				Base64Converter.convertToImage(schoolOfFisheries.getImage_src(), schoolOfFisheries.getImage());
				schoolOfFisheriesService.saveSchoolOfFisheries(ResourceData.getSchoolOfFisheries(schoolOfFisheries, user, "", UPDATE));
			}
			save = true;

		} else {
			newUniqueKeyforMap = newUniqueKey;
			imageName.replaceAll("[^a-zA-Z0-9]", "");  
			schoolOfFisheries.setImage("/images/" + imageName);
			Base64Converter.convertToImage(schoolOfFisheries.getImage_src(), schoolOfFisheries.getImage());
			schoolOfFisheriesService.saveSchoolOfFisheries(ResourceData.getSchoolOfFisheries(schoolOfFisheries, user, newUniqueKey, SAVE));
			save = true;

		}

	}
	
	@RequestMapping(value = "/getLastPageSchool", method = RequestMethod.GET)
	public @ResponseBody String getLastPageSchool(ModelMap model)
			throws JsonGenerationException, JsonMappingException, IOException {

		int totalNumberOfRecords = 0;
		int numberOfRecordsPerPage = 20;

		totalNumberOfRecords = schoolOfFisheriesService.SchoolOfFisheriesCount(authenticationFacade.getUsername());

		int noOfPages = totalNumberOfRecords / numberOfRecordsPerPage;
		if (totalNumberOfRecords > (noOfPages * numberOfRecordsPerPage)) {
			noOfPages = noOfPages + 1;
		}
		ResourcesPaginations resourcesPaginations = new ResourcesPaginations();

		resourcesPaginations.setLastpageNumber(String.valueOf(noOfPages));

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		String jsonArray = gson.toJson(String.valueOf(noOfPages));

		return jsonArray;

	}


}
