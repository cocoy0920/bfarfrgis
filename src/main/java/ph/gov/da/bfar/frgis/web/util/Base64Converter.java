package ph.gov.da.bfar.frgis.web.util;

import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;

public class Base64Converter {
	
	@Autowired
	static
	ServletContext context; 
	

	public static void convertToImage(String imageSource,String imageName) {
		try {

					//String imageName = fishCage.getImage() + "_"+ java.time.LocalDate.now();
	                  String imageNameSrc = Base64ImageUtil.removeExtension(imageName);
	                  String imgType = Base64ImageUtil.getImageType(imageSource);
	                  String imageString = Base64ImageUtil.removeBase64Header(imageSource);
	                  BufferedImage image = Base64ImageUtil.decodeToImage(imageString);
	              //    File imageFile = new File ("/static/images/svg/" + imageNameSrc+ ".jpeg");
	              //    System.out.println("imgType" + imgType);
	              
	           //  File imageFile = new File ("/Users/kiko/FVEJERANO/Documents/images/" + imageNameSrc + ".jpeg"); 
	             File imageFile = new File ("/mnt/frgis_photos/"  + imageNameSrc+"." + "jpeg" ); 
	            //      File imageFile = new File ("/mnt/frgis_photos/"  + imageName ); 
	            //  File imageFile = new File ("static/photos/"  + imageNameSrc+"." + "jpeg" ); 

		             
	               if(imageFile.isDirectory()) {
	            	   System.out.println("/mnt/frgis_photos/");
	               }else {
	            	   System.out.println("/mnt/frgis_photos/" + "is not a directory");
	               }
	                imageFile.mkdir();
	             //    File imageFile = new File ("/opt/tomcat/webapps/frgis_captured_images/" + imageNameSrc+"." + "jpeg" ); 
	                 
	                ImageIO.write(image,imgType,imageFile);    
	                
	             //   try (FileOutputStream fosFor = new FileOutputStream(imageFile)) {
	            //        fosFor.write(imageByte);
	             //   }
   
	           } catch (Exception e) {
	              //logger.info("ERROR SAVING IMAGE");
	           }
	}
}
