package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.model.MapsData;
import ph.gov.da.bfar.frgis.repository.MapsDataRepo;



@Service("mapsService")
@Transactional
public class MapsServiceImpl implements MapsService{

	@Autowired
	private MapsDataRepo dao;

	//@Autowired
   // private PasswordEncoder passwordEncoder;
	
	public Optional<MapsData> findById(int id) {
		return dao.findById(id);
	}


	public void saveMapsData(MapsData mapsData) {
		//user.setPassword(passwordEncoder.encode(user.getPassword()));
		dao.save(mapsData);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate update explicitly.
	 * Just fetch the entity from db and update it with proper values within transaction.
	 * It will be updated in db once transaction ends. 
	 */


	public List<MapsData> findAllMapsData() {
		return dao.findAll();
	}


	@Override
	public List<MapsData> findAllMapsByUser(String user,String page) {
		
		return dao.findAllMapsByUser(user,page);
	}
	
	@Override
	public List<MapsData> findAllMapsByUser(String user) {
		
		return dao.findAllMapsByUser(user);
	}
	@Override
	public List<MapsData> findAllMapsByResources(String page) {
		
		return dao.findAllMapsByResources(page);
	}


	@Override
	public List<MapsData> findByUniqueKey(String uniqueKey) {

		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<MapsData> findAllMapsByRegion(String region_id) {
		// TODO Auto-generated method stub
		return dao.findAllMapsByRegion(region_id);
	}


	@Override
	public List<MapsData> findAllMapsByRegionAndResources(String region, String page) {

		return dao.findAllMapsByRegionAndResources(region, page);
	}


	@Override
	public List<MapsData> findallMapsOfHatcheryByLegislated(String page,String legislated) {
		return dao.findallMapsOfHatcheryByLegislated(page, legislated);
	}


	@Override
	public List<MapsData> findallMapsOfHatcheryByLegislated(String user, String page, String legislated) {
		return dao.findallMapsOfHatcheryByLegislated(user, page, legislated);
	}

/*
	@Override
	public List<MapsData> findAllMapsByRegionAndProvince(String region, String province) {
		
		return dao.findAllMapsByRegionAndProvince(region, province);
	}


	@Override
	public List<MapsData> findAllMapsByRegionAndProvinceAndResources(String region, String province,String resources) {
		
		return dao.findAllMapsByRegionAndProvinceAndResources(region, province,resources);
	}


	@Override
	public List<MapsData> findAllMapsByRegionAndProvinceAndMunicipality(String region, String province,
			String municipality) {
		
		return dao.findAllMapsByRegionAndProvinceAndMunicipality(region, province, municipality);
	}


	@Override
	public List<MapsData> findAllMapsByRegionAndProvinceAndMunicipalityAndResources(String region, String province,
			String municipality, String resources) {
		
		return dao.findAllMapsByRegionAndProvinceAndMunicipalityAndResources(region, province, municipality, resources);
	}


	*/
}
