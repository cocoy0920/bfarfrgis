package ph.gov.da.bfar.frgis.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.repository.RoleRepository;
import ph.gov.da.bfar.frgis.repository.UserRepository;

@Service
public class UserServiceImp implements UserService {
	
	@Autowired
	BCryptPasswordEncoder encoder;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	UserRepository userRepository;

	@Override
	public void saveUser(User user) {
		user.setPassword(encoder.encode(user.getPass()));
		//user.sets("VERIFIED");
		//Role userRole = roleRepository.findByRole("SITE_USER");
		//user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		userRepository.save(user);
		
	}

	@Override
	public boolean isUserAlreadyPresent(String username) {

		return userRepository.isUserAlreadyPresent(username);
	}

	@Override
	public User findByUsername(String username) {
		User user = userRepository.getUserByUsername(username);
		
		return user;
	}

	@Override
	public List<User> findAllUsers() {
		
		return userRepository.findAll();
	}

	@Override
	public List<User> ListfindByUsername(String username) {
		return userRepository.ListfindByUsername(username);
	}

}
