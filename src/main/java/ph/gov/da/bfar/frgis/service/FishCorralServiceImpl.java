package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.FishCoralPage;
import ph.gov.da.bfar.frgis.model.FishCoral;
import ph.gov.da.bfar.frgis.repository.FishCoralRepo;




@Service("FishCorralService")
@Transactional
public class FishCorralServiceImpl implements FishCorralService{

	@Autowired
	private FishCoralRepo dao;

	
	public Optional<FishCoral> findById(int id) {
		return dao.findById(id);
	}

	public List<FishCoral> ListByUsername(String username) {
		List<FishCoral>FishCoral = dao.getListFishCoralByUsername(username);
		return FishCoral;
	}

	public void saveFishCoral(FishCoral FishCoral) {
		dao.save(FishCoral);
	}
	
	@Override
	public void deleteFishCoral(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public void UpdateForEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void UpdateForEnabled(int id, String reason) {
		dao.updateFishCoralByReason(id, reason);
		
	}

	public List<FishCoral> findAllFishCorals() {
		return dao.findAll();
	}

	@Override
	public List<FishCoral> getAllFishCorals(int index, int pagesize, String user) {
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<FishCoral> page = dao.getPageableFishCorals(user,secondPageWithFiveElements);
				 
		return page;
	}

	@Override
	public int FishCoralCount(String username) {
		return dao.FishCoralCount(username);
	}

	@Override
	public FishCoral findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<FishCoral> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<FishCoralPage> findByUsername(String user, Pageable pageable) {

		return dao.findByUsername(user, pageable);
	}
	
	@Override
	public Page<FishCoralPage> findByRegion(String region, Pageable pageable) {

		return dao.findByRegion(region, pageable);
	}
	@Override
	public Page<FishCoralPage> findAllFishCoral(Pageable pageable) {
	
		return dao.findAllFishCoral(pageable);
	}

	@Override
	public List<FishCoral> ListByRegion(String region_id) {

		return dao.getListFishCoralByRegion(region_id);
	}

	@Override
	public Long countFishCoralPerRegion(String region_id) {

		return dao.countFishCoralPerRegion(region_id);
	}

	@Override
	public Long countFishCoralPerProvince(String province_id) {
	
		return dao.countFishCoralPerProvince(province_id);
	}

	@Override
	public Long countAllFishCoral() {
		
		return dao.countAllFishCoral();
	}

	@Override
	public Long countFishCoralPerMunicipality(String municipality_id) {

		return dao.countFishCoralPerMunicipality(municipality_id);
	}
}
