package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.model.Municipalities;


@Repository
public interface MunicipalitiesRepo extends JpaRepository<Municipalities, Integer> {

	@Query("SELECT m from Municipalities m where m.province_id = :province_id")
	public List<Municipalities> findByProvince(String province_id);
}
