package ph.gov.da.bfar.frgis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import ph.gov.da.bfar.frgis.IAuthenticationFacade;
import ph.gov.da.bfar.frgis.model.Chart;
import ph.gov.da.bfar.frgis.model.Features;
import ph.gov.da.bfar.frgis.model.FishCage;
import ph.gov.da.bfar.frgis.model.FishCoral;
import ph.gov.da.bfar.frgis.model.FishLanding;
import ph.gov.da.bfar.frgis.model.FishPen;
import ph.gov.da.bfar.frgis.model.FishProcessingPlant;
import ph.gov.da.bfar.frgis.model.FishSanctuaries;
import ph.gov.da.bfar.frgis.model.Fishport;
import ph.gov.da.bfar.frgis.model.IcePlantColdStorage;
import ph.gov.da.bfar.frgis.model.LGU;
import ph.gov.da.bfar.frgis.model.Mangrove;
import ph.gov.da.bfar.frgis.model.MaricultureZone;
import ph.gov.da.bfar.frgis.model.Market;
import ph.gov.da.bfar.frgis.model.Municipalities;
import ph.gov.da.bfar.frgis.model.Provinces;
import ph.gov.da.bfar.frgis.model.SchoolOfFisheries;
import ph.gov.da.bfar.frgis.model.SeaGrass;
import ph.gov.da.bfar.frgis.model.Seaweeds;
import ph.gov.da.bfar.frgis.model.TrainingCenter;
import ph.gov.da.bfar.frgis.model.User;
import ph.gov.da.bfar.frgis.model.UserBean;
import ph.gov.da.bfar.frgis.model.UserProfile;
import ph.gov.da.bfar.frgis.model.UserProfileType;
import ph.gov.da.bfar.frgis.service.FishCageService;
import ph.gov.da.bfar.frgis.service.FishCorralService;
import ph.gov.da.bfar.frgis.service.FishLandingService;
import ph.gov.da.bfar.frgis.service.FishPenService;
import ph.gov.da.bfar.frgis.service.FishPortService;
import ph.gov.da.bfar.frgis.service.FishProcessingPlantService;
import ph.gov.da.bfar.frgis.service.FishSanctuariesService;
import ph.gov.da.bfar.frgis.service.HatcheryService;
import ph.gov.da.bfar.frgis.service.IcePlantColdStorageService;
import ph.gov.da.bfar.frgis.service.LGUService;
import ph.gov.da.bfar.frgis.service.MangroveService;
import ph.gov.da.bfar.frgis.service.MaricultureZoneService;
import ph.gov.da.bfar.frgis.service.MarketService;
import ph.gov.da.bfar.frgis.service.MunicipalityService;
import ph.gov.da.bfar.frgis.service.ProvinceService;
import ph.gov.da.bfar.frgis.service.RegionsService;
import ph.gov.da.bfar.frgis.service.RoleService;
import ph.gov.da.bfar.frgis.service.SchoolOfFisheriesService;
import ph.gov.da.bfar.frgis.service.SeaGrassService;
import ph.gov.da.bfar.frgis.service.SeaweedsService;
import ph.gov.da.bfar.frgis.service.TrainingCenterService;
import ph.gov.da.bfar.frgis.service.UserProfileService;
import ph.gov.da.bfar.frgis.service.UserService;
import ph.gov.da.bfar.frgis.web.util.ChartsInterface;
import ph.gov.da.bfar.frgis.web.util.FeaturesData;

@Controller
@RequestMapping("/region")
public class RegionController {
	
	@Autowired
	UserService userService;

	@Autowired
	RegionsService regionsService;
	
	@Autowired
	ProvinceService provinceService;
	
	@Autowired
	MunicipalityService municipalityService;
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	UserProfileService userProfileService;
	
	@Autowired
	FishSanctuariesService fishSanctuariesService;
	
	@Autowired
	FishProcessingPlantService fishProcessingPlantService;
	
	@Autowired
	FishLandingService fishLandingService;
	
	@Autowired
	FishPortService fishPortService;
	
	@Autowired
	FishPenService fishPenService;
	
	@Autowired
	FishCageService fishCageService;

	@Autowired
	IcePlantColdStorageService icePlantColdStorageService;
	
	@Autowired
	MarketService marketService;

	@Autowired
	SchoolOfFisheriesService schoolOfFisheriesService;
	
	@Autowired
	FishCorralService fishCorralService;
	
	@Autowired
	SeaGrassService seaGrassService;
	
	@Autowired
	SeaweedsService seaweedsService;
	
	@Autowired
	MangroveService mangroveService;
	
	@Autowired
	LGUService lGUService;
	
	@Autowired
	MaricultureZoneService zoneService;
	
	@Autowired
	TrainingCenterService trainingCenterService;
	
	@Autowired
	HatcheryService hatcheryService;
	
	
	@Autowired
	 private IAuthenticationFacade authenticationFacade;
	
	@Autowired
	ChartsInterface chartsUtil;
	
	private String province_Id;
	private String provinceName="";
	private String provinceID="";

	@RequestMapping(value = "home", method = RequestMethod.GET)
	public ModelAndView adminhome(ModelMap model) {
		ModelAndView modelAndView = new ModelAndView();
		
		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		
			
		List<Provinces> provinces = null;
		//List<Regions> regions = null;
		//regions = regionsService.findAllRegions();
		provinces = provinceService.findByRegion(user.getRegion_id());
				
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("provinces", provinces);
		model.addAttribute("regionName", "Ilocos");
		//model.addAttribute("chart_by_region", chartsUtil.getChartByRegion(authenticationFacade.getRegionID()));
		
		//model.addAttribute("regions", regions);
		modelAndView.setViewName("/region/region_admin"); // resources/template/admin.html
		return modelAndView;
	}
	@RequestMapping(value = "chart", method = RequestMethod.GET)
	public ModelAndView chart(ModelMap model) {
		ModelAndView modelAndView = new ModelAndView();
		

		modelAndView.setViewName("/region/chart"); // resources/template/admin.html
		return modelAndView;
	}
	@RequestMapping(value = "search", method = RequestMethod.GET)
	public ModelAndView search(ModelMap model) {
		ModelAndView modelAndView = new ModelAndView();
		
		
User user = new User();
		
		user  = userService.findByUsername(authenticationFacade.getUsername());
		
		
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		System.out.println("ROLE: " + setOfRole);
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		UserProfile role_account = new UserProfile();
		UserProfile role_account2 = new UserProfile();
		UserProfile role_account3 = new UserProfile();
		List<UserProfile> list2 = new ArrayList<UserProfile>();
		if(role.getType().equals("REGIONALADMIN")){
			role_account.setId(6);
			role_account.setType(UserProfileType.REGIONALENCODER.getUserProfileType());
			role_account2.setId(7);
			role_account2.setType(UserProfileType.PFOADMIN.getUserProfileType());
			role_account3.setId(8);
			role_account3.setType(UserProfileType.PFOENCODER.getUserProfileType());
			list2.add(role_account);
		}
		
		List<Provinces> provinces = null;
		
		provinces = provinceService.findByRegion(user.getRegion_id());
				

		model.addAttribute("provinces", provinces);
		 model.addAttribute("role", role_account);
		modelAndView.setViewName("/region/search"); // resources/template/admin.html
		return modelAndView;
	}
	
	@RequestMapping(value = "/regional_register", method = RequestMethod.GET)
	public ModelAndView regional_register(ModelMap model) {
		ModelAndView modelAndView = new ModelAndView();
		
		
		User user = new User();
		
		user  = userService.findByUsername(authenticationFacade.getUsername());
		
		
		Set<UserProfile> setOfRole = new HashSet<>();
		setOfRole = user.getUserProfiles();
		System.out.println("ROLE: " + setOfRole);
		List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
		UserProfile role = list.get(0);
		UserProfile role_account = new UserProfile();
		UserProfile role_account2 = new UserProfile();
		UserProfile role_account3 = new UserProfile();
		List<UserProfile> list2 = new ArrayList<UserProfile>();
		if(role.getType().equals("REGIONALADMIN")){
			role_account.setId(6);
			role_account.setType(UserProfileType.REGIONALENCODER.getUserProfileType());
			list2.add(role_account);
			role_account2.setId(7);
			role_account2.setType(UserProfileType.PFOADMIN.getUserProfileType());
			list2.add(role_account2);
			role_account3.setId(8);
			role_account3.setType(UserProfileType.PFOENCODER.getUserProfileType());
			list2.add(role_account3);
		}
		
		List<Provinces> provinces = null;
		
		provinces = provinceService.findByRegion(user.getRegion_id());
				

		model.addAttribute("provinces", provinces);
		model.addAttribute("user", user);
		 model.addAttribute("roles", list2);

		
		
		modelAndView.setViewName("region/register"); // resources/template/register.html
		return modelAndView;
		
	}
	
	@RequestMapping(value = "/saveProvinceRegistration", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String saveProvinceRegistration(@RequestBody UserBean userbean) throws JsonGenerationException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();

		
		boolean getUsername = userService.isUserAlreadyPresent(userbean.getUsername());
		if(getUsername) {
							
//			userbean.setUsername("");
//			 modelAndView.addObject("userForm", userbean);
//			modelAndView.addObject("successMessage", "Username Already taken!");
			return objectMapper.writeValueAsString("Username Already taken!");

			
		}else {
			

			User user = new User();
			User userAdmin = new User();
			System.out.println(authenticationFacade.getUsername() + "authenticationFacade.getUsername()");
			
			userAdmin  = userService.findByUsername(authenticationFacade.getUsername());
			
			
			System.out.println("User Details: " + userAdmin);
			
			
			
			user.setName(userbean.getFirstName());
			user.setLastName(userbean.getLastName());
			user.setUsername(userbean.getUsername());
			user.setState("Active");
			user.setEmail(userbean.getEmail());
			user.setEnabled(true);
			user.setPass(userbean.getPassword());
			Set<UserProfile> userProfiles = new HashSet<UserProfile>();
			UserProfile profile = new UserProfile();
			

			String role = userbean.getUserProfiles();
			String[] role_data = role.split(",", 2);
			
			profile.setId(Integer.parseInt(role_data[0]));	
			profile.setType(role_data[1]);
			
			Set<UserProfile> setOfRole = new HashSet<>();
			setOfRole = userAdmin.getUserProfiles();
	
			List<UserProfile> list = new ArrayList<UserProfile>(setOfRole);
			UserProfile region_role = list.get(0);
			
			if(region_role.getType().equals("REGION")) {
				System.out.println(role_data[1].equals("REGION") + "role_data[1].equals(\"REGION\")");
				
				user.setRegion_id(userAdmin.getRegion_id());
				user.setRegion(userAdmin.getRegion());
				
				
				String province = userbean.getProvince();
				String [] province_data = province.split(",", 2);
				
				user.setProvince(province_data[1]);
				user.setProvince_id(province_data[0]);
				
			}else {
				
				/*
				 * String region = userbean.getRegion(); String [] region_data =
				 * region.split(",", 2);
				 */
				user.setRegion_id(authenticationFacade.getRegionID());
				user.setRegion(authenticationFacade.getRegionName());
			}
			
			userProfiles.add(profile);
			user.setUserProfiles(userProfiles);
			user.setPassword(userbean.getPassword());
									
			userService.saveUser(user);
			
		}
		return objectMapper.writeValueAsString("User is registered successfully!");

	}
	
@RequestMapping(value = "/getProvince/{province}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getProvince(@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException {

	
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(province);
		String gprovince = province;
		String [] province_data = gprovince.split(":", 2);
		System.out.println("province_data[0]: " + province_data[1]);
	
		return jsonString;

	}

@RequestMapping(value = "/getProvinceByRegion", method = RequestMethod.GET, produces = "application/json")
public @ResponseBody String getAllRegion()throws JsonGenerationException, JsonMappingException, IOException {

	 ObjectMapper mapper = new ObjectMapper();
	
		List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		String jsonString = mapper.writeValueAsString(provinces);

	return jsonString;

}

@RequestMapping(value = "/getGeoJsonByRegion", method = RequestMethod.GET, produces = "application/json")
public @ResponseBody String getAllGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

	FeaturesData  featuresData = new FeaturesData();
	return alltoJson(featuresData.getFeaturesListByRegion(authenticationFacade.getRegionID()));

}
/*
private ArrayList<Features> featuresListByRegion(){

	
	ArrayList<Features> list2 = new ArrayList<>();
	ArrayList<FishSanctuaries> fslist = null;
	
	fslist = (ArrayList<FishSanctuaries>) fishSanctuariesService.findByRegionList(authenticationFacade.getRegionID());
       	 
       	 for(FishSanctuaries data : fslist) {
 				
       		Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("fishsanctuaries");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				if(data.getBfardenr().equalsIgnoreCase("BFAR")) {
				features.setInformation("Name: " + data.getNameOfFishSanctuary() + "<br>" +
										"Implementer: " +data.getBfardenr() + "<br>" + 
										"Species: " + data.getSheltered() + "<br>" + 
										"Area: " + data.getArea() + "<br>" + 
										"Type: " + data.getType());
				}else if(data.getBfardenr().equalsIgnoreCase("DENR")){
					features.setInformation("Name: " +data.getNameOfFishSanctuary() + "<br>" +
							"Implementer: " +data.getBfardenr() + "<br>" + 
							"Name of Sensitive Habitat: " + data.getNameOfSensitiveHabitatMPA() + "<br>" + 
							"Ordinance No.: " + data.getOrdinanceNo() + "<br>" +
							"Ordinance Title: " +  data.getOrdinanceTitle()+ "<br>"+
							"Date Established: " + data.getDateEstablished()+"<br>" +
							"Area: " +data.getArea() + "<br>" + 
							"Type: " + data.getType());
				}else {
					features.setInformation("Name: " + data.getNameOfFishSanctuary() + "<br>" +
							"Implementer: " +data.getBfardenr() + "<br>" + 
							"Area: " + data.getArea() + "<br>" + 
							"Type: " + data.getType());
					
				}
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfFishSanctuary());
				features.setImage(data.getImage_src());
	 				list2.add(features);
	 			}
       	 

		ArrayList<FishProcessingPlant> fplist = null;
		
				fplist = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.ListByRegionList(authenticationFacade.getRegionID());
			
				for(FishProcessingPlant data : fplist) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setResources("fishprocessingplants");
					features.setRegion(data.getRegion());
					features.setRegion_id(data.getRegion_id());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setMunicipality(data.getMunicipality());
					features.setMunicipality_id(data.getMunicipality_id());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setInformation("Name: " + data.getNameOfProcessingPlants()+ "<br>" +
											"Name of Operator: " + data.getNameOfOperator()+ "<br>" +
											"Area: " + data.getArea()+ "<br>" +
											"Classification: " + data.getOperatorClassification()+ "<br>" +
											"Processing Technique: " + data.getProcessingTechnique()+ "<br>" +
											"Processing Environment: " + data.getProcessingEnvironmentClassification()+ "<br>" +
											"BFAR Registered: " + data.getBfarRegistered()+ "<br>" +
											"Packaging Type: " + data.getPackagingType()+ "<br>" +
											"Market Reach: " + data.getMarketReach()+ "<br>" +
											"Species: " + data.getIndicateSpecies()+ "<br>" +
											"Packaging Type: " + data.getPackagingType());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfProcessingPlants());
					features.setImage(data.getImage_src());
					list2.add(features);
				}

		ArrayList<FishLanding> fllist = null;

		fllist = (ArrayList<FishLanding>) fishLandingService.findByRegion(authenticationFacade.getRegionID());
			
			for(FishLanding data : fllist) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("fishlanding");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation("Name of Fish Landing: " + data.getNameOfLanding()+ "<br>" +
										"Area (sqm.): " + data.getArea() + "<br>" +
										"Volume of Unloading (MT): " + data.getVolumeOfUnloadingMT()+ "<br>" +
										"Classification: " + data.getClassification()+ "<br>" +
										"Type: " + data.getType());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfLanding());
				features.setImage(data.getImage_src());
				list2.add(features);
			}

		ArrayList<Fishport> portlist = null;

		portlist = (ArrayList<Fishport>) fishPortService.findByRegion(authenticationFacade.getRegionID());
			
			for(Fishport data : portlist) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("FISHPORT");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation("Name of FishPort: " + data.getNameOfFishport()+ "<br>" +
										"Operated by: " + data.getNameOfCaretaker()+ "<br>" +
										"Volume of Unloading: " + data.getVolumeOfUnloading()+ "<br>" +
										"Type: " + data.getType()+ "<br>" +
										"Classification: " + data.getClassification()+ "<br>" +
										"Area (sqm.): " +data.getArea());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfFishport());
				features.setImage(data.getImage_src());
				list2.add(features);
			}		

		ArrayList<FishPen> penlist = null;
		

		penlist = (ArrayList<FishPen>) fishPenService.findByRegion(authenticationFacade.getRegionID());
			
			for(FishPen data : penlist) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("fishpen");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation("Name of Operator: " + data.getNameOfOperator()+ "<br>" +
										"No. of Fish Pen: " + data.getNoOfFishPen()+ "<br>" +
										"Species Cultured: " + data.getSpeciesCultured()+ "<br>" +
										"Area (sqm.): "+ data.getArea());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				//features.setName(data.get);
				features.setImage(data.getImage_src());
				list2.add(features);
			}	

		ArrayList<FishCage> cagelist = null;
		

		cagelist = (ArrayList<FishCage>) fishCageService.getListByRegion(authenticationFacade.getRegionID());
			
			for(FishCage data : cagelist) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("fishcage");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation("Name of Operator: " + data.getNameOfOperator()+ "<br>" +
										"Classification of Operator: " + data.getClassificationofOperator()+ "<br>" +
										"Cage Dimension (LxWxH): " + data.getCageDimension()+ "<br>" +
										"Area (m3): " + data.getArea()+ "<br>" +
										"No. of Compartment: " + data.getCageNoOfCompartments()+ "<br>" +
										"Cage Type: " + data.getCageType()+ "<br>" +
										"Cage Design: " + data.getCageDesign()+ "<br>" +
										"Indicate Species: " + data.getIndicateSpecies());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage_src());
				list2.add(features);
			}				

		
		ArrayList<IcePlantColdStorage> coldlist = null;

		coldlist = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.ListByRegion(authenticationFacade.getRegionID());
			
			for(IcePlantColdStorage data : coldlist) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("coldstorage");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation("Name of Ice Plant/Cold Storage: " + data.getNameOfIcePlant()+ "<br>" +
										"Valid Sanitary Permit: " + data.getWithValidSanitaryPermit()+ "<br>" +
										"Operator: " + data.getOperator()+ "<br>" +
										"Type of Facility: " + data.getTypeOfFacility()+ "<br>" +
										"Structure Type: " + data.getStructureType()+ "<br>" +
										"Capacity: " + data.getCapacity()+ "<br>" +
										"Area (sqm.): " + data.getArea());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfIcePlant());
				features.setImage(data.getImage_src());
				list2.add(features);
			}				

		ArrayList<Market> marketlist = null;
					
		
		marketlist = (ArrayList<Market>) marketService.ListByRegion(authenticationFacade.getRegionID());
			
			for(Market data : marketlist) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("market");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation("Name of Market: " + data.getNameOfMarket()+ "<br>" +
										"Type: " + data.getPublicprivate()+ "<br>" +
										"Classification of Market: " + data.getMajorMinorMarket()+ "<br>" +
										"Volume Traded: " + data.getVolumeTraded()+ "<br>" +
										"Area (sqm.): " + data.getArea());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfMarket());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
	
		ArrayList<SchoolOfFisheries> schoollist = null;
		
		schoollist = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.ListByRegion(authenticationFacade.getRegionID());
			
			for(SchoolOfFisheries data : schoollist) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("schoolOfFisheries");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation("Name of School: " + data.getName()+ "<br>" +
										"Date Established: " + data.getDateEstablished()+ "<br>" +
										"Number of Student Enrolled: " + data.getNumberStudentsEnrolled()+ "<br>" +
										"Area (sqm.): " + data.getArea());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getName());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
		
		ArrayList<FishCoral> corallist = null;
		
		corallist = (ArrayList<FishCoral>) fishCorralService.ListByRegion(authenticationFacade.getRegionID());
			
			for(FishCoral data : corallist) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("fishcoral");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation("Name of Operator: " + data.getNameOfOperator()+ "<br>" +
										"Operator Classification: " + data.getOperatorClassification()+ "<br>" +
										"Gear Use: " + data.getNameOfStationGearUse()+ "<br>" +
										"Unit Use: " + data.getNoOfUnitUse()+ "<br>" +
										"Fishes Caught: " + data.getFishesCaught()+ "<br>" +
										"Area (sqm.): " + data.getArea());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				list2.add(features);
			}
		
		ArrayList<SeaGrass> grasslist = null;
		
		grasslist = (ArrayList<SeaGrass>) seaGrassService.ListByRegion(authenticationFacade.getRegionID());
			
			for(SeaGrass data : grasslist) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setResources("seagrass");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation("Genus Species: " + data.getIndicateGenus() + "<br>" + 
										"Area (sqm.):" + data.getArea());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
		
		ArrayList<Seaweeds> weedslist = null;
		
		
		weedslist = (ArrayList<Seaweeds>) seaweedsService.ListByRegion(authenticationFacade.getRegionID());
			
			for(Seaweeds data : weedslist) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("seaweeds");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation("Indicate Genus: " + data.getIndicateGenus() + "<br>" +
										"Cultured Method Used: " + data.getCulturedMethodUsed() + "<br>" + 
										"Cultured Period: " + data.getCulturedPeriod()+ "<br>" +
										"Volume per Cropping (Kgs.): " + data.getProductionKgPerCropping() + "<br>" + 
										"No. of Cropping per Year: " + data.getProductionNoOfCroppingPerYear()+ "<br>" +
										"Area (sqm.): " + data.getArea());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage_src());
				
				list2.add(features);
			}
			
		
		
		ArrayList<Mangrove> mangrovelist = null;
		
		
		mangrovelist = (ArrayList<Mangrove>) mangroveService.ListByRegion(authenticationFacade.getRegionID());
			
			for(Mangrove data : mangrovelist) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("mangrove");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation("Indicate Species: " + data.getIndicateSpecies()+ "<br>" +
										"Type: " + data.getType() + "<br>" + 
										"Area (sqm.) : " + data.getArea());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getType());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
		
		ArrayList<LGU> pfolist = null;
		
		pfolist = (ArrayList<LGU>) lGUService.ListByRegion(authenticationFacade.getRegionID());
			
			for(LGU data : pfolist) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("lgu");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation("PFO Name: " + data.getLguName()+ "<br>" + 
										"Number of Municipality Coastal: " + data.getNoOfBarangayCoastal() + "<br>" + 
										"Number of Municipality Inland: " + data.getNoOfBarangayInland()+ "<br>" +
										"LGU Coastal Length(km.): " + data.getLguCoastalLength());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getLguName());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
		
		ArrayList<MaricultureZone> zonelist = null;
	
		zonelist = (ArrayList<MaricultureZone>) zoneService.ListByRegion(authenticationFacade.getRegionID());
			
			for(MaricultureZone data : zonelist) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("mariculturezone");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation("Name of Mariculture: " + data.getNameOfMariculture()+ "<br>" +
										"Hectare:" + data.getArea() + "<br>" + 
										"Date Launched: " + data.getDateLaunched()+ "<br>" +
										"Date Enacted: " + data.getDateInacted() + "<br>" + 
										"eCcNumber: " + data.getEccNumber() + "<br>" + 
										"Mariculture Type: " + data.getMaricultureType() + "<br>" + 
										"Municipal Ordinance Number: " + data.getMunicipalOrdinanceNo() + "<br>" + 
										"Date Approved: " + data.getDateApproved());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfMariculture());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
		
		ArrayList<TrainingCenter> traininglist = null;
		
		traininglist = (ArrayList<TrainingCenter>) trainingCenterService.ListByRegion(authenticationFacade.getRegionID());
			
			for(TrainingCenter data : traininglist) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setResources("trainingcenter");
				features.setRegion(data.getRegion());
				features.setRegion_id(data.getRegion_id());
				features.setProvince(data.getProvince());
				features.setProvince_id(data.getProvince_id());
				features.setMunicipality(data.getMunicipality());
				features.setMunicipality_id(data.getMunicipality_id());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setInformation("Name of Training Center: " + data.getName()+ "<br>" +
										"Specialization: " + data.getSpecialization()+ "<br>" +
										"Facility Within The Training Center: " + data.getFacilityWithinTheTrainingCenter()+ "<br>" +
										"Type: " + data.getType());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getName());
				features.setImage(data.getImage_src());
				list2.add(features);
			}
	
	
	
	
	
	return list2;
	
	

	
}
*/
/*	@RequestMapping(value = "getProvince", method = RequestMethod.POST)
	public ModelAndView getProvince(@RequestBody UserBean userBean,ModelMap model) {
		ModelAndView modelAndView = new ModelAndView();
		
		
		String province = userBean.getProvince();
		String [] province_data = province.split(",", 2);
		
		setProvince_Id(province_data[0]);
		System.out.println("getProvince/{province}: " + province_Id);
		User user = new User();
		
		user  = userService.findByUsername(authenticationFacade.getUsername());
		List<Provinces> provinces = null;
		
		provinces = provinceService.findByRegion(user.getRegion_id());
				

		model.addAttribute("provinces", provinces);
		
		modelAndView.setViewName("redirect:home"); // resources/template/admin.html
		return modelAndView;
	}*/
	@RequestMapping(value = "/getProvinceForMap", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getProvinceForMap()throws JsonGenerationException, JsonMappingException, IOException {

		
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(province_Id);
		
		return jsonString;

	}
	
	@RequestMapping(value = "/getFSGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFSGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

		
			return toJson(featuresList("fishsanctuaries",""));
		
		

	}
//	@RequestMapping(value = "/getFSGeoJson/{uniqueKey}", method = RequestMethod.GET, produces = "application/json")
//	public @ResponseBody String getFSGeoJsonbyID(@PathVariable("uniqueKey") String uniqueKey)throws JsonGenerationException, JsonMappingException, IOException {
//
//		return toJson(featuresListByID("fishsanctuaries",uniqueKey));
//
//	}
	@RequestMapping(value = "/getFPGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFPGeoJson()throws JsonGenerationException, JsonMappingException, IOException {
		
			return toJson(featuresList("fishprocessingplants",""));
		
		

	}
	@RequestMapping(value = "/getFLGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFLGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

			return toJson(featuresList("fishlanding",""));
		
		

	}
	
	@RequestMapping(value = "/getFPTGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFPTGeoJson()throws JsonGenerationException, JsonMappingException, IOException {
		
			return toJson(featuresList("FISHPORT",""));

	}
	
	@RequestMapping(value = "/getFPenGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFPenGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

			return toJson(featuresList("fishpen",""));

	}
	
	@RequestMapping(value = "/getFCageGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFCageGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

			return toJson(featuresList("fishcage",""));

	}
	
	@RequestMapping(value = "/getIPCSGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getIPCSGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

			return toJson(featuresList("iceplantorcoldstorage",""));

	}
	
	@RequestMapping(value = "/getMarketGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMarketGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

			return toJson(featuresList("market",""));

	}
	
	@RequestMapping(value = "/getSchoolGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSchoolGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

			return toJson(featuresList("schooloffisheries",""));

	}
	@RequestMapping(value = "/getFishCoralsGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getFishCoralsGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

			return toJson(featuresList("fishcorals",""));

	}
	
	@RequestMapping(value = "/getSeagrassGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSeagrassGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

			return toJson(featuresList("seagrass",""));

	}
	
	@RequestMapping(value = "/getSeaweedsGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getSeaweedsGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

			return toJson(featuresList("seaweeds",""));

	}
	
	@RequestMapping(value = "/getMangroveGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMangroveGeoJson()throws JsonGenerationException, JsonMappingException, IOException {
	
			return toJson(featuresList("mangrove",""));
	
	}
	
	@RequestMapping(value = "/getLGUGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getLGUGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

	
			return toJson(featuresList("lgu",""));

	}
	
	@RequestMapping(value = "/getMaricultureGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMaricultureGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

			return toJson(featuresList("mariculturezone",""));
	
	}
	
	@RequestMapping(value = "/getTrainingcenterGeoJson", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getTrainingcenterGeoJson()throws JsonGenerationException, JsonMappingException, IOException {

			return toJson(featuresList("trainingcenter",""));
	
	}
	
	
	private ArrayList<Features> featuresList(String page,String province_id){
	System.out.println("province_id: " + province_id);
	 List<String> roles = new ArrayList<String>();
	
	Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
   if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {

   }else {
   	Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
   	 
      

       for (GrantedAuthority a : authorities) {
           roles.add(a.getAuthority());
       }
       
       if (roles.contains("ROLE_USER")) {

       }
       else if (roles.contains("ROLE_ENCODER")) {

       }
       else if (roles.contains("ROLE_ADMIN")) {

       }
       else if (roles.contains("ROLE_SUPERADMIN")) {

       }else if (roles.contains("ROLE_SUPERVISOR")) {

       }else {

       	
       }
   }
	
	ArrayList<Features> list2 = new ArrayList<>();
	
	 System.out.println("getProvince_Id() : " +getProvince_Id());
	if(page == "fishsanctuaries") {
		ArrayList<FishSanctuaries> list = null;
		 if(roles.contains("ROLE_REGION")) {
			 
				 list = (ArrayList<FishSanctuaries>) fishSanctuariesService.findByRegionList(authenticationFacade.getRegionID());
		       	  
			 
       	 
       	 for(FishSanctuaries data : list) {
 				
	 				Features features = new Features();
	 				features.setArea(data.getArea());
	 				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
	 				features.setLatitude(data.getLat());
	 				features.setLongtitude(data.getLon());
	 				features.setName(data.getNameOfFishSanctuary());
	 				features.setImage(data.getImage());
	 				list2.add(features);
	 			}
       	 
        }
	}else if(page == "fishprocessingplants") {
		ArrayList<FishProcessingPlant> list = null;
		
			if(roles.contains("ROLE_REGION")) {
				
				list = (ArrayList<FishProcessingPlant>) fishProcessingPlantService.ListByRegionList(authenticationFacade.getRegionID());
			
				for(FishProcessingPlant data : list) {
					Features features = new Features();
					features.setArea(data.getArea());
					features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
					features.setProvince(data.getProvince());
					features.setProvince_id(data.getProvince_id());
					features.setLatitude(data.getLat());
					features.setLongtitude(data.getLon());
					features.setName(data.getNameOfProcessingPlants());
					features.setImage(data.getImage());
					list2.add(features);
				}
			}
		
	}else if(page == "fishlanding") {
		ArrayList<FishLanding> list = null;
		if(roles.contains("ROLE_REGION")) {
			list = (ArrayList<FishLanding>) fishLandingService.findByRegion(authenticationFacade.getRegionID());
			
			for(FishLanding data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfLanding());
				features.setImage(data.getImage());
				list2.add(features);
			}
		}
	}else if(page == "FISHPORT") {
		ArrayList<Fishport> list = null;
		 if(roles.contains("ROLE_REGION")) {
			list = (ArrayList<Fishport>) fishPortService.findByRegion(authenticationFacade.getRegionID());
			
			for(Fishport data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfFishport());
				features.setImage(data.getImage());
				list2.add(features);
			}		
		}
		
	}else if(page == "fishpen") {
		ArrayList<FishPen> list = null;
		 if(roles.contains("ROLE_REGION")) {
			list = (ArrayList<FishPen>) fishPenService.findByRegion(authenticationFacade.getRegionID());
			
			for(FishPen data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				//features.setName(data.get);
				features.setImage(data.getImage());
				list2.add(features);
			}	
		}
	}else if(page == "fishcage") {
		ArrayList<FishCage> list = null;
	if(roles.contains("ROLE_REGION")) {

			list = (ArrayList<FishCage>) fishCageService.getListByRegion(authenticationFacade.getRegionID());
			
			for(FishCage data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				list2.add(features);
			}				
		}
	}else if(page == "iceplantorcoldstorage") {
		
		ArrayList<IcePlantColdStorage> list = null;
		if(roles.contains("ROLE_REGION")) {
	
			list = (ArrayList<IcePlantColdStorage>) icePlantColdStorageService.ListByRegion(authenticationFacade.getRegionID());
			
			for(IcePlantColdStorage data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfIcePlant());
				features.setImage(data.getImage());
				list2.add(features);
			}				
		}
	}else if(page == "market") {
		ArrayList<Market> list = null;
		
		if(roles.contains("ROLE_REGION")) {
			
			list = (ArrayList<Market>) marketService.ListByRegion(authenticationFacade.getRegionID());
			
			for(Market data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfMarket());
				features.setImage(data.getImage());
				list2.add(features);
			}
		}
	}else if(page == "schooloffisheries") {
		ArrayList<SchoolOfFisheries> list = null;
		
		if(roles.contains("ROLE_REGION")) {
			list = (ArrayList<SchoolOfFisheries>) schoolOfFisheriesService.ListByRegion(authenticationFacade.getRegionID());
			
			for(SchoolOfFisheries data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getName());
				features.setImage(data.getImage());
				list2.add(features);
			}
		}
	}else if(page == "fishcorals") {
		
		ArrayList<FishCoral> list = null;
		
		if(roles.contains("ROLE_REGION")) {
			list = (ArrayList<FishCoral>) fishCorralService.ListByRegion(authenticationFacade.getRegionID());
			
			for(FishCoral data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				list2.add(features);
			}
		}
	}else if(page == "seagrass") {
		
		ArrayList<SeaGrass> list = null;
		
		if(roles.contains("ROLE_REGION")) {
	
			list = (ArrayList<SeaGrass>) seaGrassService.ListByRegion(authenticationFacade.getRegionID());
			
			for(SeaGrass data : list) {
				Features features = new Features();
				features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				list2.add(features);
			}
		}
	}else if(page == "seaweeds") {
		
		ArrayList<Seaweeds> list = null;
		
		if(roles.contains("ROLE_REGION")) {
			list = (ArrayList<Seaweeds>) seaweedsService.ListByRegion(authenticationFacade.getRegionID());
			
			for(Seaweeds data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setImage(data.getImage());
				
				list2.add(features);
			}
			
		}
	}else if(page == "mangrove") {
		
		ArrayList<Mangrove> list = null;
		
		if(roles.contains("ROLE_REGION")) {
	
			list = (ArrayList<Mangrove>) mangroveService.ListByRegion(authenticationFacade.getRegionID());
			
			for(Mangrove data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getType());
				features.setImage(data.getImage());
				list2.add(features);
			}
			
		}
	}else if(page == "lgu") {
		
		ArrayList<LGU> list = null;
		
		if(roles.contains("ROLE_REGION")) {
	
			list = (ArrayList<LGU>) lGUService.ListByRegion(authenticationFacade.getRegionID());
			
			for(LGU data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getLguName());
				features.setImage(data.getImage());
				list2.add(features);
			}
		}
	}else if(page == "mariculturezone") {
		
		ArrayList<MaricultureZone> list = null;
		
		 if(roles.contains("ROLE_REGION")) {
	
			list = (ArrayList<MaricultureZone>) zoneService.ListByRegion(authenticationFacade.getRegionID());
			
			for(MaricultureZone data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getNameOfMariculture());
				features.setImage(data.getImage());
				list2.add(features);
			}
		}
	}else if(page == "trainingcenter") {
		ArrayList<TrainingCenter> list = null;
		
		 if(roles.contains("ROLE_REGION")) {
	
			list = (ArrayList<TrainingCenter>) trainingCenterService.ListByRegion(authenticationFacade.getRegionID());
			
			for(TrainingCenter data : list) {
				Features features = new Features();
				//features.setArea(data.getArea());
				features.setLocation(data.getRegion()+"," +data.getProvince() + ","+ data.getMunicipality()+ "," + data.getBarangay());
				features.setLatitude(data.getLat());
				features.setLongtitude(data.getLon());
				features.setName(data.getName());
				features.setImage(data.getImage());
				list2.add(features);
			}
		}
	}
	
	
	
	return list2;
	
	}

	private String alltoJson(ArrayList<Features> list){



		  JSONObject featureCollection = new JSONObject();
		  JSONArray features = new JSONArray();
		  for(Features eachElement : list){
			  
		    	JSONObject geometry = new JSONObject();
		    	JSONObject featuresProperties = new JSONObject();
		        JSONArray JSONArrayCoord = new JSONArray();
		        JSONObject feature = new JSONObject();
				  feature.put("type", "Feature");

		        JSONArrayCoord.put(0, eachElement.getLongtitude());
		        JSONArrayCoord.put(1, eachElement.getLatitude());

		        geometry.put("type", "Point");
		        geometry.put("coordinates", JSONArrayCoord);
		        
		        featuresProperties.put("Latitude", eachElement.getLongtitude());
		        featuresProperties.put("Longitude", eachElement.getLatitude());
		        featuresProperties.put("Name", eachElement.getName());
		        featuresProperties.put("Image", eachElement.getImage());
		        featuresProperties.put("Location", eachElement.getLocation());	 
		        featuresProperties.put("Information", eachElement.getInformation());
		        featuresProperties.put("resources", eachElement.getResources());
		        featuresProperties.put("Region", eachElement.getRegion_id());
		        featuresProperties.put("Province", eachElement.getProvince_id());
		        featuresProperties.put("Municipality", eachElement.getMunicipality_id());
		        feature.put("properties", featuresProperties);
		        feature.put("geometry", geometry);
		        features.put(feature);
		  }

		 
	        featureCollection.put("features", features);
		
			 featureCollection.put("type", "FeatureCollection");
			  JSONObject properties = new JSONObject();
			  properties.put("name", "urn:ogc:def:crs:OGC:1.3:CRS84");
			  JSONObject crs = new JSONObject();
			  crs.put("type", "name");
			  crs.put("properties", properties);
			  featureCollection.put("crs", crs);
			  
		  return featureCollection.toString();
		
	}
	
	private String toJson(ArrayList<Features> list){


		  JSONObject featureCollection = new JSONObject();
		  JSONArray features = new JSONArray();
		  for(Features eachElement : list){
			  
		    	JSONObject geometry = new JSONObject();
		    	JSONObject featuresProperties = new JSONObject();
		        JSONArray JSONArrayCoord = new JSONArray();
		        JSONObject feature = new JSONObject();
				  feature.put("type", "Feature");

		        JSONArrayCoord.put(0, eachElement.getLongtitude());
		        JSONArrayCoord.put(1, eachElement.getLatitude());

		        geometry.put("type", "Point");
		        geometry.put("coordinates", JSONArrayCoord);
		        
		        featuresProperties.put("Latitude", eachElement.getLongtitude());
		        featuresProperties.put("Longitude", eachElement.getLatitude());
		        featuresProperties.put("Name", eachElement.getName());
		        featuresProperties.put("Image", eachElement.getImage());
		        featuresProperties.put("Location", eachElement.getLocation());	
		        featuresProperties.put("Province_ID", eachElement.getProvince_id());
		        feature.put("properties", featuresProperties);
		        feature.put("geometry", geometry);
		        features.put(feature);
		  }

		 
	        featureCollection.put("features", features);
		
			 featureCollection.put("type", "FeatureCollection");
			  JSONObject properties = new JSONObject();
			  properties.put("name", "urn:ogc:def:crs:OGC:1.3:CRS84");
			  JSONObject crs = new JSONObject();
			  crs.put("type", "name");
			  crs.put("properties", properties);
			  featureCollection.put("crs", crs);
			  
		  return featureCollection.toString();
		}
	

	public String getProvince_Id() {
		return province_Id;
	}
	public void setProvince_Id(String province_Id) {
		this.province_Id = province_Id;
	}
	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public ModelAndView reportall(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
 
		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		model.addAttribute("user", user);		
		modelAndView.setViewName("users/report");
		return modelAndView;
		
	}
	
	@RequestMapping(value = "bar", method = RequestMethod.GET)
	public ModelAndView provincebChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		User user = new User();		 
		user = authenticationFacade.getUserInfo();
		 List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
	
		 	model.addAttribute("region_title",authenticationFacade.getRegionName());
//			model.addAttribute("chart_by_region", getByRegion(authenticationFacade.getRegionID()));
//			model.addAttribute("province_title",provinceName);
			//model.addAttribute("provinceChart",chartsUtil.getChartByProvince(provinces));
			model.addAttribute("provinces", provinces);
			model.addAttribute("user", user);
			model.addAttribute("page",false);
			model.addAttribute("show",true);
			modelAndView.setViewName("/users/Bar");
			
		return modelAndView;
	}
	@RequestMapping(value = "bar/province", method = RequestMethod.GET)
	public ModelAndView muniChart(ModelMap model)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		List<Provinces> provinces = provinceService.findByRegion(authenticationFacade.getRegionID());
		 List<Municipalities> municipalities = municipalityService.findByProvince(provinceID);

		 
		 	model.addAttribute("region_title",authenticationFacade.getRegionName());
			model.addAttribute("province_title",provinceName);
			//model.addAttribute("provinceChart",chartsUtil.getChartByMunicipality(municipalities));
			model.addAttribute("provinces", provinces);
			model.addAttribute("municipalities", municipalities);
			modelAndView.setViewName("/region/BarProvince");
			
		return modelAndView;
	}
	
	@RequestMapping(value = "/bar/{province}", method = RequestMethod.GET)
	public ModelAndView regionChart(ModelMap model,@PathVariable("province") String province)throws JsonGenerationException, JsonMappingException, IOException  {
		ModelAndView modelAndView = new ModelAndView();
		
		System.out.println("province: " + province);
		System.out.println("provinceID: " + provinceID+  "----"+ "provinceName: " + provinceName);
		
		if(province != "all") {
		String s = province;
		String[] data = s.split(",", 2);
		provinceID = data[0];
		provinceName= data[1]; 
		}else {
			
		}

		modelAndView.setViewName("redirect:../bar/province");
		return modelAndView;
		
	}

}
