package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ph.gov.da.bfar.frgis.Page.FishLandingPage;
import ph.gov.da.bfar.frgis.model.FishLanding;



public interface FishLandingService {
	
	Optional<FishLanding> findById(int id);
	
	List<FishLanding> findByUsername(String region);
	FishLanding findByUniqueKey(String uniqueKey);
	void saveFishLanding(FishLanding FishLanding);
	void deleteFishLandingById(int id);
	void updateForEnabled(int  id);
	void updateFishLandingByReason(int  id,String  reason);
	List<FishLanding> findByUniqueKeyForUpdate(String uniqueKey);
	List<FishLanding> findAllFishLandings(); 
	List<FishLanding> findAllFishLandings(String cflc_status); 
	List<FishLanding> findAllFishLandingsByRegion(String region,String cflc_status);
    public List<FishLanding> getAllFishLandings(int index,int pagesize,String user);
    public int FishLandingCount(String username);
    Page<FishLandingPage> findByUsername(String user, Pageable pageable);
    Page<FishLandingPage> findByRegion(String region, Pageable pageable);
    Page<FishLandingPage> findAllFishLanding(Pageable pageable);
    public Long countFishLandingPerRegion(String region_id);
    public Long countFishLandingPerProvince(String province_id);
    public Long countFishLandingPerMinicipality(String municipality_id);
    
    public Long countFishLandingPerRegion(String region_id,String type);
    public Long countFishLandingPerProvince(String province_id,String type);
    public Long countFishLandingPerMinicipality(String municipality_id,String type);
    
    public Long countFishLandingPerRegion(String region_id,String type,String cflc_status);
    public Long countFishLandingPerProvince(String province_id,String type,String cflc_status);
    public Long countFishLandingPerMinicipality(String municipality_id,String type,String cflc_status);
    
    public Long countAllFishLanding();
    List<FishLanding> findByRegion(String regionID);


}