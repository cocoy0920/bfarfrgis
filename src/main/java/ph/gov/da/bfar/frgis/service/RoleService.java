package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;


import ph.gov.da.bfar.frgis.model.Role;



public interface RoleService {
	
	Optional<Role> findById(int id);
	
	void save(Role role);
	
		
	List<Role> findAllRoles();
	


}