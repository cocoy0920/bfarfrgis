package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.model.User;

@Repository
public interface UserRepository extends JpaRepository<User,Integer>{

	
	@Query("SELECT u from User u Where u.username = :username")
	public User getUserByUsername(@Param("username") String username);
	
	@Query("SELECT u from User u Where u.username = :username")
	public List<User>  ListfindByUsername(@Param("username") String username);
	
	//@Query("SELECT c.username FROM User c WHERE c.username = :username")
	@Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM User c WHERE c.username = :username")
	public boolean isUserAlreadyPresent(@Param("username") String username);
}
