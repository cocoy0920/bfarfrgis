package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.MangrovePage;
import ph.gov.da.bfar.frgis.model.Mangrove;

@Repository
public interface MangroveRepo extends JpaRepository<Mangrove, Integer> {

	@Query("SELECT c from Mangrove c where c.user = :username")
	public List<Mangrove> getListMangroveByUsername(@Param("username") String username);
	
	@Query("SELECT c from Mangrove c where c.region_id = :region_id")
	public List<Mangrove> getListMangroveByRegion(@Param("region_id") String region_id);

	@Modifying(clearAutomatically = true)
	@Query("update Mangrove f set f.enabled =false  where f.id =:id")
	public void updateByEnabled( @Param("id") int id);
	
	@Modifying(clearAutomatically = true)
	@Query("update Mangrove f set f.reason =:reason  where f.id =:id")
	public void updateMangroveByReason( @Param("id") int id, @Param("reason") String reason);
	
	@Query("SELECT COUNT(f) FROM Mangrove f WHERE f.user=:username")
	public int  MangroveCount(@Param("username") String username);
	
	@Query("SELECT c from Mangrove c where c.uniqueKey = :uniqueKey")
	public Mangrove findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from Mangrove c where c.uniqueKey = :uniqueKey")
	List<Mangrove> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from Mangrove c where c.user = :username")
	 public List<Mangrove> getPageableMangrove(@Param("username") String username,Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.MangrovePage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.indicateSpecies,c.lat,c.lon,c.enabled) from Mangrove c where c.user = :user")
	public Page<MangrovePage> findByUsername(@Param("user") String user, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.MangrovePage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.indicateSpecies,c.lat,c.lon,c.enabled) from Mangrove c where c.region_id = :region_id")
	public Page<MangrovePage> findByRegion(@Param("region_id") String region_id, Pageable pageable);

	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.MangrovePage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.indicateSpecies,c.lat,c.lon,c.enabled) from Mangrove c")
	public Page<MangrovePage> findAllMangrove(Pageable pageable);

	@Query("SELECT COUNT(f) FROM Mangrove f")
	public Long countAllMangrove();
	
	@Query("SELECT COUNT(f) FROM Mangrove f WHERE f.region_id=:region_id")
	public Long countMangrovePerRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM Mangrove f WHERE f.province_id=:province_id")
	public Long countMangrovePerProvince(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM Mangrove f WHERE f.municipality_id=:municipality_id")
	public Long countMangrovePerMunicipality(@Param("municipality_id") String municipality_id);
}
