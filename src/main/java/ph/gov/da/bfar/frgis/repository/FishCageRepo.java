package ph.gov.da.bfar.frgis.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ph.gov.da.bfar.frgis.Page.FishCagePage;
import ph.gov.da.bfar.frgis.model.FishCage;


@Repository
public interface FishCageRepo extends JpaRepository<FishCage, Integer> {

	@Query("SELECT c from FishCage c where c.user = :username")
	public List<FishCage> getListFishCageByUsername(@Param("username") String username);
	
	@Query("SELECT c from FishCage c where c.region_id = :region_id")
	public List<FishCage> getListFishCageByRegion(@Param("region_id") String region_id);
	
	@Query("SELECT COUNT(f) FROM FishCage f WHERE f.user=:username")
	public int  FishCageCount(@Param("username") String username);
	
	@Query("SELECT c from FishCage c where c.uniqueKey = :uniqueKey")
	public FishCage findByUniqueKey(@Param("uniqueKey") String uniqueKey);
	
	@Modifying(clearAutomatically = true)
	@Query("update FishCage f set f.reason =:reason  where f.id =:id")
	public void updateFishCageByReason( @Param("id") int id, @Param("reason") String reason);
	
	@Modifying(clearAutomatically = true)
	@Query("update FishCage f set f.enabled =false  where f.id =:id")
	public void updateFishCageByEnabled( @Param("id") int id);
	
	@Query("SELECT c from FishCage c where c.uniqueKey = :uniqueKey")
	List<FishCage> findByUniqueKeyForUpdate(@Param("uniqueKey") String uniqueKey);
	
	@Query("SELECT c from FishCage c where c.user = :username")
	 public List<FishCage> getPageableFishCages(@Param("username") String username,Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishCagePage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfOperator,c.lat,c.lon,c.enabled)  from FishCage c where c.user = :user")
	public Page<FishCagePage> findByUsername(@Param("user") String user, Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishCagePage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfOperator,c.lat,c.lon,c.enabled)  from FishCage c where c.region_id = :region_id")
	public Page<FishCagePage> findByRegion(@Param("region_id") String region_id, Pageable pageable);
	
	@Query("SELECT NEW ph.gov.da.bfar.frgis.Page.FishCagePage ( c.id,c.uniqueKey,c.region,c.province,c.municipality,c.barangay,c.nameOfOperator,c.lat,c.lon,c.enabled)  from FishCage c")
	public Page<FishCagePage> findAllFishcage(Pageable pageable);
	
	
	
	@Query("SELECT COUNT(f) FROM FishCage f")
	public Long countAllFishCage();
	
	 @Query("SELECT COUNT(f) FROM FishCage f WHERE f.region_id=:region_id")
	 public Long countFishCagePerRegion(@Param("region_id") String region_id);
	 
	@Query("SELECT COUNT(f) FROM FishCage f WHERE f.province_id=:province_id")
	public Long countFishCagePerProvince(@Param("province_id") String province_id);
	
	@Query("SELECT COUNT(f) FROM FishCage f WHERE f.municipality_id=:municipality_id")
	public Long countFishCagePerMunicipality(@Param("municipality_id") String municipality_id);

	@Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM FishCage c WHERE c.image = :image")
	public boolean isImageAlreadyPresent(@Param("image") String image);

	
}
