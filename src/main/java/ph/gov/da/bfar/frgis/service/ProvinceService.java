package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import ph.gov.da.bfar.frgis.model.Provinces;


public interface ProvinceService {
	
	Optional<Provinces> findById(int id);
	
	void save(Provinces provinces);
	
	void update(Provinces province);
		
	List<Provinces> findByRegion(String regionId);
	
	List<Provinces> findAllProvinces();

}