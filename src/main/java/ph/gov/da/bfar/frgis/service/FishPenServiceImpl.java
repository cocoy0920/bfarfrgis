package ph.gov.da.bfar.frgis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ph.gov.da.bfar.frgis.Page.FishPenPage;
import ph.gov.da.bfar.frgis.model.FishPen;
import ph.gov.da.bfar.frgis.repository.FishPenRepo;


@Service("FishPenService")
@Transactional
public class FishPenServiceImpl implements FishPenService{

	@Autowired
	private FishPenRepo dao;

	public Optional<FishPen> findById(int id) {
		return dao.findById(id);
	}

	public List<FishPen> findByUsername(String username) {
		List<FishPen>FishPen = dao.getListFishPenByUsername(username);
		return FishPen;
	}

	public void saveFishPen(FishPen FishPen) {
		dao.save(FishPen);
	}

	@Override
	public void updateForEnabled(int id) {
		dao.updateByEnabled(id);
		
	}

	@Override
	public void updateFishPenByReason(int id, String reason) {
		dao.updateFishPenByReason(id, reason);
		
	}

	@Override
	public void deleteFishPenById(int id) {
		dao.deleteById(id);
		
	}

	public List<FishPen> findAllFishPens() {
		return dao.findAll();
	}

	public List<FishPen> getAllFishPens(int index, int pagesize, String user) {
		
		Pageable secondPageWithFiveElements = PageRequest.of(index, pagesize);
		List<FishPen> page = dao.getPageableFishPens(user,secondPageWithFiveElements);
				 
		return page;
	}

	public int FishPenCount(String username) {
		return dao.FishPenCount(username);
	}

	@Override
	public FishPen findByUniqueKey(String uniqueKey) {
		
		return dao.findByUniqueKey(uniqueKey);
	}

	@Override
	public List<FishPen> findByUniqueKeyForUpdate(String uniqueKey) {
		
		return dao.findByUniqueKeyForUpdate(uniqueKey);
	}

	@Override
	public Page<FishPenPage> findByUsername(String user, Pageable pageable) {
		
		return dao.findByUsername(user, pageable);
	}
	
	@Override
	public Page<FishPenPage> findAllFishPen(Pageable pageable) {

		return dao.findAllFishPen(pageable);
	}
	
	@Override
	public Page<FishPenPage> findByRegion(String region, Pageable pageable) {

		return dao.findByRegion(region, pageable);
	}

	@Override
	public List<FishPen> findByRegion(String region_id) {
		
		return dao.getListFishPenByRegion(region_id);
	}

	@Override
	public Long countFishPenPerRegion(String region_id) {

		return dao.countFishPenPerRegion(region_id);
	}

	@Override
	public Long countFishPenPerProvince(String province_id) {
	
		return dao.countFishPenPerProvince(province_id);
	}

	@Override
	public Long countAllFishPen() {
		return dao.countAllFishPen();
	}

	@Override
	public Long countFishPenPerMunicipality(String municipality_id) {
		
		return dao.countFishPenPerMunicipality(municipality_id);
	}

//	@Override
//	public int FishPenCountByMuunicipality(String municipalityID) {
//		return dao.FishPenCountByMuunicipality(municipalityID);
//	}
//
//	@Override
//	public int FishPenCountByProvince(String provinceID) {
//		return dao.FishPenCountByProvince(provinceID);
//	}
//
//	@Override
//	public int FishPenCountByRegion(String regionID) {
//		return dao.FishPenCountByRegion(regionID);
//	}

/*	public boolean isFishPenSSOUnique(Integer id, String sso) {
		FishPen FishPen = findBySSO(sso);
		return ( FishPen == null || ((id != null) && (FishPen.getId() == id)));
	}*/
	
}
